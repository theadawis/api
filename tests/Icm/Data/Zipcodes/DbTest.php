<?php

class Icm_Data_Zipcodes_Db_Test extends  PHPUnit_Framework_TestCase
{
    public function testGetEthnicityData(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb');
        $zip = new Icm_Service_Zipcodes_Db(Icm_Db_Pdo::connect('checkmate', $config));
        $data = $zip->getEthnicityData(92109);
        $this->assertGreaterThan(0, count($data));
        $this->assertTrue(array_key_exists('WhitePopulation', $data));
    }

    public function testGetGenderData(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb');
        $zip = new Icm_Service_Zipcodes_Db(Icm_Db_Pdo::connect('checkmate', $config));
        $data = $zip->getGenderData(92109);
        $this->assertGreaterThan(0, count($data));
        $this->assertTrue(array_key_exists('MalePopulation', $data));
        $this->assertTrue(array_key_exists('FemalePopulation', $data));
    }
}