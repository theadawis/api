<?php

class Icm_Data_Whitepages_Test extends  PHPUnit_Framework_TestCase {

    /**
     * @var Icm_Search_Engine
     */
    protected $search;
    protected $api;
    protected $container;

    public function setUp() {
        // boostrap the api
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->container = new Icm_Container_Factory();
        $this->container->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        $search = new Icm_Search_Engine();
        $wpAdapter = $this->container->get('whitepagesSearchAdapter');

        $search->addAdditiveAdapter($wpAdapter);
        $this->search = $search;
    }

    public function testReversePhoneMobile() {
        $query = new Icm_Search_Query(
            array(
                Icm_Search_Engine::CRITERIA_PHONENUMBER => '8584052745'
            ), 'reversePhone'
        );

        $query->setAllowPaid(true);

        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);

        $this->assertTrue(count($results) > 0);

        foreach($results as $result){
            $this->assertInstanceOf('Icm_Entity_Phone', $result);
        }
    }

    public function testReversePhoneLand() {
        $query = new Icm_Search_Query(
            array(
                Icm_Search_Engine::CRITERIA_PHONENUMBER => '9782497413'
            ), 'reversePhone'
        );

        $query->setAllowPaid(true);

        $query->isA(Icm_Search_Engine::IS_A_PERSON);
        $query->hasA('first_name');

        $results = $this->search->search($query);

        $this->assertTrue(count($results) > 0);

        foreach($results as $result){
            $this->assertInstanceOf('Icm_Entity_Phone', $result);
        }
    }

    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testReversePhoneBattery($phone) {
        $query = new Icm_Search_Query(
            array(
                Icm_Search_Engine::CRITERIA_PHONENUMBER => $phone
            ), 'reversePhone'
        );

        $query->setAllowPaid(true);

        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);
    }

    public function testReversePhoneFake() {
        $query = new Icm_Search_Query(array(
            Icm_Search_Query::CRITERIA_PHONENUMBER =>'8313595555'
        ), 'reversePhone');

        $query->setAllowPaid(true);

        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);

        $this->assertTrue(count($results) == 0);
    }

    public function testReversePhoneAlpha() {
        $query = new Icm_Search_Query(array(), 'reversePhone');

        $query->addCriteria(Icm_Search_Engine::CRITERIA_PHONENUMBER, 'abcdefhijk');
        $query->setAllowPaid(true);
        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);

        $this->assertTrue(count($results) == 0);
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneNoCriteria() {
        $query = new Icm_Search_Query(array(), 'reversePhone');
        $query->setAllowPaid(true);
        $results = $this->search->search($query);
        $this->assertTrue(is_array($results));
        $this->assertTrue(count($results) == 0);
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneNull() {
        $query = new Icm_Search_Query(array(), 'reversePhone');

        $query->addCriteria(Icm_Search_Engine::CRITERIA_PHONENUMBER, null);
        $query->setAllowPaid(true);
        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);

        $this->assertCount(0, $results);
    }

    public function testReversePhoneWrongFilters() {
        $query = new Icm_Search_Query(array(), 'reversePhone');

        $query->addCriteria(Icm_Search_Engine::CRITERIA_PHONENUMBER, '9782497413');
        $query->setAllowPaid(true);
        $query->isA(Icm_Search_Engine::IS_A_BUSINESS);
        $results = $this->search->search($query);

        $this->assertInstanceOf('Icm_Collection', $results);

        // should be just one default result
        $this->assertEquals(1, $results->count());

        foreach ($results->getData() as $result) {
            $this->assertInstanceOf('Icm_Entity_Phone', $result);
        }
    }

    public function phoneProvider() {
        return array(
            array('8584052745'),
            array('4135637167'),
            array('8313595555'),
            array('6192971888'),
            array('6197024419'),
            array('9782497413'),
            array('9782499834'),
            array('5083643339'),
            array('8584562111'),
            array('6192200268'),
            array('6192762800'),
            array('8582684933'),
        );
    }
}