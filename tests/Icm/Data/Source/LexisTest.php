<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/23/12
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Data_Source_Lexis_Test extends PHPUnit_Framework_TestCase
{

    /**
     * @var Icm_Search_Engine
     */
    protected $engine;
    protected $api;
    protected $container;

    public function setUp() {
        // boostrap the api
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->container = new Icm_Container_Factory();
        $this->container->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        $search = new Icm_Search_Engine();
        $lexis = $this->container->get('lexisSearchAdapter');

        $search->addAdditiveAdapter(
            $lexis
        );

        $this->engine = $search;
    }

    public function testIdWithNameOnBpsSearch() {
        $query = new Icm_Search_Query(array(), 'personSearch');

        $query->addCriteria(Icm_Search_Query::CRITERIA_ID, '001212584341'); //some dude
        $query->addCriteria(Icm_Search_Query::CRITERIA_FIRSTNAME, 'Some');
        $query->addCriteria(Icm_Search_Query::CRITERIA_LASTNAME, 'Dude');

        $query->hasHash(
            array(
                'firstName' => strtoupper('Leslie'),
                'lastName' => strtoupper('YOUNG')
            ), Icm_Search_Query::FILTER_ACTION_EXCLUDE
        );


        $result = $this->engine->search($query);
        $this->assertGreaterThan(0, count($result));
    }

    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testReversePhone($phone) {
        $query = new Icm_Search_Query(array(
            Icm_Search_Engine::CRITERIA_PHONENUMBER => $phone
        ), 'reversePhone');

        $query->setAllowPaid(true);

        $query->hasA('street');

        $result = $this->engine->search($query);
        $this->assertTrue(count($result) > 0);
    }

    /**
     * @dataProvider nameProvider
     * @param $fname
     * @param $lname
     * @param $state
     */
    public function testBackgroundTeaser($fname, $lname, $state) {
        $query = new Icm_Search_Query(
            array(
                Icm_Search_Query::CRITERIA_FIRSTNAME => $fname,
                Icm_Search_Query::CRITERIA_LASTNAME => $lname,
                Icm_Search_Query::CRITERIA_STATE => $state
            ), 'backgroundCheckTeaser'
        );

        $result = $this->engine->search($query);

        $this->assertTrue(count($result) > 0);
    }

    public function phoneProvider() {
        return array(
            array('8584052745'),
            array('4135637167'),
            array('8313595555'),
            array('6192971888'),
            array('6197024419'),
            array('9782497413'),
            array('9782499834'),
            array('5083643339'),
            array('8584562111'),
            array('6192200268'),
            array('6192762800'),
            array('8582684933'),
        );
    }

    public function nameProvider() {
        return array(
            array('Chris', 'Guiney', 'CA'),
            array('Chris', 'Guiney', 'MA'),
            array('Kristian', 'Kibak', 'CA'),
            array('Nathan', 'Cobb', 'CA'),
            array('Joey', 'Rocco', 'CA'),
            array('Don', 'Son', 'CA')
        );
    }
}
