<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/16/12
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Data_Search_Test extends PHPUnit_Framework_TestCase
{
    public function testGetAdapters() {
        $search = new Icm_Search_Engine();
        $adapters = $search->getAdapters();
        $this->assertInternalType('array', $adapters);
    }

    public function testSearchOptout() {
        // bootstrap api
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        // search parameters
        $search['first_name'] = 'John';
        $search['last_name'] = 'Smith';
        $search['state'] = 'CA';

        // set up the npd results
        $npdResults = array(
            Icm_Entity_Person::create( array(
                'first_name'    => $search['first_name'],
                'last_name'     => $search['last_name'],
                'dob'           => '01/01/2000',
                'address'       => '123 Market Street',
                'city'          => 'San Diego',
                'state'         => $search['state'],
                'zip'           => 92131,
                'person_id'     => 9982273,
                'source'        => 'npd',
            )),
        );

        // set up the lexis results
        $lexisResults = array(
            Icm_Entity_Person::create( array(
                'first_name'    => $search['first_name'],
                'last_name'     => $search['last_name'],
                'dob'           => '01/01/2000',
                'address'       => '123 Market Street',
                'city'          => 'San Diego',
                'state'         => $search['state'],
                'zip'           => 92131,
                'person_id'     => 1923718927,
                'source'        => 'lexis',
            ))
        );

        // set up the optouts
        $optouts = array(
            Icm_Entity_Person_Optout::create( array(
                'external_person_id'    => 9982273,
                'source'                => 'npd',
            ))
        );

        // mock the npd search adapter
        $npdSearchAdapter = $this->getMock( 'Icm_Search_Adapter_Npd', array('backgroundCheck'), array(), '', false );
        $npdSearchAdapter->expects($this->once())
                         ->method('backgroundCheck')
                         ->will($this->returnValue($npdResults));

        // mock the lexis search adapter
        $lexisSearchAdapter = $this->getMock( 'Icm_Search_Adapter_Lexis', array('backgroundCheck'), array(), '', false );
        $lexisSearchAdapter->expects($this->once())
                           ->method('backgroundCheck')
                           ->will($this->returnValue($lexisResults));

        // mock optout search adapter
        $optoutSearchAdapter = $this->getMock( 'Icm_Search_Adapter_Internal', array('backgroundCheck'), array(), '', false );
        $optoutSearchAdapter->expects($this->once())
                            ->method('backgroundCheck')
                            ->will($this->returnValue($optouts));

        // set up the search
        $searchAdapter = new Icm_Search_Engine();
        $searchAdapter->addAdditiveAdapter($npdSearchAdapter);
        $searchAdapter->addAdditiveAdapter($lexisSearchAdapter);
        $searchAdapter->addSubtractiveAdapter($optoutSearchAdapter);

        // set up the criteria and query
        $criteria = array();
        $criteria[Icm_Search_Engine::CRITERIA_FIRSTNAME] = $search['first_name'];
        $criteria[Icm_Search_Engine::CRITERIA_LASTNAME]  = $search['last_name'];
        $criteria[Icm_Search_Engine::CRITERIA_STATE]     = $search['state'];

        $query = new Icm_Search_Query($criteria, 'backgroundCheck');

        // search
        $results = $searchAdapter->search($query);

        // get the actual search results
        $searchResults = $results->getData();

        // make sure that we have the right number of results
        $this->assertCount(1, $searchResults);
    }
}
