<?php

class Icm_Data_Lexis_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testWirelessReversePhoneLookup($phone) {
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $lexis = new Icm_Service_Lexis_Rest($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->directoryAssistanceWirelessSearch(array('PhoneNumber' => $phone));

        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testLandReversePhoneLookup($phone) {
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $lexis = new Icm_Service_Lexis_Rest($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->directoryAssistanceReverseSearch(array('Phone10' => $phone));

        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @expectedException Icm_Service_Lexis_Exception
     */
    public function testNoParametersLookup() {
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $lexis = new Icm_Service_Lexis_Rest($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->directoryAssistanceReverseSearch();
    }

    public function testWrongParametersLookup() {
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $lexis = new Icm_Service_Lexis_Rest($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->directoryAssistanceReverseSearch(array('Phone10' => 'abcdefghi'));

        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @dataProvider nameProvider
     * @param $first_name
     * @param $last_name
     * @param $state
     */
    public function testThinTeaserBackground($first_name, $last_name, $state) {
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexisTeaser');
        $lexis = new Icm_Service_Lexis_Teaser($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->thinRollupPersonSearch(
            array(
                'Name' => array(
                    'First' => $first_name,
                    'Last' => $last_name,
                ),
                'Address' => array(
                    'State' => $state
                )
            )
        );

        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    public function testPersonSearch(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexisTeaser');
        $lexis = new Icm_Service_Lexis_Teaser($config->getOption('username'), $config->getOption('password'));

        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $lexis = new Icm_Service_Lexis_Rest($config->getOption('username'), $config->getOption('password'));
        $lexis->setConfig($config);

        $result = $lexis->personSearch(array('UniqueId'=>'01961710053'), array());
        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    public function phoneProvider() {
        return array(
            array('8584052745'),
            array('4135637167'),
            array('8313595555'),
            array('6192971888'),
            array('6197024419'),
            array('9782497413'),
            array('9782499834'),
            array('5083643339'),
            array('8584562111'),
            array('6192200268'),
            array('6192762800'),
            array('8582684933'),
        );
    }

    public function nameProvider() {
        return array(
            array('Chris', 'Guiney', 'CA'),
            array('Christopher', 'Guiney', 'MA'),
            array('Kristian', 'Kibak', 'CA'),
            array('Nathan', 'Cobb', 'CA'),
            array('Joey', 'Rocco', 'CA'),
            array('Don', 'Son', 'CA'),
        );
    }
}
