<?php

class Icm_Auth_Resources_StorageAdapter_Test extends PHPUnit_Framework_TestCase
{
    protected $userResources;
    protected $classResources;
    protected $methodResources;

    public function setUp() {
        $adminDbConfig = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'adminDb');
        $this->adminDb = Icm_Db_Pdo::connect('adminDb', $adminDbConfig);
        $this->storage = new Icm_Auth_Resources_StorageAdapter('user_resources', 'id', $this->adminDb);

        $this->userResources = array();
        $this->classResources = array();
        $this->methodResources = array();

        parent::setUp();
    }

    public function testGetParentClassForMethod() {
        // setup default values
        $methodName = 'testingmethod';
        $resourceId = 1337;

        // first clean out the db
        $this->deleteMethodResource($methodName);
        $this->deleteClassResource($resourceId);
        $this->deleteUserResource($resourceId);

        // create a new user resource
        $this->createUserResource($resourceId);

        // create a new class resource
        $className = 'testingclass';
        $classPath = 'application/controllers/testing';
        $this->createClassResource($resourceId, $resourceId, $className, $classPath);

        // create a new method resource
        $this->createMethodResource($methodName, $resourceId);

        // pull the parent class for the method resource
        $results = $this->storage->getParentClassForMethod($methodName, $resourceId);

        // should have returned non-null results
        $this->assertNotNull($results);
        $this->assertCount(1, $results);

        // results should match the data we wrote to the db
        $parentClass = array_pop($results);
        $this->assertEquals($resourceId, $parentClass['id']);
        $this->assertEquals($className, $parentClass['class']);
        $this->assertEquals($classPath, $parentClass['class_path']);

        // delete the created resources
        $this->cleanupResources();
    }

    public function testGetDataById() {
        // first clean out the db
        $resourceId = 1337;
        $this->deleteUserResource($resourceId);

        // create a new user resource
        $resourceName = 'testuserresource';
        $resourceScope = 'testing';
        $this->createUserResource($resourceId);

        // fetch the user resource by id
        $results = $this->storage->getDataById($resourceId);

        // should have non-null results
        $this->assertNotNull($results);

        // results should match what we wrote to the db
        $this->assertEquals($resourceId, $results['id']);
        $this->assertEquals($resourceName, $results['name']);
        $this->assertEquals($resourceScope, $results['scope']);

        // cleanup
        $this->cleanupResources();
   }

    public function testReturnResourcesByName() {
        // try calling method with empty array
        $results = $this->storage->returnResourcesByName(array());

        // should return an empty array
        $this->assertNotNull($results);
        $this->assertTrue(is_array($results));
        $this->assertEmpty($results);

        // delete any previous test resources from the db
        $resourceId = 1337;
        $methodResourceId = 1338;
        $actionResourceId = 1339;
        $methodName = 'testingmethod';
        $this->deleteMethodResource($methodName);
        $this->deleteClassResource($methodResourceId);
        $this->deleteClassResource($resourceId);
        $this->deleteUserResource($resourceId);
        $this->deleteUserResource($methodResourceId);
        $this->deleteUserResource($actionResourceId);

        // create a user resource with class scope
        $resources = array();
        $className = 'testclassforuserresource';
        $classPath = 'application/controllers/testclassforuserresource';
        $this->createUserResource($resourceId, 'testuserresource', 'class');
        $this->createClassResource($resourceId, $resourceId, $className, $classPath);
        $resources[] = array('ignoredkey' => array('resource' => $resourceId));

        // create a user resource with method scope
        $methodResourceName = 'testmethodresource';
        $methodClassName = 'testmethodclassname';
        $methodClassPath = 'application/controllers/testmethodclasspath';
        $this->createUserResource($methodResourceId, $methodResourceName, 'method');
        $this->createClassResource($methodResourceId, $methodResourceId, $methodClassName, $methodClassPath);
        $this->createMethodResource($methodName, $methodResourceId);
        $resources[] = array('ignoredkey2' => array('resource' => $methodResourceId));

        // create a user resource with action scope
        $actionResourceName = 'testactionresource';
        $this->createUserResource($actionResourceId, $actionResourceName, 'action');
        $resources[] = array('ignoredkey3' => array('resource' => $actionResourceId));

        // pull the resources by id
        $results = $this->storage->returnResourcesByName($resources);

        // should have returned all three
        $this->assertCount(3, $results);

        // each should match the data we wrote in the proper scope
        $expected = array(
            0 => array(
                0 => array(
                    'id' => $resourceId,
                    'class' => $className,
                    'class_path' => $classPath,
                ),
            ),
            1 => array(
                0 => array(
                    'id' => (string) $methodResourceId,
                    'resources_id' => (string) $methodResourceId,
                    'class' => $methodClassName,
                    'class_path' => $methodClassPath,
                    'method' => $methodName,
                ),
            ),
            2 => array(
                0 => array(
                    'name' => $actionResourceName,
                ),
            ),
        );

        $this->assertEquals($expected, $results);
    }

    public function testSaveMethodData() {
        // delete any lingering data
        $this->deleteMethodResource('testingclassmethod');
        $this->deleteMethodResource('testingclassmethods');
        $this->adminDb->execute("DELETE FROM class_resources WHERE class = 'testingclass'");

        // draw up data for two method resources
        $methodData = array(
            'scope' => 'method',
            'name' => 'testingmethodresource',
            'methods' => array(
                'testingclass-testingclassmethod-testingclasspath',
                'testingclass-testingclassmethods-testingclasspath',
            ),
        );

        // save the data
        $returnValue = $this->storage->saveData($methodData);
        $this->assertEquals('Data was submitted successfully', $returnValue);

        // should have created one matching class resource
        $class = $this->adminDb->fetchAll("SELECT * FROM class_resources WHERE class = 'testingclass' and class_path = 'testingclasspath';");
        $this->assertNotEmpty($class);
        $this->assertCount(1, $class);
        $classId = $class[0]['id'];
        $this->classResources[] = $classId;

        // should have created two matching method resources
        $methods = $this->adminDb->fetchAll("SELECT * FROM method_resources WHERE method LIKE 'testingclassmethod%'");
        $this->assertCount(2, $methods);
        $this->methodResources[] = $methods[0]['id'];
        $this->methodResources[] = $methods[1]['id'];

        // cleanup the data
        $this->cleanupResources();
    }

    public function testSaveClassData() {
        // delete any lingering data
        $this->adminDb->execute("DELETE FROM class_resources WHERE class LIKE 'testingclass%'");

        // draw up data for two class resources
        $classData = array(
            'scope' => 'class',
            'name' => 'testingclassresource',
            'class' => array(
                'testingclass-testingclasspath',
                'testingclasses-testingclasspaths',
            ),
        );

        // save the data
        $returnValue = $this->storage->saveData($classData);
        $this->assertEquals('Data was submitted successfully', $returnValue);

        // should have created two matching class resources
        $classes = $this->adminDb->fetchAll("SELECT * FROM class_resources WHERE class LIKE 'testingclass%'");
        $this->assertCount(2, $classes);
        $this->classResources[] = $classes[0]['id'];
        $this->classResources[] = $classes[1]['id'];

        // cleanup the data
        $this->cleanupResources();
    }

    public function testSaveActionData() {
        // delete any lingering data
        $this->adminDb->execute("DELETE FROM user_resources WHERE name = 'testingactionresource'");

        // draw up data for two action resources
        $actionData = array(
            'name' => 'testingactionresource',
            'scope' => 'action',
        );

        // save the data
        $returnValue = $this->storage->saveData($actionData);
        $this->assertEquals('Data was submitted successfully', $returnValue);

        // should have created matching action resource
        $actionResource = $this->adminDb->fetchAll("SELECT * FROM user_resources WHERE name = 'testingactionresource'");
        $this->assertCount(1, $actionResource);
        $this->userResources[] = $actionResource[0]['id'];

        // cleanup the data
        $this->cleanupResources();
    }

    /**
     * Create a new user resource
     *
     * @param int $id
     * @param string $name
     * @param string $scope
     * @return int
     */
    protected function createUserResource($id = 1337, $name = 'testuserresource', $scope = 'testing') {
        $this->adminDb->execute("INSERT INTO user_resources (id, name, scope) VALUES ($id, '$name', '$scope');");
        $this->userResources[] = $id;
        return $id;
    }

    /**
     * Create a new class resource
     *
     * @param int $id
     * @param int $resourcesId
     * @param string $class
     * @param string $classPath
     * @return int
     */
    protected function createClassResource($id = 1337, $resourcesId = 1337, $class = 'testingclass', $classPath = 'application/controllers/testing') {
        $this->adminDb->execute("INSERT INTO class_resources (id, resources_id, class, class_path) VALUES ($id, $resourcesId, '$class', '$classPath')");
        $this->classResources[] = $id;
        return $id;
    }

    /**
     * Create a new method resource
     *
     * @param string $methodName
     * @param int $parentId
     * @return int
     */
    protected function createMethodResource($methodName = 'testingmethod', $parentId = 1337) {
        $this->adminDb->execute("INSERT INTO method_resources (method, parent_id) VALUES ('$methodName', $parentId)");
        $this->methodResources[] = $methodName;
    }

    /**
     * Delete a user resource
     *
     * @param int $id
     */
    protected function deleteUserResource($id) {
        $this->adminDb->execute("DELETE FROM user_resources WHERE id = $id");
    }

    /**
     * Delete a method resource
     *
     * @param string $name
     */
    protected function deleteMethodResource($name) {
        $this->adminDb->execute("DELETE FROM method_resources WHERE method = '$name'");
    }

    /**
     * Delete a class resource
     *
     * @param int $id
     */
    protected function deleteClassResource($id) {
        $this->adminDb->execute("DELETE FROM class_resources WHERE id = $id");
    }

    /**
     * Delete any created resources
     */
    protected function cleanupResources() {
        // delete any created method resources
        foreach ($this->methodResources as $methodName) {
            $this->deleteMethodResource($methodName);
        }

        // delete any created class resources
        foreach ($this->classResources as $resourceId) {
            $this->deleteClassResource($resourceId);
        }

        // delete any user resources
        foreach ($this->userResources as $resourceId) {
            $this->deleteUserResource($resourceId);
        }
    }
}