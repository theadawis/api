<?php

class Icm_Auth_Acl_Test extends PHPUnit_Framework_TestCase
{
    /**
     * Test the checking of permissions for a given role with session data
     */
    public function testCreatePermissionsFromSession() {
        // build the acl object
        $acl = new Icm_Auth_Acl();

        // build the sample session data array
        $sessionData = array(
            0 => array(
                0 => array(
                    'id' => 145,
                    'class' => 'container',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
                1 => array(
                    'id' => 146,
                    'class' => 'elasticsearch',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
                2 => array(
                    'id' => 147,
                    'class' => 'errors',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
            ),
            1 => array(
                0 => array(
                    'id' => 34,
                    'resources_id' => 34,
                    'class' => 'user',
                    'class_path' => 'application/controllers/',
                    'method' => 'index',
                ),
            ),
        );

        // should allow access to the global urls
        $acl->setClassName('user');
        $acl->setMethodName('login');
        $this->assertTrue($acl->createPermissionsFromSession('developer', $sessionData));

        // should allow access to the urls mentioned in the session data
        $acl = new Icm_Auth_Acl();
        $acl->setClassName('user');
        $acl->setMethodName('index');
        $this->assertTrue($acl->createPermissionsFromSession('test-dev', $sessionData));

        $acl = new Icm_Auth_Acl();
        $acl->setClassName('elasticsearch');
        $acl->setMethodName('testing');
        $this->assertTrue($acl->createPermissionsFromSession('sudo-dev', $sessionData));

        // should not allow access to the urls not in the session data
        $acl = new Icm_Auth_Acl();
        $acl->setClassName('fred');
        $acl->setMethodName('george');
        $this->assertFalse($acl->createPermissionsFromSession('developer', $sessionData));
    }

    /**
     * Test function that returns a boolean if the user can view a given url
     */
    public function testCanViewUrl() {
        // create the acl
        $acl = new Icm_Auth_Acl();

        // should not be allowed to view any url
        $this->assertFalse($acl->canViewUrl('developer', 'user/index'));

        // add some session resource data
        $sessionData = array(
            0 => array(
                0 => array(
                    'id' => 145,
                    'class' => 'container',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
                1 => array(
                    'id' => 146,
                    'class' => 'elasticsearch',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
                2 => array(
                    'id' => 147,
                    'class' => 'errors',
                    'class_path' => '/application/controllers/developer_tools/',
                ),
            ),
            1 => array(
                0 => array(
                    'id' => 34,
                    'resources_id' => 34,
                    'class' => 'user',
                    'class_path' => 'application/controllers/',
                    'method' => 'index',
                ),
            ),
        );

        // should be allowed to see global urls
        $acl->setClassName('errors');
        $acl->setMethodName('testing');
        $acl->createPermissionsFromSession('developer', $sessionData);
        $this->assertTrue($acl->canViewUrl('developer', 'user/login'));

        // should be allowed to see urls in session data
        $this->assertTrue($acl->canViewUrl('developer', 'user/index'));
        $this->assertTrue($acl->canViewUrl('developer', 'elasticsearch/testing'));

        // should not be allowed to see urls not in session data
        $this->assertFalse($acl->canViewUrl('developer', 'fred/george'));

        // should default to the acl's url if none given
        $this->assertTrue($acl->canViewUrl('developer'));
    }

    /**
     * Check that resources get setup correctly for roles
     */
    public function testCreatePermissions() {
        // setup new acl
        $acl = new Icm_Auth_Acl();

        // add a mock user roles adapter with parent roles
        $role = 'testdev';
        $parentRoles = array(
            array('parentId' => 'parentonetestdev'),
            array('parentId' => 'parenttwotestdev'),
        );
        $userRolesAdapter = $this->getMock('Icm_Auth_Roles_StorageAdapter', array(), array(), '', '', false);
        $userRolesAdapter->expects($this->once())->method('returnParentRoles')->with($role)->will($this->returnValue($parentRoles));
        $acl->setUserRolesAdapter($userRolesAdapter);

        // add a mock user permissions adapter with permissions for child and parent roles
        $testDevPermissions = array(
            'viewtestdev',
            'viewtesterdev',
            'viewothertestdev',
        );
        $parentOnePermissions = 'viewparentone';
        $parentTwoPermissions = 'viewparenttwo';
        $userPermissionsAdapter = $this->getMock('Icm_Auth_Permission_StorageAdapter', array(), array(), '', '', false);
        $userPermissionsAdapter->expects($this->exactly(3))->method('getPermissions')->will($this->onConsecutiveCalls($testDevPermissions, $parentOnePermissions, $parentTwoPermissions));
        $acl->setUserPermissionAdapter($userPermissionsAdapter);

        // add mock user resources adapter with resource names
        $resourceNames = array(
            'viewtestdev',
            'viewtesterdev',
            'viewothertestdev',
            'viewparentone',
            'viewparenttwo',
        );
        $userResourcesAdapter = $this->getMock('Icm_Auth_Resources_StorageAdapter', array(), array(), '', '', false);
        $userResourcesAdapter->expects($this->once())->method('returnResourcesByName')->will($this->returnValue($resourceNames));
        $acl->setUserResourcesAdapter($userResourcesAdapter);

        // call create permissions for an existing role
        $permissions = $acl->createPermissions($role);

        // should return the correct permissions
        $this->assertEquals($resourceNames, $permissions);
    }
}
