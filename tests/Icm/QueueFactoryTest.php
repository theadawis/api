<?php

class Icm_QueueFactory_Test extends PHPUnit_Framework_TestCase {
    /**
     * Test construction of Zend_Queue for email validate queue
     */
    public function testGetEmailValidateQueue() {
        $queue_params = array(
            'exchange_name' => 'email',
            'name' => 'icm_email_validate_queue',
            'routing_key' => 'icm_email_validate',
        );
        $this->assertCanGetQueue('getEmailValidateQueue', $queue_params);
    }

    /**
     * Test sending messages to the email validate queue
     */
    public function testEmailValidateQueueSend() {
        $config = Icm_Config::fromIni('../config/development/defaults.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getEmailValidateQueue();
        $queue_name = $queue->getName();

        $postData = array(
            'customer_id'   => 1,
            'email_address' => 'testuser@thecontrolgroup.com',
            'do_send_queue' => true,
            'skip_send'     => true,
            'do_partners_queue' => true,
            'meta'          => array(
                'email_type'         => 'lead',
                'first_name'         => 'kawika',
                'last_name'          => 'ohumukini',
                'search_result_link' => 'search-result-link',
                'email_address'      => 'kawika.ohumukini@thecontrolgroup.com',
                'report_name'        => 'report-name',
                'report_address'     => 'address-city-state'
            )
        );

        $result = $queue->send($postData);
        $this->assertTrue($result);
    }

    /**
     * Test construction of Zend_Queue for worker queue
     */
    public function testGetWorkerQueue() {
        $queue_params = array(
            'exchange_name' => 'worker',
            'name' => 'icm_work_queue',
            'routing_key' => 'icm_work',
        );
        $this->assertCanGetQueue('getWorkerQueue', $queue_params);
    }

    /**
     * Test sending messages to the worker queue
     */
    public function testWorkerQueueSendAndReceive() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getWorkerQueue();
        $this->assertQueueCanSendAndReceive($queue);
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Test construction of Zend_Queue for feedback queue
     */
    public function testGetFeedbackQueue() {
        $queue_params = array(
            'name' => 'icm_feedback_queue',
            'routing_key' => 'icm_feedback',
        );
        $this->assertCanGetQueue('getFeedbackQueue', $queue_params);
    }

    /**
     * Test sending messages to the feedback queue
     */
    public function testFeedbackQueueSend() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getFeedbackQueue();
        $this->assertQueueCanSend($queue);
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Test construction of Zend_Queue for create_click queue
     */
    public function testGetCreateClickQueue() {
        $queue_params = array(
            'exchange_name' => 'tracker',
            'name' => 'icm_create_click_queue',
            'routing_key' => 'icm_create_click',
        );

        $this->assertCanGetQueue('getCreateClickQueue', $queue_params);
    }

    /**
     * Test sending messages to the create click queue
     */
    public function testCreateClickQueueSend() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getCreateClickQueue();
        $this->assertQueueCanSend($queue);
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Test construction of Zend_Queue for update_click queue
     */
    public function testGetUpdateClickQueue() {
        $queue_params = array(
            'exchange_name' => 'tracker',
            'name' => 'icm_update_click_queue',
            'routing_key' => 'icm_update_click',
        );

        $this->assertCanGetQueue('getUpdateClickQueue', $queue_params);
    }

    /**
     * Test sending messages to the update click queue
     */
    public function testUpdateClickQueueSend() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getUpdateClickQueue();
        $this->assertQueueCanSend($queue);
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Test construction of Zend_Queue for postback queue
     */
    public function testGetPostbackQueue() {
        $queue_params = array(
            'exchange_name' => 'tracker',
            'name' => 'icm_postback_queue',
            'routing_key' => 'icm_postback',
        );

        $this->assertCanGetQueue('getPostbackQueue', $queue_params);
    }

    /**
     * Test sending messages to the postback queue
     */
    public function testPostbackQueueSend() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->getPostbackQueue();
        $this->assertQueueCanSend($queue);
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Assert can get the named queue with right params
     */
    protected function assertCanGetQueue($method_name, $params = array()) {
        // get a new queue
        $expected = array(
            'host' => 'queue01',
            'port' => 5672,
            'login' => 'guest',
            'password' => 'guest',
            'exchange_name' => 'offline',
            'vhost' => 'testing',
        );

        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $queue_factory = new Icm_QueueFactory($config);
        $queue = $queue_factory->$method_name();

        // should be an instance of Zend_Queue
        $this->assertTrue($queue instanceof Zend_Queue);

        // should use our custom adapter
        $this->assertTrue($queue->getAdapter() instanceof Icm_Queue_Adapter_Rabbitmq);

        // option values should match expected
        $expected = array_merge($expected, $params);
        $this->assertOptionsMatch($expected, $queue);
    }

    /**
     * Verify that the given queue can send messages
     */
    protected function assertQueueCanSend($queue) {
        // send a message
        $queue_name = $queue->getName();
        $test_message = "Test $queue_name Queue Message";
        $result = $queue->send($test_message);

        // should have sent the message to the right queue in rabbitmq
        $this->assertTrue($result);
    }

    /**
     * Verify that the given queue can send and receive messages
     */
    protected function assertQueueCanSendAndReceive($queue) {
        // send a message
        $queue_name = $queue->getName();
        $test_message = "Test $queue_name Queue Message";
        $queue->send($test_message);

        // should have sent the message to the right queue in rabbitmq
        $messages = $queue->receive(1);
        $this->assertEquals(1, count($messages));

        foreach ($messages as $index => $message) {
            $this->assertEquals($test_message, $message->body);
        }
    }

    /**
     * Verify that the options for the given queue match the expected values
     */
    protected function assertOptionsMatch($expected, $queue) {
        foreach ($expected as $key => $value) {
            // get the queue's option value
            $queue_value = $queue->getOption($key);

            // handle name separately
            if ($key == 'name') {
                $queue_value = $queue->getName();
            }

            // queue value and expected value should match
            $this->assertEquals($value, $queue_value);
        }
    }
}
