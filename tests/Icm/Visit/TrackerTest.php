<?php

class Icm_Visit_Tracker_Test extends PHPUnit_Framework_TestCase
{
    protected $tracker_config;
    protected $redis_config;
    protected $rabbitmq_config;
    protected $cgdb_config;
    protected $app_id;

    const ONE_WEEK = 604800;
    const TWO_WEEKS = 1209600;

    public function setUp() {
        // set server variables for testing
        $_SERVER['HTTP_HOST'] = 'localhost';
        $_SERVER['HTTP_USER_AGENT'] = 'Firefox';
        $_SERVER['REQUEST_URI'] = 'http://localhost/';

        parent::setUp();

        // get the tracker config
        $this->tracker_config = Icm_Config::fromIni('../config/development/defaults.ini', 'visitTracker');

        // get the redis config
        $this->redis_config = Icm_Config::fromIni('../config/development/defaults.ini', 'redis');

        // get the rabbitmq config
        $this->rabbitmq_config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');

        // get the cgdb config
        $this->cgdb_config = Icm_Config::fromIni('../config/development/defaults.ini', 'cgDb');

        // set the app id
        $this->app_id = 1;
    }

    public function tearDown() {
        // purge the queues
        $queueFactory = new Icm_QueueFactory($this->rabbitmq_config);
        $queue = $queueFactory->getCreateClickQueue();
        $queue->getAdapter()->purgeQueue();

        $queue = $queueFactory->getUpdateClickQueue();
        $queue->getAdapter()->purgeQueue();
    }

    /**
     * Test the creation of a basic new visit
     */
    public function testCreateVisitBasic() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with an empty link, no params and queue only
        $link = '';
        $params = array();
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => null,
            's1' => null,
            's2' => null,
            's3' => null,
            's4' => 'http://localhost/',
            's5' => 'non-member',
            'payout' => null,
            'affiliate_id' => 1,
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'is_converted' => false,
            'is_pixel_fired' => false,
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have also written to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test the use of incoming params in creating a new visit
     */
    public function testCreateVisitParams() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with an empty link, some params
        $link = '';
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have kept most of the params, but over-written s4 and s5
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => 'http://localhost/',
            's5' => 'non-member',
            'payout' => null,
            'affiliate_id' => 1,
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test whether the visit tracker overrides the s1 param if it's empty
     */
    public function testCreateVisitParamsNoS1() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with an empty link, some params
        $link = '';
        $params = array(
            'sid' => 'test_sid',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have kept most of the params, but over-written s4 and s5
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => null,
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => 'http://localhost/',
            's5' => 'non-member',
            'payout' => null,
            'affiliate_id' => 1,
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test the creation of a new visit with no link and test src param
     */
    public function testCreateVisitNoLinkTestSrc() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with an empty link, some params
        $link = '';
        $params = array(
            'src' => 'test_src',
            'cmp' => 'test_cmp',
            'cnt' => 'test_cnt',
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have successfully created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have kept most of the params
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['src'],
            's1' => $params['cmp'],
            's2' => $params['cnt'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => null,
            'affiliate_id' => 45,
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test creating a new visit with no link and a real src param
     */
    public function testCreateVisitNoLinkRealSrc() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // set the src param info in redis
        $redis = new Icm_Redis($this->redis_config);
        $src_key = $this->tracker_config->getOption('affiliateMapPrefix') . $this->app_id . '_' . 'test_src';
        $src_params = array(
            'subid' => 'test_real_src',
            'affiliate_id' => 'test_real_source_affiliate_id',
        );
        $redis->set($src_key, json_encode($src_params));

        // create a new visit with an empty link, real src params
        $link = '';
        $params = array(
            'src' => 'test_src',
            'cmp' => 'test_cmp',
            'cnt' => 'test_cnt',
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $src_params['subid'],
            's1' => $params['cmp'],
            's2' => $params['cnt'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => null,
            'affiliate_id' => $src_params['affiliate_id'],
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // remove the test src info
        $redis->del($src_key);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test creating a new visit with no link and a real src param missing subid
     */
    public function testCreateVisitNoLinkRealSrcNoSubId() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // set the src param info in redis
        $redis = new Icm_Redis($this->redis_config);
        $src_key = $this->tracker_config->getOption('affiliateMapPrefix') . $this->app_id . '_' . 'test_src';
        $src_params = array(
            'affiliate_id' => 'test_real_source_affiliate_id',
        );
        $redis->set($src_key, json_encode($src_params));

        // create a new visit with an empty link, real src params
        $link = '';
        $params = array(
            'src' => 'test_src',
            'cmp' => 'test_cmp',
            'cnt' => 'test_cnt',
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should create the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['cmp'],
            's1' => $params['cnt'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => null,
            'affiliate_id' => $src_params['affiliate_id'],
            'campaign_id' => 1,
            'affiliate_link_id' => null,
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // remove the test src info
        $redis->del($src_key);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test creating a new visit using a link id
     */
    public function testCreateVisitLink() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with a link, some params
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => $link['payout'],
            'affiliate_id' => $link['affiliate_id'],
            'campaign_id' => $link['campaign_id'],
            'affiliate_link_id' => $link['id'],
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have written everything to redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test creating a new visit with a link and caching it in redis
     */
    public function testCreateVisitLinkCache() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with a link, some params
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => $link['payout'],
            'affiliate_id' => $link['affiliate_id'],
            'campaign_id' => $link['campaign_id'],
            'affiliate_link_id' => $link['id'],
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have cached data in redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test creating new visits when visit id is invalid
     */
    public function testCreateVisitNesting() {
        // setup the visit and visitor cookies
        $visitorId = 'testvisitorid';
        $visitId = 'completelyinvalidtestvisitid';
        $_COOKIE['visitor'] = $visitorId;
        $_COOKIE['visit'] = $visitId;

        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with link data and params
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have written data to rabbitmq
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should only have written one time to the queue
        $second_visit = $this->getQueueMessage('CreateClick');
        $this->assertNull($second_visit);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => $link['payout'],
            'affiliate_id' => $link['affiliate_id'],
            'campaign_id' => $link['campaign_id'],
            'affiliate_link_id' => $link['id'],
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have cached data in redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }

    /**
     * Test the update visit method
     */
    public function testUpdateVisit() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with a link, some params
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => $link['payout'],
            'affiliate_id' => $link['affiliate_id'],
            'campaign_id' => $link['campaign_id'],
            'affiliate_link_id' => $link['id'],
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have cached data in redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // change the visit data
        $updates = array(
            'sub_id' => 'update_sid',
            's1' => 'update_s1',
            's2' => 'update_s2',
            's3' => 'update_s3',
            's4' => 'update_s4',
            's5' => 'update_s5',
            'payout' => 'updated_payout',
            'affiliate_id' => 'updated_affiliate_id',
            'campaign_id' => 'updated_campaign_id',
            'affiliate_link_id' => 'updated_affiliate_link_id',
            'is_duplicate' => true,
        );

        // hesitate
        sleep(5);

        // update the visit
        $tracker->updateVisit($updates);

        // should have written into the queue
        $visit_data = $this->getQueueMessage('UpdateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $updatedExpected = array_merge($cached_data, $updates);
        $this->assertVisitDataMatches($updatedExpected, $visit_data);

        // should have updated the updated value in the queue
        $created = strtotime($visit_data['created']);
        $updated = strtotime($visit_data['updated']);
        $this->assertTrue($updated > $created);

        // should not have cached updated data in redis
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match original data
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // delete the visit
        $redis->del($visit_key);
    }


    /**
     * Test to make sure we check every visit of a visitor to determine if they've converted.
     */
    public function testHasConvertedVisit() {
        $db = Icm_Db_Pdo::connect('cgDb', $this->cgdb_config);

        // Make sure there aren't any stale entries
        $redis = new Icm_Redis($this->redis_config);
        $redis->del('tracker_visit_fe4b1cbcde4995c8c47da63ee34347a7a9237833');
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');

        // Ensure the tracker can determine a previous conversion from the database
        $db->execute("INSERT INTO `visitors` (`visitor_id`, `customer_id`, `created`, `updated`)
                      VALUES
                    ('fe4b1cbcde4995c8c47da63ee34347a7a9237833', null, '2013-07-06 21:31:40', '2013-07-19 04:59:38');
        ");
        $db->execute("
                      INSERT INTO `visitor_visits`
                      (`visit_id`, `app_id`, `visitor_id`, `is_duplicate`, `affiliate_link_id`, `affiliate_id`,
                      `campaign_id`, `sub_id`, `s1`, `s2`, `s3`, `s4`, `s5`, `params`, `is_converted`, `is_pixel_fired`,
                      `payout`, `ip_address`, `useragent`, `converted`, `referer`, `created`, `updated`)
                      VALUES
                     ('0c292a07fc48ac8a65d941d298b0b48151d1fb9a', 1, 'fe4b1cbcde4995c8c47da63ee34347a7a9237833', 0,
                     102, 79, 1, '728', 'reportisonline', NULL, NULL, NULL, NULL,
                     '{\"lid\":\"56hj2a\",\"sid\":\"728\",\"s1\":\"reportisonline\",\"src\":\"MGSHT\",\"mdm\":\"display\"}',
                     1, 1, 30, '68.5.217.138',
                     'Mozilla/5.0 (Linux; U; Android 4.1.2; en-us; GT-N8013 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
                     '2013-07-19 04:59:40', 'http://mugshots.com/Celebrity/Shenellica-Bettencourt.8610683.html',
                     '2013-07-19 04:51:00', '2013-07-19 04:59:40');
        ");

        $_COOKIE['visitor'] = 'fe4b1cbcde4995c8c47da63ee34347a7a9237833';

        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config,
                                         $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        $this->assertTrue($tracker->hasConvertedVisit(), "No converted visit found from database");

        //Try to clean up after ourselves
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');

        unset($_COOKIE['visitor']);

        // create a new visit with a link, some params
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $create_result = $tracker->createVisit($link, $params);

        // should have created the visit
        $this->assertTrue($create_result);

        // change the visit data
        $updates = array(
            'is_converted' => true,
         );

        $tracker->updateVisit($updates);

        //This tracker will look at its internal visitor data array for is_converted, and find it's true
        $this->assertTrue($tracker->hasConvertedVisit(), "Visit data not marked is_converted");

        // delete the visit
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $redis->del($visit_key);
    }

    /**
     * Test the recording of duplicate visits
     */
    public function testVisitDuplicates() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create a new visit with an empty link, some params and queue only
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $create_result = $tracker->createVisit($link, $params, $queue_only);

        // should return the opposite of the queue only param
        $this->assertTrue($create_result);

        // should have written to the rabbitmq queue
        $visit_data = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($visit_data);

        // should have written the correct visit data
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => $params['sid'],
            's1' => $params['s1'],
            's2' => $params['s2'],
            's3' => $params['s3'],
            's4' => $params['s4'],
            's5' => $params['s5'],
            'payout' => $link['payout'],
            'affiliate_id' => $link['affiliate_id'],
            'campaign_id' => $link['campaign_id'],
            'affiliate_link_id' => $link['id'],
            'params' => json_encode($params),
        );
        $this->assertVisitDataMatches($expected, $visit_data);

        // should have cached data in redis
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $this->assertTrue($redis->exists($visit_key));

        // cached data should match data written to queue
        $cached_data = $redis->get($visit_key);
        $cached_data = json_decode($cached_data, true);
        $this->assertVisitDataMatches($expected, $cached_data);

        // update visit as converted
        $updates = array('is_converted' => true);
        $cached_data['is_converted'] = true;
        $tracker->updateVisit($updates);

        // set our cookies properly
        $firstVisitId = $tracker->getVisitId();
        $firstVisitorId = $tracker->getVisitorId();
        $_COOKIE['visitor'] = $firstVisitorId;

        // wait a bit
        sleep(5);

        // create a second visit
        $tracker->createVisit(null, array());

        // should have marked itself as duplicate
        $this->assertTrue($tracker->isDuplicate());

        // should have written it to the queue
        $secondVisitData = $this->getQueueMessage('CreateClick');
        $this->assertNotNull($secondVisitData);

        // queue data should be marked as duplicate
        $this->assertArrayHasKey('is_duplicate', $secondVisitData);
        $this->assertTrue($secondVisitData['is_duplicate']);

        // queue data should not be marked converted
        $this->assertFalse(isset($secondVisitData['is_converted']) && $secondVisitData['is_converted'],
                           print_r($secondVisitData, true));

        // queue data should match second visit params
        $expected = array(
            'app_id' => $this->app_id,
            'sub_id' => null,
            's1' => null,
            's2' => null,
            's3' => null,
            's4' => 'http://localhost/',
            's5' => 'non-member',
            'payout' => null,
            'affiliate_id' => 1,
            'campaign_id' => 1,
            'affiliate_link_id' => null,
        );
        $this->assertVisitDataMatches($expected, $secondVisitData);

        // should not have changed cookie values
        $this->assertEquals($firstVisitorId, $_COOKIE['visitor']);

        // tracker should have new visit id
        $this->assertNotEquals($firstVisitId, $tracker->getVisitId());

        // redis data should match initial values
        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $newCachedData = $redis->get($visit_key);
        $newCachedData = json_decode($newCachedData, true);
        $this->assertEquals($firstVisitId, $newCachedData['visit_id']);
        $this->assertVisitDataMatches($cached_data, $newCachedData);
    }

    /**
     * Test the method that determines whether a visit is old or not
     */
    public function testIsOldVisit() {
        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // create some visit data
        $visitData = array(
            'visit_id' => 'testvisit',
            'visitor_id' => 'testvisitor',
            'app_id' => 1,
        );

        // set the created date to now
        $visitData['created'] = date('Y-m-d H:i:s');
        $visitData['updated'] = date('Y-m-d H:i:s');

        // should not be considered old
        $isOld = $tracker->isOldVisit($visitData);
        $this->assertFalse($isOld);

        // set the created date to be 31 days in the past
        $thirtyOneDays = 60 * 60 * 24 * 31;
        $pastVisit = time() - $thirtyOneDays;
        $visitData['created'] = date('Y-m-d H:i:s', $pastVisit);
        $visitData['updated'] = date('Y-m-d H:i:s', $pastVisit);

        // should be treated as old visit
        $isOld = $tracker->isOldVisit($visitData);
        $this->assertTrue($isOld);
    }

    /**
     * Test getVisitData method
     */
    public function testGetVisitData() {
        // create new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // should get back empty array
        $savedData = $tracker->getVisitData();
        $this->assertTrue(is_array($savedData));
        $this->assertEmpty($savedData);
    }

    /**
     * Test the creation of new visitor ids
     */
    public function testCreateVisitorId() {
        // set the visitor id cookie
        $_COOKIE['visitor'] = 'testingvisitorid';

        // create new tracker and visit
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);
        $tracker->createVisit(null, array());

        // should have a visitor id that matches the cookie setting
        $visitData = $tracker->getVisitData();
        $this->assertEquals('testingvisitorid', $visitData['visitor_id']);

        // set cookie value to null
        $_COOKIE['visitor'] = null;

        // create new tracker and visit
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);
        $tracker->createVisit(null, array());

        // should have made up random new visitor id
        $visitData = $tracker->getVisitData();
        $this->assertNotNull($visitData['visitor_id']);
        $this->assertTrue(strlen($visitData['visitor_id']) > 0);

        // set cookie value to empty string
        $_COOKIE['visitor'] = '';

        // create new tracker and visit
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);
        $tracker->createVisit(null, array());

        // should have made up random new visitor id
        $visitData = $tracker->getVisitData();
        $this->assertNotNull($visitData['visitor_id']);
        $this->assertTrue(strlen($visitData['visitor_id']) > 0);

        // unset cookie value completely
        unset($_COOKIE['visitor']);

        // create new tracker and visit
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);
        $tracker->createVisit(null, array());

        // should have made up random new visitor id
        $visitData = $tracker->getVisitData();
        $this->assertNotNull($visitData['visitor_id']);
        $this->assertTrue(strlen($visitData['visitor_id']) > 0);
    }

    /**
     * Test getVisitorId
     */
    public function testGetVisitorId() {
        // set the visitor id cookie
        $_COOKIE['visitor'] = 'testingvisitorid';

        // create new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // should have a visitor id that matches the cookie setting
        $this->assertEquals('testingvisitorid', $tracker->getVisitorId());

        // set cookie value to null
        $_COOKIE['visitor'] = null;

        // create new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // should have made up random new visitor id
        $this->assertNotNull($tracker->getVisitorId());
        $this->assertTrue(strlen($tracker->getVisitorId()) > 0);

        // set cookie value to empty string
        $_COOKIE['visitor'] = '';

        // create new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // should have made up random new visitor id
        $this->assertNotNull($tracker->getVisitorId());
        $this->assertTrue(strlen($tracker->getVisitorId()) > 0);

        // unset cookie value completely
        unset($_COOKIE['visitor']);

        // create new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config, $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // should have made up random new visitor id
        $this->assertNotNull($tracker->getVisitorId());
        $this->assertTrue(strlen($tracker->getVisitorId()) > 0);
    }

    public function testUpdateVisitNullVisitId() {
        $db = Icm_Db_Pdo::connect('cgDb', $this->cgdb_config);

        // Make sure there aren't any stale entries
        $redis = new Icm_Redis($this->redis_config);
        $redis->del('tracker_visit_fe4b1cbcde4995c8c47da63ee34347a7a9237833');
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');

        // write new visitor and visit to the db
        $visitorCreated = date('Y-m-d H:i:s', time() - self::TWO_WEEKS);
        $visitorUpdated = date('Y-m-d H:i:s', time() - self::ONE_WEEK);
        $db->execute("INSERT INTO `visitors` (`visitor_id`, `customer_id`, `created`, `updated`)
                      VALUES
                    ('fe4b1cbcde4995c8c47da63ee34347a7a9237833', null, ?, ?);
        ", array($visitorCreated, $visitorUpdated));

        $visitCreated = date('Y-m-d H:i:s', time() - self::ONE_WEEK - 10);
        $visitUpdated = date('Y-m-d H:i:s', time() - self::ONE_WEEK);
        $db->execute("
                      INSERT INTO `visitor_visits`
                      (`visit_id`, `app_id`, `visitor_id`, `is_duplicate`, `affiliate_link_id`, `affiliate_id`,
                      `campaign_id`, `sub_id`, `s1`, `s2`, `s3`, `s4`, `s5`, `params`, `is_converted`, `is_pixel_fired`,
                      `payout`, `ip_address`, `useragent`, `converted`, `referer`, `created`, `updated`)
                      VALUES
                     ('0c292a07fc48ac8a65d941d298b0b48151d1fb9a', 1, 'fe4b1cbcde4995c8c47da63ee34347a7a9237833', 0,
                     102, 79, 1, '728', 'reportisonline', NULL, NULL, NULL, NULL,
                     '{\"lid\":\"56hj2a\",\"sid\":\"728\",\"s1\":\"reportisonline\",\"src\":\"MGSHT\",\"mdm\":\"display\"}',
                     1, 1, 30, '68.5.217.138',
                     'Mozilla/5.0 (Linux; U; Android 4.1.2; en-us; GT-N8013 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
                     ?, 'http://mugshots.com/Celebrity/Shenellica-Bettencourt.8610683.html',
                     ?, ?);
        ", array($visitUpdated, $visitCreated, $visitUpdated));

        // set the visitor cookie
        $_COOKIE['visitor'] = 'fe4b1cbcde4995c8c47da63ee34347a7a9237833';

        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config,
                                         $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // call update visit with updates
        $updates = array(
            'is_converted' => true,
            'is_pixel_fired' => true,
            'converted' => date('Y-m-d H:i:s'),
        );
        $tracker->updateVisit($updates);

        // should have pushed data into queue
        $updatedVisitData = $this->getQueueMessage('UpdateClick');
        $this->assertNotNull($updatedVisitData);

        // queued visit id should match the old one
        $this->assertNotNull($updatedVisitData['visit_id']);
        $this->assertEquals('0c292a07fc48ac8a65d941d298b0b48151d1fb9a', $updatedVisitData['visit_id']);

        // queued data should match the updates
        $this->assertTrue($updatedVisitData['is_converted']);
        $this->assertTrue($updatedVisitData['is_pixel_fired']);

        // cleanup
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');

        $visit_key = $this->tracker_config->getOption('visitPrefix') . $tracker->getVisitorId();
        $redis = new Icm_Redis($this->redis_config);
        $redis->del($visit_key);
    }

    /**
     * Confirm that is_duplicate gets written as boolean to queue
     */
    public function testIsDuplicateStringValues() {
        $db = Icm_Db_Pdo::connect('cgDb', $this->cgdb_config);
        $redis = new Icm_Redis($this->redis_config);

        // Make sure there aren't any stale entries
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');
        $redis->del('tracker_visit_fe4b1cbcde4995c8c47da63ee34347a7a9237833');

        // write new visitor and visit to the db with string values for is_duplicate
        $visitorCreated = date('Y-m-d H:i:s', time() - self::TWO_WEEKS);
        $visitorUpdated = date('Y-m-d H:i:s', time() - self::ONE_WEEK);

        $db->execute("INSERT INTO `visitors` (`visitor_id`, `customer_id`, `created`, `updated`)
                      VALUES
                    ('fe4b1cbcde4995c8c47da63ee34347a7a9237833', null, ?, ?);
        ", array($visitorCreated, $visitorUpdated));

        $visitCreated = date('Y-m-d H:i:s', time() - self::ONE_WEEK - 10);
        $visitUpdated = date('Y-m-d H:i:s', time() - self::ONE_WEEK);
        $db->execute("
                      INSERT INTO `visitor_visits`
                      (`visit_id`, `app_id`, `visitor_id`, `is_duplicate`, `affiliate_link_id`, `affiliate_id`,
                      `campaign_id`, `sub_id`, `s1`, `s2`, `s3`, `s4`, `s5`, `params`, `is_converted`, `is_pixel_fired`,
                      `payout`, `ip_address`, `useragent`, `converted`, `referer`, `created`, `updated`)
                      VALUES
                     ('0c292a07fc48ac8a65d941d298b0b48151d1fb9a', 1, 'fe4b1cbcde4995c8c47da63ee34347a7a9237833', '0',
                     102, 79, 1, '728', 'reportisonline', NULL, NULL, NULL, NULL,
                     '{\"lid\":\"56hj2a\",\"sid\":\"728\",\"s1\":\"reportisonline\",\"src\":\"MGSHT\",\"mdm\":\"display\"}',
                     1, 1, 30, '68.5.217.138',
                     'Mozilla/5.0 (Linux; U; Android 4.1.2; en-us; GT-N8013 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
                     ?, 'http://mugshots.com/Celebrity/Shenellica-Bettencourt.8610683.html',
                     ?, ?);
        ", array($visitUpdated, $visitCreated, $visitUpdated));

        // set the visitor cookie
        $_COOKIE['visitor'] = 'fe4b1cbcde4995c8c47da63ee34347a7a9237833';

        // create a new tracker
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config,
                                         $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        // update the visit
        $updates = array(
            'is_converted' => true,
            'is_pixel_fired' => true,
            'converted' => date('Y-m-d H:i:s'),
        );
        $tracker->updateVisit($updates);

        // should have written updated data to queue
        $updatedVisitData = $this->getQueueMessage('UpdateClick');
        $this->assertNotNull($updatedVisitData);

        // should have written boolean (or int) value for is_duplicate
        $this->assertInternalType('bool', $updatedVisitData['is_duplicate']);
        $this->assertFalse($updatedVisitData['is_duplicate']);

        // create new visit in redis with string values for is_duplicate
        $newVisitData = array(
            'visitor_id' => $_COOKIE['visitor'],
            'visit_id' => '0c292a07fc48ac8a65d941d298b0b48151d1fb9a',
            'is_duplicate' => '1',
            'created' => $visitUpdated,
        );
        $redis->set('tracker_visit_'  . $_COOKIE['visitor'], json_encode($newVisitData));

        // update the visit
        $tracker = new Icm_Visit_Tracker($this->tracker_config, $this->redis_config, $this->cgdb_config,
                                         $this->rabbitmq_config, $this->app_id);
        $tracker->setInTesting(true);

        $updates = array(
            'is_converted' => true,
            'is_pixel_fired' => true,
            'converted' => date('Y-m-d H:i:s'),
        );
        $tracker->updateVisit($updates);

        // should have written updated data to the queue
        $updatedVisitData = $this->getQueueMessage('UpdateClick');
        $this->assertNotNull($updatedVisitData);

        // should have written boolean (or int) value for is_duplicate
        $this->assertInternalType('bool', $updatedVisitData['is_duplicate']);
        $this->assertTrue($updatedVisitData['is_duplicate']);

        // cleanup the visit in redis
        $redis->del('tracker_visit_' . $_COOKIE['visitor']);

        // cleanup the visit and visitor in mysql
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');
    }

    /**
     * Verify a set of visit data matches expected values
     */
    protected function assertVisitDataMatches($expected, $data) {
        foreach ($expected as $key => $value) {
            if (is_null($value)) {
                $this->assertNull($data[$key], "$key was not null");
                continue;
            }

            if ($key == 'updated') {
                continue;
            }

            $this->assertEquals($value, $data[$key], "$key did not match: expected $value got {$data[$key]}");
        }
    }

    /**
     * Grab a message from rabbitmq
     *
     * @param Icm_Config $config
     * @param string $queue_name
     * @return array
     */
    protected function getQueueMessage($queue_name) {
        // grab a new rabbitmq queue for the given queue name
        $queue_factory = new Icm_QueueFactory($this->rabbitmq_config);
        $method_name = 'get' . $queue_name . 'Queue';
        $queue = $queue_factory->$method_name();

        // fetch the latest message
        $messages = $queue->receive(1);

        // decode it
        foreach ($messages as $message) {
            return json_decode($message->body, true);
        }
    }
}