<?php

class Icm_Visit_Pixel_Test extends PHPUnit_Framework_TestCase
{
    protected $tracker;
    protected $redis;
    protected $queue_factory;
    protected $created_keys;
    protected $marketing_config;

    public function setUp() {
        // set server variables for testing
        $_SERVER['HTTP_HOST'] = 'localhost';
        $_SERVER['HTTP_USER_AGENT'] = 'Firefox';
        $_SERVER['REQUEST_URI'] = 'http://localhost/';

        parent::setUp();

        // get the configs
        $tracker_config = Icm_Config::fromIni('../config/development/defaults.ini', 'visitTracker');
        $redis_config = Icm_Config::fromIni('../config/development/defaults.ini', 'redis');
        $cgdb_config = Icm_Config::fromIni('../config/development/defaults.ini', 'cgDb');
        $rabbitmq_config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');
        $this->marketing_config = Icm_Config::fromIni('../config/development/defaults.ini', 'marketing');

        // set the app id
        $app_id = 1;

        // create a new tracker
        $this->tracker = new Icm_Visit_Tracker($tracker_config, $redis_config, $cgdb_config, $rabbitmq_config, $app_id);
        $this->tracker->setInTesting(true);

        // setup redis connection
        $this->redis = new Icm_Redis($redis_config);
        $this->created_keys = array();

        // setup queue factory
        $this->queue_factory = new Icm_QueueFactory($rabbitmq_config);
    }

    public function tearDown() {
        $this->purgeQueue('CreateClick');
        $this->purgeQueue('UpdateClick');
        $this->purgeQueue('Postback');
        $this->deleteCreatedKeys();
        parent::tearDown();
    }

    public function resetTracker() {
        // get the configs
        $tracker_config = Icm_Config::fromIni('../config/development/defaults.ini', 'visitTracker');
        $redis_config = Icm_Config::fromIni('../config/development/defaults.ini', 'redis');
        $cgdb_config = Icm_Config::fromIni('../config/development/defaults.ini', 'cgDb');
        $rabbitmq_config = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'rabbitmq');

        // set the app id
        $app_id = 1;

        // create a new tracker
        $this->tracker = new Icm_Visit_Tracker($tracker_config, $redis_config, $cgdb_config, $rabbitmq_config, $app_id);
        $this->tracker->setInTesting(true);
    }

    /**
     * Test the basic get pixels functionality without risk mitigation
     */
    public function testGetPixelsBasicNoRisk() {
        // pre-populate pixel data in redis
        $campaign_id = 1337;
        $affiliate_id = 'testaffiliateid';
        $section_id = 1;

        // create three global pixels
        $pixel_data = array('pixel_code' => '{{affiliate_id}}');
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create the campaign pixel
        $conversion_split_context_id = $section_id + 1;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create the affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create a new tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // setup new pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should have replaced the pixel_code with the affiliate id
        foreach ($pixels as $pixel) {
            $pixel_code = $pixel['pixel_code'];
            $this->assertEquals($affiliate_id, $pixel_code);
        }

        // should not have added anything to the postback queue
        $messages = $this->getQueueMessages('Postback');
        $this->assertTrue(empty($messages));

        // none of the pixels should have fired
        $visitData = $this->tracker->getVisitData();
        $this->assertFalse($visitData['is_pixel_fired']);

        // delete the test pixel data in redis
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit();
    }

    /**
     * Test getting pixels that have affiliate-specific pixels mixed in
     */
    public function testGetPixelsAffiliateNoRisk() {
        // create the global pixels
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'type' => 'global',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create the campaign pixel
        $conversion_split_context_id = $section_id + 1;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create the initial affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create generic affiliate pixel
        $redis_key_stem = $this->marketing_config->getOption('pixelPrefix') . "{$campaign_id}_{$section_id}";
        $generic_affiliate_key = $redis_key_stem . '_' . $affiliate_id . '_' . $this->marketing_config->getOption('pixelIdPrefix') . '_0';
        $pixel_data['type'] = 'generic_affiliate';
        $this->createRedisPixel($generic_affiliate_key, $pixel_data);

        // create affiliate pixel with subid
        $sub_id = 'test_sid';
        $subid_affiliate_key = $redis_key_stem . '_' . $affiliate_id . '_' . $sub_id . '_' . $this->marketing_config->getOption('pixelIdPrefix') . '_0';
        $pixel_data['type'] = 'affiliate';
        $this->createRedisPixel($subid_affiliate_key, $pixel_data);

        // create fake affiliate pixel (should not show up in later results)
        $fake_affiliate_key = $redis_key_stem . '_' . $affiliate_id . '_fakesubid_' . $this->marketing_config->getOption('pixelIdPrefix') . '_0';
        $pixel_data['type'] = 'fake_affiliate';
        $this->createRedisPixel($fake_affiliate_key, $pixel_data);

        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => $sub_id,
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // setup new pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should have found global and affiliate-specific pixels
        $this->assertEquals(5, count($pixels));

        // info in pixels should match what was inserted into redis
        foreach ($pixels as $pixel) {
            $this->assertNotEquals('fake_affiliate', $pixel['type']);
        }

        // should not have posted to the postback queue
        $messages = $this->getQueueMessages('Postback');
        $this->assertTrue(empty($messages));

        // delete the test data in redis
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit();
    }

    /**
     * Test getting pixels that also submit to postback queue
     */
    public function testGetPixelsPostbackNoRisk() {
        // pre-populate the pixels with postback_url data in redis
        $campaign_id = 1337;
        $affiliate_id = 'testaffiliateid';
        $section_id = 1;

        // create three global pixels
        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'postback_url' => 'http://postback.com/{{s1}}/{{payout}}',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create the campaign pixel
        $conversion_split_context_id = $section_id + 1;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create the affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create the tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // setup new visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should have the right number of pixels (none, since all are postback)
        $this->assertEquals(0, count($pixels));

        // should have run replacements on postback_url
        $replaced_postback_url = 'http://postback.com/test_s1/test_link_payout';

        foreach ($pixels as $pixel) {
            $this->assertEquals($replaced_postback_url, $pixel['postback_url']);
        }

        // should have posted the right number of times to the postback queue
        $messages = $this->getQueueMessages('Postback', 3);
        $this->assertEquals(3, count($messages));

        // queue data should match what was originally in redis
        foreach ($messages as $message) {
            // postback url should be the replaced one
            $this->assertEquals($replaced_postback_url, $message);
        }

        // delete the created redis data
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit();
    }

    /**
     * Test getting pixels that process as converted
     */
    public function testGetPixelsCampaignConvertedFiredNoRisk() {
        // create global pixels with affiliate id
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'affiliate_id' => $affiliate_id,
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right context id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should have the right number
        $this->assertEquals(3, count($pixels));

        // visitData should show conversion
        $visit_data = $this->tracker->getVisitData();
        $this->assertTrue($visit_data['is_converted']);

        // visitData should show pixel fired
        $this->assertTrue($visit_data['is_pixel_fired']);

        // should have pushed updated click data into queue
        $messages = $this->getQueueMessages('UpdateClick');
        $this->assertFalse(empty($messages));
        $this->assertEquals(1, count($messages));

        // new pixel data in queue should show conversion and firing
        $queue_pixel = array_pop($messages);
        $this->assertTrue($queue_pixel['is_converted']);
        $this->assertTrue($queue_pixel['is_pixel_fired']);

        // delete the created redis data
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit();
    }

    /**
     * Test getting pixels that process as converted but not fired
     */
    public function testGetPixelsCampaignConvertedNotFiredNoRisk() {
        // create global pixels with no affiliate id
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right context id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should have the right number
        $this->assertEquals(3, count($pixels));

        // visitData should show conversion
        $visit_data = $this->tracker->getVisitData();
        $this->assertTrue($visit_data['is_converted']);

        // visitData should not show pixel fired
        $this->assertFalse($visit_data['is_pixel_fired']);

        // should have pushed updated click data into queue
        $messages = $this->getQueueMessages('UpdateClick');
        $this->assertFalse(empty($messages));
        $this->assertEquals(1, count($messages));

        // new pixel data in queue should show conversion but no firing
        $queue_pixel = $messages[0];
        $this->assertTrue($queue_pixel['is_converted']);
        $this->assertFalse($queue_pixel['is_pixel_fired']);

        // delete the created redis data
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit();
    }

    /**
     * Test replacing tokens in a context with visit data
     */
    public function testTokenReplacement() {
        // create a tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );

        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );

        $queue_only = true;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create a pixel
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);
        $order_id = 1337;
        $this->pixel->setOrderId($order_id);

        // generate a context string
        $context = "{{payout}}-{{affiliate_id}}-{{sub_id}}-{{s1}}-{{s2}}-{{s3}}-{{s4}}-{{s5}}-{{order_id}}";

        // replace tokens in the string
        $generated_context = $this->pixel->replaceTokens($context, $this->tracker->getVisitData());

        // generated version should match visit data
        $payout = $link['payout'];
        $affiliate_id = $link['affiliate_id'];
        $sub_id = $params['sid'];
        $s1 = $params['s1'];
        $s2 = $params['s2'];
        $s3 = $params['s3'];
        $s4 = $params['s4'];
        $s5 = $params['s5'];
        $expected = "{$payout}-{$affiliate_id}-{$sub_id}-{$s1}-{$s2}-{$s3}-{$s4}-{$s5}-{$order_id}";
        $this->assertEquals($expected, $generated_context);

        // delete the queued visit
        $this->deleteQueuedVisit();
    }

    /**
     * @expectedException Exception
     */
    public function testGetPixelsNoSectionId() {
        // setup tracker and visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => 'test_link_affiliate_id',
            'campaign_id' => 'test_link_campaign_id',
            'payout' => 'test_link_payout',
        );

        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );

        $queue_only = true;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create a pixel
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // getting pixels without section id should throw exception
        $this->pixel->getPixels();
    }

    /**
     * Test behavior of getPixels() method when no campaign id available
     */
    public function testGetPixelsNoCampaignIdNoRisk() {
        // create global pixels
        $campaign_id = 'test_campaign_id';
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 1);

        // create tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_management = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_management);

        // should return an empty array
        $this->assertTrue(empty($pixels), print_r($pixels, true));

        // cleanup
        $this->deleteCreatedKeys();
        $this->deleteQueuedVisit();
    }

    /**
     * Test behavior of getPixels() method when the pixels are already converted
     */
    public function testGetPixelsAlreadyConverted() {
        // create global pixels with no affiliate id
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right section id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // tell tracker the pixels are already converted
        $updates = array(
            'is_converted' => true,
        );
        $this->tracker->updateVisit($updates);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should be empty array
        $this->assertTrue(empty($pixels));

        // should not show any of the pixels as fired
        $visitData = $this->tracker->getVisitData();
        $this->assertFalse($visitData['is_pixel_fired']);

        // cleanup
        $this->deleteCreatedKeys();
        $this->deleteQueuedVisit(2);
    }

    public function testGetPixelsPastConversion() {
        // create global pixels with no affiliate id
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right section id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel
        $risk_mitigation = false;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // cleanup leftover test data
        $cgdb_config = Icm_Config::fromIni('../config/development/defaults.ini', 'cgDb');
        $db = Icm_Db_Pdo::connect('cgDb', $cgdb_config);
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');

        // write a new visitor and converted visit to the db
        $db->execute("INSERT INTO `visitors` (`visitor_id`, `customer_id`, `created`, `updated`)
                      VALUES
                      ('fe4b1cbcde4995c8c47da63ee34347a7a9237833', null, '2013-07-06 21:31:40', '2013-07-19 04:59:38');
        ");

        $db->execute("
                      INSERT INTO `visitor_visits`
                      (`visit_id`, `app_id`, `visitor_id`, `is_duplicate`, `is_converted`, `is_pixel_fired`,
                      `ip_address`, `converted`, `created`, `updated`)
                      VALUES
                      ('0c292a07fc48ac8a65d941d298b0b48151d1fb9a', 1, 'fe4b1cbcde4995c8c47da63ee34347a7a9237833', 0, 1, 0,
                      '68.5.217.138', '2013-07-19 04:59:40', '2013-07-19 04:51:00', '2013-07-19 04:59:40');
        ");

        // save the visitor id as a cookie so the tracker will use it
        $_COOKIE['visitor'] = 'fe4b1cbcde4995c8c47da63ee34347a7a9237833';

        // create a new tracker and a new visit for the visitor
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );

        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $risk_mitigation = false;
        $pixels = $this->pixel->getPixels($section_id, $risk_mitigation);

        // should be empty array
        $this->assertTrue(empty($pixels));

        // should not show any of the pixels as fired
        $visitData = $this->tracker->getVisitData();
        $this->assertFalse($visitData['is_pixel_fired']);

        // cleanup
        $this->deleteCreatedKeys();
        $this->deleteQueuedVisit(2);
        $db->execute('DELETE FROM visitor_visits WHERE visit_id = "0c292a07fc48ac8a65d941d298b0b48151d1fb9a"');
        $db->execute('DELETE FROM visitors WHERE visitor_id = "fe4b1cbcde4995c8c47da63ee34347a7a9237833"');
    }

    /**
     * Test the processing of converted pixels with risk management
     */
    public function testGetPixelsConvertedFiredRisk() {
        // create global pixels with affiliate id
        $campaign_id = 1337;
        $affiliate_id = 1337;
        $section_id = 1;

        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'affiliate_id' => $affiliate_id,
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right context id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel with high risk mitigation
        $risk_mitigation = 101;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // create tracker visit
        $link = array(
            'id' => 'test_link_id',
            'affiliate_id' => $affiliate_id,
            'campaign_id' => $campaign_id,
            'payout' => 'test_link_payout',
        );
        $params = array(
            'sid' => 'test_sid',
            's1' => 'test_s1',
            's2' => 'test_s2',
            's3' => 'test_s3',
            's4' => 'test_s4',
            's5' => 'test_s5',
        );
        $queue_only = false;
        $this->tracker->createVisit($link, $params, $queue_only);

        // create visit pixel
        $this->redis->select(0);
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);

        // get the pixels
        $pixels = $this->pixel->getPixels($section_id, true);

        // should have empty pixels array
        $this->assertTrue(empty($pixels));

        // visitData should show conversion
        $visit_data = $this->tracker->getVisitData();
        $this->assertTrue($visit_data['is_converted']);

        // visitData should show pixel not fired
        $this->assertFalse($visit_data['is_pixel_fired']);

        // delete the pixels
        $this->deleteCreatedKeys();

        // create new set with same params but risk management < 1
        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'affiliate_id' => $affiliate_id,
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right context id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel with low risk mitigation
        $risk_mitigation = -1;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // setup new tracker instance
        $this->resetTracker();
        $this->tracker->createVisit($link, $params, $queue_only);

        // get the pixels
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);
        $pixels = $this->pixel->getPixels($section_id, true);

        // should have the right number
        $this->assertEquals(3, count($pixels));

        // visit data should show conversion and firing
        $visit_data = $this->tracker->getVisitData();
        $this->assertTrue($visit_data['is_converted']);
        $this->assertTrue($visit_data['is_pixel_fired']);

        // delete the pixels
        $this->deleteCreatedKeys();

        // create new set with same params but risk management ~ 2/3
        $pixel_data = array(
            'pixel_code' => '{{affiliate_id}}',
            'affiliate_id' => $affiliate_id,
        );
        $redis_pixels = $this->createGlobalPixels($campaign_id, $section_id, $pixel_data, 3);

        // create campaign pixel with right context id
        $conversion_split_context_id = $section_id;
        $this->createCampaignPixel($campaign_id, $conversion_split_context_id);

        // create affiliate pixel with medium risk mitigation
        $risk_mitigation = 66;
        $this->createAffiliatePixel($affiliate_id, $risk_mitigation);

        // setup new tracker instance
        $this->resetTracker();
        $this->tracker->createVisit($link, $params, $queue_only);

        // get the pixels
        $this->pixel = new Icm_Visit_Pixel($this->tracker, $this->redis, $this->marketing_config, $this->queue_factory);
        $pixels = $this->pixel->getPixels($section_id, true);

        // should have zero or none
        if (count($pixels)) {
            $this->assertCount(3, $pixels);

            // visit data should show conversion and firing
            $visit_data = $this->tracker->getVisitData();
            $this->assertTrue($visit_data['is_converted']);
            $this->assertTrue($visit_data['is_pixel_fired']);
        }
        else {
            $this->assertCount(0, $pixels);

            // visit data should show conversion and but no firing
            $visit_data = $this->tracker->getVisitData();
            $this->assertTrue($visit_data['is_converted']);
            $this->assertFalse($visit_data['is_pixel_fired']);
        }

        // cleanup
        $this->deleteCreatedKeys();
        $this->deleteTrackerVisit(2);
    }

    /**
     * Delete the last queued visit(s)
     *
     * @param int $num_visits
     */
    protected function deleteQueuedVisit($num_visits = 1) {
        $this->getQueueMessages('CreateClick', $num_visits);
    }

    /**
     * Create a set of global pixels in redis
     *
     * @param string $campaign_id
     * @param string $section_id
     * @param array $pixel_data
     * @param int $num_pixels
     * @return array
     */
    protected function createGlobalPixels($campaign_id, $section_id, $pixel_data = array(), $num_pixels = 3) {
        $key_prefix = $this->marketing_config->getOption('pixelPrefix') . "{$campaign_id}_{$section_id}";
        $pixel_id_prefix = $this->marketing_config->getOption('pixelIdPrefix');
        $redis_pixels = array();

        // generate all the basic pixels
        for ($i = 1; $i <= $num_pixels; $i++) {
            $pixel_key = $key_prefix . '_' . $pixel_id_prefix . $i;
            $this->createRedisPixel($pixel_key, $pixel_data);
            $redis_pixels[] = $pixel_data;
        }

        return $redis_pixels;
    }

    /**
     * Create a campaign-level pixel
     *
     * @param string $campaign_id
     * @param string $conversion_split_context_id
     */
    protected function createCampaignPixel($campaign_id, $conversion_split_context_id) {
        $campaign_key = $this->marketing_config->getOption('campaignPrefix') . $campaign_id;
        $campaign_pixel = array(
            'conversion_split_context_id' => $conversion_split_context_id,
        );
        $this->createRedisPixel($campaign_key, $campaign_pixel);
    }

    /**
     * Create an affiliate-level pixel
     *
     * @param string $affiliate_id
     * @param int $risk_mitigation
     */
    protected function createAffiliatePixel($affiliate_id, $risk_mitigation = 0) {
        $affiliate_key = $this->marketing_config->getOption('affiliatePrefix') . $affiliate_id;
        $affiliate_pixel = array(
            'risk_mitigation' => $risk_mitigation,
        );
        $this->createRedisPixel($affiliate_key, $affiliate_pixel);
    }

    /**
     * Delete the last tracker vist
     */
    protected function deleteTrackerVisit() {
        $visit_key = $this->tracker->getConfig()->getOption('visitPrefix') . $this->tracker->getVisitId();
        $this->redis->del($visit_key);
    }

    /**
     * Delete the redis entries we created for testing
     */
    protected function deleteCreatedKeys() {
        // use the pixel db
        $redis_pixel_db = $this->marketing_config->getOption('redisDatabase');
        $this->redis->select($redis_pixel_db);

        foreach ($this->created_keys as $key) {
            $this->redis->del($key);
        }

        $this->redis->select(0);
    }

    /**
     * Add a single pixel to redis
     *
     * @param string $key
     * @param array $pixel_data
     */
    protected function createRedisPixel($key, $pixel_data = array()) {
        // use the redis pixel db
        $redis_pixel_db = $this->marketing_config->getOption('redisDatabase');
        $this->redis->select($redis_pixel_db);

        // push the pixel data into redis
        $this->redis->set($key, json_encode($pixel_data));
        $this->created_keys[] = $key;

        // re-set the redis pixel db
        $this->redis->select(0);
    }

    /**
     * Grab messages from rabbitmq
     *
     * @param Icm_Config $config
     * @param string $queue_name
     * @return array
     */
    protected function getQueueMessages($queue_name, $num_messages = 1) {
        // grab a new rabbitmq queue for the given queue name
        $method_name = 'get' . $queue_name . 'Queue';
        $queue = $this->queue_factory->$method_name();

        // fetch the latest message
        $queue_messages = $queue->receive($num_messages);

        // decode it
        $messages = array();

        foreach ($queue_messages as $queue_message) {
            $message_body = $queue_message->body;

            if (strpos($message_body, '{') !== false) {
                $message_body = json_decode($message_body, true);
            }

            $messages[] = $message_body;
        }

        return $messages;
    }

    /**
     * Purge all the messages from a given queue
     *
     * @param string $queue_name
     */
    protected function purgeQueue($queue_name) {
        // grab a new rabbitmq queue for the given queue name
        $method_name = 'get' . $queue_name . 'Queue';
        $queue = $this->queue_factory->$method_name();

        // purge the queue
        $queue->getAdapter()->purgeQueue();
    }
}