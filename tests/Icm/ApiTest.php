<?php

class Icm_Api_Test extends PHPUnit_Framework_TestCase
{
	public function testCreate() {
		$api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
        $this->assertInstanceOf('Icm_Api', $api);
		return $api;
	}

	/**
	 * @depends testCreate
	 */
	public function testEnvironmentConfig($api) {
		$env = $api->getEnvironment();
		$this->assertEquals('development',$env);
        return $api;
	}

    /**
	 * @depends testEnvironmentConfig
	 */
	public function testGetConfig($api) {
		$c = $api->getConfig();
		$this->assertInstanceOf('Icm_Config',$c);
        return $api;
	}

	/**
	 * @depends testGetConfig
	 */
	public function testGetDefaults($api) {
		$c = $api->getDefaults();
		$this->assertInstanceOf('Icm_Config',$c);
        return $api;
	}

	/**
	 * @depends testGetDefaults
	 */
	public function testDataContainer($api) {
		$this->assertInstanceOf('Icm_Container',$api->dataContainer);
	}

    public function testCreateDefaultEnv() {
        // delete any env or server data to make a clean slate
        session_unset();
        Icm_Session::writeClose();

		if (array_key_exists('api_env', $_SERVER)) {
			unset($_SERVER['api_env']);
		}

		if (array_key_exists('environment', $_SERVER)) {
			unset($_SERVER['environment']);
		}

		if (array_key_exists('api_env', $_ENV)) {
			unset($_ENV['api_env']);
		}

		$api = @Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config-no-env.ini'));
		$env = $api->getEnvironment();
        $this->assertEquals('production', $env);
	}

	public function testCreateConstantEnv() {
        session_unset();
        session_destroy();
        Icm_Session::writeClose();

		define('ENVIRONMENT','development');
		$api = @Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config-no-env.ini'));
        $this->assertInstanceOf('Icm_Api', $api);
		$env = $api->getEnvironment();
		$this->assertEquals('development', $env);
	}
}