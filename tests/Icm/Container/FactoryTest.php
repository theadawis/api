<?php

class Foo {
    const SOMETHING = 'bleh';

    function bar() {
        return 123;
    }

    function baz($b) {
        $this->b = $b;
    }

    function getB() {
        return $this->b;
    }

    function container(Icm_Container $c) {
        $this->c = $c;
    }

    function constant($c) {
        $this->con = $c;
    }
}

class Bar {
    public function __construct($string) {
        $this->string = $string;
    }
    public function hi() {
        return "Hello World!";
    }
    public function setSomething($something) {
        $this->something = $something;
    }
}

class Meh {
    public $a = array();

    public function foo() {
        $this->a = array_merge(func_get_args(), $this->a);
    }

    public function bar() {
        $this->a = array_merge(func_get_args(), $this->a);
    }

    public function baz() {
        $this->a = array_merge(func_get_args(), $this->a);
    }
}

class Icm_Container_Factory_Test extends PHPUnit_Framework_TestCase {

    public function testParseXml() {
        $xml = '
            <container name="foo">

                <service name="barService" class="Bar">
                    <arg>{Foo::SOMETHING}</arg>
                    <call method="setSomething">
                        <arg>BAR{Foo::SOMETHING}FOO</arg>
                    </call>
                </service>

                <service name="blahService" class="Blah">
                    <call method="doSomeStuff">
                        <arg>
                            <firstKey>firstValue</firstKey>
                            <secondKey>secondValue</secondKey>
                        </arg>
                    </call>
                </service>


                <service name="fooService" class="Foo">
                    <call method="baz">
                        <arg>@barService</arg>
                    </call>
                </service>

                <service name="containerContainer" class="Foo">
                    <call method="container">
                        <arg>:foo</arg>
                    </call>
                    <call method="constant">
                        <arg>{FOOBAR}</arg>
                    </call>
                </service>

                <service name="meh" class="Meh">
                    <call method="foo">
                        <arg>fooo</arg>
                        <arg>baaar</arg>
                    </call>
                    <call method="bar">
                        <arg>foooo</arg>
                        <arg>baaar</arg>
                    </call>
                    <call method="baz">
                        <arg>baaar</arg>
                        <arg>baaar</arg>
                        <arg>baaaz</arg>
                    </call>
                </service>

                <service name="sameMethodCall" class="Meh">
                    <call method="foo">
                        <arg>fooo</arg>
                        <arg>baaar</arg>
                    </call>
                    <call method="foo">
                        <arg>fooo</arg>
                        <arg>baaar</arg>
                    </call>
                    <call method="foo">
                        <arg>fooo</arg>
                        <arg>baaar</arg>
                    </call>
                    <call method="foo">
                        <arg>fooo</arg>
                        <arg>baaar</arg>
                    </call>
                </service>
            </container>
        ';

        define('FOOBAR', 'FOOOOOBAR');
        $f = new Icm_Container();

        $f->loadXml(simplexml_load_string($xml));

        $foo = $f->get('fooService');
        $this->assertTrue($foo->bar() == 123);
        $this->assertInstanceOf('Bar', $foo->getB());

        $b = $foo->getB();
        $this->assertEquals("Hello World!", $b->hi());
        $this->assertEquals(Foo::SOMETHING, $b->string);
        $this->assertEquals("BAR" . Foo::SOMETHING . "FOO", $b->something);

        $foo2 = $f->get('containerContainer');
        $this->assertInstanceOf('Icm_Container', $foo2->c);
        $this->assertEquals(FOOBAR, $foo2->con);

        $meh = $f->get('meh');
        $this->assertGreaterThan(3, count($meh->a));

        $sameMethodCall = $f->get('sameMethodCall');
        $this->assertCount(8, $sameMethodCall->a);
    }
}