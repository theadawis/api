<?php

class Icm_Config_Test extends PHPUnit_Framework_TestCase
{
    public function testSet() {
        $config = new Icm_Config();

        $config->setOption('someKey', 'someVal');

        $this->assertTrue($config->getOption('someKey') === 'someVal');
    }

    public function testDefault() {
        $config = new Icm_Config();
        $this->assertTrue($config->getOption('someKey', 'someVal') === 'someVal');
        $this->assertTrue($config->getOption('someKey2') === null);
    }

    public function testIni() {
        $config = Icm_Config::fromIni(realpath(dirname(__FILE__)) . '/test.ini');

        $this->assertInternalType('array', $config->getOption('testVals'));
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testWrongPath(){
        $config = Icm_Config::fromIni('/wrong/path');
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testUnreadableFile(){
        $chomdded = chmod(realpath(dirname(__FILE__)) . '/unreadableTest.ini', octdec(333));

        if (!$chomdded) {
            $this->markTestSkipped('Skipped test because could not set proper permissions on file');
        }

        $config = Icm_Config::fromIni(realpath(dirname(__FILE__)) . '/unreadableTest.ini');
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testWrongEnvirontment(){
        $config = Icm_Config::fromIni(realpath(dirname(__FILE__)) . '/test.ini', 'sectionDoesNotExist');
    }

    public function testSection(){
        $config = Icm_Config::fromIni(realpath(dirname(__FILE__)) . '/test.ini', 'testArrays');
        $this->assertTrue(is_array($config->getOption('key')));
        $this->assertCount(3, $config->getOption('key'));

    }

    public function tearDown() {
        chmod(realpath(dirname(__FILE__)) . '/unreadableTest.ini', octdec(666));
    }
}
