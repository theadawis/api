<?php

class Icm_Redis_Test extends PHPUnit_Framework_TestCase
{
    public function testRedis(){
        $redis = new Icm_Redis(Icm_Config::fromIni('../config/development/defaults.ini', 'redis'));
        $session = new Icm_Session_Storage_Redis(Icm_Config::fromArray(array()), $redis);
        $session_id = 'joetestsession';
        $session->write($session_id, array('hi'=>'there'));
        $data = $session->read($session_id);
        $this->assertTrue($data['hi'] == 'there');
        $session->destroy($session_id);
        $this->assertTrue(!$session->read($session_id));
    }
}
