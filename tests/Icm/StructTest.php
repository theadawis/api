<?php

class TestPerson extends Icm_Struct
{
    public $first_name, $last_name, $address, $phone;
}

class Icm_Struct_Test extends PHPUnit_Framework_TestCase
{
    public function testStruct(){
        $person = new TestPerson();
        $person->first_name = 'Bob';
        $this->assertTrue($person->first_name == 'Bob');
        $this->setExpectedException('Icm_Exception');
        $person->fname = 'Bob';
    }
}