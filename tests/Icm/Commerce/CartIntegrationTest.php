<?php
/**
 * CartIntegration.php
 * User: chris
 * Date: 1/10/13
 * Time: 8:41 AM
 */
class Icm_Commerce_CartIntegration_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Commerce_Order
     */
    protected $order;

    /**
     * @var Icm_Commerce_Gateway_Litle
     */
    protected $gateway;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $litleApi;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $pdo;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $orangeApi;

    /**
     * @var Icm_Commerce_Crm_Orange
     */
    protected $crm;

    public function setUp() {
        $this->litleApi = $this->getMock("Icm_Service_Litle_Api", array(), array(), "", false);
        $this->orangeApi = $this->getMock("Icm_Service_Orange_Api", array(), array(), "", false);
        $this->pdo = $this->getMock("Icm_Db_Pdo", array(), array(), "", false);
        $this->order = new Icm_Commerce_Order();
        $this->gateway = new Icm_Commerce_Gateway_Litle($this->litleApi);
        $this->crm = new Icm_Commerce_Crm_Orange($this->orangeApi);
    }

    public function testCreate() {
        $cart = new Icm_Commerce_Cart(
            $this->order, $this->crm, $this->gateway, $this->pdo
        );

        $this->assertInstanceOf("Icm_Commerce_Cart", $cart);
        return $cart;
    }

    /**
     * @depends testCreate
     */
    public function testAddProduct(Icm_Commerce_Cart $cart){
        $this->assertEmpty($cart->getProducts()->getKeySet());
        $this->assertFalse($cart->hasProducts());
        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 1,
        )));
        $this->assertNotEmpty($cart->getProducts());
        $this->assertTrue(($cart->hasProducts()));

        return $cart;
    }

    /**
     * @depends testCreate
     */
    public function testCheckout(){
        $document = new DOMDocument("1.0", "UTF-8");
        $root = $document->appendChild($document->createElement("litleOnlineResponse"));
        $root->setAttribute("version", "8.14");
        $root->setAttribute("response", "0");
        $root->setAttribute("message", "Valid Format");
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

        $saleResponse = $root->appendChild($document->createElement("saleResponse"));
        $saleResponse->setAttribute("id", "1");
        $saleResponse->setAttribute("reportGroup", $this->gateway->getReportGroupCode());
        $saleResponse->setAttribute("customerId", rand(100, 1000));
        $saleResponse->appendChild($document->createElement("litleTxnId", "1100026202"));
        $saleResponse->appendChild($document->createElement("orderId", $this->order->getOrderId()));
        $saleResponse->appendChild($document->createElement("response", "000"));
        $saleResponse->appendChild($document->createElement("responseTime", "2011-07-16T19:43:38"));
        $saleResponse->appendChild($document->createElement("postDate", "2011-07-16"));
        $saleResponse->appendChild($document->createElement("message", "Approved"));

        $this->litleApi->expects($this->once())->method("chargeCard")->will($this->returnValue(simplexml_load_string($document->saveXML())));

        $this->orangeApi->expects($this->once())->method("addCustomer")->will($this->returnValue(
            array(
                'q_cust_guid' => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                'code' => 0,
                'result' => "success",
                'reason' => 'testing'
            )
        ));

        $this->orangeApi->expects($this->once())->method('addTransaction')->will($this->returnValue(
            array(
                'code' => 1,
                'result' => 'success',
                'reason' => 'testing',
            )
        ));

        $customer = new Icm_Commerce_Customer();
        $creditCard = new Icm_Commerce_Creditcard();
        $creditCard->first_name = "foo";
        $creditCard->last_name = "bar";

        $visit = new Icm_Struct_Tracking_Visit();
        $affiliate = new Icm_Struct_Marketing_Affiliate();
        $affiliate->short_name = "AFFSHORTNAME";
        $visit->setAffiliate($affiliate);

        $customer->setCreditCard($creditCard);
        $customer->setVisit($visit);

        $cart = new Icm_Commerce_Cart(
            $this->order, $this->crm, $this->gateway, $this->pdo
        );

        $product = Icm_Commerce_Product::fromArray(array('product_id' => 1));
        $cart->addProduct($product);

        $productGateway = $this->getMock('Icm_Commerce_Product_Gateway_StorageAdapter', array(), array(), '', false);
        $productGateway->expects($this->once())->method('get')->will($this->returnValue(
            Icm_Commerce_Product_Gateway::fromArray(
                array(
                    'gateway_id' => $this->gateway->gateway_id,
                    'bank' => $this->gateway->bank,
                    'product_id' => 1,
                )
            )
        ));
        $cart->setProductGateway($productGateway);

        $cart->checkout($customer);

        // should have set is_test for order
        $is_test = $cart->getOrder()->is_test;
        $this->assertFalse($is_test);
    }
}
