<?php

class Icm_Commerce_Transaction_ScoreStorageAdapter_Test extends TestCase
{

    protected $conn;
    protected $score;
    
    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->score = new Icm_Commerce_Transaction_ScoreStorageAdapter('ris_score', 'ris_score_id', $this->conn);
    }
    
    public function testCreate()
    {
        $this->assertTrue(is_a($this->score, "Icm_Commerce_Transaction_ScoreStorageAdapter"));
    }

    public function testSaveWithInsert()
    {
        $scoreId = rand(1, 1000000);
        $score = $this->generateCommerceScore(array('ris_score_id' => null));
        
        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));
        
        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($scoreId));
        
        $this->score->save($score);
        $this->assertEquals($scoreId, $score->ris_score_id);
    }
}
