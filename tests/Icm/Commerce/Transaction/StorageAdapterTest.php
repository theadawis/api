<?php

class Icm_Commerce_Transaction_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $transaction;

    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->transaction = new Icm_Commerce_Transaction_StorageAdapter('transaction', 'transaction_id', $this->conn);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->transaction, "Icm_Commerce_Transaction_StorageAdapter"));
    }

    public function testGetById()
    {
        $transactionId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceTransaction(array('transaction_id' => $transactionId))->toArray()));

        $result = $this->transaction->getById($transactionId);

        $this->assertTrue(is_a($result, "Icm_Commerce_Transaction"));
        $this->assertEquals($transactionId, $result->transaction_id);
    }

    public function testSaveWithInsert()
    {
        $transactionId = rand(1, 1000000);
        $transaction = $this->generateCommerceTransaction(array('transaction_id' => null));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($transactionId));

        $this->transaction->save($transaction);
        $this->assertEquals($transactionId, $transaction->transaction_id);
    }

    public function testSaveWithUpdate()
    {
        $transactionId = rand(1, 1000000);
        $transaction = $this->generateCommerceTransaction(array('transaction_id' => $transactionId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->transaction->save($transaction);
        $this->assertEquals($transactionId, $transaction->transaction_id);
    }
}
