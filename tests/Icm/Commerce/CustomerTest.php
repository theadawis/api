<?php

class Icm_Commerce_Customer_Test extends PHPUnit_Framework_TestCase
{
    protected $cust;

    /**
     * Values that will evaluate to empty()
     * @var array
     */
    protected $paramsEmpty = array('', 0, 0.0, "0", null, false, array());

    public function setup() {
        $this->cust = new Icm_Commerce_Customer;
    }

    public function testAttributesExist() {
        // public
        $this->assertClassHasAttribute('customer_id',               'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('email',                     'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('email_score',               'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('password',                  'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('first_name',                'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('last_name',                 'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('guid',                      'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('credits',                   'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('created',                   'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('updated',                   'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('phone',                     'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('address1',                  'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('address2',                  'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('city',                      'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('state',                     'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('preferred_gateway',         'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('zip',                       'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('forgot_password_hash',      'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('forgot_password_expires',   'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('crm_config_id',             'Icm_Commerce_Customer');

        // protected
        $this->assertClassHasAttribute('visit',                     'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('creditcard',                'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('registrationVariation',     'Icm_Commerce_Customer');
        $this->assertClassHasAttribute('gateway',                   'Icm_Commerce_Customer');
    }

    public function testSetGateway() {
        $gateway = $this->getMock("Icm_Commerce_Gateway_Litle", array('getGatewayId'), array(), '', false);
        $gateway->expects($this->any())
        ->method('getGatewayId')
        ->will($this->returnValue(999));
        $obj = $this->cust->setGateway($gateway);
        $this->assertInstanceOf('Icm_Commerce_Customer', $obj);
        $this->assertObjectHasAttribute('preferred_gateway', $obj);
        $this->assertEquals(999, $obj->preferred_gateway);
        $this->assertObjectHasAttribute('gateway', $obj);
    }
    public function testSetGatewayEmptyParametersException() {
        // empty parameters
        $max = count($this->paramsEmpty);
        $count = 0;
        // loop continues as long as exceptions occur
        foreach($this->paramsEmpty as $param) {
            try {
                $this->cust->setGateway($param);
                $this->fail('An expected exception has not been raised.');
            } catch (Exception $e) {
                // expected exception, increment asserts and continue foreach
                $this->assertTrue(true);
            }
        }
    }
    public function testGetGateway() {
        $gateway = $this->getMock("Icm_Commerce_Gateway_Litle", array(), array(), '', false);
        $this->cust->setGateway($gateway);
        $obj = $this->cust->getGateway();
        $this->assertInstanceOf('Icm_Commerce_Gateway_Litle', $obj);
    }
    public function testGetGatewayException() {
        $this->setExpectedException('Exception');
        $this->cust->getGateway();
    }

    public function testSetVaration() {
        $variation = $this->getMock("Icm_SplitTest_Variation", array(), array(), '', false);
        $obj = $this->cust->setVaration($variation);
        $this->assertInstanceOf('Icm_Commerce_Customer', $obj);
    }
    public function testGetVaration() {
        $this->registrationVariation = $this->getMock("Icm_SplitTest_Variation", array(), array(), '', false);
        $this->cust->setVaration($this->registrationVariation);
        $obj = $this->cust->getVaration();
        $this->assertInstanceOf('Icm_SplitTest_Variation', $obj);
    }
    public function testGetVarationException() {
        $this->setExpectedException('Exception');
        $this->cust->getVaration();
    }

    public function testSetVisit() {
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), '', false);
        $obj = $this->cust->setVisit($visit);
        $this->assertInstanceOf('Icm_Commerce_Customer', $obj);
    }
    public function testGetVisit() {
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), '', false);
        $this->cust->setVisit($visit);
        $obj = $this->cust->getVisit();
        $this->assertInstanceOf('Icm_Struct_Tracking_Visit', $obj);
    }
    public function testGetVisitException() {
        $this->setExpectedException('Exception');
        $this->cust->getVisit();
    }

    public function testSetCreditCard() {
        $cc = $this->getMock("Icm_Commerce_Creditcard", array(), array(), '', false);
        $obj = $this->cust->setCreditCard($cc);
        $this->assertInstanceOf('Icm_Commerce_Customer', $obj);
    }

    public function testGetCreditCard() {
        $cc = $this->getMock("Icm_Commerce_Creditcard", array(), array(), '', false);
        $this->cust->setCreditCard($cc);
        $card = $this->cust->getCreditCard();
        $this->assertInstanceOf('Icm_Commerce_Creditcard', $card);
    }
    public function testGetCreditCardException() {
        $this->setExpectedException('Exception');
        $this->cust->getCreditCard();
    }

    public function testValidate() {
        $cc = $this->getMock("Icm_Commerce_Creditcard", array(), array(), '', false);
        $this->cust->setCreditCard($cc);
        $this->cust->validate();
    }
    public function testValidateException() {
        $this->setExpectedException('Exception');
        $this->cust->validate();
    }
}
