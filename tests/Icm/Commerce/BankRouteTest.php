<?php

class Icm_Commerce_BankRoute_Test extends PHPUnit_Framework_TestCase
{
    public function testAttributesExist() {
        new Icm_Commerce_BankRoute;
        $this->assertClassHasAttribute('app_id', 'Icm_Commerce_BankRoute');
        $this->assertClassHasAttribute('gateway_id', 'Icm_Commerce_BankRoute');
        $this->assertClassHasAttribute('for_upsell', 'Icm_Commerce_BankRoute');
        $this->assertClassHasAttribute('bank_route', 'Icm_Commerce_BankRoute');
        $this->assertClassHasAttribute('name', 'Icm_Commerce_BankRoute');
    }
}
