<?php

class Icm_Commerce_Product_Test extends PHPUnit_Framework_TestCase
{
    protected $product;

    public function setup() {
        $this->product = new Icm_Commerce_Product;
    }

    public function testAttributesExist() {
        // public
        $this->assertClassHasAttribute('product_id',                'Icm_Commerce_Product');
        $this->assertClassHasAttribute('name',                      'Icm_Commerce_Product');
        $this->assertClassHasAttribute('sku',                       'Icm_Commerce_Product');
        $this->assertClassHasAttribute('billing_type',              'Icm_Commerce_Product');
        $this->assertClassHasAttribute('initial_price',             'Icm_Commerce_Product');
        $this->assertClassHasAttribute('recurring_price',           'Icm_Commerce_Product');
        $this->assertClassHasAttribute('trial_days',                'Icm_Commerce_Product');
        $this->assertClassHasAttribute('membership_months',         'Icm_Commerce_Product');
        $this->assertClassHasAttribute('product_tier',              'Icm_Commerce_Product');
        $this->assertClassHasAttribute('credits',                   'Icm_Commerce_Product');
        $this->assertClassHasAttribute('product_type',              'Icm_Commerce_Product');
        $this->assertClassHasAttribute('credits_type_id',           'Icm_Commerce_Product');
        $this->assertClassHasAttribute('status',                    'Icm_Commerce_Product');
        $this->assertClassHasAttribute('created',                   'Icm_Commerce_Product');
        $this->assertClassHasAttribute('updated',                   'Icm_Commerce_Product');
        $this->assertClassHasAttribute('orange_billing_plan_id',    'Icm_Commerce_Product');
        $this->assertClassHasAttribute('is_upsell',                 'Icm_Commerce_Product');
        $this->assertClassHasAttribute('orange_program_id',         'Icm_Commerce_Product');
        $this->assertClassHasAttribute('orange_cust_token',         'Icm_Commerce_Product');
        $this->assertClassHasAttribute('fallback_product_id',       'Icm_Commerce_Product');
    }

    public function testGetProductType() {
        $this->product->product_type = 'CR';
        $this->assertEquals($this->product->product_type, $this->product->getProductType());
    }

    public function testConstantsValues() {
        $this->assertEquals(Icm_Commerce_Product::TYPE_STRAIGHT_SALE,   'straight_sale');
        $this->assertEquals(Icm_Commerce_Product::TYPE_TRIAL,           'trial');
        $this->assertEquals(Icm_Commerce_Product::TYPE_NON_RECURRING,   'non_recurring');
    }

    public function testGetHash() {
        $this->product->product_id = 999;
        $this->assertEquals($this->product->product_id, $this->product->getHash());
    }

    public function testGetContinuity() {
        $this->product->billing_type = 1;
        $this->assertEquals($this->product->billing_type, $this->product->getContinuity());
    }

    public function testGetCode() {
        $this->product->billing_type = Icm_Commerce_Product::TYPE_TRIAL;
        $this->assertEquals('TR', $this->product->getCode());
        $this->product->billing_type = Icm_Commerce_Product::TYPE_STRAIGHT_SALE;
        $this->assertEquals('SS', $this->product->getCode());
        $this->product->billing_type = Icm_Commerce_Product::TYPE_NON_RECURRING;
        $this->assertEquals('SO', $this->product->getCode());
    }

    public function testGetCodeNoException() {
        $this->product->getCode();
    }
}
