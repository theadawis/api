<?php

class Icm_Commerce_Order_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $osa;

    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->osa = new Icm_Commerce_Order_StorageAdapter('orders', 'order_id', $this->conn);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->osa, "Icm_Commerce_Order_StorageAdapter"));
    }

    public function testGetById()
    {
        $orderId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceOrder(array('order_id' => $orderId))->toArray()));

        $result = $this->osa->getById($orderId);

        $this->assertTrue(is_a($result, "Icm_Commerce_Order"));
        $this->assertEquals($orderId, $result->order_id);
    }

    public function testSaveWithInsert()
    {
        $orderId = rand(1, 1000000);
        $order = $this->generateCommerceOrder(array(
            'order_id' => null,
            'visitor_id' => null,
            'is_test' => 1
        ));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($orderId));

        $this->osa->save($order);
        $this->assertEquals($orderId, $order->order_id);
        $this->assertEquals(1, $order->is_test);
    }

    public function testSaveWithUpdate()
    {
        $orderId = rand(1, 1000000);
        $order = $this->generateCommerceOrder(array('order_id' => $orderId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->osa->save($order);
        $this->assertEquals($orderId, $order->order_id);
    }
}
