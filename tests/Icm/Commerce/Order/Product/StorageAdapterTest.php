<?php

class Icm_Commerce_Order_Product_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $order_product;

    public function setUp() {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->order_product = new Icm_Commerce_Order_Product_StorageAdapter('order_products', 'order_product_id', $this->conn);

        parent::setUp();
    }

    public function testCreate() {
        $this->assertTrue(is_a($this->order_product, "Icm_Commerce_Order_Product_StorageAdapter"));
    }

    public function testGetById() {
        $order_productId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceOrderProduct(array('order_product_id' => $order_productId))->toArray()));

        $result = $this->order_product->getById($order_productId);

        $this->assertTrue(is_a($result, "Icm_Commerce_Order_Product"));
        $this->assertEquals($order_productId, $result->order_product_id);
    }

    public function testSaveWithInsert() {
        $order_productId = rand(1, 1000000);
        $order_product = $this->generateCommerceOrderProduct(array('order_product_id' => null));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($order_productId));

        $this->order_product->save($order_product);
        $this->assertEquals($order_productId, $order_product->order_product_id);
    }

    public function testSaveWithUpdate() {
        $order_productId = rand(1, 1000000);
        $order_product = $this->generateCommerceOrderProduct(array('order_product_id' => $order_productId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->order_product->save($order_product);
        $this->assertEquals($order_productId, $order_product->order_product_id);
    }

    public function testGetByCustomer() {
        // delete any leftover test data
        $this->deleteTestOrderProduct();
        $this->deleteTestProduct();
        $this->deleteTestOrder();
        $this->deleteTestCustomer();

        // setup the order_product adapter to use a real db connection
        $adapter = new Icm_Commerce_Order_Product_StorageAdapter('order_products', 'order_product_id', $this->cgDb);

        // create customer without customer id
        $customerParams = array(
            'email' => 'tester@email.com',
            'first_name' => 'John',
        );
        $customer = Icm_Commerce_Customer::fromArray($customerParams);

        // try to get order products for customer
        $orderProducts = $adapter->getByCustomer($customer);

        // should return an empty array
        $this->assertInternalType('array', $orderProducts);
        $this->assertEmpty($orderProducts);

        // write customer to db
        $customerId = $this->writeTestCustomer($customerParams['email'], $customerParams['first_name']);
        $customer->customer_id = $customerId;

        // create an order product for that customer
        $orderId = $this->writeTestOrder($customerId);
        $productId = $this->writeTestProduct();
        $orderProductId = $this->writeTestOrderProduct($productId, $customerId, $orderId);

        // pull it back out
        $orderProducts = $adapter->getByCustomer($customer);

        // should match
        $this->assertCount(1, $orderProducts);
        $this->assertInstanceOf('Icm_Commerce_Order_Product', $orderProducts[0]);
        $this->assertEquals($orderProductId, $orderProducts[0]->order_product_id);
    }
}
