<?php

class Icm_Commerce_Transaction_Test extends PHPUnit_Framework_TestCase
{
    protected $transaction;

    public function setup() {
        $this->transaction = new Icm_Commerce_Transaction();
    }

    public function testAttributesExist() {
        $this->assertClassHasAttribute('transaction_id',    'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('parent_id',         'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('gateway_id',        'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('order_id',          'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('type',              'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('gateway_txid',      'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('amount',            'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('failure',           'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('failure_reason',    'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('orange_guid',       'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('created',           'Icm_Commerce_Transaction');
        $this->assertClassHasAttribute('updated',           'Icm_Commerce_Transaction');
    }

    public function testConstantsValues() {
        $this->assertEquals(Icm_Commerce_Transaction::TYPE_PURCHASE,    0);
        $this->assertEquals(Icm_Commerce_Transaction::TYPE_REFUND,      1);
        $this->assertEquals(Icm_Commerce_Transaction::TYPE_CHARGEBACK,  2);
    }

    public function testGetTransactionId() {
        $this->transaction->transaction_id = 999;
        $this->assertEquals(999, $this->transaction->getTransactionId());
    }

    public function testGetOrderId() {
        $this->transaction->order_id = 888;
        $this->assertEquals(888, $this->transaction->getOrderId());
    }
}
