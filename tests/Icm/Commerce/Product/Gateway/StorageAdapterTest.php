<?php

class Icm_Commerce_Product_Gateway_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $pgsa;

    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->pgsa = new Icm_Commerce_Product_Gateway_StorageAdapter('product_gateway', 'order_id', $this->conn);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->pgsa, "Icm_Commerce_Product_Gateway_StorageAdapter"));
    }

    public function testGet()
    {
        $productId = rand(1, 1000000);
        $gatewayId = rand(1, 1000000);

        $gateway = new Icm_Commerce_Gateway_Nmi(new Icm_Service_Nmi_Api(Icm_Config::fromArray(array())));
        $gateway->gateway_id = $gatewayId;
        $gateway->short_name = "NMI";

        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceProductGateway(array(
                        'product_id' => $productId,
                        'gateway_id' => $gatewayId
                    ))->toArray()));

        $result = $this->pgsa->get($this->generateCommerceProduct(array('product_id' => $productId)), $gateway);

        $this->assertTrue(is_a($result, "Icm_Commerce_Product_Gateway"));
        $this->assertEquals($productId, $result->product_id);
        $this->assertEquals($gatewayId, $result->gateway_id);
    }

    public function testSaveWithInsert()
    {
        $productId = rand(1, 1000000);
        $gatewayId = rand(1, 1000000);
        $productGateway = $this->generateCommerceProductGateway(array('product_id' => $productId, 'gateway_id' => $gatewayId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $result = $this->pgsa->save($productGateway);
        $this->assertTrue($result);
    }

    public function testSaveWithUpdate()
    {
        $productId = rand(1, 1000000);
        $gatewayId = rand(1, 1000000);
        $productGateway = $this->generateCommerceProductGateway(array('product_id' => $productId, 'gateway_id' => $gatewayId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                    ->method('fetch')
                    ->will($this->returnValue(array('product_id' => $productId, 'gateway_id' => $gatewayId)));

        $result = $this->pgsa->save($productGateway);
        $this->assertTrue($result);
    }
}
