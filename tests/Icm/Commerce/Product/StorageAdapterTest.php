<?php

class Icm_Commerce_Product_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $psa;

    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->psa = new Icm_Commerce_Product_StorageAdapter('products', 'product_id', $this->conn);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->psa, "Icm_Commerce_Product_StorageAdapter"));
    }

    public function testGetById()
    {
        $productId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceProduct(array('product_id' => $productId))->toArray()));

        $result = $this->psa->getById($productId);

        $this->assertTrue(is_a($result, "Icm_Commerce_Product"));
        $this->assertEquals($productId, $result->product_id);
    }

    public function testSaveWithInsert()
    {
        $productId = rand(1, 1000000);
        $product = $this->generateCommerceproduct(array('product_id' => null));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($productId));

        $this->psa->save($product);
        $this->assertEquals($productId, $product->product_id);
    }

    public function testSaveWithUpdate()
    {
        $productId = rand(1, 1000000);
        $product = $this->generateCommerceproduct(array('product_id' => $productId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->psa->save($product);
        $this->assertEquals($productId, $product->product_id);
    }
}
