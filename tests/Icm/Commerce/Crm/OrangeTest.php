<?php

class Icm_Commerce_Crm_Orange_Test extends TestCase
{
	/**
	 * @var Icm_Commerce_Crm_Orange
	 */
	protected $crm;

	/**
	 * @var PHPUnit_Framework_MockObject_MockObject
	 */
	protected $api;

	protected function setUp()
	{
		$this->api = $this->getMock("Icm_Service_Orange_Api", array(), array(), '', false);
	 	$this->crm = new Icm_Commerce_Crm_Orange($this->api);
	}

	public function testCreate()
	{
		$this->assertTrue(is_a($this->crm, "Icm_Commerce_Crm_Orange"));
	}

	public function testSetApi()
	{
		$crm = $this->crm->setApi($this->api);
		$this->assertEquals($crm, $this->crm);
	}

	public function testSuccessfulCreateCustomer()
	{
		$this->api->expects($this->once())
                  ->method('addCustomer')
                  ->will($this->returnValue(array(
					    "code" => 1,
					    "result" => "accepted",
					    "reason" => "Customer created",
					    "q_cust_guid" => "10EBC305D84CEEA185257AEE0078678E"
                  )));

		$response = $this->crm->createCustomer($this->generateCommerceCustomer(), $this->generateCommerceOrder());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer was not created!
	 */
	public function testExceptionForCreateCustomer()
	{
		$this->api->expects($this->once())
                  ->method('addCustomer')
                  ->will($this->returnValue(array(
					    "code" => rand(2, 3),
					    "result" => "failed",
					    "reason" => "Customer was not created!",
                  )));

		$this->crm->createCustomer($this->generateCommerceCustomer(), $this->generateCommerceOrder());
	}

	public function testSuccessfulGetCustomer()
	{
		$this->api->expects($this->once())
                  ->method('getCustomer')
                  ->will($this->returnValue(array(
					    "code" => "1",
					    "result" => "accepted",
					    "reason" => "Customer record returned",
					    "q_cust_guid" => "06240358E0497EE385257AEE0079FDA4",
					    "q_cust_status" => "ACTIVE",
					    "q_cust_first_name" => "BOB",
					    "q_cust_middle_name" => "",
					    "q_cust_last_name" => "LOBLAW",
					    "q_cust_ship_address1" => "1234 STREET LANE",
					    "q_cust_ship_address2" => "",
					    "q_cust_ship_address3" => "",
					    "q_cust_ship_city" => "SAN DIEGO",
					    "q_cust_ship_state" => "CA",
					    "q_cust_ship_zip" => "92109",
					    "q_cust_ship_country" => "US",
					    "q_cust_bill_address1" => "1234 STREET LANE",
					    "q_cust_bill_address2" => "",
					    "q_cust_bill_address3" => "",
					    "q_cust_bill_city" => "SAN DIEGO",
					    "q_cust_bill_state" => "CA",
					    "q_cust_bill_zip" => "92109",
					    "q_cust_bill_country" => "US",
					    "q_cust_email" => "BOB@LOBLAW.COM",
					    "q_cust_phone" => "8581234567",
					    "q_cust_webusername" => "BOB@LOBLAW.COM",
					    "q_cust_webpassword" => "tester",
					    "q_cust_webstatus" => "",
					    "q_cust_category" => "",
					    "q_cust_order" => "20130109",
					    "q_cust_sold" => "TEST WIDGET 1",
					    "q_cust_imaddress" => "",
					    "q_cust_extid" => "",
					    "q_cust_record" => "192.168.3.142",
					    "q_cust_lastproc" => "20130109",
					    "q_cust_billplan" => "12 Month Membership - 58.32",
					    "q_cust_affiliatecode" => "TEST_AFFILIATE",
					    "q_cust_affiliatesub" => "ORANGE_TEST",
					    "q_cust_salespage" => "45:632",
					    "q_cust_cost" => "0",
					    "q_cust_acctcode" => "",
					    "q_cust_acctcodedesc" => "",
					    "q_cust_customfield1" => "",
					    "q_cust_customvalue1" => "",
					    "q_cust_customfield2" => "",
					    "q_cust_customvalue2" => "",
					    "q_cust_customfield3" => "",
					    "q_cust_customvalue3" => "",
					    "q_cust_customfield4" => "",
					    "q_cust_customvalue4" => "",
					    "q_cust_customfield5" => "",
					    "q_cust_customvalue5" => "",
					    "q_cust_created" => "20130109",
					    "q_cust_estbilldate" => "",
					    "q_cust_estbillamt" => "",
					    "q_cust_cycle" => "1",
					    "q_cust_cyclepaid" => "1",
					    "q_cust_logincnt" => "0",
					    "q_cust_ordercnt" => "0",
					    "q_cust_logincnt_date" => "" ,
					    "q_cust_ordercnt_date" => "",
					    "q_cust_program" => "InstantCheckMate",
					    "q_cust_prorgam_guid" => "E9BFE78BE083DD8A852578220083AE54",
                  		"q_cust_ccname" => "JOHN SMITH",
                  		"q_cust_ccacct" => "4116480559370132",
                  		"q_cust_ccexpire" => "0820",
                  		"q_cust_achname" => "",
                  		"q_cust_achacct" => "",
                  		"q_cust_achaba" => "",
                  		"q_cust_achbank" => "",
                  		"q_cust_achcity" => "",
                  		"q_cust_achstate" => "",
                  		"q_cust_achtype" => "CHECKING",
                  		"q_cust_bankroute" => "",
                  		"q_cust_bankrouteid" => "",
                  )));

        $customer = $this->generateCommerceCustomer(array('guid' => "06240358E0497EE385257AEE0079FDA4"));
        $customer->setCreditCard($this->generateCommerceCreditCard());
 		$customer->setGateway(new Icm_Commerce_Gateway_Nmi(new Icm_Service_Nmi_Api(Icm_Config::fromArray(array()))));

		$response = $this->crm->getCustomer($customer, $this->generateCommerceOrder());
		$this->assertTrue(is_a($response, "Icm_Commerce_Customer"));

		$this->assertEquals(strtoupper($response->first_name), strtoupper($customer->first_name));
		$this->assertEquals(strtoupper($response->last_name), strtoupper($customer->last_name));
		$this->assertEquals(strtoupper($response->email), strtoupper($customer->email));
		$this->assertEquals($response->zip, $customer->zip);
		$this->assertEquals($response->guid, $customer->guid);

		$this->assertEquals($response->getCreditCard()->number, $customer->getCreditCard()->number);
		$this->assertEquals($response->getCreditCard()->expiration_month, $customer->getCreditCard()->expiration_month);
		$this->assertEquals($response->getCreditCard()->expiration_year, $customer->getCreditCard()->expiration_year);
		$this->assertEquals(strtoupper($response->getCreditCard()->first_name), strtoupper($customer->getCreditCard()->first_name));
		$this->assertEquals(strtoupper($response->getCreditCard()->last_name), strtoupper($customer->getCreditCard()->last_name));

	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer was not created!
	 */
	public function testExceptionForGetCustomer()
	{
		$this->api->expects($this->once())
                  ->method('getCustomer')
                  ->will($this->returnValue(array(
					    "code" => rand(2, 3),
					    "result" => "failed",
					    "reason" => "Customer was not created!",
                  )));

		$this->crm->getCustomer($this->generateCommerceCustomer());
	}

	public function testSuccessfulUpdateCustomer()
	{
		$this->api->expects($this->once())
				  ->method('updateCustomer')
				  ->will($this->returnValue(array(
						"code" => 1,
						"result" => "accepted",
						"reason" => "Transaction created",
						"q_cust_guid" => "62F596F07DE2E36B85257AEF00024E0A"
				  )));

		$response = $this->crm->updateCustomer($this->generateCommerceCustomer());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer was not updated!
	 */
	public function testExceptionForUpdateCustomer()
	{
		$this->api->expects($this->once())
                  ->method('updateCustomer')
                  ->will($this->returnValue(array(
				    	"code" => rand(2, 3),
				    	"result" => "failed",
				    	"reason" => "Customer was not updated!",
                  )));

		$this->crm->updateCustomer($this->generateCommerceCustomer());
	}

	public function testSuccessfulValidateCustomer()
	{
		$this->api->expects($this->once())
				  ->method('getCustomer')
				  ->will($this->returnValue(array(
				    	"code" => 1,
				    	"result" => "accepted",
				    	"reason" => "Customer record returned",
				    	"q_cust_webpassword" => "TESTER",
				    	"q_cust_status" => "active"
				  )));

		$response = $this->crm->validateCustomer($this->generateCommerceCustomer());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_AuthenticationError
	 * @expectedExceptionMessage Invalid user credentials provided.
	 */
	public function testExceptionForValidateCustomer()
	{
		$this->api->expects($this->once())
                  ->method('getCustomer')
                  ->will($this->returnValue(array(
				    	"code" => 1,
				    	"result" => "accepted",
				    	"reason" => "Customer record returned",
                  		"q_cust_password" => "abcdef",
                  )));

		$this->crm->validateCustomer($this->generateCommerceCustomer());
	}

	public function testAddTransaction()
	{
		$this->api->expects($this->once())
				  ->method('addTransaction')
				  ->will($this->returnValue(array(
						"code" => 1,
						"result" => "accepted",
						"reason" => "Transaction created",
						"q_cust_guid" => "1EFF9E92344C6EF285257AEF0002096B"
				  )));

		$response = $this->crm->addTransaction($this->generateCommerceCustomer(), $this->generateCommerceTransaction(), $this->generateCommerceProductGateway());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Transaction could not be created!
	 */
	public function testExceptionForAddTransaction()
	{
		$this->api->expects($this->once())
                  ->method('addTransaction')
                  ->will($this->returnValue(array(
				    	"code" => rand(2, 3),
				    	"result" => "failed",
				    	"reason" => "Transaction could not be created!",
                  )));

		$this->crm->addTransaction($this->generateCommerceCustomer(), $this->generateCommerceTransaction(), $this->generateCommerceProductGateway());
	}

	public function testRefundCustomer()
	{
		$this->api->expects($this->once())
				  ->method('refundCustomer')
				  ->will($this->returnValue(array(
						"code" => 1,
						"result" => "accepted",
						"reason" => "Customer Refunded",
						"q_cust_guid" => "79FA0CC2DA7F3EA085257AEF00028CF7"
				  )));

		$response = $this->crm->refund($this->generateCommerceCustomer(), $this->generateCommerceTransaction(), $this->generateCommerceProductGateway());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer could not be refunded!
	 */
	public function testExceptionForRefundingCustomer()
	{
		$this->api->expects($this->once())
                  ->method('refundCustomer')
                  ->will($this->returnValue(array(
				    	"code" => rand(2, 3),
				    	"result" => "failed",
				    	"reason" => "Customer could not be refunded!",
                  )));

		$this->crm->refund($this->generateCommerceCustomer(), $this->generateCommerceTransaction(), $this->generateCommerceProductGateway());
	}

	public function testCancelCustomer()
	{
		$this->api->expects($this->once())
				  ->method('cancelCustomer')
				  ->will($this->returnValue(array(
						"code" => 1,
						"result" => "accepted",
						"reason" => "Customer record updated",
						"q_cust_guid" => "79FA0CC2DA7F3EA085257AEF00028CF7"
				  )));

		$response = $this->crm->cancelCustomer($this->generateCommerceCustomer());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer could not be updated!
	 */
	public function testExceptionForCancelCustomer()
	{
		$this->api->expects($this->once())
                  ->method('cancelCustomer')
                  ->will($this->returnValue(array(
				    	"code" => rand(2, 3),
				    	"result" => "failed",
				    	"reason" => "Customer could not be updated!",
                  )));

		$this->crm->cancelCustomer($this->generateCommerceCustomer());
	}

	public function testArchiveCustomer()
	{
		$this->api->expects($this->once())
				  ->method('archiveCustomer')
				  ->will($this->returnValue(array(
						"code" => 1,
						"result" => "accepted",
						"reason" => "Customer record updated",
						"q_cust_guid" => "79FA0CC2DA7F3EA085257AEF00028CF7"
				  )));

		$response = $this->crm->archiveCustomer($this->generateCommerceCustomer());
		$this->assertTrue(is_a($response, "Icm_Commerce_Crm_Response"));
	}

	/**
	 * @expectedException Icm_Commerce_Crm_Exception
	 * @expectedExceptionMessage Customer could not be updated!
	 */
	public function testExceptionForArchiveCustomer()
	{
		$this->api->expects($this->once())
                  ->method('archiveCustomer')
                  ->will($this->returnValue(array(
				    	"code" => rand(2, 3),
				    	"result" => "failed",
				    	"reason" => "Customer could not be updated!",
                  )));

		$this->crm->archiveCustomer($this->generateCommerceCustomer());
	}

}


