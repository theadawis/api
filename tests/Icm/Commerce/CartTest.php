<?php
/**
 * Cart.php
 * User: chris
 * Date: 1/9/13
 * Time: 10:30 AM
 */
class Icm_Commerce_CartTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Commerce_Order
     */
    public $order;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $crm;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $gateway;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $orderProductStorage;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $orderStorage;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $transactionStorage;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $pdo;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $customerStorage;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    public $productGatewayStorage;

    public function setUp() {
        parent::setUp();

        $this->order = new Icm_Commerce_Order();
        $this->order->order_id;
        $this->order->app_id = 1;
        $this->order->customer_id = 123;

        $this->crm = $this->getMock("Icm_Commerce_Crm_Orange", array(), array(), "", false);
        $this->gateway = $this->getMock("Icm_Commerce_Gateway_Litle", array(), array(), "", false);
        $this->orderProductStorage = $this->getMock("Icm_Commerce_Order_Product_StorageAdapter", array(), array(), "", false);
        $this->orderStorage = $this->getMock("Icm_Commerce_Order_StorageAdapter", array(), array(), "", false);
        $this->transactionStorage = $this->getMock("Icm_Commerce_Transaction_StorageAdapter", array(), array(), "", false);
        $this->customerStorage = $this->getMock("Icm_Commerce_Customer_StorageAdapter", array(), array(), "", false);
        $this->productGatewayStorage = $this->getMock("Icm_Commerce_Product_Gateway_StorageAdapter", array(), array(), "", false);
        $this->pdo = $this->getMock("Icm_Db_Pdo", array(), array(), "", false);
        $this->creditCard = $this->getMock('Icm_Commerce_Creditcard', array(), array(), '', false);
    }

    public function testCreateCart() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $this->assertInstanceOf("Icm_Commerce_Cart", $cart);
    }

    public function testCheckoutSuccessfulProcess() {
        $addCustomerResponse    = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse        = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse        = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway         = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->once())->method("createCustomer")->will($this->returnValue($addCustomerResponse));
        $this->crm->expects($this->once())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->customerStorage->expects($this->atLeastOnce())->method("save")->will($this->returnValue(null));
        $this->orderProductStorage->expects($this->atLeastOnce())->method("save");
        $this->orderStorage->expects($this->atLeastOnce())->method("save");

        $this->productGatewayStorage->expects($this->atLeastOnce())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->once())->method("getGateway")->will($this->throwException(new Exception()));
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));

        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));

        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )));
        $cart->checkout($customer);
    }

    public function testCheckoutPdfUpsellSuccessfulProcess() {
        $addCustomerResponse    = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse        = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse        = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway         = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->once())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->customerStorage->expects($this->atLeastOnce())->method("save")->will($this->returnValue(null));
        $this->orderProductStorage->expects($this->atLeastOnce())->method("save");
        $this->orderStorage->expects($this->atLeastOnce())->method("save");

        $this->productGatewayStorage->expects($this->atLeastOnce())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));

        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 89
        )));
        $cart->checkoutUpsell($customer);
    }

    public function testPreferredGatewayIsChosen() {
        $addCustomerResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->once())->method("createCustomer")->will($this->returnValue($addCustomerResponse));
        $this->crm->expects($this->once())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->customerStorage->expects($this->atLeastOnce())->method("save")->will($this->returnValue(null));
        $this->orderProductStorage->expects($this->atLeastOnce())->method("save");
        $this->orderStorage->expects($this->atLeastOnce())->method("save");

        $this->productGatewayStorage->expects($this->atLeastOnce())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->once())->method("getGateway")->will($this->returnValue($this->gateway));

        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));
        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )));
        $cart->checkout($customer);

        $this->assertEquals($this->gateway, $cart->getGateway());
        $this->assertEquals($cart->getGateway(), $cart->getDecidedGateway());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Quantity must be a positive number
     */
    public function testThrowsExceptionOnInvalidQuantity() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )), -1);
    }

    public function testSetQuantity() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $product = Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        ));
        $cart->addProduct($product);
        $cart->setQuantity($product, 5);
        $this->assertEquals($cart->getQuantity($product), 5);
    }

    public function testHasProducts() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $this->assertFalse($cart->hasProducts());
        $product = Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        ));
        $cart->addProduct($product);
        $this->assertTrue($cart->hasProducts());
    }

    public function testClearCart() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $this->assertFalse($cart->hasProducts());
        $product = Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        ));
        $cart->addProduct($product);
        $this->assertTrue($cart->hasProducts());
        $cart->emptyCart();
        $this->assertFalse($cart->hasProducts());
    }

    public function testGetters() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $this->assertEquals($cart->getCustomerStorage(), $this->customerStorage);
        $this->assertEquals($cart->getProductGateway(), $this->productGatewayStorage);
        $this->assertEquals($cart->getTransactionStorage(), $this->transactionStorage);
        $this->assertEquals($cart->getOrderStorage(), $this->orderStorage);
        $this->assertEquals($cart->getOrderProductStorage(), $this->orderProductStorage);

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);

        $this->assertInstanceOf("Icm_Commerce_Customer_StorageAdapter", $cart->getCustomerStorage());
        $this->assertInstanceOf("Icm_Commerce_Product_Gateway_StorageAdapter", $cart->getProductGateway());
        $this->assertInstanceOf("Icm_Commerce_Transaction_StorageAdapter", $cart->getTransactionStorage());
        $this->assertInstanceOf("Icm_Commerce_Order_StorageAdapter", $cart->getOrderStorage());
        $this->assertInstanceOf("Icm_Commerce_Order_Product_StorageAdapter", $cart->getOrderProductStorage());
    }

    /**
     * @expectedException Icm_Commerce_Exception
     */
    public function testNullTransactionThrowsException() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $reflection = new ReflectionClass($cart);
        $method = $reflection->getMethod("getTransaction");
        $method->setAccessible(true);
        $method->invoke($cart);
    }

    public function testSetProductClearsCart() {
        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $product = Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        ));

        $product2 = Icm_Commerce_Product::fromArray(array(
            'product_id' => 567
        ));

        $cart->addProduct($product);
        $cart->setProduct($product2);

        $products = $cart->getProducts()->getKeySet();

        $this->assertCount(1, $products);
        $this->assertInstanceOf("Icm_Commerce_Product", $products[0]);
        $this->assertEquals(567, $products[0]->product_id);
    }

    /**
     * @expectedException Icm_Commerce_Crm_Exception
     * @expectedExceptionMessage TestCRMFail
     */
    public function testTransactionRefundedIfAddCustomerToCrmFails() {
        $addCustomerResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->returnValue($gatewayResponse));
        $this->gateway->expects($this->once())->method("refund")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->once())->method("createCustomer")->will($this->throwException(new Icm_Commerce_Crm_Exception("TestCRMFail")));
        $this->crm->expects($this->never())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->productGatewayStorage->expects($this->never())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->once())->method("getGateway")->will($this->throwException(new Exception()));
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));

        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )));
        $cart->checkout($customer);
    }

    /**
     * @expectedException Icm_Commerce_Crm_Exception
     * @expectedExceptionMessage TestCRMFail
     */
    public function testCrmCancelsAndArchivesIfAddTransactionFails() {
        $addCustomerResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->returnValue($gatewayResponse));
        $this->gateway->expects($this->once())->method("refund")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->once())->method("createCustomer")->will($this->returnValue($addCustomerResponse));
        $this->crm->expects($this->once())->method("addTransaction")->will($this->throwException(new Icm_Commerce_Crm_Exception("TestCRMFail")));
        $this->crm->expects($this->once())->method("cancelCustomer");
        $this->crm->expects($this->once())->method("archiveCustomer");

        $this->productGatewayStorage->expects($this->once())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->once())->method("getGateway")->will($this->throwException(new Exception()));
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));

        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )));
        $cart->checkout($customer);
    }

    /**
     * @expectedException Icm_Commerce_Gateway_Exception
     * @expectedExceptionMessage TestGatewayFail
     */
    public function testTransactionIsSetToFailureIfFails() {
        $addCustomerResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->once())->method("process")->will($this->throwException(new Icm_Commerce_Gateway_Exception("TestGatewayFail")));
        $this->gateway->expects($this->never())->method("refund")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->never())->method("createCustomer")->will($this->throwException(new Icm_Commerce_Crm_Exception("TestCRMFail")));
        $this->crm->expects($this->never())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->productGatewayStorage->expects($this->never())->method("get")->will($this->returnValue($productGateway));
        $this->transactionStorage->expects($this->once())->method("save");

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->once())->method("getGateway")->will($this->throwException(new Exception()));
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->atLeastOnce())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->atLeastOnce())->method("getVisit")->will($this->returnValue($visit));
        $customer->expects($this->atLeastOnce())->method('getCreditCard')->will($this->returnValue($this->creditCard));

        $cart->addProduct(Icm_Commerce_Product::fromArray(array(
            'product_id' => 213
        )));
        $cart->checkout($customer);
    }

    /**
     * @expectedException Icm_Commerce_Exception
     */
    public function testThrowsExceptionOnNoProducts() {
        $addCustomerResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_cust_guid" => 12345));
        $addTranResponse = new Icm_Commerce_Crm_Response(1, 2, 3, array("q_tran_guid" => 5678));
        $gatewayResponse = new Icm_Commerce_Gateway_Response(9741, 1000, "success", "somemsg", "<litle/>");
        $productGateway = Icm_Commerce_Product_Gateway::fromArray(array(
            'token' => 'abc123'
        ));
        $this->gateway->expects($this->never())->method("process")->will($this->returnValue($gatewayResponse));
        $this->crm->expects($this->never())->method("createCustomer")->will($this->returnValue($addCustomerResponse));
        $this->crm->expects($this->never())->method("addTransaction")->will($this->returnValue($addTranResponse));

        $this->customerStorage->expects($this->never())->method("save")->will($this->returnValue(null));
        $this->orderProductStorage->expects($this->never())->method("save");
        $this->orderStorage->expects($this->never())->method("save");

        $this->productGatewayStorage->expects($this->never())->method("get")->will($this->returnValue($productGateway));

        $cart = new Icm_Commerce_Cart($this->order, $this->crm, $this->gateway, $this->pdo);
        $cart->setCustomerStorage($this->customerStorage);
        $cart->setGateway($this->gateway);
        $cart->setOrderProductStorage($this->orderProductStorage);
        $cart->setOrderStorage($this->orderStorage);
        $cart->setProductGateway($this->productGatewayStorage);
        $cart->setTransactionStorage($this->transactionStorage);

        $customer = $this->getMock("Icm_Commerce_Customer", array(), array(), "", false);
        $customer->expects($this->never())->method("getGateway")->will($this->throwException(new Exception()));
        $visit = $this->getMock("Icm_Struct_Tracking_Visit", array(), array(), "", false);
        $visit->expects($this->never())->method("getAffiliate")->will($this->returnValue(new Icm_Struct_Marketing_Affiliate()));
        $customer->expects($this->never())->method("getVisit")->will($this->returnValue($visit));

        $cart->checkout($customer);
    }
}
