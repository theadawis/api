<?php

class Icm_Commerce_Customer_Guids_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $customer_guid;

    public function setUp() {
        $this->conn = Icm_Db_Pdo::connect('cgDb',
                                          Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini',
                                          'cgDb'));
        $this->adapter = new Icm_Commerce_Customer_Guids_StorageAdapter('customer_guids',
                                                                        'customer_guid_id',
                                                                        $this->conn);
        parent::setUp();
    }

    public function testInsert() {
        // setup defaults
        $customerGuid = 'testingGuid';
        $customerId = 1337;
        $productId = 1337;
        $orderId = 1337;
        $orderProductId = 1337;
        $appId = 1;

        // delete any leftover data
        $this->conn->execute("DELETE FROM customer_guids WHERE order_product_id = ?", array($orderProductId));
        $this->deleteTestOrderProduct();
        $this->deleteTestProduct();
        $this->deleteTestOrder();
        $this->deleteTestCustomer();

        // create new customer without guid
        $customerParams = array(
            'email' => 'tester@email.com',
            'first_name' => 'John',
        );
        $customer = Icm_Commerce_Customer::fromArray($customerParams);

        // create new order product
        $orderProductParams = array(
            'order_product_id' => $orderProductId,
        );
        $orderProduct = Icm_Commerce_Order_Product::fromArray($orderProductParams);

        // try to save the guid
        $this->adapter->insert($customer, $orderProduct);

        // should not have written anything to the db
        $guids = $this->conn->fetch('SELECT * FROM customer_guids WHERE order_product_id = ' . $orderProductId);
        $this->assertEmpty($guids);

        // create new customer with guid
        $customerId = $this->writeTestCustomer($customerParams['email'], $customerParams['first_name']);
        $customerParams['guid'] = $customerGuid;
        $customer = Icm_Commerce_Customer::fromArray($customerParams);

        // create order product without order product id
        $orderProductParams = array();
        $orderProduct = Icm_Commerce_Order_Product::fromArray($orderProductParams);

        // try to save the guid
        $this->adapter->insert($customer, $orderProduct);

        // should not have written anything to the db
        $guids = $this->conn->fetchAll("SELECT * FROM customer_guids WHERE guid = '" . $customerGuid . "'");
        $this->assertEmpty($guids);

        // create new order product with order product id
        $orderId = $this->writeTestOrder($customerId);
        $productId = $this->writeTestProduct();
        $orderProductId = $this->writeTestOrderProduct($productId, $customerId, $orderId);

        $orderProductParams = array(
            'order_product_id' => $orderProductId,
        );
        $orderProduct = Icm_Commerce_Order_Product::fromArray($orderProductParams);

        // save the guid
        $this->adapter->insert($customer, $orderProduct);

        // should have written matching row to the db
        $guids = $this->conn->fetchAll("SELECT * FROM customer_guids WHERE guid = '" . $customerGuid . "' AND order_product_id = " . $orderProductId);
        $this->assertNotEmpty($guids);
        $this->assertCount(1, $guids);
        $this->cleanupStmts = array();

        return array(
            'customer' => $customer,
            'customerId' => $customerId,
            'guids' => $guids,
            'customerGuid' => $customerGuid,
            'orderProductId' => $orderProductId,
            'productId' => $productId,
            'orderId' => $orderId,
        );
    }

    /**
     * @depends testInsert
     */
    public function testGetGuidsForCustomer($insertedData) {
        // pull out info from the inserted data
        extract($insertedData);

        // pull out all the guids for the customer
        $customer->customer_id = $customerId;
        $savedGuids = $this->adapter->getGuidsForCustomer($customer);

        // should have pulled out the guid
        $this->assertCount(1, $savedGuids);
        $this->assertEquals($guids[0]['guid'], $savedGuids[0]['guid']);

        // try pulling out guids for a customer with unsaved customer id
        $customerParams = array(
            'customer_id' => 135643,
            'email' => 'testeriffic@testerly.com',
            'first_name' => 'first',
        );
        $secondCustomer = Icm_Commerce_Customer::fromArray($customerParams);
        $secondGuids = $this->adapter->getGuidsForCustomer($secondCustomer);

        // should have returned nothing
        $this->assertCount(0, $secondGuids);

        // cleanup db
        $params = array(
            $customerGuid,
            $orderProductId
        );
        $this->conn->execute("DELETE FROM customer_guids WHERE guid = ? AND order_product_id = ?", $params);
        $this->conn->execute("DELETE FROM order_products WHERE order_product_id = " . $orderProductId);
        $this->conn->execute("DELETE FROM products WHERE product_id = " . $productId);
        $this->conn->execute("DELETE FROM orders WHERE order_id = " . $orderId);
        $this->conn->execute("DELETE FROM customer WHERE customer_id = " . $customer->customer_id);
    }
}
