<?php
/**
 * Factory.php
 * User: chris
 * Date: 1/10/13
 * Time: 9:54 AM
 */
class Icm_Commerce_Customer_FactoryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $customerStorageAdapter;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $gatewayStorageAdapter;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $visitStorageAdapter;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $affiliateStorageAdapter;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $tracker;


    public function setUp() {
        $this->customerStorageAdapter = $this->getMock("Icm_Commerce_Customer_StorageAdapter", array(), array(), "", false);
        $this->gatewayStorageAdapter = $this->getMock("Icm_Commerce_Gateway_StorageAdapter", array(), array(), "", false);
        $this->tracker = $this->getMock("Icm_SplitTest_Tracker", array(), array(), "", false);
        $this->affiliateStorageAdapter = $this->getMock("Icm_Service_Internal_Marketing_Affiliate", array(), array(), "", false);
        $this->visitTracker = $this->getMock('Icm_Visit_Tracker', array(), array(), "", false);
        $this->registrationSectionId = 5;
        $this->variationId = 5;
    }

    public function testCreate() {
        $factory = new Icm_Commerce_Customer_Factory(
            $this->customerStorageAdapter,
            $this->gatewayStorageAdapter,
            $this->visitTracker,
            $this->affiliateStorageAdapter,
            $this->tracker,
            $this->registrationSectionId
        );
        $this->assertInstanceOf("Icm_Commerce_Customer_Factory", $factory);
    }

    public function testGetCustomer() {
        $this->customerStorageAdapter->expects($this->once())->method("getById")->will($this->returnValue(
            Icm_Commerce_Customer::fromArray(array(
                "customer_id" => 123
            ))
        ));

        $this->affiliateStorageAdapter->expects($this->once())->method("getById")->will($this->returnValue(Icm_Struct_Marketing_Affiliate::fromArray(
            array(
                "short_name" => "foo_bar"
            ))
        ));

        $this->visitTracker->expects($this->once())->method("getVisitData")->will($this->returnValue(array('visit_id' => 'testingvisittrackervisit')));

        $this->tracker->expects($this->once())->method('getTestContext')->will($this->returnValue(array(
            $this->registrationSectionId => array('variationId' => $this->variationId)
        )));

        $factory = new Icm_Commerce_Customer_Factory(
            $this->customerStorageAdapter,
            $this->gatewayStorageAdapter,
            $this->visitTracker,
            $this->affiliateStorageAdapter,
            $this->tracker,
            $this->registrationSectionId
        );

        $customerId = 123;
        $customer = $factory->getCustomer($customerId);

        $this->assertInstanceOf("Icm_Commerce_Customer", $customer);
        $this->assertInstanceOf("Icm_Struct_Tracking_Visit", $customer->getVisit());
    }
}
