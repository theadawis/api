<?php

class Icm_Commerce_Customer_StorageAdapter_Test extends TestCase
{
    protected $conn;
    protected $csa;

    public function setUp()
    {
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->csa = new Icm_Commerce_Customer_StorageAdapter('customer', 'customer_id', $this->conn);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->csa, "Icm_Commerce_Customer_StorageAdapter"));
    }

    public function testGetByIdCreateNewCustomer()
    {
        $this->conn->expects($this->once())
             ->method('fetch')
             ->will($this->returnValue(array()));
        $result = $this->csa->getById(null);
        $this->assertTrue(is_a($result, 'Icm_Commerce_Customer'));
        $this->assertNull($result->customer_id);
    }

    public function testGetById()
    {
        $customerId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceCustomer(array('customer_id' => $customerId))->toArray()));

        $result = $this->csa->getById($customerId);

        $this->assertTrue(is_a($result, "Icm_Commerce_Customer"));
        $this->assertEquals($customerId, $result->customer_id);
    }

    public function testGetByVisitorId()
    {
        $customerId = rand(1, 1000000);
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceCustomer(array('customer_id' => $customerId))->toArray()));

        $result = $this->csa->getByVisitorId(rand(1, 1000000));

        $this->assertTrue(is_a($result, "Icm_Commerce_Customer"));
        $this->assertEquals($customerId, $result->customer_id);
    }

    public function testSaveWithInsert()
    {
        $customerId = rand(1, 1000000);
        $customer = $this->generateCommerceCustomer(array('customer_id' => null));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($customerId));

        $this->csa->save($customer);
        $this->assertEquals($customerId, $customer->customer_id);
    }

    public function testSaveWithUpdate()
    {
        $customerId = rand(1, 1000000);
        $customer = $this->generateCommerceCustomer(array('customer_id' => $customerId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->csa->save($customer);
        $this->assertEquals($customerId, $customer->customer_id);
    }

    public function testGetByEmail() {
        $testEmail = 'bob@loblaw.com';
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceCustomer(array('email' => $testEmail))->toArray()));

        $result = $this->csa->getByEmail($testEmail, 1);

        $this->assertTrue(is_a($result, "Icm_Commerce_Customer"));
        $this->assertEquals($testEmail, $result->email);
    }

    public function testGetByForgotPasswordHash() {
        $testHash = '12341234123412341234123412341234';
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($this->generateCommerceCustomer(array('forgot_password_hash' => $testHash))->toArray()));

        $result = $this->csa->getByForgotPasswordHash($testHash);

        $this->assertTrue(is_a($result, "Icm_Commerce_Customer"));
        $this->assertEquals($testHash, $result->forgot_password_hash);
    }

    public function testGetByForgotPasswordHashCustomerNotFound() {
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue(array()));

        $result = $this->csa->getByForgotPasswordHash('badhash');

        $this->assertTrue(is_a($result, "Icm_Commerce_Customer"));
        $this->assertEquals(null, $result->customer_id);
    }

    public function testSetForgotPasswordHash() {
        $testHash = '12341234123412341234123412341234';
        $customerId = rand(1, 1000000);
        $customer = $this->generateCommerceCustomer(array('customer_id' => $customerId));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $result = $this->csa->setForgotPasswordHash($customer);
        $this->assertTrue($result);
    }

    public function testSetForgotPasswordHashCustomerNotFound() {
        $testHash = '12341234123412341234123412341234';
        $customerId = rand(1, 1000000);
        $customer = $this->generateCommerceCustomer(array('customer_id' => $customerId,'forgot_password_hash'=>null));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $result = $this->csa->setForgotPasswordHash($customer);
        $this->assertTrue($result);
    }

    public function testGetByEmailCreateNewCustomer()
    {
        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue(array()));

        $result = $this->csa->getByEmail(null, 1);

        $this->assertTrue(is_a($result, 'Icm_Commerce_Customer'));
        $this->assertNull($result->email);
    }
}
