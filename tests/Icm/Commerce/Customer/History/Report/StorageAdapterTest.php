<?php

class Icm_Commerce_Customer_History_Report_StorageAdapter_Test extends TestCase
{
    public function setUp() {
        parent::setUp();

        $this->adapter = new Icm_Commerce_Customer_History_Report_StorageAdapter('report_history', 'report_history_id', $this->cgDb);
    }

    public function testGetByCustomer() {
        // cleanup test data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
        $this->deleteTestCustomer();

        // call with customer with blank customer id
        $customer = Icm_Commerce_Customer::fromArray(array());
        $history = $this->adapter->getByCustomer($customer);

        // should return empty array
        $this->assertEmpty($history);
        $this->assertInternalType('array', $history);

        // create a test customer
        $customerId = $this->writeTestCustomer();
        $customer->customer_id = $customerId;

        // save two report history rows for them
        $params = array(
            1337,
            'john',
            'smith',
            'testreportkey',
            1,
            $customer->customer_id
        );
        $this->cgDb->execute("INSERT INTO report_history (report_history_id, first_name, last_name, report_key, report_type_id, customer_id) VALUES (?, ?, ?, ?, ?, ?)", $params);

        sleep(5);

        $params = array(
            1338,
            'mark',
            'smith',
            'testreportkey2',
            1,
            $customer->customer_id
        );
        $this->cgDb->execute("INSERT INTO report_history (report_history_id, first_name, last_name, report_key, report_type_id, customer_id) VALUES (?, ?, ?, ?, ?, ?)", $params);

        // pull back by customer id
        $history = $this->adapter->getByCustomer($customer);

        // should return both rows
        $this->assertCount(2, $history);
        $this->assertContainsOnlyInstancesOf('Icm_Commerce_Customer_History_Report_Struct', $history);

        // data should match
        foreach ($history as $reportHistory) {
            $this->assertEquals('smith', $reportHistory->last_name);
            $this->assertEquals($customer->customer_id, $reportHistory->customer_id);
            $this->assertStringStartsWith('testreportkey', $reportHistory->report_key);
        }

        // should have returned them in reverse order
        $this->assertGreaterThan($history[1]->updated, $history[0]->updated);

        // cleanup the data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
    }

    public function testSave() {
        // cleanup test data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
        $this->deleteTestCustomer();

        // create new customer
        $customerId = $this->writeTestCustomer();

        // generate report history struct
        $params = array(
            'first_name' => 'John',
            'last_name' => 'Smith',
            'customer_id' => $customerId,
            'report_key' => 'testreportkey',
            'report_type_id' => 1,
        );
        $reportHistory = Icm_Commerce_Customer_History_Report_Struct::fromArray($params);

        // save to db
        $reportHistoryId = $this->adapter->save($reportHistory);

        // should have returned the id of the new row
        $this->assertInternalType('string', $reportHistoryId);

        // should have written it to the db
        $history = $this->cgDb->fetchAll("SELECT * FROM report_history WHERE report_key = 'testreportkey'");
        $this->assertCount(1, $history);
        $this->assertEquals($reportHistoryId, $history[0]['report_history_id']);
        $this->assertEquals($customerId, $history[0]['customer_id']);

        // try saving same struct again
        $secondReportHistoryId = $this->adapter->save($reportHistory);

        // should have returned the same id
        $this->assertEquals($reportHistoryId, $secondReportHistoryId);

        // should have incremented the report views instead of writing new row
        $history = $this->cgDb->fetchAll("SELECT * FROM report_history WHERE report_key = 'testreportkey'");
        $this->assertCount(1, $history);
        $this->assertEquals($reportHistoryId, $history[0]['report_history_id']);
        $this->assertEquals(2, $history[0]['view_count']);

        // change report key and re-save
        $reportHistory['report_key'] = 'testreportkey2';
        $thirdReportHistoryId = $this->adapter->save($reportHistory);

        // should have returned new id
        $this->assertNotEquals($reportHistoryId, $thirdReportHistoryId);

        // should have written new row to the db
        $history = $this->cgDb->fetchAll("SELECT * FROM report_history WHERE report_key = 'testreportkey2'");
        $this->assertCount(1, $history);
        $this->assertEquals($thirdReportHistoryId, $history[0]['report_history_id']);
        $this->assertEquals(1, $history[0]['view_count']);
        $this->assertEquals($customerId, $history[0]['customer_id']);

        // cleanup test data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
    }

    public function testGetDistinctMonthlyViews() {
        // cleanup test data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
        $this->deleteTestCustomer();

        // create new customer
        $customerId = $this->writeTestCustomer();

        // create recent report history view for customer
        $reportTypeId = 1;
        $params = array(
            1337,
            'john',
            'smith',
            'testreportkey',
            $reportTypeId,
            $customerId
        );
        $this->cgDb->execute("INSERT INTO report_history (report_history_id, first_name, last_name, report_key, report_type_id, customer_id) VALUES (?, ?, ?, ?, ?, ?)", $params);

        // create old report history view for the customer
        $params = array(
            1338,
            'john',
            'smith',
            'testreportkey2',
            $reportTypeId,
            $customerId
        );
        $this->cgDb->execute("INSERT INTO report_history (report_history_id, first_name, last_name, report_key, report_type_id, customer_id) VALUES (?, ?, ?, ?, ?, ?)", $params);
        $oldCreated = '2013-01-04 00:00:00';
        $this->cgDb->execute("UPDATE report_history SET created = ? WHERE report_history_id = 1338", array($oldCreated));

        // fetch distinct views
        $customer = Icm_Commerce_Customer::fromArray(array('customer_id' => $customerId));
        $historyCount = $this->adapter->getDistinctMonthlyViews($reportTypeId, $customer);

        // should have returned just the recent one
        $this->assertEquals(1, $historyCount);

        // create second view of the recent report
        $this->cgDb->execute("UPDATE report_history SET view_count = 2 WHERE report_key = 'testreportkey'");

        // pull monthly views
        $historyCount = $this->adapter->getDistinctMonthlyViews($reportTypeId, $customer);

        // should still have the first report history
        $this->assertEquals(1, $historyCount);

        // create second report history view in recent past
        $params = array(
            1339,
            'mark',
            'smith',
            'testreportkey3',
            $reportTypeId,
            $customerId
        );
        $this->cgDb->execute("INSERT INTO report_history (report_history_id, first_name, last_name, report_key, report_type_id, customer_id) VALUES (?, ?, ?, ?, ?, ?)", $params);

        // pull monthly views
        $historyCount = $this->adapter->getDistinctMonthlyViews($reportTypeId, $customer);

        // should have pulled first and third, but not second
        $this->assertEquals(2, $historyCount);

        // cleanup test data
        $this->cgDb->execute("DELETE FROM report_history WHERE report_key LIKE 'testreportkey%'");
    }
}