<?php

class Icm_Commerce_Order_Test extends TestCase
{
    /**
     * @var Icm_Commerce_Order
     */
    public $order;

    public function setUp()
    {
        $this->order = new Icm_Commerce_Order();
    }

    public function testCreate()
    {
    	$this->assertTrue(is_a($this->order, "Icm_Commerce_Order"));
    }

    public function testAddProduct()
    {
    	$product = $this->generateCommerceProduct();
    	$this->order->addProduct($product, 1);

    	$products = $this->order->getProducts();
    	$productsArray = $products->getKeySet();

    	$this->assertEquals($productsArray[0], $product);
    }

    public function testAddTwoOfTheSameProduct()
    {
    	$this->order->addProduct($this->generateCommerceProduct(), 2);
    	$this->order->addProduct($this->generateCommerceProduct(), 1);

    	$products = $this->order->getProducts();
    	$productsArray = $products->getKeySet();

    	$this->assertEquals(3, $products[$productsArray[0]]);
    }

    public function testHasProduct()
    {
    	$this->assertFalse($this->order->hasProducts());

    	$product = $this->generateCommerceProduct();
    	$this->order->addProduct($product, 1);
    	$this->assertTrue($this->order->hasProducts());

    	$this->order->addProduct($product, 1);
    	$this->assertTrue($this->order->hasProducts());
    }

	public function testGetQuantityNoProducts()
	{
    	$product = $this->generateCommerceProduct();
		$this->order->getQuantity($product);
	}

    public function testGetQuantity()
    {
    	$total = rand(1, 2000);
    	$product = $this->generateCommerceProduct();

    	$this->order->addProduct($product, $total);
    	$this->order->addProduct($this->generateCommerceProduct(array('product_id' => 99999999999)), 1);
    	$this->assertEquals($total, $this->order->getQuantity($product));
    }

    public function testSetQuantity()
    {
    	$total = rand(10, 2000);
    	$product = $this->generateCommerceProduct();

    	$this->order->addProduct($product, $total);
    	$this->order->setQuantity($product, $total - 1);
    	$this->assertEquals($total - 1, $this->order->getQuantity($product));
    }

    public function testGetProducts()
    {
    	$products = array(
    		$this->generateCommerceProduct(array('product_id' => 1234)),
    		$this->generateCommerceProduct(array('product_id' => 1235))
    	);

    	foreach ($products as $product) {
    		$this->order->addProduct($product, 1);
    	}

    	$orderProducts = $this->order->getProducts();

    	$i = 0;
    	foreach ($orderProducts as $orderProduct) {
    		$this->assertEquals($orderProduct[0], $products[$i]);
    		$i++;
    	}
    }

    public function testGetSubtotal()
    {
    	$totalQuanity1 = rand(10, 2000);
    	$totalQuanity2 = rand(10, 2000);
    	$product1 = $this->generateCommerceProduct(array('product_id' => '1234', 'initial_price' => 10.99));
    	$product2 = $this->generateCommerceProduct(array('product_id' => '1235', 'initial_price' => 6.99));

    	$this->order->addProduct($product1, $totalQuanity1);
    	$this->order->addProduct($product2, $totalQuanity2);

    	$expectedSubtotal = ($product1->initial_price * $totalQuanity1 + $product2->initial_price * $totalQuanity2);
    	$this->assertEquals($expectedSubtotal, $this->order->getSubtotal());
    }

    public function testRemoveAllProducts()
    {
    	$this->order->addProduct($this->generateCommerceProduct(array('product_id' => '1234')), rand(10, 2000));
    	$this->order->addProduct($this->generateCommerceProduct(array('product_id' => '1235')), rand(10, 2000));

    	$this->order->removeAllProducts();
    	$this->assertCount(0, $this->order->getProducts());
    }

    public function testGetOrderId()
    {
    	$orderId = 1234;
    	$this->order->order_id = $orderId;
    	$this->assertEquals($orderId, $this->order->getOrderId());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Quantity must be a positive number.
     */
    public function testInvalidQuantityNegative()
    {
    	$this->order->addProduct($this->generateCommerceProduct(array('product_id' => '1234')), -25);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Quantity must be a positive number.
     */
    public function testInvalidQuantityNonNumeric()
    {
    	$this->order->addProduct($this->generateCommerceProduct(array('product_id' => '1234')), 'abcd');
    }
}
