<?php

class Icm_Commerce_Creditcard_Test extends PHPUnit_Framework_TestCase
{
    /**
     * Creditcard object
     * @var Icm_Commerce_Creditcard
     */
    protected $cc;

    /**
     * Valid test credit card number
     * @var string
     */
    protected $testGoodCCNumberNew = '4222222222222';

    /**
     * Invalid credit card number
     * @var string
     */
    protected $testBadCCNumber  = '1234123412341234';

    /**
     * Known good credit card numbers by type
     * @var array
     */
    protected $testGoodCCNumbersByType = array(
            'visa' => '4119692004990226',
            'amex' => '378282246310005',
            'mastercard' => '5123695007103193',
            'discover' => '6011111111111117',
            'diners' => '30569309025904');

    /**
     * Values that will evaluate to empty()
     * @var array
     */
    protected $paramsEmpty = array('', 0, 0.0, "0", null, false, array());

    public function setup() {
        $this->cc = new Icm_Commerce_Creditcard;
    }

    public function testAttributesExist() {
        $this->assertClassHasAttribute('number', 'Icm_Commerce_Creditcard');
        $this->assertClassHasAttribute('cvv', 'Icm_Commerce_Creditcard');
        $this->assertClassHasAttribute('expiration_month', 'Icm_Commerce_Creditcard');
        $this->assertClassHasAttribute('expiration_year', 'Icm_Commerce_Creditcard');
        $this->assertClassHasAttribute('first_name', 'Icm_Commerce_Creditcard');
        $this->assertClassHasAttribute('last_name', 'Icm_Commerce_Creditcard');
    }

    public function testConstantsValues() {
        $this->assertEquals('visa', Icm_Commerce_Creditcard::CARD_TYPE_VISA);
        $this->assertEquals('mastercard', Icm_Commerce_Creditcard::CARD_TYPE_MASTERCARD);
        $this->assertEquals('amex', Icm_Commerce_Creditcard::CARD_TYPE_AMEX);
        $this->assertEquals('discover', Icm_Commerce_Creditcard::CARD_TYPE_DISCOVER);
        $this->assertEquals('diners', Icm_Commerce_Creditcard::CARD_TYPE_DINERS);
    }

    public function testTestCCsArray() {
        $this->assertClassHasAttribute('testCCs', 'Icm_Commerce_Creditcard');
    }

    public function testIsValidCCNumber() {
        $this->assertTrue($this->cc->isValidCCNumber($this->testGoodCCNumberNew));
    }

    public function testIsValidCCNumberBadParameterFalse() {
        // empty parameters
        foreach($this->paramsEmpty as $param) {
            $this->assertFalse($this->cc->isValidCCNumber($param));
        }

        $this->assertFalse($this->cc->isValidCCNumber($this->testBadCCNumber));

        // doing this to get a $current > 9
        $this->assertFalse($this->cc->isValidCCNumber('9999999999999999'));
    }

    public function testIsValidCCNumberNoParameterUseAttribute() {
        $this->cc->number = $this->testGoodCCNumberNew;
        $this->assertTrue($this->cc->isValidCCNumber());
    }

    public function testIsValidCCNumberNoParameterNoAttributeFalse() {
        $this->assertFalse($this->cc->isValidCCNumber());
    }

    public function testIsValidCCNumberNoParameterBadAttributeFalse() {
        // attribute not set
        $this->assertFalse($this->cc->isValidCCNumber());

        // attribute set to a bad CC number
        $this->cc->number = $this->testBadCCNumber;
        $this->assertFalse($this->cc->isValidCCNumber());

        // attribute set to 0 which is not valid
        foreach($this->paramsEmpty as $param) {
            $this->cc->number = $param;
            $this->assertFalse($this->cc->isValidCCNumber());
        }
    }

    public function testAddTestCCNumber() {
        $this->cc->addTestCCNumber($this->testGoodCCNumberNew);
    }

    public function testAddTestCCNumberException() {
        $this->setExpectedException('Exception');

        // empty parameters
        foreach($this->paramsEmpty as $param) {
            $this->assertFalse($this->cc->addTestCCNumber($param));
        }

        $this->cc->addTestCCNumber($this->testBadCCNumber);
    }

    public function testIsTestCCNumber() {
        $this->cc->addTestCCNumber($this->testGoodCCNumberNew);
        $this->assertTrue($this->cc->isTestCCNumber($this->testGoodCCNumberNew));
    }

    public function testIsTestCCNumberFalse() {
        // empty parameters
        foreach($this->paramsEmpty as $param) {
            $this->assertFalse($this->cc->isTestCCNumber($param));
        }
    }

    public function testGetCardType() {
        foreach($this->testGoodCCNumbersByType as $type=>$number) {
            $this->cc->number = $number;
            $this->assertEquals($type, $this->cc->getCardType());
        }
    }

    public function testGetCardTypeNoNumber() {
        $this->assertFalse($this->cc->getCardType());
    }

    public function testIsValidExpirationDate() {
        $this->cc->expiration_month = 12;
        $this->cc->expiration_year = 9999;
        $this->assertTrue($this->cc->isValidExpirationDate());
    }

    public function testIsValidExpirationDateFalse() {
        // no attributes
        $this->assertFalse($this->cc->isValidExpirationDate());

        // expired attributes
        $this->cc->expiration_month = 12;
        $this->cc->expiration_year = 2000;
        $this->assertFalse($this->cc->isValidExpirationDate());
    }

    public function testFormatExpirationDefaultFormat() {
        $this->cc->expiration_month = 12;
        $this->cc->expiration_year = 2014;
        $this->assertEquals('122014', $this->cc->formatExpiration());
    }

    public function testFormatExpirationSetFormat() {
        $this->cc->expiration_month = 12;
        $this->cc->expiration_year = 2014;
        $this->assertEquals('1214', $this->cc->formatExpiration('my'));
    }

    public function testValidate() {
        $this->cc->number = $this->testGoodCCNumberNew;
        $this->cc->validate();
    }

    public function testValidateException() {
        $this->setExpectedException('Exception');
        $this->cc->validate();
    }

    public function testGetNumber() {
        $this->cc->number = $this->testGoodCCNumberNew;
        $this->assertEquals($this->testGoodCCNumberNew, $this->cc->getNumber());
    }

    public function testGetFullName() {
        $this->cc->first_name = 'John';
        $this->cc->last_name = 'Smith';
        $this->assertEquals('John Smith', $this->cc->getFullName());
    }
}
