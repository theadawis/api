<?php

class Icm_Commerce_Gateway_Nmi_Test extends TestCase
{
    /**
     * @var Icm_Commerce_Gateway_Nmi
     */
    protected $gateway;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $api;

    protected function setUp()
    {
        $this->api = $this->getMock("Icm_Service_Nmi_Api", array(), array(), '', false);
        $this->gateway = new Icm_Commerce_Gateway_Nmi($this->api);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->gateway, "Icm_Commerce_Gateway_Nmi"));
    }

    public function testReportGroupCode()
    {
        $this->assertEquals('SLCT', $this->gateway->getReportGroupCode());
    }

    public function testSaleSuccessful()
    {
        $customer = $this->generateCommerceCustomer();
        $order = $this->generateCommerceOrder();

        $this->api->expects($this->once())
                  ->method('sale')
                  ->will($this->returnValue(array(
                    'response' => 1,
                    'response_code' => 100,
                    'transactionid' => 123,
                    'authcode' => 1,
                    'responsetext' => 'test'
                  )));

        $response = $this->gateway->process($customer, $order);
        $this->assertTrue(is_a($response, 'Icm_Commerce_Gateway_Response'));
    }

    public function testRefundSuccessful()
    {
        $customer = $this->generateCommerceCustomer();
        $transaction = $this->generateCommerceTransaction();

        $this->api->expects($this->once())
                  ->method('refund')
                  ->will($this->returnValue(array(
                    'response' => 1,
                    'response_code' => 100,
                    'transactionid' => 123,
                    'authcode' => 1,
                    'responsetext' => 'test'
                  )));

        $response = $this->gateway->refund($transaction, $customer, $transaction->amount);
        $this->assertTrue(is_a($response, 'Icm_Commerce_Gateway_Response'));
    }

    /**
     * @dataProvider getExceptionSets
     */
    public function testSaleExceptions($code, $class, $message)
    {
        $this->setExpectedException($class, $message);

        $customer = $this->generateCommerceCustomer();
        $order = $this->generateCommerceOrder();

        $this->api->expects($this->once())
                  ->method('sale')
                  ->with($customer, $order)
                  ->will($this->returnValue(array(
                      'response' => rand(2, 3),
                      'response_code' => $code,
                      'transactionid' => 123,
                      'authcode' => 1,
                      'responsetext' => $message
        )));

        $this->gateway->process($customer, $order);
    }

    /**
     * @dataProvider getExceptionSets
     */
    public function testRefundExceptions($code, $class, $message)
    {
        $this->setExpectedException($class, $message);

        $customer = $this->generateCommerceCustomer();
        $transaction = $this->generateCommerceTransaction();
        $amount = $transaction->amount;

        $this->api->expects($this->once())
                  ->method('refund')
                  ->with($transaction, $amount)
                  ->will($this->returnValue(array(
                    'response' => rand(2, 3),
                    'response_code' => $code,
                    'transactionid' => 123,
                    'authcode' => 1,
                    'responsetext' => $message
                  )));

        $this->gateway->refund($transaction, $customer, $amount);
    }

    public function getExceptionSets()
    {
        // FORMAT: code, exceptionClass, exceptionMessage
        return array(
            array('200', 'Icm_Commerce_Gateway_TransactionError', 'Transaction declined by processor.'),
            array('201', 'Icm_Commerce_Gateway_TransactionError', 'Do not honor.'),
            array('202', 'Icm_Commerce_Gateway_InsufficientFunds', 'Insufficient funds.'),
            array('203', 'Icm_Commerce_Gateway_TransactionError', 'Over limit.'),
            array('204', 'Icm_Commerce_Gateway_TransactionError', 'Transaction not allowed.'),
            array('221', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('222', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('223', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('224', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('225', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('250', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('251', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('252', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('253', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('260', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('261', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('262', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('263', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('264', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
            array('300', 'Icm_Commerce_Gateway_TransactionError', 'Transaction rejected by gateway.'),
            array('600', 'Icm_Commerce_Gateway_TransactionError', 'test123'),
        );
    }
}
