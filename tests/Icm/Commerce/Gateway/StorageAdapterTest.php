<?php

class Icm_Commerce_Gateway_StorageAdapter_Test extends TestCase
{
    protected $gatewayFactory;
    protected $conn;
    protected $gsa;

    public function setUp()
    {
        $this->gatewayFactory = $this->getMock("Icm_Commerce_Gateway_Factory", array(), array(), '', false);
        $this->conn = $this->getMock("Icm_Db_Pdo", array(), array(), '', false);
        $this->gsa = new Icm_Commerce_Gateway_StorageAdapter('gateways', 'gateway_id', $this->conn, $this->gatewayFactory);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->gsa, "Icm_Commerce_Gateway_StorageAdapter"));
    }

    public function testGetById()
    {
        $nmiApi = $this->getMock("Icm_Service_Nmi_Api", array(), array(), '', false);
        $gatewayId = rand(1, 1000000);
        $gateway = new Icm_Commerce_Gateway_Nmi($nmiApi);
        $gateway->gateway_id = $gatewayId;
        $gateway->short_name = "NMI";

        $this->gatewayFactory->expects($this->once())
                             ->method('nmi')
                             ->will($this->returnCallback(function($data) use ($nmiApi) {
                                 $gw = new Icm_Commerce_Gateway_Nmi($nmiApi);

                                 foreach($data as $k => $v){
                                     $gw->$k = $v;
                                 }

                                 return $gw;
        }));

        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($gateway->toArray()));

        $result = $this->gsa->getById($gatewayId);
        $this->assertInstanceOf('Icm_Commerce_Gateway_Abstract', $gateway);
        $this->assertEquals($gatewayId, $result->gateway_id);
    }

    public function testGetByIdNameNotFoundException()
    {
        $nmiApi = $this->getMock("Icm_Service_Nmi_Api", array(), array(), '', false);
        $gatewayId = rand(1, 1000000);
        $gateway = new Icm_Commerce_Gateway_Nmi($nmiApi);
        $gateway->gateway_id = $gatewayId;
        $gateway->short_name = "badname";

        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($gateway->toArray()));

        $this->setExpectedException('Icm_Commerce_Gateway_Exception', "No gateway could be found by the name of badname");
        $result = $this->gsa->getById($gatewayId);
    }

    public function testSaveWithInsert()
    {
        $gatewayId = rand(1, 1000000);
        $gateway = new Icm_Commerce_Gateway_Nmi($this->getMock("Icm_Service_Nmi_Api", array(), array(), '', false));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('INSERT'))
                   ->will($this->returnValue(true));

        $this->conn->expects($this->once())
                   ->method('lastInsert')
                   ->will($this->returnValue($gatewayId));

        $this->gsa->save($gateway);
        $this->assertEquals($gatewayId, $gateway->gateway_id);
    }

    public function testSaveWithoutUpdate()
    {
        $gatewayId = rand(1, 1000000);
        $gateway = new Icm_Commerce_Gateway_Nmi($this->getMock("Icm_Service_Nmi_Api", array(), array(), '', false));
        $gateway->gateway_id = $gatewayId;

        $this->conn->expects($this->never())
                   ->method('execute');

        // updates are not allowed so right now it does nothing... let's make sure of that
        $this->gsa->save($gateway);
        $this->assertEquals($gatewayId, $gateway->gateway_id);
    }
}