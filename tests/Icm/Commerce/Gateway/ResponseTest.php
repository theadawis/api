<?php

class Icm_Commerce_Gateway_Response_Test extends TestCase
{
    protected $response;

    public function setUp() {
        $this->response = new Icm_Commerce_Gateway_Response(1,2,3,4,5);
    }

    public function testGetTransactionId() {
        $this->assertEquals(1, $this->response->getTransactionId());
    }

    public function testGetAuthCode() {
        $this->assertEquals(2, $this->response->getAuthCode());
    }

    public function testGetStatus() {
        $this->assertEquals(3, $this->response->getStatus());
    }

    public function testGetMessage() {
        $this->assertEquals(4, $this->response->getMessage());
    }

    public function testGetRawResponse() {
        $this->assertEquals(5, $this->response->getRawResponse());
    }
}
