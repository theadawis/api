<?php

class Icm_Commerce_Gateway_Litle_Test extends TestCase
{
    /**
     * @var Icm_Commerce_Gateway_Litle
     */
    protected $gateway;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $api;

    protected function setUp()
    {
        $this->api = $this->getMock("Icm_Service_Litle_Api", array(), array(), '', false);
        $this->gateway = new Icm_Commerce_Gateway_Litle($this->api);
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->gateway, "Icm_Commerce_Gateway_Litle"));
    }

    public function testReportGroupCode()
    {
        $this->assertEquals('LTL', $this->gateway->getReportGroupCode());
    }

    public function testSaleSuccessful()
    {
        $customer = $this->generateCommerceCustomer();
        $order = $this->generateCommerceOrder();

        $document = new DOMDocument("1.0", "UTF-8");

        $root = $document->appendChild($document->createElement("litleOnlineResponse"));
        $root->setAttribute("version", "8.14");
        $root->setAttribute("response", "0");
        $root->setAttribute("message", "Valid Format");
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

        $voidResponse = $root->appendChild($document->createElement("saleResponse"));
        $voidResponse->setAttribute("id", "1");
        $voidResponse->setAttribute("reportGroup", $this->gateway->getReportGroupCode());
        $voidResponse->setAttribute("customerId", rand(100, 1000));
        $voidResponse->appendChild($document->createElement("litleTxnId", "1100026202"));
        $voidResponse->appendChild($document->createElement("orderId", $order->getOrderId()));
        $voidResponse->appendChild($document->createElement("response", "000"));
        $voidResponse->appendChild($document->createElement("responseTime", "2011-07-16T19:43:38"));
        $voidResponse->appendChild($document->createElement("postDate", "2011-07-16"));
        $voidResponse->appendChild($document->createElement("message", "Approved"));

        $this->api->expects($this->once())
                  ->method('chargeCard')
                  ->will($this->returnValue(simplexml_load_string($document->saveXML())));

        $response = $this->gateway->process($customer, $order);
        $this->assertTrue(is_a($response, 'Icm_Commerce_Gateway_Response'));
    }

    public function testRefundSuccessful()
    {
        $customer = $this->generateCommerceCustomer();
        $transaction = $this->generateCommerceTransaction();

        $document = new DOMDocument("1.0", "UTF-8");

        $root = $document->appendChild($document->createElement("litleOnlineResponse"));
        $root->setAttribute("version", "8.14");
        $root->setAttribute("response", "0");
        $root->setAttribute("message", "Valid Format");
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

            $voidResponse = $root->appendChild($document->createElement("voidResponse"));
            $voidResponse->setAttribute("id", "1");
            $voidResponse->setAttribute("reportGroup", $this->gateway->getReportGroupCode());
            $voidResponse->appendChild($document->createElement("litleTxnId", "1100026202"));
            $voidResponse->appendChild($document->createElement("response", "000"));
            $voidResponse->appendChild($document->createElement("responseTime", "2011-07-16T19:43:38"));
            $voidResponse->appendChild($document->createElement("postDate", "2011-07-16"));
            $voidResponse->appendChild($document->createElement("message", "Approved"));

        $this->api->expects($this->once())
                  ->method('void')
                  ->will($this->returnValue(simplexml_load_string($document->saveXML())));

        $response = $this->gateway->refund($transaction, $customer);
        $this->assertTrue(is_a($response, 'Icm_Commerce_Gateway_Response'));
    }

    /**
     * @dataProvider getSaleExceptions
     */
    public function testSaleExceptions($code, $class, $message)
    {
        $this->setExpectedException($class, $message);

        $customer = $this->generateCommerceCustomer();
        $order = $this->generateCommerceOrder();

        $document = new DOMDocument("1.0", "UTF-8");

        $root = $document->appendChild($document->createElement("litleOnlineResponse"));
        $root->setAttribute("version", "8.14");
        $root->setAttribute("response", "0");
        $root->setAttribute("message", "Valid Format");
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

        $voidResponse = $root->appendChild($document->createElement("saleResponse"));
        $voidResponse->setAttribute("id", "1");
        $voidResponse->setAttribute("reportGroup", $this->gateway->getReportGroupCode());
        $voidResponse->setAttribute("customerId", rand(100, 1000));
        $voidResponse->appendChild($document->createElement("litleTxnId", "1100026202"));
        $voidResponse->appendChild($document->createElement("orderId", $order->getOrderId()));
        $voidResponse->appendChild($document->createElement("response", $code));
        $voidResponse->appendChild($document->createElement("responseTime", "2011-07-16T19:43:38"));
        $voidResponse->appendChild($document->createElement("postDate", "2011-07-16"));
        $voidResponse->appendChild($document->createElement("message", $message));

        $this->api->expects($this->once())
                  ->method('chargeCard')
                  ->will($this->returnValue(simplexml_load_string($document->saveXML())));

        $response = $this->gateway->process($customer, $order);

    }

    public function getSaleExceptions()
    {
        // FORMAT: code, exceptionClass, exceptionMessage
        return array(
            array('010', 'Icm_Commerce_Gateway_TransactionError', 'Partially Approved'),
            array('110', 'Icm_Commerce_Gateway_InsufficientFunds', 'Insufficient Funds'),
            array('707', 'Icm_Commerce_Gateway_InsufficientFunds', 'Insufficient Funds'),
            array('102', 'Icm_Commerce_Gateway_TransactionError', 'Resubmit Transaction'),
            array('320', 'Icm_Commerce_Gateway_InvalidCardError', 'Invalid Expiration Date'),
            array('305', 'Icm_Commerce_Gateway_InvalidCardError', 'Card has expired'),
            array('352', 'Icm_Commerce_Gateway_InvalidCardError', 'Invalid CVV'),
            array('999', 'Icm_Commerce_Gateway_TransactionError', 'Valid Format'),
        );
    }

    /**
     * @dataProvider getRefundExceptions
     */
    public function testRefundExceptions($code, $class, $message)
    {
        $this->setExpectedException($class, $message);

        $customer = $this->generateCommerceCustomer();
        $transaction = $this->generateCommerceTransaction();

        $document = new DOMDocument("1.0", "UTF-8");

        $root = $document->appendChild($document->createElement("litleOnlineResponse"));
        $root->setAttribute("version", "8.14");
        $root->setAttribute("response", "0");
        $root->setAttribute("message", "Valid Format");
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

        $voidResponse = $root->appendChild($document->createElement("voidResponse"));
        $voidResponse->setAttribute("id", "1");
        $voidResponse->setAttribute("reportGroup", $this->gateway->getReportGroupCode());
        $voidResponse->appendChild($document->createElement("litleTxnId", "1100026202"));
        $voidResponse->appendChild($document->createElement("response", $code));
        $voidResponse->appendChild($document->createElement("responseTime", "2011-07-16T19:43:38"));
        $voidResponse->appendChild($document->createElement("postDate", "2011-07-16"));
        $voidResponse->appendChild($document->createElement("message", $message));

        $this->api->expects($this->once())
                  ->method('void')
                  ->will($this->returnValue(simplexml_load_string($document->saveXML())));

        $response = $this->gateway->refund($transaction, $customer);
    }

    public function getRefundExceptions()
    {
        // FORMAT: code, exceptionClass, exceptionMessage
        return array(
            array('360', 'Icm_Commerce_Gateway_TransactionError', 'Invalid transaction id'),
            array('999', 'Icm_Commerce_Gateway_Exception', 'Cannot void transaction'),
        );
    }

    /**
     * @expectedException Icm_Commerce_Gateway_NotAvailableError
     * @expectedExceptionMessage test1234
     */
    public function testSaleExceptionHttp()
    {
        $customer = $this->generateCommerceCustomer();
        $order = $this->generateCommerceOrder();

        $this->api->expects($this->once())
                  ->method('chargeCard')
                  ->will($this->throwException(new Icm_Util_Http_Exception('test1234')));

        $response = $this->gateway->process($customer, $order);
    }

    /**
     * @expectedException Icm_Commerce_Gateway_NotAvailableError
     * @expectedExceptionMessage test1234
     */
    public function testRefundExceptionHttp()
    {
        $customer = $this->generateCommerceCustomer();
        $transaction = $this->generateCommerceTransaction();

        $this->api->expects($this->once())
                  ->method('void')
                  ->will($this->throwException(new Icm_Util_Http_Exception('test1234')));

        $response = $this->gateway->refund($transaction, $customer);
        $this->assertTrue(is_a($response, 'Icm_Commerce_Gateway_Response'));
    }
}
