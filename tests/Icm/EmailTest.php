<?php

class Icm_Email_Test extends TestCase
{
    /**
     * @var Icm_Email
     */
    protected $email;

    protected $rules;

    protected $today;

    protected $conn;

    protected $statAdapter;

    protected $countsBefore;

    public function setup() {
        $this->config = Icm_Config::fromIni('/sites/api/tests/config/config.ini');
        $this->email = new Icm_Email($this->config);
        $this->rules = Icm_Api::bootstrap($this->config)->getConfigFileFromIni('emailrules.ini');
        $this->conn = Icm_Db_Pdo::connect('cg_internal', $this->config->getSection('cgDb'));
        $this->statAdapter = new Icm_Email_Stats_StorageAdapter($this->conn);
        $counts = $this->statAdapter->getByCreated(date('Y-m-d'));

        $this->countsBefore = array();

        foreach($counts as $countItem) {
            $this->countsBefore[$countItem->email_stat_rule] = $countItem->email_stat_count;
        }

        parent::setUp();
    }

    public function tearDown() {
        $countsAfter = $this->statAdapter->getByCreated(date('Y-m-d'));

        foreach($countsAfter as $countItem) {
            if (array_key_exists($countItem->email_stat_rule, $this->countsBefore) &&
                $countItem->email_stat_count != $this->countsBefore[$countItem->email_stat_rule]) {
                $this->conn->execute("UPDATE email_stats SET email_stat_count = :newcount
                                      WHERE email_stat_id = :id",
                                      array(':newcount' => $this->countsBefore[$countItem->email_stat_rule],
                                            ':id' => $countItem->email_stat_id));
            } else {
                // before record didn't exist so delete after record
                $this->conn->execute("DELETE FROM email_stats WHERE email_stat_id = :id",
                                      array(':id' => $countItem->email_stat_id));
            }
        }

        parent::tearDown();
    }

    public function testEmailGood() {
        $result = $this->email->validate('testuser@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_VALID, $result);
    }

    /**
     * @expectedException Exception
     */
    public function testEmailMissingRequiredField() {
        $result = $this->email->validate();
    }

    public function testEmailFailInternalRuleCharacters() {
        $result = $this->email->validate('a space@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER, $result);
        $result = $this->email->validate('a%percent@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER, $result);
    }

    public function testEmailFailInternalRuleTwoAmpersands() {
        // two ampersands
        $result = $this->email->validate('aspace@@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER, $result);
    }

    public function testEmailFailInternalRuleAddressPartial() {
        $list = $this->rules->getOption('specific')['address-partial'];
        $emailName = substr($list, 0, strpos($list, ','));
        $result = $this->email->validate($emailName . '@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_ADDRESS_PARTIAL, $result);
    }

    public function testEmailFailInternalRuleAddressExact() {
        $list = $this->rules->getOption('specific')['address-exact'];
        $emailName = substr($list, 0, strpos($list, ','));
        $result = $this->email->validate($emailName . '@thecontrolgroup.com');
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_ADDRESS_EXACT, $result);
    }

    public function testEmailFailInternalRuleDomainExact() {
        $list = $this->rules->getOption('specific')['domain-exact'];
        $hostName = substr($list, 0, strpos($list, ','));
        $result = $this->email->validate('passme@' . $hostName);
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_DOMAIN_EXACT, $result);
    }

    public function testEmailSendGood() {
        $queueFactory = new Icm_QueueFactory($this->config->getSection('rabbitmq'));
        $queueFactory->getEmailValidateQueue()->getAdapter()->purgeQueue();

        // create the test customer
        $this->deleteTestCustomer();
        $testCustomerId = $this->writeTestCustomer();

        $customerStorage = new Icm_Commerce_Customer_StorageAdapter('customer', 'customer_id', $this->conn);
        $customerBefore = $customerStorage->getById($testCustomerId);
        $customerBefore->email_score = 0;
        $customerStorage->save($customerBefore);

        $emailAddress = "testuser@thecontrolgroup.com";
        $data = array(
            "customer_created"      => "2009-01-01 01:01:01",
            "customer_fname"        => "Test",
            "customer_id"           => $testCustomerId,
            "customer_lname"        => "User",
            "customer_ip_address"   => "123.123.123.123",
            "registration_url"      => "http://www.instantcheckmate.com/register",
            "meta"  => array(
                "email_type"            => "lead",
                "email"                 => "$emailAddress",
                "listid"                => 29,
                "newsletterid"          => 145,
                "report_address"        => "1234 Some Street, San Diego, CA",
                "report_name"           => "firstname lastname",
                "search_result_link"    => "http://www.instantcheckmate.com/resultsAPI/CA/Smith/John",
                "unsubscribe_url"       => "http://unsubscribe.instantcheckmate.com",
                "headers"               => array('')
            )
        );
        $result = $this->email->send($data);
        $this->assertTrue($result);

        $customerAfter = $customerStorage->getById($testCustomerId);
        $this->assertEquals(Icm_Email::EMAIL_RULE_INTERNAL_VALID, $customerAfter->email_score);

        $messages = $queueFactory->getEmailValidateQueue()->receive(1);
        $this->assertEquals($emailAddress, json_decode($messages->current()->body)->email_data->meta->email);
        $queueFactory->getEmailValidateQueue()->getAdapter()->purgeQueue();
    }

    public function testEmailSendSkipValidation() {
        $queueFactory = new Icm_QueueFactory($this->config->getSection('rabbitmq'));
        $queueFactory->getEmailValidateQueue()->getAdapter()->purgeQueue();

        // create the test customer
        $this->deleteTestCustomer();
        $testCustomerId = $this->writeTestCustomer();

        // this email should fail internal validation but we're skipping the internal test
        $emailAddress = "a space@thecontrolgroup.com";
        $data = array(
            "customer_created"      => "2009-01-01 01:01:01",
            "customer_fname"        => "Test",
            "customer_id"           => $testCustomerId,
            "customer_lname"        => "User",
            "customer_ip_address"   => "123.123.123.123",
            "registration_url"      => "http://www.instantcheckmate.com/register",
            "meta"  => array(
                "email_type"            => "lead",
                "email"                 => "$emailAddress",
                "listid"                => 29,
                "newsletterid"          => 145,
                "report_address"        => "1234 Some Street, San Diego, CA",
                "report_name"           => "firstname lastname",
                "search_result_link"    => "http://www.instantcheckmate.com/resultsAPI/CA/Smith/John",
                "unsubscribe_url"       => "http://unsubscribe.instantcheckmate.com",
                "headers"               => array('')
            )
        );
        $result = $this->email->send($data, true);
        $this->assertTrue($result);

        $messages = $queueFactory->getEmailSendQueue()->receive(1);
        $this->assertEquals($emailAddress, json_decode($messages->current()->body)->email_data->meta->email);
        $queueFactory->getEmailSendQueue()->getAdapter()->purgeQueue();
    }

    public function testVerifyErrorInCustomerTableAndEmailStatsTable() {
        // create the test customer
        $this->deleteTestCustomer();
        $testCustomerId = $this->writeTestCustomer();

        $data['customer_id'] = $testCustomerId;
        $data['meta'] = array("email" => "a space@thecontrolgroup.com");
        $validationResult = Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER;

        // get test customer info
        $customerStorage = new Icm_Commerce_Customer_StorageAdapter('customer', 'customer_id', $this->conn);
        $customerBefore = $customerStorage->getById($data['customer_id']);
        $customerBefore->email_score = 0;
        $customerStorage->save($customerBefore);

        $statBefore = $this->getStat($validationResult);

        $this->email->send($data);

        $customerAfter = $customerStorage->getById($data['customer_id']);
        $this->assertEquals($validationResult, $customerAfter->email_score);

        $statAfter = $this->getStat($validationResult);
        $this->assertEquals($statBefore->email_stat_count + 1, $statAfter->email_stat_count);

        // reset email_score
        $customerStorage->save($customerBefore);
    }

    protected function getStat($validationResult) {
        $statStorage = new Icm_Email_Stats_StorageAdapter($this->conn);
        $statCheck = $statStorage->getByRuleCreated($validationResult, date("Y-m-d"));

        if (!$statCheck) {
            $stat = new Icm_Email_Stats;
            $stat->email_stat_rule = $validationResult;
            $stat->email_stat_count = 0;
            $stat->email_stat_created = date('Y-m-d');
            $statStorage->save($stat);
        }

        return $statStorage->getByRuleCreated($validationResult, date("Y-m-d"));
    }
}
