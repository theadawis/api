<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/11/12
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Event_Dispatcher_Test extends PHPUnit_Framework_TestCase implements Icm_Event_Listener_Interface
{

    protected $_testEvt = false;

    /**
     * @var Icm_Event_Dispatcher
     */
    protected $dispatcher;

    public function setUp() {

        $this->dispatcher = new Icm_Event_Dispatcher();
        $this->dispatcher->addListener('testEvt', $this);
    }

    /**
     * @param Icm_Event_Dispatcher $dispatcher
     */
    public function testDispatch() {

        $this->assertFalse($this->_testEvt);
        $this->dispatcher->trigger('testEvt', new Icm_Event());
        $this->assertTrue($this->_testEvt);

    }

    public function dispatch($evtName, Icm_Event_Interface $event)
    {
        switch($evtName) {
            case 'testEvt':
                $this->_testEvt($event);
            break;
            case 'testEvt2':
                $this->_testEvt2($event);
            break;
        }
    }

    public function _testEvt() {
        $this->_testEvt = true;
    }

    public function _testEvt2(Icm_Event_Interface $event) {
        $event->stopPropogation();
    }

    public function testDefaults() {
        $dispatcher = new Icm_Event_Dispatcher();
        $this->assertFalse($dispatcher->hasListeners('test'));

        $this->assertSame(array(), $dispatcher->getListeners('test'));
    }

    public function testPropogationStop() {
        $evt =  new Icm_Event();
        $this->dispatcher->addListener('testEvt2', $this);
        $this->dispatcher->trigger('testEvt2', $evt);
        $this->assertTrue($evt->propogationStopped());
    }
}
