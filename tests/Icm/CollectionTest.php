<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/23/12
 * Time: 8:08 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Collection_Test extends PHPUnit_Framework_TestCase
{

    public function testCanCreate() {
        $collection = new Icm_Collection(array(
            'foo' => 'bar',
            'baz' => 'foo',
            'abc' => '123'
        ));

        $this->assertInstanceOf('Icm_Collection', $collection);
    }

    public function testCanIterate() {
        $collection = new Icm_Collection(array(
            'foo' => 'bar',
            'baz' => 'foo',
            'abc' => '123'
        ));

        $this->assertInstanceOf('ArrayIterator', $collection->getIterator());

        foreach($collection as $foo => $bar) {
            $this->assertNotEmpty($bar);
            $this->assertNotEmpty($foo);
        }

        foreach($collection->getIterator() as $foo => $bar) {
            $this->assertNotEmpty($bar);
            $this->assertNotEmpty($foo);
        }

    }

}
