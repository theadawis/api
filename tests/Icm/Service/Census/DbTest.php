<?php

class Icm_Service_Census_Db_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    protected $census;

    protected function setUp(){
        $conn = Icm_Db_Pdo::connect('phoneDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'phoneDb'));
        $this->census = new Icm_Service_Census_Db($conn);
    }

    public function testGetByZip(){
        $result = $this->census->getByZip(92126);
        $this->assertArrayHasKey('Population', $result[0]);
    }
}
