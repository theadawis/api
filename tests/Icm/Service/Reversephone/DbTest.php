<?php

class Icm_Service_Reversephone_Db_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    protected $reversePhone;

    protected $api;

    protected function setUp(){
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $conn = Icm_Db_Pdo::connect('phoneDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'phoneDb'));
        $this->reversePhone = new Icm_Service_Reversephone_Db($conn);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testGetByPhone(){
        $phone = $this->reversePhone->getByPhone(8582925529);
        $this->assertNotNull($phone);
        $this->assertInstanceOf('Icm_Entity_Phone', $phone);
        $this->assertNotNull($phone->zip);
    }

    public function testGetByZip(){
        $result = $this->reversePhone->getByZip(92126);
        $this->assertEquals($result[0]['Latitude'], 32.911042);
    }
}
