<?php

class Icm_Service_Pipl_Api_Test extends PHPUnit_Framework_TestCase {

    public function testPiplSearch(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'pipl');
        $pipl = new Icm_Service_Pipl_Api($config);

        $first = "Mike";
        $last = "Linn";
        $from_age = "24";
        $to_age = "26";
        $city = "San Luis Obispo";
        $state = 'CA';

        $searchFields = array(  "first_name"=>$first,
                                "last_name"=>$last,
                                "city"=>$city,
                                'state'=>$state,
                                'country'=>'US',
                                'no_sponsored'=>true,
                                "exact_name"=>false);
        $collection = $pipl->search($searchFields);

        $itColl = $collection->getIterator();

        $this->assertInstanceOf('Icm_Collection', $collection);

        // iterate over the values in the collection
        while ($itColl->valid())
        {
            $this->assertInstanceOf('Icm_Struct_Social', $itColl->current());
            $itColl->next();
        }

    }

}
