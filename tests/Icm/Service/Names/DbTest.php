<?php

class Icm_Service_Names_Db_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    /**
     * @var Icm_Service_Names_Db
     */
    protected $names;

    protected function setUp(){
        $this->db = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));
        $this->names = new Icm_Service_Names_Db($this->db);
    }

    public function testGetGender(){
        $this->assertTrue($this->names->getGender('Joe') === 'm');
        $this->assertTrue($this->names->getGender('Jill') === 'f');
        $this->assertTrue($this->names->getGender('oiweoewf') === 'u');
    }

    public function testGetFullName(){
        $this->assertTrue(in_array('joseph', $this->names->getFullName('Joe')));
    }
}