<?php
/**
 * Api.php
 * User: chris
 * Date: 12/21/12
 * Time: 11:41 AM
 */
class Icm_Service_Litle_ApiTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $config;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $customer;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $order;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $transaction;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $creditcard;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $httpClient;

    public function setUp() 
    {
        $this->config = $this->getMock("Icm_Config", array(), array(), '', false);
        $this->customer = $this->getMock("Icm_Commerce_Customer", array(), array(), '', false);
        $this->creditcard = $this->getMock("Icm_Commerce_Creditcard", array(), array(), '', false);
        $this->order = $this->getMock("Icm_Commerce_Order", array(), array(), '', false);
        $this->transaction = $this->getMock("Icm_Commerce_Transaction", array(), array(), '', false);
        $this->httpClient = $this->getMock("Icm_Util_Http_Client", array(), array(), '', false);
    }

    public function testCreate() 
    {
        $litle = new Icm_Service_Litle_Api($this->config);
        $this->assertTrue(is_a($litle, "Icm_Service_Litle_Api"));
    }

    /**
     * @dataProvider getTestNumbers
     */
    public function testChargeCard($ccnumber) 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "proxy":
                    return true;
                    break;
                case "user":
                    return "foo";
                    break;
                case "password":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));

        $this->creditcard->expects($this->exactly(2))->method("getCardType")->will($this->returnValue(Icm_Commerce_Creditcard::CARD_TYPE_VISA));
        $this->creditcard->expects($this->atLeastOnce())->method("formatExpiration")->will($this->returnValue("1214"));
        $this->creditcard->expects($this->atLeastOnce())->method("getNumber")->will($this->returnValue($ccnumber));
        $this->creditcard->expects($this->atLeastOnce())->method("isValidCCNumber")->will($this->returnValue(true));
        $this->customer->expects($this->atLeastOnce())->method("getCreditCard")->will($this->returnValue($this->creditcard));

        $this->order->expects($this->atLeastOnce())->method("getOrderId")->will($this->returnValue(12345));
        $this->transaction->expects($this->never())->method("getTransactionId")->will($this->returnValue(123456));

        $this->order->expects($this->never())->method("getTransaction")->will($this->returnValue($this->transaction));

        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);

        $response = $litle->chargeCard($this->customer, $this->order);
    }

    public function testVoid() {

        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "foo";
                break;
                case "password":
                    return "bar";
                break;
                case "url":
                    return "http://litle.com";
                break;
            }
        }));

        $this->transaction->expects($this->once())->method("getTransactionId")->will($this->returnValue(123456));

        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->void($this->transaction);

    }
    
    /**
     * @expectedException Icm_Service_Litle_Exception
     */
    public function testThrowsExceptionWithoutUsername() 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "password":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));


        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);
    }

    /**
     * @expectedException Icm_Service_Litle_Exception
     */
    public function testThrowsExceptionWithoutPassword() 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));


        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);
    }

    /**
     * @expectedException Icm_Service_Litle_Exception
     * @expectedExceptionMessage You must have 'url' defined in your configuration
     */
    public function testThrowsExceptionWithoutUrl() 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "proxy":
                    return false;
                    break;
                case "user":
                    return "bar";
                    break;
                case "password":
                    return "foo";
                    break;
            }
        }));

        $this->creditcard->expects($this->exactly(2))->method("getCardType")->will($this->returnValue(Icm_Commerce_Creditcard::CARD_TYPE_VISA));
        $this->creditcard->expects($this->once())->method("isValidCCNumber")->will($this->returnValue(true));

        $this->customer->expects($this->atLeastOnce())->method("getCreditCard")->will($this->returnValue($this->creditcard));
        $this->transaction->expects($this->never())->method("getTransactionId")->will($this->returnValue(123456));
        $this->order->expects($this->never())->method("getTransaction")->will($this->returnValue($this->transaction));
        $this->order->expects($this->atLeastOnce())->method("getOrderId")->will($this->returnValue(12345));
        
        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);
    }

    /**
     * @expectedException Icm_Service_Litle_Exception
     */
    public function testThrowsExceptionWithNullCreditCard() 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "foo";
                    break;
                case "password":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));

        $this->customer->expects($this->atLeastOnce())->method("getCreditCard")->will($this->returnValue(null));
        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);
    }

    /**
     * @expectedException Icm_Service_Litle_Exception
     * @expectedExceptionMessage Credit card type is null
     */
    public function testThrowsExceptionWithNullCreditCardType() 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "bar";
                    break;
                case "password":
                    return "foo";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));

        $this->creditcard->expects($this->once())->method("getCardType")->will($this->returnValue(null));
        $this->creditcard->expects($this->never())->method("isValidCCNumber")->will($this->returnValue(true));

        $this->customer->expects($this->atLeastOnce())->method("getCreditCard")->will($this->returnValue($this->creditcard));
        $this->transaction->expects($this->never())->method("getTransactionId")->will($this->returnValue(123456));
        $this->order->expects($this->never())->method("getTransaction")->will($this->returnValue($this->transaction));

        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);
    }

    /**
     * @dataProvider getTestNumbers
     * @expectedException Icm_Service_Litle_Exception
     * @expectedExceptionMessage Invalid credit card number:
     */
    public function testThrowsExceptionWithInvalidCard($ccnumber) 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "foo";
                    break;
                case "password":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));

        $this->creditcard->expects($this->once())->method("getCardType")->will($this->returnValue(Icm_Commerce_Creditcard::CARD_TYPE_VISA));
        $this->creditcard->expects($this->never())->method("formatExpiration")->will($this->returnValue("1214"));
        $this->creditcard->expects($this->atLeastOnce())->method("getNumber")->will($this->returnValue($ccnumber));
        $this->creditcard->expects($this->atLeastOnce())->method("isValidCCNumber")->will($this->returnValue(false));
        $this->customer->expects($this->once())->method("getCreditCard")->will($this->returnValue($this->creditcard));

        $this->transaction->expects($this->never())->method("getTransactionId")->will($this->returnValue(null));

        $this->order->expects($this->never())->method("getTransaction")->will($this->returnValue($this->transaction));

        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);

    }

    /**
     * @dataProvider getTestNumbers
     * @expectedException Icm_Service_Litle_Exception
     * @expectedExceptionMessage Invalid credit card type:
     */
    public function testThrowsExceptionWithInvalidCardType($ccnumber) 
    {
        $this->config->expects($this->atLeastOnce())->method("getOption")->will($this->returnCallback(function($in) {
            switch($in) {
                case "user":
                    return "foo";
                    break;
                case "password":
                    return "bar";
                    break;
                case "url":
                    return "http://litle.com";
                    break;
            }
        }));

        $this->creditcard->expects($this->once())->method("getCardType")->will($this->returnValue("Foobar"));
        $this->creditcard->expects($this->never())->method("formatExpiration")->will($this->returnValue("1214"));
        $this->creditcard->expects($this->never())->method("getNumber")->will($this->returnValue($ccnumber));
        $this->creditcard->expects($this->never())->method("isValidCCNumber")->will($this->returnValue(false));
        $this->customer->expects($this->once())->method("getCreditCard")->will($this->returnValue($this->creditcard));

        $this->transaction->expects($this->never())->method("getTransactionId")->will($this->returnValue(null));

        $this->order->expects($this->never())->method("getTransaction")->will($this->returnValue($this->transaction));

        $litle = new Icm_Service_Litle_Api($this->config);
        $litle->setHttpClient($this->httpClient);
        $reponse = $litle->chargeCard($this->customer, $this->order);

    }


    public function getTestNumbers() 
    {
        return array(
            array("4116480559370132"),
            array("5123695007103193"),
            array("5122415444933925"),
            array("4119692004990226"),
            array("5106447623213738"),
            array("4112296825756779"),
            array("5155659853141666"),
            array("4110716719147654"),
            array("5199339067794810"),
            array("4112530021797363"),
            array("5102881917494147"),
            array("4112653620165475"),
            array("5155239927190195"),
            array("4113864427850982"),
            array("5184778657904478"),
            array("4111111111111111"),
            array("4012888888881881"),
            array("5555555555554444"),
            array("5105105105105100")
        );
    }
}
