<?php

class Icm_Service_Nmi_Api_Test extends TestCase
{
    /**
     * @var Icm_Service_Nmi_Api
     */
    protected $nmi;

    protected function setUp()
    {
        parent::setUp();

        // create the nmi service object
        $this->nmi = new Icm_Service_Nmi_Api(Icm_Config::fromArray(array()));
    }

    public function testCreate()
    {
        $this->assertTrue(is_a($this->nmi, "Icm_Service_Nmi_Api"));
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Customer must have a credit card.
     */
    public function testThrowsExceptionWithoutCustomerCreditCard()
    {
        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // create an order
        $order = $this->generateCommerceOrder();

        // get the response
        $response = $this->nmi->sale($customer, $order);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Invalid credit card number.
     */
    public function testThrowsExceptionWithInvalidCustomerCreditCard()
    {
        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // create an order
        $order = $this->generateCommerceOrder();

        // add a credit card to the customer
        $customer->setCreditCard($this->generateCommerceCreditCard(array(
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
            'number' => 1234,
        )));

        // get the response
        $response = $this->nmi->sale($customer, $order);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Invalid credit card expiration date.
     */
    public function testThrowsExceptionWithInvalidCustomerCreditCardExpirationDate()
    {
        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // add a variation
        $customer->setVaration($this->generateSplitTestVariation());

        // create an order
        $order = $this->generateCommerceOrder();

        // add a credit card to the customer
        $customer->setCreditCard($this->generateCommerceCreditCard(array(
            'expiration_year' => -9999,
        )));

        // get the response
        $response = $this->nmi->sale($customer, $order);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Invalid or no order id provided.
     */
    public function testThrowsExceptionWithInvalidCustomerCreditCardOrderId()
    {
        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // add a variation
        $customer->setVaration($this->generateSplitTestVariation());

        // create an order
        $order = $this->generateCommerceOrder(array(
            'order_id' => null
        ));

        // add a credit card to the customer
        $customer->setCreditCard($this->generateCommerceCreditCard());

        // get the response
        $response = $this->nmi->sale($customer, $order);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage At least one product must be provided in this order.
     */
    public function testThrowsExceptionWithInvalidCustomerCreditCardOrderWithoutAtLeastOneProduct()
    {
        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // add a variation
        $customer->setVaration($this->generateSplitTestVariation());

        // create an order
        $order = $this->generateCommerceOrder();

        // add a credit card to the customer
        $customer->setCreditCard($this->generateCommerceCreditCard());

        // get the response
        $response = $this->nmi->sale($customer, $order);
    }

    public function testSale()
    {
        //        $this->markTestSkipped('Test credit card is not going through');
        // set up a new visit
        $visit = new Icm_Struct_Tracking_Visit();
        $visit->setAffiliate(new Icm_Struct_Marketing_Affiliate());

        // get a commerce customer
        $customer = $this->generateCommerceCustomer();

        // add a variation
        $customer->setVaration($this->generateSplitTestVariation());

        // add the visit to the customer
        $customer->setVisit($visit);

        // add a credit card to the customer
        $customer->setCreditCard($this->generateCommerceCreditCard(array('first_name' => $customer->first_name, 'last_name' => $customer->last_name)));

        // create an order
        $order = $this->generateCommerceOrder(array('customer_id' => $customer->customer_id));

        // add a product to the order
        $order->addProduct($this->generateCommerceProduct(), 2);

        // create a transaction
        $transaction = $this->generateCommerceTransaction(array('order_id' => $order->order_id, 'amount' => $order->getSubtotal()));

        // get the response
        $response = $this->nmi->sale($customer, $order);

        // should fail because of the credit card
        $this->assertEquals(2, $response['response']);
        $this->assertEquals('Invalid card number', $response['responsetext']);

        // update the transaction id with the one from the response
        $transaction->transaction_id = $response['transactionid'];

        return $transaction;
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Transaction amount or amount must be specified.
     */
    public function testThrowsExceptionRefundWithoutProperTransactionAmount()
    {
        // create a transaction
        $transaction = $this->generateCommerceTransaction(array(
            'amount' => null
        ));

        // get the response
        $response = $this->nmi->refund($transaction);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage $amount must be either a numeric value or boolean false.
     */
    public function testThrowsExceptionRefundWithoutProperAmount()
    {
        // create a transaction
        $transaction = $this->generateCommerceTransaction();

        // get the response
        $response = $this->nmi->refund($transaction, 'asdf');
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Refund amount cannot be greater than the original transaction amount.
     */
    public function testThrowsExceptionRefundWithAmountGreaterThanOriginalTransactionAmount()
    {
        // create a transaction
        $transaction = $this->generateCommerceTransaction(array(
            'amount' => 123
        ));

        // get the response
        $response = $this->nmi->refund($transaction, 125);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Transaction Id is required.
     */
    public function testThrowsExceptionRefundWithoutTransactionId()
    {
        // create a transaction
        $transaction = $this->generateCommerceTransaction(array(
            'transaction_id' => null
        ));

        // get the response
        $response = $this->nmi->refund($transaction);
    }

    /**
     * @expectedException Icm_Service_Nmi_Exception
     * @expectedExceptionMessage Order Id is required.
     */
    public function testThrowsExceptionRefundWithoutOrderId()
    {
        // create a transaction
        $transaction = $this->generateCommerceTransaction(array(
            'order_id' => null
        ));

        // get the response
        $response = $this->nmi->refund($transaction);
    }

    /**
     * @depends testSale
     */
    public function testRefund($transaction)
    {
        // get the response
        $response = $this->nmi->refund($transaction);

        // should fail, because the transaction didn't go through
        $this->assertEquals(3, $response['response']);
        $this->assertRegexp('/Transaction not found.*/i', $response['responsetext']);
    }
}