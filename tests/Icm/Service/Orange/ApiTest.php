<?php

class Icm_Service_Orange_Api_Test extends TestCase
{
    const API_RESPONSE_SUCCESS = 1;

    /**
     * @var Icm_Service_Orange_Api
     */
    protected $orange;

    protected function setUp()
    {
        $this->orange = new Icm_Service_Orange_Api(Icm_Config::fromArray(array()));
        $this->orange->useTestToken(true);
    }

    public function testEmergencyArchiveAndCancelCustomer()
    {
        // add a customer
        $customer = $this->generateCommerceCustomer();
        $customer->setCreditCard($this->generateCommerceCreditCard());
        $customer->setVaration($this->generateSplitTestVariation());
        $customer->setVisit($this->generateVisit()->setAffiliate($this->generateAffiliate()));

        $customer->guid = "3C7E7E5A46F2B6B385257AEF0002AFBC";

        // cancel and archive
        $response = $this->orange->cancelCustomer($customer);
        $response = $this->orange->archiveCustomer($customer);
    }

    public function testAddCustomer()
    {
        // add a customer
        $customer = $this->generateCommerceCustomer();
        $customer->setCreditCard($this->generateCommerceCreditCard());
        $customer->setVaration($this->generateSplitTestVariation());
        $customer->setVisit($this->generateVisit()->setAffiliate($this->generateAffiliate()));

        // add an order
        $order = Icm_Commerce_Order::fromArray(array('customer_id' => $customer->customer_id));
        $productQuantity = 1;
        $order->addProduct($this->generateCommerceProduct(), $productQuantity);

        // call orange
        $response = $this->orange->addCustomer($customer, $order);

        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));
        $customer->guid = $response['q_cust_guid'];

        return $customer;
    }

    /**
     * @depends testAddCustomer
     */
    public function testGetCustomer(Icm_Commerce_Customer $customer)
    {
        // call orange
        $response = $this->orange->getCustomer($customer);
        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));

        $this->assertEquals($response['q_cust_first_name'], strtoupper($customer->first_name));
        $this->assertEquals($response['q_cust_last_name'], strtoupper($customer->last_name));
        $this->assertEquals(strtoupper($response['q_cust_email']), strtoupper($customer->email));
        $this->assertEquals($response['q_cust_guid'], strtoupper($customer->guid));

        return $customer;
    }

    /**
     * @depends testGetCustomer
     */
    public function testUpdateCustomer(Icm_Commerce_Customer $customer)
    {
        $customer->first_name = 'John';
        $customer->last_name = 'Smith';

        // call orange
        $response = $this->orange->updateCustomer($customer);
        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));

        return $customer;
    }

    /**
     * @depends testUpdateCustomer
     */
    public function testAddTransaction(Icm_Commerce_Customer $customer)
    {
        // generate a transaction
        $transaction = $this->generateCommerceTransaction();

        // generate a product gateway
        $productGateway = $this->generateCommerceProductGateway();

        // call orange
        $response = $this->orange->addTransaction($customer, $transaction, $productGateway);
        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));

        // set the orange guid for future refund of trans
        $transaction->orange_guid = $response['q_tran_guid'];

        return array($customer, $transaction, $productGateway);
    }

    /**
     * @depends testUpdateCustomer
     */
    public function testCancelCustomer(Icm_Commerce_Customer $customer)
    {
        // call orange
        $response = $this->orange->cancelCustomer($customer);
        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));

        return $customer;
    }

    /**
     * @depends testCancelCustomer
     */
    public function testArchiveCustomer(Icm_Commerce_Customer $customer)
    {
        // call orange
        $response = $this->orange->archiveCustomer($customer);
        $this->assertTrue($response['code'] == self::API_RESPONSE_SUCCESS, "Bad response: " . print_r($response, true));
    }
}