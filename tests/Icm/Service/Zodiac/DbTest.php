<?php

class Icm_Service_Zodiac_DbTest extends PHPUnit_Framework_TestCase
{
    public function testGetCompatibility() {
        // erase any leftover test data
        $conn = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));
        $conn->execute("DELETE FROM horoscope_matches WHERE sign1 = 'testsign'");

        // setup zodiac db service
        $db = new Icm_Service_Zodiac_Db($conn);

        // try to get compatibility of non-existent signs
        $result = $db->getCompatibility('testsign', 'testersign');

        // should give back empty array
        $this->assertEmpty($result);

        // add entry for two new signs
        $params = array(
            'testsign',
            'testersign',
            'These two signs are completely incompatible',
            'Nope',
            1,
            'No way, dude',
        );
        $conn->execute("INSERT INTO horoscope_matches (sign1, sign2, original, paraphrased, score, score_phrase) VALUES (?, ?, ?, ?, ?, ?)", $params);

        // get compatibility for them
        $result = $db->getCompatibility('testsign', 'testersign');

        // should return matching data
        $this->assertNotEmpty($result);
        $this->assertEquals($params[3], $result['paraphrased']);
        $this->assertEquals($params[5], $result['score_phrase']);
        $this->assertEquals($params[4], $result['score']);

        // should not have 'original' column
        $this->assertFalse(isset($result['original']));

        // cleanup db
        $conn->execute("DELETE FROM horoscope_matches WHERE sign1 = 'testsign'");
    }
}