<?php

class Icm_Service_Yahoo_Api_Test extends PHPUnit_Framework_TestCase
{
    public function testYahooWebSearch(){
        $first = "Joey";
        $last = "Rocco";

        $query = $first . "+" . $last;

        // first test a search query that won't return any results properly returns null
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini','yahooBoss');
        $yahoo = new Icm_Service_Yahoo_Api();
        $yahoo->setConfig($config);

        // now a legit search
        $collection = $yahoo->search($query,array("web"));

        // return a collection of structs
        $this->assertInstanceOf('Icm_Collection',$collection);
        $itColl = $collection->getIterator();

        while ($itColl->valid()){
            $this->assertInstanceOf('Icm_Struct_WebResults',$itColl->current());
            $itColl->next();
        }
    }

    public function testGeocode() {
        $yahoo = new Icm_Service_Yahoo_Api();
        $this->assertArrayHasKey('results', $yahoo->geocode('10823 worthing avenue, san diego, ca 92126'));
    }

    public function testIsTeaser() {
        $yahoo = new Icm_Service_Yahoo_Api();
        $this->assertTrue($yahoo->isTeaser(92126));
    }
}
