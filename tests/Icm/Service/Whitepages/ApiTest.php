<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/13/12
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Service_Whitepages_Api_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Config
     */
    protected $config;

    public function setUp(){
        parent::setUp();
        $this->config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/whitepages.ini', 'whitepages');
    }

    /**
     * @param $mode string
     * @dataProvider modeProvider
     */
    public function testSetMode($mode){
        if ($mode == Icm_Service_Whitepages_Api::MODE_FREE) {
            $this->setExpectedException('Icm_Service_Whitepages_Exception');
        }

        $wp = Icm_Service_Whitepages_Api::create($this->config->getOption('apiKey'));
        $wp->setMode($mode);
        $wp->setOutput(Icm_Service_Whitepages_Api::OUTPUT_XML);

        $this->assertTrue($wp->getMode() == $mode);

        $result = $wp->execute(Icm_Service_Whitepages_Api::METHOD_REVERSEPHONE, array('phone' => '8584052745'));

        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @param $output
     * @dataProvider outputProvider
     */
    public function testSetOutput($output){
        $wp = Icm_Service_Whitepages_Api::create($this->config->getOption('apiKey'));
        $wp->setMode(Icm_Service_Whitepages_Api::MODE_PAID);

        $wp->setOutput($output);
        $this->assertTrue($wp->getOutput() == $output);

        $result = $wp->execute(Icm_Service_Whitepages_Api::METHOD_REVERSEPHONE, array('phone' => '8584052745'));

        if ($output == Icm_Service_Whitepages_Api::OUTPUT_XML) {
            $this->assertInstanceOf('SimpleXMLElement', $result);
        }
        elseif ($output == Icm_Service_Whitepages_Api::OUTPUT_JSON) {
            $this->assertInternalType('array', $result);
        }
    }

    public function modeProvider(){
        return array(
            array(Icm_Service_Whitepages_Api::MODE_FREE),
            array(Icm_Service_Whitepages_Api::MODE_PAID)
        );
    }

    public function outputProvider(){
        return array(
            array(Icm_Service_Whitepages_Api::OUTPUT_JSON),
            array(Icm_Service_Whitepages_Api::OUTPUT_XML)
        );
    }
}
