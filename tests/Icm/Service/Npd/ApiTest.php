<?php

class Icm_Service_Npd_Api_Test extends  PHPUnit_Framework_TestCase
{
    public function testCriminal(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
            'firstName' => 'Tyler',
            'lastName' => 'Clayton',
            'state' => 'CA',
            'DOB' => '12/30/1986'
        );
        $results = $npd->criminalSearch($params);

        // should have gotten multiple results
        $this->assertGreaterThan(0, count($results));

        // each one should be a criminal record struct
        foreach ($results as $result) {
            $this->assertInstanceOf('Icm_Struct_Criminalrecord', $result);
        }
    }

    public function testSexOffender(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstName' => NULL,
                'lastName' => NULL,
                'city' => NULL,
                'state' => NULL,
                'zip' => 92109
        );
        $result = $npd->sexOffenderSearch($params);
        $this->assertTrue(count($result) > 0);

        foreach ($result as $offender) {
            $this->assertInstanceOf('Icm_Struct_Sexoffender', $offender);
        }
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testTerrorist(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
            'firstName' => 'Saddam',
            'lastName' => 'Hussein'
        );
        $npd->terroristSearch($params);
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testMerchantVessel(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'ownerfirstname' => 'John',
                'ownerlastname' => 'Smith'
        );
        $npd->merchantVesselSearch($params);
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testAircraft(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstname' => 'John',
                'lastname' => 'Smith'
        );
        $npd->aircraftSearch($params);
    }

    public function testFAALicense(){
        $this->markTestSkipped('Getting NPD error when running FAA License search in test mode');
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'Firstname' => 'John',
                'Lastname' => 'Smith'
        );
        $results = $npd->faaLicenseSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $license) {
            $this->assertInstanceOf('Icm_Struct_Faalicense', $license);
            $this->assertRegexp("/.*{$params['Firstname']}.*/i", $license->first_name);
            $this->assertRegexp("/.*{$params['Lastname']}.*/i", $license->last_name);
        }
    }

    public function testDeath(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstName' => 'John',
                'lastName' => 'Smith',
                'ssn' => '',
                'dob' => NULL
        );
        $results = $npd->deathSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $death) {
            $this->assertInstanceOf('Icm_Struct_Deathrecord', $death);
            $this->assertNotNull($death->dod);
        }
    }

    public function testDEA(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'Deanumber' => NULL,
                'firstName' => 'John',
                'lastName' => 'Smith',
                'middlename' => NULL,
                'city' => NULL,
                'state' => NULL
        );
        $results = $npd->deaSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $dea) {
            $this->assertInstanceOf('Icm_Struct_Dealicense', $dea);
            $this->assertNotNull($dea->dea_number);
        }
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testBusiness(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'ownerfirst' => 'John',
                'ownerlast' => 'Smith'
        );
        $npd->businessSearch($params);
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testWhitePages(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstname' => 'John',
                'lastname' => 'Smith'
        );
        $npd->whitePagesSearch($params);
    }

    public function testCell(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstname' => 'John',
                'lastname' => 'Smith',
                'state' => 'CA',
                'phone' => NULL
        );
        $results = $npd->cellSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $cell) {
            $this->assertInstanceOf('Icm_Struct_Cellphone', $cell);
            $this->assertNotNull($cell->phone);
        }
    }

    public function testMarriage(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstName' => 'John',
                'lastName' => 'Smith',
                'state' => 'CA',
                'county' => ''
        );

        $results = $npd->marriageSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $marriage) {
            $this->assertInstanceOf('Icm_Struct_Marriagerecord', $marriage);
            $this->assertNotNull($marriage->certificate_number);

            // two people involved
            $this->assertNotNull($marriage->first_person_first_name);
            $this->assertNotNull($marriage->first_person_last_name);
            $this->assertNotNull($marriage->second_person_first_name);
            $this->assertNotNull($marriage->second_person_last_name);
        }
    }

    public function testDivorce(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'),
                                       $config->getOption('password'),
                                       $config->getOption('testmode'));
        $params = array(
                'firstName' => 'John',
                'lastName' => 'Williams',
                'state' => '',
                'county' => ''
        );

        $results = $npd->divorceSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $divorce) {
            $this->assertInstanceOf('Icm_Struct_Marriagerecord', $divorce);

            // two people involved
            $this->assertNotNull($divorce->first_person_first_name);
            $this->assertNotNull($divorce->first_person_last_name);
            $this->assertNotNull($divorce->second_person_first_name);
            $this->assertNotNull($divorce->second_person_last_name);
        }
    }

    public function testBirth(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'),
                                       $config->getOption('password'),
                                       $config->getOption('testmode'));
        $params = array(
                'firstName' => 'Joseph',
                'lastName' => 'Linn',
                'middlename' => 'Harris',
                'state' => 'CA'
        );
        $results = $npd->birthSearch($params);

        $this->assertGreaterThan(0, count($results));

        foreach ($results as $birth) {
            $this->assertInstanceOf('Icm_Struct_Birthrecord', $birth);
            $this->assertNotNull($birth->birth_county);
            $this->assertNotNull($birth->dob);
        }
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testDoctor(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstname' => 'John',
                'lastname' => 'Smith'
        );
        $npd->doctorSearch($params);
    }

    /**
     * @expectedException Icm_Exception
     */
    public function testDomain(){
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'npd');
        $npd = new Icm_Service_Npd_Api($config->getOption('username'), $config->getOption('password'), $config->getOption('testmode'));
        $params = array(
                'firstname' => 'John',
                'lastname' => 'Smith'
        );
        $npd->domainSearch($params);
    }
}
