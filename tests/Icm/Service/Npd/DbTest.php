<?php

class Icm_Service_Npd_Db_Test extends  PHPUnit_Framework_TestCase {

    public function testQueryString() {
        if (!extension_loaded('mssql')) {
            $this->markTestSkipped('MSSql Driver not loaded');
        }

        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql');
        $npdAdapter = new Icm_Service_Npd_Db(Icm_Db_Mssql::connect('npd', $config));
        $multipleCharacterWildcardStringPositive = 'Jo*';
        $multipleCharacterWildcardStringNegative = '*Jo';
        $normalString = 'John';

        //positive test case
        $result = Icm_Db::parseQueryString($multipleCharacterWildcardStringPositive);
        $this->assertTrue((($result[0] == ' like ') && ($result[1] == 'Jo%')));

        //negative test case
        $result = Icm_Db::parseQueryString($multipleCharacterWildcardStringNegative);
        $this->assertTrue((($result[0] === ' = ') && ($result[1] === '*Jo')));

        //benign
        $result = Icm_Db::parseQueryString($normalString);
        $this->assertTrue((($result[0] === ' = ') && ($result[1] === 'John')));

    }

    public static function provider() {
        return array(
            //normal
            array('Cobb','John','CA'),
            array('Smit*','John','CA'),
            array('Cobb','Jo*' ,'CA'),
            array('Cobb','John','C*'),
            array('Cobb','*'   ,'CA'),
            array( null  ,'John','CA'),
            array('Cobb', null ,'CA'),
            array('Cobb','John', null),
            array('Cobb', null , null),
            array( null  ,'John', null),
            array( null  , null ,'CA'),
            array('Smith','J*' , null), //this should test maxResults
        );
    }

    /**
     * @dataProvider provider
     */
    public function testGetPerson($lname,$fname,$state) {
        $this->markTestSkipped('MSSQL server unavailable in dev environment');

        if (!extension_loaded('mssql')) {
            $this->markTestSkipped('MSSql Driver not loaded');
        }

        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql');
        $npdAdapter = new Icm_Service_Npd_Db(Icm_Db_Mssql::connect('npd', $config));
        $args = array();

        if (!is_null($lname)) {
            $args['last_name'] = $lname;
        }

        if (!is_null($fname)) {
            $args['first_name'] = $fname;
        }

        if (!is_null($state)) {
            $args['state'] = $state;
        }

        //we should get an exception if there's no last name
        if (is_null($lname)) {
            $this->setExpectedException('Icm_Service_Exception');
        }

        if (count($args) < 2) {
            $this->setExpectedException('Icm_Service_Exception');
        }

        if (($lname == '*') || ($fname == '*') || ($state == '*')) {
            $this->setExpectedException('Icm_Service_Exception');
        }

        $rows = $npdAdapter->getPerson($args);
        $this->assertTrue(count($rows) > 0);
    }

    public function testReverseAddress(){
        $this->markTestSkipped('MSSQL server not available in dev environment');
        $args = array(
            'address'=>'1576 Chalcedony St',
            'city'=>'San Diego',
            'state'=>'CA',
            'zip'=>'92109'
        );
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql');
        $npdAdapter = new Icm_Service_Npd_Db(Icm_Db_Mssql::connect('npd', $config));
        $npdAdapter->reverseAddress($args);
    }

    public function testSexoffender(){
        $this->markTestSkipped('MSSQL server not available in dev environment');
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql2');
        $npdAdapter = new Icm_Service_Npd_Db(Icm_Db_Mssql::connect('npd', $config));
        $args = array(
            'zip'=>'43123',
            'state'=>'OH'
        );
    }
}
