<?php

class Icm_Service_Internal_Optout_Test extends PHPUnit_Framework_TestCase {

    /**
     * Connection used in every function
     *
     * @var Icm_Db_Pdo
     */
    private $conn;

    /**
     * Adapter used for testing
     *
     * @var Icm_Service_Internal_Optout
     */
    private $optoutAdapter;

    protected function setUp() {
        $this->conn = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));
        $this->optoutAdapter = new Icm_Service_Internal_Optout($this->conn);
    }

    public static function tearDownAfterClass() {
        $conn = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));
        $conn->execute("DELETE FROM optout_requests WHERE first_name = 'John' AND last_name = 'Smith'");
    }

    public function optoutRecords() {
        return array(
            array(array(
                'source_id' => $this->getRandomArrayValue(array(
                                    Icm_Service_Internal_Source::SOURCE_LEXIS,
                                    Icm_Service_Internal_Source::SOURCE_NPD
                              )),
                'external_person_id' => rand(0, 1200000000),
                'first_name' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(8, 16)),
                'last_name' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(4, 20)),
                'dob' => rand(0, 1200000000),
                'city' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(4, 8)),
                'state' => $this->getRandomArrayValue(array(
                            "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL",
                            "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA",
                            "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",
                            "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI",
                            "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"
                          )),
                'zip' => substr(str_shuffle('0123456789'), 0, 5)
           ))
       );
    }

    private function getRandomArrayValue($a) {
        shuffle($a);
        return current($a);
    }

    private function saveRecord(array $optoutData) {
        // write a random optout request to the db
        $optoutRequestId = $this->writeTestOptoutRequest();

        // set the optout request id in the optout data array
        $optoutData['optout_request_id'] = $optoutRequestId;

        // save the record in db and return the id
        $optoutData['id'] = $this->optoutAdapter->save(Icm_Struct_Optout::fromArray($optoutData));

        return $optoutData;
    }

    /**
     * @param $optoutData
     * @dataProvider optoutRecords
     */
    public function testGetOptoutRecordsById(array $optoutData) {
        // save the record
        $optoutData = $this->saveRecord($optoutData);

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutData['id']);

        // make sure we can grab the record
        $dbData = $this->optoutAdapter->getOptoutById($optoutData['id']);

        // make sure all the data sent to save is in the
        $this->assertEquals(0, count(array_diff($optoutData, $dbData)));

        // remove the object that was just created
        $this->optoutAdapter->remove($optoutData['id']);
    }

    /**
     * @dataProvider provider
     */
    public function testgetAllExternalPersonIds($id, $originalDate, $externalId) {
        $data = array();
        $result = $this->optoutAdapter->getAllExternalPersonIds();

        // get all external_person_id results
        foreach ($result as $key => $value){
            $data[] = $value['external_person_id'];
        }

        // Confirm that the query contains the record we received from provider method
        $this->assertContains($externalId, $data);

        // Make date create > 2 days
        $sql = "UPDATE optouts SET `date_created` = :date
            WHERE `id` = :id";
        $param = array(':date' => strtotime('-3 days'), ':id' => $id);
        $this->conn->execute($sql, $param);

        // Now run the query again:
        $result = $this->optoutAdapter->getAllExternalPersonIds();

        // Reset $data array:
        $data = array();

        // get all external_person_id results
        foreach ($result as $key => $value){
            $data[] = $value['external_person_id'];
        }

        // Confirm that the query does not contain the record we received
        // from provider method due to the time update we did
        $this->assertNotContains($externalId, $data);
    }

    public function provider() {
        $ops = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));

        // create three new optouts and optout requests
        $optoutRequestId1 = $this->writeTestOptoutRequest();
        $optoutRequestId2 = $this->writeTestOptoutRequest();
        $optoutRequestId3 = $this->writeTestOptoutRequest();

        $this->writeTestOptout($optoutRequestId1);
        $this->writeTestOptout($optoutRequestId2);
        $this->writeTestOptout($optoutRequestId3);

        $sql = "SELECT `id`, `date_created`, `external_person_id` FROM
            optouts WHERE `date_created` IS NOT NULL LIMIT 0, 1";
        $result = $ops->fetchAll($sql);
        $sql = "UPDATE optouts SET `date_created` = :date
            WHERE `id` = :id";
        $param = array(':date' => time(), ':id' => $result[0]['id']);
        $ops->execute($sql, $param);

        return $result;
    }

    protected function writeTestOptoutRequest() {
        $db = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));

        $optoutRequestId = rand(1, 1337);
        $params = array(
            $optoutRequestId,
            2,
            'John',
            'Smith',
        );

        $db->execute("INSERT INTO optout_requests (id, status, first_name, last_name) VALUES (?, ?, ?, ?)", $params);

        return $optoutRequestId;
    }

    protected function writeTestOptout($optoutRequestId) {
        $db = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));

        $optoutId = rand(1, 1337);
        $params = array(
            $optoutId,
            $optoutRequestId,
            'testpersonId' . $optoutId,
            'John',
            'Smith',
            time(),
        );

        $db->execute("INSERT INTO optouts (id, optout_request_id, external_person_id, first_name, last_name, date_created) VALUES (?, ?, ?, ?, ?, ?)", $params);

        return $optoutId;
    }

    /**
     * @param $optoutData
     * @dataProvider optoutRecords
     */
    public function testGetOptoutRecordsByOptoutRequestId(array $optoutData) {
        // save the record
        $optoutData = $this->saveRecord($optoutData);

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutData['id']);

        // make sure we can grab the records using optout request id
        $records = $this->optoutAdapter->getAllByOptoutRequestId($optoutData['optout_request_id']);

        // make sure we have at least one record coming back
        $this->assertGreaterThan(0, count($records));

        // make sure the optout request id matches for all records
        foreach ($records as $record) {
            $this->assertEquals($optoutData['optout_request_id'], $record['optout_request_id']);
        }

        // remove the object that was just created
        $this->optoutAdapter->remove($optoutData['id']);
    }

    /**
     * @param $optoutData
     * @dataProvider optoutRecords
     */
    public function testGetOptoutsSuccess(array $optoutData) {
        // save the record
        $optoutData = $this->saveRecord($optoutData);

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutData['id']);

        // set up the output filter
        $optoutFilter = array (
            'first_name' => $optoutData['first_name'],
            'last_name' => $optoutData['last_name'],
            'state' => $optoutData['state'],
       );

        // make sure we can grab the records using optout request id
        $records = $this->optoutAdapter->getOptouts($optoutFilter);

        // make sure there are multiple records coming back
        $this->assertGreaterThan(0, count($records));

        // remove the object that was just created
        $this->optoutAdapter->remove($optoutData['id']);
    }

    /**
     * @param $optoutData
     * @dataProvider optoutRecords
     */
    public function testGetOptoutsFailure(array $optoutData) {
        // save the record
        $optoutData = $this->saveRecord($optoutData);

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutData['id']);

        // set up the output filter
        $optoutFilter = array (
            'first_name' => md5(rand()),
            'last_name' => md5(rand()),
            'state' => md5(rand()),
       );

        // make sure we can grab the records using optout request id
        $records = $this->optoutAdapter->getOptouts($optoutFilter);

        $this->assertEquals(0, count($records));

        // remove the object that was just created
        $this->optoutAdapter->remove($optoutData['id']);
    }

    /**
     * @param $optoutData
     * @dataProvider optoutRecords
     */
    public function testUniqueSourceAndExternalId(array $optoutData) {
        // save the record
        $optoutData1 = $this->saveRecord($optoutData);

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutData1['id']);

        $code = null;

        try {
            // save the record again
            $optoutData2 = $this->saveRecord($optoutData);

            // remove the object that was just created
            $this->optoutAdapter->remove($optoutData2['id']);
        }
        catch (PDOException $e) {
            // duplicate key
            if ($e->getCode() == 23000) {
                $code = $e->getCode();
            }
        }

        $this->assertEquals(23000, $code);

        // remove the object that was just created
        $this->optoutAdapter->remove($optoutData1['id']);
    }
}
