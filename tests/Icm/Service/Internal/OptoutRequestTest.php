<?php

class Icm_Service_Internal_OptoutRequest_Test extends PHPUnit_Framework_TestCase {

    /**
     * Connection used in every function
     *
     * @var Icm_Db_Pdo
     */
    private $conn;

    /**
     * Adapter used for testing
     *
     * @var Icm_Service_Internal_OptoutRequest
     */
    private $optoutRequestAdapter;

    protected function setUp() {
        $this->conn = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));
        $this->optoutRequestAdapter = new Icm_Service_Internal_OptoutRequest($this->conn);
    }

    public function optoutRequestRecord() {
        return array(
            array(array(
                'status' => Icm_Service_Internal_OptoutRequest::STATUS_PENDING,
                'first_name' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(8, 16)),
                'last_name' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(4, 20)),
                'dob' => rand(0, 1200000000),
                'email' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(10, 16)) . '@instantcheckmate.com',
                'address' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(10, 20)),
                'city' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(4, 8)),
                'order_id' => rand(0, 1200000000),
                'ip_address' => rand(0, 1200000000),
                'notes' => substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, rand(20, 200)),
                'state' => $this->getRandomArrayValue(array(
                            "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL",
                            "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA",
                            "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",
                            "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI",
                            "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"
                          )),
                'zip' => substr(str_shuffle('0123456789'), 0, 5)
            ))
        );
    }

    private function getRandomArrayValue($a) {
        shuffle($a);
        return current($a);
    }

    /**
     * @param $optoutRequestData
     * @dataProvider optoutRequestRecord
     */
    public function testGetOptoutRequestById(array $optoutRequestData) {
        // save the record
        $optoutRequestData['id'] = $this->optoutRequestAdapter->save(Icm_Struct_OptoutRequest::fromArray($optoutRequestData));

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutRequestData['id']);

        // make sure we can grab the record
        $dbData = $this->optoutRequestAdapter->getOptoutRequestById($optoutRequestData['id']);

        // make sure all the data sent to save is in the
        $this->assertEquals(0, count(array_diff($optoutRequestData, $dbData)));

        // remove the object that was just created
        $this->optoutRequestAdapter->remove($optoutRequestData['id']);
    }

    /**
     * @param $optoutRequestData
     * @dataProvider optoutRequestRecord
     */
    public function testGetOptoutRequests(array $optoutRequestData) {
        // save the record
        $optoutRequestData['id'] = $this->optoutRequestAdapter->save(Icm_Struct_OptoutRequest::fromArray($optoutRequestData));

        // make sure we have a valid id coming back
        $this->assertGreaterThan(0, $optoutRequestData['id']);

        // make sure we can grab the records using optout request id
        $records = $this->optoutRequestAdapter->getOptoutRequests();

        // make sure we have at least one record coming back
        $this->assertGreaterThan(0, count($records));

        // remove the object that was just created
        $this->optoutRequestAdapter->remove($optoutRequestData['id']);
    }
}
