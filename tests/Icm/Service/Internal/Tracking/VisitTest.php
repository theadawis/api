<?php

class Icm_Service_Internal_Tracking_VisitTest extends PHPUnit_Framework_TestCase
{
    protected $cgDb;
    protected $visitDb;

    public function setUp() {
        parent::setUp();

        $cgDbConfig = Icm_Config::fromIni('/sites/api/config/development/defaults.ini', 'cgDb');
        $this->cgDb = Icm_Db_Pdo::connect('cgDb', $cgDbConfig);
        $this->visitDb = new Icm_Service_Internal_Tracking_Visit($this->cgDb);

        // clean out testing visits
        $sql = "DELETE FROM visitor_visits WHERE visit_id LIKE ?";
        $replacements = array('service_testing_%');
        $this->cgDb->execute($sql, $replacements);

        // clean out testing visitors
        $sql = "DELETE FROM visitors WHERE visitor_id LIKE ?";
        $replacements = array('service_testing_%');
        $this->cgDb->execute($sql, $replacements);
    }

    public function testGetMostRecentForVisitorId() {
        // pull the most recent visit for a non-existent visitor id
        $visitorId = 'service_testing_visitor_id';
        $visit = $this->visitDb->getMostRecentForVisitorId($visitorId);

        // should give back null
        $this->assertNull($visit);

        // create a new bare-bones visitor
        $createdDate = date('Y-m-d H:i:s');
        $sql = "INSERT INTO visitors (visitor_id, created, updated) VALUES (?, ?, ?)";
        $replacements = array($visitorId, $createdDate, $createdDate);
        $result = $this->cgDb->execute($sql, $replacements);
        $this->assertTrue($result > 0);

        // create a new bare-bones visit
        $visitId = 'service_testing_visit_id';
        $appId = 1;
        $sql = "INSERT INTO visitor_visits (visitor_id, visit_id, app_id, created, updated) VALUES (?, ?, ?, ?, ?)";
        $replacements = array($visitorId, $visitId, $appId, $createdDate, $createdDate);
        $result = $this->cgDb->execute($sql, $replacements);
        $this->assertTrue($result > 0);

        // pull the most recent visit for the visitor id
        $visit = $this->visitDb->getMostRecentForVisitorId($visitorId);

        // visit ids should match
        $this->assertInstanceOf('Icm_Struct_Tracking_Visit', $visit);
        $this->assertEquals($visitId, $visit->visit_id);

        // create a second visit for the visitor id
        sleep(5);
        $createdDate = date('Y-m-d H:i:s');
        $secondVisitId = 'service_testing_visit_id2';
        $sql = "INSERT INTO visitor_visits (visitor_id, visit_id, app_id, created, updated) VALUES (?, ?, ?, ?, ?)";
        $replacements = array($visitorId, $secondVisitId, $appId, $createdDate, $createdDate);
        $result = $this->cgDb->execute($sql, $replacements);
        $this->assertTrue($result > 0);

        // pull most recent visit
        $visit = $this->visitDb->getMostRecentForVisitorId($visitorId);

        // visit id should match second visit
        $this->assertInstanceOf('Icm_Struct_Tracking_Visit', $visit);
        $this->assertEquals($secondVisitId, $visit->visit_id);

        // create third visit that is marked as duplicate
        sleep(5);
        $createdDate = date('Y-m-d H:i:s');
        $thirdVisitId = 'service_testing_visit_id3';
        $isDuplicate = 1;
        $sql = "INSERT INTO visitor_visits (visitor_id, visit_id, app_id, created, updated, is_duplicate) VALUES (?, ?, ?, ?, ?, ?)";
        $replacements = array($visitorId, $thirdVisitId, $appId, $createdDate, $createdDate, $isDuplicate);
        $result = $this->cgDb->execute($sql, $replacements);
        $this->assertTrue($result > 0);

        // pull most recent visit
        $visit = $this->visitDb->getMostRecentForVisitorId($visitorId);

        // visit id should match second visit
        $this->assertInstanceOf('Icm_Struct_Tracking_Visit', $visit);
        $this->assertEquals($secondVisitId, $visit->visit_id);

        // clear out the visits
        $sql = "DELETE FROM visitor_visits WHERE visit_id IN (?, ?, ?)";
        $replacements = array($visitId, $secondVisitId, $thirdVisitId);
        $this->cgDb->execute($sql, $replacements);

        // clear out the visitor
        $sql = "DELETE FROM visitors WHERE visitor_id = ?";
        $replacements = array($visitorId);
        $this->cgDb->execute($sql, $replacements);
    }
}