<?php

class Icm_Service_Lexis_Storage_ElasticsearchTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Config
     */
    protected $config;

    /**
     * @var Icm_Service_Lexis_Storage_Elasticsearch
     */
    protected $elasticsearch;

    const  INDEX = "basic";

    protected function setUp(){
        //ES config is needed for our cleanup
        $this->esConfig = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'elasticsearch');

        // Load the Storage from a DataContainer
        $this->container = new Icm_Container_Factory();
        $this->container->loadXml(simplexml_load_file('/sites/api/tests/config/' . 'services/dataContainer.xml'));
        $this->lexisStorage = $this->container->get('LexisBasicStorage');

        // Set up our dummy document
        $this->documentId = '000123';
        $this->dummyReport = file_get_contents(LIBRARY_PATH . "/tests/Icm/Service/Lexis/Storage/report_data.xml");

        // Make sure document does not already exsist before we start
        $this->cleanup();
    }

    public function testTestKeyNotPresent(){
        $result = $this->lexisStorage->test($this->documentId);
        $this->assertFalse($result);
    }

    public function testSave(){
        $result = $this->lexisStorage->save($this->dummyReport, $this->documentId);
        $this->assertTrue($result);
    }

    public function testTestKeyPresent(){
        $result = $this->lexisStorage->test($this->documentId);
        $this->assertTrue($result);
        return;
    }

    public function testLoad(){
        $result = $this->lexisStorage->load($this->documentId);
        $result = simplexml_load_string($result);
        $this->assertInstanceOf('SimpleXMLElement', $result);
        $this->cleanup();
    }

    private function cleanup(){
        $configHosts = array();

        foreach ($this->esConfig as $host){
            $host = explode(':', $host);
            $configHosts[] = array('host' => $host[0], 'port' => $host[1]);
        }

        $this->elasticsearch = new Icm_Service_Internal_Elasticsearch_Client(array('servers' => $configHosts));

        $type = $this->elasticsearch->getIndex('report')
            ->getType(self::INDEX);

        $a = $type->deleteById($this->documentId);
    }
}
