<?php

class Icm_Service_Lexis_Rest_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Config
     */
    protected $config;

    /**
     * @var Icm_Service_Lexis_Rest
     */
    protected $rest;

    protected function setUp(){
        $this->config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'lexis');
        $this->rest = new Icm_Service_Lexis_Rest($this->config->getOption('username'), $this->config->getOption('password'));
        $this->rest->setConfig($this->config);
    }

    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testWirelessReversePhoneLookup($phone) {
        $result = $this->rest->directoryAssistanceWirelessSearch(array('PhoneNumber' => $phone));
        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @dataProvider phoneProvider
     * @param $phone
     */
    public function testLandReversePhoneLookup($phone) {
        $result = $this->rest->directoryAssistanceReverseSearch(array('Phone10' => $phone));
        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    /**
     * @expectedException Icm_Service_Lexis_Exception
     */
    public function testNoParametersLookup() {
        $result = $this->rest->directoryAssistanceReverseSearch();
    }

    public function testWrongParametersLookup() {
        $result = $this->rest->directoryAssistanceReverseSearch(array('Phone10' => 'abcdefghi'));
        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    public function testPersonSearch(){
        $result = $this->rest->personSearch(array('UniqueId'=>''), array());
        $this->assertInstanceOf('SimpleXMLElement', $result);
    }

    public function testComprehensiveSearch(){
        $result = $this->rest->comprehensiveSearch(array(
            'UniqueId'=>''
            ),
            array(
                'IncludeAKAs'=>true,
                'IncludeImposters'=>true,
                'IncludeOldPhones'=>true,
                'IncludeAssociates'=>true,
                'IncludeProperties'=>true,
                'IncludePriorProperties'=>true,
                'IncludeCurrentProperties'=>true,
                'IncludeDriversLicenses'=>true,
                'IncludeMotorVehicles'=>true,
                'IncludeBankruptcies'=>true,
                'IncludeLiensJudgments'=>true,
                'IncludeCorporateAffiliations'=>true,
                'IncludeUCCFilings'=>true,
                'IncludeFAACertificates'=>true,
                'IncludeCriminalRecords'=>true,
                'IncludeCensusData'=>true,
                'IncludeAccidents'=>true,
                'IncludeWaterCrafts'=>true,
                'IncludeProfessionalLicenses'=>true,
                'IncludeHealthCareSanctions'=>true,
                'IncludeDEAControlledSubstance'=>true,
                'IncludeVoterRegistrations'=>true,
                'IncludeHuntingFishingLicenses'=>true,
                'IncludeWeaponPermits'=>true,
                'IncludeSexualOffenses'=>true,
                'IncludeCivilCourts'=>true,
                'IncludeFAAAircrafts'=>true,
                'IncludePeopleAtWork'=>true,
                'IncludeHighRiskIndicators'=>true,
                'IncludeForeclosures'=>true,
                'IncludePhonesPlus'=>true,
                'IncludeStudentInformation'=>true,
                'DoPhoneReport'=>true,
                'Relatives'=>array(
                    'IncludeRelatives'=>true,
                    'MaxRelatives'=>100,
                    'RelativeDepth'=>100,
                    'IncludeRelativeAddresses'=>true,
                    'MaxRelativeAddresses'=>100
                ),
                'Neighbors'=>array(
                    'IncludeNeighbors'=>true,
                    'IncludeHistoricalNeighbors'=>true,
                    'NeighborhoodCount'=>100,
                    'NeighborCount'=>100
                ),
                'MaxAddresses'=>100,
                'IncludeEmailAddresses'=>1
            )
        );
        $data = print_r($result, true);
    }

    public function phoneProvider() {
        return array(
            array('8584052745'),
            array('4135637167'),
            array('8313595555'),
            array('6192971888'),
            array('6197024419'),
            array('9782497413'),
            array('9782499834'),
            array('5083643339'),
            array('8584562111'),
            array('6192200268'),
            array('6192762800'),
            array('8582684933'),
        );
    }

    public function nameProvider() {
        return array(array('Tyler', 'Clayton', 'CA'));
        return array(
            array('Chris', 'Guiney', 'CA'),
            array('Christopher', 'Guiney', 'MA'),
            array('Kristian', 'Kibak', 'CA'),
            array('Nathan', 'Cobb', 'CA'),
            array('Joey', 'Rocco', 'CA'),
            array('Don', 'Son', 'CA'),
            array('John', 'Smith', 'CA')
        );
    }
}
