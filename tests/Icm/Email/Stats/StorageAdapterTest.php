<?php

class Icm_Email_Stats_StorageAdapter_Test extends TestCase {

    /**
     * @var Icm_Config
     */
    protected $conn;

    /**
     * @var Icm_Email_Stats_StorageAdapter
     */
    protected $statStorageAdapter;

    public function setUp() {
        $config = Icm_Config::fromIni('/sites/api/tests/config/config.ini');
        $this->conn = Icm_Db_Pdo::connect('cg_internal', $config->getSection('cgDb'));
        $this->statAdapter = new Icm_Email_Stats_StorageAdapter($this->conn);
    }

    public function testSaveWithInsert() {
        $stats = new Icm_Email_Stats;
        $stats->email_stat_rule = Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER;
        $stats->email_stat_count = 1;

        $this->statAdapter->getByRuleCreated(Icm_Email::EMAIL_RULE_INTERNAL_INVALID_CHARACTER, date('Y-m-d'));
    }

    public function testSaveWithUpdate() {
        return;
        $emailStatId = rand(1, 1000000);
        $emailStat = $this->generateEmailStat(array('email_stat_id' => $emailStatId, 'email_stat_count' => 11));

        $this->conn->expects($this->once())
                   ->method('execute')
                   ->with($this->stringStartsWith('UPDATE'))
                   ->will($this->returnValue(true));

        $this->statStorageAdapter->save($emailStat);
        $this->assertEquals($emailStatId, $emailStat->email_stat_id);
        $this->assertEquals(11, $emailStat->email_stat_count);
    }

    public function testGetByRuleCreated() {
        return;
        $emailStat = $this->generateEmailStat();

        $this->conn->expects($this->once())
                   ->method('fetch')
                   ->will($this->returnValue($emailStat));

        $result = $this->statStorageAdapter->getByRuleCreated(101, date('Y-m-d'));

        $this->assertTrue(is_a($result, "Icm_Email_Stats"));
        $this->assertEquals(141, $result->email_stat_id);
    }
}
