<?php

class Icm_OAuth2_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenEmpty() {
        // create a new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to verify an empty token
        $oauth->verifyAccessToken('');
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenNull() {
        // setup storage to return null for token
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue(null));

        // create new oauth object
        $oauth = new Icm_OAuth2($storage);

        // try to verify a null token
        $oauth->verifyAccessToken('flibberdigibbet');
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenMissingClientId() {
        // create token missing client id
        $params = array(
            'userID' => 'testUserId',
            'expires' => time() + 3600,
            'token' => 'testtoken',
        );
        $token = Icm_OAuth2_Accesstoken::fromArray($params);

        // set storage to return that token
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue($token));

        // create oauth object with token
        $oauth = new Icm_OAuth2($storage);

        // try to verify missing client id token
        $oauth->verifyAccessToken($params['token']);
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenExpired() {
        // create token with expired time
        $params = array(
            'clientID' => 'testClientId',
            'userID' => 'testUserId',
            'expires' => time() - 3600,
            'token' => 'testtoken',
        );
        $token = Icm_OAuth2_Accesstoken::fromArray($params);

        // set storage to return that token
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue($token));

        // create oauth object with token
        $oauth = new Icm_OAuth2($storage);

        // try to verify expired token
        $oauth->verifyAccessToken($params['token']);
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenNoScope() {
        // create token with no scope
        $params = array(
            'clientID' => 'testClientId',
            'userID' => 'testUserId',
            'expires' => time() + 3600,
            'token' => 'testtoken',
        );
        $token = Icm_OAuth2_Accesstoken::fromArray($params);

        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue($token));

        // create oauth object with token
        $oauth = new Icm_OAuth2($storage);

        // try to verify expired token
        $scope = 'testscope1 testscope2';
        $oauth->verifyAccessToken($params['token'], $scope);
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testVerifyAccessTokenBadScope() {
        // create token with bad scope
        $params = array(
            'clientID' => 'testClientId',
            'userID' => 'testUserId',
            'expires' => time() + 3600,
            'token' => 'testtoken',
            'scope' => 'testscope1 testscope2',
        );
        $token = Icm_OAuth2_Accesstoken::fromArray($params);

        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue($token));

        // create oauth object with storage
        $oauth = new Icm_OAuth2($storage);

        // try to verify expired token
        $scope = 'noscope';
        $oauth->verifyAccessToken($params['token'], $scope);
    }

    /**
     * Check normal performance of verify access token
     */
    public function testVerifyAccessToken() {
        // create valid token
        $params = array(
            'clientID' => 'testClientId',
            'userID' => 'testUserId',
            'expires' => time() + 3600,
            'token' => 'testtoken',
            'scope' => 'testscope1 testscope2',
        );
        $token = Icm_OAuth2_Accesstoken::fromArray($params);

        // set storage to return it
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('getAccessToken')->will($this->returnValue($token));

        // create oauth object with storage
        $oauth = new Icm_OAuth2($storage);

        // verify the token
        $scope = 'testscope1';
        $result = $oauth->verifyAccessToken($params['token'], $scope);

        // should get back the token
        $this->assertEquals($token, $result);
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testGetBearerTokenMultipleMethods() {
        // set get and post param name tokens
        $_GET[Icm_OAuth2::PARAM_NAME_TOKEN] = 'testgetparamnametoken';
        $_POST[Icm_OAuth2::PARAM_NAME_TOKEN] = 'testpostparamnametoken';

        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to fetch the bearer token
        $oauth->getBearerToken();
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testGetBearerTokenNoMethods() {
        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to fetch the bearer token without setting method tokens
        $oauth->getBearerToken();
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testGetBearerTokenBadHeaders() {
        // set nonsense server header
        $_SERVER['HTTP_AUTHORIZATION'] = 'testserverauth';

        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to fetch the bearer token
        $oauth->getBearerToken();
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testGetBearerTokenBadPost() {
        // set post param name token
        $_POST[Icm_OAuth2::PARAM_NAME_TOKEN] = 'testpostparamtoken';

        // set server request method to get
        $_SERVER['REQUEST_METHOD'] = 'GET';

        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to fetch the bearer token
        $oauth->getBearerToken();
    }

    /**
     * @expectedException Icm_OAuth2_Authexception
     */
    public function testGetBearerTokenBadContentType() {
        // set post param name token
        $_POST[Icm_OAuth2::PARAM_NAME_TOKEN] = 'testpostparamtoken';

        // set server request method to post
        $_SERVER['REQUEST_METHOD'] = 'POST';

        // set server content type to non-form
        $_SERVER['CONTENT_TYPE'] = 'application/testing';

        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // try to fetch the bearer token
        $oauth->getBearerToken();
    }

    /**
     * Test normal operation of getBearerToken method
     */
    public function testGetBearerToken() {
        // set the get param name token
        $getToken = 'testinggetparamtoken';
        $_GET[Icm_OAuth2::PARAM_NAME_TOKEN] = $getToken;

        // create an oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // fetch the bearer token
        $token = $oauth->getBearerToken();

        // should get back the get param name token
        $this->assertEquals($getToken, $token);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenNoGrantType() {
        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // build input array without grant type
        $input = array();

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenFilteredGrantType() {
        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // build input array with bad grant type
        $input = array(
            'grant_type' => 'testing',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenBadGrantType() {
        // create new oauth object with storage setup to check credentials and grant type
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(false));
        $oauth = new Icm_OAuth2($storage);

        // build input array with a grant type
        $input = array(
            'grant_type' => 'token',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenNoClientCredentials() {
        // create new oauth object
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $oauth = new Icm_OAuth2($storage);

        // build input array missing client id
        $input = array(
            'grant_type' => 'token',
            'client_id' => '',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenBadClientCredentials() {
        // create new oauth object with storage setup to check credentials
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(false));
        $oauth = new Icm_OAuth2($storage);

        // build input array with a grant type
        $input = array(
            'grant_type' => 'token',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenNoGrantCodeStorage() {
        // create new oauth object with storage setup to check credentials and grant type
        $storage = $this->getMock('Icm_OAuth2_Storage_Interface');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));
        $oauth = new Icm_OAuth2($storage);

        // build input array with auth code grant type
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeNoCode() {
        // create new oauth object with storage setup to check credentials and grant type
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));
        $oauth = new Icm_OAuth2($storage);

        // build input array with no code
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeNoRedirect() {
        // build storage object for oauth
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));

        // build oauth object with enforceRedirect setting
        $config = Icm_Config::fromArray(array('enforceRedirect' => true));
        $oauth = new Icm_OAuth2($storage, $config);

        // build input array missing redirect_uri
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeNoStoredCode() {
        // build storage object for oauth
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));
        $storage->expects($this->once())->method('getAuthCode')->will($this->returnValue(null));

        // build oauth object with enforceRedirect setting
        $oauth = new Icm_OAuth2($storage);

        // build input array with code
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
            'code' => 'thisisthecodedotcom',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeMismatchedClientId() {
        // build storage object for oauth
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));

        $storedCode = Icm_OAuth2_Authcode::fromArray(array(
            'clientID' => 'wrongclientId',
        ));
        $storage->expects($this->once())->method('getAuthCode')->will($this->returnValue($storedCode));

        // build oauth object with enforceRedirect setting
        $oauth = new Icm_OAuth2($storage);

        // build input array with different client id
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => 'testClientId',
            'client_secret' => 'whatliesbeneath',
            'code' => 'thisisthecodedotcom',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeMismatchedRedirect() {
        // build storage object for oauth
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));

        // setup stored code with mismatched redirect uri value
        $clientId = 'testClientId';
        $storedCode = Icm_OAuth2_Authcode::fromArray(array(
            'clientID' => $clientId,
            'redirectUri' => 'http://somethingelse.com',
        ));
        $storage->expects($this->once())->method('getAuthCode')->will($this->returnValue($storedCode));

        // build oauth object with enforceRedirect setting
        $oauth = new Icm_OAuth2($storage);

        // build input array with code
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => $clientId,
            'client_secret' => 'whatliesbeneath',
            'code' => 'thisisthecodedotcom',
            'redirect_uri' => 'http://redirect.com',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }

    /**
     * @expectedException Icm_OAuth2_Exception
     */
    public function testGrantAccessTokenAuthCodeExpired() {
        // build storage object for oauth
        $storage = $this->getMock('Icm_OAuth2_Storage_Grantcode');
        $storage->expects($this->once())->method('checkClientCredentials')->will($this->returnValue(true));
        $storage->expects($this->once())->method('checkGrantType')->will($this->returnValue(true));

        // setup stored code with mismatched redirect uri value
        $clientId = 'testClientId';
        $storedCode = Icm_OAuth2_Authcode::fromArray(array(
            'clientID' => $clientId,
            'expires' => time() - 3600,
        ));
        $storage->expects($this->once())->method('getAuthCode')->will($this->returnValue($storedCode));

        // build oauth object with enforceRedirect setting
        $oauth = new Icm_OAuth2($storage);

        // build input array with code
        $input = array(
            'grant_type' => 'authorization_code',
            'client_id' => $clientId,
            'client_secret' => 'whatliesbeneath',
            'code' => 'thisisthecodedotcom',
        );

        // try to grant an access token
        $oauth->grantAccessToken($input);
    }
}