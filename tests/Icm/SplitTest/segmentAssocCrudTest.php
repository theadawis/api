<?php

class Icm_SplitTest_SegmentAssocCrud_Test extends PHPUnit_Framework_Testcase
{
    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn, $this->redis);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // need app/context for test, context for segment, need test+var to create a test+var assoc
        $optParams = array('name' => 'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id += 0;

        $optParams = array('limit' => 1);
        $list = $this->mySplit->listContexts($app_id, $optParams);
        $section_id = $list['return'][0]['id'];
        $section_id += 0;

        $optParams['status'] = Icm_SplitTest::STATUS_LIVE;
        $list = $this->mySplit->listTests($app_id, $optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;

        $list = $this->mySplit->listSegments($app_id, $optParams);
        $segment_id = $list['return'][0]['id'];
        $segment_id +=0;

        $optParams = array('exclusive' => 1);
        $results = $this->mySplit->createTestSegmentAssoc($test_id, $segment_id, $optParams);

        $this->assertInternalType('array', $results);
        $this->assertEquals(1000, $results['status'], print_r($results, true));
        $test_segment_assoc_id = $results['return']['test_segment_assoc_id'];

        // test_segment_assoc/app id is a string when its pulled from the array, add 0 to cast it back to int
        $test_segment_assoc_id += 0;

        $this->assertNotEquals(0, $test_segment_assoc_id);

        // store app and context id in array and return
        $ret_arr = array($app_id, $test_segment_assoc_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $test_segment_assoc_id = $ret_arr[1];

        $optParams = array('test_segment_assoc_id' => $test_segment_assoc_id);
        $list = $this->mySplit->listTestSegmentAssocs($app_id, $optParams);

        $this->assertInternalType('array', $list);
        $this->assertEquals(1000, $list['status'], print_r($list, true));
        $this->assertEquals(1, count($list['return']), print_r($list, true));

        $test_segment_assoc = $list['return'][0];
        $this->assertEquals($test_segment_assoc['id'], $test_segment_assoc_id);

        return $test_segment_assoc_id;
    }

    /**
     * @depends testList
     * @param int $test_segment_assoc_id
     */
    public function testDelete($test_segment_assoc_id){
        $delResults = $this->mySplit->deleteTestSegmentAssoc($test_segment_assoc_id);
        $this->assertInternalType('array', $delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

