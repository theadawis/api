<?php

  /**
   * Description of StorageAdapterTest
   *
   * @author fadi
   */

class Icm_SplitTest_Test_StorageAdapter_Test extends PHPUnit_Framework_TestCase
{
    protected $tableName,$conn,$data,$SectionStorageAdapter,$dataToObject;

    protected function setUp(){
        // put variables in data array()
        $this->tableName = 'split_test';
        $this->data = array('id' => 1);
        $this->redis = $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $dbConfig = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgAnalyticsDb');
        $this->conn = Icm_Db_Pdo::connect('cgAnalyticsDb', $dbConfig);
        $this->storage = new Icm_SplitTest_Test_StorageAdapter($this->tableName,'id',$this->conn);

        // create data objects from the given data array:
        $this->dataToObject =  Icm_SplitTest_Test::fromArray($this->data);
    }

    /*
     * make sure returnArrayFromObject is returning an array when
     * its passed an object parameter.
     */
    public function testreturnArrayFromObject(){
        $expcted = $this->data;
        $actual = $this->storage->returnArrayFromObject($this->dataToObject);
        $this->assertEquals($expcted,$actual,'Returned value must be an array');
    }

    /*
     * Make sure data is fetched properly for active status:
     */
    public function testfetchDataActiveStatus(){
        // try for active status:
        $dataToInsert = array('app_id' => '1',
                              'stats' => true,'segments' => false,
                              'getSegments' => false,'section_id' => '1',
                              'status' => '3');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Test::fromArray($dataToInsert);
        $sql = "SELECT  `id`, `name`, `app_id`, `control_variation_id`, `section_id`, `type`, `status`, `desc`, `weight`, `status`,
                   `last_modified`, `start_time`, `end_time`
                FROM {$this->tableName}
                WHERE `app_id`= :app_id AND `section_id` = :section_id AND `status` = :status";

        $parameter = array('app_id' => '1','section_id' => '1','status' => '3');
        $expected = $this->conn->fetchAll($sql,$parameter);
        $this->assertEquals($expected,$this->storage->fetchData($newdataToObject));
    }

    /*
     * Make sure data is fetched properly for paused status:
     */
    public function testfetchDataPausedStatus(){
        // try for paused status:
        $dataToInsert = array('app_id' => '1',
                              'stats' => true,'segments' => false,
                              'getSegments' => false,'section_id' => '1',
                              'status' => '4');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Test::fromArray($dataToInsert);
        $sql = "SELECT  `id`, `name`, `app_id`, `control_variation_id`, `section_id`, `type`, `status`, `desc`, `weight`, `status`,
                   `last_modified`, `start_time`, `end_time`
                FROM {$this->tableName}
                WHERE `app_id`= :app_id AND `section_id` = :section_id AND `status` = :status";

        $parameter = array('app_id' => '1','section_id' => '1','status' => '4');
        $expected = $this->conn->fetchAll($sql,$parameter);
        $this->assertEquals($expected,$this->storage->fetchData($newdataToObject));
    }

    /*
     * Make sure data is fetched properly for active segments:
     */
    public function testfetchDataTrueSegment(){
        // try for active segments:
        $dataToInsert = array('app_id' => '1',
                              'stats' => true,'segments' => true,
                              'getSegments' => true,'section_id' => '1',
                              'status' => '3');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Test::fromArray($dataToInsert);
        $sql = "SELECT  `id`, `name`, `app_id`, `control_variation_id`, `section_id`, `type`, `status`, `desc`, `weight`, `status`,
                   `last_modified`, `start_time`, `end_time`
                FROM {$this->tableName}
                WHERE `app_id`= :app_id AND `section_id` = :section_id AND `status` = :status";

        $parameter = array('app_id' => '1','section_id' => '1','status' => '3');
        $expected = $this->conn->fetchAll($sql,$parameter);

        foreach ($expected as $key => $test){
            $testId = $test['id'];
            $sql = "SELECT `stsa`.`segment_id`, .`stsa`.`exclusive`, `stsa`.`exclude`, `cgs`.`key`, `cgs`.`value`, `cgs`.`super`, `cgs`.`app_id`
                    FROM `split_test_segment_assoc` as `stsa`
                    JOIN `cg_segment` as `cgs` ON `stsa`.`test_id` = :test_id AND `cgs`.`id` = stsa.`segment_id`";
            $params = array(':test_id' => $testId);
            $items = $this->conn->fetchAll($sql,$params);
            $test['segments'] = $items;
            $expected[$key] = $test;
        }

        $this->assertEquals($expected,$this->storage->fetchData($newdataToObject));
    }

    /*
     * Make sure primary goal is returned for the selected test id:
     */
    public function testlistPrimaryGoal(){
        $dataToInsert = array('app_id' => '1','test_id' => '1160');

        // convert data to object:
        $newdataToObject = Icm_SplitTest_Test::fromArray($dataToInsert);
        $sql = "SELECT `sg`.`id`, `sg`.`event_id`, `sg`.`test_id`, `sg`.`primary_goal`
               FROM `split_goal` as `sg`
               JOIN `cg_event_map` as `cem` ON(`cem`.`id` = `sg`.`event_id`)
               WHERE `sg`.`test_id` = :test_id
               ORDER BY `cem`.`order` ASC";

        $placement = array('test_id' => '1160');
        $result = $this->conn->fetchAll($sql,$placement);

        foreach ($result as $key => $value){
            if ($value['primary_goal']){
                $expected = $value['event_id'];
            }
        }

        $this->assertEquals($expected,$this->storage->listPrimaryGoal($newdataToObject));
    }

    /*
     * Make sure goals are returned for the selected test id:
     */
    public function testlistGoals(){
        $dataToInsert = array('app_id' => '1','test_id' => '1160');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Test::fromArray($dataToInsert);
        $sql = "SELECT `sg`.`id`, `sg`.`event_id`, `sg`.`test_id`, `sg`.`primary_goal`
                FROM `split_goal` as `sg`
                JOIN `cg_event_map` as `cem` ON(`cem`.`id` = `sg`.`event_id`)
                WHERE `sg`.`test_id` = :test_id
                ORDER BY `cem`.`order` ASC ";

        $placement = array('test_id' => '1160');
        $expected = $this->conn->fetchAll($sql,$placement);
        $this->assertEquals($expected,$this->storage->listGoals($newdataToObject));
    }

    public function testListTestSegmentAssocs() {
        // delete any lingering data
        $testAppId = 1337;
        $tests = $this->conn->fetchAll("SELECT id FROM split_test WHERE app_id = $testAppId");

        foreach ($tests as $test) {
            $testId = $test['id'];
            $this->conn->execute("DELETE FROM split_test_segment_assoc WHERE test_id = $testId");
            $this->conn->execute("DELETE FROM split_test WHERE id = $testId");
        }

        $this->conn->execute("DELETE FROM cg_application WHERE id = $testAppId");

        // create a new split test for a test app id
        $this->conn->execute("INSERT INTO cg_application (id, name) VALUES ($testAppId, 'testingapplication')");
        $this->conn->execute("INSERT INTO split_test "
                                . "(app_id, section_id, type, name, weight, status, last_modified) "
                                . "VALUES ($testAppId, 1, 'testing', 'testingtest1', 1, 1, 1234567)");
        $firstSplitTestId = $this->conn->lastInsert();

        // create some new split test segments for that test
        $this->conn->execute("INSERT INTO split_test_segment_assoc "
                                . "(test_id, segment_id) "
                                . "VALUES ($firstSplitTestId, 3), ($firstSplitTestId, 5), ($firstSplitTestId, 7)");

        // pull all the segments for the app id
        $criteria = array(
            'app_id' => $testAppId,
        );
        $segments = $this->storage->listTestSegmentAssocs($criteria);

        // should have given back the right number
        $this->assertCount(3, $segments);

        // segments returned should match
        foreach ($segments as $segment) {
            $this->assertEquals($firstSplitTestId, $segment['test_id']);
        }

        // create a second split test with a different section id for the test app
        $this->conn->execute("INSERT INTO split_test "
                                . "(app_id, section_id, type, name, weight, status, last_modified) "
                                . "VALUES ($testAppId, 2, 'testing', 'testingtest2', 2, 1, 1234577)");
        $secondSplitTestId = $this->conn->lastInsert();

        // create some split test segments for that test
        $this->conn->execute("INSERT INTO split_test_segment_assoc "
                                . "(test_id, segment_id) "
                                . "VALUES ($secondSplitTestId, 3), ($secondSplitTestId, 5), ($secondSplitTestId, 7)");
        $firstTestSegmentId = $this->conn->lastInsert();

        // pull the segments for the app and section
        $criteria = array(
            'app_id' => $testAppId,
            'section_id' => 2
        );
        $segments = $this->storage->listTestSegmentAssocs($criteria);

        // should have pulled back the right number
        $this->assertCount(3, $segments);

        // segments returned should match
        foreach ($segments as $segment) {
            $this->assertEquals($secondSplitTestId, $segment['test_id']);
        }

        // pull the segments for the app and first test
        $criteria = array(
            'app_id' => $testAppId,
            'test_id' => $firstSplitTestId,
        );
        $segments = $this->storage->listTestSegmentAssocs($criteria);

        // should have given back the right number
        $this->assertCount(3, $segments);

        // segments returned should match
        foreach ($segments as $segment) {
            $this->assertEquals($firstSplitTestId, $segment['test_id']);
        }

        // pull segments by test, app and segment id
        $criteria = array(
            'app_id' => $testAppId,
            'segment_id' => 5,
            'test_id' => $secondSplitTestId,
        );
        $segments = $this->storage->listTestSegmentAssocs($criteria);

        // should have returned the right segment
        $this->assertCount(1, $segments);
        $this->assertEquals($secondSplitTestId, $segments[0]['test_id']);
        $this->assertEquals(5, $segments[0]['segment_id']);

        // pull segment by app and test_segment_assoc_id
        $criteria = array(
            'app_id' => $testAppId,
            'test_segment_assoc_id' => $firstTestSegmentId,
        );
        $segments = $this->storage->listTestSegmentAssocs($criteria);

        // should have returned the right segment
        $this->assertCount(1, $segments);
        $this->assertEquals($firstTestSegmentId, $segments[0]['id']);
        $this->assertEquals($secondSplitTestId, $segments[0]['test_id']);
        $this->assertEquals(3, $segments[0]['segment_id']);

        // cleanup everything we created
        $this->conn->execute("DELETE FROM split_test_segment_assoc WHERE test_id IN ($firstSplitTestId, $secondSplitTestId)");
        $this->conn->execute("DELETE FROM split_test WHERE app_id = $testAppId");
        $this->conn->execute("DELETE FROM cg_application WHERE id = $testAppId");
    }

    // unset the object:
    protected function tearDown(){
        $this->conn = NULL;
        $this->data = array();
        $this->tableName = NULL;
        unset($this->storage);
        unset($this->dataToObject);
    }
}
