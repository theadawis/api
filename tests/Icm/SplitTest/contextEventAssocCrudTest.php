<?php

class Icm_SplitTest_ContextEventAssocCrud_Test extends PHPUnit_Framework_Testcase
{
    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn,$this->redis);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // need app/context for test, context for variation, need test+var to create a test+var assoc
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit'=>1);
        $list = $this->mySplit->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;

        $list = $this->mySplit->listEvents($app_id,$optParams);
        $event_id = $list['return'][0]['id'];
        $event_id +=0;

        $results = $this->mySplit->createContextEventAssoc($section_id,$event_id);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status'], print_r($results, true));
        $section_event_assoc_id = $results['return']['section_event_assoc_id'];
        $section_event_assoc_id += 0;

        $this->assertNotEquals(0, $section_event_assoc_id);

        // store app and context id in array and return
        $ret_arr = array($section_id,$section_event_assoc_id);

        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $section_id = $ret_arr[0];
        $section_event_assoc_id = $ret_arr[1];
        $optParams = array('section_event_assoc_id'=>$section_event_assoc_id);
        $list = $this->mySplit->listContextEventAssocs($section_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));
        $section_event_assoc = $list['return'][0];
        $this->assertEquals($section_event_assoc['id'],$section_event_assoc_id);

        return $section_event_assoc_id;
    }

    /**
     * @depends testList
     * @param int $section_event_assoc_id
     */
    public function testDelete($section_event_assoc_id){
        $delResults = $this->mySplit->deleteContextEventAssoc($section_event_assoc_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

