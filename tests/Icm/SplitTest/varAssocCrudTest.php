<?php

class Icm_SplitTest_VariationAssocCrud_Test extends PHPUnit_Framework_Testcase
{
    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn,$this->redis);
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // need app/context for test, context for variation, need test+var to create a test+var assoc
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit'=>1);
        $list = $this->mySplit->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;
        $list = $this->mySplit->listTests($app_id,$optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;
        $list = $this->mySplit->listVariations($optParams);

        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        $optParams = array('weight'=>1,'active'=>1);
        $results = $this->mySplit->createTestVariationAssoc($test_id,$variation_id,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status'], print_r($results, true));
        $test_variation_assoc_id = $results['return']['test_variation_assoc_id'];

        // test_variation_assoc/app id is a string when its pulled from the array, add 0 to cast it back to int
        $test_variation_assoc_id += 0;

        $this->assertNotEquals($test_variation_assoc_id, 0);

        // store app and context id in array and return
        $ret_arr = array($test_id,$test_variation_assoc_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $test_id = $ret_arr[0];
        $test_variation_assoc_id = $ret_arr[1];

        $optParams = array('test_variation_assoc_id'=>$test_variation_assoc_id);
        $list = $this->mySplit->listTestVariationAssocs($test_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));

        $test_variation_assoc = $list['return'][0];

        $this->assertEquals($test_variation_assoc['id'],$test_variation_assoc_id);

        return $test_variation_assoc_id;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testUpdate($ret_arr){
        $test_id = $ret_arr[0];
        $test_variation_assoc_id = $ret_arr[1];

        $optParams = array('active'=>1,'weight'=>4);
        $list = $this->mySplit->updateTestVariationAssoc($test_variation_assoc_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        return $ret_arr;
    }

    /**
     * @depends testList
     * @param int $test_variation_assoc_id
     */
    public function testDelete($test_variation_assoc_id){
        $delResults = $this->mySplit->deleteTestVariationAssoc($test_variation_assoc_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

