<?php

class Icm_SplitTest_Storage_Test extends PHPUnit_Framework_TestCase{

    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redconn = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn, $this->redconn);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testStorageGetTestMeta(){
        $optParams = array('name' => 'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $list = $this->mySplit->listVariations($optParams);

        // pick a random variation out of the list
        $randomVariationKey = array_rand($list['return']);
        $randomVariation = $list['return'][$randomVariationKey];
        $variation_id = $randomVariation['id'];
        $variation_id +=0;

        // save the variation's meta data for comparison later
        $variationMeta = $randomVariation['meta'];
        $expected = array();

        foreach ($variationMeta as $metaEntry) {
            $expected[$metaEntry['key']] = $metaEntry['value'];
        }

        // fetch the variation's meta data directly
        $meta = $this->mySplit->getTestMeta($variation_id);
        $this->assertEquals($expected, $meta);
    }

    public function testStorageGetTestByVariation(){
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);
        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $list = $this->mySplit->listVariations($optParams);
        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        $testVariation = $this->mySplit->getTestByVariation($variation_id);
        $this->assertFalse(empty($testVariation));
    }

    public function testIsSuperSegment(){
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);
        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $list = $this->mySplit->listSegments($app_id);
        $segment_id = $list['return'][0]['id'];
        $segment_id +=0;
        $segKey = $list['return'][0]['key'];
        $segValue = $list['return'][0]['value'];

        $isSuper = $this->mySplit->isSuperSegment($segKey,$segValue);
        $this->assertFalse($isSuper);
    }

    public function testGetSegmentId(){
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);
        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $list = $this->mySplit->listSegments($app_id);
        $segment_id = $list['return'][0]['id'];
        $segment_id +=0;
        $segKey = $list['return'][0]['key'];
        $segValue = $list['return'][0]['value'];

        $testSegId = $this->mySplit->getSegmentId($segKey,$segValue);

        $this->assertEquals($segment_id, intval($testSegId));
    }
}