<?php

class Icm_SplitTest_SegmentCrud_Test extends PHPUnit_Framework_Testcase{
    public function testCreate(){
        // although we're not testing application list here, we need it to grab an app id
        // for us since we must have the new segment referencing an app id
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('name'=>'InstantCheckmate');
        $list = $mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id += 0;

        // now we have the app_id, use it to create a segment
        $key = "src";
        $value = "6527";
        $optParams = array('super'=>0);
        $results = $mySplit->createSegment($key,$value,$app_id,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status']);
        $segment_id = $results['return']['segment_id'];

        // segment/app id is a string when its pulled from the array, add 0 to cast it back to int
        $segment_id += 0;

        $this->assertNotEquals(0, $segment_id);

        // store app and context id in array and return
        $ret_arr = array($app_id,$segment_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $segment_id = $ret_arr[1];

        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('segment_id'=>$segment_id);
        $list = $mySplit->listSegments($app_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));

        $segment = $list['return'][0];
        $this->assertEquals($segment['id'],$segment_id);
        $this->assertInternalType('string',$segment['key']);
        $this->assertInternalType('string',$segment['value']);

        return $segment_id;
    }

    /**
     * @depends testList
     * @param int $segment_id
     */
    public function testDelete($segment_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $delResults = $mySplit->deleteSegment($segment_id);

        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

