<?php

/**
 * Description of StorageAdapterTest
 *
 * @author fadi
 */
class Icm_SplitTest_Section_StorageAdapter_Test extends TestCase
{
    protected $tableName,$conn,$data,$SectionStorageAdapter,$dataToObject,$redis;

    public function setUp(){
        $this->tableName = 'split_section';
        $this->data = array('app_id' => 1); //Icm_Config $config
        $this->redisConfig = Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis');//$this->getMock("Icm_Config",array());
        $this->redis = new Icm_Redis($this->redisConfig);
        $this->conn = Icm_Db_Pdo::connect('cgAnalyticsDb',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cgAnalyticsDb'));
        $this->SectionStorageAdapter = new Icm_SplitTest_Section_StorageAdapter($this->tableName,'id',$this->conn);
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        // create data objects from the given data array:
        $this->dataToObject =  Icm_SplitTest_Section::fromArray($this->data);

    }

    /*
     * make sure returnArrayFromObject is returning an array when
     * its passed an object parameter.
     */
    public function testreturnArrayFromObject(){
        $expcted = $this->data;
        $actual = $this->SectionStorageAdapter->returnArrayFromObject($this->dataToObject);
        $this->assertEquals($expcted,$actual,'Returned value must be an array');
    }

    /*
     * Make sure that our getMaxValue method is returning the maximum value
     * of our order field which is located in the split_section table..
     */
    public function testgetMaxValue(){
        $sql = "SELECT MAX(`order`) as `order` FROM `{$this->tableName}` LIMIT 0,1";
        $results = $this->conn->fetchAll($sql);
        $expected = $results[0]['order'];
        $classFunction = $this->SectionStorageAdapter->getMaxValue('order');
        $actual = $classFunction[0]['order'];
        $this->assertEquals($expected,$actual);
    }

    /*
     * Make sure that our fetchData method is returning expected
     * data with given parameters.
     */
    public function testfetchData(){
        $sql = "SELECT * FROM `{$this->tableName}`
                WHERE app_id = :app_id ORDER BY `order`";
        $parameter = array('app_id'=>'1');
        $expected = $this->conn->fetchAll($sql,$parameter);
        $actual = $this->SectionStorageAdapter->fetchData($this->dataToObject);
        $this->assertEquals($expected,$actual);
    }

    /*
     * Make sure our data is saved.
     */
    public function testsaveData(){
        $slug = 'test_'.time();
        $dataToInsert = array('app_id'=>'1','name'=>'testSaveData','type'=>'testSaveData33','slug'=>$slug);

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Section::fromArray($dataToInsert);

        /*
         * When the process is successful, the status is set to 1000. Please note
         * that field slug is unique and must not be duplicated. An error will
         * be thrown if you run the test twice with the same slug name
         */
        $this->assertEquals(true,$this->SectionStorageAdapter->saveData($newdataToObject),'Make sure slug is not duplicated');
    }

    /**
     * Make sure data is delete:
     */
    public function testdeleteData(){
        // select an id:
        $dataToInsert = array('id'=>'22');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Section::fromArray($dataToInsert);

        /*
         * When the process is successful, the status is set to 1000.
         */
        $this->assertEquals(true,$this->SectionStorageAdapter->deleteData($newdataToObject,$this->redis),'Delete failed. Make sure the id you inserted exists');
    }

    // unset the object:
    protected function tearDown(){
        $this->conn = NULL;
        $this->data = array();
        $this->tableName = NULL;
        $this->redis = NULL;
        unset($this->SectionStorageAdapter);
        unset($this->dataToObject);
    }
}
