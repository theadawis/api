<?php

class Icm_SplitTest_MetaCrud_Test extends PHPUnit_Framework_Testcase
{
    public function testCreate(){
        // although we're not testing application list here, we need it to grab an app id
        // for us since we must have the new meta referencing an app id
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('name'=>'InstantCheckmate');
        $list = $mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];

        // meta is a string when its pulled from the array, add 0 to cast it back to int
        $app_id += 0;

        // now we have the app_id, use it to create a meta
        $key = "buttonColor";
        $value = "Green";

        $results = $mySplit->createMeta($key,$value,$app_id);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status']);
        $meta_id = $results['return']['meta_id'];

        // meta is a string when its pulled from the array, add 0 to cast it back to int
        $meta_id += 0;
        $app_id += 0;
        $this->assertNotEquals(0, $meta_id);

        // store app and meta id in array and return
        $ret_arr = array($app_id,$meta_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $meta_id = $ret_arr[1];

        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('meta_id'=>$meta_id);
        $list = $mySplit->listMetas($app_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));
        $meta = $list['return'][0];
        $this->assertEquals($meta['id'],$meta_id);
        $this->assertInternalType('string',$meta['key']);
        $this->assertInternalType('string',$meta['value']);

        return $meta_id;
    }

    /**
     * @depends testList
     * @param int $meta_id
     */
    public function testDelete($meta_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $delResults = $mySplit->deleteMeta($meta_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

