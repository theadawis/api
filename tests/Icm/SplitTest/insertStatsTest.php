<?php

class Icm_SplitTest_AggStats_Test extends PHPUnit_Framework_Testcase
{
    public function testInsertTestAggStat(){
        // although we're not testing application/segment/context list here, we need it to grab the associated id's
        // for us since we must have the new Test referencing one of each
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplitSummary = new Icm_SplitTest_Summary($conn,$redis);

        $optParams = array('name' => 'InstantCheckmate');
        $list = $mySplitSummary->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $optParams = array('limit' => 1);
        $list = $mySplitSummary->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;

        $optParms = array('section_id' => $section_id);
        $list = $mySplitSummary->listVariations($optParams);
        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        $list = $mySplitSummary->listTests($app_id,$optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;

        $list = $mySplitSummary->listEvents($app_id,$optParams);

        $event_id = $list['return'][0]['id'];
        $event_id +=0;

        // test agg stat insert
        $testArray = array(
            array('test_id' => $test_id,'event_id' => $event_id,'views' => 100,'conversions' => 50,'conversion_rate' => 0.5000),
            array('test_id' => $test_id,'event_id' => $event_id,'views' => 100,'conversions' => 50,'conversion_rate' => 0.5000)
        );

        $result = $mySplitSummary->insertTestAggStat($testArray);

        $this->assertInternalType('array',$result);
        $this->assertEquals(1000, $result['status']);
    }

    public function testInsertVarAggStat(){
        // although we're not testing application/segment/context list here, we need it to grab the associated id's
        // for us since we must have the new Test referencing one of each
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplitSummary = new Icm_SplitTest_Summary($conn,$redis);

        $optParams = array('name' => 'InstantCheckmate');
        $list = $mySplitSummary->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;

        $optParams = array('limit' => 1);
        $list = $mySplitSummary->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;

        $optParms = array('section_id' => $section_id);
        $list = $mySplitSummary->listVariations($optParams);
        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        $list = $mySplitSummary->listTests($app_id,$optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;

        $list = $mySplitSummary->listEvents($app_id,$optParams);

        $event_id = $list['return'][0]['id'];
        $event_id +=0;

        $varAggStat =array(
            array(
                'test_id' => $test_id,
                'event_id' => $event_id,
                'variation_id' => $variation_id,
                'standard_deviation' => 45.1120,
                'estimated_standard_error' => 45.1111,
                'standard_error' => 44.1123,
                'improvement' => 34.0000,
                'pooled_sample_proportion' => 343.2333,
                'two_proportion_obt_z' => 432.2283,
                'expected_chi_squared' => 7465.9478,
                'chi_squared' => 3456.6666,
                'effect_size_chi_squared' => 123.9870,
                'chi_significance' => 1,
                'obt_z_significance' => 1,
                'obt_z_difference' => 58.0099,
                'normal_distribution' => 34.0009,
                'views' => 20000,
                'conversions' => 10000,
                'conversion_rate' => 0.5000
            ),array(
                'test_id' => $test_id,
                'event_id' => $event_id,
                'variation_id' => $variation_id,
                'standard_deviation' => 6645.1120,
                'estimated_standard_error' => 45.1111,
                'standard_error' => 6644.1123,
                'improvement' => 6634.0000,
                'pooled_sample_proportion' => 66343.2333,
                'two_proportion_obt_z' => 66432.2283,
                'expected_chi_squared' => 667465.9478,
                'chi_squared' => 6666.6666,
                'effect_size_chi_squared' => 66123.9870,
                'chi_significance' => 0,
                'obt_z_significance' => 0,
                'obt_z_difference' => 6658.0099,
                'normal_distribution' => 6634.0009,
                'views' => 20000,
                'conversions' => 10000,
                'conversion_rate' => 0.5000
            ));

        $result = $mySplitSummary->insertVarAggStat($varAggStat);

        $this->assertInternalType('array',$result);
        $this->assertEquals(1000, $result['status']);
    }

    public function testGetAggStats(){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplitSummary = new Icm_SplitTest_Summary($conn,$redis);

        $optParams = array('name' => 'InstantCheckmate');
        $list = $mySplitSummary->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $testAggStats = $mySplitSummary->getTestAggStats($app_id);

        $varAggStats = $mySplitSummary->getVariationAggStats($app_id);

        $this->assertInternalType('array',$testAggStats);
        $this->assertEquals(1000, $testAggStats['status']);

        $this->assertInternalType('array',$varAggStats);
        $this->assertEquals(1000, $varAggStats['status']);

        $viewsNeededValue = null;

        // check for viewsNeeded in varAggStats
        foreach ($varAggStats['return'] as $stats) {
            // viewsNeeded should always be set, but might be null
            $this->assertTrue(array_key_exists('viewsNeeded', $stats));
        }
    }
}
