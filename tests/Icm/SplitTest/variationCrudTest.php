<?php

class Icm_SplitTest_VariationCrud_Test extends PHPUnit_Framework_Testcase{

    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn,$this->redis);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // we need an app_id to get a context
        // we need a context id and 1 or more meta ids create a variation
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);
        $app_id = $list['return'][0]['id'];
        $app_id += 0;

        $optParams = array('limit'=>1);
        $list = $this->mySplit->listContexts($app_id,$optParams);

        $section_id = $list['return'][0]['id'];

        // context id is a string here, add 0 to cast it back to int
        $section_id += 0;

        // we need meta id's to create a variation
        $optParams = array('limit'=>3);
        $list = $this->mySplit->listMetas($app_id,$optParams);
        $meta_ids = $list['return'];
        $metastring = "";

        foreach ($meta_ids as $meta){
            if ($metastring == "") {
                $metastring .= $meta['id'];
            }
            else {
                $metastring .= "," .$meta['id'];
            }
        }

        $label = "StandardVariation";
        $status = 1;
        $type = "mytype";
        $optParams = array('status'=>$status);
        $results = $this->mySplit->createVariation($label,$metastring,$section_id,$type,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status']);
        $variation_id = $results['return']['variation_id'];

        // variation is a string when its pulled from the array, add 0 to cast it back to int
        $variation_id += 0;
        $this->assertNotEquals($variation_id, 0);

        // store app and variation id in array and return
        $ret_arr = array($section_id,$variation_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $section_id = $ret_arr[0];
        $variation_id = $ret_arr[1];

        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $this->mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('variation_id'=>$variation_id,'section_id'=>$section_id);
        $list = $this->mySplit->listVariations($optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));

        $variation = $list['return'][0];

        $this->assertEquals($variation['id'],$variation_id);
        $this->assertInternalType('string',$variation['label']);

        return $variation_id;
    }

    /**
     * @depends testList
     * @param int $variation_id
     */
    public function testUpdate($variation_id){
        // probably don't need to test every single permutation,
        // but we should test at least a couple
        $label = "NewType";
        $status = 10;
        $meta_ids = "1,4,9";
        $optParams = array('label'=>$label,'status'=>$status,'meta_ids'=>$meta_ids);
        $result = $this->mySplit->updateVariation($variation_id,$optParams);

        $this->assertEquals(1000, $result['status']);
        $this->assertEquals($result['return']['affectedRows'],1);
        return $variation_id;
    }

    /**
     * @depends testList
     * @param int $variation_id
     */
    public function testDelete($variation_id){
        $delResults = $this->mySplit->deleteVariation($variation_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

