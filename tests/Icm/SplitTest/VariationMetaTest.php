<?php

class Icm_SplitTest_VariationMetaTest extends PHPUnit_Framework_TestCase {
    /**
     * Test the static method for fetching meta ids
     */
    public function testGetMetaByString() {
        // setup db connection object
        $db = Icm_Db_Pdo::connect('cgAnalyticsDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgAnalyticsDb'));

        // call with an empty string
        $metaString = '';
        $metas = Icm_SplitTest_VariationMeta::getMetaByString($metaString, $db);

        // should return empty array
        $this->assertEmpty($metas, print_r($metas, true));

        // call with multiple ids
        $metaString = '90,91';
        $metas = Icm_SplitTest_VariationMeta::getMetaByString($metaString, $db);

        // should return multiple values
        $this->assertCount(2, $metas, print_r($metas, true));

        // call with single id
        $metaString = '90';
        $metas = Icm_SplitTest_VariationMeta::getMetaByString($metaString, $db);

        // should return single value
        $this->assertCount(1, $metas, print_r($metas, true));
    }
}