<?php

class Icm_SplitTest_EventCrud_Test extends PHPUnit_Framework_Testcase
{
    public function testCreate(){
        // although we're not testing application list here, we need it to grab an app id
        // for us since we must have the new event referencing an app id
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('name'=>'InstantCheckmate');
        $list = $mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];

        // app id is a string when its pulled from the array, add 0 to cast it back to int
        $app_id += 0;

        // now we have the app_id, use it to create a event
        $type = "purchase";
        $slug = "eventSlug";
        $desc = "somebody clicked the purchase button";
        $slug = "desc slug";
        $order = 2;
        $slug = "superslug";
        $optParams = array('order'=>$order);
        $results = $mySplit->createEvent($desc,$slug,$type,$app_id,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status']);
        $event_id = $results['return']['event_id'];

        // event is a string when its pulled from the array, add 0 to cast it back to int
        $event_id += 0;
        $this->assertNotEquals(0, $event_id);

        // store app and event id in array and return
        $ret_arr = array($app_id,$event_id);

        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $event_id = $ret_arr[1];
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('event_id'=>$event_id);
        $list = $mySplit->listEvents($app_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));
        $event = $list['return'][0];
        $this->assertEquals($event['id'],$event_id);
        $this->assertInternalType('string',$event['type']);

        return $event_id;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testUpdate($ret_arr){
        $app_id = $ret_arr[0];
        $event_id = $ret_arr[1];
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $order = 5;
        $type = "DifferentType";
        $desc = "A description is moot";
        $optParams = array('type'=>$type,'desc'=>$desc);
        $list = $mySplit->updateEvents($event_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);

        return $event_id;
    }

    /**
     * @depends testList
     * @param int $event_id
     */
    public function testDelete($event_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $delResults = $mySplit->deletEevent($event_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

