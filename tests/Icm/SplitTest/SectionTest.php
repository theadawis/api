<?php

  /*
   * To change this template, choose Tools | Templates
   * and open the template in the editor.
   */

  /**
   * Description of SectionTest
   * This is a Test Driven Development (tdd)
   * for Section object.
   * @author fadi
   */

class Icm_SplitTest_SectionTest extends PHPUnit_Framework_TestCase
{
    // instantiate the class and array so it will be only claaed once:
    protected function setUp() {
        // key to associative array must represent class attributes.
        $this->data = array('contextName' => 'Fadi','contextId' => '1');
        $this->section = new Icm_SplitTest_Section();
    }

    /*
     * Test to make sure that the associative array that was passed as a
     * parameter is an attribute of the class. This will prevent inserting
     * parameters that are not declared by the class
     */
    public function testFromArrayMethod() {
        $actual = $this->section->toArray($this->data);

        foreach($actual as $key => $value) {
                $this->assertClassHasAttribute($key,'Icm_SplitTest_Section');
        }
    }

    // unset the object:
    protected function tearDown() {
        unset($this->section);
    }
}
