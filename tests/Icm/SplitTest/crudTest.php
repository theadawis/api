<?php

class Icm_SplitTest_Crud_Test extends PHPUnit_Framework_Testcase{

    public function setUp() {
        parent::setUp();
        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn,$this->redis);
		$this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // although we're not testing application/segment/context list here, we need it to grab the associated id's
        // for us since we must have the new Test referencing one of each
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit'=>1);
        $list = $this->mySplit->listContexts($app_id,$optParams);

        $section_id = $list['return'][0]['id'];
        $section_id +=0;
        $list = $this->mySplit->listSegments($app_id,$optParams);

        $segment_id = $list['return'][0]['id'];
        $segment_id += 0;
        $optParams = array('section_id'=>$section_id);
        $segment_id += 0;
        $list = $this->mySplit->listVariations($optParams);
        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        // now we have the app_id, use it to create a Test
        $testType = "page";
        $desc = "This is a page test";
        $name = "Somename";
        $weight = 1;
        $status = 1;
        $name = "TestivusForTheRestivus";
        $optParams = array('desc'=>$desc,'weight'=>$weight,'status'=>$status);
        $results = $this->mySplit->createTest($app_id,$section_id,$testType,$name,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals($results['status'], 1000);
        $test_id = $results['return']['test_id'];

        // test/app id is a string when its pulled from the array, add 0 to cast it back to int
        $test_id += 0;
        $app_id += 0;
        $this->assertNotEquals(0, $test_id);

        // store app and context id in array and return
        $ret_arr = array($app_id,$test_id,$section_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $test_id = $ret_arr[1];
        $section_id = $ret_arr[2];

        $optParams = array('test_id'=>$test_id,'getSegments'=>false);
        $list = $this->mySplit->listTests($app_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals($list['status'], 1000, print_r($list, true));
        $this->assertEquals(count($list['return']), 1);
        $test = $list['return'][0];
        $this->assertEquals($test['id'],$test_id);
        $this->assertInternalType('string',$test['type']);

        // try to print out results with segment data as well
        $optParams = array('getSegments'=>true);
        $list = $this->mySplit->listTests($app_id,$optParams);

        // store app and context id in array and return
        $ret_arr = array($app_id,$test_id,$section_id);
        return $ret_arr;
    }

    /**
     * @depends testList
     * @param array $ret_arr
     */
    public function testUpdate($ret_arr){
        $app_id = $ret_arr[0];
        $test_id = $ret_arr[1];
        $section_id = $ret_arr[2];

        // probably don't need to test every single permutation,
        // but we should test at least a couple
        $type = "NewType";
        $weight = 10;

        // start time is now
        $start_time = time();

        // end time is 12 hours from now
        $end_time = time() + (60 * 60 * 12);
        $optParams = array('type'=>$type,'start_time'=>$start_time,'end_time'=>$end_time);
        $result = $this->mySplit->updateTest($test_id,$optParams);

        $this->assertEquals(1000, $result['status']);
        $this->assertEquals(1, $result['return']['affectedRows']);

        // store app and context id in array and return
        $ret_arr = array($app_id,$test_id,$section_id);
        return $ret_arr;
    }

    /**
     * @depends testList
     * @param array $ret_arr
     */
    public function testDelete($ret_arr){
        $app_id = $ret_arr[0];
        $test_id = $ret_arr[1];
        $section_id = $ret_arr[2];
        $delResults = $this->mySplit->deleteTest($test_id);

        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

