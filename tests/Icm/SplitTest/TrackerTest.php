<?php
/**
 * TrackerTest.php
 * User: chris
 * Date: 5/11/13
 * Time: 1:32 PM
 */
class Icm_SplitTest_TrackerTest extends PHPUnit_Framework_TestCase {

    public function testCreate() {
        $visitTracker = $this->getVisitTracker();
        $decider = $this->getDecider();
        $tracker = new Icm_SplitTest_Tracker($visitTracker, $decider);
        $this->assertInstanceOf('Icm_SplitTest_Tracker', $tracker);
    }

    public function testRequestContext() {
        $visitTracker = $this->getVisitTracker();
        $decider = $this->getDecider();

        $visitTracker->expects($this->exactly(2))
            ->method('getVisitData')
            ->will($this->returnValue(array("ip_address" => "127.0.0.1",
                                            "useragent" => "Mozilla")));

        $tracker = new Icm_SplitTest_Tracker($visitTracker, $decider);
        $requestContext = $tracker->getRequestContext();
        $this->assertInternalType('array', $requestContext);
        $this->assertArrayHasKey("ip_address", $requestContext);
        $this->assertArrayHasKey("useragent", $requestContext);
        $this->assertEquals("127.0.0.1", $requestContext["ip_address"]);
        $this->assertEquals("Mozilla", $requestContext["useragent"]);
    }

    public function testTrackingContext() {
        $visitTracker = $this->getVisitTracker();
        $decider = $this->getDecider();

        $_SERVER['HTTP_USER_AGENT'] = "Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)";

        $visitTracker
            ->expects($this->atLeastOnce())
            ->method('getVisitData')
            ->will($this->returnValue(array("ip_address" => "127.0.0.1",
                                            "referrer" => "http://localhost",
                                            "affiliate_id" => "HYCK",
                                            "sub_id" => "awcn",
                                            "params" => json_encode(array('src' => 'testingsrc')),
        )));

        $tracker = new Icm_SplitTest_Tracker($visitTracker, $decider);
        $trackingContext = $tracker->getTrackingContext();
        $this->assertArrayHasKey("affiliate_id", $trackingContext);
        $this->assertArrayHasKey("sub_id", $trackingContext);
        $this->assertArrayHasKey("src", $trackingContext);
        $this->assertArrayHasKey("mdm", $trackingContext);
        $this->assertArrayHasKey("brs", $trackingContext);

        $this->assertEquals("HYCK", $trackingContext["affiliate_id"]);
        $this->assertEquals("awcn", $trackingContext["sub_id"]);
        $this->assertEquals("testingsrc", $trackingContext["src"]);
        $this->assertEquals(null, $trackingContext["mdm"]);
        $this->assertEquals("MSIE/7", $trackingContext["brs"]);
    }

    public function testIsPaidTrafficWithOrganic() {
        $visitTracker = $this->getVisitTracker();
        $decider = $this->getDecider();

        $visitTracker->expects($this->atLeastOnce())
            ->method('getVisitData')
            ->will($this->returnValue(array(
                "src" => "organic"
        )));

        $tracker = new Icm_SplitTest_Tracker($visitTracker, $decider);
        $this->assertFalse($tracker->isPaidTraffic());
    }

    public function getDecider() {
        return $this->getMockBuilder("Icm_SplitTest_Decider")
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function getVisitTracker() {
        return $this->getMockBuilder("Icm_Visit_Tracker")
            ->disableOriginalConstructor()
            ->getMock();
    }

}
