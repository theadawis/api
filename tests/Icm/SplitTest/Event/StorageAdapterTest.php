<?php

/**
 * Description of StorageAdapterTest
 *
 * @author fadi
 */

class Icm_SplitTest_Event_StorageAdapter_Test extends PHPUnit_Framework_TestCase
{
    protected $tableName;
    protected $conn;
    protected $data;
    protected $eventStorageAdapter;
    protected $dataToObject;

    protected function setUp(){
        // put variables in data array()
        $this->tableName = 'cg_event_map';
        $this->data = array('app_id' => 1);

        $dbConfig = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgAnalyticsDb');
        $this->conn = Icm_Db_Pdo::connect('cgAnalyticsDb', $dbConfig);
        $this->eventStorageAdapter = new Icm_SplitTest_Event_StorageAdapter($this->tableName,'id', $this->conn);

        // create data objects from the given data array:
        $this->dataToObject =  Icm_SplitTest_Event::fromArray($this->data);

    }

    /*
     * make sure returnArrayFromObject is returning an array when
     * its passed an object parameter.
     */
    public function testReturnArrayFromObject(){
        $expcted = $this->data;
        $actual = $this->eventStorageAdapter->returnArrayFromObject($this->dataToObject);
        $this->assertEquals($expcted, $actual, 'Returned value must be an array');
    }

    /*
     * Make sure that our getMaxValue method is returning the maximum value
     * of our order field which is located in the split_section table..
     */
    public function testGetMaxValue(){
        $sql = "SELECT MAX(`order`) as `order` FROM `{$this->tableName}` LIMIT 0, 1";
        $results = $this->conn->fetchAll($sql);
        $expected = $results[0]['order'];
        $classFunction = $this->eventStorageAdapter->getMaxValue('order');
        $actual = $classFunction[0]['order'];
        $this->assertEquals($expected, $actual);
    }

    /*
     * Make sure that our fetchData method is returning expected
     * data with given parameters.
     */
    public function testFetchData(){
        $sql = "SELECT * FROM `{$this->tableName}`
            WHERE app_id = :app_id ORDER BY `order` ASC";
        $parameter = array('app_id'=>'1');
        $expected = $this->conn->fetchAll($sql, $parameter);
        $actual = $this->eventStorageAdapter->fetchData($this->dataToObject);
        $this->assertEquals($expected, $actual);
    }

    /*
     * Make sure our data is saved.
     */
    public function testSaveData(){
        $slug = 'test_'.time();
        $dataToInsert = array('app_id'=>'1', 'desc'=>'testSaveData', 'type'=>'testSaveData33', 'slug'=>$slug);

        // convert data to object:
        $newdataToObject = Icm_SplitTest_Event::fromArray($dataToInsert);

        /*
         * When the process is successful, saving returns true. Please note
         * that field slug is unique and must not be duplicated. An error will
         * be thrown if you run the test twice with the same slug name
         */
        $this->assertTrue($this->eventStorageAdapter->saveData($newdataToObject), 'Make sure slug is not duplicated');
    }

    // unset the object:
    protected function tearDown(){
        $this->conn = NULL;
        $this->data = array();
        $this->tableName = NULL;
        unset($this->eventStorageAdapter);
        unset($this->dataToObject);
    }
}
