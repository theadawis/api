<?php

class Icm_SplitTest_ApplicationCrud_Test extends PHPUnit_Framework_TestCase
{
    public function testCreate(){
        $testAppName = "TheMostAwesomestApplication";
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redconn = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redconn);
        $optParams = array('desc'=>'This is the description field', 'url'=>'www.thisismyawesomeappdomain.com');
        $ret = $mySplit->createApplication($testAppName, $optParams);
        $this->assertInternalType('array',$ret);
        $this->assertEquals($ret['status'], 1000);
        $app_id = $ret['return']['app_id'];
        $app_id += 0;
        $this->assertNotEquals($app_id, 0);

       return $app_id;
    }

    /**
     * @depends testCreate
     * @param int $app_id
     */
    public function testList($app_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redconn = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redconn);
        $optParams = array('app_id'=>$app_id);
        $list = $mySplit->listApplications($optParams);
        $this->assertInternalType('array',$list);
        $this->assertEquals($list['status'], 1000);
        $this->assertEquals(count($list['return']), 1);
        $app = $list['return'][0];
        $this->assertEquals($app['id'],$app_id);
        $this->assertInternalType('string',$app['name']);
        return $app_id;
    }

    /**
     * @depends testList
     * @param int $app_id
     */
    public function testDelete($app_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redconn = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redconn);
        $delResults = $mySplit->deleteApplication($app_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals($delResults['status'], 1000);
    }
}



