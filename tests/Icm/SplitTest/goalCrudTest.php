<?php

class Icm_SplitTest_GoalCrud_Test extends PHPUnit_Framework_Testcase
{
    public function testCreate(){
        // need app/context for test, need test to create a goal
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('name'=>'InstantCheckmate');
        $list = $mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit'=>1);
        $list = $mySplit->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;
        $list = $mySplit->listTests($app_id,$optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;
        $list = $mySplit->listEvents($app_id,$optParams);
        $event_id = $list['return'][0]['id'];
        $event_id +=0;

        $optParams = array('primary_goal'=>1);
        $results = $mySplit->createGoal($event_id,$test_id,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status']);

        $goal_id = $results['return']['goal_id'];

        // goal/app id is a string when its pulled from the array, add 0 to cast it back to int
        $goal_id += 0;

        $this->assertNotEquals(0, $goal_id);

        // store app and context id in array and return
        $ret_arr = array($test_id,$goal_id);

        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $test_id = $ret_arr[0];
        $goal_id = $ret_arr[1];

        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $optParams = array('goal_id'=>$goal_id);
        $list = $mySplit->listGoals($test_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));

        $goal = $list['return'][0];

        $this->assertEquals($goal['id'],$goal_id);

        return $goal_id;
    }

    /**
     * @depends testList
     * @param int $test_arr
     */
    public function testUpdate($goal_id){
        // probably don't need to test every single permutation,
        // but we should test at least a couple
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $primary_goal = 0;
        $optParams = array('primary_goal'=>$primary_goal);
        $result = $mySplit->updateGoal($goal_id,$optParams);

        $this->assertEquals(1000, $result['status']);
        $this->assertEquals(1, $result['return']['affectedRows']);

        return $goal_id;
    }

    /**
     * @depends testUpdate
     * @param int $goal_id
     */
    public function testDelete($goal_id){
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redis = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redis);
        $delResults = $mySplit->deleteGoal($goal_id);

        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

