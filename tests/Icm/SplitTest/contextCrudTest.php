<?php

class Icm_SplitTest_ContextCrud_Test extends PHPUnit_Framework_Testcase
{
    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplit = new Icm_SplitTest($this->conn,$this->redis);
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testCreate(){
        // although we're not testing application list here, we need it to grab an app id
        // for us since we must have the new context referencing an app id
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id += 0;

        // now we have the app_id, use it to create a context
        $contextName = "HomePage";
        $type = "contextType";
        $slug = "contextSlug";
        $optParams = array('order'=>1);
        $type = "Mytype";
        $slug = "Myslug";
        $optParams = array('default_variation_id'=>5);
        $results = $this->mySplit->createContext($contextName,$type,$slug,$app_id,$optParams);

        $this->assertInternalType('array',$results);
        $this->assertEquals($results['status'], 1000);
        $section_id = $results['return']['section_id'];

        // context is a string when its pulled from the array, add 0 to cast it back to int
        $section_id += 0;
        $app_id += 0;
        $this->assertNotEquals(0, $section_id);

        // store app and context id in array and return
        $ret_arr = array($app_id,$section_id);
        return $ret_arr;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testList($ret_arr){
        $app_id = $ret_arr[0];
        $section_id = $ret_arr[1];
        $optParams = array('section_id'=>$section_id);
        $list = $this->mySplit->listContexts($app_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status']);
        $this->assertEquals(1, count($list['return']));
        $context = $list['return'][0];
        $this->assertEquals($context['id'],$section_id);
        $this->assertInternalType('string',$context['name']);

        return $section_id;
    }

    /**
     * @depends testCreate
     * @param array $ret_arr
     */
    public function testUpdate($ret_arr){
        $app_id = $ret_arr[0];
        $section_id = $ret_arr[1];
        $optParams = array('name'=>"newName",'type'=>"newType");
        $list = $this->mySplit->updateContext($section_id,$optParams);

        $this->assertInternalType('array',$list);
        $this->assertEquals(1000, $list['status'], print_r($list, true));

        return $section_id;
    }

    /**
    * @depends testList
    * @param int $section_id
    */
    public function testDelete($section_id){
        $delResults = $this->mySplit->deleteContext($section_id);
        $this->assertInternalType('array',$delResults);
        $this->assertEquals(1000, $delResults['status']);
    }
}

