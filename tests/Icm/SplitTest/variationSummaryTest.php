<?php

class Icm_SplitTest_VariationSummary_Test extends PHPUnit_Framework_Testcase
{
    public function setUp() {
        parent::setUp();

        $this->conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini', 'cg_analytics'));
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));
        $this->mySplitSummary = new Icm_SplitTest_Summary($this->conn, $this->redis);
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testVariationSummaryAdd(){
        $optParams = array('name'=>'InstantCheckmate');
        $list = $this->mySplitSummary->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit'=>1);
        $list = $this->mySplitSummary->listContexts($app_id,$optParams);
        $section_id = $list['return'][0]['id'];
        $section_id +=0;

        $list = $this->mySplitSummary->listSegments($app_id,$optParams);
        $segment_id = $list['return'][0]['id'];
        $segment_id += 0;

        $optParms = array('section_id'=>$section_id);
        $list = $this->mySplitSummary->listVariations($optParams);
        $variation_id = $list['return'][0]['id'];
        $variation_id +=0;

        $optParams['status'] = Icm_SplitTest::STATUS_LIVE;
        $list = $this->mySplitSummary->listTests($app_id,$optParams);
        $test_id = $list['return'][0]['id'];
        $test_id +=0;

        $list = $this->mySplitSummary->listEvents($app_id,$optParams);
        $event_id = $list['return'][0]['id'];
        $event_id +=0;

        $optParams = array('primary_goal'=>1);
        $results = $this->mySplitSummary->createGoal($event_id,$test_id,$optParams);
        $goal_id = $results['return']['goal_id'];
        $goal_id += 0;

        $views = 100;
        $conversions = 50;
        $timestamp = time();

        $results = $this->mySplitSummary->insertVariationSummary($section_id, $test_id, $variation_id, $event_id, $views, $conversions, $timestamp);
        $this->assertInternalType('array',$results);
        $this->assertEquals(1000, $results['status'], print_r($results, true));
        $variation_summary_id = $results['return']['variation_summary_id'];

        $create = array($variation_summary_id,
                        $app_id,
                        $section_id,
                        $test_id,
                        $variation_id,
                        $event_id);
        return $create;
    }

    /**
     * @depends testVariationSummaryAdd
     */
    public function testListVariationSummary($create){
        $variation_summary_id = $create[0];
        $app_id = $create[1];
        $section_id = $create[2];
        $test_id = $create[3];
        $variation_id = $create[4];
        $event_id = $create[5];

        $optParams = array('variation_id'=>$variation_id);
        $results = $this->mySplitSummary->getVariationSummary($app_id, $section_id, $test_id, $optParams);

        $this->assertInternalType('array', $results);
        $this->assertEquals(1000, $results['status']);
    }
}

