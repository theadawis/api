<?php

class Icm_SplitTest_bulkTestAssoc_Test extends PHPUnit_Framework_Testcase
{
    public function testCreate(){
        //Need app/context for test
        $conn = Icm_Db_Pdo::connect('cg_analytics',Icm_Config::fromIni(LIBRARY_PATH .'/tests/config/mysql.ini','cg_analytics'));
        $redconn = new Icm_Redis(Icm_Config::fromArray(array()));
        $mySplit = new Icm_SplitTest($conn,$redconn);
        $optParams = array('name' => 'InstantCheckmate');
        $list = $mySplit->listApplications($optParams);

        $app_id = $list['return'][0]['id'];
        $app_id +=0;
        $optParams = array('limit' => 1);
        $list = $mySplit->listContexts($app_id,$optParams);

        $section_id = $list['return'][0]['id'];
        $section_id +=0;

        $optParams = array('limit' => 3);
        $list = $mySplit->listMetas($app_id,$optParams);
        $meta_ids = $list['return'];
        $metastring = "";

        foreach ($meta_ids as $meta){
            if ($metastring == "") {
                $metastring .= $meta['id'];
            }
            else {
                $metastring .= "," .$meta['id'];
            }
        }

        $label = "StandardVariation";
        $status = 1;
        $type = "mytype";
        $optParams = array('status' => $status);
        $results = $mySplit->createVariation($label,$metastring,$section_id,$type,$optParams);

        $variation_id1 = $results['return']['variation_id'];
        $variation_id1 += 0;
        $label = "NonstandardVariation";
        $type = "yourtype";
        $results = $mySplit->createVariation($label,$metastring,$section_id,$type,$optParams);
        $variation_id2 = $results['return']['variation_id'];
        $variation_id2 += 0;

        $type = "purchase";
        $desc = "somebody clicked the purchase button";
        $slug = "desc slug";
        $order = 2;
        $optParams = array('order' => $order);
        $results = $mySplit->createEvent($desc,$slug,$type,$app_id,$optParams);
        $event_id = $results['return']['event_id'];
        $event_id += 0;

        $segArr1 = array('type' => "src",'value' => "fb");
        $segArr2 = array('type' => "src", 'value' => "msn");
        $varArr1 = array('id' => $variation_id1,'weight' => 6);
        $varArr2 = array('id' => $variation_id2,'weight' => 5);

    }
}
