<?php

/**
 * Description of StorageAdapterTest
 *
 * @author fadi
 */
class Icm_tests_Icm_SplitTest_Variation_StorageAdapterTest extends PHPUnit_Framework_TestCase
{
    protected $tableName, $conn, $data, $variationStorageAdapter, $dataToObject;

    protected function setUp(){
        // put variables in data array()
        $this->tableName = 'split_variation';
        $this->data = array('id' => 1);
        $dbConfig = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgAnalyticsDb');
        $this->conn = Icm_Db_Pdo::connect('cgAnalyticsDb', $dbConfig);
        $this->variationStorageAdapter = new Icm_SplitTest_Variation_StorageAdapter($this->tableName, 'id', $this->conn);
        $this->redis = new Icm_Redis(Icm_Config::fromIni('/sites/api/tests/config/config.ini', 'redis'));

        // create data objects from the given data array:
        $this->dataToObject =  Icm_SplitTest_Variation::fromArray($this->data);
    }

    /*
     * Make sure returned status is correct
     */
    public function testfetchStatusCountActive(){
        // try for active status:
        $dataToInsert = array('status' => '1', 'section_id' => '1');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Variation::fromArray($dataToInsert);
        $sql = "SELECT count(`id`) as `idCount`FROM {$this->tableName}
                WHERE `section_id` = :section_id AND `status` = :status LIMIT 0, 1";
        $parameter = array('section_id' => '1', 'status' => '1');
        $result = $this->conn->fetchAll($sql, $parameter);
        $expected = $result[0]['idCount'];
        $this->assertEquals($expected, $this->variationStorageAdapter->fetchStatusCount($newdataToObject));
    }

    /*
     * Make sure returned status is correct
     */
    public function testfetchStatusCountArchived(){
        // try for archived status:
        $dataToInsert = array('status' => '2', 'section_id' => '1');

        // convert data to object:
        $newdataToObject =  Icm_SplitTest_Variation::fromArray($dataToInsert);
        $sql = "SELECT count(`id`) as `idCount`FROM {$this->tableName}
                WHERE `section_id` = :section_id AND `status` = :status LIMIT 0, 1";
        $parameter = array('section_id' => '1', 'status' => '2');
        $result = $this->conn->fetchAll($sql, $parameter);
        $expected = $result[0]['idCount'];
        $this->assertEquals($expected, $this->variationStorageAdapter->fetchStatusCount($newdataToObject));
    }

    /*
     * Make sure our data is saved.
     */
    public function testsaveData(){
        $meta_ids = '1,2,3';
        $dataToInsert = array('app_id' => '1', 'label' => 'Label Test',
                              'meta_ids' => $meta_ids, 'section_id' => '1',
                              'type' => 'page', 'desc' => 'description','status' => '1');

        // convert data to object:
        $newdataToObject = Icm_SplitTest_Variation::fromArray($dataToInsert);

        /*
         * When the process is successful, the save op returns true. Please note
         * that field slug is unique and must not be duplicated. An error will
         * be thrown if you run the test twice with the same slug name
         */
        $this->assertTrue($this->variationStorageAdapter->saveData($newdataToObject));
    }

    /*
     * Make sure our meta data is saved.
     */
    public function testsaveDataMeta(){
        $dataToInsert = array('key' => 'my page', 'value' => 'page value',
                              'app_id' => '1');

        // convert data to object:
        $newdataToObject = Icm_SplitTest_Variation::fromArray($dataToInsert);

        /*
         * When the process is successful, the method should return a value greater than zero
         * (variation id)
         */
        $this->assertNotEquals(1, $this->variationStorageAdapter->saveDataMeta($newdataToObject, 'split_meta'));
    }

    /**
     * Make sure data is delete:
     */
    public function testdeleteData(){
        // select an id:
        $dataToInsert = array('id' => '2392');

        // convert data to object:
        $newdataToObject = Icm_SplitTest_Variation::fromArray($dataToInsert);

        $this->assertTrue($this->variationStorageAdapter->deleteData($newdataToObject), 'Delete failed. Make sure the id you inserted exists');
    }

    public function testdecodeMetas(){
        $meta_ids = "1,2,3" ;
        $metaDataArray = array('id' => $meta_ids);
        $newdataToObject = Icm_SplitTest_Variation::fromArray($metaDataArray);
        $metaIds = implode(',', $metaDataArray);
        $metaIds = explode(',', $meta_ids);
        $associativeData = array();

        foreach ($metaIds as $value){
            $associativeData = array('id' => $value);
            $sql = "SELECT * FROM `split_meta` WHERE `id` = :id";
            $result = $this->conn->fetchAll($sql, $associativeData);
            $metaDataArrayFinal[] = $result;
        }

        $this->assertEquals($metaDataArrayFinal, $this->variationStorageAdapter->decodeMetas($newdataToObject, 'split_meta'));
    }

    /*
     * Tests if submitted data is being updated (status change for example).
     */
    public function testupdateData(){
        $meta_ids = '10297,10298,10299,10300,10301,10302';
        $lastUpdated = time();
        $data = array('id' => '20', 'app_id' => '1', 'label' => 'Test for update',
                      'meta_ids' => $meta_ids, 'type' => 'page', 'desc' => 'test', 'last_updated' => $lastUpdated);
        $newdataToObject = Icm_SplitTest_Variation::fromArray($data);
        $this->assertTrue($this->variationStorageAdapter->updateData($newdataToObject, $this->redis));
    }

    // unset the object:
    protected function tearDown(){
        $this->conn = NULL;
        $this->data = array();
        $this->tableName = NULL;
        unset($this->variationStorageAdapter);
        unset($this->dataToObject);
    }
}
