<?php

class Icm_Db_Mssql_Test extends PHPUnit_Framework_TestCase
{
    public function testConnect(){
        $this->markTestSkipped('MSSQL not available in dev environment.');
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql');
        $conn = Icm_Db_Mssql::connect('npd', $config);

        $results = $conn->fetch('SELECT 1');
        $this->assertTrue(count($results) > 0);

        $conn2 = Icm_Db_Mssql::getInstance('npd');
        $this->assertSame($conn, $conn2);
    }

    public function testConnect2(){
        $this->markTestSkipped('MSSQL not available in dev environment');
        $this->markTestIncomplete('Test not quite done');
        $config = Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'mssql2');
        $conn = Icm_Db_Mssql::connect('npd', $config);

        $results = $conn->fetch('SELECT 1');
        $this->assertTrue(count($results) > 0);

        $conn2 = Icm_Db_Mssql::getInstance('npd');
        $this->assertSame($conn, $conn2);

        $r = $conn->fetch("SELECT * FROM aprilsormain where zip = '95060'");
    }
}
