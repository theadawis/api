<?php

class Icm_Db_Pdo_Test extends PHPUnit_Framework_TestCase
{
    public function testCreate(){
        $conn = Icm_Db_Pdo::connect('locationDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/mysql.ini', 'locationDb'));
        $conn->execute("DELETE FROM data_census WHERE ZipCode = '92101' OR ZipCode = '92121'");

        // create a couple of data census entries
        $conn->execute("INSERT INTO data_census (ZipCode, City, State) VALUES ('92101', 'San Diego', 'CA'), ('92121', 'San Diego', 'CA')");

        // pull them back out
        $results = $conn->fetch('
            SELECT *
            FROM
                data_census
            WHERE
                ZipCode = :zipcode
        ', array(':zipcode' => '92101'));
        $this->assertInternalType('array', $results);
        $this->assertTrue(count($results) > 0);
        $conn2 = Icm_Db_Pdo::getInstance('locationDb');
        $this->assertSame($conn, $conn2);
        $conn->execute("DELETE FROM data_census WHERE ZipCode = '92101' OR ZipCode = '92121'");
    }

    /**
     * @expectedException PDOException
     */
    public function testBadQuery() {
        $conn = Icm_Db_Pdo::connect('locationDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/mysql.ini', 'locationDb'));

        $conn->fetch('
            SELECT *
            FROM
                foobar
            WHERE
                ZipCode = :zipcode
        ');
    }

    /**
     * @expectedException Icm_Db_Exception
     */
    public function testNonExistantConnection() {
        $conn = Icm_Db_Pdo::getInstance('foobar');
    }

    /**
     * @expectedException Icm_Db_Exception
     */
    public function testWrongDsn() {
        $conn = Icm_Db_Pdo::connect('wrongDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini'));
        $conn->fetch("SELECT NOW() FROM dual", array());
    }
}
