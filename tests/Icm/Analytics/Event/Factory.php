<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/3/12
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Analytics_Event_Factory_Test extends PHPUnit_Framework_TestCase {



    public function testCreate() {
        $tracker = $this->getMock("Icm_Analytics_Tracker", array(), array(), '', false);

        $factory = new Icm_Analytics_Event_Factory($tracker);
        $this->assertInstanceOf("Icm_Analytics_Event_Factory", $factory);
    }

    public function testCreateEvent() {
        $tracker = $this->getMock("Icm_Analytics_Tracker", array(), array(), '', false);

        $tracker->expects($this->once())
            ->method('getVisitorId')
            ->will($this->returnValue(3));
        $tracker->expects($this->once())
            ->method('getRequestContext')
            ->will($this->returnValue(
                    $this->getMock('Icm_Analytics_Context_Request')
                )
            );
        $tracker->expects($this->once())
            ->method('getTestContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Test')
            )
        );

        $tracker->expects($this->once())
            ->method('getTrackingContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Tracking', array("toArray"))
            )
        );

        $tracker->expects($this->once())
            ->method('getCurrentTest')
            ->will($this->returnValue(
                $this->getMock('Icm_SplitTest_Variation', array(), array(1,2,3))
            )
        );

        $factory = new Icm_Analytics_Event_Factory($tracker);
        $event = $factory->createEvent(1);

        $this->assertInstanceOf("Icm_Analytics_Event", $event);
        $this->assertTrue($event->getVisitorId(3) == 3);
    }

    public function testSetEventClass() {
        $tracker = $this->getMock("Icm_Analytics_Tracker", array(), array(), '', false);

        $tracker->expects($this->once())
            ->method('getVisitorId')
            ->will($this->returnValue(3));
        $tracker->expects($this->once())
            ->method('getRequestContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Request')
            )
        );
        $tracker->expects($this->once())
            ->method('getTestContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Test')
            )
        );

        $tracker->expects($this->once())
            ->method('getTrackingContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Tracking', array("toArray"))
            )
        );

        $tracker->expects($this->once())
            ->method('getCurrentTest')
            ->will($this->returnValue(
                $this->getMock('Icm_SplitTest_Variation', array(), array(1,2,3))
            )
        );

        $this->getMock('Icm_Analytics_Event',array(),array(), 'Checkmate_Analytics_Event', false, false, false);

        $factory = new Icm_Analytics_Event_Factory($tracker, "Checkmate_Analytics_Event");
        $factory->setEventClass("Checkmate_Analytics_Event");
        $event = $factory->createEvent(1);
        $this->assertInstanceOf("Checkmate_Analytics_Event", $event);
    }

    function testCreateEventWithNoActiveTest() {

        $tracker = $this->getMock("Icm_Analytics_Tracker", array(), array(), '', false);

        $tracker->expects($this->once())
            ->method('getVisitorId')
            ->will($this->returnValue(3));
        $tracker->expects($this->once())
            ->method('getRequestContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Request')
            )
        );
        $tracker->expects($this->once())
            ->method('getTestContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Test')
            )
        );

        $tracker->expects($this->once())
            ->method('getTrackingContext')
            ->will($this->returnValue(
                $this->getMock('Icm_Analytics_Context_Tracking', array("toArray"))
            )
        );

        $tracker->expects($this->once())
            ->method('getCurrentTest')
            ->will($this->returnValue(null)
        );

        //$this->getMock('Icm_Analytics_Event',array(),array(), 'Checkmate_Analytics_Event', false, false, false);

        $factory = new Icm_Analytics_Event_Factory($tracker, "Checkmate_Analytics_Event");
        $factory->setEventClass("Checkmate_Analytics_Event");
        $event = $factory->createEvent(1);
        $this->assertInstanceOf("Checkmate_Analytics_Event", $event);

    }

}
