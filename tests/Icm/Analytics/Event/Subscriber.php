<?php
class Icm_Event_Subscriber_Test extends PHPUnit_Framework_TestCase
{

    /**
     * @var Icm_Event_Dispatcher
     */
    protected $dispatcher;

    /**
     * @var Icm_Analytics_Event_Subscriber
     */
    protected $subscriber;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $logger;

    /**
     * @var array
     */
    protected $eventList;

    public function setUp() {

        $this->dispatcher = new Icm_Event_Dispatcher();

        $this->logger = $this->getMock('Icm_Util_Logger_Syslog', array(), array(), '', false);

        $this->eventList = array(
            1 => Icm_Analytics_Event::EVT_ACTION,
            2 => Icm_Analytics_Event::EVT_CLICK,
            3 => Icm_Analytics_Event::EVT_PAGELOAD
        );

    }

    /**
     *
     */
    public function testIgnoresNonAnalyticsEvent() {

        $this->logger->expects($this->never())
            ->method('logEvent');

        $this->subscriber = new Icm_Analytics_Event_Subscriber_Syslog(
            $this->eventList,
            $this->logger
        );

        $this->dispatcher->addSubscriber($this->subscriber);


        $this->dispatcher->trigger(Icm_Analytics_Event::EVT_PAGELOAD, new Icm_Event());
    }

    public function testListensToAnalyticsEvent() {

        $this->logger->expects($this->once())
            ->method('logEvent');

        $this->subscriber = new Icm_Analytics_Event_Subscriber_Syslog(
            $this->eventList,
            $this->logger
        );

        $this->dispatcher->addSubscriber($this->subscriber);


        $rc = $this->getMock('Icm_Analytics_Context_Request', array(), array(), '', false);
        $rc->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
            'ip_address' => '123.456.789'
        )));

        $testctx = $this->getMock('Icm_Analytics_Context_Test', array(), array(), '', false);
        $testctx->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
            1 => array(
                'testId' => 1,
                'variationId' => 2,
                'sectionId' => 3
            )
        )));

        $trackingctx = $this->getMock('Icm_Analytics_Context_Tracking', array(), array(), '', false);

        $trackingctx->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
            'src' => 'foo'
        )));

        $evt = Icm_Analytics_Event::create(
            1, $rc, $testctx, $trackingctx
        );

        $evt->setEventId(1);

        $evt->setCurrentTest(new Icm_SplitTest_Variation(1,2,3));
        $this->dispatcher->trigger(Icm_Analytics_Event::EVT_PAGELOAD, $evt);
    }

    public function testIntegratesWithSyslog() {

        $subscriber = new Icm_Analytics_Event_Subscriber(array(
            Icm_Analytics_Event::EVT_PAGELOAD => 1
        ), Icm_Util_Logger_Syslog::getInstance('api'));

        $this->dispatcher->addSubscriber($subscriber);

        $rc = $this->getMock('Icm_Analytics_Context_Request', array(), array(), '', false);
        $rc->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
                'ip_address' => '123.456.789'
        )));

        $testctx = $this->getMock('Icm_Analytics_Context_Test', array(), array(), '', false);
        $testctx->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
            1 => array(
                'testId' => 1,
                'variationId' => 2,
                'sectionId' => 3
            )
        )));

        $trackingctx = $this->getMock('Icm_Analytics_Context_Tracking', array(), array(), '', false);

        $trackingctx->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue(array(
                'src' => 'foo'
        )));

        $evt = Icm_Analytics_Event::create(
            3, $rc,
            $testctx,
            $trackingctx
        );

        $currentTest = $this->getMock('Icm_SplitTest_Variation', array(), array(), '', false);

        $currentTest->expects($this->once())
            ->method('getSectionId')
            ->will($this->returnValue(1));
        $currentTest->expects($this->once())
            ->method('getTestId')
            ->will($this->returnValue(2));
        $currentTest->expects($this->once())
            ->method('getVariationId')
            ->will($this->returnValue(3));

        $evt->setCurrentTest($currentTest);

        $this->dispatcher->trigger(Icm_Analytics_Event::EVT_PAGELOAD, $evt);
    }

}
