<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/29/12
 * Time: 1:18 PM
 * To change this template use File | Settings | File Templates.
 */
#class Icm_Analytics_Tracker_Test extends PHPUnit_Framework_TestCase implements Icm_Analytics_Tracker_Storage_Interface, Icm_SplitTest_Storage_Interface
class Icm_Analytics_Tracker_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Analytics_Tracker $tracker
     */
    protected $tracker;

    protected $hasStored = true;

    protected $session;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $trackingContext;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestContext;

    public function setUp() {
        if($this->getName() == "testIsActive") {
            $this->markTestSkipped("This is not an actual test.  It's part of an interface.  if that bothers you...well, we're not going to change it, so deal.");
        }

        $this->trackingContext = $this->getMock('Icm_Analytics_Context_Tracking', array(), array(), '', false);
        $this->requestContext = $this->getMock('Icm_Analytics_Context_Request', array(), array(), '', false);
/*        $trackingContext = new Icm_Analytics_Context_Tracking();
        $trackingContext->setCmp('testCmp');
        $trackingContext->setCnt('testCnt');
        $trackingContext->setMdm('testMdm');
        $trackingContext->setSrc('testSrc');*/

/*        $requestContext = new Icm_Analytics_Context_Request();
        $requestContext->setIpAddress('127.0.0.1');
        $requestContext->setReferrer('http://www.instantcheckmate.com');
        $requestContext->setUrl('http://www.instantcheckmate.com/search');*/

        $this->session = new Icm_Session_Namespace('tracker');
        //Icm_Session::setStorageAdapter(new Icm_Session_Storage_Dummy());
        //Icm_Session::start();
    }

    public function testHasVisitorId() {

        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );

        $this->tracker->setVisitorId(3);

        $this->assertTrue($this->tracker->hasVisitorId());

        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );

        $this->tracker->assignVisitorId();

        $this->assertTrue($this->tracker->hasVisitorId());
    }

    /**
     * @depends testHasVisitorId
     */
    public function testAssignVisitorId() {

        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );

        $this->assertTrue($this->tracker->assignVisitorId());
    }

    /**
     * @depends testAssignVisitorId
     */
    public function testGetVisitorId() {
        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );
        $this->assertInternalType('int', $this->tracker->getVisitorId());
        $this->assertTrue(123 == $this->tracker->getVisitorId());
    }

    public function isSuperSegment($type, $segment) {
        return true;
    }

    public function testIsPaidTraffic(){

        $this->trackingContext->expects(
            $this->once()
        )->method('getSrc')->will($this->returnValue(
            'foo'
        ));

        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );
        $this->assertTrue($this->tracker->isPaidTraffic());
    }

    public function testTrackingStore() {

        $this->tracker = new Icm_Analytics_Tracker(
            $this->requestContext,
            $this->trackingContext,
            $this,
            $this
        );

        $this->tracker->restore($this);

        $this->tracker->store();
        $this->assertTrue($this->hasStored);
        $this->assertTrue($this->tracker->requiresResegment());

    }

    public function store($visitorId, Icm_Analytics_Context_Request $request, Icm_Analytics_Context_Tracking $tracking)
    {
        return $this->hasStored = true;
    }


    /**
     * @param $visitorId
     * @return Icm_Analytics_Context_Tracking
     */
    public function retrieve($visitorId)
    {
        $context = new Icm_Analytics_Context_Tracking();
        $context->setCmp('testCmp2');
        $context->setSrc('testSrc2');
        $context->setCnt('testCnt2');
        $context->setMdm('testMdm2');
        return $context;
    }

    /**
     * @return int
     */
    public function createVisitor()
    {
        return 123;
    }

    public function getDefault($sectionId)
    {
        // TODO: Implement getDefault() method.
    }

    public function testIsActive(Icm_SplitTest_Variation $t)
    {
        // TODO: Implement testIsActive() method.
    }

    public function getActiveTest($sectionId, Icm_Analytics_Context_Tracking $trackingContext)
    {
        // TODO: Implement getActiveTest() method.
    }

    public function getControl($sectionId)
    {
        // TODO: Implement getControl() method.
    }

    public function getTestMeta($variationId)
    {
        // TODO: Implement getTestMeta() method.
    }

    public function getTestByVariation($variationId)
    {
        // TODO: Implement getTestByVariation() method.
    }

    /**
     * @param $type
     * @param $value
     * @return int
     */
    public function getSegmentId($type, $value)
    {
        // TODO: Implement getSegmentId() method.
    }


}
