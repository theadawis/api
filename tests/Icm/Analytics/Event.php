<?php
class Icm_Analytics_Event_Test extends PHPUnit_Framework_TestCase
{

    /**
     * @var Icm_Analytics_Context_Request
     */
    public $requestContext;

    /**
     * @var Icm_Analytics_Context_Tracking
     */
    public $trackingContext;

    /**
     * @var Icm_Analytics_Context_Test $testContext
     */
    public $testContext;

    /**
     * @var Icm_Event_Dispatcher $dispatcher
     */
    public $dispatcher;

    /**
     * @var Icm_Analytics_Event_Subscriber
     */
    public $subscriber;

    /**
     * @var Icm_Util_Logger_Dummy
     */
    public $logger;

    public function setUp() {
        $this->requestContext = new Icm_Analytics_Context_Request();
        $this->requestContext->setReferrer('http://instantcheckmate.com');
        $this->requestContext->setUrl('http://instantcheckmate.com/search');
        $this->requestContext->setIpAddress('127.0.0.1');

        $this->trackingContext = new Icm_Analytics_Context_Tracking();
        $this->trackingContext->setSrc('GOOG');
        $this->trackingContext->setMdm('LE');
        $this->trackingContext->setCmp('ADWORDS');
        $this->trackingContext->setCnt('HAI');

        $this->testContext = new Icm_Analytics_Context_Test();
        $this->testContext->addTest(new Icm_SplitTest_Variation(4, 5, 20));
        $this->testContext->addTest(new Icm_SplitTest_Variation(5, 6, 21));

        $this->logger = new Icm_Util_Logger_Dummy();

        $this->subscriber = new Icm_Analytics_Event_Subscriber(array(
               Icm_Analytics_Event::EVT_PAGELOAD => 1
            ), $this->logger);


        $this->dispatcher = new Icm_Event_Dispatcher();
        $this->dispatcher->addSubscriber($this->subscriber);
    }

    public function testCreate() {
        $evt = Icm_Analytics_Event::create(1, $this->requestContext, $this->testContext, $this->trackingContext);
        $evt->setCurrentTest(new Icm_SplitTest_Variation(1,2,3));
        $this->assertInstanceOf('Icm_Analytics_Context_Request', $evt->getRequestContext());
        $this->assertInstanceOf('Icm_Analytics_Context_Test', $evt->getTestContext());
        $this->assertInstanceOf('Icm_Analytics_Context_Tracking', $evt->getTrackingContext());
        return $evt;
    }

    /**
     * @depends testCreate
     * @param Icm_Analytics_Event $evt
     */
    public function testSerialize($evt) {
        $array = $evt->toArray();
        $this->assertArrayHasKey('trackingContext', $array);
        $this->assertArrayHasKey('testContext', $array);
        $this->assertArrayHasKey('testContext', $array);
        $this->assertArrayHasKey('src',$array['trackingContext']);
        $this->assertArrayHasKey('cmp',$array['trackingContext']);
        $this->assertArrayHasKey('mdm',$array['trackingContext']);
        $this->assertArrayHasKey('cnt',$array['trackingContext']);
        $this->assertArrayHasKey('ipAddress',$array['requestContext']);
        $this->assertArrayHasKey('referrer',$array['requestContext']);
        $this->assertArrayHasKey('url',$array['requestContext']);
    }

    /**
     * @depends testCreate
     * @param Icm_Analytics_Event $evt
     */
    public function testTrigger($evt) {
        $this->dispatcher->trigger(Icm_Analytics_Event::EVT_PAGELOAD, $evt);
        $this->assertEquals(1, $this->logger->getEventCount());
    }

}
