<?php
/**
 * @runInSeparateProcess
 */
class Icm_Session_Test extends PHPUnit_Framework_TestCase
{
    public function setUp() {
        $_SESSION = array();
    }

    public function testStart() {
        session_unset();
        session_destroy();
        Icm_Session::writeClose();

        Icm_Session::setStorageAdapter(new Icm_Session_Storage_Dummy());
        Icm_Session::setOptions(array(
            'use_only_cookies' => false,
            'use_trans_sid' => true,
            'use_cookies' => false,
            'cache_limiter' => '',
        ));

        Icm_Session::start();
        $this->assertNotNull(session_id());
        $this->assertTrue(Icm_Session::isStarted());
    }

    /**
     * @depends testStart
     */
    public function testNamespaces() {
        $ns = new Icm_Session_Namespace('foobar');
        $ns->foo = 'bar';

        $this->assertTrue($ns->foo == 'bar');
        $this->assertArrayHasKey('_ICM', $_SESSION);
        $this->assertArrayHasKey('foobar', $_SESSION['_ICM']);
        $this->assertArrayHasKey('foo', $_SESSION['_ICM']['foobar']);
        unset($ns->foo);
        $this->assertNull($ns->foo);
    }

    public function tearDown() {
        //        Icm_Session::writeClose();
    }
}
