<?php

class Icm_Search_Query_Test extends PHPUnit_Framework_TestCase {

    protected $query;

    protected function setUp() {
        $this->query = new Icm_Search_Query(array(), '');
    }

    public function testAllowPaid() {
        $this->query->setAllowPaid(true);
        $this->assertTrue($this->query->allowPaid());
    }

    public function testCriteria() {
        $this->query->setCriteria(array('first_name' => 'John'));
        $this->assertArrayHasKey('first_name', $this->query->getCriteria());
        $this->assertEquals('John', $this->query->getCriterion('first_name'));
    }

    public function testSearchType() {
        $this->query->setSearchType('lead');
        $this->assertEquals('lead', $this->query->getSearchType());
    }

    public function testFilters() {
        $this->query->addFilter('type', 'value');
        $this->assertEquals('type', $this->query->getFilters()[0][0]);
        $this->query->clearFilters();
        $this->assertCount(0, $this->query->getFilters());
    }
}
