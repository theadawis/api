<?php

class Icm_Search_EngineTest extends PHPUnit_Framework_TestCase {

    const TEST_PHONENUMBER = '8582925525';
    const TEST_FIRSTNAME = 'John';
    const TEST_LASTNAME = 'Smith';

    protected $api;
    protected $phoneNumber;
    protected $Adapter;
    protected $dataContainer;

    protected function setUp() {
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
        $this->api->dataContainer = new Icm_Container_Factory();
        $this->api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));
    }

    public function testPhoneSearchNpd() {
        $criteria = array();
        $criteria[Icm_Search_Query::CRITERIA_PHONENUMBER] = self::TEST_PHONENUMBER;
        $query = new Icm_Search_Query($criteria, 'reversePhone');
        $engine = $this->api->dataContainer->get('reversePhoneSearchNP');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $phone) {
            $this->assertInstanceOf('Icm_Entity_Phone', $phone);
        }
    }

    public function testPhoneSearchWhitepages() {
        $criteria = array();
        $criteria[Icm_Search_Query::CRITERIA_PHONENUMBER] = self::TEST_PHONENUMBER;
        $query = new Icm_Search_Query($criteria, 'reversePhone');
        $query->setAllowPaid(true);
        $engine = $this->api->dataContainer->get('reversePhoneSearchWP');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $phone) {
            $this->assertInstanceOf('Icm_Entity_Phone', $phone);
        }
    }

    public function testPhoneSearchLexis() {
        $criteria = array();
        $criteria[Icm_Search_Query::CRITERIA_PHONENUMBER] = self::TEST_PHONENUMBER;
        $query = new Icm_Search_Query($criteria, 'reversePhone');
        $engine = $this->api->dataContainer->get('reversePhoneSearchLX');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
    }

    public function testBackgroundCheckNpd() {
        $criteria = array();
        $criteria[Icm_Search_Query::CRITERIA_ID] = '15137642651';
        $criteria[Icm_Search_Query::CRITERIA_FIRSTNAME] = self::TEST_FIRSTNAME;
        $criteria[Icm_Search_Query::CRITERIA_LASTNAME] = self::TEST_LASTNAME;
        $query = new Icm_Search_Query($criteria, 'backgroundCheck');
        $engine = $this->api->dataContainer->get('reversePhoneSearchNP');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
    }

    public function testBackgroundCheckLexis() {
        $criteria = array();
        $criteria[Icm_Search_Query::CRITERIA_ID] = '15137642651';
        $criteria[Icm_Search_Query::CRITERIA_FIRSTNAME] = self::TEST_FIRSTNAME;
        $criteria[Icm_Search_Query::CRITERIA_LASTNAME] = self::TEST_LASTNAME;
        $query = new Icm_Search_Query($criteria, 'backgroundCheck');
        $engine = $this->api->dataContainer->get('lexisOnlyPersonSearch');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $person) {
            $this->assertInstanceOf('Icm_Entity_Person', $person);
        }
    }
}
