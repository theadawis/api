<?php

/**
 * Test DB Search Adapter
 */
class Icm_Search_Adapter_Census_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Internal_DbSearch
     */
    protected $cgInternalDb;

    /**
     * @var Icm_Config
     */
    protected $config;

    protected function setUp(){
        $conn = Icm_Db_Pdo::connect('phoneDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'phoneDb'));
        $cgInternalDb = new Icm_Service_Census_Db($conn);
        $this->adapter = new Icm_Search_Adapter_Census($cgInternalDb);
    }

    public function testSearchCriteriaEmptyNoException(){
        $result = $this->adapter->getByZip(92126);
    }

    public function testGetByZip() {
        // call with known zip
        $result = $this->adapter->getByZip('72632');

        // should return census data
        $this->assertNotNull($result);
        $this->assertGreaterThan(0, count($result));
        $this->assertArrayHasKey('Population', $result[0]);

        // call with array zip
        $result = $this->adapter->getByZip(array('zip' => '79606'));

        // should still return census data
        $this->assertNotNull($result);
        $this->assertGreaterThan(0, count($result));
        $this->assertArrayHasKey('Population', $result[0]);

        // call with no zip
        $result = $this->adapter->getByZip(array());

        // should return empty array
        $this->assertNotNull($result);
        $this->assertCount(0, $result);

        $result = $this->adapter->getByZip('');
        $this->assertNotNull($result);
        $this->assertCount(0, $result);
    }

    public function testIsPaid() {
        // should return false no matter what param passed in
        $this->assertFalse($this->adapter->isPaid(''));
        $this->assertFalse($this->adapter->isPaid('testMethod'));
        $this->assertFalse($this->adapter->isPaid(new stdClass()));
    }

    public function testIsTeaser() {
        // should return true no matter what param passed in
        $this->assertTrue($this->adapter->isTeaser(''));
        $this->assertTrue($this->adapter->isTeaser('testMethod'));
        $this->assertTrue($this->adapter->isTeaser(new stdClass()));
    }
}
