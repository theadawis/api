<?php

class Icm_Search_Adapter_Whitepages_Test extends PHPUnit_Framework_TestCase {

    const TEST_PHONE_NUMBER = '8582925526'; // '2069730000';

    protected $apiUrl;
    protected $phoneNumber;
    protected $Adapter;

    protected function setUp() {
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->container = new Icm_Container_Factory();
        $this->container->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        $this->adapter = $this->container->get('whitepagesSearchAdapter');
    }

    public function testReversePhone() {
        // check reverse phone search
        $results = $this->adapter->reversePhone(array('phone' => self::TEST_PHONE_NUMBER));

        // should get back an array of icm_entity_phone objects
        $this->assertInternalType('array', $results);

        foreach ($results as $phone) {
            $this->assertInstanceOf('Icm_Entity_Phone', $phone);
            $this->assertNotNull($phone->identities);

            // each should have identities array of icm_entity_person objects
            foreach ($phone->identities as $person) {
                $this->assertInstanceOf('Icm_Entity_Person', $person->identity);
            }

            // should also have location attached
            $this->assertInstanceOf('Icm_Entity_Location', $phone->location);
        }
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneTeaser() {
        // should not be able to call reversePhoneTeaser method
        $this->adapter->reversePhoneTeaser(array());
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneCriteria() {
        // should not be able to call reversePhone without some criteria
        $this->adapter->reversePhone(array());
    }

    public function testIsTeaser() {
        $this->assertFalse($this->adapter->isTeaser('test'));
    }

    public function testIsPaid() {
        $this->assertTrue($this->adapter->isPaid('test'));
    }
}
