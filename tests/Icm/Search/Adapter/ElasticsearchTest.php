<?php

class Icm_Search_Adapter_Elasticsearch_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Internal_Elasticsearch
     */
    protected $elasticsearch;

    /**
     * @var Icm_Config
     */
    protected $config;

    protected function setUp(){
        $this->config = Icm_Config::fromIni('/sites/api/config/development/defaults.ini');
        $this->elasticsearch = new Icm_Service_Internal_Elasticsearch($this->config->getSection('elasticsearch'));
    }

    public function testBackgroundCheckTeaser(){
        $this->markTestIncomplete('No asserts written yet');
        $criteria = array(
            'first_name'=>'john',
            'last_name'=>'smith',
            'state'=>'ca'
        );
        $adapter = new Icm_Search_Adapter_Elasticsearch($this->elasticsearch);
        $map = new Icm_Entity_Factory_Map();
        $map->attach('person', new Icm_Entity_Factory('Icm_Entity_Person'), new Icm_Entity_Plugin_Broker());
        $adapter->setFactoryMap($map);
    }
}