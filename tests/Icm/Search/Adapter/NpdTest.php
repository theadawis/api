<?php

class Icm_Search_Adapter_Npd_Test extends PHPUnit_Framework_TestCase {

    protected function setUp() {
        defined('ENVIRONMENT') || define('ENVIRONMENT', trim(file_get_contents('/etc/tcg/environment')));
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->container = new Icm_Container_Factory();
        $this->container->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testBackgroundcheck() {
        Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 0: '));

        if (!extension_loaded('mssql')) {
            $this->markTestSkipped('MSSql extension not loaded');
        }

        // setup npdAdapter
        $npdAdapter = $this->container->get('npdSearchAdapter');

        // tell search what data to get
        $searchArgs = array(
            'first_name' => 'Joseph',
            'last_name' => 'Smith',
            'state' => 'CA',
        );

        // go get the data
        $results = $npdAdapter->backgroundCheck($searchArgs);
        $this->assertTrue(is_array($results));
        $this->assertTrue(count($results) > 0);

        // paginator will be our interface into pages of search data
        $paginator = Zend_Paginator::factory($results);

        // cache these results
        Zend_Paginator::setCache(Icm_Util_Cache::getCacheManager()->getCache('default'));

        // set up some pagination rules
        $paginator->setCurrentPageNumber(6);
        $paginator->setItemCountPerPage(50);

        $this->assertTrue($paginator->getCurrentItemCount() == 50);
        $this->assertTrue($paginator->getCurrentPageNumber() == 6);

        $page = $paginator->getCurrentItems();

        foreach ($page as $result){
            $this->assertTrue(is_subclass_of($result, 'Icm_Entity'));
        }

        // go to the next page
        $paginator->setCurrentPageNumber(3);

        $this->assertTrue($paginator->getCurrentItemCount() == 50);
        $this->assertTrue($paginator->getCurrentPageNumber() == 3);

        $page = $paginator->getCurrentItems();

        foreach ($page as $result){
            $this->assertTrue(is_subclass_of($result, 'Icm_Entity'));
        }
    }

    public function testReversePhone() {
        $phonenumber = '8582925525';
        $npdAdapter = $this->container->get('npdSearchAdapter');
        $people = $npdAdapter->reversePhone(array('phone' => $phonenumber));
        $this->assertEquals($phonenumber, $people[0]->phone);
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneTeaser() {
        $npdAdapter = $this->container->get('npdSearchAdapter');
        $npdAdapter->reversePhoneTeaser(array());
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testBackgroundCheckTeaser() {
        $npdAdapter = $this->container->get('npdSearchAdapter');
        $npdAdapter->backgroundCheckTeaser(array());
    }
}
