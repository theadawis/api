<?php

/**
 * Test Redis Search Adapter
 */
class Icm_Search_Adapter_RedisSearch_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Internal_RedisSearch
     */
    protected $redis;

    /**
     * @var Icm_Config
     */
    protected $config;

    protected function setUp(){
        $this->redis = $this->getMock("Icm_Redis", array('keys'), array(), '', false);
        $map = array(
                array('dev-popularnames-firstname:joh*', array('firstname:john:1','firstname:johnny:2')),
                array('dev-popularnames-lastname:sm*', array('lastname:smith:1','lastname:smothers:2'))
                );
        $this->redis->expects($this->any())->method('keys')->will($this->returnValueMap($map));
        $this->adapter = new Icm_Search_Adapter_RedisPopularNames($this->redis);
    }

    public function testRedisSearchCriteriaEmptyNoException(){
        $result = $this->adapter->nameSearch();
    }

    public function testRedisSearchCriteriaEmptyArrayNoException(){
        $this->adapter->nameSearch(array());
    }

    public function testRedisSearchCriteriaBadKeysNoException(){
        $this->assertEquals(array(), $this->adapter->nameSearch(array('foo'=>'joh')));
        $this->assertEquals(array(), $this->adapter->nameSearch(array('name'=>'joh','bar'=>'notype')));
    }

    public function testRedisSearchCriteriaNullException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch(null);
    }

    public function testRedisSearchCriteriaEmptyException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch('');
    }

    public function testRedisSearchCriteriaIntegerException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch(1);
    }

    public function testRedisSearchFirstnameSuccess(){
        // keymaker overrides the static call in nameSearch
        $map = array(array('firstname','joh', 'dev-popularnames-firstname:joh*'));
        $keymaker = $this->getMock('Icm_Util_PopularNames');
        $keymaker::staticExpects($this->any())->method('makePopularnamesRedisSearchKey')
        ->will($this->returnValueMap($map));
        $this->adapter->setKeymaker($keymaker);

        $criteria = array('name'=>'joh','type'=>'firstname');
        $result = $this->adapter->nameSearch($criteria);
        $this->assertTrue(is_a($result[0], "Icm_Entity_PopularPerson"));
        $this->assertEquals('john', $result[0]->name);
        $this->assertTrue(is_a($result[1], "Icm_Entity_PopularPerson"));
        $this->assertEquals('johnny', $result[1]->name);
    }

    public function testRedisSearchLastnameSuccess(){
        // keymaker overrides the static call in nameSearch
        $map = array(array('lastname','sm', 'dev-popularnames-lastname:sm*'));
        $keymaker = $this->getMock('Icm_Util_PopularNames');
        $keymaker::staticExpects($this->any())->method('makePopularnamesRedisSearchKey')
        ->will($this->returnValueMap($map));
        $this->adapter->setKeymaker($keymaker);

        $criteria = array('name'=>'sm','type'=>'lastname');
        $result = $this->adapter->nameSearch($criteria);
        $this->assertTrue(is_a($result[0], "Icm_Entity_PopularPerson"));
        $this->assertEquals('smith', $result[0]->name);
    }
}