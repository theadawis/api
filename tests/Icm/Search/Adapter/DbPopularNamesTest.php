<?php

/**
 * This mock class is required to prevent PDO __construct() from being called
 */
class MockPDOHelper extends Icm_Db_Pdo
{
    public function __construct () {}
}

/**
 * Test DB Search Adapter
 */
class Icm_Search_Adapter_DbSearch_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Internal_DbSearch
     */
    protected $cgInternalDb;

    /**
     * @var Icm_Config
     */
    protected $config;

    protected function setUp(){
        $this->cgInternalDb = $this->getMock("MockPDOHelper", array('fetchAll'), array(), '', true);

        // this mocks fetchAll(string $sql, array $replacements)
        // and returns name list based on parameters
        $this->cgInternalDb->expects($this->any())
             ->method('fetchAll')
             ->will($this->returnCallback(function ($sql,$name) {
                         if (strpos($sql, 'firstnames') !== FALSE && $name['name'] == 'joh%') {
                             return array(array('name'=>'john'),array('name'=>'johnny'));
                         }

                         if (strpos($sql, 'lastnames') !== FALSE && $name['name'] == 'sm%') {
                             return array(array('name'=>'smith'),array('name'=>'smothers'));
                         }})
        ); // end will()

        $this->adapter = new Icm_Search_Adapter_DbPopularNames($this->cgInternalDb);

        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));
    }

    public function testDbSearchCriteriaEmptyNoException(){
        $result = $this->adapter->nameSearch();
    }

    public function testDbSearchCriteriaEmptyArrayNoException(){
        $this->adapter->nameSearch(array());
    }

    public function testDbSearchCriteriaBadElementNamesNoException(){
        $this->assertEquals(array(), $this->adapter->nameSearch(array('foo'=>'joh')));
        $this->assertEquals(array(), $this->adapter->nameSearch(array('name'=>'joh','bar'=>'notype')));
    }

    public function testDbSearchCriteriaNullException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch(null);
    }

    public function testDbSearchCriteriaEmptyException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch('');
    }

    public function testDbSearchCriteriaIntegerException(){
        $this->setExpectedException('Exception');
        $this->adapter->nameSearch(1);
    }

    public function testDbSearchFirstname(){
        $criteria = array(
            'name'=>'joh',
            'type'=>'firstname',
            'maxresults' => 5,
        );
        $result = $this->adapter->nameSearch($criteria);
        $this->assertTrue(is_a($result[0], "Icm_Entity_PopularPerson"));
        $this->assertEquals('john', $result[0]->name);
    }

    public function testDbSearchLastname(){
        $criteria = array(
            'name'=>'sm',
            'type'=>'lastname',
            'maxresults' => 5,
        );
        $result = $this->adapter->nameSearch($criteria);
        $this->assertTrue(is_a($result[0], "Icm_Entity_PopularPerson"));
        $this->assertEquals('smith', $result[0]->name);
    }
}
