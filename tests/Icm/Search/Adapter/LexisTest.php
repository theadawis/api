<?php

class Icm_Search_Adapter_Lexis_Test extends PHPUnit_Framework_TestCase
{
    protected $api;

    protected function setUp(){
        date_default_timezone_set('America/Los_Angeles');
        defined('ENVIRONMENT') || define('ENVIRONMENT', trim(file_get_contents('/etc/tcg/environment')));
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');

        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $this->api->dataContainer = new Icm_Container_Factory();
        $this->api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));
    }

    public function testTeaserSearch(){
         $criteria = array();
         $criteria[Icm_Search_Query::CRITERIA_ID] = mt_rand(5, 12);
         $criteria[Icm_Search_Query::CRITERIA_FIRSTNAME] = 'DEE';
         $criteria[Icm_Search_Query::CRITERIA_LASTNAME] = 'SKEEN';

         // Why mt random? The Lexis adapter caches search whether you like it or not, the random criteria prevents pulling a cached result.
         $criteria[Icm_Search_Query::CRITERIA_CITY] = '';
         $criteria[Icm_Search_Query::CRITERIA_STATE] = 'CA';
         $query = new Icm_Search_Query($criteria, 'backgroundCheck');
         $engine = $this->api->dataContainer->get('lexisOnlyPersonSearch');
         $result = $engine->search($query);
         $person = $result->offsetGet(0);

         // check that result[0] is a person entity
         $this->assertInstanceOf('Icm_Entity_Person', $person);

         $this->assertObjectHasAttribute('first_name', $person);
         $this->assertObjectHasAttribute('middle_name', $person);
         $this->assertObjectHasAttribute('last_name', $person);
         $this->assertObjectHasAttribute('dod', $person);
         $this->assertObjectHasAttribute('dob', $person);
         $this->assertObjectHasAttribute('yod', $person);
         $this->assertObjectHasAttribute('yob', $person);
         $this->assertObjectHasAttribute('zip', $person);
         $this->assertObjectHasAttribute('city', $person);
         $this->assertObjectHasAttribute('state', $person);
     }

    public function testComprehensiveSearch(){
        $criteria = array(Icm_Search_Query_Interface::CRITERIA_ID => '830473773');
        $query = new Icm_Search_Query($criteria, 'comprehensiveSearch');
        $engine = $this->api->dataContainer->get('lexisOnlyPersonSearch');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $person) {
            // should be the right type
            $this->assertInstanceOf('Icm_Entity_Person', $person);
        }
    }

    public function testReversePhone(){
        // try a search without allowing paid searches
        $criteria = array(Icm_Search_Query_Interface::CRITERIA_PHONENUMBER => '8664905980');
        $query = new Icm_Search_Query($criteria, 'reversePhone');

        $engine = $this->api->dataContainer->get('reversePhoneSearchLX');
        $result = $engine->search($query);

        // should have gotten empty search result back
        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertCount(0, $result->getData());

        // try same search allowing paid
        $query->setAllowPaid(true);
        $result = $engine->search($query);

        // should have gotten back empty search result
        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertCount(0, $result->getData());

        // try different paid search
        $criteria = array(Icm_Search_Query_Interface::CRITERIA_PHONENUMBER => '6192436871');
        $query = new Icm_Search_Query($criteria, 'reversePhone');
        $query->setAllowPaid(true);
        $result = $engine->search($query);

        // should have gotten back non-empty result
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $phone) {
            $this->assertInstanceOf('Icm_Entity_Phone', $phone);
        }
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testPersonSearchCriteria() {
        // try to search with blank criteria
        $criteria = array();
        $query = new Icm_Search_Query($criteria, 'personSearch');

        // should throw an exception
        $engine = $this->api->dataContainer->get('lexisOnlyPersonSearch');
        $result = $engine->search($query);
    }

    public function testPersonSearch() {
        $criteria = array(Icm_Search_Query_Interface::CRITERIA_ID => '830473773');
        $query = new Icm_Search_Query($criteria, 'personSearch');
        $engine = $this->api->dataContainer->get('lexisOnlyPersonSearch');
        $result = $engine->search($query);

        $this->assertInstanceOf('Icm_Search_Result', $result);
        $this->assertGreaterThan(0, count($result->getData()));

        foreach ($result->getData() as $person) {
            // should be the right type
            $this->assertInstanceOf('Icm_Entity_Person', $person);
        }
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneCriteria() {
        // try to search with blank criteria
        $criteria = array();
        $query = new Icm_Search_Query($criteria, 'reversePhone');
        $query->setAllowPaid(true);

        // should throw an exception
        $engine = $this->api->dataContainer->get('reversePhoneSearchLX');
        $result = $engine->search($query);
    }

    public function testIsTeaser() {
        // get a solo lexis search adapter
        $adapter = $this->api->dataContainer->get('lexisSearchAdapter');

        // three methods should count as teasers
        $this->assertTrue($adapter->isTeaser('backgroundCheck'));
        $this->assertTrue($adapter->isTeaser('backgroundCheckTeaser'));
        $this->assertTrue($adapter->isTeaser('reversePhoneTeaser'));

        // everything else should not
        $this->assertFalse($adapter->isTeaser('noMethodHere'));

        // should be able to handle empty arguments
        $this->assertFalse($adapter->isTeaser(''));
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testReversePhoneTeaser() {
        $adapter = $this->api->dataContainer->get('lexisSearchAdapter');
        $criteria = array(Icm_Search_Query_Interface::CRITERIA_PHONENUMBER => '8664905980');
        $adapter->reversePhoneTeaser($criteria);
    }

    /**
     * @expectedException Icm_Search_Adapter_Exception
     */
    public function testBackgroundCheckTeaserCriteria() {
        $adapter = $this->api->dataContainer->get('lexisSearchAdapter');
        $adapter->backgroundCheckTeaser(array());
    }
}
