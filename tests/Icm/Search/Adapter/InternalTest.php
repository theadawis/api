<?php

class Icm_Search_Adapter_Internal_Test extends  PHPUnit_Framework_TestCase {

    public function tesDb() {
        $conn = Icm_Db_Pdo::connect('internalDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'internalDb'));
        $callback = function($row) {
            return $row;
        };

        $results = $conn->fetchAll('
            SELECT *
            FROM
                site_optouts
            WHERE
                state = :state
        ', array('state' => 'CA'),$callback);
        $this->assertInternalType('array', $results);
        $this->assertTrue(count($results) > 0);
        $conn2 = Icm_Db_Pdo::getInstance('internalDb');
        $this->assertSame($conn, $conn2);
    }

    public function testOptout() {
        $this->markTestSkipped('Icm_Data_Search_Optout class no longer exists');
        try {
            Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 0: '));
            //instantiate search
            $search = new Icm_Data_Search_Optout();

            //tell search where to get the data
            $internalAdapter = Icm_Search_Adapter_Internal::factory(Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini','internalDb'));

            //tell search where/how to get the data
            $search->setAdapters(array($internalAdapter));

            //tell search what data to get
            //$search->addCriteria(Icm_Search_Engine::CRITERIA_FIRSTNAME, 'Jo*');
            $search->addCriteria(Icm_Search_Engine::CRITERIA_LASTNAME,  'Smith');
            $search->addCriteria(Icm_Search_Engine::CRITERIA_STATE,     'CA');

            //go get the data
            $results = $search->optout();
            $this->assertTrue(is_array($results));
            $this->assertTrue(count($results) > 0);

            //paginator will be our interface into pages of search data
            $paginator = Zend_Paginator::factory($results);

            //cache these results
            Zend_Paginator::setCache(Icm_Util_Cache::getCacheManager()->getCache('default'));
            //set up some pagination rules
            $paginator->setCurrentPageNumber(2);
            $paginator->setItemCountPerPage(50);

            $this->assertTrue($paginator->getCurrentItemCount() == 50);
            $this->assertTrue($paginator->getCurrentPageNumber() == 2);

            $page = $paginator->getCurrentItems();
            foreach($page as $result){
                $this->assertTrue(is_subclass_of($result, 'Icm_Entity'));
            }

            //go to the next page
            $paginator->setCurrentPageNumber(3);

            $this->assertTrue($paginator->getCurrentItemCount() == 50);
            $this->assertTrue($paginator->getCurrentPageNumber() == 3);

            $page = $paginator->getCurrentItems();
            foreach($page as $result){
                $this->assertTrue(is_subclass_of($result, 'Icm_Entity'));
            }
        }
        catch(Icm_Db_Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }

    }

}