<?php

class Icm_Queue_Adapter_Rabbitmq_Test extends PHPUnit_Framework_TestCase {

    protected $adapter;

    public function setUp() {
        parent::setUp();

        // create a new rabbitmq adapter
        $options = array(
            'host' => 'queue01',
            'exchange_name' => 'test',
            'vhost' => 'testing',
        );

        $this->adapter = new Icm_Queue_Adapter_Rabbitmq($options);
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testDeleteMessage() {
        // deleteMessage method should throw an exception
        $message = new Zend_Queue_Message(array('body' => 'Test Message'));
        $this->adapter->deleteMessage($message);
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testCount() {
        $this->adapter->count();
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testIsExists() {
        $this->adapter->isExists('test');
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testCreate() {
        $this->adapter->create('test');
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testDelete() {
        $this->adapter->delete('test');
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testGetQueues() {
        $this->adapter->getQueues();
    }

    /**
     * Test that adapter reports the right capabilities
     */
    public function testGetCapabilities() {
        // build array of expected values
        $expected = array(
            'create' => false,
            'delete' => false,
            'send' => true,
            'receive' => true,
            'deleteMessage' => false,
            'getQueues' => false,
            'count' => false,
            'isExists' => false,
        );

        // get the adapter's capabilities
        $capabilities = $this->adapter->getCapabilities();

        // should match expected
        $this->assertEquals($expected, $capabilities);
    }

    /**
     * @expectedException Zend_Queue_Exception
     */
    public function testSendNoQueue() {
        // sending message without queue should throw exception
        $this->adapter->send('Test Message');
    }

    /**
     * Ensure send method handles edge cases correctly
     */
    public function testSend() {
        // create a queue to use for the adapter
        $options = array(
            'name' => 'test_queue',
            'routing_key' => 'test.queue.key',
        );
        $queue = new Zend_Queue($this->adapter, $options);

        // purge the queue of leftover messages
        $this->adapter->purgeQueue();

        // sending regular message should return true
        $test_message = 'Test Message';
        $response = $this->adapter->send($test_message);
        $this->assertTrue($response);

        // should have pushed the right message into the queue
        $messages = $queue->receive(1);

        $this->assertTrue($messages instanceof Zend_Queue_Message_Iterator);
        $this->assertEquals(1, count($messages));

        $message = $messages->current();
        $this->assertEquals($test_message, $message->body);

        // send a blank message
        $response = $this->adapter->send('');

        // should not have sent anything out
        $this->assertFalse($response);
        $messages = $queue->receive(1);
        $this->assertEquals(0, count($messages));

        // send a string of spaces
        $response = $this->adapter->send('   ');

        // should not have sent anything out
        $this->assertFalse($response);
        $messages = $queue->receive(1);
        $this->assertEquals(0, count($messages));

        // sending an array should auto-convert to json
        $data = array(
            'column' => 'first',
            'row' => 'second',
            'comments' => 'this is a comment',
        );
        $response = $this->adapter->send($data);
        $this->assertTrue($response);

        $messages = $queue->receive(1);
        $this->assertEquals(1, count($messages));

        $message = $messages->current();
        $this->assertEquals(json_encode($data), $message->body);

        // including queue in method call should override built-in queue
        $options = array(
            'name' => 'testing_queue',
            'routing_key' => 'testing.queue.key',
        );
        $new_queue = new Zend_Queue(null, $options);
        $test_message = 'Testing Message';
        $response = $this->adapter->send($test_message, $new_queue);
        $this->assertTrue($response);

        // old queue should not have any messages
        $messages = $queue->receive(1);
        $this->assertEquals(0, count($messages));

        // new queue messages should match
        $messages = $this->adapter->receive(1, null, $new_queue);
        $this->assertEquals(1, count($messages));
        $message = $messages->current();
        $this->assertEquals($test_message, $message->body);
    }

    /**
     * Test the rabbitmq adapter's receive method
     */
    public function testReceive() {
        // create a queue to use for the adapter
        $options = array(
            'name' => 'test_queue',
            'routing_key' => 'test.queue',
        );
        $queue = new Zend_Queue($this->adapter, $options);

        // purge the queue of leftover messages
        $this->adapter->purgeQueue();

        // should get single message back if send one
        $test_messages = array('Test receive message');
        $queue->send($test_messages[0]);
        $messages = $this->adapter->receive(1);

        // messages should be correct class
        $this->assertTrue($messages instanceof Zend_Queue_Message_Iterator);
        $this->assertEquals(1, count($messages));
        $this->assertMessagesMatch($test_messages, $messages);

        // should get three messages back if send three
        $test_messages = array(
            'Test message one',
            'Test message two',
            'Test message three',
        );

        foreach ($test_messages as $test_message) {
            $queue->send($test_message);
        }

        $messages = $this->adapter->receive(3);
        $this->assertEquals(count($test_messages), count($messages));
        $this->assertMessagesMatch($test_messages, $messages);

        // should be able to pull last two messages if push three
        foreach ($test_messages as $test_message) {
            $queue->send($test_message);
        }

        $messages = $this->adapter->receive(2);
        $this->assertEquals(2, count($messages));
        $this->assertMessagesMatch($test_messages, $messages);

        // calling without maxMessages should pull just one
        $messages = $this->adapter->receive();
        $this->assertEquals(1, count($messages));
        $this->assertMessagesMatch($test_messages, $messages);

        // including queue in method call should override instance attribute
        $options = array(
            'name' => 'testing_queue',
            'routing_key' => 'testing.queue.key',
        );
        $new_queue = new Zend_Queue(null, $options);
        $test_message = 'Testing Message';
        $response = $this->adapter->send($test_message, $new_queue);
        $this->assertTrue($response);

        // old queue should not have any messages
        $messages = $queue->receive(1);
        $this->assertEquals(0, count($messages));

        // new queue messages should match
        $messages = $this->adapter->receive(1, null, $new_queue);
        $this->assertEquals(1, count($messages));
        $message = $messages->current();
        $this->assertEquals($test_message, $message->body);
    }

    /**
     * Test the purgeQueue method
     */
    public function testPurgeQueue() {
        // setup a queue for the adapter
        $options = array('name' => 'test_queue');
        $queue = new Zend_Queue($this->adapter, $options);

        // purge queue shouldn't throw errors on empty queue
        $this->adapter->purgeQueue();

        // should have no messages
        $messages = $queue->receive(1);
        $this->assertEquals(0, count($messages));

        // queue 3 messages
        $test_messages = array(
            'Test message one',
            'Test message two',
            'Test message three',
        );

        foreach ($test_messages as $test_message) {
            $queue->send($test_message);
        }

        // purge
        $this->adapter->purgeQueue();

        // should have nothing
        $messages = $queue->receive(3);
        $this->assertEquals(0, count($messages));

        // including queue in method call should override instance attribute
        $options = array('name' => 'testing_queue');
        $new_queue = new Zend_Queue(null, $options);
        $test_message = 'Testing Message';
        $queue->send($test_message);
        $this->adapter->send($test_message, null, $new_queue);
        $this->adapter->purgeQueue($new_queue);

        // purged queue should not have any messages
        $messages = $this->adapter->receive(1, null, $new_queue);
        $this->assertEquals(0, count($messages));

        // non-purged queue should have something
        $messages = $queue->receive(1);
        $this->assertEquals(1, count($messages));
    }

    /**
     * Verify the given messages were the ones passed into the queue
     *
     * @param string[] $expected
     * @param Zend_Queue_Message_Iterator $received
     */
    public function assertMessagesMatch($expected, $received) {
        foreach ($received as $message) {
            $this->assertTrue(in_array($message->body, $expected));
        }
    }
}