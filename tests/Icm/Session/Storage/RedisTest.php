<?php

class Icm_Session_Storage_Redis_Test extends PHPUnit_Framework_TestCase
{
    public function testRedis(){
        //configure the session handler
        $redisConfig = Icm_Config::fromIni('./config/config.ini', 'redis');
        $redis = new Icm_Redis($redisConfig);
        $storage = new Icm_Session_Storage_Redis(Icm_Config::fromArray(array()), $redis);

        //set the session handler
        Icm_Session::setStorageAdapter($storage);

        //set some data
        $_SESSION['test'] = true;
        $this->assertTrue($_SESSION['test']);

        //unset some data
        unset($_SESSION['test']);
        $this->assertTrue(!isset($_SESSION['test']));
    }
}
