<?php

class Icm_Image_Resize_Test extends PHPUnit_Framework_TestCase
{
    public function testResize(){
        $image = Icm_Image_Resize::load('http://3.bp.blogspot.com/_wxtdziSoI_M/S8KucmuYKZI/AAAAAAAABrU/9YVxoUjyM_Y/s1600/1439530136_b8411b54e8.jpg');
        $image->resizeToHeight(100)->save('/var/tmp/image_test.jpg');

        $resizedImage = Icm_Image_Resize::load('/var/tmp/image_test.jpg');
        $this->assertEquals(100, $resizedImage->getHeight());
    }

    public function testResizeWithWhitespace(){
        $image = Icm_Image_Resize::load('http://3.bp.blogspot.com/_wxtdziSoI_M/S8KucmuYKZI/AAAAAAAABrU/9YVxoUjyM_Y/s1600/1439530136_b8411b54e8.jpg');
        $image->resizeWithWhitespace(50, 50)->save('/var/tmp/image_test.jpg');
        $resizedImage = Icm_Image_Resize::load('/var/tmp/image_test.jpg');
        $this->assertEquals(50, $resizedImage->getWidth());
        $this->assertEquals(50, $resizedImage->getHeight());

        $image = Icm_Image_Resize::load('http://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Tour_Eiffel_Wikimedia_Commons.jpg/220px-Tour_Eiffel_Wikimedia_Commons.jpg');
        $image->resizeWithWhitespace(50, 50)->save('/var/tmp/image_test2.jpg');
        $resizedImage = Icm_Image_Resize::load('/var/tmp/image_test2.jpg');
        $this->assertEquals(50, $resizedImage->getWidth());
        $this->assertEquals(50, $resizedImage->getHeight());
    }
}