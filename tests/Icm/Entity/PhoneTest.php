<?php

class Icm_Entity_PhoneTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
        // bootstrap the api
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
    }

    public function testCreate() {
        // setup all phone properties
        $properties = array(
            'phone' => '479-355-6754',
            'zip' => '92101',
            'state' => 'CA',
            'ltype' => 'something',
            'carrier' => 'verizon',
        );

        // create new phone
        $phone = Icm_Entity_Phone::create($properties);

        // should have all matching properties
        $this->assertInstanceOf('Icm_Entity_Phone', $phone);

        foreach ($properties as $name => $value) {
            $this->assertEquals($value, $phone->$name);
        }
    }

    /**
     * @expectedException Icm_Entity_Exception
     */
    public function testCreateInvalidProperty() {
        // should not let us create a phone with an invalid property
        $properties = array(
            'phone' => '479-355-3243',
            'testprop' => 'forsure',
        );

        $phone = Icm_Entity_Phone::create($properties);
    }

    public function testGetHash() {
        // create a new phone
        $phone = Icm_Entity_Phone::create(array('phone' => '4563423456'));

        // get the hash
        $hash = $phone->getHash();

        // should be an empty array
        $this->assertInternalType('array', $hash);
        $this->assertEmpty($hash);
    }
}