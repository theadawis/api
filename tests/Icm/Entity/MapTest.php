<?php

class Icm_Entity_MapTest extends PHPUnit_Framework_TestCase
{
    public function testAddressHelper() {
        // initialize the api and dataContainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');

        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // grab an entity map
        $entityMap = Icm_Entity_Map::getInstance();

        // create a dummy parent object
        $parent = new stdClass();

        // create a dummy value object with some address properties
        $value = new stdClass();
        $value->zip = '92101';
        $value->address = '808 North 34th St';
        $value->anyValue = 'thisisanarbitrarystring';

        // call the addressHelper for the parent, arbitrary key, and value
        $key = 'testKey';
        $entityMap->addressHelper($parent, $key, $value);

        // should have set the key on the parent
        $location = $parent->testKey;
        $this->assertNotNull($location);

        // should have added a location entity at that key
        $this->assertInstanceOf('Icm_Entity_Location', $location);

        // location entity properties should match the dummy value object
        $this->assertEquals($value->zip, $location->zip);
        $this->assertEquals($value->address, $location->street);
        $this->assertEquals($value->anyValue, $location->anyValue);
    }
}