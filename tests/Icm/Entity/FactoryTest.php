<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/28/12
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Factory_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Entity_Plugin_Broker $broker
     */
    protected $broker;

    public function setUp() {
        $this->broker = $this->getMock('Icm_Entity_Plugin_Broker');
    }

    public function testCreate() {
        $f = new Icm_Entity_Factory('Icm_Entity_Person', $this->broker);
        $this->assertInstanceOf('Icm_Entity_Factory', $f);
        return $f;
    }

    /**
     * @param Icm_Entity_Factory $f
     * @depends testCreate
     */
    public function testCreateEntity($f) {
        $e = $f->create();

        $this->assertInstanceOf('Icm_Entity_Person', $e);
    }
}
