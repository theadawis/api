<?php

class Icm_Entity_Plugin_Broker_Test extends PHPUnit_Framework_TestCase
{
    public function testCreate() {
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $this->api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        $b = new Icm_Entity_Plugin_Broker();
        $this->assertInstanceOf('Icm_Entity_Plugin_Broker', $b);
        return $b;
    }

    /**
     * @depends testCreate
     * @param Icm_Entity_Plugin_Broker $b
     */
    public function testRegisterPlugin($b) {
        $plugin = $this->getMock('Icm_Entity_Plugin_Death', array('__invoke'), array(), '', false);

        $entity = new Icm_Entity_Person();

        $plugin->expects($this->once())->method('__invoke')->will($this->returnValue(array('foo' => 'bar')));

        $b->setFactoryMap(new Icm_Entity_Factory_Map());
        $b->registerPlugin('test', $plugin);

        $b('test', $entity);

        $this->assertInternalType('array', $entity->test);
        $this->assertArrayHasKey('foo', $entity->test);
    }
}
