<?php

class Icm_Entity_Plugin_Faa_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Npd_Api');
    }

    public function testInvokePersonEntity(){
        $plug = new Icm_Entity_Plugin_Faa();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person', array('isA'));
        $entity->expects($this->once())->method('isA')->will($this->returnValue(true));
        $this->adapter->expects($this->once())->method('faaLicenseSearch')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug($entity)));
    }
}