<?php

class Icm_Entity_Plugin_Ethnicitybreakdown_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Zipcodes_Db
     */
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Zipcodes_Db');
    }

    public function testInvokeLocationEntity(){
        $plug = new Icm_Entity_Plugin_Ethnicitybreakdown();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Location', array('isA'));
        $entity->expects($this->once())->method('isA')->will($this->returnValue(true));
        $this->adapter->expects($this->once())->method('getEthnicityData')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug($entity)));
    }
}