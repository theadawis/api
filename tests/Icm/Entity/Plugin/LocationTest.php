<?php

class Icm_Entity_Plugin_LocationTest extends PHPUnit_Framework_TestCase
{
    public function testInvoke() {
        // setup the api and datacontainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // pull out a location plugin
        $locationPlugin = $api->dataContainer->get('locationPlugin');
        $entityFactoryMap = $api->dataContainer->get('entityFactoryMap');
        $locationPlugin->setFactoryMap($entityFactoryMap);

        // create non-person entity to invoke with
        $entity = Icm_Entity_Location::create(array('zip' => '92101'));

        // should return null
        $this->assertNull($locationPlugin($entity));

        // create person with some address info
        $properties = array(
            'first_name' => 'John',
            'zip' => '92101',
            'address' => '808 North 34th St',
        );
        $person = Icm_Entity_Person::create($properties);

        // should return location with that data
        $record = $locationPlugin($person);
        $this->assertInstanceOf('Icm_Entity_Location', $record);
        $this->assertEquals($properties['zip'], $record->zip);
        $this->assertEquals($properties['address'], $record->address);

        // create person with full address info
        $properties = array(
            'first_name' => 'John',
            'zip' => '92101',
            'address' => '808 North 34th St',
            'state' => 'CA',
            'city' => 'San Diego',
        );
        $person = Icm_Entity_Person::create($properties);

        // should return location with all data
        $record = $locationPlugin($person);
        $this->assertInstanceOf('Icm_Entity_Location', $record);
        $this->assertEquals($properties['zip'], $record->zip);
        $this->assertEquals($properties['address'], $record->address);
        $this->assertEquals($properties['state'], $record->state);
        $this->assertEquals($properties['city'], $record->city);

        // create person without zipcode
        $properties = array(
            'first_name' => 'John',
            'last_name' => 'Smith',
        );
        $person = Icm_Entity_Person::create($properties);

        // should return null
        $this->assertNull($locationPlugin($person));
    }
    public function testSetIsPoBox() {
        // setup the api and datacontainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // pull out a location plugin
        $locationPlugin = $api->dataContainer->get('locationPlugin');
        $entityFactoryMap = $api->dataContainer->get('entityFactoryMap');
        $locationPlugin->setFactoryMap($entityFactoryMap);
        // create person with PO Box
        $properties = array(
            'first_name' => 'John',
            'zip' => '92101',
            'address' => 'PO Box 1234',
            'state' => 'CA',
            'city' => 'San Diego',
        );
        $person = Icm_Entity_Person::create($properties);
        $record = $locationPlugin($person);
        $this->assertTrue($record->is_pobox);
    }
}
