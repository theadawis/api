<?php

class Icm_Entity_Plugin_Sexoffender_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Npd_Db');
    }

    public function testInvokePersonEntity(){
        $plug = new Icm_Entity_Plugin_Sexoffender();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person', array('isA'));
        $entity->expects($this->exactly(1))->method('isA')->will($this->returnCallback(function($val){
            if ($val == 'Icm_Entity_Person'){
                return true;
            }
        }));
        $this->adapter->expects($this->once())->method('sexOffenderSearch')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug->__invoke($entity)));
    }

    public function testInvokeLocationEntity(){
        $plug = new Icm_Entity_Plugin_Sexoffender();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Location', array('isA'));
        $entity->expects($this->exactly(2))->method('isA')->will($this->returnCallback(function($val){
            if ($val == 'Icm_Entity_Person'){
                return false;
            }
            else if ($val == 'Icm_Entity_Location'){
                return true;
            }
        }));
        $this->adapter->expects($this->once())->method('sexOffenderSearch')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug->__invoke($entity)));
    }
}