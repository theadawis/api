<?php

class Icm_Entity_Plugin_Fbidata_Test extends PHPUnit_Framework_TestCase
{
    public function testInvoke() {
        // setup the api and dataContainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // pull out a fbi plugin
        $fbiPlugin = $api->dataContainer->get('fbiDataPlugin');

        // build a non-location for invoking
        $entity = Icm_Entity_Person::create(array('first_name' => 'John'));

        // when invoked, should return null
        $this->assertNull($fbiPlugin($entity));

        // build a location entity for invoking with all data
        $properties = array(
            'city' => 'San Diego',
            'zip' => '92101',
        );
        $location = Icm_Entity_Location::create($properties);
        $location->set('stateLongName', 'California');

        // should return a set of stats for that city and state
        $record = $fbiPlugin($location);

        // should have city and state set
        $this->assertArrayHasKey('city', $record);
        $this->assertArrayHasKey('state', $record);

        // should have five years of entries for city and state
        $this->assertCount(5, $record['city']);
        $this->assertCount(5, $record['state']);

        // build a location entity for invoking with just zipcode
        $properties = array(
            'zip' => '92101',
        );
        $location = Icm_Entity_Location::create($properties);

        // should still return stats for city and state
        $record = $fbiPlugin($location);

        // should have city and state set
        $this->assertArrayHasKey('city', $record);
        $this->assertArrayHasKey('state', $record);

        // should have five years of entries for city and state
        $this->assertCount(5, $record['city']);
        $this->assertCount(5, $record['state']);
    }
}