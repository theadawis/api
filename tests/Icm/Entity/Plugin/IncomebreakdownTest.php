<?php

class Icm_Entity_Plugin_IncomebreakdownTest extends PHPUnit_Framework_TestCase
{
    public function testInvoke() {
        // setup the api and datacontainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // pull out the incomebreakdown plugin
        $incomePlugin = $api->dataContainer->get('incomePlugin');

        // create a non-location for invoking
        $entity = Icm_Entity_Person::create(array('first_name' => 'John'));

        // should return null
        $this->assertNull($incomePlugin($entity));

        // create a location with just zipcode for invoking
        $properties = array(
            'zip' => '92101',
        );
        $location = Icm_Entity_Location::create($properties);

        // should return average income data
        $record = $incomePlugin($location);
        $this->assertArrayHasKey('IncomePerHousehold', $record);
        $this->assertNotNull($record['IncomePerHousehold']);
    }
}