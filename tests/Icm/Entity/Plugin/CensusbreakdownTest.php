<?php

class Icm_Entity_Plugin_CensusbreakdownTest extends PHPUnit_Framework_TestCase
{
    public function testInvoke() {
        // setup the api and dataContainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // pull out a census plugin
        $censusPlugin = $api->dataContainer->get('censusPlugin');

        // build a non-location for invoking
        $entity = Icm_Entity_Person::create(array('first_name' => 'John'));

        // when invoked, should return null
        $this->assertNull($censusPlugin($entity));

        // build a location entity for invoking
        $properties = array(
            'zip' => '92101',
            'street' => '808 North 34th St',
        );
        $location = Icm_Entity_Location::create($properties);

        // should return a set of db records for that zipcode
        $record = $censusPlugin($location);

        // should have all the right properties set
        $expected = array(
            'AverageHouseValue',
            'Latitude',
            'Longitude',
            'Elevation',
            'NumberOfBusinesses',
            'NumberOfEmployees',
            'BusinessAnnualPayroll',
            'GrowthRank',
            'Population',
            'HouseholdsPerZipcode',
            'IncomePerHousehold',
            'County',
        );

        foreach ($expected as $property) {
            $this->assertArrayHasKey($property, $record);
        }
    }
}