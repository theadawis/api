<?php

class Icm_Entity_Plugin_ZodiacTest extends PHPUnit_Framework_TestCase
{
    public function testInvoke() {
        // setup api and datacontainer
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/config.ini'));
        $api->dataContainer = new Icm_Container_Factory();
        $api->dataContainer->loadXml(simplexml_load_file('/sites/api/tests/config/services/dataContainer.xml'));

        // grab a zodiac plugin
        $zodiacPlugin = $api->dataContainer->get('zodiacPlugin');

        // create a non-person entity to invoke with
        $properties = array(
            'city' => 'San Diego',
            'zip' => '92101',
        );
        $location = Icm_Entity_Location::create($properties);

        // should return null
        $this->assertNull($zodiacPlugin($location));

        // create a person for invocation
        $properties = array(
            'first_name' => 'John',
            'dob' => '12/01/1978',
        );
        $person = Icm_Entity_Person::create($properties);

        // should return anonymous function
        $returnFunction = $zodiacPlugin($person);

        // should work if used on two sign strings
        $compatibility = $returnFunction('aries', 'capricorn');
        $this->assertNotNull($compatibility);
        $this->assertArrayHasKey('score', $compatibility);
        $this->assertNotNull($compatibility['score']);
    }
}