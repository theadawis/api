<?php

class Icm_Entity_Plugin_Social_Test extends PHPUnit_Framework_TestCase
{
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Pipl_Api', array(), array(), '', false);
    }

    public function testInvokePersonEntity(){
        $plugin = new Icm_Entity_Plugin_Social();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person');
        $entity->expects($this->once())
                ->method('isA')
                ->will($this->returnValue(true));

        $entity->expects($this->exactly(8))
                ->method("__get")
                ->will($this->returnValue(10));

        $this->adapter->expects($this->once())
                    ->method('search')
                    ->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }
}
