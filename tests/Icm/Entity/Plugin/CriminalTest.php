<?php

/**
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Criminal_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Npd_Api');
    }

    public function testInvokePersonEntity(){
        $plug = new Icm_Entity_Plugin_Criminal();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person');
        $entity->expects($this->once())->method('isA')->will($this->returnValue(true));
        $this->adapter->expects($this->once())->method('criminalSearch')->will($this->returnValue(array()));
        $this->assertInstanceOf('Icm_Search_Result', $plug($entity));
    }

    /**
     * Test to ensure we don't return duplicate results
     */
    public function testRemoveDuplicates() {
        // setup plugin
        $plugin = new Icm_Entity_Plugin_Criminal();
        $plugin->setAdapter($this->adapter);

        $map = new Icm_Entity_Factory_Map();
        $factory = new Icm_Entity_Factory('Icm_Entity_Criminal');
        $broker = new Icm_Entity_Plugin_Broker();
        $map->attach('criminal', $factory, $broker);
        $plugin->setFactoryMap($map);

        // create person with multiple addresses
        $addresses = array(
            0 => array(
                'state' => 'OR',
            ),
            1 => array(
                'state' => 'CA',
            ),
        );
        $firstName = 'Fred';
        $lastName = 'Flintstone';

        $person = new Icm_Entity_Person();
        $person->priorAddresses = $addresses;
        $person->first_name = $firstName;
        $person->last_name = $lastName;
        $person->state = 'DE';

        // setup adapter to return duplicate criminal history
        $records = array(
            0 => Icm_Struct_Criminalrecord::fromArray(array(
                'first_name' => $firstName,
                'last_name' => $lastName,
                'case_number' => 'Y0145DS',
                'charges_filed_date' => '04/12/2005',
                'charge_category' => 'Criminal/traffic',
                'offense_description1' => 'jaywalking',
                'source_name' => 'youtube',
                'source_state' => 'CA',
            )),
            1 => Icm_Struct_Criminalrecord::fromArray(array(
                'first_name' => $firstName,
                'last_name' => $lastName,
                'case_number' => 'Z3451AW',
                'charges_filed_date' => '06/21/2009',
                'charge_category' => 'Criminal/traffic',
                'offense_description1' => 'bluejaywalking',
                'source_name' => 'youtube',
                'source_state' => 'CA',
            )),
        );
        $this->adapter->expects($this->exactly(3))->method('criminalSearch')->will($this->returnValue($records));

        // invoke the plugin
        $result = $plugin($person);
        $this->assertInstanceOf('Icm_Search_Result', $result);

        // should only return 2 results
        $this->assertEquals(2, $result->count());

        // should not be any duplicates in the result set
        $firstResult = $result->offsetGet(0);
        $secondResult = $result->offsetGet(1);
        $this->assertNotEquals($firstResult->case_number, $secondResult->case_number);
    }
}