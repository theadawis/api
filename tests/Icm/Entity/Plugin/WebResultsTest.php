<?php

/**
 *   @author: Ryan Baker
 *
 */
class Icm_Entity_Plugin_WebResults_Test extends PHPUnit_Framework_TestCase{
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Yahoo_Api',
                                        array(),
                                        array(),"",false);
    }

    public function testInvokePersonEntity(){
        $plugin = new Icm_Entity_Plugin_WebResults();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person');
        $entity->expects($this->once())->method('isA')->will($this->returnValue(true));
        $entity->expects($this->exactly(2))->method('__get')->will($this->returnValue("Ron"));
        $this->adapter->expects($this->once())->method('search')->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }

    public function testInvokeCrimeEntity(){
        $plugin = new Icm_Entity_Plugin_WebResults();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Crime');
        $entity->expects($this->exactly(2))
                ->method('isA')
                ->will( $this->returnCallback(function($val){
                    if($val == 'Icm_Enitity_Person'){return false;}
                    else if($val == 'Icm_Entity_Crime'){return true;}
                    }
                ));
        $entity->expects($this->once())->method('__get')->will($this->returnValue("pc:123"));
        $this->adapter->expects($this->once())->method('search')->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }

    public function testInvokeBusinessEntity(){
        $plugin = new Icm_Entity_Plugin_WebResults();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Business');
        $entity->expects($this->exactly(3))
                ->method('isA')
                ->will( $this->returnCallback(function($val){
                    if($val == 'Icm_Enitity_Person'){return false;}
                    else if($val == 'Icm_Entity_Crime'){return false;}
                    else if($val == 'Icm_Entity_Business'){return true;}
                    }
                ));
        $entity->expects($this->once())->method("__call")->will($this->returnValue("InstantCheckamte.com"));
        $this->adapter->expects($this->once())->method('search')->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }

    public function testInvokeCriminalEntity(){
        $plugin = new Icm_Entity_Plugin_WebResults();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Criminal');
        $entity->expects($this->exactly(4))
                ->method('isA')
                ->will( $this->returnCallback(function($val){
                    if($val == 'Icm_Enitity_Person'){return false;}
                    else if($val == 'Icm_Entity_Crime'){return false;}
                    else if($val == 'Icm_Entity_Business'){return false;}
                    else if($val == 'Icm_Entity_Criminal'){return true;}
                    }
                ));
        $entity->expects($this->exactly(2))->method("__get")->will($this->returnValue("Ruby"));
        $this->adapter->expects($this->once())->method('search')->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }

    public function testInvokeLocationEntity(){
        $plugin = new Icm_Entity_Plugin_WebResults();
        $plugin->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Location');
        $entity->expects($this->exactly(5))
                ->method('isA')
                ->will( $this->returnCallback(function($val){
                    if($val == 'Icm_Enitity_Person'){return false;}
                    else if($val == 'Icm_Entity_Crime'){return false;}
                    else if($val == 'Icm_Entity_Business'){return false;}
                    else if($val == 'Icm_Entity_Criminal'){return false;}
                    else if($val == 'Icm_Entity_Location'){return true;}
                    }
                ));
        $entity->expects($this->once())->method("get")->will($this->returnValue("92104"));
        $this->adapter->expects($this->once())->method('search')->will($this->returnValue(new Icm_Collection(array())));

        $this->assertInstanceOf('Icm_Collection', $plugin($entity));
    }
}
