<?php

class Icm_Entity_Plugin_Death_Test extends PHPUnit_Framework_TestCase
{
    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Npd_Api');
    }

    public function testInvokePersonEntity(){
        $plug = new Icm_Entity_Plugin_Death();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person', array('isA'));
        $entity->expects($this->once())->method('isA')->will($this->returnValue(true));
        $this->adapter->expects($this->once())->method('deathSearch')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug($entity)));
    }
}