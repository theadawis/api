<?php

class Icm_Entity_Plugin_Birth_Test extends PHPUnit_Framework_TestCase
{
    protected $adapter;

    public function setUp(){
        $this->adapter = $this->getMock('Icm_Service_Npd_Api');
    }

    public function testInvokePersonEntity(){
        $plug = new Icm_Entity_Plugin_Birth();
        $plug->setAdapter($this->adapter);
        $entity = $this->getMock('Icm_Entity_Person', array('isA'));
        $entity->expects($this->exactly(1))->method('isA')->will($this->returnCallback(function($val){
            if($val == 'Icm_Entity_Person'){
                return true;
            }
        }));
        $this->adapter->expects($this->once())->method('birthSearch')->will($this->returnValue(array()));
        $this->assertTrue(is_array($plug($entity)));
    }
}