<?php

/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/24/12
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Person_Test extends PHPUnit_Framework_TestCase
{

    /**
     * @param $person
     * @dataProvider persons
     */
    public function testCreate($person) {
        defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/tests/config/');
        $api = Icm_Api::bootstrap(Icm_Config::fromIni('/sites/api/tests/config/config.ini'));

        try {
            $person = Icm_Entity_Person::create($person);
            $this->assertNotNull($person->get('first_name'));
            return;
        } catch(Icm_Entity_Exception $e) {
            return;
        }

        $this->fail('Could not create Person, and no exception was thrown');
    }

    public function persons() {
        return array(
            array(array(
                'first_name' => 'Bob',
                'last_name' => 'Dole'
            )),
            array(array(
                'firstname' => 'Bob',
                'last_name' => 'Dole'
            )),
            array(array(
                'first_name' => 'Bob',
                'lastname' => 'Dole'
            )),
            array(array(
                'first_name' => 'Chris',
                'last_name' => 'Guiney'
            )),
            array(array(
                'first_name' => 'Tom',
                'last_name' => 'W'
            )),
            array(array(
                'first_name' => 'Nathan',
                'firstname' => 'BobDole',
                'last_name' => 'Cobb'
            )),

        );
    }
}
