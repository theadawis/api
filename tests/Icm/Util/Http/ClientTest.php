<?php

class Icm_Util_Http_Client_Test extends PHPUnit_Framework_TestCase
{
    public function testBuildQueryString() {
        $baseUrl = 'http://instantcheckmate.com/';
        $params = array(
            'foo' => 'bar',
            'bazz' => 'zanzibar'
        );

        $this->assertTrue(
            Icm_Util_Http_Client::buildUrlString($baseUrl, $params) == 'http://instantcheckmate.com/?foo=bar&bazz=zanzibar'
        );
    }

    /**
     * @expectedException Icm_Util_Http_Exception
     */
    public function testNonResponsiveDomain(){
        $domain = 'http://somewrongdomain/';
        $curl = new Icm_Util_Http_Client();
        $curl->setUrl($domain);
        $curl->execute();
    }
}
