<?php

class Icm_Util_RatelimiterTest extends PHPUnit_Framework_TestCase {

    public function testCheckLimit() {
        // Set Up
        $ip = '11.11.11.11';
        $key = Icm_Util_Ratelimiter::LIMITER_PREFIX . $ip;
        $redis = new Icm_Redis(Icm_Config::fromIni('../config/development/defaults.ini', 'redis'));
        $cgdb = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni('../config/development/defaults.ini', 'cgDb'));
        $ratelimiter = new Icm_Util_Ratelimiter($redis, $cgdb);
        list($timeLimit, $viewsLimit) = $ratelimiter->getLimits();

        if (!$timeLimit) {
            // set the time limit to the minimum
            $timeLimit = 1;
        }

        if (!$viewsLimit) {
            // set the views limit to something unreasonably high
            $viewsLimit = $timeLimit * 100;
        }
        $ratelimiter->setLimits($timeLimit, $viewsLimit);

        // remove any preexisting data about this ip
        $redis->del($key);
        $ratelimiter->removeFromWhitelist($ip);

        // TEST the whitelist does not limit
        $ratelimiter->addToWhitelist($ip);
        $this->assertFalse($ratelimiter->checkLimit($ip));
        $this->assertNull($redis->get($key));
        $ratelimiter->removeFromWhitelist($ip);

        // TEST checklimit increments
        $this->assertFalse($ratelimiter->checklimit($ip));
        $this->assertEquals(1, $redis->get($key));
        $this->assertFalse($ratelimiter->checklimit($ip));
        $this->assertEquals(2, $redis->get($key));
        $redis->del($key);

        // TEST checklimit limits after the limit is reached
        $redis->setex($key, $timeLimit, $viewsLimit);
        $this->assertTrue($ratelimiter->checklimit($ip));
        $redis->del($key);

        // TEST checklimit resets ips with ttl = -1
        $redis->incr($key);
        $this->assertEquals(1, $redis->get($key));
        $this->assertEquals(-1, $redis->ttl($key));
        $this->assertFalse($ratelimiter->checklimit($ip));
        $this->assertEquals(1, $redis->get($key));
        $this->assertEquals($timeLimit, $redis->ttl($key));
        $redis->del($key);
    }
}