<?php

class Icm_Util_Logger_Syslog_Test extends PHPUnit_Framework_TestCase
{
    public function testGetInstance() {
        $logger = Icm_Util_Logger_Syslog::getInstance('api');
        $this->assertInstanceOf('Icm_Util_Logger_Syslog', $logger);
        return $logger;
    }

    /**
     * @expectedException Icm_Util_Exception
     */
    public function testInvalidApp() {
        $logger = Icm_Util_Logger_Syslog::getInstance('foo');
    }

    /**
     * @depends testGetInstance
     */
    public function testLogEvent($logger) {
        $args = array('test' => 'this is a test event');
        $this->assertTrue($logger->logEvent($args));

        $this->setExpectedException('Icm_Util_Exception');
        $logger->logEvent('some random string');
    }

    /**
     * @depends testGetInstance
     */
    public function testLogInfo($logger) {
        Icm_Util_Logger_Syslog::setLoggingLevel(LOG_INFO);
        $this->assertTrue($logger->logInfo('Testing logInfo'));
    }

    /**
     * @depends testGetInstance
     */
    public function testLogErr($logger) {
        Icm_Util_Logger_Syslog::setLoggingLevel(LOG_ERR);
        $this->assertTrue($logger->logErr('Testing logErr'));
    }

    /**
     * @depends testGetInstance
     */
    public function testLogDebug($logger) {
        Icm_Util_Logger_Syslog::setLoggingLevel(LOG_DEBUG);
        $this->assertTrue($logger->logDebug('Testing logDebug'));
    }
}