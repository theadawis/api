<?php
defined('LIBRARY_PATH') || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../'));

set_include_path(
    implode(PATH_SEPARATOR, array(
        LIBRARY_PATH,
        LIBRARY_PATH . '/lib/',
        "/sites",
        "/sites/external-libs/Elastica/lib",
        get_include_path()
)));

require_once 'Zend/Loader/Autoloader.php';
require_once 'Predis/Autoloader.php';
Predis\Autoloader::register();
$autoLoader = Zend_Loader_Autoloader::getInstance();
$autoLoader->registerNamespace("Icm");
$autoLoader->registerNamespace("Elastica");

# set the default time zone to be PST
date_default_timezone_set('America/Los_Angeles');

# require once the TestCase class
require_once("TestCase.php");
