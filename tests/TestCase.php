<?php

class TestCase extends PHPUnit_Framework_TestCase
{
    protected $cleanupStmts = array();

    const TEST_CUSTOMER_ID = 1337;
    const TEST_ORDER_ID = 1337;
    const TEST_PRODUCT_ID = 1337;
    const TEST_ORDER_PRODUCT_ID = 1337;

    protected function setUp() {
        $this->cgDb = Icm_Db_Pdo::connect('cgDb', Icm_Config::fromIni(LIBRARY_PATH . '/tests/config/config.ini', 'cgDb'));

        parent::setUp();
    }

    protected function tearDown() {
        // run each of the delete stmts in order
        foreach ($this->cleanupStmts as $stmt) {
            $this->cgDb->execute($stmt);
        }

        parent::tearDown();
    }

    protected function generateSplitTestVariation(array $oData = array())
    {
        $data = array(
            'variationId'   => 632,
            'testId'        => 45
        );

        return new Icm_SplitTest_Variation(array_merge($data, $oData));
    }

    protected function generateCommerceCustomerGuid(array $cgData = array())
    {
        $data = array(
            'customer_guid_id' => 1,
            'customer_guid' => '6011EDF547601FE485257AE900038406',
            'order_product_id' => 1,
            'created'               => date('Y-m-d H:i:s'),
            'updated'               => date('Y-m-d H:i:s')
        );

        return Icm_Commerce_Customer_Guid::fromArray(array_merge($data, $cgData));
    }

    protected function generateCommerceCustomer(array $oData = array())
    {
        $data = array(
            'customer_id'           => 9001,
            'email'                 => 'bob@loblaw.com',
            'first_name'            => 'Bob',
            'last_name'             => 'Loblaw',
            'zip'                   => 92109,
            'credits'               => 0,
            'phone'                 => '8581234567',
            'address1'              => '1234 Street Lane',
            'address2'              => 'Apt 123',
            'city'                  => 'San Diego',
            'state'                 => 'CA',
            'password'              => 'tester',
            'guid'                  => '6011EDF547601FE485257AE900038406',
            'preferred_gateway'     => 1,
            'created'               => date('Y-m-d H:i:s'),
            'updated'               => date('Y-m-d H:i:s'),
            'forgot_password_hash'  => '12341234123412341234123412341234',
            'forgot_password_expires'   => strtotime("+24 hours")
        );

        return Icm_Commerce_Customer::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceCreditCard(array $oData = array())
    {
        $data = array(
            'number'            => '4116480559370132',
            'cvv'               => '123',
            'expiration_month'  => '08',
            'expiration_year'   => '2020',
            'first_name'        => 'John',
            'last_name'         => 'Smith',
        );

        return Icm_Commerce_Creditcard::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceOrder(array $oData = array())
    {
        $data = array(
            'order_id'      => 2949,
            'app_id'        => 1,
            'customer_id'   => 123,
            'visitor_id'    => 1234,
            'visit_id'      => 1,
            'is_test'       => 1,
        );

        return Icm_Commerce_Order::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceProduct(array $oData = array())
    {
        $data = array(
            'product_id'            => 1,
            'name'                  => 'Test Widget 1',
            'sku'                   => 'testwidget1',
            'billing_type'          => 'straight_sale',
            'initial_price'         => 4.99,
            'product_type'          => 'CR',
            'credits'               => 0,
            'credits_type_id'       => 0,
            'orange_cust_token'     => '8ZMMRV',
            'status'                => 'active',
            'orange_billing_plan_id'    => 'someorangebillingplanid',
            'is_upsell'             => 0,
            'orange_program_id'     => 'someprogramid',
            'orange_cust_token'     => 'someorangecusttoken',
            'fallback_product_id'   => 0,
            'product_tier'          => 'Standard',
            'recurring_price'       => 0.00,
            'trial_days'            => 0,
            'membership_months'     => 3
        );

        return Icm_Commerce_Product::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceOrderProduct(array $oData = array())
    {
        $data = array(
            'order_product_id'  => 1,
            'order_id'          => 1,
            'product_id'        => 1,
            'customer_id'       => 1,
            'refund_id'         => null,
            'status'            => 'active',
            'created'           => date('Y-m-d H:i:s'),
            'updated'           => date('Y-m-d H:i:s')
        );
        return Icm_Commerce_Order_Product::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceTransaction(array $oData = array())
    {
        $data = array(
            'transaction_id'    => 123,
            'parent_id'         => 12134,
            'order_id'          => 2949,
            'type'              => 1,
            'gateway_txid'      => 1775463634,
            'amount'            => 4.99,
            'failure'           => 0
        );

        return Icm_Commerce_Transaction::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceScore(array $oData = array())
    {
        $data = array(
            'ris_score_id' => 123,
            'transaction_id' => 12134,
            'ris_score' => 55
        );

        return Icm_Commerce_Score::fromArray(array_merge($data, $oData));
    }

    protected function generateCommerceProductGateway(array $oData = array())
    {
        $data = array(
            'gateway_id'        => 3,
            'product_id'        => 46,
            'token'             => '8ZMMSW',
            'bank'              => "SELECT"
        );

        return Icm_Commerce_Product_Gateway::fromArray(array_merge($data, $oData));
    }

    protected function generateBankRoute(array $oData = array())
    {
        $data = array(
            'bank_route'        => 'DDE1DC2E9D4AAAAA852578A20071D41B',
            'name'              => 'select'
        );

        return Icm_Commerce_BankRoute::fromArray(array_merge($data, $oData));
    }

    protected function generateVisit(array $oData = array())
    {
        $data = array(
            'ip_address'        => '192.168.3.142',
            'sub_id'            => 'orange_test'
        );

        return Icm_Struct_Tracking_Visit::fromArray(array_merge($data, $oData));
    }

    protected function generateAffiliate(array $oData = array())
    {
        $data = array(
            'short_name'        => 'test_affiliate'
        );

        return Icm_Struct_Marketing_Affiliate::fromArray(array_merge($data, $oData));
    }

    protected function generateEmailStat(array $oData = array()) {
        $data = array(
            'email_stat_id'         => 141,
            'email_stat_created'    => date('Y-m-d'),
            'email_stat_rule'       => 101,
            'email_stat_count'      => 10,
            'email_stat_updated'    => date('Y-m-d H:i:s')
        );

        return Icm_Email_Stats::fromArray(array_merge($data, $oData));
    }

    protected function writeTestCustomer($email = 'tester@test.com', $first_name = 'John') {
        $customerId = self::TEST_CUSTOMER_ID;

        $params = array(
            $customerId,
            1,
            'forgotpass',
            $email,
            $first_name,
        );

        $this->cgDb->execute("INSERT INTO customer (customer_id, forgot_password_expires, forgot_password_hash, email, first_name) VALUES (?, ?, ?, ?, ?)", $params);
        array_unshift($this->cleanupStmts, "DELETE FROM customer WHERE customer_id = " . $customerId);

        return $customerId;
    }

    protected function deleteTestCustomer() {
        $this->cgDb->execute("DELETE FROM customer WHERE customer_id = " . self::TEST_CUSTOMER_ID);
    }

    protected function writeTestOrder($customerId) {
        $orderId = self::TEST_ORDER_ID;
        $appId = 1;

        $params = array(
            $orderId,
            $appId,
            $customerId,
            'testvisitorid',
            'testvisitid',
        );

        $this->cgDb->execute("INSERT INTO orders (order_id, app_id, customer_id, visitor_id, visit_id) VALUES (?, ?, ?, ?, ?)", $params);
        array_unshift($this->cleanupStmts, "DELETE FROM orders WHERE order_id = " . $orderId);

        return $orderId;
    }

    protected function deleteTestOrder() {
        $this->cgDb->execute("DELETE FROM orders WHERE order_id = " . self::TEST_ORDER_ID);
    }

    protected function writeTestProduct() {
        $productId = self::TEST_PRODUCT_ID;

        $params = array(
            $productId,
            'testingproduct',
            'testproducttype',
            1,
            1,
        );

        $this->cgDb->execute("INSERT INTO products (product_id, name, product_type, credits_type_id, status) VALUES (?, ?, ?, ?, ?)", $params);
        array_unshift($this->cleanupStmts, "DELETE FROM products WHERE product_id = " . $productId);

        return $productId;
    }

    protected function deleteTestProduct() {
        $this->cgDb->execute("DELETE FROM products WHERE product_id = " . self::TEST_PRODUCT_ID);
    }

    protected function writeTestOrderProduct($productId, $customerId, $orderId) {
        $orderProductId = self::TEST_ORDER_PRODUCT_ID;

        $params = array(
            $orderProductId,
            $productId,
            $customerId,
            $orderId,
        );

        $this->cgDb->execute("INSERT INTO order_products (order_product_id, product_id, customer_id, order_id) VALUES (?, ?, ?, ?)", $params);
        array_unshift($this->cleanupStmts, "DELETE FROM order_products WHERE order_product_id = " . $orderProductId);
        array_unshift($this->cleanupStmts, "DELETE FROM customer_guids WHERE order_product_id = " . $orderProductId);

        return $orderProductId;
    }

    protected function deleteTestOrderProduct() {
        $this->cgDb->execute("DELETE FROM customer_guids WHERE order_product_id = " . self::TEST_ORDER_PRODUCT_ID);
        $this->cgDb->execute("DELETE FROM order_products WHERE order_product_id = " . self::TEST_ORDER_PRODUCT_ID);
    }
}
