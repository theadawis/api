<?php
/**
 * wicked_ghetto_hey_product_token_mapper.php
 * User: chris
 * Date: 1/4/13
 * Time: 11:23 AM
 */

date_default_timezone_set('America/Los_Angeles');
defined('LIBRARY_PATH') || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../'));
defined('ENVIRONMENT') || define('ENVIRONMENT', trim(file_get_contents('/etc/tcg/environment')));
defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/config/'.ENVIRONMENT);
/*
set_include_path(
implode(PATH_SEPARATOR, array(
	LIBRARY_PATH,
	LIBRARY_PATH . '/lib/',
	"/sites",
	"/sites/Elastica/lib",
	get_include_path()
)));*/

require_once LIBRARY_PATH.'/Icm/Api.php';
require_once LIBRARY_PATH.'/Icm/Config.php';
/**
 * @var Icm_Api $api
 */
$api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/defaults.ini'));

$products = array();
$order_data = array();

$order_data['product_id']                	= '4';
$order_data['product_name']              	= '1-Month_Membership_24.86';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '24.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= 'Monthly Billing';
$order_data['product_description']       	= '1-Month_Membership_24.86';
$order_data['product_user_description']  	= 'ICM_1Month_Cust_24.86';
$order_data['product_alt_price']         	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '24.86';
$order_data['descriptor']                	= 'monthly plan';
$order_data['descriptor_phone']          	= '8664905980';
$order_data['api_cust_token']            	= '8DF2K2'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8DF2LV';
$order_data['api_tran_token']['select']   = '8SN7FR';
$order_data['api_tran_token']['litle_icm']= '8DF2LV';

$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '24.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


$order_data['product_id']                	= '5';
$order_data['product_name']              	= '3-Month_Cust_59.58';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '59.58';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '3 month membership';
$order_data['product_description']       	= 'ICM_3Month_Cust_59.58';
$order_data['product_user_description']  	= '3-Month Membership';
$order_data['product_alt_price']         	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '59.58';
$order_data['descriptor']                	= '3month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8F6QSU'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8F6QWR';
$order_data['api_tran_token']['select']   = '8SN7GZ';
$order_data['api_tran_token']['litle_icm']= '8F6QWR';

$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '3';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '10';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '59.58'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '3'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


$order_data['product_id']                	= '6';
$order_data['product_name']              	= '6-Month_Membership_89.16';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '14.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '6 month membership - 89.16';
$order_data['product_description']       	= '6-Month_Membership_89.16';
$order_data['product_user_description']  	= '6-Month Membership';
$order_data['product_alt_price']         	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '89.16';
$order_data['descriptor']                	= '6month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8F6QRT'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8F6QSA';
$order_data['api_tran_token']['select']   = '8SN7HC';
$order_data['api_tran_token']['litle_icm']= '8F6QSA';

$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '6';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '12';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '89.16'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '6'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


$order_data['product_id']                	= '8';
$order_data['product_name']              	= '6-Month_Cust_59.16';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '9.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '6 Month Membership - 59.16';
$order_data['product_description']       	= '6-Month Membership_59.16';
$order_data['product_user_description']  	= '6-Month Membership';
$order_data['product_alt_price']         	= '24.86'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '59.16';
$order_data['descriptor']                	= '6month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8FV5RP'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8FV5WK';
$order_data['api_tran_token']['select']   = '8RQU28';
$order_data['api_tran_token']['litle_icm']= '8RQSR8';
$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '6';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '11';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '59.16'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '6'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


$order_data['product_id']                	= '9';
$order_data['product_name']              	= '3-Month_Cust_44.58';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '14.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '3 Month Membership - 44.58';
$order_data['product_description']       	= '3-Month Membership_44.58';
$order_data['product_user_description']  	= '3-Month Membership';
$order_data['product_alt_price']         	= '29.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '44.58';
$order_data['descriptor']                	= '6month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8FV5QH'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8FV5UX';
$order_data['api_tran_token']['select']   = '8RQTLN';
$order_data['api_tran_token']['litle_icm']= '8RQSAS';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']           		= '3';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '12';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '44.58'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '3'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


$order_data['product_id']               	= '10';
$order_data['product_name']             	= '1-Month_Cust_19.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '19.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 19.86';
$order_data['product_description']      	= '1-Month Membership_19.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '19.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8FV5PF'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8FV5TR';
$order_data['api_tran_token']['select']   = '8RQU6M';
$order_data['api_tran_token']['litle_icm']= '8RQSZ3';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '19.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Fallback for 6-month

$order_data['product_id']               	= '11';
$order_data['product_name']             	= '1-Month_Trans_9.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '9.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 9.86';
$order_data['product_description']      	= '1-Month Membership_9.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '9.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8HV2VX'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8HV2TU';
$order_data['api_tran_token']['select']   = '8RQUAD';
$order_data['api_tran_token']['litle_icm']= '8RQT42';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '9.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Fallback for 3-month

$order_data['product_id']               	= '12';
$order_data['product_name']             	= '1-Month_Cust_14.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '14.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 14.86';
$order_data['product_description']      	= 'ICM_Monthly_Trans_14.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '44.58'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '14.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8FV5NE'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8FV5SG';
$order_data['api_tran_token']['select']   = '8RQU4F';
$order_data['api_tran_token']['litle_icm']= '8RQSSA';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '14.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 29.63

$order_data['product_id']               	= '13';
$order_data['product_name']             	= '5-day_Trial_Cust_29.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 29.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '29.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8LQRSS'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8LQSAF';
$order_data['api_tran_token']['select']   = '8RQTVP';
$order_data['api_tran_token']['litle_icm']= '8RQSMB';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '29.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// DNC Upsell

$order_data['product_id']               	= '14';
$order_data['product_name']             	= 'DoNotCall_1.99';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.99';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'DNC_Upsell';
$order_data['product_description']      	= 'ICM_DoNotCall_1.99';
$order_data['product_user_description'] 	= 'DNC Registration';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.99';
$order_data['descriptor']               	= 'DoNotCall list';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8MGTHG';

$order_data['api_tran_token']['original'] = '8MH2DX';
$order_data['api_tran_token']['select']   = '8RQUC2';
$order_data['api_tran_token']['litle_icm']= '8RQT7D';
$order_data['report_group']				= 'CR_UP';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'upsell';
$order_data['loc_indicator']				= '-1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = ''; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = ''; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 34.63

$order_data['product_id']               	= '15';
$order_data['product_name']             	= '5-day_Trial_Cust_34.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 34.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '34.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8M6C7C'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8M6C55';
$order_data['api_tran_token']['select']   = '8RQTX2';
$order_data['api_tran_token']['litle_icm']= '8RQSNT';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '34.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial

$order_data['product_id']               	= '16';
$order_data['product_name']             	= '5-day_Trial_Cust_39.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 39.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8MBMAD'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8MBMF6';
$order_data['api_tran_token']['select']   = '8RQTYB';
$order_data['api_tran_token']['litle_icm']= '8RQSQE';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '39.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial

$order_data['product_id']               	= '17';
$order_data['product_name']             	= '5-day_Trial_Cust_39.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '0.99';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 39.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_0.99';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '0.99';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8MBMAD'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8MBMF6';
$order_data['api_tran_token']['select']   = '8RQTYB';
$order_data['api_tran_token']['litle_icm']= '8RQSQE';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '0.99'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '39.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();



$order_data['product_id']               	= '19';
$order_data['product_name']             	= '5-day_Trial_Cust_19.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 19.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8PPL3N'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8PPLAG';
$order_data['api_tran_token']['select']   = '8RQTNB';
$order_data['api_tran_token']['litle_icm']= '8RQSHN';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '19.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


// 1-Month Free Trial 24.63

$order_data['product_id']               	= '20';
$order_data['product_name']             	= '5-day_Trial_Cust_24.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 24.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8PPL7A'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['original'] = '8PPLF6';
$order_data['api_tran_token']['select']   = '8RQTQ3';
$order_data['api_tran_token']['litle_icm']= '8RQSKF';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '24.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


/*
        -- the following are duplicated in process_order! --

      */

// Single Order (add credits) default 1 credit

$order_data['product_id']                  = '21';
$order_data['product_name']                = 'Non_Recurring_29.95_Cust';
$order_data['cust_category']               = ''; // Used by Orange for Customer Category
$order_data['order_date']                  = date('Ymd');
$order_data['order_amount']                = '29.95';
$order_data['product_group']               = 'criminal_records';
$order_data['product_sku']                 = 'Single Order 1 - 29.95';
$order_data['product_description']         = 'Non_Recurring_29.95_Cust';
$order_data['product_user_description']    = 'Single Report';
$order_data['product_charge_now']          = '29.95';
$order_data['descriptor']                  = '';
$order_data['descriptor_phone']            = '866-490-5980';
$order_data['api_cust_token']              = '8S27SS';
$order_data['api_tran_token']['select']    = '8S27VT';
$order_data['api_tran_token']['litle_icm'] = '8S27V3';
$order_data['report_group']                = 'CR_SO';
$order_data['plan_months']                 = '0';
$order_data['product_type']                = 'single_order';
$order_data['credits']                     = 1; //how many credits you get when you buy this product
$order_data['loc_indicator']               = '-1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = ''; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = ''; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Single Order (add credits) default 1 credit

$order_data['product_id']                  = '22';
$order_data['product_name']                = 'ICM_Non_Recurring_19.95_Cust';
$order_data['cust_category']               = ''; // Used by Orange for Customer Category
$order_data['order_date']                  = date('Ymd');
$order_data['order_amount']                = '19.95';
$order_data['product_group']               = 'criminal_records';
$order_data['product_sku']                 = 'Single Order - Coupon 1 - 19.95';
$order_data['product_description']         = 'ICM_Non_Recurring_19.95_Cust';
$order_data['product_user_description']    = 'Single Report';
$order_data['product_charge_now']          = '19.95';
$order_data['descriptor']                  = '';
$order_data['descriptor_phone']            = '866-490-5980';
$order_data['api_cust_token']              = '8S27SS';
$order_data['api_tran_token']['select']    = '8S27VT';
$order_data['api_tran_token']['litle_icm'] = '8S27V3';
$order_data['report_group']                = 'CR_SO';
$order_data['plan_months']                 = '0';
$order_data['product_type']                = 'single_order';
$order_data['credits']                     = 1;
$order_data['loc_indicator']               = '-1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = ''; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = ''; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial trial 1.00 -> 27.63

$order_data['product_id']               	= '23';
$order_data['product_name']             	= '5-day_Trial_Cust_27.63';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.00';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.00';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.00';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.00'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 0.95 -> 27.63

$order_data['product_id']               	= '24';
$order_data['product_name']             	= '5-day_Trial_Cust_0.95';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '0.95';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 0.95 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_0.95';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '0.95';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '0.95'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 1.95 -> 27.63

$order_data['product_id']               	= '25';
$order_data['product_name']             	= '5-day_Trial_Cust_1.95';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.95';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 1.95 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_1.95';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.95';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '1.95'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 2.95 -> 27.63

$order_data['product_id']               	= '26';
$order_data['product_name']             	= '5-day_Trial_Cust_2.95';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '2.95';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 2.95 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_2.95';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '2.95';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '2.95'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


// 1-Month Free Trial 4.95 -> 27.63

$order_data['product_id']               	= '27';
$order_data['product_name']             	= '5-day_Trial_Cust_4.95';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '4.95';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 4.95 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_4.95';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '4.95';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '4.95'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 9.95 -> 27.63

$order_data['product_id']               	= '28';
$order_data['product_name']             	= '5-day_Trial_Cust_9.95';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '9.95';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 9.95 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_9.95';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '9.95';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '9.95'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// 1-Month Free Trial 0.99 -> 27.63

$order_data['product_id']               	= '29';
$order_data['product_name']             	= '5-day_Trial_Cust_0.99';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '0.99';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= '5 day trial - 0.99 -> 27.63';
$order_data['product_description']      	= 'ICM_Trial_Trans_0.99';
$order_data['product_user_description'] 	= '1-Month Trial Membership';
$order_data['product_alt_price']        	= '39.63'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '0.99';
$order_data['descriptor']               	= '1month trial plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8XRJMR';

$order_data['api_tran_token']['select']   = '8XRK3Y';
$order_data['api_tran_token']['litle_icm']= '8XRK24';
$order_data['report_group']				= 'CR_TR';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'trial';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = '0.99'; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '27.63'; // $Amount charged each membership period
$order_data['trial_period']               = '5'; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


// -----------------------------
// -----------------------------
// New Pricing Models (Oct 2012)

// -----Plan A-----
// Plan A - 1 Year

$order_data['product_id']                	= '30';
$order_data['product_name']              	= '1-Year_Cust_58.32';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '4.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '1 Year Membership - 58.32';
$order_data['product_description']       	= '1 Year Membership_58.32';
$order_data['product_user_description']  	= '1 Year Membership';
$order_data['product_alt_price']         	= '24.86'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '58.32';
$order_data['descriptor']                	= '1year plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8ZCGVD'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCH4K';
$order_data['api_tran_token']['litle_icm']= '8ZCH2Y';
$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '6';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '39';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '58.32'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '12'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Plan A - 3 Months

$order_data['product_id']                	= '31';
$order_data['product_name']              	= '3-Month_Cust_29.58';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '9.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '3 Month Membership - 29.58';
$order_data['product_description']       	= '3-Month Membership_29.58';
$order_data['product_user_description']  	= '3-Month Membership';
$order_data['product_alt_price']         	= '29.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '29.58';
$order_data['descriptor']                	= '3month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8FV5PZ'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCGNU';
$order_data['api_tran_token']['litle_icm']= '8ZCGH2';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']           		= '3';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '11';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '29.58'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '3'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Plan A - 1 Month

$order_data['product_id']               	= '32';
$order_data['product_name']             	= '1-Month_Cust_14.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '14.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 14.86';
$order_data['product_description']      	= '1-Month Membership_14.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '14.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8FV5NE'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8RQU4F';
$order_data['api_tran_token']['litle_icm']= '8RQSSA';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '14.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();
// -----Plan A-----


// -----Plan B-----
// Plan B - 6 Months
// See product ID no. 8

// Plan B - 3 Months

$order_data['product_id']                	= '33';
$order_data['product_name']              	= '3-Month_Cust_41.58';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '13.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '3 Month Membership - 41.58';
$order_data['product_description']       	= '3-Month Membership_41.58';
$order_data['product_user_description']  	= '3-Month Membership';
$order_data['product_alt_price']         	= '29.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '41.58';
$order_data['descriptor']                	= '3month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8ZCHDY'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCHJC';
$order_data['api_tran_token']['litle_icm']= '8ZCHJ6';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']           		= '3';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '40';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '41.58'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '3'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Plan B - 1 Month

$order_data['product_id']               	= '34';
$order_data['product_name']             	= '1-Month_Cust_17.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '17.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 17.86';
$order_data['product_description']      	= '1-Month Membership_17.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '17.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZCHCJ'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCHGU';
$order_data['api_tran_token']['litle_icm']= '8ZCHFN';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '17.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();
// -----Plan B-----


// -----Plan C-----
// Plan C - 6 Months

$order_data['product_id']                	= '35';
$order_data['product_name']              	= '6-Month_Cust_47.16';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '7.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '6 Month Membership - 47.16';
$order_data['product_description']       	= '6 Month Membership_47.16';
$order_data['product_user_description']  	= '6 Month Membership';
$order_data['product_alt_price']         	= '24.86'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '47.16';
$order_data['descriptor']                	= '6month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8FV5R5'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCHUR';
$order_data['api_tran_token']['litle_icm']= '8ZCHT2';
$order_data['report_group']				= 'CR_SS';
$order_data['plan_months']           		= '6';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '41';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '47.16'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '6'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// Plan C - 3 Months

$order_data['product_id']                	= '36';
$order_data['product_name']              	= '3-Month_Cust_26.58';
$order_data['cust_category']             	= ''; // Used by Orange for Customer Category
$order_data['order_date']                	= date('Ymd');
$order_data['order_amount']              	= '8.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']               	= '3 Month Membership - 26.58';
$order_data['product_description']       	= '3-Month Membership_26.58';
$order_data['product_user_description']  	= '3-Month Membership';
$order_data['product_alt_price']         	= '29.95'; // was $XX.XX/mo
$order_data['product_charge_now']        	= '26.58';
$order_data['descriptor']                	= '3month plan';
$order_data['descriptor_phone']          	= '866-490-5980';
$order_data['api_cust_token']            	= '8ZCHML'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCHPX';
$order_data['api_tran_token']['litle_icm']= '8ZCHP6';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']           		= '3';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction
$order_data['fallback_product_id']		= '42';

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '26.58'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '3'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();


// Plan C - 1 Month

$order_data['product_id']               	= '37';
$order_data['product_name']             	= '1-Month_Cust_9.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '9.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 9.86';
$order_data['product_description']      	= '1-Month Membership_9.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '9.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8HV2VX'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8RQUAD';
$order_data['api_tran_token']['litle_icm']= '8RQT42';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '9.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();
// -----Plan C-----


// -----Plan D-----
// Plan D - 6 Month
// See Product ID No 8

// Plan D - 3 Month
// See Product ID No 9

// Plan D - 1 Month
// See Product ID No 10

// Plan D - Lifetime

$order_data['product_id']               	= '38';
$order_data['product_name']             	= 'Lifetime_Cust_199.99';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '199.99';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'One-Time Billing - 199.99';
$order_data['product_description']      	= 'Lifetime Membership_199.99';
$order_data['product_user_description'] 	= 'Lifetime Membership';
$order_data['product_alt_price']        	= '299.99'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '199.99';
$order_data['descriptor']               	= 'lifetime plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZCTGD'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZCTU4';
$order_data['api_tran_token']['litle_icm']= '8ZCTMB';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '9999';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '199.99'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '9999'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();
// -----Plan D-----


// -----Fallbacks-----
// $4.86 - Fallback

$order_data['product_id']               	= '39';
$order_data['product_name']             	= '1-Month_Trans_4.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '4.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 4.86';
$order_data['product_description']      	= '1-Month Membership_4.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '4.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZDH8N'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZDHGM';
$order_data['api_tran_token']['litle_icm']= '8ZDHD8';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '4.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// $13.86 Fallback

$order_data['product_id']               	= '40';
$order_data['product_name']             	= '1-Month_Trans_13.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '13.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 13.86';
$order_data['product_description']      	= '1-Month Membership_13.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '13.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZDHBD'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZDHN4';
$order_data['api_tran_token']['litle_icm']= '8ZDHKF';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '13.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// $7.86 Fallback

$order_data['product_id']               	= '41';
$order_data['product_name']             	= '1-Month_Trans_7.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '7.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 7.86';
$order_data['product_description']      	= '1-Month Membership_7.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '7.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZDKYM'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZDL89';
$order_data['api_tran_token']['litle_icm']= '8ZDL63';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '7.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// $8.86 Fallback

$order_data['product_id']               	= '42';
$order_data['product_name']             	= '1-Month_Trans_8.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '8.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 8.86';
$order_data['product_description']      	= '1-Month Membership_8.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '8.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZDL2R'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZDLBK';
$order_data['api_tran_token']['litle_icm']= '8ZDL9R';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '8.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();
// -----Fallbacks-----

// $22.86 Monthly Membership

$order_data['product_id']               	= '43';
$order_data['product_name']             	= '1-Month_Cust_22.86';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '22.86';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'Monthly Billing - 22.86';
$order_data['product_description']      	= '1-Month Membership_22.86';
$order_data['product_user_description'] 	= '1-Month Membership';
$order_data['product_alt_price']        	= '34.95'; // was $XX.XX/mo
$order_data['product_charge_now']       	= '22.86';
$order_data['descriptor']               	= '1month plan';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '8ZMMRV'; // This token is how orange determines billing plans for rebills and is unique per billing plan/price point

$order_data['api_tran_token']['select']   = '8ZMMU4';
$order_data['api_tran_token']['litle_icm']= '8ZMMSW';
$order_data['report_group']				= 'CR_SS';

$order_data['plan_months']				= '1';
$order_data['product_type']           	= 'membership';
$order_data['loc_indicator']				= '1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = '22.86'; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = '1'; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// PDF Upsell

$order_data['product_id']               	= '44';
$order_data['product_name']             	= 'PDF_Upsell_1.99';
$order_data['cust_category']            	= ''; // Used by Orange for Customer Category
$order_data['order_date']               	= date('Ymd');
$order_data['order_amount']             	= '1.99';
$order_data['product_group'] 				= 'criminal_records';
$order_data['product_sku']              	= 'PDF_Upsell';
$order_data['product_description']      	= 'ICM_PDF_1.99';
$order_data['product_user_description'] 	= 'PDF Upgrade';
$order_data['product_alt_price']        	= ''; // was $XX.XX/mo
$order_data['product_charge_now']       	= '1.99';
$order_data['descriptor']               	= 'PDF Upsell';
$order_data['descriptor_phone']         	= '866-490-5980';
$order_data['api_cust_token']           	= '943FLK';

$order_data['api_tran_token']['original'] = '8MH2DX';
$order_data['api_tran_token']['select']   = '943GH4';
$order_data['api_tran_token']['litle_icm']= '943G6A';
$order_data['report_group']				= 'CR_UP';

$order_data['plan_months']				= '0';
$order_data['product_type']           	= 'upsell';
$order_data['loc_indicator']				= '-1'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

$order_data['trial_amount']               = ''; // $Amount charged for the Trial period
$order_data['recurring_amount']           = ''; // $Amount charged each membership period
$order_data['trial_period']               = ''; // In Days, the number of days the trial lasts for
$order_data['membership_period']          = ''; // In months, the Recurring Billing Cycle time period
$products[] = $order_data; $order_data = array();

// New Pricing Models (Oct 2012)
// -----------------------------
// -----------------------------

$config['product'][0]['name']                        = 'ICM_Trial_Cust_24.63';
$config['product'][0]['sku']                         = 'RP 5 day trial - 24.63';
$config['product'][0]['description']                 = 'RP 5 day trial - 24.63';
$config['product'][0]['continuity']                  = 1;
$config['product'][0]['group']                       = 'reverse_phone';
$config['product'][0]['amount']                      = '1.00';
$config['product'][0]['type']                        = 'trial';
$config['product'][0]['report_group']                = 'RP_TR';
$config['product'][0]['api_cust_token']              = '8SEA6C'; //have this
$config['product'][0]['api_tran_token']['select']    = '8SUG6X'; //have this
$config['product'][0]['api_tran_token']['litle_icm'] = '8SEA88'; //have this

// $23.40 Annual Membership ($1.95/mo)
$config['product'][1]['name']                        = 'RP_Annual_Cust_23.40';
$config['product'][1]['sku']                         = 'RP Annual Membership - 23.40';
$config['product'][1]['description']                 = 'RP Annual Membership - 23.40';
$config['product'][1]['continuity']                  = 1;
$config['product'][1]['group']                       = 'reverse_phone';
$config['product'][1]['amount']                      = '23.40';
$config['product'][1]['type']                        = 'membership';
$config['product'][1]['report_group']                = 'RP_TR';
$config['product'][1]['api_cust_token']              = '8ZMLC6'; //have this
$config['product'][1]['api_tran_token']['select']    = '8ZMLQC'; //have this
$config['product'][1]['api_tran_token']['litle_icm'] = '8ZMLN4'; //have this
$config['product'][1]['plan_months']                 = '12';
$config['product'][1]['loc_indicator']               = '0'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

// $35.40 Annual Membership ($2.95/mo)
$config['product'][2]['name']                        = 'RP_Annual_Cust_35.40';
$config['product'][2]['sku']                         = 'RP Annual Membership - 35.40';
$config['product'][2]['description']                 = 'RP Annual Membership - 35.40';
$config['product'][2]['continuity']                  = 1;
$config['product'][2]['group']                       = 'reverse_phone';
$config['product'][2]['amount']                      = '35.40';
$config['product'][2]['type']                        = 'membership';
$config['product'][2]['report_group']                = 'RP_TR';
$config['product'][2]['api_cust_token']              = '8ZMLER'; //have this
$config['product'][2]['api_tran_token']['select']    = '8ZMLW8'; //have this
$config['product'][2]['api_tran_token']['litle_icm'] = '8ZMLUY'; //have this
$config['product'][2]['plan_months']                 = '12';
$config['product'][2]['loc_indicator']               = '0'; // Field defined by Verifi; 0 = Initial, trial, or sign-up billing and -1 = One Time Transaction

// $47.40 Annual Membership ($3.95/mo)
$config['product'][3]['name']                        = 'RP_Annual_Cust_47.40';
$config['product'][3]['sku']                         = 'RP Annual Membership - 47.40';
$config['product'][3]['description']                 = 'RP Annual Membership - 47.40';
$config['product'][3]['continuity']                  = 1;
$config['product'][3]['group']                       = 'reverse_phone';
$config['product'][3]['amount']                      = '47.40';
$config['product'][3]['type']                        = 'membership';
$config['product'][3]['report_group']                = 'RP_TR';
$config['product'][3]['api_cust_token']              = '8ZMLK3'; //have this
$config['product'][3]['api_tran_token']['select']    = '8ZMM2Y'; //have this
$config['product'][3]['api_tran_token']['litle_icm'] = '8ZMLZ3'; //have this
$config['product'][3]['plan_months']                 = '12';
$config['product'][3]['loc_indicator']               = '0';


$tokens = array();
foreach($products as $product) {
    $tokens[$product['api_tran_token']['litle_icm']] = $product['api_cust_token'];
}

$dataContainer = new Icm_Container();
$dataContainer->loadXml(simplexml_load_file("services/dataContainer.xml"));
$conn = $dataContainer->get("cgDbConnection");

/**
 * @var Icm_Db_Pdo $conn
 */
foreach($tokens as $tran_token => $cust_token){
    $result = $conn->fetch("
        SELECT * FROM
        product_gateway
        WHERE token = :tran_token
        AND gateway_id = 4
    ", array(":tran_token" => $tran_token));
    $product_id = $result['product_id'];
    echo "UPDATE products SET orange_cust_token = '{$cust_token}' WHERE product_id = {$product_id};\n";

}
foreach($config['product'] as $product) {
    //$tokens[$product['api_tran_token']['litle_icm']] = $product['api_cust_token'];
    $tran_token = $product['api_tran_token']['litle_icm'];
    $cust_token = $product['api_cust_token'];
    $result = $conn->fetch("
        SELECT * FROM
        product_gateway
        WHERE token = :tran_token
        AND gateway_id = 4
    ", array(":tran_token" => $tran_token));
    $product_id = $result['product_id'];
    echo "UPDATE products SET orange_cust_token = '{$cust_token}' WHERE product_id = {$product_id};\n";
}