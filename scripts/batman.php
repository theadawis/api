#!/usr/bin/php
<?php
/*
       _,    _   _    ,_
  .o888P     Y8o8Y     Y888o.
 d88888      88888      88888b
d888888b_  _d88888b_  _d888888b
8888888888888888888888888888888
8888888888888888888888888888888
YJGS8P"Y888P"Y888P"Y888P"Y8888P
 Y888   '8'   Y8P   '8'   888Y
  '8o          V          o8'
    `                     `
*/
date_default_timezone_set('America/Los_Angeles');
defined('LIBRARY_PATH') || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../'));
defined('ENVIRONMENT') || define('ENVIRONMENT', trim(file_get_contents('/etc/tcg/environment')));
defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/config/'.ENVIRONMENT);
/*
set_include_path(
implode(PATH_SEPARATOR, array(
	LIBRARY_PATH,
	LIBRARY_PATH . '/lib/',
	"/sites",
	"/sites/Elastica/lib",
	get_include_path()
)));*/

require_once LIBRARY_PATH.'/Icm/Api.php';
require_once LIBRARY_PATH.'/Icm/Config.php';
$api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/defaults.ini'));
require_once 'Predis/Autoloader.php';
Predis\Autoloader::register();
/* require_once 'Zend/Loader/Autoloader.php';
$autoLoader = Zend_Loader_Autoloader::getInstance();
$autoLoader->registerNamespace("Icm");
$autoLoader->registerNamespace("Elastica"); */

new Icm_Daemon_Process_Redisqueue($api->getConfig());