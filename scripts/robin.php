#!/usr/bin/php
<?php
date_default_timezone_set('America/Los_Angeles');
defined('LIBRARY_PATH') || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../'));
defined('ENVIRONMENT') || define('ENVIRONMENT', trim(file_get_contents('/etc/tcg/environment')));
defined('CONFIG_DIR') || define('CONFIG_DIR', LIBRARY_PATH . '/config/'.ENVIRONMENT);
/*
set_include_path(
implode(PATH_SEPARATOR, array(
	LIBRARY_PATH,
	LIBRARY_PATH . '/lib/',
	"/sites",
	"/sites/Elastica/lib",
	get_include_path()
)));*/

require_once LIBRARY_PATH.'/Icm/Api.php';
require_once LIBRARY_PATH.'/Icm/Config.php';
$api = Icm_Api::bootstrap(Icm_Config::fromIni(CONFIG_DIR.'/defaults.ini'));
require_once 'Predis/Autoloader.php';
Predis\Autoloader::register();
/* require_once 'Zend/Loader/Autoloader.php';
$autoLoader = Zend_Loader_Autoloader::getInstance();
$autoLoader->registerNamespace("Icm");
$autoLoader->registerNamespace("Elastica"); */

//print_r($argv);
new Icm_Daemon_Process_Searchgenerator($api->getConfig());