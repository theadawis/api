<?php
/**
 * Data adapter for our ZIPCodes table
 * @author Joe Linn
 *
 */
class Icm_Data_Zipcodes_Db implements Icm_Configurable{
    protected $connection, $table, $tableName;
    protected $config;
    const tableName = 'ZIPCodes';

    public function __construct(Icm_Db_Interface $conn = NULL){
        $this->connection = $conn;
        $this->tableName = self::tableName;
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->connection = Icm_Db::getInstance($this->config->getOption('connection'));
    }

    /**
     * Get the population breakdown by ethnicity for the given zip code
     * @param int $zipcode
     * @return array
     */
    public function getEthnicityData($zipcode){
        $fields = array(
            'WhitePopulation',
            'BlackPopulation',
            'HispanicPopulation',
            'AsianPopulation',
            'HawaiianPopulation',
            'IndianPopulation',
            'OtherPopulation',
        );
        return $this->get($fields, $zipcode);
    }

    /**
     * Get the population breakdown by gender for the given zip code
     * @param int $zipcode
     * @return array
     */
    public function getGenderData($zipcode){
        $fields = array(
            'MalePopulation',
            'FemalePopulation'
        );
        return $this->get($fields, $zipcode);
    }

    /**
     * Get the average house value for the given zip code
     * @param int $zipcode
     * @return array
     */
    public function getAverageHouseValue($zipcode){
        return $this->get(array('AverageHouseValue'), $zipcode);
    }

    /**
     * Returns the average income per household for the given zip code
     * @param int $zipcode
     * @return array
     */
    public function getAverageIncome($zipcode){
        return $this->get(array('IncomePerHousehold'), $zipcode);
    }

    /**
     * Get the overall median age and the median age breakdown by gender for the given zip code
     * @param int $zipcode
     * @return array
     */
    public function getMedianAge($zipcode){
        $fields = array(
            'MedianAge',
            'MedianAgeMale',
            'MedianAgeFemale'
        );
        return $this->get($fields, $zipcode);
    }

    /**
     * @param int $zipcode
     */
    public function getCoordinates($zipcode){
        $fields = array(
            'Latitude',
            'Longitude',
            'Elevation'
        );
        return $this->get($fields, $zipcode);
    }

    function getAliasesByZips(array $zips, $cityName = false){
        $zips = implode(', ', $zips);
        $query = "SELECT ZipCode as zip, LOWER(CityAliasName) as neighborhood, LOWER(City) as city, LOWER(State) as state FROM `{$this->tableName}` WHERE ZipCode IN ({$zips})";
        if ($cityName){
            $query .= " AND CityAliasName != '{$cityName}'";
        }
        $query .= " GROUP BY CityAliasName ORDER BY CityAliasName ASC;";
        $return = $this->connection->fetchAll($query);
        return $return;
    }

    public function get(array $fields, $zipcode){
        $query = "SELECT ";
        $comma = '';
        foreach ($fields as $field){
            $query .= "$comma$field";
            $comma = ', ';
        }
        $query .= " FROM `{$this->tableName}` WHERE ZipCode=:zipcode AND PrimaryRecord='P'";
        $return = $this->connection->fetchAll($query, array('zipcode' => $zipcode));
        if (is_array($return) && sizeof($return)){
            return $return[0]; //change this if we decide to return more than one row
        }
        return NULL;
    }
}