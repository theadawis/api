<?php
class Icm_Data_Lexis_Rest implements Icm_Configurable
{
    /**
     * @var Icm_Util_Http_Client $http
     */
    protected $http;

    protected $username;
    protected $password;
    protected $config;


    const BASEURL = 'https://wsonline.seisint.com/WsAccurint/';

    public function __construct($username = NULL, $password = NULL){
        libxml_use_internal_errors(true);
        $this->setUsername($username);
        $this->setPassword($password);
    }

    /**
     * Perform a reverse phone search.  Does not include wireless numbers
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function directoryAssistanceReverseSearch($searchBy = array(), $options = array()) {
        $params = array_merge(
            $this->arrayToParams('SearchBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            ))
        );

        return $this->get('DirectoryAssistanceReverseSearch', $params);

    }

    /**
     * Performs a reverse search, includes wireless results.  See documentation for possible search clauses and options
     *
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function directoryAssistanceWirelessSearch($searchBy = array(), $options = array()) {
        $params = array_merge(
            $this->arrayToParams('SearchBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            ))
        );

        return $this->get('DirectoryAssistanceWirelessSearch', $params);

    }

    /**
     * Performs a Lexis BpsSearch.  See Lexis documentation for possible search clauses and options
     * @param array $searchBy
     * @param array $options
     * @param array $name
     * @return SimpleXMLElement
     */
    function personSearch($searchBy = array(), $options = array()){
        $params = array_merge(
                $this->arrayToParams('SearchBy', $searchBy),
                $this->arrayToParams('Options', $options),
                // $this->arrayToParams('Name', $name),
                $this->arrayToParams('User', array(
                        'GLBPurpose' => 0,
                        'DLPurpose' => 0
                )),
                array(
                    'ver_' => '1.65'
                )
        );

        return $this->get('BpsSearch', $params);
    }

    /**
     * Execute the request
     * @param $func
     * @param $params
     * @return SimpleXMLElement
     */
    protected function get($func, $params) {
        /* $url = static::BASEURL . $func . '?' . http_build_query($params, '', '&');
        echo 'URL: ' . $url.'<br/>'; */
        $this->getClient()->setUrl($this->getUrl() . $func, $params);
        $result = $this->getClient()->post();
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($result);
        $resultParsed = simplexml_load_string($result);
        if (!$resultParsed){
            // var_dump($result);
            $this->_throwXmlException();
        }
        return $resultParsed;
    }

    /**
     * Convert an array to Lexis Nexis's url notation
     * @param $glue
     * @param $array
     * @return array
     */
    protected function arrayToParams($glue, $array) {
        $params = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $params += $this->arrayToParams("{$glue}.{$key}", $value);
            } else {
                $params["{$glue}.$key"] = $value;
            }
        }
        return $params;
    }

    /**
     * Get an instance of the HTTP Client
     * @return Icm_Util_Http_Client
     */
    public function getClient() {
        if (is_null($this->http)) {
            $this->setClient(new Icm_Util_Http_Client());
        }
        return $this->http;
    }

    /**
     * Set an instance of the HTTP Client
     * @param Icm_Util_Http_Client $client
     */
    public function setClient(Icm_Util_Http_Client $client) {
        $this->http = $client;
        $this->http->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $this->http->setOption(CURLOPT_USERPWD, $this->getUsername() . ':' . $this->getPassword());
    }

    /**
     * @param $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }

    /**
     * @param $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    protected function _throwXmlException(){
        $errs = array();
        foreach (libxml_get_errors() as $err) {
            $errs[] = $err->message;
        }
        throw new Icm_Data_Lexis_Exception(implode('\n', $errs));
    }

    public function getUrl() {
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug("TRYING URL: " . static::BASEURL);
        return static::BASEURL;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        $this->setUsername($this->config->getOption('username'));
        $this->setPassword($this->config->getOption('password'));
    }

}
