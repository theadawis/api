<?php
/**
 * Icm Email driver for Green Arrow
 * @author Joe Linn
 *
 */
class Icm_Email_Greenarrow extends Icm_Email_Abstract implements Icm_Email_Interface, Icm_Configurable{
    const mailhost = 'instantcheckmatemail.com'; //hostname of the SMTP server
    const url = 'http://instantcheckmatemail.com/ss/api.php?output=json'; //url for our Green Arrow API, including output format

    // default config values
    protected $defaultConfig = array(
        'enableCache' => false,
        'apiKey' => 'a18758f8',
        'cacheTemplateName' => 'default',
        'cacheLifetime' => 3600,
        'cacheCleaningFactor' => 10000,
        'cacheBackend' => 'redis',
        'cacheDir' => '/dev/shm',
        'cacheIdPrefix' => 'greenarrow'
    );
    protected $http;

    public function __construct(Icm_Config $config = NULL){
        if ($config != NULL){
            $this->setConfig($config);
        }
    }

    public function setConfig(Icm_Config $c){
        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $c->getOption($key, $default);
        }
        $this->setupCache();
    }

    public function getConfig(){
        return Icm_Config::fromArray($this->config);
    }

    /**
     * Determines whether or not the given email address is properly formatted
     * @param string $address
     * @return boolean
     */
    public function checkEmailAddress($address){
        $params = array('email' => $address);
        $result = $this->get('checkEmailAddress', $params);
        return $result['success'] == 1 && $result['valid_format'] == 1;
    }

    /**
     * Create a new email subscriber
     * @param array $params see Green Arrow docs for expected parameters
     * @return array on success; false otherwise
     */
    public function subscriberCreate(array $params){
        $allowedFields = array('listid', 'email', 'format', 'confirmed', 'reactivate', 'reactivate_customfields_merge_type', 'requestip');
        $params = $this->createCustomFields($params, $allowedFields);
        $result = $this->get('subscriberAdd', $params);
        if ($result['success'] == 1){
            return $result;
        }
        return false;
    }

    /**
     * Modify an existing email subscriber
     * @param array $params see Green Arrow docs for expected parameters
     * @return array on success; false otherwise
     */
    public function subscriberModify(array $params){
        $allowedFields = array('listid', 'email', 'status', 'customfields_merge_type', 'format', 'confirmed', 'requestip', 'subscribe_time', 'confirm_time', 'unsubscribe_time', 'bounce_time', 'deactivate_time', 'update_email');
        $params = $this->createCustomFields($params, $allowedFields);
        $result = $this->get('subscriberSetInfo', $params);
        if ($result['success'] == 1){
            return $result;
        }
        return false;
    }

    /**
     * Get information about an existing email subscriber
     * @param array $params see Green Arrow docs for expected parameters
     * @return array on success; false otherwise
     */
    public function subscriberGet(array $params){
        $result = $this->get('subscriberGetInfo', $params);
        if ($result['success'] == 1){
            return $result;
        }
        return false;
    }

    /**
     * Send a transactional email to the specified subscriber
     * @param array $params array('email' => destination email address; must be an existing GreenArrow subscriber, 'listid' => Green Arrow mailing list ID, 'newsletterid' => Green Arrow newsletter / campaign ID)
     * @return array on success; false otherwise
     */
    public function sendEmail(array $params){
        $template = $this->newsletterGetInfo($params['newsletterid']);
        $subscriber = $this->subscriberGet(array('email' => $params['email'], 'listid' => $params['listid']));
        $list = $this->mailingListGetInfo($params['listid']);
        if (is_array($subscriber)){
            // we got subscriber data from GreenArrow; use it
            $subscriber = array_merge($params, $subscriber);
        }
        else{
            // no subscriber data retrieved; use whatever we've got
            $subscriber = $params;
        }

        if (isset($subscriber['customfields'])){
            // replace placeholders with actual values
            foreach ($subscriber['customfields'] as $key => $field){
                $template['textbody'] = str_replace('%%' . $key . '%%', $field, $template['textbody']);
                $template['htmlbody'] = str_replace('%%' . $key . '%%', $field, $template['htmlbody']);
            }
            foreach ($subscriber as $key => $field){
                if (!is_array($field)){
                    $template['textbody'] = str_replace('%%' . $key . '%%', $field, $template['textbody']);
                    $template['htmlbody'] = str_replace('%%' . $key . '%%', $field, $template['htmlbody']);
                }
            }
        }
        else if (is_array($subscriber)){
            foreach ($subscriber as $key => $field){
                if (!is_array($field)){
                    $template['textbody'] = str_replace('%%' . $key . '%%', $field, $template['textbody']);
                    $template['htmlbody'] = str_replace('%%' . $key . '%%', $field, $template['htmlbody']);
                }
            }
        }
        /*print_r($subscriber);
        print_r($list);*/
        // echo '<pre>'.print_r($template, true) . '</pre>';

        $sendParams = array(
            'toAddress' => $params['email'],
            // 'toName' => @$subscriber['customfields']['First_Name'] . ' '.@$subscriber['customfields']['Last_Name'],
            'fromAddress' => $list['owneremail'],
            'fromName' => $list['ownername'],
            'subject' => $template['subject'],
            'bodyHtml' => $template['htmlbody'],
            'bodyText' => $template['textbody']
        );
        if (isset($subscriber['customfields'])){
            $sendParams['toName'] = @$subscriber['customfields']['First_Name'] . ' ' . @$subscriber['customfields']['Last_Name'];
        }
        $this->send($sendParams);
        return true;
    }

    /**
     * Sends an email
     * @param array $params associative array
     * Required keys: toAddress, toName, subject, bodyText
     * Optional keys: fromAddress, fromName, bodyHtml
     */
    public function send(array $params){
        $defaults = array(
            'fromAddress' => 'updates@mailicm.com',
            'fromName' => 'Instant Checkmate',
            'bodyHtml' => NULL,

        );
        $params = array_merge($defaults, $params);


        $mail = new Zend_Mail();
        $mail->setBodyText($params['bodyText']);
        if ($params['bodyHtml'] != NULL){
            $mail->setBodyHtml($params['bodyHtml']);
        }
        $mail->setFrom($params['fromAddress'], $params['fromName']);
        $mail->addTo($params['toAddress'], $params['toName']);
        $mail->setSubject($params['subject']);
        date_default_timezone_set('America/Los_Angeles'); //Zend gets all uppity if we don't specify a timezone
                // Send email to our gmail for split test notifications
                if (isset($params['gmailTransport'])) {
                     $config = array('auth' => 'login',
                    'username' => 'splittest.monitor@gmail.com',
                    'password' => 'gbC78!$Fv1195SCfvQ',
                    'port' => '25',
                    'ssl' => 'tls');
                    $transport = new Zend_Mail_Transport_Smtp('smtp.googlemail.com', $config);
                    $mail->send($transport);
                }
                else
                {
                   $transport = new Zend_Mail_Transport_Smtp(self::mailhost, array('port' => 587));
                   $mail->send($transport);
                }
    }

    /**
     * Get misc option info from Green Arrow
     * @return array
     */
    public function getValidOptions(){
        $params = array();
        $result = $this->get('get_valid_options', $params);
        return $result;
    }

    /**
     * Retrieve information regarding the specified mailing list
     * @param int $listID
     * @return Ambigous <multitype:, mixed>|boolean
     */
    public function mailingListGetInfo($listID){
        $params = array('listid' => $listID);
        $result = $this->get('mailing_list_get_info', $params);
        if ($result['success'] == 1){
            return $result;
        }
        return false;
    }

    /**
     * Retrieve information regarding the specified newsletter
     * @param int $id
     * @return Ambigous <multitype:, mixed>|boolean
     */
    public function newsletterGetInfo($id){
        $params = array('newsletterid' => $id);
        $result = $this->get('newsletter_get_info', $params);
        if ($result['success'] == 1){
            return $result;
        }
        return false;
    }

    /**
     * Makes a Green Arrow API call and returns the results in associative array form
     * @param string $method see Green Arrow Studio docs
     * @param array $params is an associative array (see Green Arrow docs)
     * @return array
     */
    public function get($method, array $params){
        if ($this->config['enableCache']){
            $cacheKeyArgs = array_values($params);
            array_unshift($cacheKeyArgs, $method);
            $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);

            if ($this->cacheManager->getCache($this->config['cacheTemplateName'])->test($cacheKey)){
                // cache exists; use it!
                return $this->cacheManager->getCache($this->config['cacheTemplateName'])->load($cacheKey);
            }
        }
        $params['method'] = $method;
        $this->getClient()->setUrl(self::url . $this->buildQueryString($params));
        $result = $this->getClient()->post();
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($result);
        $resultParsed = json_decode($result, true);

        if ($this->config['enableCache']){
            $this->cacheManager->getCache($this->config['cacheTemplateName'])->save($resultParsed, $cacheKey);
        }
        return $resultParsed;
    }

    /**
     * @param array $params
     * @return string
     */
    protected function buildQueryString(array $params){
        $string = '&apikey=' . $this->config['apiKey'];
        foreach ($params as $key => $param){
            $string .= '&' . urlencode($key) . '=' . urlencode($param);
        }
        return $string;
    }

    /**
     * Some Green Arrow API methods allow custom fields. This function will properly format custom fields
     * @param array $params associative array of all parameters to be passed for the current API call
     * @param array $allowedFields associative array of fields specified in the Green Arrow docs for the current API call
     * @return array of properly formatted parameters for the current API call
     */
    protected function createCustomFields(array $params, array $allowedFields){
        $return = array();
        foreach ($params as $key => $param){
            if (in_array($key, $allowedFields)){
                // $key is allowed; pass it along
                $return[$key] = $param;
            }
            else{
                // $key is not allowed; create a custom field
                $return['customfields[' . $key . ']'] = $param;
            }
        }
        return $return;
    }

    /**
     * Get an instance of the HTTP Client
     * @return Icm_Util_Http_Client
     */
    public function getClient() {
        if (is_null($this->http)) {
            $this->setClient(new Icm_Util_Http_Client());
        }
        return $this->http;
    }

    /**
     * Set an instance of the HTTP Client
     * @param Icm_Util_Http_Client $client
     */
    public function setClient(Icm_Util_Http_Client $client) {
        $this->http = $client;
    }
}
