<?php
/**
 * Icm_Email_Stats_StorageAdapter
 *
 * Insert, Update and Get methods
 */
class Icm_Email_Stats_StorageAdapter extends Icm_Db_Table {

    public function __construct(Icm_Db_Interface $conn) {
        parent::__construct('email_stats', 'email_stat_id', $conn);
    }

    /**
     * add or update email rule statistics
     *
     * @param Icm_Email_Stats $stat
     *
     * @return boolean $result
     */
    public function save(Icm_Email_Stats $stat) {
        if (isset($stat->email_stat_id)) {
            $this->updateOne($stat->toArray());
        } else {
            $this->insertOne($stat->toArray());
        }

        return true;
    }

    /**
     * get rule/created record if it exists
     *
     * @param integer $ruleId
     * @param string $created date YYYY-MM-DD
     *
     * @return Icm_Email_Stats
     */
    public function getByRuleCreated($ruleId, $created){
        $result = $this->findOneByFields(array('email_stat_rule' => $ruleId, 'email_stat_created' => $created));

        if (is_array($result)) {
            return Icm_Email_Stats::fromArray($result);
        }

        return null;
    }

    /**
     * get rule records
     *
     * @param integer $ruleId
     *
     * @return Icm_Email_Stats
     */
    public function getByCreated($created){
        $result = $this->findBy('email_stat_created',  "'$created'");

        if (is_array($result)) {
            $resultList = array();

            foreach ($result as $resultItem) {
                $resultList[] = Icm_Email_Stats::fromArray($resultItem);
            }

            return $resultList;
        }

        return null;
    }
}
