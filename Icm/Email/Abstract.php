<?php
abstract class Icm_Email_Abstract{
    protected $config;
    protected $cachemanager;

    protected function setupCache(){
        if ($this->config['enableCache']) {
            /*$templateName = $this->config['cacheTemplateName'];
            $this->setCacheTemplateName($templateName);*/
            $this->setCacheManager(Icm_Util_Cache::initCacheFromConfig(Icm_Config::fromArray($this->config)));
        }
    }

    /**
     * @param Zend_Cache_Manager $cacheManager
     */
    public function setCacheManager(Zend_Cache_Manager $cacheManager) {
        $this->cacheManager = $cacheManager;
    }
}
?>