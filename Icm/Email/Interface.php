<?php
/**
 * All Icm Email drivers should implement this interface.
 * @author Joe Linn
 *
 */
interface Icm_Email_Interface{
    /**
     * Create a new email subscriber
     * @param array $params
     */
    public function subscriberCreate(array $params);

    /**
     * Modify an existing email subscriber
     * @param array $params
     */
    public function subscriberModify(array $params);

    /**
     * Get stored information regarding the specified email subscriber
     * @param array $params
     */
    public function subscriberGet(array $params);

    /**
     * Send a transactional email to the specified email subscriber
     * @param array $params
     */
    public function sendEmail(array $params);
}
?>