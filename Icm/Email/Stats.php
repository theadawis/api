<?php

class Icm_Email_Stats extends Icm_Struct {
    public $email_stat_id;
    public $email_stat_created;
    public $email_stat_rule;
    public $email_stat_count;
    public $email_stat_updated;
}
