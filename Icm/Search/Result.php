<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/20/12
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Search_Result extends Icm_Collection implements ArrayAccess
{
    protected $data;
    protected $searchCriteria;
    protected $searchType;

    public function __construct($searchCriteria, $searchType, $data) {
        parent::__construct($data);
        $this->searchCriteria = $searchCriteria;
        $this->searchType = $searchType;
    }

    public function count(){
        return sizeof($this->data);
    }

    public function getData(){
        return $this->data;
    }

    /**
     * Applies passed filters to current result set
     * @param array $filters array(array(string filter function, string filter value, bool filter action))
     */
    public function applyFilters($filters = array()) {
        $backup = null;

        foreach ($filters as $filter) {
            if ($this->searchType != 'backgroundCheck' && sizeof($this->data)){
                $backup = $this->data[0];
            }

            $this->data = array_values(array_filter($this->data, function($entity) use($filter) {
                    $filterFunc = $filter[0];
                    $filterVal = $filter[1];
                    $filterAction = $filter[2];
                    if (!$entity->$filterFunc($filterVal)) {
                        // remove this
                        return $filterAction;
                    }
                    // don't remove this
                    return !$filterAction;
            }));

            if ($this->searchType != 'backgroundCheck' && !sizeof($this->data)){
                // everything got filtered out!  oh noes!
                $this->data = array($backup);
            }
        }
    }

    public function offsetExists($offset){
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset){
        if ($this->offsetExists($offset)){
            return $this->data[$offset];
        }
        throw new Exception("Offset $offset does not exist.");
    }

    public function offsetSet($offset, $value){
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset){
        unset($this->data[$offset]);
    }

    public function sortQuantitative(){
        usort($this->data, function($a, $b){
            $avars = sizeof(get_object_vars($a));
            $bvars = sizeof(get_object_vars($b));
            if ($avars > $bvars){
                return -1;
            }
            else if ($avars < $bvars){
                return 1;
            }
            return 0;
        });
        return $this;
    }

    public function sortRecent(){
        usort($this->data, function($a, $b){
            $aTime = strtotime($a->timestamp);
            $bTime = strtotime($b->timestamp);
            if ($aTime > $bTime){
                return -1;
            }
            else if ($aTime < $bTime){
                return 1;
            }
            return 0;
        });
        return $this;
    }

    public function sortAlphabetical($field, $reverse = false){
        usort($this->data, function($a, $b) use ($field, $reverse){
            $aData = $a->$field;
            $bData = $b->$field;
            $gt = 1;
            $lt = -1;
            if ($reverse){
                $gt = -1;
                $lt = 1;
            }
            if (is_null($aData) || $aData == ''){
                // no data for $a->$field
                return 1;
            }
            if (is_null($bData) || $bData == ''){
                // no data for $b->$field
                return -1;
            }
            $compare = strcasecmp($aData, $bData);
            if ($compare < 0){
                return $lt;
            }
            else if ($compare > 0){
                return $gt;
            }
            return 0;
        });
        return $this;
    }

}
