<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/23/12
 * Time: 9:01 AM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Search_Query_Interface
{

    const CRITERIA_PHONENUMBER = 'phone';
    const CRITERIA_FIRSTNAME = 'first_name';
    const CRITERIA_MIDDLENAME = 'middle_name';
    const CRITERIA_LASTNAME = 'last_name';
    const CRITERIA_STATE = 'state';
    const CRITERIA_CITY = 'city';
    const CRITERIA_ZIP = 'zip';
    const CRITERIA_ID = 'id';
    const CRITERIA_AGE_LOW = 'age_low';
    const CRITERIA_AGE_HIGH = 'age_high';

    const FILTER_HAS_A = 'hasA';
    const FILTER_IS_A = 'isA';
    const FILTER_HAS_HASH = 'hasHash';

    const FILTER_ACTION_EXCLUDE = FALSE;
    const FILTER_ACTION_INCLUDE = TRUE;


    const IS_A_BUSINESS = 'Icm_Entity_Business';
    const IS_A_PERSON = 'Icm_Entity_Person';
    const IS_A_CRIMINAL = 'Icm_Entity_Criminal';
    const IS_A_TERRORIST = 'Icm_Entity_Terrorist';

    const SEARCH_BACKGROUND = 'backgroundCheck';
    const SEARCH_REVERSEPHONE = 'reversePhone';
    const SEARCH_BACKGROUND_TEASER = 'backgroundCheckTeaser';
    const SEARCH_REVERSEPHOEN_TEASER = 'reversePhoneTeaser';



    /**
     * Add criteria to the query
     * @param string $name
     * @param mixed $value
     */
    public function addCriteria($name, $value);

    /**
     * @return array: an array of query criteria (typically stored in a private property)
     */
    public function getCriteria();

    /**
     * @return int An integer representing the search type of the current Query
     */
    public function getSearchType();

    /**
     * @return array: An array of filters to be applied to the current Query's result set
     */
    public function getFilters();

    /**
     * Add a filter to the private array of filters
     * @param int $type represents a filter type
     * @param string $value should be a property name
     */
    public function addFilter($type, $value);

    /**
     * Adds a filter of type FILTER_HAS_A to the current Query's private array of filters
     * @param string $value
     */
    public function hasA($value);

    /**
     * Adds a filter of type FILTER_IS_A to the current Query's private array of filters
     * @param string $type
     */
    public function isA($type);

    /**
     * Adds a filter of type FILTER_HAS_HASH to the current Query's private array of filters
     * @param string $hash
     */
    public function hasHash($hash);

    /**
     * True if this query should allow paid searches
     * @abstract
     * @return bool
     */
    public function allowPaid();
}