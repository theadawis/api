<?php

/**
 *
 */
class Icm_Search_Engine implements Icm_Cacheable, Icm_Configurable
{

    const CRITERIA_PHONENUMBER = 'phone';
    const CRITERIA_FIRSTNAME = 'first_name';
    const CRITERIA_MIDDLENAME = 'middle_name';
    const CRITERIA_LASTNAME = 'last_name';
    const CRITERIA_CITY = 'city';
    const CRITERIA_ZIP = 'zip';
    const CRITERIA_STATE = 'state';
    const CRITERIA_AGE_LOW = 'age_low';
    const CRITERIA_AGE_HIGH = 'age_high';

    const FILTER_HAS_A = 'hasA';
    const FILTER_IS_A = 'isA';
    const FILTER_HAS_HASH = 'hasHash';

    const IS_A_BUSINESS = 'Icm_Entity_Business';
    const IS_A_PERSON = 'Icm_Entity_Person';
    const IS_A_CRIMINAL = 'Icm_Entity_Criminal';
    const IS_A_TERRORIST = 'Icm_Entity_Terrorist';

    protected $_paginator;
    protected $_adapters  = array();
    protected $_additiveAdapters = array();
    protected $_subtractiveAdapters = array();
    protected $_criteria  = array();
    protected $_filters   = array();

    /**
     * @var Zend_Cache_Manager
     */
    protected $cachemanager;

    /**
     * @var Icm_Config
     */
    protected $config;


    /**
     * @param $adapters
     */
    public function setAdapters($adapters) {
        $this->_adapters = array();
        foreach ($adapters as $adapter){
            $this->addAdapter($adapter);
        }
    }

    /**
     * @param $paginator
     */
    public function setPaginator(Icm_Paginator $paginator) {
        $this->_paginator = $paginator;
    }

    /**
     * @param Icm_Search_Adapter_Abstract $adapter
     */
    public function addAdapter(Icm_Search_Adapter_Abstract $adapter) {
        $this->_adapters[] = $adapter;
    }

    /**
     * @param Icm_Search_Adapter_Abstract $adapter
     */
    public function addAdditiveAdapter(Icm_Search_Adapter_Abstract $adapter){
        $this->_additiveAdapters[] = $adapter;
    }

    /**
     * @param Icm_Search_Adapter_Abstract $adapter
     */
    public function addSubtractiveAdapter(Icm_Search_Adapter_Abstract $adapter){
        $this->_subtractiveAdapters[] = $adapter;
    }

    /**
     * @return array
     */
    public function getAdapters() {
        if (!count($this->_adapters)) {
            return array();
        }
        return $this->_adapters;
    }

    public function getAdditiveAdapters(){
        if (!count($this->_additiveAdapters)) {
            return array();
        }
        return $this->_additiveAdapters;
    }

    public function getSubtractiveAdapters(){
        if (!count($this->_subtractiveAdapters)) {
            return array();
        }
        return $this->_subtractiveAdapters;
    }

    /**
     * @param $type
     * @param $value
     */
    public function addCriteria($type, $value) {
        $this->_criteria[$type] = $value;
    }

    /**
     * @return array
     */
    public function getCriteria() {
        return $this->_criteria;
    }

    /**
     * @param $filterType
     * @param $valueType
     */
    public function addFilter($filterType, $valueType) {
        $this->_filters[] = array($filterType, $valueType);
    }

    /**
     * @param $valueType
     */
    public function hasA($valueType) {
        $this->addFilter(self::FILTER_HAS_A, $valueType);
    }

    /**
     * @param $valueType
     */
    public function isA($valueType) {
        $this->addFilter(self::FILTER_IS_A, $valueType);
    }

    /**
     * $args is an assoc array of key => value pairs, keys should be properties of the entities to filter
     *
     * @param $args
     */
    public function hasHash($args = array()) {
        $this->addFilter(self::FILTER_HAS_HASH, $args);
    }

    public function imFeelingLucky(Icm_Search_Query $q) {
        return $this->search($q)->offsetGet(0);
    }

    public function search(Icm_Search_Query_Interface $q) {
        if ($this->getConfig()->getOption('enableCache', false)){
            $cacheName = $this->getConfig()->getOption('cacheTemplateName', 'searchcache_');
            $cacheKey = md5(serialize($q->getCriteria()) . $q->getSearchType().get_class($this->_additiveAdapters[0]) . 'search');
            if ($this->cachemanager->getCache($cacheName)->test($cacheKey)){
                // we have cached data; filter, then return it
                $resultSet = $this->cachemanager->getCache($cacheName)->load($cacheKey);

                return $resultSet;
            }
        }

        $criteria = $q->getCriteria();
        $type = $q->getSearchType();
        $resultSet = new Icm_Search_Result($criteria, $type, array());

        $adapters = $this->getAdditiveAdapters();

        if (!count($adapters)) {
            throw new Icm_Service_Exception("No additive Data Adapters present. Shame.");
        }

        foreach ($adapters as $adapter) {
            if (!method_exists($adapter, $type)) {
                $class = get_class($adapter);
                throw new Icm_Service_Exception("Invalid search type: $type in $class. ...Shame. Shame on you.");
            }

            if ($adapter->isPaid($type)) {
                if (!$q->allowPaid()) {
                    continue;
                }
            }

            $results = $adapter->$type($q->getCriteria());

            if (count($results)) {
                $resultSet->addAll($results);
            }
        }

        foreach ($this->getSubtractiveAdapters() as $adapter){
            if (!method_exists($adapter, $type)) {
                $class = get_class($adapter);
                throw new Icm_Service_Exception("Invalid search type: $type in $class. ...Shame. Shame on you.");
            }

            if ($adapter->isPaid($type) && !$q->allowPaid()){
                continue;
            }

            $results = $adapter->$type($q->getCriteria());

            if (count($results)){
                foreach ($results as $result){
                    if (!is_a($result, 'Icm_Entity')){
                        throw new Icm_Service_Exception("Data sources MUST return Entities. SHAME!");
                    }
                    $q->hasHash($result->getHash());
                }
            }
        }

        // filter BEFORE caching
        $resultSet->applyFilters($q->getFilters());

        if ($this->getConfig()->getOption('enableCache', false)){
            $this->cachemanager->getCache($cacheName)->save($resultSet, $cacheKey);
        }

        return $resultSet;
    }

    /**
     * @param $fn
     * @param $interfaceType
     * @return array
     * @throws Icm_Service_Exception
     */
    protected function _execute($fn, $interfaceType) {

        $resultSet = array();

        $adapters = $this->getAdapters();
        if (!count($adapters)) {
            throw new Icm_Service_Exception("No Data Adapters present");
        }

        foreach ($this->getAdapters() as $adapter) {
            if (in_array($interfaceType, class_implements(get_class($adapter)))) {
                $results = $adapter->$fn($this->getCriteria());
                $results = $this->_filterResults($results);
                if (count($results)) {
                    $resultSet = array_merge($results, $resultSet);
                }
            } else {
                throw new Icm_Service_Exception("Class {".get_class($adapter)."} does not implement {$interfaceType}");
            }
        }

        return $resultSet;
    }

    /**
     * @param $results
     * @return array
     */
    protected function _filterResults($results) {

        $filteredResults = array();

        foreach ($results as $entity) {
            $clean = true;
            foreach ($this->_filters as $filter) {
                $filterFunc = $filter[0];
                $valueType = $filter[1];
                if (!$entity->$filterFunc($valueType)) {
                    $clean = false;
                    break;
                }
            }
            if ($clean) {
                $filteredResults[] = $entity;
            }
        }

        return $filteredResults;
    }

    public function setCacheManager(Zend_Cache_Manager $manager){
        $this->cachemanager = $manager;
    }

    public function getCacheManager(){
        return $this->manager;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
    }

    public function getConfig(){
        if (is_null($this->config)){
            $this->config = new Icm_Config();
        }
        return $this->config;
    }
}
