<?php

/**
 *
 */
interface Icm_Search_Adapter_Interface_Optout extends Icm_Search_Adapter_Interface{
    /**
     * @abstract
     * @param array $args
     */
    public function optout($args=array(), $personIds=null);
}
