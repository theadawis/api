<?php
/**
 *
 */
interface Icm_Search_Adapter_Interface_Reversephone extends Icm_Search_Adapter_Interface{
    /**
     * @abstract
     * @param $criteria
     */
    public function reversePhone($criteria);

    /**
     * @abstract
     * @param $criteria
     */
    public function reversePhoneTeaser($criteria);
}
