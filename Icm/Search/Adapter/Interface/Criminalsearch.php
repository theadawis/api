<?php

/**
 *
 */
interface Icm_Search_Adapter_Interface_Criminalsearch extends Icm_Search_Adapter_Interface{

    /**
     * Performs a NPD criminal search
     * @param array $args array('firstName' => 'bob', 'lastName' => 'smith', 'DOB' => '12/31/1950', 'state' => 'CA')
     * @return array of Icm_Entity_Criminal objects
     */
    public function criminalSearch(array $args=array());
}