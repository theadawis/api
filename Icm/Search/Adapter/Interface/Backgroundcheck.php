<?php

/**
 *
 */
interface Icm_Search_Adapter_Interface_Backgroundcheck extends Icm_Search_Adapter_Interface{
    /**
     * @abstract
     * @param array $args
     */
    public function backgroundCheck($args=array());

    /**
     * @abstract
     * @param array $args
     */
    public function backgroundCheckTeaser($args = array());
}
