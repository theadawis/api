<?php

/**
 * Redis search adapter file
 *
 * @category Api
 * @version 1.0
 * @package Api
 * @subpackage Api_Search
 */

/**
 * Redis search adapter class
 *
 * @category Api
 * @package Api_Search
 * @subpackage Api_Search
 */
 class Icm_Search_Adapter_RedisPopularNames extends Icm_Search_Adapter_Abstract
{
    /**
     * @var Icm_Redis
     */
    protected $redis;

    /**
     * @var string keymaker class
     */
    protected $keymaker;

    /**
     * @var integer
     */
    protected static $maxSearchResults = 5;

    /**
     * @todo make this work, probably want to accept an object not an array
     * @param array $options
     */
    public function __construct(Icm_Redis $redis = null){
        $this->redis = $redis;
    }

    /**
     * doing this to support unit test mocks
     * @param string $keymaker class name
     */
    public function setKeymaker($keymaker) {
        $this->keymaker = $keymaker;
    }

    /**
     * doing this to support unit test mocks
     * @param string $keymaker class name
     */
    public function getKeymaker() {
        if (!$this->keymaker) {
            $this->keymaker = 'Icm_Util_PopularNames';
        }
        return $this->keymaker;
    }

    /**
     * do the redis search here
     * @param array $criteria
     * @return array $result list of popular names
     */
    public function nameSearch(array $criteria=array()) {
        $result = array();
        if (!empty($criteria['name']) && !empty($criteria['type'])) {
            $maxResults = !empty($criteria['maxresults']) ?
                        $criteria['maxresults'] :
                        self::$maxSearchResults;
            // valid search criteria so build redis key search
            $keymaker = $this->getKeymaker();
            $key = $keymaker::makePopularnamesRedisSearchKey($criteria['type'], $criteria['name']);
            // get all matching keys (e.g. firstname:john:1)
            if ($data = $this->redis->keys($key)) {
                // loop through returned data
                $newdata = array();
                foreach ($data as $sub) {
                    // split key string into name-position, name, rank
                    $datachunks = preg_split('/:/', $sub);
                    // key=name, value=rank
                    $newdata[$datachunks[1]] = $datachunks[2];
                }
                // cleanup memory
                unset($data);

                // sort array by rank value
                asort($newdata);

                // shrink results
                $newdata = array_slice($newdata, 0, $maxResults);
                // create array with name only
                $finaldata = array();
                foreach ($newdata as $name => $rank) {
                    $finaldata[] = array('name' => $name);
                }
                // cleanup memory
                unset($newdata);

                // map and create result array
                $result = array();
                foreach ($finaldata as $row) {
                    $_person = Icm_Entity_PopularPerson::create($row);
                    if ($_person) {
                        $result[] = $_person;
                    }
                }
                // cleanup memory
                unset($finaldata);
            }
        }
        return $result;
}

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
     */
    public function isPaid($method){
        return false;
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
     */
    public function isTeaser($method){
        return false;
    }
}

?>