<?php

/**
 *
 */
abstract class Icm_Search_Adapter_Abstract implements Icm_Search_Adapter_Interface{

    /**
     * @var Icm_Config $config;
     */
    protected $config;

    /**
     * @var Icm_Util_Paginator $paginator;
     */
    protected $hasPaginator = false;
    protected $paginator;

    /**
     * @var Zend_Cache_Manager $cacheManager;
     */
    protected $cacheTemplateName; //could be in config or could be default - lets store it here to avoid unneccary logic all the time
    protected $cacheManager;
    protected $enableCache = false;

    /**
     * @var Icm_Entity_Factory_Map $factoryMap
     */
    protected $factoryMap;

    /**
     * @param $type
     * @return Icm_Entity_Factory
     * @throws Icm_Search_Adapter_Exception
     */
    public function getEntityFactory($type) {
        return $this->factoryMap->getEntityFactory($type);
    }

    public function setFactoryMap(Icm_Entity_Factory_Map $map) {
        $this->factoryMap = $map;
    }

    /**
     * @param bool $enable
     */
    public function setEnableCache($enable){
        $this->enableCache = $enable;
    }

    public static function factory(Icm_Config $config) {
        $class = get_called_class();
        return self::create($class, $config);
    }

    public static function create($class, Icm_Config $config) {
        $class = new $class();
        $class->setConfig($config);

        // init a cache template for this obj if we don't yet have one
        if ($config->getOption('enableCache')) {
            $templateName = $config->getOption('cacheTemplateName') ? $config->getOption('cacheTemplateName') : 'default';
            $class->setCacheTemplateName($templateName);
            $class->setCacheManager(Icm_Util_Cache::initCacheFromConfig($config));
        }

        return $class;
    }

    /**
     * @param Icm_Config $config
     */
    public function setConfig(Icm_Config $config) {
        $this->config = $config;
    }

    /**
     * @return Icm_Config
     */
    public function getConfig() {
        if (is_null($this->config)) {
            $this->config = new Icm_Config();
        }

        return $this->config;
    }

    /**
     * @param Icm_Util_Paginator $paginator
     */
    public function setPaginator(Icm_Util_Paginator $paginator) {
        $this->hasPaginator = true;
        $this->paginator = $paginator;
    }

    /**
     * @return Icm_Util_Paginator
     */
    public function getPaginator() {
        return $this->paginator;
    }

    /**
     * @param Zend_Cache_Manager $cacheManager
     */
    public function setCacheManager(Zend_Cache_Manager $cacheManager) {
        $this->cacheManager = $cacheManager;
    }

    /**
     * @return Zend_Cache_Manager
     */
    public function getCacheManager() {
        return $this->cacheManager;
    }

    /**
     * @param $cacheTemplateName
     */
    public function setCacheTemplateName($cacheTemplateName) {
        $this->cacheTemplateName = $cacheTemplateName;
    }

    /**
     * @return $cacheTemplateName
     */
    public function getCacheTemplateName() {
        return $this->cacheTemplateName;
    }

    /**
     * This function is a convenience function - it will basically do 2 things that you probably want to do:
     * 1. handle caching: check to see if your search is cached and use that as well as cache your results if they have not been cached
     * 2. handle building entity objects based on a function you provide
     *
     * @param $params array
     * @return $results
     */
    protected function _fetchAndCache($params = array()) {
        // must pass in params:
        // entityCallback: function
        // fetchCallback: function
        // cacheKey: str
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug('enable cache? ' . $this->enableCache);

        // if caching is enabled and there is a cache, use it
        if (!empty($params['cacheKey'])
            && $this->enableCache
            && $this->cacheManager->getCache($this->cacheTemplateName)->test($params['cacheKey'])) {
                Icm_Util_Logger_Syslog::getInstance('api')->logDebug('loaded results from cache');
                $result = $this->cacheManager->getCache($this->cacheTemplateName)->load($params['cacheKey']);
        }
        else {
            Icm_Util_Logger_Syslog::getInstance('api')->logDebug('cache miss');

            // fetch
            $rows = $params['fetchCallback']();
            Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 2: '));

            // apply the make entity func to each element in row
            $merged = array();
            $results = array();

            foreach ($rows as $row) {
                $row = $params['entityCallback']($row);

                if (isset($row->hash_key)) {
                    $merged[$row->hash_key] = $row;
                }
                else {
                    $results[] = $row;
                }
            }

            if (count($merged) > 0) {
                $result = array_values($merged);
            }
            else {
                $result = $results;
            }

            // cache it if caching is enabled
            if ($this->enableCache) {
                $this->cacheManager->getCache($this->cacheTemplateName)->save($result, $params['cacheKey']);
            }
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 3: '));

        return $result;
    }

    /**
     * Alter the case of an array's keys and values
     *
     * @param array $arr
     * @param array $skipList
     * @return array
     */
    public function changeCaseRecursive($arr, $skipList = array()) {
        $changedArray = array();

        foreach ($arr as $key => $value) {
            // ensure the key starts out lowercase
            $newKey = lcfirst($key);

            // non-array values Become Proper Case
            if (!is_array($value) && !in_array($key, $skipList)) {
                $value = ucwords(strtolower($value));
            }
            elseif (is_array($value)) {
                // recurse if we have an array for a value
                $value = self::changeCaseRecursive($value, $skipList);
            }

            // set the new key to altered value
            $changedArray[$newKey] = $value;
        }

        // return the re-built array
        return $changedArray;
    }

    /**
     * Re-format a phone number
     *
     * @param string $phone
     * @return string
     */
    protected function phoneFormat($phone) {
        $formattedPhone = preg_replace("/[^0-9]/", "", $phone);

        // insert the final '-'
        $formattedPhone = substr($formattedPhone, 0, -4) . '-' . substr($formattedPhone, -4);

        // if all 10 digits given, add () around the first three
        if (strlen($phone) == 10) {
            $formattedPhone = '(' . substr($formattedPhone, 0, 3) . ')' . substr($formattedPhone, 3);
        }

        return $formattedPhone;//$phone;
    }
}
