<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/18/12
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Search_Adapter_Lexis extends Icm_Search_Adapter_Abstract implements
    Icm_Search_Adapter_Interface_Reversephone,
    Icm_Search_Adapter_Interface_Backgroundcheck {

    const PROVIDER_NAME = 'LEXIS';

    /**
     * @var Icm_Service_Lexis_Rest
     */
    protected $restApi;
    /**
     * @var Icm_Service_Lexis_Teaser
     */
    protected $teaserApi;

    public function __construct(Icm_Service_Lexis_Rest $restApi, Icm_Service_Lexis_Teaser $teaserApi) {
        $this->restApi = $restApi;
        $this->teaserApi = $teaserApi;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
    */
    public function isPaid($method){
        switch($method){
            case 'reversePhone':
            case 'reversePhoneTeaser': return true;
                break;
            default: return false;
        }
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
    */
    public function isTeaser($method){
        switch($method){
            case 'reversePhoneTeaser':
            case 'backgroundCheck': //change this if / when we start using this for full background checks
            case 'backgroundCheckTeaser': return true;
                break;
            default: return false;
                break;
        }
    }

    /**
     * Perform a Lexis comprehensive person report search
     * @param array $criteria
     * @throws Icm_Service_Exception
     * @return array
     */
    public function comprehensiveSearch($criteria){
        if (!array_key_exists(Icm_Search_Query_Interface::CRITERIA_ID, $criteria)){
            throw new Icm_Service_Exception("You must provide an ID in order to use the comprehensive search.");
        }
        $results = $this->restApi->comprehensiveSearch(array('UniqueId' => $criteria[Icm_Search_Query_Interface::CRITERIA_ID]),
                array(
                        'IncludeAKAs' => true,
                        'IncludeImposters' => true,
                        'IncludeOldPhones' => true,
                        'IncludeAssociates' => true,
                        'IncludeProperties' => true,
                        'IncludePriorProperties' => true,
                        'IncludeCurrentProperties' => true,
                        'IncludeDriversLicenses' => true,
                        'IncludeMotorVehicles' => true,
                        'IncludeBankruptcies' => true,
                        'IncludeLiensJudgments' => true,
                        'IncludeCorporateAffiliations' => true,
                        'IncludeUCCFilings' => true,
                        'IncludeFAACertificates' => true,
                        'IncludeCriminalRecords' => true,
                        'IncludeCensusData' => true,
                        'IncludeAccidents' => true,
                        'IncludeWaterCrafts' => true,
                        'IncludeProfessionalLicenses' => true,
                        'IncludeHealthCareSanctions' => true,
                        'IncludeDEAControlledSubstance' => true,
                        'IncludeDEAControlledSubstance2' => true, //Lexis documentation is kidof ambiguous here, including it just in case
                        'IncludeVoterRegistrations' => true,
                        'IncludeHuntingFishingLicenses' => true,
                        'IncludeWeaponPermits' => true,
                        'IncludeConcealedWeapons' => true, //Was the old call route, including it just in case
                        'MaxConcealedWeapons' => 100,
                        'IncludeSexualOffenses' => true,
                        // 'IncludeCivilCourts' => true,  //not implemented by Lexis
                        'IncludeFAAAircrafts' => true,
                        'IncludeInternetDomains' => true,
                        'IncludePeopleAtWork' => true,
                        'IncludePeopleAtWork2' => true,
                        'IncludeHighRiskIndicators' => true,
                        'IncludeForeclosures' => true,
                        'IncludePhonesPlus' => true,
                        'IncludeStudentInformation' => true,
                        'IncludeIncludeSourceDocs' => true,
                        'IncludeFirearmExplosives' => true,
                        'IncludeCivilCourts' => true,
                        'IncludeStudentInformation' => true,
                        'DoPhoneReport' => true,
                        'LawEnforcement' => true,
                        'AcceptedNonSolicitationTerms' => true,
                        'IncludeNoticeOfDefaults' => true,
                        'EnableNationalAccidents' => true,
                        'Relatives' => array(
                                'IncludeRelatives' => true,
                                'MaxRelatives' => 100,
                                'RelativeDepth' => 100,
                                'IncludeRelativeAddresses' => true,
                                'MaxRelativeAddresses' => 100
                        ),
                        'Neighbors' => array(
                                'IncludeNeighbors' => true,
                                'IncludeHistoricalNeighbors' => true,
                                'NeighborhoodCount' => 100,
                                'NeighborCount' => 100
                        ),
                        'MaxAddresses' => 100,
                        'IncludeEmailAddresses' => true,
                        'HealthCareProviders' => array(
                            'IncludeHealthCareProviders' => true,
                            'IncludeGroupAffiliations' => true,
                            'IncludeHospitalAffiliations' => true,
                            'IncludeBusinessAddress' => true,

                        )
                )
            );

        return $this->parseComprehensiveResults($results);
    }

    /**
     * Parse the result set from a Lexis comprehensive person report
     * @param array $results
     * @return array
     */
    protected function parseComprehensiveResults($results){
        $people = array();

        foreach ($results->response->Individual as $record){
            $person = $this->parseComprehensiveRecord($record);

            if ($person){
                $people[] = $person;
            }
        }

        return $people;
    }

    protected function parseComprehensiveRecord($record, $objectify = true){
        $person = $this->getEntityFactory('person')->create();

        $sections = array(
            'liens' => array('LiensJudgments', 'LienJudgment'),
            'watercraft' => array('WaterCrafts', 'WaterCraft'),
            'huntingFishingLicenses' => array('HuntingFishingLicenses', 'HuntingFishingLicense'),
            'criminalRecords' => array('CriminalRecords', 'Criminal'),
            'voterRegistrations' => array('VoterRegistrations', 'VoterRegistration'),
            'sexualOffenses' => array('SexualOffenses', 'SexualOffense'),
            'aircraft' => array('Aircrafts', 'Aircraft'),
            'vehicles' => array('Vehicles', 'Vehicle'),
            'uccFilings' => array('UCCFilings', 'UCCFiling'),
            'peopleAtWork' => array('PeopleAtWorks', 'PeopleAtWork'),
            'faaCertifications' => array('FAACertifications', 'FAACertification'),
            'weaponPermits' => array('WeaponPermits', 'WeaponPermit'),
            'superiorLiens' => array('SuperiorLiens', 'SuperiorLien'),
            'foreclosures' => array('Foreclosures', 'Foreclosure'),
            'providers' => array('Providers', 'Provider'),
            'sanctions' => array('Sanctions', 'Sanction'),
            'controlledSubstances' => array('ControlledSubstances', 'ControlledSubstance'),
            'imposters' => array('Imposters', 'Imposter'),
            'phonesPlus' => array('PhonesPluses', 'PhonesPlus'),
            'accidents' => array('Accidents', 'Accident'),
            'relatives' => array('Relatives', 'Relative'),
            'associates' => array('Associates', 'Associate'),
            'neighbors' => array('Neighbors', 'Neighborhood'),
            'professionalLicenses' => array('ProfessionalLicenses', 'ProfessionalLicense'),
            'oldPhones' => array('OldPhones', 'OldPhone'),
            'akas' => array('AKAs', 'Identity'),
            'bankruptcies' => array('Bankruptcies', 'Bankruptcy'),
            'properties' => array('Properties', 'Property'),
            'corporateAffiliations' => array('CorporateAffiliations', 'Affiliation'),
            'addresses' => array('BpsReportAddresses', 'BpsReportAddress'),
            'emailAddresses' => array('EmailAddresses', 'EmailAddress'),
        );

        /*
        |---------------------------------------------------------------------------|
        |                         --====!!!IMPORTANT!!!====--                       |
        |---------------------------------------------------------------------------|
        |                                                                           |
        |TODO:the "if (properties)" statement needs to be removed (functional hack) |
        |The if properties statement is a quick hack we put together to avoid the   |
        |incorrect modification of the lexis data by the xml2array function.  Long  |
        |term we need to have a "normalized" data structure, and a "raw" data       |
        |structure for forward compatibility.                                       |
        |___________________________________________________________________________|

        */
        foreach ($sections as $name => $section){
            $arrayData = $this->parseComprehensiveSection($section, $record);

            if ($name == 'properties') {
                $arrayData = $this->normalizePropertyEntities($arrayData);
            }

            $person->{$name} = json_decode(json_encode($arrayData));
        }

        // build some flags so we can display only relevant information

        // sex offender flag
        $person->isSexOffender = (!empty($person->sexualOffenses)) ? true : false;

        // business owner flag
        $person->isBusiness = (!empty($person->corporateAffiliations)) ? true : false;

        // assets flag
        $person->hasAssets = (empty($person->watercraft) && empty($person->properties) && empty($person->aircraft) && empty($person->bankruptcies) && empty($person->superiorLiens)) ? false :true;

        // Accidents Flag (florida only)
        $person->hasAccidents = (!empty($person->accidents)) ? true : false;

        return $person;
    }

    /**
     * If property entity is not a numeric indexed array, make it so
     *
     * @param array $arrayData
     * @return array
     */
    private function normalizePropertyEntities(array $arrayData) {
        foreach ($arrayData as $key => $property) {
            if (!array_key_exists(0, $property['entities']['entity'])) {
                $tmpArray = $arrayData['entities']['entity'];
                unset($arrayData[$key]['entities']['entity']);
                $arrayData[$key]['entities']['entity'] = array($tmpArray);
            }
        }

        return $arrayData;
    }

    protected function parseComprehensiveSection(array $section, $data){
        $return = array();

        if (isset($data->{$section[0]}->{$section[1]})){
            foreach ($data->{$section[0]}->{$section[1]} as $xml){
                $return[] = json_decode(json_encode((array) $xml), 1);
            }
        }

        return $this->changeCaseRecursive($return);
    }

    protected function xmlToArray($xml){
        // why are we using recursion??  This is deleting and skipping elements, we can take advantage of PHP's stupidity, and do this: return json_decode(json_encode((array) $xml), 1); --shiem
        $arr = array();

        foreach ($xml as $element) {
            $tag = lcfirst($element->getName());
            $e = get_object_vars($element);

            if (strtolower($tag) == 'ssn'){
                continue;
            }

            if (strtolower($tag) == 'dob'){
                $tag = strtolower($tag);
            }

            if (strpos(strtolower($tag), 'date') !== false || strtolower($tag) == 'dob'){
                $arr[$tag] = $this->parseDate($element);
            }
            else if (!empty($e)) {
                $arr[$tag] = $element instanceof SimpleXMLElement ? $this->xmlToArray($element) : $e;
            }
            else {
                $arr[$tag] = trim($element);
            }
        }

        return $arr;
    }

    protected function parseDate($lexisDate){
        if (!isset($lexisDate->Day)){
            $day = '01';
        }
        else{
            $day = $lexisDate->Day;
        }

        return strtotime($lexisDate->Month.'/' . $day.'/' . $lexisDate->Year);
    }

    protected function parseProperties($data){
        $properties = array();

        foreach ($data as $property){
            $propertyStruct = new Icm_Struct_Lexis_Property();
            $propertyStruct->address = $this->getEntityFactory('location')->create()->putAddress($property->Address->StreetNumber.' ' . $property->Address->StreetName.' ' . $property->Address->StreetSuffix, (string) $property->Address->City, (string) $property->Address->State, $property->Address->Zip5.'-' . $property->Address->Zip4);
            $propertyStruct->lender = (string) $property->LenderName;
            $propertyStruct->sellerName = (string) $property->NameSeller;
            $propertyStruct->salePrice = (string) $property->SalePrice;
            $propertyStruct->saleDate = $this->parseDate($property->SaleDate);
            $propertyStruct->recordingDate = $this->parseDate($property->RecordingDate);
            $propertyStruct->ownerAddress = $this->getEntityFactory('location')->create()->putAddress($property->OwnerAddress->StreetNumber.' ' . $property->OwnerAddress->StreetName.' ' . $property->OwnerAddress->StreetSuffix, (string) $property->OwnerAddress->City, (string) $property->OwnerAddress->State, $property->OwnerAddress->Zip5.'-' . $property->OwnerAddress->Zip4);
            $propertyStruct->type = (string) $property->Type;
            $propertyStruct->titleCompany = (string) $property->TitleCompanyName;
            $propertyStruct->landUse = (string) $property->LandUse;
            $properties[] = $propertyStruct;
        }
        return $properties;
    }

    protected function parseAddress($address){
        $addressEntity = $this->getEntityFactory('location')->create();
        $addressEntity->putAddress($address->Address->StreetNumber.' ' . $address->Address->StreetName.' ' . $address->Address->StreetSuffix, (string) $address->Address->City, (string) $address->Address->State, $address->Address->Zip5.'-' . $address->Address->Zip4);
        $addressEntity->dateLastSeen = $this->parseDate($address->DateLastSeen);
        $addressEntity->dateFirstSeen = $this->parseDate($address->DateFirstSeen);

        if (isset($address->Residents->Identity)){
            // this address record has residents
            $residents = array();

            foreach ($address->Residents->Identity as $resident){
                $residentEntity = $this->getEntityFactory('person')->create();
                $residentEntity->first_name = (string) $resident->Name->First;
                $residentEntity->middle_name = (string) $resident->Name->Middle;
                $residentEntity->last_name = (string) $resident->Name->Last;
                $residentEntity->age = (int) $resident->Age;
                $residentEntity->dob = $this->parseDate($resident->DOB);
                $residentEntity->person_id = isset($resident->UniqueId) ? (string) $resident->UniqueId : null;
                $residents[] = $residentEntity;
            }
            $addressEntity->residents = $residents;
        }

        if (isset($address->Properties->Property)){
            // the address record has property records. Get 'em.
            $addressEntity->properties = $this->parseProperties($address->Properties->Property);
        }

        if (isset($address->Phones->Phone)){
            $phones = array();

            foreach ($address->Phones->Phone as $phone){
                $phones[] = Icm_Struct_Lexis_Phone::fromArray(array(
                        'number' => (string) $phone->Phone10,
                        'listingName' => (string) $phone->ListingName,
                        'first_name' => isset($phone->Name) ? (string) $phone->Name->First : null,
                        'last_name' => isset($phone->Name) ? (string) $phone->Name->Last : null,
                        'public' => (string) $phone->PubNonpub == 'P'
                ));
            }

            $addressEntity->phones = $phones;
        }

        return $addressEntity;
    }

    public function personSearch($criteria) {
        if (!array_key_exists('id', $criteria)) {
            throw new Icm_Search_Adapter_Exception("To use person search, you must have a ID criteria");
        }

        $results = $this->restApi->personSearch(array(
            'UniqueId' => $criteria['id']
        ));

        return $this->_parseResults($results);
    }

    protected function _parseResults($results) {
        $people = array();

        foreach ($results->response->Records->Record as $record) {
            $person = $this->_parseRecord($record);

            if ($person) {
                $people[] = $person;
            }
        }

        return $people;
    }

    public function reversePhone($criteria) {
        if (!isset($criteria['phone'])) {
            throw new Icm_Search_Adapter_Exception("Icm_Search_Adapter_Lexis requires a criteria of 'phone' to do a reversePhone search");
        }

        $results = $this->restApi->directoryAssistanceWirelessSearch(array('PhoneNumber' => $criteria['phone']));

        return $this->_parseResultsPhone($results);
    }

    protected function _parseResultsPhone($results) {

        $entities = array();

        // ensure we have records to process
        if ($results->response->RecordCount == 0) {
            return $entities;
        }

        foreach ($results->response->Records->Record as $listing) {
            $entity = $this->getEntityFactory('phone')->create();
            $people = $this->_parseListing($listing);

            // person info
            if ($people) {
                $entity->set('identities', $people);
                unset($people['name']);
            }
            else {
                continue;
            }

            // phone info
            $skipList = array('state');
            $lowercaseListing = $this->changeCaseRecursive($this->xml2array($listing), $skipList);

            foreach ($lowercaseListing as $key => $value) {
                switch ($key) {
                    case 'phone10':
                        $entity->set('phone', $value);
                        $entity->set('phone_formatted', $this->phoneFormat($value));
                        break;
                    case 'address':
                        $location = array_merge(array(), $value);

                        if (isset($value['streetName'], $value['streetSuffix'])) {
                            $location['street'] = trim($value['streetNumber'] . " " . $value['streetName'] . " " . $value['streetSuffix']);
                        }

                        $location[ 'zip']   = $value['zip5'];
                        unset($value);
                        $entity->set('location', $location);
                        break;
                    case 'carrierName':
                        $entity->set('carrier', $value);
                        break;
                    case 'centralOfficeCode':
                        $entity->set('centralOfficeCode', $value);
                        $entity->set('ltype', $value['descriptions']['description']);
                        break;
                    default:
                        $entity->set($key, $value);
                }
            }

            $entity->set('provider', self::PROVIDER_NAME);

            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * Parse a person listing
     *
     * @param SimpleXMLElement $listing
     * @return array
     */
    public function _parseListing($listing){
        $identities = array();
        $identity = new stdClass();

        $person = $this->changeCaseRecursive($this->xml2array($listing));

        if (isset($person['listingType']) && isset($person['listingTypes']['Type']) && (string) $person['listingTypes']['Type'] == 'B') {
            $identity->business_name = (string) $person['listedName'];
            $identities[] = $identity;
        }
        else {
            if (isset($person['name'])) {
                $identity->identity = new stdClass();
                $identity->identity->name = (object) $person['name'];
                $identities[] = $identity;
            }
        }

        return $identities;
    }

    /**
     * Convert a simplexml object to an array
     *
     * @param SimpleXMLElement $xmlObject
     * @param array $out
     * @return array
     */
    public function xml2array ($xmlObject, $out = array()){
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = is_object($node) ? $this->xml2array($node) : $node;
        }

        return $out;
    }

    /**
     * Process the incoming location results
     *
     * @param SimpleXMLElement $results
     * @return array
     */
    protected function _parseLocationsResults($results) {
        $locations = array();

        foreach ($results->response->Records->Record as $record) {
            $location = $this->parseLocationRecord($record);

            if ($location) {
                $locations[] = $location;
            }
        }

        return $locations;
    }

    /**
     * Process the incoming teaser results
     *
     * @param SimpleXMLElement $results
     * @return array
     */
    protected function _parseTeaserResults($results) {
        $people = array();

        if ((int) $results->response->RecordCount == 0) {
            return $people;
        }

        foreach ($results->response->Records->Record as $record) {
            $person = $this->_parseTeaserRecord($record);

            if ($person) {
                $people[] = $person;
            }
        }

        return $people;
    }

    /**
     * Create location entity from record
     *
     * @param SimpleXMLElement $record
     * @return Icm_Entity_Location
     */
    protected function parseLocationRecord($record) {
        $location = $this->getEntityFactory('location')->create();
        $location->set('zip', (integer) $record->Address[0]->Zip5);
        $location->set('fullstreet', (string) $record->Address[0]->StreetNumber . ' ' .
                                     (string) $record->Address[0]->StreetName . ' ' .
                                     (string) $record->Address[0]->StreetSuffix);
        $location->set('uniqueid', (integer) $record->UniqueId);

        return $location;
    }

    /**
     * Create person entity from teaser record
     *
     * @param SimpleXMLElement $record
     * @return Icm_Entity_Person
     */
    public function _parseTeaserRecord($record) {
        $entity = $this->getEntityFactory('person')->create();
        $entity->set('person_id',       (string) $record->UniqueId);
        $entity->set('phone_number',    (string) $record->PhoneIndicator);
        $entity->set('first_name',      ucwords(strtolower((string) $record->Names->Name[0]->First)));
        $entity->set('middle_name',     ucwords(strtolower((string) $record->Names->Name[0]->Middle)));
        $entity->set('last_name',       ucwords(strtolower((string) $record->Names->Name[0]->Last)));

        if ($record->Names->Name->count() > 1){
            // Handle aliases
            $aliases = array();
            $counter = 0;

            foreach ($record->Names->Name as $name){
                if ($counter != 0){
                    // exclude first name given
                    $aliases[] = array(
                        'first_name'    =>  ucwords(strtolower((string) $name->First)),
                        'middle_name'   =>  ucwords(strtolower((string) $name->Middle)),
                        'last_name'     =>  ucwords(strtolower((string) $name->Last))
                    );
                }

                $counter++;
            }

            $entity->set('aliases', $aliases);
        }

        if ($record->Addresses->count()) {
            // doesn't look like teaser data has street
            $entity->set('street',  '');
            $entity->set('city',    ucwords(strtolower((string) $record->Addresses[0]->Address->City)));
            $entity->set('state',   (string) $record->Addresses[0]->Address->State);
            $entity->set('zip',     (string) $record->Addresses[0]->Address->Zip5);
        }

        if ($record->Addresses->Address->count() > 1){
            // multiple addresses have been returned; pass them along
            $priorAddresses = array();
            $counter = 0;

            foreach ($record->Addresses->Address as $prior){
                if ($counter != 0){
                    // exclude first address
                    // teaser data doesn't include street
                    $priorAddresses[] = array(
                        'street'    => '',
                        'city'      => ucwords(strtolower((string) $prior->City)),
                        'state'     => (string) $prior->State,
                        'zip'       => (string) $prior->Zip5,
                    );
                }

                $counter++;
            }

            $entity->set('priorAddresses', $priorAddresses);
        }

        if ($record->DOBs->count()) {
            $entity->set('age', (string) $record->DOBs->Item->Age);

            // standardize dob with other data sets:
            // yyyy/mm/dd (not yyyy-mm-dd)
            $entity->set('dob', (string) $record->DOBs->Item->DOB->Year . '/' . (string) $record->DOBs->Item->DOB->Month . '/' . (string) $record->DOBs->Item->DOB->Day);

            // make a yob from dob
            $entity->set('yob', (string) $record->DOBs->Item->DOB->Year);

        }

        if ($record->Relatives->count()) {
            $relatives = array();

            foreach ($record->Relatives->Relative as $relative) {
                $person = new Icm_Entity_Person();
                $person->set('first_name',  ucwords(strtolower((string) $relative->Name->First)));
                $person->set('middle_name', ucwords(strtolower((string) $relative->Name->Middle)));
                $person->set('last_name',   ucwords(strtolower((string) $relative->Name->Last)));
                $relatives[] = $person;
            }

            if (count($relatives)) {
                $entity->set('relatives', $relatives);
            }
        }

        return $entity;
    }

    /**
     * Create entities from a record
     *
     * @param SimpleXMLElement $record
     * @return Icm_Entity
     */
    public function _parseRecord($record) {
        if ((string) $record->ListingTypes->Type == 'B') {
            $entity = $this->getEntityFactory('business')->create();
            $entity->setBusinessName(ucwords(strtolower((string) $record->ListedName)));
        }
        else {
            $arr = $this->changeCaseRecursive(array('first' => (string) $record->Name->First,
                                                      'last' => (string) $record->Name->Last,
                                                      'middle' => (string) $record->Name->Middle));
            $entity = $this->getEntityFactory('person')->create();
            $entity->set('first_name', $arr['first']);
            $entity->set('last_name', $arr['last']);
            $entity->set('middle_name', $arr['middle']);
        }

        $entity->set('personId', (string) $record->UniqueId);

        if ($record->Address->count()) {
            $address = $record->Address;
            $entity->set('street', (string) $record->Address->StreetNumber . ' ' .
                                   ucwords(strtolower((string) $record->Address->StreetName)) . ' ' .
                                   ucwords(strtolower($record->Address->StreetSuffix)));
            $entity->set('city', ucwords(strtolower((string) $record->Address->City)));
            $entity->set('state', (string) $record->Address->State);
            $entity->set('zip', (string) $record->Address->Zip5);

            if (isset($address->UnitNumber)){
                $entity->set('unit', (string) $address->UnitNumber);
            }
        }

        if (isset($record->Gender)) {
            $entity->set('gender', (string) $record->Gender);
        }

        if (isset($record->Age)) {
            $entity->set('age', (string) $record->Age);
        }

        if (isset($record->DOB)) {
            $dob = '';

            if (isset($record->DOB->Month)){
                $dob = (string) $record->DOB->Month.'/';
            }

            if (isset($record->DOB->Day)) {
                $dob .= (string) $record->DOB->Day. '/';
            }

            $dob .= (string) $record->DOB->Year;
            $entity->set('dob', $dob);

            // make a yob from dob
            $entity->set('yob', (string) $record->DOB->Year);
        }

        if (isset($record->DOD)) {
            $dod = '';

            if (isset($record->DOD->Month)){
                $dod = (string) $record->DOD->Month . '/';
            }

            if (isset($record->DOD->Day)) {
                $dod .= (string) $record->DOD->Day . '/';
            }

            $dod .= (string) $record->DOD->Year;
            $entity->set('dod', $dod);

            // make a yod from death year
            $entity->set('yod', (string) $record->DOD->Year);
        }

        if (isset($record->PhoneInfo)){
            $phone = $record->PhoneInfo;
            $entity->set('phone', (string) $phone->Phone10);
        }

        if (isset($record->DateFirstSeen)){
            $entity->set('dateFirstSeen', $record->DateFirstSeen->Month . '/' . $record->DateFirstSeen->Year);
        }

        if (isset($record->DateLastSeen)){
            $entity->set('dateLastSeen', $record->DateLastSeen->Month . '/' . $record->DateLastSeen->Year);
            $entity->set('timestamp', $record->DateLastSeen->Month . '/01/' . $record->DateLastSeen->Year);
        }

        if (isset($entity)) {
            return $entity;
        }

        return false;
    }

    /**
     * @param $criteria
     */
    public function reversePhoneTeaser($criteria) {
        throw new Icm_Search_Adapter_Exception(__METHOD__ . ' is not implemented in ' . get_called_class());
    }

    /**
     * @param array $args
     */
    public function backgroundCheck($args = array()) {
        return $this->backgroundCheckTeaser($args);
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function backgroundCheckTeaser($criteria = array()) {
        $searchArray = array();
        $useId = array_key_exists('id', $criteria);

        if (!$useId) {

            foreach (array('last_name') as $required) {

                if (empty($criteria[$required])) {
                    throw new Icm_Search_Adapter_Exception("
                        Icm_Search_Adapter_Lexis requires a criteria of {$required} to do a
                        backgroundCheckTeaser search if an ID is not specified
                    ");
                }
            }
        }
        else {
            $searchArray['UniqueId'] = $criteria['id'];
        }

        $cacheKeyArgs = array_values($criteria);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);

        $searchArray['Name']['First'] = $criteria['first_name'];
        $searchArray['Name']['Last'] = $criteria['last_name'];

        if (isset($criteria['middle_name'])){
            $searchArray['Name']['Middle'] = $criteria['middle_name'];
        }

        if (isset($criteria['suffix'])){
            $searchArray['Name']['Suffix'] = $criteria['suffix'];
        }

        // Zip radius
        if (array_key_exists('zip_radius', $criteria)) {
            $searchArray['ZipRadius'] = $criteria['zip_radius'];
        }

        // Location
        if (array_key_exists('city', $criteria)) {
            $searchArray['Address']['City'] = $criteria['city'];
        }

        if (array_key_exists('state', $criteria)) {
            $searchArray['Address']['State'] = $criteria['state'];
        }

        if (array_key_exists('zip', $criteria)) {
            $searchArray['Address']['Zip5'] = $criteria['zip'];
        }

        // Age Range
        if (array_key_exists(Icm_Search_Engine::CRITERIA_AGE_LOW, $criteria)) {
            $searchArray['AgeRange']['Low'] = $criteria[Icm_Search_Engine::CRITERIA_AGE_LOW];
        }

        if (array_key_exists(Icm_Search_Engine::CRITERIA_AGE_HIGH, $criteria)) {
            $searchArray['AgeRange']['High'] = $criteria[Icm_Search_Engine::CRITERIA_AGE_HIGH];
        }

        // DOB
        if (array_key_exists('dob_day', $criteria)) {
            $searchArray['DOB']['Day'] = $criteria['dob_day'];
        }

        if (array_key_exists('dob_month', $criteria)) {
            $searchArray['DOB']['month'] = $criteria['dob_month'];
        }

        if (array_key_exists('dob_year', $criteria)) {
            $searchArray['DOB']['Year'] = $criteria['dob_year'];
        }

        $options = array('IncludeRelativeNames' => 'true', 'IncludeFullHistory' => 'true', 'ReturnCount' => '100', 'AlwaysReturnRecords' => 'true', 'UseNicknames' => 'true', 'UsePhonetics' => 'true');

        if (isset($criteria['UseNicknames']) && !$criteria['UseNicknames']){
            $options['UseNicknames'] = 'false';
        }

        if (isset($criteria['UsePhonetics']) && !$criteria['UsePhonetics']){
            $options['UsePhonetics'] = 'false';
        }

        $config =& $this->config;
        $lexis = $this->teaserApi;

        $fetchCallback = function() use ($searchArray, &$config, &$lexis, $options) {
            $results = $lexis->thinRollupPersonSearch($searchArray, $options);

            $people = array();

            if ((int) $results->response->RecordCount == 0) {
                return $people;
            }

            return $results->response->Records->Record;
        };

        $factory = $this->getEntityFactory('person');
        $entityCallback = function($record) use ($factory) {

            /**
             * @var Icm_Entity_Factory $factory
             */
            $entity = $factory->create();
            $entity->set('person_id', (string) $record->UniqueId);
            $entity->set('source', 'lexis');
            $entity->set('phone_number', (string) $record->PhoneIndicator);
            $entity->set('first_name',  ucwords(strtolower((string) $record->Names->Name[0]->First)));
            $entity->set('middle_name', ucwords(strtolower((string) $record->Names->Name[0]->Middle)));
            $entity->set('last_name',   ucwords(strtolower((string) $record->Names->Name[0]->Last)));

            if ($record->Names->Name->count() > 1){
                // Handle aliases
                $aliases = array();
                $counter = 0;

                foreach ($record->Names->Name as $name){

                    if ($counter != 0){
                        // exclude first name given
                        $aliases[] = array(
                            'first_name'    => ucwords(strtolower((string) $name->First)),
                            'middle_name'   => ucwords(strtolower((string) $name->Middle)),
                            'last_name'     => ucwords(strtolower((string) $name->Last))
                        );
                    }

                    $counter++;
                }
                $entity->set('aliases', $aliases);
            }

            if ($record->Addresses->count()) {
                // doesn't look like teaser data has street
                $entity->set('street', '');
                $entity->set('city',    ucwords(strtolower((string) $record->Addresses[0]->Address->City)));
                $entity->set('state',   (string) $record->Addresses[0]->Address->State);
                $entity->set('zip',     (string) $record->Addresses[0]->Address->Zip5);
            }

            if ($record->Addresses->Address->count() > 1){
                // multiple addresses have been returned; pass them along
                $priorAddresses = array();
                $counter = 0;

                foreach ($record->Addresses->Address as $prior){

                    if ($counter != 0){
                        // exclude first address
                        // teaser data doesn't include street
                        $priorAddresses[] = array(
                            'street'    => '',
                            'city'      => ucwords(strtolower((string) $prior->City)),
                            'state'     => (string) $prior->State,
                            'zip'       => (string) $prior->Zip5,
                        );
                    }

                    $counter++;
                }

                $entity->set('priorAddresses', $priorAddresses);
            }

            if ($record->DOBs->count()) {
                $entity->set('age', (string) $record->DOBs->Item->Age);

                // standardize dob with other data sets:
                // yyyy/mm/dd (not yyyy-mm-dd)
                $dobYear = (string) $record->DOBs->Item->DOB->Year;
                $dobMonth = (string) $record->DOBs->Item->DOB->Month;
                $dobDay = (string) $record->DOBs->Item->DOB->Day;

                $entity->set('dob', $dobYear . '/' . $dobMonth . '/' . $dobDay);

                // make a yob from dob
                $entity->set('yob', $dobYear);

            }

            if ($record->DODs->count()) {
                $entity->set('ageDead', (string) $record->DODs->Item->Age);

                // standardize dat -> yyyy/mm/dd (not yyyy-mm-dd)
                $dodYear = (string) $record->DODs->Item->DOD->Year;
                $dodMonth = (string) $record->DODs->Item->DOD->Month;
                $dodDay = (string) $record->DODs->Item->DOD->Day;

                $entity->set('dod', $dodYear . '/' . $dodMonth . '/' . $dodDay);
                $entity->set('yod', $dodYear);
            }

            if ($record->Relatives->count()) {
                $relatives = array();

                foreach ($record->Relatives->Relative as $relative) {
                    $person = new Icm_Entity_Person();
                    $person->set('first_name',  ucwords(strtolower((string) $relative->Name->First)));
                    $person->set('middle_name', ucwords(strtolower((string) $relative->Name->Middle)));
                    $person->set('last_name',   ucwords(strtolower((string) $relative->Name->Last)));
                    $person->set('person_id',   (string) $relative->UniqueId);
                    $relatives[] = $person;
                }

                if (count($relatives)) {
                    $entity->set('relatives', $relatives);
                }
            }

            $hash_values[] = $entity->person_id;

            return $entity;
        };

        $results = $this->_fetchAndCache(array(
                'cacheKey'       => $cacheKey,
                'fetchCallback'  => $fetchCallback,
                'entityCallback' => $entityCallback
        ));

        return $results;
    }
}
