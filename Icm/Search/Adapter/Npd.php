<?php

class Icm_Search_Adapter_Npd extends Icm_Search_Adapter_Abstract implements
    Icm_Search_Adapter_Interface_Backgroundcheck,
    Icm_Search_Adapter_Interface_Criminalsearch,
    Icm_Search_Adapter_Interface_Reversephone {

    const PROVIDER_NAME = 'NPD';

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $api;

    /**
     * @var Icm_Service_Npd_Db
     */
    protected $db;

    public function __construct(Icm_Service_Npd_Api $api, Icm_Service_Npd_Db $db) {
        $this->api = $api;
        $this->db = $db;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
     */
    public function isPaid($method){
        return false;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
     */
    public function isTeaser($method){
        switch ($method){
            case 'reversePhoneTeaser':
            case 'backgroundCheckTeaser':
                return true;
            default:
                return false;
        }
    }

    /**
     * this is a search algorithm that returns a set of person entity objects
     *
     * @param array $args
     * @return Icm_Entity
     */
    public function backgroundCheck($args=array()) {
        // TODO: Validate args: right now this takes a last name, first name and optional state
        if (isset($args['id'])){
            $args['person_id'] = $args['id'];
        }

        // remove all unallowed keys from the arguments array
        $allowedArgs = array('first_name', 'middle_name', 'last_name', 'state', 'person_id', 'city', 'zip');
        $allowedArgs = array_fill_keys($allowedArgs, 1);
        $args = array_intersect_key($args, $allowedArgs);

        // we can't do pagination at this level because Icm_Search_Engine is the interface for these functions
        // Icm_Search_Engine allows you to use multiple adapters and filter by object and other fancy stuff

        // check if this search is cached
        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);

        // this function gets the data - this is the actual background check search
        $npdAdapter = $this->db;

        $fetchCallback = function() use ($args, $npdAdapter) {
            // get the data from the db

            // for now just get all the person records that match the criteria
            return $npdAdapter->getPerson($args);
        };

        $factory = $this->getEntityFactory('person');

        $entityCallback = function($row) use($factory) {
            $entity = $factory->create();

            // fix the date format
            $row = Icm_Search_Adapter_Npd::correctNpdDateFormat($row);

            if (isset($row['validity_date']) && (!empty($row['validity_date']) && $row['validity_date'] != '')) {
                $pieces = str_split($row['validity_date'], 4);

                if (count($pieces) == 2){
                    $row['validity_date'] = $pieces[1] . '/' . $pieces[0];
                }
            }
            elseif (isset($row['validity_date']) && (empty($row['validity_date']) && $row['validity_date'] == '')) {
                $row['validity_date'] = '00/0000';
            }

            if (isset($row['city']) && (!empty($row['city']) && $row['city'] != '')) {
                $row['city'] = ucwords(strtolower($row['city']));
            }

            $hash_values[] = strtoupper($row['last_name']);
            $hash_values[] = strtoupper($row['address']);
            $hash_values[] = strtoupper($row['city']);
            $row['hash_key'] = sha1(serialize($hash_values));

            unset($hash_values);

            foreach ($row as $key => $value) {
                $entity->set($key, $value);
            }

            $entity->set('source', 'npd');
            $entity->formatData();

            return $entity;
        };

        $results = $this->_fetchAndCache(array(
            'cacheKey'       => $cacheKey,
            'fetchCallback'  => $fetchCallback,
            'entityCallback' => $entityCallback
        ));

        Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 4: '));

        // sort: dob_sort DESC, city ASC, validity_date ASC
        if (!empty($results)) {
            Icm_Util_Logger_Syslog::getInstance('api')->logDebug('sorting merged results');

            $results = Icm_Entity::orderBy($results,
                                           array(
                                               array('dob_sort', 'NUMERIC', 'DESC'),
                                               array('city', 'STRING', 'ASC'),
                                               array('validity_date', 'STRING', 'ASC')
                                           )
            );
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logElapsedTime(array('marker' => 'mark 5: '));
        return $results;
    }

    /**
     * Fix the date format coming in from Npd
     *
     * @param array $row
     * @return array
     */
    public static function correctNpdDateFormat($row) {
        // Gordo's NPD Date hackery:
        // seriously npd, wtf kind of formatting is yymmdd, what is someone was born in 1800s or 2000s?
        if (isset($row['dob']) && (!empty($row['dob']) && $row['dob'] != '')) {
            // strip non alpha chars
            // for some fucked up reason NPD has some results formatted like x0yymmdd
            $row['dob'] = preg_replace('/\D/', '', $row['dob']);

            // after stripping the x we get 0yymmdd
            if (strlen($row['dob']) == 7){
                $row['dob'] = substr($row['dob'], 1);

            }
            // sometimes we get yyyymmdd or or mmddyyyy
            elseif (strlen($row['dob']) == 8){
                $pieces = str_split($row['dob'], 2);

                // yyyymmdd
                if ($pieces[0] == 19) {
                    $row['dob'] = $pieces[1] . $pieces[2] . $pieces[3];
                }
                // mmddyyyy
                elseif ($pieces[2] == 19) {
                    $row['dob'] = $pieces[3] . $pieces[0] . $pieces[1];
                }
            }

            // x12
            // http://node.gordo4.com/snaps/14d0615508afb79ebe243ad96994e7bf.png what the fuck is this little guy?! let's null this fucker.
            if (strlen($row['dob']) < 6) {
                $row['dob'] = '';
            }
            else{
                // now we have a somewhat consistent cleaned up version of NPDs yymmdd, let's make it mm/dd/yyyy
                $pieces = str_split($row['dob'], 2);

                if (count($pieces) == 3){
                    // NPD uses YY format, not YYYY so let's prefix with 18 if YY > 94, so anything over 94 = 1895, 1896, etc, and anything under = 19, so like 1993, 1992, etc... fair compromise?
                    if ($pieces[0] > 94){
                        $prefix = 18;
                    }
                    else {
                        $prefix = 19;
                    };

                    $row['dob'] = $pieces[1] . '/' . $pieces[2] . '/' . $prefix.$pieces[0];
                }
                else{
                    $row['dob'] = '';
                }
            }
        }

        return $row;
    }

    public function personSearch(array $args = array()){
        return $this->backgroundCheck($args);
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface_Criminalsearch::criminalSearch()
     */
    public function criminalSearch(array $args = array()){
        // check if this search is cached
        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);

        // this function gets the data - this is the actual criminal records search
        $npdAdapter = $this->api;

        $fetchCallback = function() use ($args, $npdAdapter) {
            // for now just get all the criminal records that match the criteria
            return $npdAdapter->criminalSearch($args);
        };

        $factory = $this->getEntityFactory('criminal');

        $entityCallback = function(Icm_Struct_Criminalrecord $row) use ($factory) {
            $data = array();
            $hash_values[] = array();

            if (strlen($row->dob) == 8 && strpos($row->dob, '/') === false){
                // full date, but no slashes; assume YYYYMMDD
                $row->dob = substr($row->dob, 4, 2) . '/'.substr($row->dob, 6) . '/'.substr($row->dob, 0, 4);
            }

            $hash_values[] = strtoupper($row->last_name);
            $hash_values[] = strtoupper($row->case_number);
            $hash_values[] = strtoupper($row->offense_description1);
            $data['hash_key'] = sha1(serialize($hash_values));

            $entity = $factory->create();

            foreach ($data as $k => $v) {
                $entity->set($k, $v);
            }

            return $entity;
        };

        $results = $this->_fetchAndCache(array(
            'cacheKey'       => $cacheKey,
            'fetchCallback'  => $fetchCallback,
            'entityCallback' => $entityCallback
        ));

        return $results;
    }

    /**
     * @param array $criteria
     */
    public function sexOffenderSearch(array $criteria){

    }

    /**
     * @param $criteria
     */
    public function reversePhone($criteria) {
        if (!isset($criteria['phone'])) {
            throw new Icm_Search_Adapter_Exception("Icm_Search_Adapter_Npd requires a criteria of 'phone' to do a reversePhone search");
        }

        $this->detail = 1;

        // Cell or Unlisted Telephone Search
        $this->searchType = 'PFPHN';

        $reversePhoneCriteria = array(
            'firstname' => '',
            'lastname' => '',
            'state' => '',
            'phonenumber' => $criteria['phone']
        );

        $results = $this->api->reversePhone($reversePhoneCriteria);

        $entities = array();
        $skipList = array('state');

        foreach ($results as $listing) {

            $listing = $this->changeCaseRecursive($listing, $skipList);

            $location = new stdClass();
            $identities = new stdClass();
            $identities->identity = new stdClass();
            $identities->identity->name = new stdClass();
            $entity = $this->getEntityFactory('phone')->create();

            foreach ($listing as $key => $value) {
                switch ($key) {
                    case 'dob':
                        $date = new Icm_Util_DateHelper();
                        $date->setTimestamp(strtotime($value));
                        $identities->identity->dOB = $date;
                        break;
                    case 'personid':
                        $identities->identity->npdPersonId = $value;
                        break;
                    case 'firstname':
                        $identities->identity->name->first = $value;
                        break;
                    case 'middlename':
                        $identities->identity->name->middle = $value;
                        break;
                    case 'lastname':
                        $identities->identity->name->last = $value;
                        break;
                    case 'zip':
                        $identities->identity->zip = $value;
                        $location->zip = $value;
                        break;
                    case 'state':
                        $identities->identity->state = $value;
                        $location->state = $value;
                        break;
                    case 'city':
                        $identities->identity->city = $value;
                        $location->city = $value;
                        break;
                    case 'county':
                        $identities->identity->county = $value;
                        $location->county = $value;
                        break;
                    case 'address':
                        $identities->identity->street = $value;
                        $location->street = $value;
                        break;
                    case 'phone':
                        $identities->identity->phone = $value;
                        $entity->phone = $value;
                        $entity->phone_formatted = $this->phoneFormat($value);
                        break;
                    default:
                        $entity->set($key, $value);
                }
            }

            if (!$location->city && $location->county){
                $location->city = $location->county;
                $identities->identity->city = $identities->identity->county;
            }

            $entity->set('provider', self::PROVIDER_NAME);
            $entity->set('identities', array($identities));
            $entity->set('location', $location);
            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * @param array $args
     */
    public function backgroundCheckTeaser($args = array()) {
        // TODO: Implement backgroundCheckTeaser() method.
        throw new Icm_Search_Adapter_Exception('backgroundCheckTeaser() not implemented in Icm_Search_Adapter_Npd');
    }

    /**
     * @param $criteria
     */
    public function reversePhoneTeaser($criteria) {
        // TODO: Implement reversePhoneTeaser() method.
        throw new Icm_Search_Adapter_Exception('reversePhoneTeaser() not implemented in Icm_Search_Adapter_Npd');
    }
}
