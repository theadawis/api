<?php

/**
 * Database search adapter file
 *
 * @category Api
 * @version 1.0
 * @package Api
 * @subpackage Api_Search
 */

/**
 * Database search adapter class
 *
 * @category Api
 * @package Api
 * @subpackage Api_Search
 */
class Icm_Search_Adapter_DbPopularNames extends Icm_Search_Adapter_Abstract
{
    /**
     * @var Icm_Service_Internal_Db
     */
    protected $cgInternalDb;

    /**
     * @var integer
     */
    protected static $maxSearchResults = 5;

    /**
     * @param Icm_Db_Pdo $cgInternalDb
     */
    public function __construct(Icm_Db_Pdo $cgInternalDb = null) {
        $this->cgInternalDb = $cgInternalDb;
    }

    /**
     * do the pdo search here
     * @param array $criteria
     * @return array $result list of popular names
     */
    public function nameSearch(array $criteria=array()) {
        $result = array();
        if (!empty($criteria['name']) && !empty($criteria['type'])) {
            $maxResults = !empty($criteria['maxresults']) ?
                        $criteria['maxresults'] :
                        self::$maxSearchResults;
            $data = array();
            // valid criteria
            if ($criteria['type'] == 'firstname') {
                // firstnames table
                $sql = sprintf("SELECT name
                                FROM census_firstnames
                                WHERE name LIKE :name
                                ORDER BY rank
                                LIMIT %d", $maxResults);
            } else {
                // lastnames table
                $sql = sprintf("SELECT name
                                FROM census_lastnames
                                WHERE name LIKE :name
                                ORDER BY rank
                                LIMIT %d", $maxResults);
            }
            if ($data = $this->cgInternalDb->fetchAll($sql, array('name' => $criteria['name'] . '%'))) {
                // map result row to entity (array of Icm_Entity_PopularPerson objects)
                foreach ($data as $row) {
                    if ($_person = Icm_Entity_PopularPerson::create($row)) {
                        $result[] = $_person;
                    }
                }
                // cleanup
                unset($data);
            }
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
     */
    public function isPaid($method){
        return false;
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
     */
    public function isTeaser($method){
        return false;
    }

}

?>