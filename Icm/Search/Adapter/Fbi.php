<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/18/12
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Search_Adapter_Fbi extends Icm_Search_Adapter_Abstract {


    /**
     * @var Icm_Service_Fbi_Db
     */
    public $db;

    public function __construct(Icm_Service_Fbi_Db $db) {
        $this->db = $db;
    }

    function getCityStats($args = array()){

        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = "fbi_city" . Icm_Util_Cache::generateKey($cacheKeyArgs);

        if (isset($args['stateName']) && isset($args['cityName'])){

            $response = $this->db->getCityStats($args);
            return $response;
        }
        return array();
    }

    function getStateStats($args = array()){

        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = "fbi_state" . Icm_Util_Cache::generateKey($cacheKeyArgs);

        if (isset($args['stateName'])){

            $response = $this->db->getStateStats($args);
            return $response;
        }
        return array();
    }

    function getCountryStats($args = array()){

        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = "fbi_country" . Icm_Util_Cache::generateKey($cacheKeyArgs);

        $response = $this->db->getCountryStats($args);
        return $response;
    }

    function getNotes($args = array()){

        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = "fbi_notes" . Icm_Util_Cache::generateKey($cacheKeyArgs);


        $response = $this->db->getNotes($args);
        return $response;
    }

    // stupid ass does nothing functions
    public function isPaid($method){
        return false;
    }

    public function isTeaser($method){
        return false;
    }
}
