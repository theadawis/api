<?php
/**
 * @author Joe Linn <br/>
 *
 */
interface Icm_Search_Adapter_Interface{
    /**
     * @param string $method
     * @return boolean true if $method is a paid search; false otherwise
     */
    public function isPaid($method);

    /**
     * @param string $method
     * @return boolean true if $method is a teaser search; false otherwise
     */
    public function isTeaser($method);
}