<?php

/**
 * Database search adapter class
 *
 */
class Icm_Search_Adapter_Census extends Icm_Search_Adapter_Abstract
{
    /**
     * @var Icm_Service_Internal_Db
     */
    protected $db;

    public function __construct(Icm_Service_Census_Db $db = null) {
        $this->db = $db;
    }

    /**
     * Do the pdo search here
     * @param string|array $zip
     * @return array
     */
    public function getByZip($zip) {
        $result = array();

        if (empty($zip)) {
            return $result;
        }

        if (is_array($zip) && isset($zip['zip'])) {
            $zip = $zip['zip'];
        }

        $result = $this->db->getByZip($zip);

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
     */
    public function isPaid($method){
        return false;
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
     */
    public function isTeaser($method){
        return true;
    }
}
