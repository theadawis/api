<?php
/**
 *
 */
class Icm_Search_Adapter_Whitepages extends Icm_Search_Adapter_Abstract implements Icm_Search_Adapter_Interface_Reversephone
{

    const PROVIDER_NAME = 'WHITEPAGES';

    protected $api;

    public function __construct(Icm_Service_Whitepages_Api $whitepagesApi) {
        $this->api = $whitepagesApi;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
    */
    public function isPaid($method){
        return true;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
    */
    public function isTeaser($method){
        return false;
    }

    /**
     * @param $criteria
     * @return array|string|void
     * @throws Icm_Search_Adapter_Exception
     */
    public function reversePhone($criteria) {
        if (!isset($criteria['phone'])) {
            throw new Icm_Search_Adapter_Exception("Icm_Search_Adapter_Whitepages requires a criteria of 'phone' to do a reversePhone search");
        }

        $this->api->setMode(Icm_Service_Whitepages_Api::MODE_PAID);
        $this->api->setVersion('1.0');
        $this->api->setOutput(Icm_Service_Whitepages_Api::OUTPUT_JSON);

        $results = $this->api->reversePhone($criteria['phone']);

        $results = $this->_parseResults($results);

        return $results;
    }

    /**
     * @param mixed $results as JSON or XML
     * @return array
     */
    protected function _parseResults($results){
        $entities = array();

        if (empty($results['listings'][0]['people']) && empty($results['listings'][0]['business'])) {
            return $entities;
        }

        $skipList = array('state');

        foreach ($results['listings'] as $listing) {
            $listing = $this->changeCaseRecursive($listing, $skipList);

            $people = $this->_parseListing($listing);

            $entity = $this->getEntityFactory('phone')->create();

            // pull out the people info
            if ($people) {
                $entity->set('identities', $people);
                unset($listing['people']);
            }

            // pull out the address info
            if (isset($listing['address']) && !empty($listing['address'])) {
                $address = $listing['address'];
                $location = array_merge(array(), $address);
                $location['street'] = isset($address['fullstreet']) ? $address['fullstreet'] : '';
                $location['zip5'] = isset($address['zip']) ? $address['zip'] : '';
                $location['geodata'] = $listing['geodata'];

                $entity->set('location', $location);
                unset($listing['address']);
            }

            // pull out the phone info
            if (isset($listing['phonenumbers']) && is_array($listing['phonenumbers'])) {
                $phoneNumbers = $listing['phonenumbers'];

                foreach ($phoneNumbers[0] as $phKey => $phValue) {
                    $entity->set($phKey, $phValue);
                }

                $entity->set('ltype', $phoneNumbers[0]['type']);

                if (array_key_exists('areacode', $phoneNumbers[0])
                    && array_key_exists('exchange', $phoneNumbers[0])
                    && array_key_exists('linenumber', $phoneNumbers[0])) {

                    $phoneUnformatted = $phoneNumbers[0]['areacode']
                        . $phoneNumbers[0]['exchange']
                        . $phoneNumbers[0]['linenumber'];
                    $entity->set('phone', $phoneUnformatted);
                    $entity->set('phone_formatted', $this->phoneFormat($phoneUnformatted));
                }
                elseif (array_key_exists('carrier', $phoneNumbers[0])) {
                    $entity->set('carrier', $phoneNumbers[0]['carrier']);
                }
                elseif (array_key_exists('type', $phoneNumbers[0])) {
                    $entity->set('ltype', $phoneNumbers[0]['type']);
                }

                unset($listing['phonenumbers']);
            }

            // all the rest
            foreach ($listing as $key => $value) {
                $entity->set($key, $value);
            }

            $entity->set('provider', self::PROVIDER_NAME);

            $entities[] = $entity;
        }

        return $entities;
    }

    /**
     * @param $listing
     * @return bool|Icm_Entity_Business|Icm_Entity_Person
     */
    protected function _parseListing($listing){
        $identities = array();

        if (array_key_exists('people', $listing)) {
            foreach ($listing['people'] as $person) {
                $identity = new stdClass();
                $identity->identity = new stdClass();
                $identity->identity->name = new stdClass();
                $identity->identity->name->first   = $person['firstname'];
                $identity->identity->name->middle  = isset($person['middlename']) ? $person['middlename'] : '';
                $identity->identity->name->last    = $person['lastname'];

                unset($person['firstname']);
                unset($person['middlename']);
                unset($person['lastname']);

                foreach ($person as $personProperty => $personValue) {
                    $identity->identity->{$personProperty} = $personValue;
                }

                $identities[] = $identity;
            }
        }
        elseif (array_key_exists('business', $listing)) {
            $identity = new stdClass();
            $identity->identity = new stdClass();
            $identity->identity->business_name = $listing['business']['businessname'];
            $identities[] = $identity;
        }

        return $identities;
    }

    /**
     * @param $criteria
     */
    public function reversePhoneTeaser($criteria) {
        throw new Icm_Search_Adapter_Exception(__CLASS__ . 'does not implement ' . __METHOD__);
    }
}
