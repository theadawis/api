<?php
/**
 * @author Joe Linn
 */
class Icm_Search_Adapter_Elasticsearch extends Icm_Search_Adapter_Abstract implements Icm_Search_Adapter_Interface_Backgroundcheck{
    /**
     * @var Icm_Service_Internal_Elasticsearch
     */
    protected $elasticsearch;

    public function __construct(Icm_Service_Internal_Elasticsearch $elasticsearch){
        $this->elasticsearch = $elasticsearch;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
     */
    public function isPaid($method){
        // It's Elasticsearch. Free queries for everybody!
        return false;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
    */
    public function isTeaser($method){
        switch($method){
            case 'backgroundCheck': //change this if we start using this for full background checks
            case 'backgroundCheckTeaser': return true;
            break;
            default: return false;
            break;
        }
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface_Backgroundcheck::backgroundCheckTeaser()
     */
    public function backgroundCheckTeaser($criteria = array()){
        $cacheKeyArgs = array_values($criteria);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);
        $query = new Icm_Service_Internal_Elasticsearch_Query();
        $options = NULL;

        if (isset($criteria['limit']) && is_numeric($criteria['limit'])){
            $query->setLimit($criteria['limit']);
        }

        // name
        if (isset($criteria['first_name'])){
            $query->addFilter(new Elastica_Filter_Terms('name.first', array(strtolower($criteria['first_name']))));
        }
        if (isset($criteria['last_name'])){
            $query->addFilter(new Elastica_Filter_Terms('name.last', array(strtolower($criteria['last_name']))));
        }
        if (isset($criteria['middle_name'])){
            $query->addFilter(new Elastica_Filter_Terms('name.middle', array(strtolower($criteria['middle_name']))));
        }

        // location
        if (isset($criteria['city'])){
            $query->addFilter(new Elastica_Filter_Terms('address.city', array(strtolower($criteria['city']))));
        }
        if (isset($criteria['state']) && strtolower($criteria['state']) != 'all'){
            $options = array('routing' => strtoupper($criteria['state']));
            $query->addFilter(new Elastica_Filter_Terms('address.state', array(strtoupper($criteria['state']))));
        }
        if (isset($criteria['zip'])){
            $query->addFilter(new Elastica_Filter_Terms('address.zip', array(strtolower($criteria['zip']))));
        }

        // age
        if (array_key_exists(Icm_Search_Engine::CRITERIA_AGE_LOW, $criteria)){
            $query->addFilter(new Elastica_Filter_Range('age', array('from' => $criteria[Icm_Search_Engine::CRITERIA_AGE_LOW], 'include_lower' => true)));
        }
        if (isset($criteria[Icm_Search_Engine::CRITERIA_AGE_HIGH])){
            $query->addFilter(new Elastica_Filter_Range('age', array('to' => $criteria[Icm_Search_Engine::CRITERIA_AGE_HIGH], 'include_upper' => true)));
        }
        $elasticsearch = $this->elasticsearch;

        $fetchCallback = function() use ($query, $options, &$elasticsearch){
            return $elasticsearch->search($query, $options)->getResults(); //TODO: specify index
        };

        $factory = $this->getEntityFactory('person');

        $entityCallback = function(Elastica_Result $record) use ($factory) {
            /**
             * @var Icm_Entity_Factory $factory
             */
            $entity = $factory->create();
            $entity->set('person_id', (string) $record->person_id);
            $entity->set('first_name', (string) $record->name['first']);
            $entity->set('middle_name', (string) $record->name['middle']);
            $entity->set('last_name', (string) $record->name['last']);

            if (sizeof($record->aliases) > 0){
                $aliases = array();
                foreach ($record->aliases as $alias){
                    $aliases[] = array(
                        'first_name' => $alias['first'],
                        'middle_name' => $alias['middle'],
                        'last_name' => $alias['last']
                    );
                }
                $entity->set('aliases', $aliases);
            }

            $entity->set('street', '');
            $entity->set('city', (string) $record->address['city']);
            $entity->set('state', (string) $record->address['state']);
            $entity->set('zip', (string) $record->address['zip']);
            $addresses = array();
            foreach ($record->priorAddresses as $prior){
                $addresses[] = array(
                    'street' => '',
                    'city' => $prior['city'],
                    'state' => $prior['state'],
                    'zip' => $prior['zip']
                );
            }
            $entity->set('priorAddresses', $addresses);
            $entity->set('age', (string) $record->age);
            $entity->set('dob', $record->dob);
            $entity->set('yob', $record->yob);
            $entity->set('phone_number', '');
            $relatives = array();
            foreach ($record->relatives as $relative){
                $person = new Icm_Entity_Person();
                $person->set('first_name', (string)$relative['name']['first']);
                $person->set('middle_name', (string)$relative['name']['middle']);
                $person->set('last_name', (string)$relative['name']['last']);
                $person->set('person_id', (string)$relative['person_id']);
                $relatives[] = $person;
            }
            $entity->set('relatives', $relatives);

            return $entity;
        };

        $results = $this->_fetchAndCache(array(
                'cacheKey'       => $cacheKey,
                'fetchCallback'  => $fetchCallback,
                'entityCallback' => $entityCallback
        ));
        return $results;
    }

    public function backgroundCheck($args = array()){
        return $this->backgroundCheckTeaser($args);
        // throw new Exception("The backroundCheck() method has not been implemented for the Elasticsearch search adapter.");
    }
}
