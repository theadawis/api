<?php

/**
 *
 */
class Icm_Search_Adapter_Internal extends Icm_Search_Adapter_Abstract implements
      Icm_Search_Adapter_Interface_Optout {

    /**
     * @var Icm_Service_Internal_Db
     */
    protected $checkmateDb;

    /**
     * @var Icm_Service_Internal_Optout
     */
    protected $cgDb;

    /**
     * @var Icm_Redis
     */
    protected $redisConn;

    public function __construct(Icm_Service_Internal_Optout $cgDb, Icm_Service_Internal_Db $checkmateDb = null, $redisConn = null) {
        $this->cgDb = $cgDb;
        $this->checkmateDb = $checkmateDb;
        $this->redisConn = $redisConn;

    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isPaid()
    */
    public function isPaid($method){
        return false;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Adapter_Interface::isTeaser()
    */
    public function isTeaser($method){
        return false;
    }

    /**
     * this is a search algorithm that returns a set of person entity objects derived from the opt outs table
     *
     * @param array $args
     * @return $result
     */
    public function optout($args = array(), $personIds = null) {

        // check if this search is cached
        $cacheKeyArgs = array_values($args);
        array_unshift($cacheKeyArgs, __METHOD__);
        $cacheKey = Icm_Util_Cache::generateKey($cacheKeyArgs);

        // fetchCallback
         $cgDbAdapter = $this->cgDb;
         $checkmateDbAdapter = $this->checkmateDb;
         $redisConn = $this->redisConn;

        $fetchCallback = function() use ($args, $cgDbAdapter, $checkmateDbAdapter, $personIds, $redisConn) {

            $optouts = array();

            if ( is_array($personIds) ) {

                 // set up the redis pipline
                $pipe = $redisConn->pipeline();

                 // loop through all the personIds
                foreach ($personIds as $personId) {
                    $pipe->sismember('optouts_set', $personId);
                }

                 // execute that no good pipe
                $redisResults = $pipe->execute();

                 // loop through all the personIds
                foreach ($redisResults as $key => $booleanFlag) {
                    if ($booleanFlag){
                        $optouts[] = array('external_person_id' => $personIds[$key]);
                    }
                }

            } else {

                 // get all the records that match the criteria from the new optout structure via query params
                $optouts = $cgDbAdapter->getOptouts($args);
            }

             // if we found optouts via the new structure then just return them
            // if ( count($optouts) > 0 ) {
            //    return $optouts;
            // }

             // get data from the old site_optouts table if there are no results from the new structure
            // if ( !is_null($checkmateDbAdapter) ) {
            //    $optouts = $checkmateDbAdapter->getOptouts($args);
            // }

            return $optouts;
        };

        $factory = $this->getEntityFactory('person_optout');
        $entityCallback = function($row) {
            return Icm_Entity_Person_Optout::create($row);
        };

        return $this->_fetchAndCache(array(
                                           'cacheKey'       => $cacheKey,
                                           'fetchCallback'  => $fetchCallback,
                                           'entityCallback' => $entityCallback
                                           ));

    }

    public function backgroundCheck($args = array(), $personIds = null){
        return $this->optout($args, $personIds);
    }

    public function reversephone($args = array(), $personIds = null){
        return $this->optout($args, $personIds);
    }

    public function comprehensiveSearch($args = array(), $personIds = NULL){
        return $this->optout($args, $personIds);
    }

}