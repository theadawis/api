<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Search_Query implements Icm_Search_Query_Interface{
    protected $criteria = array();
    protected $searchType;
    protected $filters = array();
    protected $allowPaid = false;

    public function __construct(array $criteria, $searchType, $allowPaid = false){
        $this->criteria = $criteria;
        $this->searchType = $searchType;
        $this->allowPaid = $allowPaid;
    }

    public function setSearchType($type){
        $this->searchType = $type;
    }

    public function allowPaid() {
        return $this->allowPaid;
    }

    public function setAllowPaid($flag) {
        $this->allowPaid = $flag;
    }

    public function setCriteria(array $criteria){
        $this->criteria = $criteria;
    }

    /** (non-PHPdoc)
     * @see Icm_Search_Query_Interface::addCriteria()
     */
    public function addCriteria($name, $value){
        $this->criteria[$name] = $value;
    }

    /** (non-PHPdoc)
     * @see Icm_Search_Query_Interface::getCriteria()
     */
    public function getCriteria(){
        return $this->criteria;
    }

    /**
     * Returns a single criterion from the current Query
     * @param string $name
     * @return multitype:|NULL
     */
    public function getCriterion($name){
        if (isset($this->criteria[$name])){
            return $this->criteria[$name];
        }
        return NULL;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::getSearchType()
     */
    public function getSearchType(){
        return $this->searchType;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::getFilters()
     */
    public function getFilters(){
        return $this->filters;
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::addFilter()
     */
    public function addFilter($type, $value, $action = self::FILTER_ACTION_EXCLUDE){
        $this->filters[] = array($type, $value, $action);
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::hasA()
     */
    public function hasA($value, $action = self::FILTER_ACTION_EXCLUDE){
        $this->addFilter(self::FILTER_HAS_A, $value, $action);
    }


    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::isA()
     */
    public function isA($type, $action = self::FILTER_ACTION_EXCLUDE){
        $this->addFilter(self::FILTER_IS_A, $type, $action);
    }

    /* (non-PHPdoc)
     * @see Icm_Search_Query_Interface::hasHash()
     */
    public function hasHash($hash, $action = self::FILTER_ACTION_EXCLUDE){
        $this->addFilter(self::FILTER_HAS_HASH, $hash, $action);
    }

    public function clearFilters(){
        $this->filters = array();
    }

    /**
     * Filters stored search criteria; attempts to separate middle names and suffixes for use in search.
     * Altered search criteria are stored internally, but are also returned for your convenience.
     * @return array of search criteria as altered by this function
     */
    function filterCriteria(){
        $prefixes = array('dr', 'mr', 'ms', 'mrs');
        $suffixes = array('jr', 'sr', 'esq', 'ii', 'iii', 'iv');
        $affixes = array_merge($prefixes, $suffixes);
        $searchData = $this->criteria;
        foreach ($searchData as $key => &$data){
            $data = strtolower(urldecode(trim($data)));
            $data = str_replace(' . ', '', $data); //remove periods
            $data = str_replace(', ', '', $data); //remove commas
            foreach ($affixes as $affix){
                // get rid of affixes
                if (($pos = strpos($data, $affix)) !== false){
                    if (in_array($affix, $suffixes) && ctype_space($data[$pos - 1])){
                        // current affix is a suffix; pass it along with search criteria
                        $this->criteria['suffix'] = $affix;
                        $data = str_replace($affix, '', $data);
                    }
                    else if (in_array($affix, $prefixes) && $pos == 0 && ctype_space($data[$pos + strlen($affix)])){
                        $data = str_replace($affix, '', $data);
                    }
                }
            }
            $data = trim($data);
            if (strpos($data, ' ') !== false){
                // get rid of middle names / initials and suffixes
                if ($key == 'first_name'){
                    $data = explode(' ', $data);
                    if (isset($data[1])){
                        $this->criteria['middle_name'] = $data[1];
                    }
                    $data = $data[0];
                }
                else if ($key == 'last_name'){
                    $data = explode(' ', $data);
                    $data = end($data);
                }
            }
        }
        $this->criteria = array_merge($this->criteria, $searchData);
        return $this->criteria;
    }
}
