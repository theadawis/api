<?php
class Icm_OAuth2_Refreshtoken extends Icm_Struct{
    /**
     * @var string
     */
    public $clientID;

    /**
     * @var int Expiration time (unix timestamp)
     */
    public $expires;

    /**
     * @var string Space separated scopes for this token
     */
    public $scope;

    /**
     * @var string
     */
    public $token;
}