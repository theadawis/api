<?php
class Icm_OAuth2_Redirectexception extends Icm_OAuth2_Exception{
    protected $redirectUri;

    public function __construct($redirectUri, $error, $description = NULL, $state = NULL){
        parent::__construct(Icm_OAuth2::HTTP_FOUND, $error, $description);
        $this->redirectUri = $redirectUri;
        if ($state){
            $this->errorData['state'] = $state;
        }
    }

    protected function sendHeaders(){
        header('Location: ' . $this->buildUri($this->redirectUri, array('query' => $this->errorData)));
        exit;
    }

    protected function buildUri($uri, $params){
        $parsed = parse_url($uri);
        foreach ($params as $key => $value){
            if (isset($parsed[$key])){
                $parsed[$key] .= '&'.http_build_query($value);
            }
            else{
                $parsed[$key] = http_build_query($value);
            }
        }
        extract($parsed);
        return (isset($scheme) ? $scheme.'://' : '') . (isset($user) ? $user . (isset($pass) ? ':' . $pass : '') . '@' : '') .
            (isset($host) ? $host : '') . (isset($port) ? ':' . $port : '') . (isset($path) ? $path : '') . (isset($query) ? '?' . $query : '') .
            (isset($fragment) ? '#' . $fragment : '');
    }
}