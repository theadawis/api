<?php
class Icm_OAuth2_Authcode extends Icm_Struct{
    /**
     * @var string
     */
    public $clientID;

    /**
     * @var string URI for redirection of users for the current client
     */
    public $redirectUri;

    /**
     * @var int Expiration time (unix timestamp) of this auth code
     */
    public $expires;

    /**
     * @var string Scope for the current auth code
     */
    public $scope;

    /**
     * @var string
     */
    public $code;
}