<?php
class Icm_OAuth2_Accesstoken extends Icm_Struct{
    /**
     * @var string
     */
    public $clientID;

    /**
     * @var string
     */
    public $userID;

    /**
     * @var int Expiration time (unix timestamp) for this access token
     */
    public $expires;

    /**
     * @var string Scope associated with this access token
     */
    public $scope;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $type;

    public $permanent;
}