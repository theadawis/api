<?php
interface Icm_OAuth2_Storage_Grantcode extends Icm_OAuth2_Storage_Interface{
    const RESPONSE_TYPE_CODE = Icm_OAuth2::RESPONSE_TYPE_AUTH_CODE;

    /**
     * Retrieve an authorization code.
     * @param string $code
     * @return Icm_OAuth2_Authcode if a valid code is provided; NULL otherwise
     */
    public function getAuthCode($code);

    /**
     * Store an authorization code.
     * @param string $code Authorization code
     * @param string $clientID
     * @param string $userID
     * @param string $redirectUri URI to which the user will be redirected
     * @param int $expires Expiration time (unix timestamp) of the code
     * @param string $scope Space separated list of valid scopes for this auth code
     */
    public function setAuthCode($code, $clientID, $userID, $redirectUri, $expires, $scope = NULL);
}