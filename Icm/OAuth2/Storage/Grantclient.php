<?php
interface Icm_OAuth2_Storage_Grantclient extends Icm_OAuth2_Storage_Interface{
    /**
     * Implement this to use GRANT_TYPE_CLIENT_CREDENTIALS
     * @param string $clientID
     * @param string $secret
     * @return boolean true or array('scope' => scope string) if client credentials are valid; false otherwise
     */
    public function checkClientCredentialsGrant($clientID, $secret);
}