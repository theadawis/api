<?php
interface Icm_OAuth2_Storage_Interface{
    /**
     * Ensure validity of client credentials.
     * @param string $clientID
     * @param string $secret
     * @return boolean true if credentials are valid; false otherwise
     */
    public function checkClientCredentials($clientID, $secret = NULL);

    /**
     * Get stored information for given client ID
     * @param string $clientID
     * @return array('redirect_uri' => URI) on success; boolean false if client ID is invalid
     */
    public function getClientDetails($clientID);

    /**
     * Get an access token from storage.
     * @param string $token
     * @return Icm_OAuth2_Accesstoken on success; NULL if the token is invalid
     */
    public function getAccessToken($token);

    /**
     * Store an access token.
     * @param string $token Access token
     * @param string $clientID
     * @param string $userID
     * @param int $expires
     * @param string $scope
     */
    public function setAccessToken($token, $clientID, $userID, $expires, $scope = NULL);

    /**
     * Check if the given client is allowed to use the given grant type.
     * @param string $clientID
     * @param string $grantType
     * @return boolean true if grant type is permitted for this client; false otherwise
     */
    public function checkGrantType($clientID, $grantType);
}