<?php
interface Icm_OAuth2_Storage_Refreshtoken extends Icm_OAuth2_Storage_Interface{
    /**
     * Implement this to support GRANT_TYPE_REFRESH_TOKEN
     * @param string $token
     * @return Icm_OAuth2_Refreshtoken on successful retrieval; NULL otherwise
     */
    public function getRefreshToken($token);

    /**
     * Store a refresh token.
     * @param string $token
     * @param string $clientID
     * @param string $userID
     * @param int $expires Unix timestamp
     * @param string $scope
     */
    public function setRefreshToken($token, $clientID, $userID, $expires, $scope = NULL);

    /**
     * Expire the givne refresh token.
     * @param string $token
     */
    public function expireRefreshToken($token);
}