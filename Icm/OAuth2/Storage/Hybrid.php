<?php
class Icm_OAuth2_Storage_Hybrid implements Icm_OAuth2_Storage_Interface, Icm_OAuth2_Storage_Grantclient{
    const REDIS_PREFIX = 'icm_api_';
    const ACCESS_TOKEN_PREFIX = 'accesstoken_';

    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    /**
     * @var Icm_Redis
     */
    protected $redis;

    public function __construct(Icm_Db_Pdo $db, Icm_Redis $redis){
        $this->db = $db;
        $this->redis = $redis;
    }

    /* (non-PHPdoc)
     * @see Icm_OAuth2_Storage_Interface::checkClientCredentials()
     */
    public function checkClientCredentials($clientID, $secret = NULL){
        $stored = $this->db->fetch("SELECT id, secret, scope, permanent FROM api_clients WHERE id=:clientID;", array(':clientID' => $clientID));
        if (!sizeof($stored)){
            // nothing retrieved
            return false;
        }
        if (is_null($secret)){
            // no secret provided to check against, and we got a client record back
            return true;
        }
        if ($secret === $stored['secret']){
            return Icm_OAuth2_Clientstruct::fromArray($stored);
        }
        return false;
    }

    /* (non-PHPdoc)
     * @see Icm_OAuth2_Storage_Interface::getClientDetails()
     */
    public function getClientDetails($clientID){
        $stored = $this->db->fetch("SELECT * FROM api_clients WHERE id=:clientID;", array(':clientID' => $clientID));
        if (!sizeof($stored)){
            return NULL;
        }
        $stored['redirectUri'] = $stored['redirect_uri'];
        unset($stored['redirect_uri']);
        $stored = Icm_OAuth2_Clientstruct::fromArray($stored);
        return $stored;
    }

    public function getAccessToken($token){
        $pipe = $this->redis->pipeline();
        $pipe->hgetall(self::REDIS_PREFIX.self::ACCESS_TOKEN_PREFIX.$token);
        $pipe->ttl(self::REDIS_PREFIX.self::ACCESS_TOKEN_PREFIX.$token);
        $data = $pipe->execute();
        if (isset($data[0]['permanent']) && $data[0]['permanent'] == 1){
            // permanent token; reset expiration time
            $this->redis->expire(self::REDIS_PREFIX.self::ACCESS_TOKEN_PREFIX.$token, 7776000);
        }
        $token = $data[0];
        $token['expires'] = time() + $data[1];
        // $token = $this->redis->hgetall(self::REDIS_PREFIX.self::ACCESS_TOKEN_PREFIX.$token);
        if (is_null($token)){
            return NULL;
        }
        return Icm_OAuth2_Accesstoken::fromArray($token);
    }

    public function setAccessToken($token, $clientID, $userID, $expires, $scope = NULL){
        $token = self::REDIS_PREFIX.self::ACCESS_TOKEN_PREFIX.$token;
        $pipe = $this->redis->pipeline();
        if ($expires == 0){
            // permanent token
            $pipe->hmset($token, 'clientID', $clientID, 'userID', $userID, 'scope', $scope, 'permanent', 1);
            $expires = 7776000; //3 months
        }
        else{
            $pipe->hmset($token, 'clientID', $clientID, 'userID', $userID, 'scope', $scope, 'permanent', 0);
        }
        $pipe->expire($token, $expires);
        $pipe->execute();
    }

    public function checkGrantType($clientID, $grantType){
        return true; //TODO: implement permissions by grant type
    }

    // implementing Icm_OAuth2_Storage_Grantclient
    public function checkClientCredentialsGrant($clientID, $secret){
        if ($this->checkClientCredentials($clientID, $secret)){
            return $this->getClientDetails($clientID);
        }
        return false;
    }
}