<?php
interface Icm_OAuth2_Storage_Grantuser extends Icm_OAuth2_Storage_Interface{
    /**
     * Implement this to support GRANT_TYPE_USER_CREDENTIALS
     * @param string $clientID
     * @param string $username
     * @param string $password
     * @return boolean true or array('scope' => scope string) if credentials are valid; false otherwise
     */
    public function checkUserCredentials($clientID, $username, $password);
}