<?php
class Icm_OAuth2_Authexception extends Icm_OAuth2_Exception{
    protected $header;

    /**
     * @param string $httpCode
     * @param string $tokenType
     * @param string $realm
     * @param string $error
     * @param string $description
     * @param string $scope
     */
    public function __construct($httpCode, $tokenType, $realm, $error, $description = NULL, $scope = NULL){
        parent::__construct($httpCode, $error, $description);
        if ($scope){
            $this->errorData['scope'] = $scope;
        }
        $this->header = "WWW-Authenticate: ".ucwords($tokenType)." realm=\"{$realm}\"";
        foreach ($this->errorData as $key => $value){
            $this->header .= ', ' . $key.'="' . $value.'"';
        }
    }

    protected function sendHeaders(){
        header($this->header);
    }
}