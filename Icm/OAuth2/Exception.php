<?php
class Icm_OAuth2_Exception extends Exception{
    protected $httpCode;
    protected $errorData = array();

    /**
     * @param string $httpCode
     * @param string $error
     * @param string $description
     */
    public function __construct($httpCode, $error, $description = NULL){
        parent::__construct($error);
        $this->httpCode = $httpCode;
        $this->errorData['error'] = $error;
        if ($description){
            $this->errorData['description'] = $description;
        }
    }

    public function getDescription(){
        if (isset($this->errorData['description'])){
            return $this->errorData['description'];
        }
        return NULL;
    }

    public function getHttpCode(){
        return $this->httpCode;
    }

    public function sendHttpResponse(){
        header('HTTP/1.1 ' . $this->httpCode);
        $this->sendHeaders();
        echo (string)$this;
        exit;
    }

    protected function sendHeaders(){
        header('Content-Type: application/json');
        header('Cache-Control: no-store');
    }

    public function __toString(){
        return json_encode($this->errorData);
    }
}