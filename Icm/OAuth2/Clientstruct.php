<?php
class Icm_OAuth2_Clientstruct extends Icm_Struct{
    public $id;

    public $secret;

    public $scope;

    public $redirectUri;

    public $permanent;
}