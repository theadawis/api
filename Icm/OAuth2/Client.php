<?php
class Icm_OAuth2_Client{
    protected $accessToken = NULL;

    protected $defaultConfig = array(
        'id' => 'tester',
        'secret' => 'itsasecret',
        'baseUri' => 'https://rest.jlinn.local.icm/'
    );
    protected $config = array();

    public function __construct(Icm_Config $config = NULL){
        if (is_null($config)){
            $config = Icm_Config::fromArray(array());
        }
        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $config->getOption($key, $default);
        }
    }

    public function getAccessToken($grantType = 'client_credentials'){
        $uri = $this->config['baseUri'] . 'accesstoken';
        $http = new Icm_Util_Http_Client();
        $url = $http->buildUrlString($uri, array('grant_type' => $grantType));
        $http->setUrl($url);
        $http->setCurlOpt(CURLOPT_USERPWD, $this->config['id'] . ':' . $this->config['secret']);
        $data = json_decode($http->execute(), true);
        $this->accessToken = $data['access_token'];
    }

    public function api($path, array $params = array(), $method = 'GET'){
        if ($this->accessToken == NULL){
            $this->getAccessToken();
        }
        $url = $this->config['baseUri'] . $path;
        $http = new Icm_Util_Http_Client();
        $url = $http->buildUrlString($url, $params);
        $http->setCurlOpt(CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $this->accessToken));
        $http->setUrl($url);
        return $http->execute();
    }
}