<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/19/12
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Session_Storage_Dummy implements Icm_Session_Storage_Interface
{

    /**
     * @var array
     */
    protected $_data = array();

    /**
     *
     */
    public function close() {
        return true;
    }

    /**
     * @param $session_id
     */
    public function destroy($session_id) {
        unset($this->_data[$session_id]);
    }

    /**
     * @param $maxlifetime
     */
    public function gc($maxlifetime) {
        return true;
    }

    /**
     * @param $save_path
     * @param $session_id
     */
    public function open($save_path, $session_id) {
        return true;
    }

    /**
     * @param $session_id
     */
    public function read($session_id) {
        if (!isset($this->_data[$session_id])){
            $this->_data[$session_id] = array();
        }

        return $this->_data[$session_id];
    }

    /**
     * @param $session_id
     * @param $session_data
     */
    public function write($session_id, $session_data) {
        $this->_data[$session_id] = $session_data;
    }

}
