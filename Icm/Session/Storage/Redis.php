<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jlinn
 * Date: 6/28/12
 * Time: 4:51 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Session_Storage_Redis implements Icm_Session_Storage_Interface{
    protected $defaultConfig = array(
        'sessionPrefix' => 'session_',
        'expire' => 14400 //4 hours
    );
    protected $config = array();
    protected $redis;

    protected $sessionData = '';

    /**
     * @param Icm_Config $config
     * @param Icm_Redis $redis
     */
    public function __construct(Icm_Config $config, Icm_Redis $redis){
        foreach ($this->defaultConfig as $key => $value){
            $this->config[$key] = $config->getOption($key, $value);
        }
        $this->redis = $redis;
        // register_shutdown_function(array($this, '__destruct'));
    }

    /**
     * Retrieve session data from Redis
     * @param string $session_id
     * @return string
     */
    protected function retrieve($session_id){
        $session_id = $this->config['sessionPrefix'] . $session_id;
        $pipe = $this->redis->pipeline();
        $pipe->get($session_id);
        $pipe->expire($session_id, $this->config['expire']);
        $return = $pipe->execute();
        // $return = $pipe->executeute();
        return $return[0];
    }

    /**
     * Store session data in Redis
     * @param unknown_type string
     * @param unknown_type string
     * @return boolean
     */
    protected function store($session_id, $data){
        $session_id = $this->config['sessionPrefix'] . $session_id;
        $this->redis->setex($session_id, $this->config['expire'], $data);
        return true;
    }

    /* (non-PHPdoc)
     * @see Icm_Session_Storage_Interface::write()
     */
    public function write($session_id, $data){
        $this->sessionData = $data;
        return true;
    }

    /* (non-PHPdoc)
     * @see Icm_Session_Storage_Interface::read()
     */
    public function read($session_id){
        if ($this->sessionData == ''){
            $this->sessionData = $this->retrieve($session_id);
        }
        return $this->sessionData;
    }

    /* (non-PHPdoc)
     * @see Icm_Session_Storage_Interface::open()
     */
    public function open($save_path, $session_id){
        return true;
    }

    /* (non-PHPdoc)
     * @see Icm_Session_Storage_Interface::gc()
     */
    public function gc($maxlifetime){
        return true;
    }

    /* (non-PHPdoc)
     * @see Icm_Session_Storage_Interface::destroy()
     */
    public function destroy($session_id){
        $session_id = $this->config['sessionPrefix'] . $session_id;
        $this->redis->del($session_id);
        $this->sessionData = '';
        return true;
    }

    public function close(){
        return true;
    }

    public function __destruct(){
        $session_id = session_id();
        if ($session_id != ''){
            $this->store($session_id, $this->sessionData);
        }
    }
}