<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/19/12
 * Time: 9:46 AM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Session_Storage_Interface
{

    /**
     * @abstract
     */
    public function close ();
    /**
     * @abstract
     * @param $session_id
     */
    public function destroy ($session_id );
    /**
     * @abstract
     * @param $maxlifetime
     */
    public function gc ( $maxlifetime );
    /**
     * @abstract
     * @param $save_path
     * @param $session_id
     */
    public function open ($save_path , $session_id );

    /**
     * @abstract
     * @param $session_id
     */
    public function read ( $session_id );

    /**
     * @abstract
     * @param $session_id
     * @param $session_data
     */
    public function write ( $session_id , $session_data );
}
