<?php
class Icm_Session_Namespace
{

    protected $namespace;

    public function __construct($namespace) {
        $this->namespace = $namespace;
    }

    public function getAllData(){
        if (!isset($_SESSION['_ICM']) || !isset($_SESSION['_ICM'][$this->namespace])){
            $_SESSION['_ICM'][$this->namespace] = array();
        }
        return $_SESSION['_ICM'][$this->namespace];
    }

    public function __set($key, $val) {
        if (!array_key_exists('_ICM', $_SESSION)) {
            $_SESSION['_ICM'] = array();
        }
        if (!array_key_exists($this->namespace, $_SESSION['_ICM'])) {
            $_SESSION['_ICM'][$this->namespace] = array();
        }
        $_SESSION['_ICM'][$this->namespace][$key] = $val;

    }

    public function __get($key) {
        if (isset($_SESSION['_ICM'][$this->namespace][$key])) {
            return $_SESSION['_ICM'][$this->namespace][$key];
        }

        return null;
    }

    public function __unset($key) {
        unset($_SESSION['_ICM'][$this->namespace][$key]);
    }

    public function destroy(){
        unset($_SESSION['_ICM'][$this->namespace]);
    }
}
