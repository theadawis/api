<?php

class Icm_SplitTest_BarrelCounter extends Icm_SplitTest{

    protected $app_id;
    protected $section_id;
    protected $conn;
    protected $redis;

    // Redis barrel counter to track available tests for app/con
    // Redis barrel counter to track available variations for tests

    public function __construct($app_id, $section_id, Icm_Db_Interface $mysqlConn, Icm_Redis $redisConn){
        $this->app_id = intval($app_id);
        $this->section_id = intval($section_id);
        $this->conn = $mysqlConn;
        $this->redis = $redisConn;
    }
        /*
         * returns alltests-development:1:1
         * where the first one represents the app id and the second one
         * represents context id
         */
    protected function getAllTestKey(){
        return Icm_Util_SplitTest::makeTestCounterRedisKey($this->section_id, $this->app_id);
    }

    protected function getTVAKey($test_id){
        return Icm_Util_SplitTest::makeVariationCounterRedisKey($this->section_id, $test_id);
    }

    // add test to list of available tests
    public function addTestToQueue($test_id){
        try{
            $weight = 1;
            $status = $this->LIVE;
            $redisKey = $this->getAllTestKey();
            // return test as argument data:
            $list = $this->listTests($this->app_id, array('test_id' => intval($test_id)) );

            // should only be one test with test id supplied in above function
            foreach ($list['return'] as $test){
                $status = intval($test['status']);
                $weight = intval($test['weight']);
            }

            if ($status != $this->LIVE) {
                return $this->removeTestFromQueue($test_id);
            }

            // check to see if the test id already exists
            $values = $this->redis->lrange($redisKey, 0, -1);

            $currWeight = 0;
            foreach ($values as $value){
                if ($test_id == $value){
                    $currWeight++;
                }
            }

            // if current weight matches the weight specified in mysql, exit
            if ($currWeight == intval($weight)) {
                return true;
            }

            // if current weight is greater than 0 and doesnt match weight from mysql, update
            if ($currWeight > 0 && $currWeight != intval($weight) ){
                if (!$this->removeTestFromQueue($test_id)) {
                    return false;
                }
            }
            // add to queue
            for ($i=0;$i<$weight;$i++){
                $this->redis->lpush($redisKey, $test_id);
            }

            return true;
        }
        catch (Exception $e){
            return false;
        }
    }

    // function to make calling an update to the test queue visually make more sense
    public function updateTestInQueue($test_id){
        return $this->addTestToQueue($test_id);
    }

    // delete test from list of available tests
    public function removeTestFromQueue($test_id){
        try{
            $redisKey = $this->getAllTestKey();
            $len = $this->redis->llen($redisKey);

            for ($i=0;$i<$len;$i++){
                $ckId = $this->redis->rpop($redisKey);

                // If the popped id does not match the test id, push it back to the end of the list
                if ($ckId != $test_id){
                    $this->redis->lpush($redisKey, $ckId);
                }
            }
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }

    // add variation to test variation association barrel
    public function addVariationToQueue($test_id, $variation_id){
        try{
            $redisKey = $this->getTVAKey($test_id);
            $weight = 1;
            $active = 1;

            // get all the test variation associations
            $tva_list = $this->listTestVariationAssocs(intval($test_id), array('variation_id' => intval($variation_id)));

            // add them to the test->variation queue
            foreach ($tva_list['return'] as $tva){
                $weight = intval($tva['weight']);
                $active = intval($tva['active']);
            }

            if (!$active) {
                return $this->removeVariationFromQueue($test_id, $variation_id);
            }

            // also check the variation itself, if its status is not active, remove
            $variation_list = $this->listVariations(array('variation_id' => intval($variation_id)));

            if ($variation_list['return'][0]['status'] != $this->VARLIVE) {
                return $this->removeVariationFromQueue($test_id, $variation_id);
            }

            // check to see if the variation id already exists
            $values = $this->redis->lrange($redisKey, 0, -1);
            $currWeight = 0;

            foreach ($values as $value){
                if (intval($variation_id) == intval($value)){
                    $currWeight++;
                }
            }

            // If current weight matches the weight specified in mysql, exit
            if ($currWeight == intval($weight)) {
                return true;
            }

            // if current weight is greater than 0 and doesnt match weight, update
            if ($currWeight > 0 && $currWeight != intval($weight)){
                return $this->removeVariationFromQueue($test_id, $variation_id);
            }

            // add to queue
            for ($i=0;$i<$weight;$i++){
                $this->redis->lpush($redisKey, $variation_id);
            }

            return true;
        }
        catch(Exception $e){
            return false;
        }
    }

    // make the call to updating variation queue a bit clearer
    public function updateVariationInQueue($test_id, $variation_id){
        return $this->addVariationToQueue($test_id, $variation_id);
    }

    // delete variation from test variation association barrel
    public function removeVariationFromQueue($test_id, $variation_id){
        try{
            $redisKey = $this->getTVAKey($test_id);
            $len = $this->redis->llen($redisKey);

            for ($i=0;$i<$len;$i++){
                $ckId = $this->redis->rpop($redisKey);

                // If the popped id doesnt match the variationid, push it back to the end of the list
                if ($ckId != $variation_id){
                    $this->redis->lpush($redisKey, $ckId);
                }
            }
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }

    public function purgeVariation($variationId) {
        $key = Icm_Util_SplitTest::makeVariationRedisKey($variationId, $this->app_id);
        return $this->redis->del($key);
    }

    public function purgeSectionControl($sectionId) {
        $key = Icm_Util_SplitTest::makeSectionControlVariationRedisKey($sectionId, $this->app_id);
        return $this->redis->del($key);
    }

    public function purgeSectionDefault($sectionId) {
        $key = Icm_Util_SplitTest::makeSectionDefaultVariationRedisKey($sectionId, $this->app_id);
        return $this->redis->del($key);
    }

    protected function getAllActiveTestsForAppCon(){
        try{
            $live = $this->LIVE;
            $sql = "SELECT
                        `id`
                    FROM
                        `split_test`
                    WHERE
                        `status` = $live
                    AND
                        `app_id` = :app
                    AND
                        `section_id` = :con
                    ";
            $params = array(':app' => $this->app_id, ':con' => $this->section_id);

            return $this->conn->fetchAll($sql, $params);
        }
        catch(Excpetion $e){
            return array();
        }
    }

    protected function getAllActiveVariationsForTest($test_id){
        try{
            $live = $this->VARLIVE;
            $sql = "SELECT
                        `stva`.`variation_id`
                    FROM
                        `split_test_variation_assoc` as `stva`
                    JOIN
                        `split_variation` as `sv`
                    ON
                        `sv`.`id` = `stva`.`variation_id`
                    AND
                        `sv`.`status` = $live
                    AND
                        `stva`.`active` = 1
                    AND
                        `stva`.`test_id` = :test_id
                ";
            $params = array(':test_id' => $test_id);
            return $this->conn->fetchAll($sql, $params);

        }
        catch(Exception $e){
            return array();
        }
    }

    // rebuild the redis queues from mysql data
    public function rebuildQueues(){

        try{

            $redisAllTestKey = $this->getAllTestKey();

            // clear out all the values within alltest key
            $this->redis->del($redisAllTestKey);

            // grab all the active tests within the app and context
            $activeTests = $this->getAllActiveTestsForAppCon();

            // insert each active test into the alltests queue and
            // they're variations into the tva queues
            foreach ($activeTests as $test){
                $test_id = $test['id'];

                if (!$this->addTestToQueue($test_id) ) {
                    throw new Exception("Error adding test#$test_id to alltest queue");
                }

                $redisTVAKey = $this->getTVAKey($test_id);

                 $redisTVAKey = $this->getTVAKey($test_id);

                 // delete current key
                 $this->redis->del($redisTVAKey);

                // get list of active variations from test-var-assoc
                $activeVars = $this->getAllActiveVariationsForTest($test_id);

                // foreach variation, add to the queue
                foreach ($activeVars as $var){
                    $var_id = $var['variation_id'];

                    if (!$this->addVariationToQueue($test_id, $var_id)) {
                        throw new Exception("Error adding var#$var_id to tva:$test_id");
                    }
                }
            }

            return true;
        }
        catch(Exception $e){
            return false;
        }
    }
}
