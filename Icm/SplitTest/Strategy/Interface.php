<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/3/12
 * Time: 1:47 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_SplitTest_Strategy_Interface
{

    /**
     * @abstract
     * @param $sectionId
     * @param $test
     * @return Icm_SplitTest_Variation
     * @throws Icm_Exception - if invalid array is passed
     */
    public function fromArray($sectionId, $test);

    /**
     * @abstract
     * @param $sectionId
     * @param bool $forceVariation
     * @return Icm_SplitTest_Variation
     */
    public function getTest($sectionId, $forceVariation = false);

}
