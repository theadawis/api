<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/2/12
 * Time: 12:32 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_SplitTest_Decider_Interface
{

    public function getDefault($sectionId);
    public function testIsActive(Icm_SplitTest_Variation $t);
    public function getNextActiveTest($sectionId, $trackingContext);
    public function getControl($sectionId);
    public function getTestMeta($variationId);
    public function getTestByVariation($variationId);

    /**
     * @abstract
     * @param string $type
     * @param string $value
     * @return boolean
     */
    public function isSuperSegment($type, $value);

    /**
     * @abstract
     * @param $type
     * @param $value
     * @return int
     */
    public function getSegmentId($type, $value);

}
