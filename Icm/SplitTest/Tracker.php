<?php
/**
 * Tracker.php
 * User: chris
 * Date: 5/11/13
 * Time: 7:55 AM
 */

class Icm_SplitTest_Tracker {

    const ORGANIC_AFFILIATE = 1;

    /**
     * List of keys that we use relating to the current request
     * @var array
     */
    private static $REQUEST_KEYS = array(
        'ip_address', 'useragent'
    );

    /**
     * List of all possible tracking keys.
     * Includes GA style and affiliate marketing style
     * @var array
     */
    private static $TRACKING_KEYS = array(
        'mdm', 'src', 'cmp', 'cnt', 'brs',
        'affiliate_id', 'sub_id', 's1', 's2', 's3', 's4', 's5'
    );

    /**
     * List of all the variation details we want
     * @var array
     */
    private static $VARIATION_KEYS = array(
        'testId', 'variationId', 'sectionId'
    );

    private static $NONPAID_INDICATORS = array(
        'organic'
    );

    /**
     * @var Icm_Visit_Tracker
     */
    protected $visitTracker;

    /**
     * @var Icm_Splittest_Decider
     */
    protected $decider;

    /**
     * List of currently tracked tests for user
     * @var array
     */
    protected $trackedTests = array();

    /**
     * The current variation
     * @var Icm_SplitTest_Variation
     */
    protected $currentVariation;

    /**
     * Keep track of if the user is a ho, or not
     * @TODO migrate this to the visit tracker
     * @var bool $isHo
     */
    protected $isHo;

    /**
     * @var Icm_Util_Useragent $userAgent
     */
    protected $userAgent;

    /**
     * Holds if the visitor is logged in or not
     * @var bool $isLoggedIn
     */
    protected $isLoggedIn;

    function __construct(Icm_Visit_Tracker $visitTracker, Icm_Splittest_Decider $decider) {
        $this->visitTracker = $visitTracker;
        $this->decider = $decider;
    }

    public function getRequestContext() {
        return $this->getVisitDataset(self::$REQUEST_KEYS);
    }

    public function getTrackedTestsBySection() {
        return array_reduce($this->getTestContext(), function(&$accumulated, $item) {
            $accumulated[$item["sectionId"]] = $item;
            return $accumulated;
        }, array());
    }

    public function getTrackingContext() {
        return $this->getVisitDataset(self::$TRACKING_KEYS);
    }

    public function getTestContext() {
        return array_map(function($item){
            return array_reduce(self::$VARIATION_KEYS, function(&$accumulated, $key) use ($item){
                /**
                 * @var Icm_SplitTest_Variation $item
                 */
                $fn = 'get' . ucfirst($key);
                $accumulated[$key] = (int) $item->$fn();
                return $accumulated;
            }, array());
        }, array_values($this->trackedTests));
    }

    /**
     * @deprecated
     * @see setCurrentVariation
     * @param Icm_SplitTest_Variation $variation
     */
    public function setCurrentTest(Icm_SplitTest_Variation $variation) {
        $this->setCurrentVariation($variation);
    }

    /**
     * Set (and track) the current variation
     * @param Icm_SplitTest_Variation $variation
     */
    public function setCurrentVariation(Icm_SplitTest_Variation $variation) {
        $this->currentVariation = $variation;
        $this->trackVariation($variation);
    }

    /**
     * @deprecated
     * @see getCurrentVariation
     * @return mixed
     */
    public function getCurrentTest() {
        return $this->getCurrentVariation();
    }

    /**
     * Get the current variation
     * @return Icm_SplitTest_Variation
     */
    public function getCurrentVariation() {
        return $this->currentVariation;
    }

    /**
     * @deprecated
     * @see trackVariation
     * @param Icm_SplitTest_Variation $variation
     */
    public function trackTest(Icm_SplitTest_Variation $variation) {
        $this->trackVariation($variation);
    }

    /**
     * Keep track of an assigned variation for that segment
     * @param Icm_SplitTest_Variation $variation
     */
    public function trackVariation(Icm_SplitTest_Variation $variation) {
        $this->trackedTests[$variation->getSectionId()] = $variation;
    }

    /**
     * @deprecated
     * @see hasVariationForSection
     * @param $sectionId
     * @return bool
     */
    public function hasTestForSection($sectionId) {
        return $this->hasVariationForSection($sectionId);
    }

    /**
     * @param $sectionId
     *
     * @return bool
     */
    public function hasVariationForSection($sectionId) {
        return array_key_exists($sectionId, $this->trackedTests);
    }

    /**
     * @deprecated
     * @see retrieveVariationForSection
     * @param $sectionId
     */
    public function retrieveTestForSection($sectionId) {
        return $this->retrieveVariationForSection($sectionId);
    }

    public function retrieveVariationForSection($sectionId) {
        if ($this->hasVariationForSection($sectionId)) {
            return $this->trackedTests[$sectionId];
        }
    }

    public function requiresResegment() {
        $ctx = $this->getTrackingContext();
        return in_array(true, array_map(function($key) use ($ctx){
            return $this->decider->isSuperSegment($key, $ctx[$key]);
        }, array_keys($ctx)));
    }

    /*
     * Utility/Backwards Compatibility methods
     */

    /**
     * @return string
     */
    public function getVisitorId() {
        return $this->visitTracker->getVisitorId();
    }

    /**
     * @return string
     */
    public function getVisitId() {
        return $this->visitTracker->getVisitId();
    }

    /**
     * @param bool $flag
     * @return Icm_SplitTest_Tracker
     */
    public function hos($flag = true) {
        $this->isHo = $flag;
        return $this;
    }

    /**
     * Return if the user has been deemed a HO by the HOS system
     * @return bool
     */
    public function isHosed() {
        return $this->isHo;
    }

    /**
     * @return bool
     */
    public function isBrowser() {
        return $this->getUserAgent()->isBrowser();
    }

    /**
     * @return bool
     */
    public function isBot() {
        return $this->getUserAgent()->isRobot();
    }

    /**
     * @return bool
     */
    public function isNotBrowser() {
        // This may seem counter-intuitive, but we say
        // Anything that is explicitly a robot is not a browser.
        // This is because our whitelist for browsers is incomplete,
        // And we would rather include a bot in a test than
        // risk a user not being included
        return $this->getUserAgent()->isRobot();
    }

    public function isMobile() {
        return $this->getUserAgent()->isMobile();
    }

    /**
     * @TODO double check if theres any other values than 'organic' for the new tracking variables
     * @return bool
     */
    public function isPaidTraffic() {
        $trackingContext = $this->getTrackingContext();
        // If they're from the organic affiliate, they're automatically not paid traffic
        if (isset($trackingContext['affiliate_id']) && $trackingContext['affiliate_id'] == self::ORGANIC_AFFILIATE) {
            return false;
        }
        $trackingValues = array_values($trackingContext);
        return !in_array(true, array_map(function($indicator) use ($trackingValues) {
            return in_array($indicator, $trackingValues);
        }, self::$NONPAID_INDICATORS));
    }

    /**
     * @param bool $flag
     *
     * @return Icm_SplitTest_Tracker
     */
    public function loggedIn($flag = true) {
        $this->isLoggedIn = $flag;
        return $this;
    }

    public function isLoggedIn() {
        return $this->isLoggedIn;
    }

    /**
     * Get a specific key from the visit tracker
     * @param $key
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getVisitData($key, $default = null) {
        // set default
        $visitValue = $default;

        // handle any special cases
        switch ($key) {

            // get browser from the userAgent
            case 'brs':
                $browser = $this->getUserAgent()->getBrowser();

                if ($browser) {
                    $visitValue = $browser;
                }
                break;

            // get everything else from the visitTracker
            default:

                // use visit data and get params
                $visitData = $this->visitTracker->getVisitData();
                $params = isset($visitData['params']) ? json_decode($visitData['params'], true) : array();
                $data = array_merge($params, $visitData);

                if (isset($data[$key])) {
                    $visitValue = $data[$key];
                }
                break;
        }

        return $visitValue;
    }

    /**
     * Get a key value set from visitor data
     * @param array $dataset
     *
     * @return array
     */
    protected function getVisitDataset($dataset) {
        return array_reduce($dataset, function(&$accumulated, $item) {
            $accumulated[$item] = $this->getVisitData($item);
            return $accumulated;
        }, array());
    }

    protected function getUserAgent() {
        if (!$this->userAgent) {
            $this->userAgent = new Icm_Util_Useragent();
        }
        return $this->userAgent;
    }

}