<?php
/**
 * This class handles all operations pertaining to events
 * @package  splitTest
 * @subpackage Event
 * @author fadi
 */
class Icm_SplitTest_Event_StorageAdapter extends Icm_Db_Table implements Icm_SplitTest_Event_Interface
{
    /**
     * This function returns return objects as an array.
     * @param Icm_SplitTest_Event_StorageAdapter $data
     * @return array
     */
    public function returnArrayFromObject($data, $exclude = array()){
        $dataArray = array();

        foreach ($data as $key => $value){
            if ($value != NULL && (!in_array($key, $exclude))){
                $dataArray[$key] = $value;
            }

        }

        return $dataArray;
    }

    /**
     * Returns Maximum value o fthe indicated field.
     * @param string $field
     * @return array
     */
    public function getMaxValue($field){
        $sql = "SELECT MAX(`$field`) as `$field` FROM {$this->tableName} LIMIT 0, 1";
        return $this->conn->fetchAll($sql);
    }

    /**
     * Check to make sure the record has been deleted.
     * @param string $fieldName
     * @param int $id
     * @return boolean
     */
    public function isRecordDeleted($fieldName, $id){
        $result = $this->rowExists($fieldName, $id);

        // if $result is bool false, then the delete is a success.
        if ($result){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Check to make sure the record is not a duplicate.
     * @param string $field
     * @param string $value
     * @return type
     */
    public function isRecordAduplicate($field, $value){
        $sql = "SELECT `{$field}` FROM {$this->tableName} WHERE `{$field}` = :{$field}";
        $placement = array($field => $value);
        return $this->conn->fetchAll($sql, $placement);
    }

    /**
     * retrievs and returns data from db table.
     * @param Icm_SplitTest_Event_StorageAdapter $data
     * @return array
     */
    public function fetchData($data){
        $parameter =  $this->returnArrayFromObject($data);
        $sql = "SELECT * FROM {$this->tableName} WHERE ";
        $sql .= $this->parseWhere($parameter);
        $sql .= " ORDER BY `order` ASC";
        $placement = $this->makeReplacements($parameter);

        return $this->conn->fetchAll($sql, $placement);
    }

    /**
     * saves data into the event table.
     * @param Icm_SplitTest_Event_StorageAdapter $data
     * @return string|int
     */
    public function saveData($data){
        $parameter =  $this->returnArrayFromObject($data);
        $doesSlugExist = $this->isRecordAduplicate('slug', $parameter['slug']);
        if ($doesSlugExist){
            throw new Exception('Operation failed');
        }
        else{
            /*
             * Get the max order from table:
             */
            $responseOrder = $this->getMaxValue('order');

            // get the max order number.
            $order =  $responseOrder[0]['order'];
            $order++;

            // add the new order to the associative array:
            $parameter['order'] = $order;

            // do the insert:
            $isRecordSaved = $this->insertOne($parameter);

            // if data was submitted, return true. Otherwise, throw an exception:
            if ($isRecordSaved){
                return  true;
            }
            else{
                throw new Exception('Operation failed');
            }
        }

    }

    /**
     * Deletes data from the event table.
     * @param Icm_SplitTest_Event_StorageAdapter $data
     * @return int|string
     */
    public function deleteData($data){
        $parameter =  $this->returnArrayFromObject($data);
        $sectionId = array($parameter['id']);

        // do the delete:
        $sql = "DELETE FROM {$this->tableName} WHERE `id` = ?";
        $this->conn->execute($sql, $sectionId);

        // make sure data was actually deleted:
        $isRecordDeleted = $this->isRecordDeleted('id', $parameter['id']);

        // if data was deleted, return a status of 1000. Otherwise, return a fail status:
        if ($isRecordDeleted){
            return  true;
        }
        else{
            throw new Exception('Operation failed');
        }
    }

    /**
     * Sorts rows of event table.
     * @param Icm_SplitTest_Event_StorageAdapter $data
     * @return int
     */
    public function sortDataEvent($data){
        $parameter = $this->returnArrayFromObject($data);
        $response = $this->updateOne($parameter, 'id');
        return true;
    }
}
