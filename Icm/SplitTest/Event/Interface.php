<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This interface is designed for Icm_SplitTest_Event
 * @package  splitTest
 * @subpackage Event
 * @author fadi
 */
interface Icm_SplitTest_Event_Interface
{
    public function returnArrayFromObject($data);
    public function getMaxValue($field);
    public function isRecordDeleted($fieldName, $id);
    public function isRecordAduplicate($field, $value);
    public function fetchData($data);
    public function saveData($data);
    public function deleteData($data);
    public function sortDataEvent($data);
}
