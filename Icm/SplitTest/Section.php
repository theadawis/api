<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Section
 * returns array object of the parameter
 * ensures passed on data has a class attribute.
 *
 * @author fadi
 */
class Icm_SplitTest_Section extends Icm_Struct
{
    // Needed access modifiers:
    public $id, $name, $app_id, $slug, $type, $default_variation_id, $order,
            $url, $path, $control_variation_id, $event_id, $section_id;

}
