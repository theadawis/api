<?php
/**
 * This class handles all operations pertaining to splittest
 * @package  splitTest
 * @subpackage Test
 * @author fadi
 */
class Icm_SplitTest_Test_StorageAdapter extends Icm_Db_Table implements Icm_SplitTest_Test_Interface {

    protected $VARLIVE = 1;
    const DEFAULT_TABLE = 'split_test';

    /**
     * This function returns return objects as an array.
     * I added an exclusion array to this function because app_id is passed
     * throughout the application as a parameter but this table doesn't have an
     * app_id field so it will drop it as it creates an array of values otherwise
     * the insertion will not succeed.
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param array $exclude
     * @return array
     */
    public function returnArrayFromObject($data, $exclude = array()) {
        $dataArray = array();

        foreach ($data as $key => $value) {

            if ($value != NULL && (!in_array($key, $exclude))) {
                $dataArray[$key] = $value;
            }

        }
        return $dataArray;
    }

    /**
     * Check to make sure the record has been deleted
     * @param string $fieldName
     * @param int $id
     * @return boolean
     */
    public function isRecordDeleted($fieldName, $id) {
        $result = $this->rowExists($fieldName, $id);

        // If $result is bool false, then the delete is a success.
        if ($result) {
            return false;
        }

        else {
            return true;
        }
    }

    /**
     * Retrieves segments assigned to a test
     * @param int $testId
     * @return array
     */
    public function getSegmentsForTest($testId) {

        $sql = "SELECT `stsa`.`segment_id`, `stsa`.`exclusive`, `stsa`.`exclude`,
            `cgs`.`key`, `cgs`.`value`, `cgs`.`super`, `cgs`.`app_id`
            FROM
            `split_test_segment_assoc` as `stsa`
            JOIN
            `cg_segment` as `cgs`
            ON
            `stsa`.`test_id` = :test_id AND
            `cgs`.`id` = stsa.`segment_id`";
        $params = array(':test_id' => $testId);
        $items = $this->conn->fetchAll($sql, $params);
        return $items;
    }

    /**
     * Retrievs and returns data from db table.
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function fetchData($data) {

        $limit = NULL;
        $exclude = array('stats', 'segments', 'getSegments', 'goals');
        $parameter =  $this->returnArrayFromObject($data, $exclude);
        $sql = "SELECT  `id`, `name`, `app_id`, `control_variation_id`,
            `section_id`, `type`, `status`, `desc`, `weight`, `status`,
            `last_modified`, `start_time`, `end_time`
            FROM {$this->tableName} WHERE ";
        $sql .= $this->parseWhere($parameter);

        if ($limit != NULL) {
            $sql .= " LIMIT ".$limit;
        }

        $placement = $this->makeReplacements($parameter);

        $result = $this->conn->fetchAll($sql, $placement);

        if ($data->getSegments) {

            foreach ($result as $key => $test) {
                $testId = $test['id'];
                $segments = $this->getSegmentsForTest($testId);
                $test['segments'] = $segments;
                $result[$key] = $test;
            }
        }

        return $result;
    }

    /**
     * Retrieves test primary goal
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return int
     */
    public function listPrimaryGoal($data) {

        $exclude = array('id', 'app_id', '$control_variation_id', 'section_id',
            'type', 'name', 'desc', 'weight', 'status', 'last_modified',
            'start_time', 'end_time', 'stats', 'segments', 'getSegments',
            'event_id', 'primary_goal');
        $parameter =  $this->returnArrayFromObject($data, $exclude);
        $sql ="
            SELECT
            `sg`.`id`, `sg`.`event_id`, `sg`.`test_id`, `sg`.`primary_goal`
            FROM
            `split_goal` as `sg`
            JOIN `cg_event_map` as `cem` ON(`cem`.`id` = `sg`.`event_id`)
            WHERE
            `sg`.`test_id` = :test_id ORDER BY `cem`.`order` ASC";

        $placement = array('test_id' => $parameter['test_id']);
        $result = $this->conn->fetchAll($sql, $placement);

        foreach ($result as $key => $value) {

            if ($value['primary_goal']) {
                return $value['event_id'];
            }
        }
    }

    /**
     * Retrieves variation associated with a test.
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function listTestVariationAssoc($data) {

        $exclude = array();
        $parameter =  $this->returnArrayFromObject($data, $exclude);

        $sql ="
            SELECT
            `stva`.`id`, `stva`.`test_id`,
            `stva`.`variation_id`, `stva`.`weight`, `stva`.`active`,
            `sv`.`label`, `sv`.`meta_ids`, `sv`.`section_id`, `sv`.`last_updated`,
            `sv`.`status` as `variation_status`, `sv`.`desc`, `sv`.`type`
            FROM
            `split_test_variation_assoc` as `stva`
            JOIN
            `split_variation` as `sv`
            ON
            `stva`.`test_id` = :test_id
            AND
            `stva`.`variation_id` = `sv`.`id`
            ";
        $placement = array('test_id' => $parameter['test_id']);

        if (isset($parameter['test_variation_assoc_id'])) {
            $sql = $sql . " AND `stva`.`id` = :test_variation_assoc_id";
            $placement[':test_variation_assoc_id'] = $parameter['test_variation_assoc_id'];
        }

        else if (isset($parameter['variation_id'])) {
            $sql = $sql . " AND `stva`.`variation_id` = :variation_id";
            $placement[':variation_id'] = $parameter['variation_id'];
        }

        $results = $this->conn->fetchAll($sql, $placement);
        $superset = array();

        foreach ($results as $key => $variation) {
            $meta_ids = $variation['meta_ids'];
            $metaData = $this->decodeMetas($meta_ids, 'split_meta');
            $variation['meta'] = $metaData;
            $superset[$key] = $variation;
        }

        return $superset;
    }

    /**
     * Retrieves metas
     * @param string $parameter
     * @param string $table
     * @return array
     */
    public function decodeMetas($parameter) {

        $metaIds = explode(',', $parameter);

        foreach ($metaIds as $value) {
            $associativeData = array('id' => $value);
            $sql = "SELECT * FROM `split_meta` WHERE ";
            $sql .= $this->parseWhere($associativeData);
            $placement = $this->makeReplacements($associativeData);
            $metaDataArray[] = $this->conn->fetchAll($sql, $placement);
        }

        return $metaDataArray;
    }

    /**
     * Retrieves all goals associated with a test
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function listGoals($data) {

        $exclude = array('id', 'app_id', '$control_variation_id', 'section_id',
            'type', 'name', 'desc', 'weight', 'status', 'last_modified',
            'start_time', 'end_time', 'stats', 'segments', 'getSegments',
            'event_id', 'primary_goal');
        $parameter =  $this->returnArrayFromObject($data, $exclude);
        $sql ="
            SELECT
            `sg`.`id`, `sg`.`event_id`, `sg`.`test_id`, `sg`.`primary_goal`
            FROM
            `split_goal` as `sg`
            JOIN `cg_event_map` as `cem` ON(`cem`.`id` = `sg`.`event_id`)
            WHERE
            `sg`.`test_id` = :test_id ORDER BY `cem`.`order` ASC ";

        $placement = array('test_id' => $parameter['test_id']);
        return $this->conn->fetchAll($sql, $placement);
    }

    /**
     * Retrieves app id and section id from split_test table
     * @param int $testId
     * @return array
     */
    public function getAppConForTest($testId) {

        $sql = "SELECT
            `app_id`, `section_id`
            FROM
            `split_test`
            WHERE
            `id`=:test_id
            ";
        $params = array(':test_id' => $testId);
        return $this->conn->fetch($sql, $params);
    }

    /**
     * saves data into the split_test_variation_assoc table.
     * Updates barrel counter
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param int $testId
     * @param Icm_redis $redis
     */
    public function createTestVariationAssociation($data, $testId, $redis) {

        $this->tableName = 'split_test_variation_assoc';

        foreach ($data->variation_ids as $variations) {
            $varId = $variations['id'];
            $varWeight = $variations['weight'];
            $parameters = array('test_id' => $testId, 'variation_id' => $varId, 'weight' => $varWeight, 'active' => $this->VARLIVE);
            $variationAssosiationRecord = $this->insertOne($parameters);

            if ($variationAssosiationRecord) {
                $list = $this->getAppConForTest($testId);
                $app_id = $list['app_id'];
                $section_id = $list['section_id'];
                $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $redis);
                $BarrelCounter->addVariationToQueue($testId, $varId);
            }
        }

        $this->tableName = self::DEFAULT_TABLE;
    }

    /**
     * checks the cg_segment table to see if a segment exists.
     * if so, return the segment id
     * @param array $segmentParameter
     * @return int
     */
    public function checkSegmentExists($segmentParameter) {

        $key = $segmentParameter['key']	;
        $value = $segmentParameter['value'];
        $app_id = $segmentParameter['app_id'];
        $sql = "SELECT `id`
            FROM `cg_segment`
            WHERE `key` = :key
            AND `value` = :value
            AND `app_id` = :app_id";
        $params = array(':key' => $key, ':value' => $value, 'app_id' => $app_id);

        $result = $this->conn->fetch($sql, $params);

        if ($result) {
            return $result['id'];
        }

        else {
            return 0;
        }
    }

    /**
     * saves data into the cg_segment table.
     * @param array $segmentParameter
     * @return int
     */
    public function createSegment($segmentParameter) {

        if (array_key_exists('super', $segmentParameter)) {
            $super = $segmentParameter['super'];
        }

        else {
            $super = 0;
        }

        $segmentParameter['super'] = $super;
        // If segment exists return it and do not insert a new segment (duplicate).
        $segId = $this->checkSegmentExists($segmentParameter);
        if ($segId) {
            return $segId;
        }

        else {
            $this->tableName = 'cg_segment';
            $segmentRecord = $this->insertOne($segmentParameter);

            if ($segmentRecord) {
                return $this->conn->lastInsert();
            }

            $this->tableName = self::DEFAULT_TABLE;
        }
    }

    /**
     * saves data into the split_test_segment_assoc table.
     * to create a relashioship between segment id and test id.
     * @param array $segmentParameters
     * @param int $testId
     * @param Icm_redis $redis
     */
    public function createTestSegmentAssoc($segmentParameters, $testId, $redis) {

        $this->tableName = 'split_test_segment_assoc';
        $segmentRecord = $this->insertOne($segmentParameters);

        if ($segmentRecord) {
            $list = $this->getAppConForTest($testId);
            $app_id = $list['app_id'];
            $section_id = $list['section_id'];
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $redis);
            $BarrelCounter->rebuildQueues();
        }

        $this->tableName = self::DEFAULT_TABLE;
    }

    /**
     * saves data into the split_goal table.
     * to create a relashioship between event id and test id.
     * @param array $goalParameters
     * @return int
     */
    public function createGoal($goalParameters) {

        // validate the optional parameters
        $primary_goal = 0;
        $limit = null;
        $this->tableName = 'split_goal';
        $goalRecord = $this->insertOne($goalParameters);
        $this->tableName = self::DEFAULT_TABLE;

        // Process the results from the SQL operation
        if ($goalRecord) {
            return $this->conn->lastInsert();
        }

        else {
            return 0;
        }
    }

    /**
     * Deletes data related to test
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param string $table
     * @return void
     */
    public function deleteTestRelatedData($data, $table) {

        $id = array($data['id']);
        // Do the delete:
        $sql = "DELETE FROM {$table}
        WHERE `id` = ?";
        $this->conn->execute($sql, $id);
    }

    /**
     * Retrieves segmenta associated with a test
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function listTestSegmentAssocs($data) {
        $sql = "SELECT
            `stsa`.`id`, `stsa`.`test_id`, `stsa`.`segment_id`, `stsa`.`exclusive`, `stsa`.`exclude`
            FROM
            `split_test_segment_assoc` as `stsa`
            JOIN
            `split_test` as `st` ON(`st`.`id` = `stsa`.`test_id`)
            WHERE
            `st`.`app_id` = :app_id
            ";

        $params[':app_id'] = $data['app_id'];

        if (isset($data['test_segment_assoc_id'])) {
            $sql = $sql . " AND `stsa`.`id` = :test_segment_assoc_id";
            $params[':test_segment_assoc_id'] = $data['test_segment_assoc_id'];
        }

        if (isset($data['segment_id'])) {
            $sql = $sql . " AND `stsa`.`segment_id` = :segment_id";
            $params[':segment_id'] = $data['segment_id'];
        }

        if (isset($data['test_id'])) {
            $sql  = $sql . " AND `stsa`.`test_id` = :test_id";
            $params[':test_id'] = $data['test_id'];
        }

        if (isset($data['section_id'])) {
            $sql  = $sql . " AND `st`.`section_id` = :section_id";
            $params[':section_id'] = $data['section_id'];
        }

        $sql = $sql . " GROUP BY `stsa`.`id` ";

        return $this->conn->fetchAll($sql, $params);
    }

    /**
     * Gets the section id from test id
     * @param int $testId
     * @return array
     */
    public function getSectionIdFromTest($testId) {

        $sql = "SELECT `section_id` FROM {$this->tableName} WHERE id = :id";
        $params = array(':id' => $testId);

        return $this->conn->fetch($sql, $params);
    }

    /**
     * Updates test status
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param Icm_redis $redis
     * @return int|string
     */
    public function updateDataTestStatus($data, $redis) {

        $id = $data->id;
        $data->last_modified = time();
        $excludeForTest = array('test_id', 'variation_ids', 'active',
            'segments', 'primary_goal', 'event_ids', 'variations', 'exclusive_target');
        $testParameter =  $this->returnArrayFromObject($data, $excludeForTest);
        $recordUpdate = $this->updateOne($testParameter, 'id');
        $sectionId = $this->getSectionIdFromTest($id);

        if ($recordUpdate) {
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($data->app_id, $sectionId['section_id'], $this->conn, $redis);
            $BarrelCounter->rebuildQueues();
            return  true;
        }
        else {
            throw new Exception('Operation failed');
        }
    }

    /**
     * Updates test data
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param Icm_redis $redis
     * @return string
     */
    public function updateDataTest($data, $redis) {

        $id = $data->test_id;
        $data->last_modified = time();
        $excludeForTest = array('test_id', 'variation_ids', 'active',
            'segments', 'primary_goal', 'event_ids', 'variations', 'exclusive_target');
        $testParameter =  $this->returnArrayFromObject($data, $excludeForTest);
        $testParameter['id'] = $id;

        try {

            // Begin transaction:
            $this->conn->beginTransaction();
            $recordUpdate = $this->updateOne($testParameter, 'id');

            $dataForTestVariationAssoc = array('test_id' => $id);
            $testVariationassocData = $this->listTestVariationAssoc($dataForTestVariationAssoc);

            foreach ($testVariationassocData as $key => $value) {
                $variationAssocId = $value['id'];
                $dataParameter = array('id' => $variationAssocId);
                $this->deleteTestRelatedData($dataParameter, 'split_test_variation_assoc');
            }

            $listTestSegmentAssocsData = array('test_id' => $id, 'app_id' => $data->app_id);
            $listTestSegmentAssocs = $this->listTestSegmentAssocs($listTestSegmentAssocsData);

            foreach ($listTestSegmentAssocs as $key => $value) {
                $listTestSegmentAssocsId = $value['id'];
                $dataParameter = array('id' => $listTestSegmentAssocsId);
                $this->deleteTestRelatedData($dataParameter, 'split_test_segment_assoc');
            }

            $testGoalsData = array('test_id' => $id);
            $testGoalsList = $this->listGoals($testGoalsData);

            foreach ($testGoalsList as $key => $value) {
                $listGoalsId = $value['id'];
                $dataParameter = array('id' => $listGoalsId);
                $this->deleteTestRelatedData($dataParameter, 'split_goal');
            }

            // Start building test related data:
            $this->createTestVariationAssociation($data, $id, $redis);

            // process segments and their testSegmentAssocs
            $this->createSegmentsAndTestSegmentAssocs($data, $id, $redis);

            // create the goals (event/test association)

            foreach ($data->event_ids as $event_id) {

                if ($event_id == $data->primary_goal) {
                    $primaryGoal = 1;

                }

                else {
                    $primaryGoal = 0;
                }

                $goalParameters = array('event_id' => $event_id, 'test_id' => $id,
                    'primary_goal' => $primaryGoal);
                $goalId[] = $this->createGoal($goalParameters);
            }

            $BarrelCounter = new Icm_SplitTest_BarrelCounter($data->app_id, $data->section_id, $this->conn, $redis);
            $BarrelCounter->rebuildQueues();

            // All goes well so commit:
            $this->conn->commit();

            return $id;

        }

        catch (Exception $e) {

            // Something went wrong, so roll back:
            $this->conn->rollBack();
            echo "Failed: " . $e->getMessage();
        }
    }

    /**
     * saves data into the split_test table
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param Icm_redis $redis
     * @return string
     */
    public function saveData($data, $redis) {

        // If this is a copy test, we need to set its status to "draft"
        if ($data->action == 'copy') {
            $data->status = '1';
        }

        $exclude = array('test_id', 'variation_ids', 'active',
            'segments', 'primary_goal', 'event_ids', 'variations', 'exclusive_target', 'action');
        $testParameter =  $this->returnArrayFromObject($data, $exclude);

        // Do the insert:
        try {

            // Begin transaction:
            $this->conn->beginTransaction();
            $testRecord = $this->insertOne($testParameter);
            $testId = $this->conn->lastInsert();

            // create the test variation association
            $this->createTestVariationAssociation($data, $testId, $redis);

            // process segments and their testSegmentAssocs
            $this->createSegmentsAndTestSegmentAssocs($data, $testId, $redis);

            // create the goals (event/test association)
            foreach ($data->event_ids as $event_id) {

                if ($event_id == $data->primary_goal) {
                    $primaryGoal = 1;
                }

                else {
                    $primaryGoal = 0;
                }

                $goalParameters = array('event_id' => $event_id, 'test_id' => $testId,
                    'primary_goal' => $primaryGoal);
                $goalId[] = $this->createGoal($goalParameters);
            }

            $BarrelCounter = new Icm_SplitTest_BarrelCounter($data->app_id, $data->section_id, $this->conn, $redis);
            $BarrelCounter->rebuildQueues();

            // All goes well so commit:
            $this->conn->commit();

            return $testId;
        }

        catch (Exception $e) {

            // Something went wrong, so roll back:
            $this->conn->rollBack();
            echo "Failed: " . $e->getMessage();
        }
    }

    /**
     * Deletes data into the variation table.
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return int|string
     */
    public function deleteData($data) {

        $parameter =  $this->returnArrayFromObject($data);
        $sectionId = array($parameter['id']);

        // Do the delete:
        $sql = "DELETE FROM {$this->tableName} WHERE `id` = ?";
        $this->conn->execute($sql, $sectionId);

        // Make sure data was actually deleted:
        $isRecordDeleted = $this->isRecordDeleted('id', $parameter['id']);

        // If data was deleted, return a status of 1000. Otherwise, return a fail status:
        if ($isRecordDeleted) {
            return  true;
        }

        else {
            throw new Exception('Operation failed');
        }
    }

    /**
     * gets test id and variaiton id from split_test_variation_assoc table
     * @param int $splitTestVarAssoId
     * @return arrray
     */
    public function getTestIdVariationIdFromSplitTestVariationAssociation($splitTestVarAssoId) {

        $table = 'split_test_variation_assoc';
        $sql = "SELECT `stva`.`test_id`, `stva`.`variation_id`, `sv`.`label`, `ss`.`name`,
        `ss`.`id` as `section_id`
        FROM {$table} as `stva`
        JOIN `split_variation` as `sv` ON `sv`.`id` = `stva`.`variation_id`
        JOIN `split_section` as `ss` ON `sv`.`section_id` = `ss`.`id`
        WHERE `stva`.`id` = :id";
        $params = array(':id' => $splitTestVarAssoId);

        return $this->conn->fetch($sql, $params);
    }

    /**
     * Updates variations associated with a test
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param Icm_redis $redis
     * @return int|string
     */
    public function updateTestVariationAssoc($data, $redis) {

        $splitTestVarAssoData = $this->getTestIdVariationIdFromSplitTestVariationAssociation($data->id);
        $testId = $splitTestVarAssoData['test_id'];
        $sectionData = $this->getSectionIdFromTest($testId);
        $sectionId = $sectionData['section_id'];
        $exclude = array('label', 'meta_ids', 'section_id', 'last_updated',
           'variation_status', 'desc', 'type', 'meta', 'app_id');
        $parameter =  $this->returnArrayFromObject($data, $exclude);
        $this->tableName = 'split_test_variation_assoc';

        $recordUpdate = $this->updateOne($parameter, 'id');
        $this->tableName = self::DEFAULT_TABLE;

        if ($recordUpdate) {
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($data->app_id, $sectionId, $this->conn, $redis);
            $BarrelCounter->rebuildQueues();
            return  true;
        }

        else {
            throw new Exception('Operation failed');
        }
    }

    /**
     * This function gets brief info regarding the split_test table
     * for email notification purposes.
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function getSimpleTestInfo($data) {

        $sql = "SELECT `st`.`start_time`, `st`.`name` as `testName`, `st`.`id`, `ss`.`name`, `ss`.`id` as `section_id` FROM
        {$this->tableName} as `st`
        JOIN `split_section` as `ss` ON `ss`.`id` = `st`.`section_id`
        WHERE `st`.`id` = :id";
        $param = array(':id' => $data->id);

        return $this->conn->fetch($sql, $param);
    }

    /**
     * This function returns test status based on test id
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @return array
     */
    public function isTestLive($data) {

        $sql = "SELECT status FROM {$this->tableName} WHERE id = :id";
        $param = array(':id' => $data->id);
        return $this->conn->fetch($sql, $param);
    }

    /**
     * Encapsulates the logic and business rules for creating segments and their corresponding TestSegmentAssocs
     * @param Icm_SplitTest_Test_StorageAdapter $data
     * @param int $testId
     * @param ICM_Redis $redis
     */
    protected function createSegmentsAndTestSegmentAssocs($data, $testId, $redis) {

        // get variables from $data
        $appId = $data->app_id;
        $segmentDatas = $data->segments;
        $exclusive = $data->exclusive_target;

        // whether we want to add the 'all' => 'all' segment
        $addDefaultSegment = true;

        if (!empty($segmentDatas)) {

            // add each segment and assoc
            foreach ($segmentDatas as $segData) {

                // if there is at least one non-excluded segment, we don't want to add the default segment
                if (!$segData['exclude']) {
                    $addDefaultSegment = false;
                }

                // call our partner function to sort out the args
                $this->createSegmentAndTestSegmentAssoc(
                    $appId,
                    $segData['type'],
                    $segData['value'],
                    $testId,
                    $exclusive,
                    $segData['exclude'],
                    $redis
                );
            }
        }

        // add the default segment if $addDefaultSegment was never set to false
        if ($addDefaultSegment) {
            $this->createSegmentAndTestSegmentAssoc(
                $appId,
                'all',
                'all',
                $testId,
                $exclusive,
                0,
                $redis
            );
        }
    }

    /**
     * Encapsulates the logic for creating a segment and its corresponding TestSegmentAssoc
     * @param int $appId
     * @param string $segKey
     * @param string $segValue
     * @param int $testId
     * @param int $exclusive
     * @param int $exclude
     * @param Icm_Redis $redis
     */
    protected function createSegmentAndTestSegmentAssoc($appId, $segKey, $segValue, $testId, $exclusive, $exclude, $redis) {

        // create the segment
        $segmentParams = array('app_id' => $appId, 'key' => $segKey, 'value' => $segValue);
        $segmentId = $this->createSegment($segmentParams);

        // create the corresponding testSegementAssoc
        $assocParams = array('test_id' => $testId, 'segment_id' => $segmentId, 'exclusive' => $exclusive, 'exclude' => $exclude);
        $this->createTestSegmentAssoc($assocParams, $testId, $redis);
    }
}
