<?php
/**
 * This interface is designed for Icm_SplitTest_Test
 * @package  splitTest
 * @subpackage Test
 * @author fadi
 */
interface Icm_SplitTest_Test_Interface
{
    public function returnArrayFromObject($data, $exclude = array());
    public function isRecordDeleted($fieldName, $id);
    public function getSegmentsForTest($testId);
    public function fetchData($data);
    public function listPrimaryGoal($data);
    public function listTestVariationAssoc($data);
    public function decodeMetas($parameter);
    public function listGoals($data);
    public function getAppConForTest($testId);
    public function createTestVariationAssociation($data, $testId, $redis);
    public function checkSegmentExists($segmentParameter);
    public function createSegment($segmentParameter);
    public function createTestSegmentAssoc($segmentParameters, $testId, $redis);
    public function createGoal($goalParameters);
    public function deleteTestRelatedData($data, $table);
    public function listTestSegmentAssocs($data);
    public function updateDataTestStatus($data, $redis);
    public function updateDataTest($data, $redis);
    public function saveData($data, $redis);
    public function deleteData($data);
    public function getSectionIdFromTest($testId);
    public function getTestIdVariationIdFromSplitTestVariationAssociation($id);
    public function updateTestVariationAssoc($data, $redis);
    public function getSimpleTestInfo($data);
    public function isTestLive($data);
}
