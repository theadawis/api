<?php
/**
 * Works as a liason between the admin model (split_test_admin.php) and the
 * storage adapters
 * @package  split_test
 * @subpackage Facade
 * @author fadi
 */
class Icm_SplitTest_StorageFacade
{
    /*
     * Access modifiers for adapters
     */
    private $variationAdapter, $eventAdapter, $sectionAdapter, $testAdapter, $redis;
    /*************************************
     * This section set the adapter type.
     *************************************/
    /**
    * Creates the variaiton adapter
    * @param object $adapterName Icm_SplitTest_Variation_StorageAdapter object
    */
    public function setVariationAdapter($adapterName) {
        if (!isset($this->variationAdapter)) {
             $this->variationAdapter = $adapterName;
        }
    }
    /**
     * Creates an event adapter
     * @param object $adapterName Icm_SplitTest_Event_StorageAdapter object
     */
    public function setEventAdapter($adapterName) {
       if (!isset($this->eventAdapter)) {
           $this->eventAdapter = $adapterName;
       }
    }
    /**
     * Creates a section adapter
     * @param object $adapterName Icm_SplitTest_Section_StorageAdapter object
     */
    public function setSectionAdapter($adapterName) {
       if (!isset($this->sectionAdapter)) {
          $this->sectionAdapter = $adapterName;
       }

    }
    /**
     * Creates a test adapter
     * @param object $adapterName Icm_SplitTest_Test_StorageAdapter object
     */
    public function setTestAdapter($adapterName) {
        if (!isset($this->testAdapter)) {
             $this->testAdapter = $adapterName;
        }
    }
    /**
     * Creates a radi connection
     * @param object $redisConnection
     */
    public function setRedis($redisConnection) {
        if (!isset($this->redis)) {
            $this->redis = $redisConnection;
        }
    }
    /**********************************
     * This is for split test sections*
     **********************************/
    /**
     * Fetches data pertaining to sections
     * @param Icm_SplitTest_Section $data
     * @return array
     */

    public function fetchDataSection(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->fetchData($data);
    }
    /**
     * Lists section-event data
     * @param Icm_SplitTest_Section $data
     * @return  array
     */
    public function listSectionEventAssocs(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->listSectionEventAssocs($data);
    }

    /**
     * Creates data for sections.
     * @param Icm_SplitTest_Section $data
     * @return string.
     */
    public function addDataSection(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->saveData($data);
    }

    /**
     * Deletes section data.
     * @param Icm_SplitTest_Section $data
     * @return string
     */
    public function deleteDataSection(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->deleteData($data, $this->redis);
    }
    /**
     * Sorts section order.
     * @param Icm_SplitTest_Section $data
     * @return string
     */
    public function sortDataSection(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->sortDataSection($data, $this->redis);

    }
    /**
     * Updates default variaiton for a section.
     * @param Icm_SplitTest_Section $data
     * @return string
     */
    public function updateDefaultVariationForSection(Icm_SplitTest_Section $data) {
        return $this->sectionAdapter->updateDefaultVariationForSection($data, $this->redis);
    }
    /*******************************
     * This is for split test event*
     *******************************/
    /**
     * Returns data pertaining to an event.
     * @param Icm_SplitTest_Event $data
     * @return  array
     */
    public function fetchDataEvent(Icm_SplitTest_Event $data) {
        return $this->eventAdapter->fetchData($data);
    }

    /**
     * Creates an event data
     * @param Icm_SplitTest_Event $data
     * @return string
     */
    public function addDataEvent(Icm_SplitTest_Event $data) {
        return $this->eventAdapter->saveData($data);
    }
    /**
     * Deletes an event data
     * @param Icm_SplitTest_Event $data
     * @return string
     */
    public function deleteDataEvent(Icm_SplitTest_Event $data) {
        return $this->eventAdapter->deleteData($data);
    }

    /**
     * Sorts event order
     * @param Icm_SplitTest_Event $data
     * @return string
     */
    public function sortDataEvent(Icm_SplitTest_Event $data) {
        return $this->eventAdapter->sortDataEvent($data);
    }
    /***********************************
     * This is for split test variation*
     ***********************************/
    /**
     * Returns the status count of variations.
     * @param Icm_SplitTest_Variation $data
     * @return int
     */
    public function fetchStatusCount(Icm_SplitTest_Variation $data) {
         return $this->variationAdapter->fetchStatusCount($data);
    }

    /**
     * Creates new variation.
     * @param Icm_SplitTest_Variation $data
     * @return string
     */
    public function addDataVariation(Icm_SplitTest_Variation $data) {
        return $this->variationAdapter->saveData($data);
    }
    /**
     * Fetches data from variation
     * @param Icm_SplitTest_Variation $data
     * @return array
     */
    public function fetchDataVariation(Icm_SplitTest_Variation $data) {
        return $this->variationAdapter->fetchData($data);
    }
    /**
     * Iterates through meta ids and returns meta data
     * @param Icm_SplitTest_Variation $data
     * @return array
     */
    public function decodeMetas(Icm_SplitTest_Variation $data) {
        return $this->variationAdapter->decodeMetas($data);
    }
    /**
     * Deletes metas
     * @param Icm_SplitTest_Variation $data
     * @return void
     */
    public function deleteMetas(Icm_SplitTest_Variation $data) {
        $this->variationAdapter->deleteMetas($data);
    }
    /**
     * Updates a variation
     * @param Icm_SplitTest_Variation $data
     * @return string
     */
    public function updateDataVariation(Icm_SplitTest_Variation $data) {

        return $this->variationAdapter->updateData($data, $this->redis);
    }
    /**
     * Creates a meta
     * @param Icm_SplitTest_Variation $data
     * @return string
     */
    public function createMeta(Icm_SplitTest_Variation $data) {
        return $this->variationAdapter->saveDataMeta($data);
    }
    /******************************
     * This is for split test test*
     ******************************/
    /**
     * Lists tests based on the requirements that are specified in the parameter
     * @param Icm_SplitTest_Test $data
     * @return array
     */
    public function listTests(Icm_SplitTest_Test $data) {
        return $this->testAdapter->fetchData($data);
    }
    /**
     * Lists test goals based on the requirements that are specified in
     * the parameter
     * @param Icm_SplitTest_Test $data
     * @return array
     */
    public function readTestPrimaryGoal(Icm_SplitTest_Test $data) {
        return $this->testAdapter->listPrimaryGoal($data);
    }
    /**
     * Lists test goals based on the requirements that are specified in
     * the parameter
     * @param Icm_SplitTest_Test $data
     * @return array
     */
    public function readTestGoals(Icm_SplitTest_Test $data) {
        return $this->testAdapter->listGoals($data);
    }
    /**
     * Creates test data
     * @param Icm_SplitTest_Test $data
     * @return string
     */
    public function saveDataTest(Icm_SplitTest_Test $data) {
        return $this->testAdapter->saveData($data, $this->redis);
    }
    /**
     * Updates test data
     * @param Icm_SplitTest_Test $data
     * @return string
     */
    public function updateDataTest(Icm_SplitTest_Test $data) {
        return $this->testAdapter->updateDataTest($data, $this->redis);
    }
    /**
     * Lists variations that are related to tests
     * @param Icm_SplitTest_Test $data
     * @return array
     */
    public function listTestVariationAssocs(Icm_SplitTest_Test $data) {
        return $this->testAdapter->listTestVariationAssoc($data);
    }
    /**
     * Updates test status
     * @param Icm_SplitTest_Test $data
     * @return string
     */
    public function updateDataTestStatus(Icm_SplitTest_Test $data) {
        return $this->testAdapter->updateDataTestStatus($data, $this->redis);
    }
    /**
     * Updates test variations
     * @param Icm_SplitTest_Test $data
     * @return string
     */
    public function updateTestVariationAssoc(Icm_SplitTest_Test $data) {
        return $this->testAdapter->updateTestVariationAssoc($data, $this->redis);
    }
    public function getSimpleTestInfo(Icm_SplitTest_Test $data) {
        return $this->testAdapter->getSimpleTestInfo($data);
    }
    public function getTestIdVariationIdFromSplitTestVariationAssociation(Icm_SplitTest_Test $data) {
         return $this->testAdapter->getTestIdVariationIdFromSplitTestVariationAssociation($data->id);
    }
    public function isTestLive(Icm_SplitTest_Test $data) {
        return $this->testAdapter->isTestLive($data);
    }
}