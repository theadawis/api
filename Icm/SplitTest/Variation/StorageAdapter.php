<?php
/**
 * This class handles all operations pertaining to variations
 * @package  splitTest
 * @subpackage Variation
 * @author fadi
 */
class Icm_SplitTest_Variation_StorageAdapter extends Icm_Db_Table implements Icm_SplitTest_Variation_Interface
{
    const DEFAULT_TABLE = 'split_variation';
    /**
     * This function returns objects as an array.
     * I added an exclusion array to this function because app_id is passed
     * throughout the application as a parameter but this table doesn't have an
     * app_id field so it will drop it as it creates an array of values otherwise
     * the insertion will not succeed.
     * @param Icm_SplitTest_Variation_StorageAdapter $data
     * @param array $exclude
     * @return array
     */
   public function returnArrayFromObject($data, $exclude = array()) {
       $dataArray = array();
       foreach ($data as $key => $value) {
           if ($value != NULL && (!in_array($key, $exclude))) {
               $dataArray[$key] = $value;
           }

       }
       return $dataArray;
   }
   /**
    * Check to make sure the record has been deleted.
    * @param srting $fieldName
    * @param int $id
    * @return boolean
    */
   public function isRecordDeleted($fieldName, $id) {
       $result = $this->rowExists($fieldName, $id);//conn->execute($sql, $id);
       // If $result is bool false, then the delete is a success.
       if ($result) {
           return false;
       }
       else
       {
           return true;
       }
   }
   /**
    * retrievs and returns data from db table.
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @return array
    */
   public function fetchData($data) {
       $limit = NULL;
       $exclude = array('app_id', 'tableName', 'pk', 'variation_id');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       if (count($parameter)) {
            $sql = "SELECT `id`, `label`, `meta_ids`, `section_id`, `status`, `desc`,
           `type`, `last_updated` FROM {$this->tableName} WHERE ";
            $sql .= $this->parseWhere($parameter);
            $placement = $this->makeReplacements($parameter);
            if ($limit != NULL) {
                $sql .= " LIMIT ".$limit;
            }
            return $this->conn->fetchAll($sql, $placement);
       }
       else
       {
           $sql = "SELECT `id`, `label`, `meta_ids`, `section_id`, `status`, `desc`,
           `type`, `last_updated` FROM {$this->tableName} ";
           if ($limit != NULL) {
                $sql .= " LIMIT ".$limit;
            }
            return $this->conn->fetchAll($sql);
       }
   }
   /**
    * saves data into the variation table.
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @return int|string
    */
   public function saveData($data) {
       $exclude = array('app_id', 'tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       // Do the insert:
       $isRecordSaved = $this->insertOne($parameter);

       // If data was submitted, return a status of 1000. Otherwise, return a fail status:
       if ($isRecordSaved) {
          return  true;
       }
       else
       {
          throw new Exception('Operation failed');
       }
   }
   /**
    * Deletes data into the variation table.
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @return int|string
    */
   public function deleteData($data) {
       $exclude = array('app_id', 'tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $sectionId = array($parameter['id']);
       // Do the delete:
       $sql = "DELETE FROM {$this->tableName}
               WHERE `id` = ?";
       $this->conn->execute($sql, $sectionId);
       // Make suer data was actually deleted:
       $isRecordDeleted = $this->isRecordDeleted('id', $parameter['id']);
       // If data was deleted, return a status of 1000. Otherwise, return a fail status:
       if ($isRecordDeleted) {
           return  true;
       }
       else
       {
           throw new Exception('Operation failed');
       }
   }
   /**
    * Returns status count of variations.
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @return int
    */
   public function fetchStatusCount($data) {
       $exclude = array('conn', 'redis', 'tableName', 'pk', 'sectionId',
           'testId', 'variationId', 'active', 'weight', 'metaIds');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $sql = "SELECT count(`id`) as `idCount`FROM {$this->tableName} WHERE ";
       $sql .= $this->parseWhere($parameter);
       $sql .=' LIMIT 0, 1 ';
       $placement = $this->makeReplacements($parameter);
       $statusTotal  = $this->conn->fetchAll($sql, $placement);
       return $statusTotal[0]['idCount'];
   }
   /**
    * Returns meta info from the split_meta table.
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @param string $table
    * @return array
    */
   public function decodeMetas($data) {
       $exclude = array('app_id', 'tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $metaIds = implode(',', $parameter);
       $metaIds = explode(',', $metaIds);
       foreach ($metaIds as $value) {
            $associativeData = array('id' => $value);
            $sql = "SELECT * FROM `split_meta` WHERE ";
            $sql .= $this->parseWhere($associativeData);
            $placement = $this->makeReplacements($associativeData);
            $metaDataArray[] = $this->conn->fetchAll($sql, $placement);
       }

       return $metaDataArray;
   }
   /**
    * Saves data into 'split_meta' table
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @param string $table
    * @return array
    */
   public function saveDataMeta($data) {
       $this->tableName = 'split_meta';
       $exclude = array('tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $isRecordSaved = $this->insertOne($parameter);
       $id = $this->conn->lastInsert();
       $this->tableName = self::DEFAULT_TABLE;
       return $id;
   }
   /**
    * selects data from 'split_variation' then
    * deletes data from 'split_meta' table
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @param string $table
    */
   public function deleteMetas($data) {
       $exclude = array('app_id', 'tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $sql = "SELECT `meta_ids` FROM {$this->tableName} WHERE ";
       $sql .= $this->parseWhere($parameter);
       $placement = $this->makeReplacements($parameter);
       $variationMetaIds = $this->conn->fetchAll($sql, $placement);
       foreach ($variationMetaIds as $key => $value) {
           $metaIds = $value['meta_ids'];
       }

       $metaIds = explode(',', $metaIds);

       foreach ($metaIds as $value) {
            $associativeData = array('id' => $value);
            $sql = "DELETE FROM `split_meta` WHERE ";
            $sql .= $this->parseWhere($associativeData);
            $placement = $this->makeReplacements($associativeData);
            $this->conn->execute($sql, $placement);
       }
   }
   /**
    * Gets section id from variation id in the split_variaiotn data
    * @param int $variationId
    * @return array
    */
   public function getSectionIdFromVariation($variationId) {
        $sql = "SELECT `section_id` FROM {$this->tableName} WHERE id = :id";
        $params = array(':id' => $variationId);
        return $this->conn->fetch($sql, $params);
    }
    /**
     * Updates the active status of a variation in the split_test_variation_assoc
     * table
     * @param int $variationId
     * @param string $active
     * @return void
     */
    public function updateTestVariationAssociation($variationId, $active) {
        $this->tableName = 'split_test_variation_assoc';
        $sql = "UPDATE {$this->tableName} SET `active` = :active
        WHERE `variation_id` = :variationId";
        $params = array(':active' => $active, ':variationId' => $variationId);
        $this->conn->execute($sql, $params);
    }
   /**
    * Updates data from 'split_variation' then
    * updates barrell counter
    * @param Icm_SplitTest_Variation_StorageAdapter $data
    * @param Icm_redis $redis
    * @return int|string
    * @throws Exception
    */
   public function updateData($data, $redis) {
       $exclude = array('app_id', 'tableName', 'pk');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $recordUpdate = $this->updateOne($parameter, 'id');
       $app = $data->app_id;
       $id = $data->id;
       // $this->updateTestVariationAssociation($id, $data->status);
       $sectionData = $this->getSectionIdFromVariation($id);
       $sectionId = $sectionData['section_id'];
       if ($recordUpdate) {
           $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $sectionId, $this->conn, $redis);
           $BarrelCounter->rebuildQueues();
           return  true;
       }
       else
       {
           throw new Exception('Operation failed');
       }

   }
}
