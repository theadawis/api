<?php
/**
 * This interface is designed for Icm_SplitTest_Variation
 * @package  splitTest
 * @subpackage Test
 * @author fadi
 */
interface Icm_SplitTest_Variation_Interface
{
    public function returnArrayFromObject($data, $exclude = array());
    public function isRecordDeleted($fieldName, $id);
    public function fetchData($data);
    public function saveData($data);
    public function deleteData($data);
    public function fetchStatusCount($data);
    public function decodeMetas($data);
    public function saveDataMeta($data);
    public function deleteMetas($data);
    public function getSectionIdFromVariation($variationId);
    public function updateTestVariationAssociation($variationId, $active);
    public function updateData($data, $redis);
}
