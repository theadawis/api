<?php
/**
 * This class handles all operations pertaining to
 * sections.
 * @package  split_test
 * @subpackage Section_StorageFacade
 * @author fadi
 */
class Icm_SplitTest_Section_StorageAdapter extends Icm_Db_Table implements Icm_SplitTest_Section_Interface
{
    /**
     * This function returns return objects as an array.
     * I added an exclusion array to this function because app_id is passed
     * throughout the application as a parameter but this table doesn't have an
     * app_id field so it will drop it as it creates an array of values otherwise
     * the insertion will not succeed.
     * @param Icm_SplitTest_Section_StorageAdapter $data
     * @param array $exclude
     * @return array
     */
   public function returnArrayFromObject($data, $exclude = array()) {
       $dataArray = array();
       foreach ($data as $key => $value) {
           if ($value != NULL && (!in_array($key, $exclude))) {
               $dataArray[$key] = $value;
           }

       }
       return $dataArray;
   }
   /**
    * Returns Maximum value of the indicated field.
    * @param string $field
    * @return array
    */
   public function getMaxValue($field) {
       $sql = "SELECT MAX(`$field`) as `$field` FROM {$this->tableName} LIMIT 0, 1";
       return $this->conn->fetchAll($sql);
   }
   /**
    * Check to make sure the record has been deleted.
    * @param string $fieldName
    * @param int $id
    * @return boolean
    */
   public function isRecordDeleted($fieldName, $id) {
       $result = $this->rowExists($fieldName, $id);//conn->execute($sql, $id);
       // If $result is bool false, then the delete is a success.
       if ($result) {
           return false;
       }
       else
       {
           return true;
       }
   }
   /**
    * Check to see if we have a duplicate record.
    * @param string $field
    * @param string $value
    * @return int
    */
   public function isRecordAduplicate($field, $value) {
       $sql = "SELECT `{$field}` FROM {$this->tableName} WHERE
       `{$field}` = :{$field}";
       $placement = array($field => $value);
       return $this->conn->fetchAll($sql, $placement);
   }

   /**
    * retrievd and returns data from db table.
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @return array
    */
   public function fetchData($data) {
       $parameter =  $this->returnArrayFromObject($data);
       if (count($parameter)) {
            $sql = "SELECT * FROM {$this->tableName} WHERE ";
            $sql .= $this->parseWhere($parameter);
            $sql .= " ORDER BY `order`";
            $placement = $this->makeReplacements($parameter);
            // return $this->findByFields($parameter);
            return $this->conn->fetchAll($sql, $placement);
       }
       else
       {
            $sql = "SELECT * FROM {$this->tableName} ";
            $sql .= " ORDER BY `order`";
            return $this->conn->fetchAll($sql);
       }

   }
   /**
    * Retrieves data related to sections and events
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @param string $table
    * @return array
    */
   public function listSectionEventAssocs($data) {
       $limit = NULL;
       $exclude = array('app_id');
       $parameter =  $this->returnArrayFromObject($data, $exclude);
       $sql = "SELECT `id`, `section_id`, `event_id`
           FROM `split_section_event_assoc` WHERE ";
       $sql .= $this->parseWhere($parameter);
       if ($limit != null) {
          $sql .= " LIMIT".$limit;
       }
       $placement = $this->makeReplacements($parameter);
       // return $this->findByFields($parameter);
       return $this->conn->fetchAll($sql, $placement);
   }

   /**
    * saves data into the section table.
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @return string|int
    */
   public function saveData($data) {
       $parameter =  $this->returnArrayFromObject($data);
       $doesSlugExist = $this->isRecordAduplicate('slug', $parameter['slug']);
       if ($doesSlugExist) {
           throw new Exception('Operation failed');
       }
       else
       {
            /*
             * Get the max order from table:
             */
            $responseOrder = $this->getMaxValue('order');
            // Get the max order number.
            $order =  $responseOrder[0]['order'];
            $order++;
            // Add the new order to the associative array:
            $parameter['order'] = $order;
            // Do the insert:
            $isRecordSaved = $this->insertOne($parameter);
            // If data was submitted, return a status of 1000. Otherwise, return a fail status:
            if ($isRecordSaved) {
                return  true;
            }
            else
            {
                throw new Exception('Operation failed');
            }
       }

   }
   /**
    *
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @param Icm_redis $redis
    * @return int|string
    * @throws Exception
    */
   public function deleteData($data, $redis) {
       $parameter =  $this->returnArrayFromObject($data);
       $sectionId = array($parameter['id']);
       // Do the delete:
       $sql = "DELETE FROM {$this->tableName}
               WHERE `id` = ?";
       $this->conn->execute($sql, $sectionId);
       // Make suer data was actually deleted:
       $isRecordDeleted = $this->isRecordDeleted('id', $parameter['id']);
       // Rebuild the queue:
       $BarrelCounter = new Icm_SplitTest_BarrelCounter($data->app_id, $parameter['id'], $this->conn, $redis);
       $BarrelCounter->rebuildQueues();
       // If data was deleted, return a status of 1000. Otherwise, return a fail status:
       if ($isRecordDeleted) {
           return  true;
       }
       else
       {
           throw new Exception('Operation failed');
       }
   }
   /**
    * Sorts section order
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @param Icm_redis $redis
    * @return int
    */
   public function sortDataSection($data, $redis) {
        $parameter =  $this->returnArrayFromObject($data);
        $response = $this->updateOne($parameter, 'id');
        $id = $response['id'];
        $app_id = $response['app_id'];
       // purge default and control caches for this section too
        $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $id, $this->conn, $redis);
    $BarrelCounter->purgeSectionControl($id);
    $BarrelCounter->purgeSectionDefault($id);
        return true;
   }
   /**
    * Updates default variation for section
    * @param Icm_SplitTest_Section_StorageAdapter $data
    * @param Icm_redis $redis
    * @return int|string
    */
   public function updateDefaultVariationForSection($data, $redis) {
       $parameter =  $this->returnArrayFromObject($data);
       $response = $this->updateOne($parameter, 'id');
       $id = $response['id'];
       $app_id = $response['app_id'];
       // purge default and control caches for this section too
        $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $id, $this->conn, $redis);
    $BarrelCounter->purgeSectionControl($id);
    $BarrelCounter->purgeSectionDefault($id);
       if ($response) {
           return  true;
       }
       else
       {
           throw new Exception('Operation failed');
       }
   }
}
