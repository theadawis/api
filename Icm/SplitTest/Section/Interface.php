<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fadi
 */
interface Icm_SplitTest_Section_Interface
{
    public function returnArrayFromObject($data, $exclude = array());
    public function getMaxValue($field);
    public function isRecordDeleted($fieldName, $id);
    public function isRecordAduplicate($field, $value);
    public function fetchData($data);
    public function saveData($data);
    public function deleteData($data, $redis);
    public function sortDataSection($data, $redis);
    public function updateDefaultVariationForSection($data, $redis);
    public function listSectionEventAssocs($data);
}

