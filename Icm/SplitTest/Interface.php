<?php
interface Icm_SplitTest_Interface{

    /*All functions return:
     *Array
     *  status: 1000 = success/no errors
     *          1200 = Error, see optional message for more detail
     *          1910 = Insufficient parameters supplied by caller
     *          1920 = Invalid parameters supplied by caller
     *          1930 = Database did not update
     *  return:
     *          [relevant information for specific call]
     *  optionalMessage:
     *          [relevant information for specific call]
     */

    // Application Functions:
    // optParams: $desc, $url
    public function createApplication($name, $optParams=array());

    // optParams: $app_id, $name
    public function listApplications($optParams=array());
    public function deleteApplication($app_id);


    // Context Functions:
    // optParams: $url, $default_variation_id
    public function createContext($name, $type, $slug, $app_id, $optParams=array());

    // optParams: $section_id, $name, $limit
    public function listContexts($app_id, $optParams=array());
    public function deleteContext($section_id);


    // Segment Functions:
    // optParams: $super
    public function createSegment($key, $value, $app_id, $optParams=array());

    // optParams: $segment_id, $key, $value, $super, $limit
    public function listSegments($app_id, $optParams=array());
    public function deleteSegment($segment_id);


    // Test Functions:
    // optParams: $control_variation_id, $status, $desc, $weight, $start_time, $end_time
    public function createTest($app_id, $section_id, $type, $name, $optParams=array());

    // optParams: $test_id, $segment_id, $type, $status, $limit
    public function listTests($app_id, $optParams=array());
    public function deleteTest($test_id);

    // optParams: $segment_id, $app_id, $control_variation_id, $section_id,
    // $type, $desc, $weight, $status, $start_time, $end_time
    public function updateTest($test_id, $optParams=array());

    // Event Functions:
    // optParams: $desc, $order
    public function createEvent($desc, $slug, $type, $app_id, $optParams=array());

    // optParams: $type, $event_id, $limit
    public function listEvents($app_id, $optParams=array());
    public function deleteEvent($event_id);


    // Goal Functions:
    // optParams: $primary_goal, $limit
    public function createGoal($event_id, $test_id, $optParams=array());

    // optParams: $goal_id, $event_id, $limit
    public function listGoals($test_id, $optParams=array());
    public function deleteGoal($goal_id);

    // optParams: $primary_goal
    public function updateGoal($goal_id, $optParams=array());

    // Meta Functions:
    public function createMeta($key, $value, $app_id);

    // optParams: $key, $value, $meta_id
    public function listMetas($app_id, $optParams=array());
    public function deleteMeta($meta_id);

    // Variation Functions:
    // optParams: $status
    public function createVariation($label, $meta_ids, $section_id, $type, $optParams=array());

    // optParams: $variation_id, $label, $status, $limit
    public function listVariations($optParams=array());
    public function deleteVariation($variation_id);

    // optParams: $label, $meta_ids, $status, $section_id, $desc
    public function updateVariation($variation_id, $optParams=array());

    // Test Variation Association Functions:
    // optParams: $weight, $active
    public function createTestVariationAssoc($test_id, $variation_id, $optParams=array());
    // optParams: $test_variation_assoc, $variation_id, $limit
    public function listTestVariationAssocs($test_id, $optParams=array());
    public function deleteTestVariationAssoc($test_var_assoc_id);

    // Test Segment Association Functions:
    // optParams: $exclusive
    public function createTestSegmentAssoc($test_id, $segment_id, $optParams=array());

    // optParams: $test_segment_assoc, $test_id $segment_id, $limit
    public function listTestSegmentAssocs($app_id, $optParams=array());
    public function deleteTestSegmentAssoc($test_seg_assoc_id);

    // Context Event Association Functions:
    public function createContextEventAssoc($section_id, $event_id);
    // optParams: $context_event_id, $event_id, $limit
    public function listContextEventAssocs($section_id, $optParams=array());
    public function deleteContextEventAssoc($context_event_assoc_id);
}

