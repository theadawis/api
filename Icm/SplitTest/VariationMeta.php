<?php

class Icm_SplitTest_VariationMeta {
    protected static $placeholder_cache = array();

    public static function getMetaByString($csv, $conn) {
        $metas = array();
        $logger = Icm_Util_Logger_Syslog::getInstance('checkmate');
        $logger->logDebug("[Icm_SplitTest_VariationMeta::getMetaByString] Checking csv: $csv");

        if (array_key_exists($csv, self::$placeholder_cache)) {
            $logger->logDebug("[Icm_SplitTest_VariationMeta::getMetaByString] Found csv: $csv");
            return self::$placeholder_cache[$csv];
        }

        if (!empty($csv)) {
            // convert the csv string into an array of bind params
            $params = explode(',', $csv);

            // create the string of ? for the params to bind to
            $placeholders = array_fill(0, count($params), '?');
            $placeholders = implode(',', $placeholders);
            $logger->logDebug("[Icm_SplitTest_VariationMeta::getMetaByString] Fetching csv: $csv");
            // build and run the sql query
            $sql = "SELECT `key`,`value` FROM split_meta WHERE id IN ($placeholders)";
            $result = $conn->fetchAll($sql, $params);

            // convert the returned array into a key => value pairs
            foreach ($result as $meta) {
                $metas[$meta['key']] = $meta['value'];
            }
        }
        $logger->logDebug("[Icm_SplitTest_VariationMeta::getMetaByString] Storing csv: $csv");
        self::$placeholder_cache[$csv] = $metas;
        return $metas;
    }
}