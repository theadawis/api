<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/3/12
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */



class Icm_SplitTest_Strategy implements Icm_SplitTest_Strategy_Interface
{
    protected $decider;
    protected $tracker;
    protected $sectionMap;
    public function __construct(Icm_SplitTest_Tracker $tracker, Icm_SplitTest_Decider_Interface $decider) {
        $this->tracker = $tracker;
        $this->decider = $decider;
    }

    /**
     * @param boolean $testMember
     * @return bool
     */
    protected function showDefault($testMember) {
        if ($testMember === true) {
            return false;
        }
        $showDefaultToMember = $this->tracker->isLoggedIn() && ($testMember == false);

        return ($this->tracker->isHosed() ||
                $this->tracker->isNotBrowser() ||
                !$this->tracker->isPaidTraffic() ||
                $showDefaultToMember
        );
    }

    protected function showPreviousTest($sectionId) {
        return (
            !$this->tracker->requiresResegment() &&
            $this->tracker->hasVariationForSection($sectionId)
        );
    }

    protected function getPreviousTest($sectionId) {
        $test = $this->tracker->retrieveVariationForSection($sectionId);
        // debug_array($test, 'previous test:');
        if ($this->decider->testIsActive($test)){
            // get the metadata for a previously seen test
            $meta = $this->decider->getTestMeta($test->getVariationId());

            foreach ($meta as $key => $val) {
                $test->setMeta($key, $val);
            }

            return $test;
        }
        return null;
    }

    public function getTest($sectionId, $forcedVariation = false, $testMember = false) {

        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("getting test for sectionId:".$sectionId);

        if (!$this->tracker->isHosed() && $forcedVariation ) {
            Icm_Util_Logger_Syslog::getInstance('api')->logInfo("returning a forced variation: ".$forcedVariation);
            $test = $this->decider->getTestByVariation($forcedVariation);
            return $test;
        }

        if ($this->showDefault($testMember)){
            Icm_Util_Logger_Syslog::getInstance('api')->logInfo("Showing Default");
            $test = $this->decider->getDefault($sectionId);

            return $test;
        }

        if ($this->showPreviousTest($sectionId)) {
            $test = $this->getPreviousTest($sectionId);

            if ($test) {
                Icm_Util_Logger_Syslog::getInstance('api')->logInfo("Showing Previous");

                return $test;
            }
        }

        $test = $this->decider->getNextActiveTest(
            $sectionId,
            $this->tracker->getTrackingContext()
        );

        if ($test) {
            Icm_Util_Logger_Syslog::getInstance('api')->logInfo("Showing Active Test");
            return $test;
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("Showing Control");
        $test = $this->decider->getControl($sectionId);

        return $test;
    }

    /**
     * @param $sectionId
     * @param $test
     * @return Icm_SplitTest_Variation
     * @throws Icm_Exception
     */
    public function fromArray($sectionId, $test) {
        @$testId = isset($test['testId']) ? $test['testId'] : $test['test_id'] OR false;
        @$variationId = isset($test['variationId']) ? $test['variationId'] : $test['variation_id'] OR false;

        // Sorry if this looks hacky -- we need empty returns false on 0 -- which is a valid testId)
        if ((string) $testId != "" && (string) $variationId != ""){
            return $this->decider->getSplitTestFactory()->variation(array('sectionId' => $sectionId,
                                                                          'testId' => $testId,
                                                                          'variationId' => $variationId));
        }

        throw new Icm_Exception("Could not instantiate test");
    }
}
