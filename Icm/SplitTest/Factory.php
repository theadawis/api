<?php

  /*
    use it like this:
    $factory = new Icm_SplitTest_Factory($dbConnection);
    $variationObj = $factory->variation($sectionId, $testId, $variationId);
  */

class Icm_SplitTest_Factory {

    protected $conn;

    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
    }

    public function getConn() {
        if (!isset($this->conn)) {
            // help 'em out here - split test is always cg_analytics db
            $this->conn = Icm_Api::getInstance()->dataContainer->get('analyticsDbConnection');
        }
        return $this->conn;
    }

    public function __call($method, $args=array()) {
        $class = 'Icm_SplitTest_'.ucfirst($method);
        return new $class($args[0], $this->getConn());
    }
}