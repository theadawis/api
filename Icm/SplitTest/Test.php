<?php

class Icm_SplitTest_Test extends Icm_Struct {

    public $id, $app_id, $control_variation_id, $section_id, $type, $name, $desc,
        $weight, $status, $last_modified, $start_time, $end_time, $stats,
        $segments, $getSegments, $event_id, $test_id, $primary_goal,
        $variation_id, $active, $exclusive_target, $variation_ids,
        $variations, $event_ids, $goals, $goal_stats, $action;

    /* return a list of active test ids give a tracking context */
    public static function getTestIdsForSectionAndSegments($sectionId, $trackingContext, Icm_Db_Interface $conn, Icm_Redis $redis) {

        // use the datacontainer if no db conection is passed in
        // make sure we have valid db and redis passed in
        if (!isset($conn)) {
            $conn = Icm_Api::getInstance()->dataContainer->get('analyticsDbInterface');
        }

        if (!isset($conn)) {
            throw new Icm_Exception('no database connection provided');
        }

        if (!isset($trackingContext)) {
            throw new Icm_Exception('no tracking context provided');
        }

        if (!isset($sectionId)) {
            throw new Icm_Exception('no sectionId provided');
        }

        // make a string of sql based on the data in tracking context
        $segments = $trackingContext;
        $snippet = "";
        $pairCnt = 0;
        $args = array(':section' => $sectionId);

        foreach ($segments as $key => $val) {
            $snippet .= "(cg.key = :key_".$pairCnt." AND cg.value = :value_".$pairCnt.") OR ";
            $args[":key_".$pairCnt] = $key;
            $args[":value_".$pairCnt] = $val;
            $pairCnt++;
        }

        $snippet .= " (cg.key = 'all' AND cg.value = 'all')";
        $sql = "
            SELECT test_id, exclusive, max(stsa.exclude) AS exclude
            FROM split_test st
            JOIN split_test_segment_assoc stsa ON stsa.test_id = st.id
            JOIN cg_segment cg ON cg.id = stsa.segment_id
            WHERE st.section_id = :section AND st.status = 3 AND (" . $snippet . ")
            GROUP BY test_id
            HAVING NOT exclude";

        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("get test ids for segments: ".$sql);
        $results = $conn->fetchAll($sql, $args);

        // check for exclusive
        $testIds = array();

        foreach ($results as $set) {
            if ($set['exclusive'] == 1) {
                $testIds = array($set['test_id']);
                break;
            }
            $testIds[] = $set['test_id'];
        }

        // TODO: cache $testIds in redis?
        return $testIds;
    }
}
