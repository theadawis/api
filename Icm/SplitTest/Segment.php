<?php

class Icm_SplitTest_Segment {

    protected $conn;
    protected $redis;
    protected $tableName = 'cg_segment';
    protected $pk = 'id';

    protected $segmentId;
    protected $key;
    protected $value;
    protected $allId;
    protected $super;

    public static function getSegmentByKeyValue($key, $value, $conn, $redis) {
        $sql = "select * from cg_segment where `key` = :key and `value` = :value";
        $result = $conn->fetch($sql, array(':key' => $key,
                                          ':value' => $value));

        return new Icm_SplitTest_Segment(array(
                                               'segmentId' => $result['id'],
                                               'key'       => $result['key'],
                                               'value'     => $result['value'],
                                               'appId'     => $result['app_id'],
                                               'super'     => $result['super'],
                                               ),
                                         $conn);
    }

    public function __construct($data, Icm_Db_Interface $conn) {
        $this->conn        = $conn;

        // TODO - clean this up so you can't set arbitrary key/values
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        // store a db table interface for vanilla variation table queries
        $this->variationTable = new Icm_Db_Table($this->tableName, $this->pk, $this->conn);

    }

    public function getSuper() {
        return $this->super;
    }

    public function getSegmentId() {
        return $this->segmentId;
    }


}