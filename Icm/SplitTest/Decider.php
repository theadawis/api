<?php

class Icm_Splittest_Decider implements Icm_SplitTest_Decider_Interface {

    protected $redis;
    protected $conn;
    protected $factory;

    function __construct(Icm_Db_Interface $dbconn, Icm_Redis $redconn){
        $this->conn = $dbconn;
        $this->redis = $redconn;
        $this->factory = new Icm_SplitTest_Factory($this->conn);
    }

    // default is what non-paid users see
    public function getDefault($sectionId) {
        // get key for variation defaults
        $key = Icm_Util_SplitTest::makeSectionDefaultVariationRedisKey($sectionId);

        // get the default variation from redis
        $variationData = json_decode($this->redis->get($key), true);

        if (!$variationData) {
            // not in redis hit the db (which will cache it in redis)
            return Icm_SplitTest_Variation::getDefaultBySectionId($sectionId, $this->conn, $this->redis);
        }

        // create and return the variation object
        return $this->factory->variation($variationData);
    }

    // control is what paid users see
    public function getControl($sectionId) {
        // get key for variation defaults
        $key = Icm_Util_SplitTest::makeSectionControlVariationRedisKey($sectionId);

        // get the default variation from redis
        $variationData = json_decode($this->redis->get($key), true);

        if (!$variationData) {
            // not in redis hit the db (which will cache it in redis)
            return Icm_SplitTest_Variation::getControlBySectionId($sectionId, $this->conn, $this->redis);
        }

        // create and return the variation object
        return $this->factory->variation($variationData);
    }

    public function getNextActiveTest($sectionId, $trackingContext) {
        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("get next active test:".$sectionId);

        // given an app_id (implicit), a sectionId, and a tracker context, figure out which test and variation is up next
        $validTestIds = Icm_SplitTest_Test::getTestIdsForSectionAndSegments($sectionId, $trackingContext, $this->conn, $this->redis);

        // now given a set of valid tests, get one from the all tests redis barrel counter
        $key = Icm_Util_SplitTest::makeTestCounterRedisKey($sectionId);
        $len = $this->redis->llen($key);
        $testId = 0;
        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("looping through test counter: $key");

        for ($i=0;$i<$len;$i++){
            $testId = $this->redis->rpoplpush($key, $key);
            Icm_Util_Logger_Syslog::getInstance('api')->logInfo("checking $testId");

            if (in_array($testId, $validTestIds)) {
                break;
            }
            else {
                $testId = 0;
            }
        }

        if (!$testId) {
            Icm_Util_Logger_Syslog::getInstance('api')->logInfo("no valid tests to serve next - returning control");

            // return control if there is no test
            return $this->getControl($sectionId);
        }

        // getNextVariation from redis barrel counter
        $key = Icm_Util_SplitTest::makeVariationCounterRedisKey($sectionId, $testId);
        Icm_Util_Logger_Syslog::getInstance('api')->logInfo("no valid tests to serve next - returning control");
        $variationId = $this->redis->rpoplpush($key, $key);

        // create and return the variation object
        return $this->factory->variation(array('testId'      => $testId,
                                               'variationId' => $variationId,
                                               'sectionId'   => $sectionId));
    }

    public function testIsActive(Icm_SplitTest_Variation $variation) {
        // check if there's data in redis for this variation
        return $variation->isActive();
    }

    public function getTestByVariation($variationId) {
        $key = Icm_Util_SplitTest::makeVariationRedisKey($variationId);

        // get the test data for this obj to determine if the status is now LIVE (status == 3)
        $variationData = json_decode($this->redis->get($key), true);

        if (is_array($variationData)) {
            $variation = new Icm_SplitTest_Variation($variationData, $this->conn);
        }
        else {
            // get it from the db
            $variation = Icm_SplitTest_Variation::getVariationById($variationId, $this->conn, $this->redis);
        }

        // select * from split_variation where variation_id = $variationId
        $variationMeta = Icm_SplitTest_VariationMeta::getMetaByString($variation->getMetaIds(), $this->conn);

        foreach ($variationMeta as $key => $value) {
            $variation->setMeta($key, $value);
        }

        return $variation;
    }

    public function getTestMeta($variationId) {
        $key = Icm_Util_SplitTest::makeVariationRedisKey($variationId);

        // get the test data for this obj to determine if the status is now LIVE (status == 3)
        $variationData = json_decode($this->redis->get($key), true);

        if (is_array($variationData)) {
            $variation = new Icm_SplitTest_Variation($variationData, $this->conn);
        }
        else {
            // get it from the db
            $variation = Icm_SplitTest_Variation::getVariationById($variationId, $this->conn, $this->redis);
        }

        // select * from split_variation where variation_id = $variationId
        return Icm_SplitTest_VariationMeta::getMetaByString($variation->getMetaIds(), $this->conn);
    }

    public function isSuperSegment($key, $value) {
        // select from cg_segment where super = 1 and key = $key and value = $value
        $segment = Icm_SplitTest_Segment::getSegmentByKeyValue($key, $value, $this->conn, $this->redis);
        return $segment->getSuper() ? true : false;
    }

    public function getSegmentId($key, $value) {
        // select from cg_segment where super = 1 and key = $key and value = $value
        $segment = Icm_SplitTest_Segment::getSegmentByKeyValue($key, $value, $this->conn, $this->redis);
        return $segment->getSegmentId();
    }

    public function getSplitTestFactory() {
        if (!isset($this->factory)) {
            $this->factory = new Icm_SplitTest_Factory($this->conn);
        }

        return $this->factory;
    }
}
