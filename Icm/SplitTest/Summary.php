<?php

class Icm_SplitTest_Summary extends Icm_SplitTest
{
    public function insertVariationSummary($section_id, $test_id, $variation_id, $event_id, $views, $conversions, $timestamp){
        try{
            // validate params
            $tests = array( 'section_id' => 'int',
                            'test_id' => 'int',
                            'event_id' => 'int',
                            'variation_id' => 'int',
                            'views' => 'int',
                            'conversions' => 'int',
                            'timestamp' => 'int');
            $reqParams = array( 'section_id' => $section_id,
                                'test_id' => $test_id,
                                'event_id' => $event_id,
                                'variation_id' => $variation_id,
                                'views' => $views,
                                'conversions' => $conversions,
                                'timestamp' => $timestamp);
            $this->validateReqs($reqParams, $tests);

            $optParams = array('variation_id' => $variation_id);
            $tests = array('variation_id' => 'int');

            $this->validateOpts($optParams, $tests);

            // get app id from test id
            $app_id = $this->getAppIdFromTestId($test_id);
            $app_id +=0;

            // build sql to insert row into cg_test_summary
            $sql ="
                INSERT INTO
                `cg_variation_summary`
                (`app_id`, `section_context_id`,
                `test_id`, `variation_id`,
                `event_id`, `views`,
                `conversions`, `timestamp`)
                VALUES
                (:app_id, :section_context_id,
                :test_id, :variation_id,
                :event_id, :views,
                :conversions, :timestamp)
            ";
            $params = array(':app_id' => $app_id,
                            ':section_context_id' => $section_id,
                            ':test_id' => $test_id,
                            ':event_id' => $event_id,
                            ':variation_id' => $variation_id,
                            ':views' => $views,
                            ':conversions' => $conversions,
                            ':timestamp' => $timestamp);

            try{
                $sqlRet = $this->conn->execute($sql, $params);

                // check for at least one row affected
                if ($sqlRet != 1) {
                    $this->results = array();
                    $this->msg = "Insert test summary failed";
                    $this->status = 1930;
                    return $this->compileReturn();
                }
            }
            catch(Exception $e){
                $this->msg =  "Error executing SQL for variation summary insert: " . $e->getMessage() . "\n";
                $this->status = 1930;
                $this->results = array();
                return $this->compileReturn();
            }

            $insertId = $this->conn->lastInsert();

            $this->status = 1000;
            $this->results = array('variation_summary_id' => $insertId);
            $this->msg = "Succesfully inserted variation summary entry";

            // return status/return/optionalMessage
            return $this->compileReturn();
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error inserting summary id" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    public function getVariationSummary($app_id, $section_id, $test_id, $optParams = null){
        try{
            $variation_id = null;
            $event_id = null;

            // check for optional variation id parameters
            $tests = array('variation_id' => 'int', 'event_id' => $event_id);
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('variation_id', $optParams)) {
                $variation_id = $optParams['variation_id'];
            }

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            $tests = array( 'app_id' => 'int',
                            'section_id' => 'int',
                            'test_id' => 'int');
            $reqParams = array( 'app_id' => $app_id,
                                'section_id' => $section_id,
                                'test_id' => $test_id);
            $this->validateReqs($reqParams, $tests);

            // build sql for select statement
            $params = array(':app_id' => $app_id,
                            ':section_id' => $section_id,
                            ':test_id' => $test_id);
            $sql = "SELECT *
                    FROM
                    `cg_variation_summary`
                    WHERE
                    `app_id` = :app_id AND
                    `section_context_id` = :section_id AND
                    `test_id` = :test_id ";

            if ($variation_id != null){
                $sql .= " AND `variation_id` = :variation_id";
                $params['variation_id'] = $variation_id;
            }

            if ($event_id != null){
                $sql .= " AND `event_id` = :event_id";
                $params['event_id'] = $event_id;
            }

            $sql .= " ORDER BY `timestamp` ASC ";

            $results = $this->conn->fetchAll($sql, $params);

            // check for accuracy
            if (empty($results)){
                $this->status = 1000;
                $this->msg = "The result set was empty";
                $this->results = array();
                return $this->compileReturn();
            }

            // return status/return/optionalMessage
            $this->status = 1000;
            $this->msg = "Successfully returned variation summar[y](ies)";
            $this->results = $results;
            return $this->compileReturn();
        }
        catch (Exception $e){
            $this->msg = "Error getting variation summary data: " . $e->getMessage();
            $this->results = array();
            $this->status = 1200;
            return $this->compileReturn();
        }
    }

    // as the name implies, a helper function to grab the application id from a test id
    protected function getAppIdFromTestId($test_id){
        try{
            if ($test_id == null || $test_id < 1) {
                throw new Exception("Test ID invalid");
            }
            $sql = "SELECT
                        `app_id`
                    FROM
                        `split_test`
                    WHERE
                        `id` = :test_id
                    ";
            $params = array(':test_id' => $test_id);
            $app_id = $this->conn->fetch($sql, $params);
            if (empty($app_id)) {
                throw new Exception("No information could be found for the test id");
            }
            return $app_id['app_id'];
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error while obtaining Application ID from test ID" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    protected function getAppIdFromContextId($section_id){
        try{
            if ($section_id == null || $section_id < 1) {
                throw new Exception("Context ID invalid");
            }
            $sql = "SELECT
                        `app_id`
                    FROM
                        `split_section`
                    WHERE
                        `id` = :section_id
                    ";
            $params = array(':section_id' => $section_id);
            $app_id = $this->conn->fetch($sql, $params);

            if (empty($app_id)) {
                throw new Exception("No information could be found for the context id");
            }

            return $app_id['app_id'];
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error while obtaining Application ID from context ID" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    public function insertTestAggStat($testAggStats){
        try{
            $tests = array( 'test_id' => 'int',
                            'event_id' => 'int',
                            'views' => 'int',
                            'conversions' => 'int',
                            'conversion_rate' => 'float');

            foreach ($testAggStats as $stat){
                $this->validateReqs($stat, $tests);
                $param = array();
                $test_id = $stat['test_id'];
                $test_id +=0;

                // Get app id from test id
                $app_id = $this->getAppIdFromTestId($test_id);
                $app_id +=0;
                $params[':app_id'] = $app_id;
                $params[':test_id'] = $stat['test_id'];
                $params[':event_id'] = $stat['event_id'];
                $params[':views'] = $stat['views'];
                $params[':conversions'] = $stat['conversions'];
                $params[':conversion_rate'] = $stat['conversion_rate'];

                $sql = "INSERT INTO
                            `cg_test_agg_stat`
                            (`app_id`,
                             `test_id`,
                             `event_id`,
                             `views`,
                             `conversions`,
                             `conversion_rate`)
                        VALUES
                            (:app_id,
                             :test_id,
                             :event_id,
                             :views,
                             :conversions,
                             :conversion_rate)
                         ON DUPLICATE KEY UPDATE
                        `views` = :views,
                        `conversions`= :conversions,
                        `conversion_rate` = :conversion_rate ";

                try{
                    $sqlRet = $this->conn->execute($sql, $params);
                }
                catch(Exception $e){
                    $this->msg =  "Error executing SQL for test agg stat: " . $e->getMessage() . "\n";
                    $this->status = 1930;
                    $this->results = array();
                    return $this->compileReturn();
                }

            }

            // finished without errors
            $this->status = 1000;
            $this->msg = "Successfully upserted all test agg stat data";
            $this->results = array('success' => 1);
            return $this->compileReturn();

        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error inserting test agg stats:" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    public function getTestAggStats($app_id, $optParams = array()){
        try{
            // validate optional parameters
            $test_id = null;
            $event_id = null;
            $tests = array(
                'test_id' => $test_id,
                'event_id' => $event_id);
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('test_id', $optParams)) {
                $test_id = $optParams['test_id'];
            }

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            // validate req params
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => $app_id);
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql = "SELECT
                        `app_id`,
                        `test_id`,
                        `event_id`,
                        `views`,
                        `conversions`,
                        `conversion_rate`
                    FROM
                        `cg_test_agg_stat`
                    WHERE
                        `app_id` = :app_id
                    ";
            if ($test_id != null){
                $sql .= " AND `test_id` = :test_id ";
                $params[':test_id'] = $test_id;
            }

            if ($event_id != null){
                $sql .= " AND `event_id` = :event_id";
                $params[':event_id'] = $event_id;
            }

            $testAggStats = $this->conn->fetchAll($sql, $params);

            if (empty($testAggStats)){
                $this->status = 1000;
                $this->results = array();
                $this->msg = "The result set was empty";
                return $this->compileReturn();
            }

            $this->status = 1000;
            $this->results = $testAggStats;
            $this->msg = "Successfully fetched the test agg stats";

            return $this->compileReturn();
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error fetching test agg stats:" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    public function insertVarAggStat($varAggStat){
        try{
            $tests = array( 'test_id' => 'int',
                            'event_id' => 'int',
                            'variation_id' => 'int',
                            'standard_deviation' => 'float',
                            'standard_error' => 'float',
                            'improvement' => 'float',
                            'pooled_sample_proportion' => 'float',
                            'estimated_standard_error' => 'float',
                            'two_proportion_obt_z' => 'float',
                            'expected_chi_squared' => 'float',
                            'chi_squared' => 'float',
                            'effect_size_chi_squared' => 'float',
                            'chi_significance' => 'bool',
                            'obt_z_significance' => 'bool',
                            'obt_z_difference' => 'float',
                            'normal_distribution' => 'float',
                            'views' => 'int',
                            'conversions' => 'int',
                            'conversion_rate' => 'float');

            foreach ($varAggStat as $stat){
                $this->validateReqs($stat, $tests);
                $param = array();
                $test_id = $stat['test_id'];
                $test_id +=0;

                // get app id from test id
                $app_id = $this->getAppIdFromTestId($test_id);
                $app_id +=0;
                $params[':app_id'] = $app_id;
                $params[':test_id'] = $stat['test_id'];
                $params[':event_id'] = $stat['event_id'];
                $params[':variation_id'] = $stat['variation_id'];
                $params[':standard_deviation'] = $stat['standard_deviation'];
                $params[':estimated_standard_error'] = $stat['estimated_standard_error'];
                $params[':standard_error'] = $stat['standard_error'];
                $params[':improvement'] = $stat['improvement'];
                $params[':pooled_sample_proportion'] = $stat['pooled_sample_proportion'];
                $params[':two_proportion_obt_z'] = $stat['two_proportion_obt_z'];
                $params[':expected_chi_squared'] = $stat['expected_chi_squared'];
                $params[':chi_squared'] = $stat['chi_squared'];
                $params[':effect_size_chi_squared'] = $stat['effect_size_chi_squared'];
                $params[':chi_significance'] = $stat['chi_significance'];
                $params[':obt_z_significance'] = $stat['obt_z_significance'];
                $params[':obt_z_difference'] = $stat['obt_z_difference'];
                $params[':normal_distribution'] = $stat['normal_distribution'];
                $params[':views'] = $stat['views'];
                $params[':conversions'] = $stat['conversions'];
                $params[':conversion_rate'] = $stat['conversion_rate'];

                $sql = "INSERT INTO
                            `cg_variation_agg_stat`
                            (`app_id`,
                             `test_id`,
                             `event_id`,
                             `variation_id`,
                             `standard_deviation`,
                             `standard_error`,
                             `estimated_standard_error`,
                             `improvement`,
                             `pooled_sample_proportion`,
                             `two_proportion_obt_z`,
                             `expected_chi_squared`,
                             `chi_squared`,
                             `effect_size_chi_squared`,
                             `chi_significance`,
                             `obt_z_significance`,
                             `obt_z_difference`,
                             `normal_distribution`,
                             `views`,
                             `conversions`,
                             `conversion_rate`)
                        VALUES
                            (:app_id,
                             :test_id,
                             :event_id,
                             :variation_id,
                             :standard_deviation,
                             :standard_error,
                             :estimated_standard_error,
                             :improvement,
                             :pooled_sample_proportion,
                             :two_proportion_obt_z,
                             :expected_chi_squared,
                             :chi_squared,
                             :effect_size_chi_squared,
                             :chi_significance,
                             :obt_z_significance,
                             :obt_z_difference,
                             :normal_distribution,
                             :views,
                             :conversions,
                             :conversion_rate)
                         ON DUPLICATE KEY UPDATE
                        `standard_deviation` = :standard_deviation,
                        `standard_error` = :standard_error,
                        `estimated_standard_error` = :estimated_standard_error,
                        `improvement` = :improvement,
                        `pooled_sample_proportion` = :pooled_sample_proportion,
                        `two_proportion_obt_z` = :two_proportion_obt_z,
                        `expected_chi_squared` = :expected_chi_squared,
                        `chi_squared` = :chi_squared,
                        `effect_size_chi_squared` = :effect_size_chi_squared,
                        `chi_significance` = :chi_significance,
                        `obt_z_significance` = :obt_z_significance,
                        `obt_z_difference` = :obt_z_difference,
                        `normal_distribution` = :normal_distribution,
                        `views` = :views,
                        `conversions`= :conversions,
                        `conversion_rate` = :conversion_rate ";

                try{
                    $sqlRet = $this->conn->execute($sql, $params);
                }
                catch (Exception $e){
                    $this->msg = "Error executing SQL for var agg stat: " . $e->getMessage() . "\n";
                    $this->status = 1930;
                    $this->results = array();
                    return $this->compileReturn();
                }
            }

            // reached here without error, return with success
            $this->status = 1000;
            $this->results = array('success' => 1);
            $this->msg = "Succesfully upserted variation agg stats";
            return $this->compileReturn();
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error inserting variation agg stats:" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    public function getVariationAggStats($app_id, $optParams = array()){
        try{
            // validate optional parameters
            $test_id = null;
            $event_id = null;
            $variation_id = null;

            $tests = array(
                'test_id' => 'int',
                'event_id' => 'int',
                'variation_id' => 'int',
            );

            if (array_key_exists('test_id', $optParams)) {
                $test_id = $optParams['test_id'];
            }

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            if (array_key_exists('variation_id', $optParams)) {
                $variation_id = $optParams['variation_id'];
            }

            $params = array(':app_id' => $app_id);
            $sql = "SELECT
                        vas.`app_id`,
                        vas.`variation_id`,
                        vas.`test_id`,
                        vas.`event_id`,
                        vas.`standard_deviation`,
                        vas.`standard_error`,
                        vas.`improvement`,
                        vas.`pooled_sample_proportion`,
                        vas.`estimated_standard_error`,
                        vas.`two_proportion_obt_z`,
                        vas.`expected_chi_squared`,
                        vas.`chi_squared`,
                        vas.`effect_size_chi_squared`,
                        vas.`chi_significance`,
                        vas.`obt_z_significance`,
                        vas.`obt_z_difference`,
                        vas.`normal_distribution`,
                        vas.`views`,
                        vas.`conversions`,
                        vas.`conversion_rate`,
                        vas_control.`views` as control_views,
                        vas_control.`conversions` as control_conversions
                    FROM `cg_variation_agg_stat` vas
                    JOIN `split_test` st ON st.id = vas.test_id
                    JOIN `cg_variation_agg_stat` vas_control ON
                        vas_control.variation_id = st.control_variation_id AND
                        vas_control.test_id = st.id AND
                        vas_control.event_id = vas.event_id
                    WHERE vas.app_id = :app_id
                ";

            if ($test_id != null){
                $sql .= " AND vas.`test_id` = :test_id";
                $params[':test_id'] = $test_id;
            }

            if ($event_id != null){
                $sql .= " AND vas.`event_id` = :event_id";
                $params['event_id'] = $event_id;
            }

            if ($variation_id != null){
                $sql .= " AND vas.`variation_id` = :variation_id";
                $params['variation_id'] = $variation_id;
            }

            $varAggStats = $this->conn->fetchAll($sql, $params);

            if (empty($varAggStats)){
                $this->status = 1000;
                $this->msg = "The result set was empty";
                $this->results = array();
                return $this->compileReturn();
            }

            // get the viewsNeeded for each set of stats
            foreach ($varAggStats as &$statSet) {
                $statSet['viewsNeeded'] = $this->calculateViewsNeeded($statSet['views'], $statSet['conversions'],
                                          $statSet['control_views'], $statSet['control_conversions']);
            }
            // return status/return/optionalMessage
            $this->status = 1000;
            $this->msg = "Successfully returned variation summar[y](ies)";
            $this->results = $varAggStats;

            return $this->compileReturn();
        }
        catch (Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error fetching variation agg stats:" . $e->getMessage();
            return $this->compileReturn();
        }
    }

    /**
     * see "Power analysis" at the following URL:  http://admin.instantcheckmate.com/wiki/images/d/d1/Formulas.png
     * @param int $testViews - the number of views of the given test
     * @param int $testConversions - the number of conversions of the given test
     * @param int $controlViews - the number of views of the control
     * @param int $controlConversions - the number of conversions of the control
     * @return NULL|number
     */
    protected function calculateViewsNeeded($testViews, $testConversions, $controlViews, $controlConversions) {

        // no views means no result (and no dividing by 0)
        if (!$testViews || !$controlViews) {
            return null;
        }

        $testCR = $testConversions/$testViews;
        $controlCR = $controlConversions/$controlViews;

        // is there is no difference, we don't want to divide by zero, and it's probably the control checking against itself
        if ($testCR == $controlCR) {
            return null;
        }

        // alpha = .1, beta = .8
        $zAlphaOverTwo = 1.645;
        $zBeta = .842;
        $zTotal = $zAlphaOverTwo + $zBeta;

        // Effect Size of the Difference
        $esDiff = $testCR - $controlCR;
        $variance = ($controlCR * (1 - $controlCR)) + ($testCR * (1 - $testCR));

        // Formula for estimating sample size
        $sampleSize = round($variance * ($zTotal * $zTotal) / ($esDiff * $esDiff));

        return $sampleSize - $testViews;
    }
}

