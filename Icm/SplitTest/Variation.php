<?php

class Icm_SplitTest_Variation extends Icm_Struct
{
    protected $conn;
    protected $redis;
    protected $tableName = 'split_variation';
    protected $pk = 'id';
        protected $sectionId;
        protected $testId;
        protected $variationId;
    protected $active;
    protected $weight;
    protected $metaIds;
        // Fadi's variables :
        public $id, $label, $meta_ids, $section_id, $status, $last_updated,
            $app_id, $key, $value, $sectionName, $appId, $slug, $sectionType, $defaultVariationId,
                $order, $path, $controlVariationId, $lastUpdated, $variationTable, $desc, $type;

        protected $data = array();

        private static $keyMap = array(
                   'id' => 'variationId',
                   'section_id' => 'sectionId',
                   'label' => 'label',
                   'meta_ids' => 'metaIds',
                   'status' => 'status',
                   'desc' => 'desc',
                   'type' => 'type',
                   'last_updated' => 'lastUpdated',
                   );


    /* STATIC METHODS THAT RETURN AN OBJECT */

    public static function getVariationById($variation, Icm_Db_Interface $conn, Icm_Redis $redis) {

        // make sure we have valid db and redis passed in
        // use the datacontainer if no db conection is passed in
        if (!isset($conn)) {
            $conn = Icm_Api::getInstance()->dataContainer->get('analyticsDbConnection');
        }

        if (!isset($conn)) {
            throw new Icm_Exception('no database connection provided');
        }

        if (!isset($variation)) {
            throw new Icm_Exception('no sectionId provided');
        }

        $sql = "SELECT
                  sv.section_id as sectionId,
                  sv.id as variationId,
                  sv.label as label,
                  sv.meta_ids as metaIds,
                  sv.status as status,
                  sv.desc as `desc`,
                  sv.type as type,
                  sv.last_updated as lastUpdated,
                  coalesce(stva.test_id, 0) as testId,
                  stva.weight as weight,
                  stva.active as active
                FROM split_variation sv
                LEFT JOIN split_test_variation_assoc stva on stva.variation_id = sv.id
                WHERE sv.id = :variation";
        $args = array(':variation' => $variation);

        Icm_Util_Logger_Syslog::getInstance('api')->logInfo($sql);
        $result = $conn->fetch($sql, $args);

        if (!$result) {
            throw new Icm_Exception('no variation for this id ' . $variation);
        }

        // no caching until we fix the clearing mechanism in admin
        // serialize the data
        // $str = json_encode($result);
        // set this variation in redis
        // $key = Icm_Util_SplitTest::makeVariationRedisKey($variation);
        // $redis->set($key, $str);


        // there's more data than just these three things..
        return new Icm_SplitTest_Variation($result, $conn);

    }


    // static functions that return an object...if you already have the data and need an object, use the Icm_SplitTest_Factory
    public static function getVariationBySectionId($sectionId, $variation, Icm_Db_Interface $conn, Icm_Redis $redis) {

        // make sure we have valid db and redis passed in
        // use the datacontainer if no db conection is passed in
        if (!isset($conn)) {
            $conn = Icm_Api::getInstance()->dataContainer->get('analyticsDbConnection');
        }

        if (!isset($conn)) {
            throw new Icm_Exception('no database connection provided');
        }

        if (!isset($sectionId)) {
            throw new Icm_Exception('no sectionId provided');
        }

        // get the data from the db
        // - the default_variation_id from the split_section table for this context (section)
        // - the data from split_variation
        // - the meta for meta_ids

        $sql = "";
        $args = array();
        $key = null;

        switch ($variation) {
            case "control":
                $sql = "SELECT
                          sc.id as sectionId,
                          sc.name as sectionName,
                          sc.app_id as appId,
                          sc.slug as slug,
                          sc.type as sectionType,
                          sc.default_variation_id as defaultVariationId,
                          sc.order as `order`,
                          sc.path as path,
                          sc.control_variation_id as controlVariationId,
                          sv.id as variationId,
                          sv.label as label,
                          sv.meta_ids as metaIds,
                          sv.status as status,
                          sv.desc as `desc`,
                          sv.type as type,
                          sv.last_updated as lastUpdated,
                          0 as testId,
                          stva.weight as weight,
                          stva.active as active
                        FROM split_section sc
                        JOIN split_variation sv on sv.id = sc.control_variation_id
                        LEFT JOIN split_test_variation_assoc stva on stva.variation_id = sv.id
                        WHERE sc.id = :section";
                $args = array(':section' => $sectionId);
                $key = Icm_Util_SplitTest::makeSectionControlVariationRedisKey($sectionId);
                break;
            case "default":
                $sql = "SELECT
                          sc.id as sectionId,
                          sc.name as sectionName,
                          sc.app_id as appId,
                          sc.slug as slug,
                          sc.type as sectionType,
                          sc.default_variation_id as defaultVariationId,
                          sc.order as `order`,
                          sc.path as path,
                          sc.control_variation_id as controlVariationId,
                          sv.id as variationId,
                          sv.label as label,
                          sv.meta_ids as metaIds,
                          sv.status as status,
                          sv.desc as `desc`,
                          sv.type as type,
                          sv.last_updated as lastUpdated,
                          0 as testId,
                          stva.weight as weight,
                          stva.active as active
                        FROM split_section sc
                        JOIN split_variation sv on sv.id = sc.default_variation_id
                        LEFT JOIN split_test_variation_assoc stva on stva.variation_id = sv.id
                        WHERE sc.id = :section";
                $args = array(':section' => $sectionId);
                $key = Icm_Util_SplitTest::makeSectionDefaultVariationRedisKey($sectionId);
                break;
            default:
                $sql = "SELECT
                          sv.section_id as sectionId,
                          sv.id as variationId,
                          sv.label as label,
                          sv.meta_ids as metaIds,
                          sv.status as status,
                          sv.desc as `desc`,
                          sv.type as type,
                          sv.last_updated as lastUpdated,
                          coalesce(stva.test_id, 0) as testId,
                          stva.weight as weight,
                          stva.active as active
                        FROM split_variation sv
                        LEFT JOIN split_test_variation_assoc stva on stva.variation_id = sv.id
                        WHERE sv.id = :variation and sv.section_id = :section";
                $args = array(':variation' => $variation, ':section' => $sectionId);
                break;
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logInfo($sql);
        $result = $conn->fetch($sql, $args);

        if (!$result) {
            throw new Icm_Exception('no variation is set for section ' . $sectionId. 'variation: ' . $variation);
        }


        // no cache for now until we can clear cache properly
        if ($key) {
            // serialize the data
            // $str = json_encode($result);
            // set in the section defaults in redis
            // $redis->set($key, $str);
        }


        // there's more data than just these three things..
        return new Icm_SplitTest_Variation($result, $conn);
    }

    public static function getDefaultBySectionId($sectionId, Icm_Db_Interface $conn, Icm_Redis $redis) {
        return self::getVariationBySectionId($sectionId, 'default', $conn, $redis);
    }

    public static function getControlBySectionId($sectionId, Icm_Db_Interface $conn, Icm_Redis $redis) {
        return self::getVariationBySectionId($sectionId, 'control', $conn, $redis);
    }


    /* OBJECT METHODS */

    // see Icm_SplitTest_Factory
    public function __construct($data=null, Icm_Db_Interface $conn = null) {
        $this->conn = $conn;

        // TODO - clean this up so you can't set arbitrary key/values
        if ($data) {
            foreach ($data as $key => $value) {
                $this->$key = $value;

            }
            // store a db table interface for vanilla variation table queries
        }

        if (!is_null($conn)) {
            // store a db table interface for vanilla variation table queries
            $this->variationTable = new Icm_Db_Table($this->tableName, $this->pk, $this->conn);
        }
    }

    // this class does not extend Icm_Db_Table - so use the magic method to invoke Icm_Db_Table methods
    public function __call($method, $args) {
        // TODO: write a function to handle the get and set funcs
        $data = $this->variationTable->findOneBy('id', $this->variationId);

        foreach ($data as $key => $value) {
          if (isset(self::$keyMap[$key])) {
            $mappedKey = self::$keyMap[$key];
          }
          else {
            $mappedKey = $key;
          }
          $this->$mappedKey = $value;
        }

        if (isset($this->$method)) {
            return $this->$method;
        }
        else {
            return;
        }
    }

    public function setSectionId($sectionId) {
        $this->sectionId = $sectionId;
    }

    public function getSectionId() {
        return $this->sectionId;
    }

    public function setTestId($testId) {
        $this->testId = $testId;
    }

    public function getTestId() {
        return $this->testId;
    }

    public function getActive() {
        return $this->active;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function getDesc() {
        return $this->desc;
    }

    public function getType() {
        return $this->type;
    }

    public function getMetaIds() {
        if (!isset($this->metaIds)) {
            $this->metaIds = $this->metaIds();
        }

        return $this->metaIds;
    }

    public function setVariationId($variationId) {
        $this->variationId = $variationId;
    }

    public function getVariationId() {
        return $this->variationId;
    }

    public function setMeta($key, $value) {
        $this->data[$key] = $value;
    }

    public function getMeta($key = null, $default = null) {

        if (sizeof($this->data <= 0)) {
               // get the meta data from the db
               $metaIds = $this->getMetaIds();
             $this->data = Icm_SplitTest_VariationMeta::getMetaByString($metaIds, $this->conn);
        }

       if ($key === null){
           return $this->data;
       }
       else {
           if (array_key_exists($key, $this->data)) {
               return $this->data[$key];
           }
       }

       return $default;
   }

    // this func has crazy logic to support backwards compat
    public function isActive() {
        // if we got here it means active and status are == 1, now make sure the test is LIVE
        $sql = "select status from split_test where id = :test";
        $testStatus = $this->conn->fetch($sql, array(':test' => $this->testId));

        if ($testStatus['status'] != 3) {
            return false;
        }

        return true;
    }

    public function save() {
        // upsert the variation data
        // testId must exist
        // save the meta too
    }
}
