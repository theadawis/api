<?php
interface Icm_Cacheable{
    public function setCacheManager(Zend_Cache_Manager $manager);

    public function getCacheManager();
}