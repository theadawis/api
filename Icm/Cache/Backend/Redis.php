<?php
/**
 * Redis backend for Zend's cache manager
 * @author Joe Linn
 *
 */
class Icm_Cache_Backend_Redis extends Zend_Cache_Backend implements Zend_Cache_Backend_Interface{
    protected $redis;


    public function __construct(array $options = array()){
        $this->redis = new Icm_Redis(Icm_Config::fromArray($options));
        parent::__construct($options);
    }

    /**
     * Set the frontend directives
     *
     * @param array $directives assoc of directives
     */
    public function setDirectives($directives){
        parent::setDirectives($directives);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else)
     *
     * Note : return value is always "string" (unserialization is done by the core not by the backend)
     *
     * @param  string  $id                     Cache id
     * @param  boolean $doNotTestCacheValidity If set to true, the cache validity won't be tested
     * @return string|false cached datas
    */
    public function load($id, $doNotTestCacheValidity = false){
        if (!$doNotTestCacheValidity && !$this->test($id)){
            return false;
        }
        return gzuncompress($this->redis->get($id));
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param  string $id cache id
     * @return mixed|false (a cache is not available) or "last modified" timestamp (int) of the available cache record
    */
    public function test($id){
        return $this->redis->exists($id);
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is always "string" (serialization is done by the
     * core not by the backend)
     *
     * @param  string $data            Datas to cache
     * @param  string $id              Cache id
     * @param  array $tags             Array of strings, the cache record will be tagged by each string entry
     * @param  int   $specificLifetime If != false, set a specific lifetime for this cache record (null => infinite lifetime)
     * @return boolean true if no problem
    */
    public function save($data, $id, $tags = array(), $specificLifetime = false){
        $lifetime = $this->getLifetime($specificLifetime);
        $result = $this->redis->setex($id, $lifetime, gzcompress($data));
        if (count($tags) > 0) {
            $this->_log('Tags are not supported by Redis backend.');
        }
        return $result;
    }

    /**
     * Remove a cache record
     *
     * @param  string $id Cache id
     * @return boolean True if no problem
    */
    public function remove($id){
        return $this->redis->del($id);
    }

    /**
     * Clean some cache records
     *
     * Available modes are :
     * Zend_Cache::CLEANING_MODE_ALL (default)    => remove all cache entries ($tags is not used)
     * Zend_Cache::CLEANING_MODE_OLD              => remove too old cache entries ($tags is not used)
     * Zend_Cache::CLEANING_MODE_MATCHING_TAG     => remove cache entries matching all given tags
     *                                               ($tags can be an array of strings or a single string)
     * Zend_Cache::CLEANING_MODE_NOT_MATCHING_TAG => remove cache entries not {matching one of the given tags}
     *                                               ($tags can be an array of strings or a single string)
     * Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG => remove cache entries matching any given tags
     *                                               ($tags can be an array of strings or a single string)
     *
     * @param  string $mode Clean mode
     * @param  array  $tags Array of tags
     * @return boolean true if no problem
    */
    public function clean($mode = Zend_Cache::CLEANING_MODE_ALL, $tags = array()){
        switch ($mode) {
            case Zend_Cache::CLEANING_MODE_ALL:
                if (($keys = $this->redis->keys('*_cache')) && is_array($keys)){
                    // Redis doesn't do wildcard deletion, so iterate...
                    foreach ($keys as $key){
                        $this->redis->del($key);
                    }
                }
                return true;
                break;
            case Zend_Cache::CLEANING_MODE_OLD:
                $this->_log("Icm_Cache_Backend_Redis::clean() : CLEANING_MODE_OLD is unsupported by the Redis backend");
                break;
            case Zend_Cache::CLEANING_MODE_MATCHING_TAG:
            case Zend_Cache::CLEANING_MODE_NOT_MATCHING_TAG:
            case Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG:
                $this->_log('Tags are not supported by Redis backend.');
                break;
            default:
                Zend_Cache::throwException('Invalid mode for clean() method');
                break;
        }
    }
}
?>