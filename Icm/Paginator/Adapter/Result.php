<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Paginator_Adapter_Result implements Zend_Paginator_Adapter_Interface{
    protected $result;

    public function __construct(Icm_Search_Result $result){
        $this->result = $result;
    }

    /* (non-PHPdoc)
     * @see Countable::count()
     */
    public function count(){
        return $this->result->count();
    }

    /**
     * Returns an collection of items for a page.
     *
     * @param  integer $offset Page offset
     * @param  integer $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage){
        return array_slice($this->result->getData(), $offset, $itemCountPerPage);
    }
}