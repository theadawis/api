<?php
/**
 * ElasticSearch.php
 * User: chris
 * Date: 2/25/13
 * Time: 8:51 AM
 */
class Icm_Log_Writer_ElasticSearch extends Zend_Log_Writer_Abstract {

    protected $client;

    protected static $FILTER_FIELDS = array(
        "cc_number", "cc_expire_Month", "cc_expire_Year", "cc_code", //Icm Fields
        "ccnum", "exp_year", "exp_month",
        "context", //This will have the CI object and crap in it, which is bad.
    );

    public function __construct(
        Icm_Service_Internal_Elasticsearch $client,
        Icm_Config $config,
        Icm_Visit_Tracker $visitTracker,
        $get,
        $post,
        $analyticsTracker
    ) {
        $this->client = $client;
        $this->config = $config;
        $this->visitTracker = $visitTracker;
        $this->get = $get;
        $this->post = $post;
        $this->analyticsTracker = $analyticsTracker;
    }


    /**
     * Write a message to the log.
     *
     * @param  array  $event  log data event
     *
     * @return void
     */
    protected function _write($event) {

        $event = array_merge($event, array(
            "app_id" => $this->config->getSection("global")->getOption("app_id"),
            "app_name" => $this->config->getSection("global")->getOption("app_name"),
            "visitor_id" => $this->visitTracker->getVisitorId(),
            "session" => isset($_SESSION) ? $_SESSION : null,
            "get_args" => $this->get,
            "post_args" => $this->post,
            "tests" => $this->analyticsTracker->getTestContext()->toArray(),
            "request" => $this->analyticsTracker->getRequestContext()->toArray(),
            "_timestamp" => time() * 1000,
            // "_ttl" => (86400 * 2)*1000,

        ));

        foreach (self::$FILTER_FIELDS as $field) {
            if (array_key_exists($field, $event)) {
                unset($event[$field]);
            }
        }

        if (array_key_exists("exception_class", $event)) {
            $type = "exception";
        } elseif (array_key_exists("line", $event)) {
            $type = "error";
        } else {
            $type = "message";
        }

        $resp = $this->client->getClient()->addDocuments(array(new Elastica_Document(null, $event, $type, "errors")));
    }

    /**
     * Construct a Zend_Log driver
     *
     * @param  array|Zend_Config $config
     *
     * @return Zend_Log_FactoryInterface
     */
    static public function factory($config) {
        throw new Exception("Not implemented");
    }
}
