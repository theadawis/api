<?php
/**
 * ICM Data Structure Class
 * @author Joe Linn
 *
 */
abstract class Icm_Struct implements IteratorAggregate, ArrayAccess{
    /**
     * Create a data structure from an associative array
     * @param array $data
     * @param bool $filter true: will ignore any array keys which are not pre-defined properties of your struct; false: will throw the usual exception if you attempt to use keys which are not defined
     * @return Icm_Struct
     */
    public static function fromArray(array $data, $filter = false){
        $struct = new static();
        foreach ($data as $key => $value){
            if (!$filter || property_exists($struct, $key)){
                $struct->$key = $value;
            }
        }
        return $struct;
    }

    public function getIterator(){
        return new ArrayIterator($this->toArray());
    }

    /**
     * Generates an associative array comprised of the current structure's properties
     * @return array
     */
    public function toArray(){
        // return get_object_vars($this);
        $reflection = new ReflectionClass($this);
        $publicProperties = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
        $properties = array();
        foreach ($publicProperties as $property) {
            /**
             * @var ReflectionProperty $property
             */
            $properties[$property->getName()] = $property->getValue($this);
        }
        return $properties;
    }

    public function __set($name, $value){
        $this->checkProperty($name);
    }

    public function __get($name){
        $this->checkProperty($name);
    }

    /**
     * Checks to ensure that property $name has been defined.
     * @param string $name
     * @throws Icm_Exception
     */
    final private function checkProperty($name){
        $class = get_called_class();
        if (!property_exists($class, $name)){
            throw new Icm_Exception("Property $name is not defined in $class. Shame! Shame!!");
            return false;
        }
        return true;
    }

    public function offsetExists($offset){
        return $this->checkProperty($offset);
    }

    public function offsetGet($offset){
        return $this->$offset;
    }

    public function offsetSet($offset, $value){
        $this->$offset = $value;
    }

    public function offsetUnset($offset){
        $this->$offset = NULL;
    }
}