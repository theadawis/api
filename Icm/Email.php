<?php

/**
 * Email processing class
 *
 * @category Api
 * @package Api_Email
 * @subpackage Api_Email
 */

class Icm_Email {

    const EMAIL_RULE_INTERNAL_VALID             = 1;
    const EMAIL_RULE_INTERNAL_INVALID_CHARACTER = 101;
    const EMAIL_RULE_INTERNAL_ADDRESS_PARTIAL   = 201;
    const EMAIL_RULE_INTERNAL_ADDRESS_EXACT     = 301;
    const EMAIL_RULE_INTERNAL_DOMAIN_EXACT      = 401;

    /**
     * @var Icm_Config
     */
    protected $config;

    public function __construct(Icm_Config $config) {
        $this->config = $config;
        $this->cgdb = Icm_Db_Pdo::connect('cgDb', $this->config->getSection('cgDb'));
    }

    /**
     * Update error counter
     *
     * @param integer $validationResult
     */
    protected function updateCounter($validationResult) {
        $statStorageAdapter = new Icm_Email_Stats_StorageAdapter($this->cgdb);
        $stat = $statStorageAdapter->getByRuleCreated($validationResult, date("Y-m-d"));

        // insert or update counter record
        if (!$stat) {
            $stat = new Icm_Email_Stats;
            $stat->email_stat_rule = $validationResult;
            $stat->email_stat_count = 1;
        } else {
            $stat->email_stat_count += 1;
        }
        $statStorageAdapter->save($stat);
    }

    /**
     * Update customer email score
     *
     * @param integer $customerId
     * @param integer $validationResult
     */
    protected function updateCustomerEmailScore($customerId, $validationResult) {
        $customerStorage = new Icm_Commerce_Customer_StorageAdapter('customer', 'customer_id', $this->cgdb);
        $customer = $customerStorage->getById($customerId);
        $customer->email_score = $validationResult;
        $customerStorage->save($customer);
    }

    /**
     * Validate email address against internal rules
     *
     * @param string $emailAddress
     *
     * @return integer validation result (1=Valid, >1 Invalid)
     */
    public function validate($emailAddress) {
        // load validation file
        $ruleConfig = Icm_Api::getInstance()->getConfigFileFromIni('emailrules.ini');
        $rules = $ruleConfig->getOption('specific');
        $emailChunks = preg_split('/@/', $emailAddress);

        // characters
        $pattern = '/' . $rules['characters-partial'] . '/';

        if (preg_match($pattern, $emailAddress, $matches)) {
            $this->updateCounter(self::EMAIL_RULE_INTERNAL_INVALID_CHARACTER);
            return self::EMAIL_RULE_INTERNAL_INVALID_CHARACTER;
        }

        // address-partial
        foreach (explode(',', $rules['address-partial']) as $rule) {
            $ruleRegEx = preg_replace('/\*/', '.*', $rule);

            if (preg_match("/$ruleRegEx/", $emailAddress, $matches)) {
                $this->updateCounter(self::EMAIL_RULE_INTERNAL_ADDRESS_PARTIAL);
                return self::EMAIL_RULE_INTERNAL_ADDRESS_PARTIAL;
            }
        }

        // address-exact
        foreach (explode(',', $rules['address-exact']) as $rule) {
            $pattern = "/^$rule@/";

            if (preg_match($pattern, $emailAddress, $matches)) {
                $this->updateCounter(self::EMAIL_RULE_INTERNAL_ADDRESS_EXACT);
                return self::EMAIL_RULE_INTERNAL_ADDRESS_EXACT;
            }
        }

        // domain-exact
        foreach (explode(',', $rules['domain-exact']) as $rule) {
            $ruleRegEx = preg_replace('/\*/', '.*', $rule);
            $pattern = "/^$ruleRegEx$/";

            if (preg_match($pattern, $emailChunks[1], $matches)) {
                $this->updateCounter(self::EMAIL_RULE_INTERNAL_DOMAIN_EXACT);
                return self::EMAIL_RULE_INTERNAL_DOMAIN_EXACT;
            }
        }

        // valid email
        return self::EMAIL_RULE_INTERNAL_VALID;
    }

    /**
     * Validate then send email
     *
     * @param array $emailData
     * @param boolean $skipValidation if TRUE, skip internal rules testing
     * 
     * @return boolean true on success, false on error
     */
    public function send(array $emailData, $skipValidation = false) {
        if ($skipValidation) {
            $validationResult = self::EMAIL_RULE_INTERNAL_VALID;
        } else {
            $validationResult = $this->validate($emailData['meta']['email']);
            $this->updateCustomerEmailScore($emailData['customer_id'], $validationResult);
        }

        if ($validationResult === self::EMAIL_RULE_INTERNAL_VALID) {
            $queueFactory = new Icm_QueueFactory($this->config->getSection('rabbitmq'));

            if($skipValidation) {
                $queue = $queueFactory->getEmailSendQueue();
            } else {
                $queue = $queueFactory->getEmailValidateQueue();
            }

            $partners_list = explode(',', $this->config->getSection('partners')->getOption('partnersList'));
            $message = array(
                "email_data"        => $emailData,
                "partners_list"     => $partners_list
            );
            $queue->send($message);

            return true;
        } else {
            return false;
        }
    }

}
