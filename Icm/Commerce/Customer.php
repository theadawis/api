<?php
class Icm_Commerce_Customer extends Icm_Struct{
    public
        $customer_id,
        $email,
        $email_score,
        $password, //this should only be set when adding a customer to Orange
        $first_name,
        $last_name,
        $guid,
        $created,
        $updated,
        $phone,
        $address1,
        $address2,
        $city,
        $state,
        $preferred_gateway,
        $zip = '',
        $forgot_password_hash,
        $forgot_password_expires,
        $password_last_changed,
        $crm_config_id,
        $app_id,
        $is_test;

    /**
     * @var Icm_Struct_Tracking_Visit
     */
    protected $visit;

    /**
     * @var Icm_Commerce_Creditcard
     */
    protected $creditcard;

    /**
     * @var Icm_SplitTest_Variation
     */
    protected $registrationVariation;

    /**
     * @var Icm_Commerce_Gateway_Interface
     */
    protected $gateway;

    /**
     * @var array
     */
    protected $credits = array();

    public function getGateway() {
        if ($this->gateway && is_a($this->gateway, "Icm_Commerce_Gateway_Interface")) {
            return $this->gateway;
        }
        throw new Icm_Exception("Attempting to retrieve a gateway when no gateway has been set.");
    }

    public function setGateway(Icm_Commerce_Gateway_Abstract $gateway) {
        $this->gateway = $gateway;
        if (!$this->preferred_gateway) {
            $this->preferred_gateway = $gateway->getGatewayId();
        }
        return $this;
    }

    /**
     * @param Icm_SplitTest_Variation $variation
     * @return Icm_Commerce_Customer
     */
    public function setVaration(Icm_SplitTest_Variation $variation){
        $this->registrationVariation = $variation;
        return $this;
    }

    /**
     * @return Icm_SplitTest_Variation
     */
    public function getVaration(){
        if (!$this->registrationVariation instanceof Icm_SplitTest_Variation){
            throw new Icm_Exception("Attempting to retrieve a variation when no variation has been set.");
        }
        return $this->registrationVariation;
    }

    /**
     * @param Icm_Struct_Tracking_Visit $visit
     * @return Icm_Commerce_Customer
     */
    public function setVisit(Icm_Struct_Tracking_Visit $visit){
        $this->visit = $visit;
        return $this;
    }

    /**
     * @return Icm_Struct_Tracking_Visit
     * @throws Icm_Exception
     */
    public function getVisit(){
        if ($this->visit && is_a($this->visit, "Icm_Struct_Tracking_Visit")){
            return $this->visit;
        }
        throw new Icm_Exception("Attempting to retrieve a visit when none has been set.");
    }

    /**
     * @param Icm_Commerce_Creditcard $card
     * @return Icm_Commerce_Customer
     */
    public function setCreditCard(Icm_Commerce_Creditcard $card){
        $this->creditcard = $card;
        return $this;
    }

    /**
     * @return Icm_Commerce_Creditcard
     */
    public function getCreditCard(){
        if (!$this->creditcard instanceof Icm_Commerce_Creditcard){
            throw new Icm_Commerce_Exception("Attempting to get a Creditcard object when none has been set.");
        }
        return $this->creditcard;
    }

    public function validate() {
        if (is_null($this->creditcard)) {
            throw new Icm_Service_Litle_Exception("The customers credit card object cannot be null");
        }
        $this->creditcard->validate();
    }

    /**
     * @param Icm_Commerce_Credits $credits
     * @return Icm_Commerce_Customer
     */
    public function setCredits($credits_type_id, Icm_Commerce_Customer_Credits $credits){
        $this->credits[$credits_type_id] = $credits;
        return $this;
    }

    /**
     * @return Icm_Commerce_Customer_Credits
     */
    public function getCredits($credits_type_id){
        if (!$this->credits[$credits_type_id] instanceof Icm_Commerce_Customer_Credits){
            throw new Icm_Commerce_Exception("Attempting to get a Credits object when none has been set.");
        }
        return $this->credits[$credits_type_id];
    }

    public function isTest() {
        return (bool)$this->is_test;
    }
}

