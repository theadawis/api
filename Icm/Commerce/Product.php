<?php
/**
 * TCG Product object
 * @author Joe Linn
 */
class Icm_Commerce_Product extends Icm_Struct implements Icm_Hashable{
    const TYPE_STRAIGHT_SALE = 'straight_sale';
    const TYPE_TRIAL = 'trial';
    const TYPE_NON_RECURRING = 'non_recurring';

    public $product_id,
        $name,
        $sku,
        $billing_type,
        $initial_price,
        $recurring_price,
        $trial_days,
        $membership_months,
        $product_tier,
        $credits,
        $product_type,
        $credits_type_id,
        $status,
        $created,
        $updated,
        $orange_billing_plan_id,
        $is_upsell,
        $orange_program_id,
        $orange_cust_token,
        $fallback_product_id;

    public function getProductType() {
        return $this->product_type;
    }

    public function getHash(){
        return $this->product_id; //ICM product IDs should be absolutely unique
    }

    /**
     * Return -1 for single orders; 1 for recurring orders
     * @return int
     */
    public function getContinuity(){
        return $this->billing_type == self::TYPE_NON_RECURRING ? -1 : 1;
    }

    public function getCode() {
        switch($this->billing_type) {
            case self::TYPE_TRIAL:
                return "TR";
            case self::TYPE_STRAIGHT_SALE:
                return "SS";
            case self::TYPE_NON_RECURRING:
                return "SO";
        }
    }
}
