<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Commerce_Cart{

    const MODE_TEST = 1;
    const MODE_PRODUCTION = 0;

    /**
     * @var int
     */
    protected $mode = self::MODE_PRODUCTION;

    /**
     * @var Icm_Commerce_Order
     */
    protected $order;

    /**
     * @var Icm_Commerce_Crm_Interface
     */
    protected $crm;

    /**
     * @var Icm_Commerce_Gateway_Interface
     */
    protected $gateway;

    /**
     * @var Icm_Commerce_Gateway_Interface
     */
    protected $decidedGateway;

    /**
     * @var Icm_Db_Pdo
     */
    protected $conn;

    /**
     * @var Icm_Commerce_Product_Gateway_StorageInterface
     */
    protected $productGateway;

    /**
     * @var Icm_Commerce_Order_StorageInterface
     */
    protected $orderStorage;

    /**
     * @var Icm_Commerce_Transaction_StorageInterface
     */
    protected $transactionStorage;

    /**
     * @var Icm_Commerce_Customer_StorageInterface
     */
    protected $customerStorage;

    /**
     * @var Icm_Commerce_Order_Product_StorageInterface
     */
    protected $orderProductStorage;

    /**
     * @var Icm_Commerce_Customer_Guids_StorageInterface
     */
    protected $customerGuidsStorage;

    /**
     * @var Icm_Commerce_Transaction
     */
    protected $transaction;

    /**
     * @var Icm_Commerce_Transaction
     */
    protected $refundTransaction;

    public $cancelAndArchive;

    public function __construct(Icm_Commerce_Order $order,
                                Icm_Commerce_Crm_Interface $crm,
                                Icm_Commerce_Gateway_Interface $gateway,
                                Icm_Db_Pdo $conn){
        $this->order = $order;
        $this->crm = $crm;
        $this->gateway = $gateway;
        $this->decidedGateway = $gateway;
        $this->conn = $conn;
    }

    /**
     * checkout upsell only
     * @param Icm_Commerce_Customer $customer
     */
    public function checkoutUpsell(Icm_Commerce_Customer $customer) {
        // Set the test state
        $this->updateTestState($customer);

        // assign visit info to order and save
        $this->relateOrderToVisit($customer);

        /**
         * @var Icm_Commerce_Transaction $transaction
         */
        $transaction = Icm_Commerce_Transaction::fromArray(array(
                'order_id' => $this->order->order_id,
                'type' => Icm_Commerce_Transaction::TYPE_PURCHASE,
                'amount' => $this->order->getSubtotal(),
                'gateway_id' => $this->getGateway()->getGatewayId(),
                'card_type' => $customer->getCreditCard()->getCardType()
        ));
        $this->setTransaction($transaction);

        $gatewayResponse = $this->processTransaction($customer);

        $this->getTransaction()->failure = 0;

        $this->getTransaction()->gateway_txid = $gatewayResponse->getTransactionId(); //wait for the Orange GUID to store the successful transaction

        $this->addTransactionToCrm($customer);

        $this->persistOrderProducts($customer);

        // save the transaction
        $this->getTransactionStorage()->save($this->getTransaction());

        // Save the order
        $this->order->is_test = $customer->getCreditCard()->isTestCCNumber();
        $this->getOrderStorage()->save($this->order);

        $this->crm->addComment($customer,
                               "This customer purchased the PDF Upsell on " . date("Y-m-d"),
                               "PDF download enabled for customer");

        if ($this->cancelAndArchive) {
            $this->crm->cancelCustomer($customer);
            $this->crm->archiveCustomer($customer);
        }
    }

    /**
     * @param Icm_Commerce_Customer $customer
     * @throws Exception|Icm_Commerce_Crm_Exception|Icm_Commerce_Crm_TransactionError
     */
    public function checkout(Icm_Commerce_Customer $customer) {
        if (!$this->hasProducts()) {
            throw new Icm_Commerce_Exception("Cart must have atleast one product to purchase");
        }

        /*
         * The customer may have a "preferred gateway" -- If they've already done a transaction, we want to keep them
         * on the same gateway and bank, so any chargeback will only count as 1, instead of >1, if they were to be spread
         * out across multiple gateways/banks.
         */
        try{
            $gateway = $customer->getGateway();
        } catch (Exception $e) {
            $gateway = $this->getDecidedGateway();
            $customer->setGateway($gateway); //set the customer's preferred gateway -- we only want to do this if they dont have one already
        }

        $this->setGateway($gateway);

        // Set the test state
        $this->updateTestState($customer);

        // Make sure the order has relations to the visit
        $this->relateOrderToVisit($customer);

        /**
         * @var Icm_Commerce_Transaction $transaction
         */
        $transaction = Icm_Commerce_Transaction::fromArray(array(
                'gateway_id' => $gateway->getGatewayId(),
                'order_id' => $this->order->order_id,
                'type' => Icm_Commerce_Transaction::TYPE_PURCHASE,
                'amount' => $this->order->getSubtotal(),
                'card_type' => $customer->getCreditCard()->getCardType()
        ));

        $this->setTransaction($transaction);

        $gatewayResponse = $this->processTransaction($customer);

        // transaction succeeded; store the gateway transaction id in the transaction object for later storage
        $this->getTransaction()->failure = 0;

        /**
         * @var Icm_Commerce_Gateway_Response $gatewayResponse
         */
        $this->getTransaction()->gateway_txid = $gatewayResponse->getTransactionId(); //wait for the Orange GUID to store the successful transaction

        $this->addCustomerToCrm($customer);
        $this->addTransactionToCrm($customer);

        // customer creation and transaction storage succeeded.
        $this->persistOrderProducts($customer);

        // save the transaction
        $this->getTransactionStorage()->save($this->getTransaction());

        // Save the order
        $this->order->is_test = $customer->getCreditCard()->isTestCCNumber();
        $this->getOrderStorage()->save($this->order);

        if ($this->cancelAndArchive) {
            $this->crm->cancelCustomer($customer);
            $this->crm->archiveCustomer($customer);
        }
    }


    /**
     * @param Icm_Commerce_Customer $customer
     * @throws Exception|Icm_Commerce_Crm_Exception|Icm_Commerce_Crm_TransactionError
     */
    public function checkoutCredits(Icm_Commerce_Customer $customer) {
        if (!$this->hasProducts()) {
            throw new Icm_Commerce_Exception("Cart must have atleast one product to purchase");
        }

        /*
         * The customer may have a "preferred gateway" -- If they've already done a transaction, we want to keep them
         * on the same gateway and bank, so any chargeback will only count as 1, instead of >1, if they were to be spread
         * out across multiple gateways/banks.
         */
        try{
            $gateway = $customer->getGateway();
        } catch (Exception $e) {
            $gateway = $this->getDecidedGateway();
            $customer->setGateway($gateway); //set the customer's preferred gateway -- we only want to do this if they dont have one already
        }

        $this->setGateway($gateway);

        // Set the test state
        $this->updateTestState($customer);

        // $reportGroup = $this->generateReportGroup($customer, $gateway); //Litle and Orange require a report group

        // Make sure the order has relations to the visit
        $this->relateOrderToVisit($customer);

        /**
         * @var Icm_Commerce_Transaction $transaction
         */
        $transaction = Icm_Commerce_Transaction::fromArray(array(
                'gateway_id' => $gateway->getGatewayId(),
                'order_id' => $this->order->order_id,
                'type' => Icm_Commerce_Transaction::TYPE_PURCHASE,
                'amount' => $this->order->getSubtotal(),
                'card_type' => $customer->getCreditCard()->getCardType()
        ));

        $this->setTransaction($transaction);

        $gatewayResponse = $this->processTransaction($customer);

        // transaction succeeded; store the gateway transaction id in the transaction object for later storage
        $this->getTransaction()->failure = 0;

        /**
         * @var Icm_Commerce_Gateway_Response $gatewayResponse
         */
        $this->getTransaction()->gateway_txid = $gatewayResponse->getTransactionId(); //wait for the Orange GUID to store the successful transaction

        $this->addTransactionToCrm($customer);

        // customer creation and transaction storage succeeded.
        $this->persistOrderProducts($customer);

        // save the transaction
        $this->getTransactionStorage()->save($this->getTransaction());
        // Save the order
        $this->getOrderStorage()->save($this->order);

        if ($this->cancelAndArchive) {
            $this->crm->cancelCustomer($customer);
            $this->crm->archiveCustomer($customer);
        }

    }


    public function refund(Icm_Commerce_Customer $customer, $cancelAndArchive = true) {
        $gateway = $customer->getGateway();
        $this->setGateway($gateway);
        $this->setTransaction($this->getTransactionStorage()->getFirstForOrder($this->order->getOrderId()));
        $this->refundTransaction($customer);
        $products = $this->order->getProducts()->getKeySet();
        $product = $products[0];
        $crmResponse = $this->crm->refund($customer, $this->getTransaction(), $this->getProductGateway()->get($product, $gateway));
        $this->getRefundTransaction()->orange_guid = $crmResponse->q_tran_guid;
        $this->getTransactionStorage()->save($this->getRefundTransaction());
        $this->crm->addComment($customer, "Customer Refunded via website", "Refund");

        if ($cancelAndArchive) {
            $this->crm->cancelCustomer($customer);
            $this->crm->archiveCustomer($customer);
            $this->crm->addComment($customer, "Customer canceled and archived via website", "Cancel and Archive");
        }

        $this->deactivateProductsForOrder();
    }


    /**
     * Add a Product to the cart
     * You probably dont want to use this yet.
     * @param Icm_Commerce_Product $product
     * @param int $quantity
     */
    public function addProduct(Icm_Commerce_Product $product, $quantity = 1){
        if ($this->isValidQuantity($quantity)) {
            $this->order->addProduct($product, $quantity);
        }
    }

    public function setProduct(Icm_Commerce_Product $product){
        $this->order->removeAllProducts();
        $this->order->addProduct($product, 1);
    }

    /**
     * @return boolean
     */
    public function hasProducts(){
        return $this->order->hasProducts();
    }

    /**
     * @param Icm_Commerce_Product $product
     * @return number
     */
    public function getQuantity(Icm_Commerce_Product $product){
        return $this->order->getQuantity($product);
    }

    /**
     * @param Icm_Commerce_Product $product
     * @param int $quantity
     */
    public function setQuantity(Icm_Commerce_Product $product, $quantity){
        $this->order->setQuantity($product, $quantity);
    }

    /**
     * @return Icm_HashMap
     */
    public function getProducts(){
        return $this->order->getProducts();
    }

    /**
     * @return Icm_Commerce_Gateway_Interface
     */
    public function getGateway(){
        return $this->gateway;
    }

    /**
     * @return number
     */
    public function getSubtotal(){
        return $this->order->getSubtotal();
    }

    /**
     * Remove all current cart items
     */
    public function emptyCart(){
        $this->order->removeAllProducts();
    }

    public function generateReportGroup(Icm_Commerce_Customer $customer) {
        /**
         * @var array $products
         */
        $products = $this->getProducts()->getKeySet();
        /**
         * @var Icm_Commerce_Product $product
         */
        $product = $products[0];

        $productCode = $product->getCode();
        $affShortCode = $customer->getVisit()->getAffiliate()->short_name;
        $productType = $product->getProductType();
        $gatewayCode = $this->gateway->getReportGroupCode();

        return "ICM_{$gatewayCode}_{$productType}_{$productCode}_{$affShortCode}";
    }

    /**
     * @param int $quantity
     * @throws Exception
     * @return boolean
     */
    protected function isValidQuantity($quantity){
        if (!is_numeric($quantity) || $quantity < 0){
            throw new Exception("Quantity must be a positive number.");
        }

        return true;
    }

    /**
     * @param Icm_Commerce_Order_Product_StorageInterface $orderProductStorage
     */
    public function setOrderProductStorage(Icm_Commerce_Order_Product_StorageInterface $orderProductStorage) {
        $this->orderProductStorage = $orderProductStorage;
    }

    /**
     * @param Icm_Commerce_Customer_Guids_StorageInterface $orderProductStorage
     */
    public function setCustomerGuidsStorage(Icm_Commerce_Customer_Guids_StorageInterface $customerGuidsStorage) {
        $this->customerGuidsStorage = $customerGuidsStorage;
    }

    public function setOrder(Icm_Commerce_Order $order) {
        $this->order = $order;
    }

    public function getOrder() {
        return $this->order;
    }

    public function getOrderId() {
        return $this->order->order_id;
    }

    /**
     * @return Icm_Commerce_Order_Product_StorageInterface
     */
    public function getOrderProductStorage() {
        if (is_null($this->orderProductStorage)) {
            $this->orderProductStorage = new Icm_Commerce_Order_Product_StorageAdapter('order_products', 'order_product_id', $this->conn);
        }

        return $this->orderProductStorage;
    }

    /**
     * @return Icm_Commerce_Customer_Guids_StorageInterface
     */
    public function getCustomerGuidsStorage() {
        if (is_null($this->customerGuidsStorage)) {
            $this->customerGuidsStorage = new Icm_Commerce_Customer_Guids_StorageAdapter('customer_guids', 'customer_guid_id', $this->conn);
        }

        return $this->customerGuidsStorage;
    }

    /**
     * @param Icm_Commerce_Order_StorageInterface $orderStorage
     */
    public function setOrderStorage(Icm_Commerce_Order_StorageInterface $orderStorage) {
        $this->orderStorage = $orderStorage;
    }

    /**
     * @return Icm_Commerce_Order_StorageInterface
     */
    public function getOrderStorage() {
        if (is_null($this->orderStorage)) {
            $this->orderStorage = new Icm_Commerce_Order_StorageAdapter('orders', 'order_id', $this->conn);
        }

        return $this->orderStorage;
    }

    /**
     * @param Icm_Commerce_Product_Gateway_StorageInterface $productGateway
     */
    public function setProductGateway(Icm_Commerce_Product_Gateway_StorageInterface $productGateway){
        $this->productGateway = $productGateway;
    }

    /**
     * @return Icm_Commerce_Product_Gateway_StorageInterface
     */
    public function getProductGateway() {
        if (is_null($this->productGateway)) {
            $this->productGateway = new Icm_Commerce_Product_Gateway_StorageAdapter('product_gateway', '', $this->conn);
        }

        return $this->productGateway;
    }

    /**
     * @param Icm_Commerce_Transaction_StorageInterface $transactionStorage
     */
    public function setTransactionStorage(Icm_Commerce_Transaction_StorageInterface $transactionStorage) {
        $this->transactionStorage = $transactionStorage;
    }

    /**
     * @return Icm_Commerce_Transaction_StorageInterface
     */
    public function getTransactionStorage() {
        if (is_null($this->transactionStorage)) {
            $this->transactionStorage = new Icm_Commerce_Transaction_StorageAdapter('transactions', 'transaction_id', $this->conn);
        }

        return $this->transactionStorage;
    }

    /**
     * @param Icm_Commerce_Customer_StorageInterface $customerStorage
     */
    public function setCustomerStorage(Icm_Commerce_Customer_StorageInterface $customerStorage){
        $this->customerStorage = $customerStorage;
    }

    /**
     * @return Icm_Commerce_Customer_StorageInterface
     */
    public function getCustomerStorage() {
        if (is_null($this->customerStorage)) {
            $this->customerStorage = new Icm_Commerce_Customer_StorageAdapter('customer', 'customer_id', $this->conn);
        }

        return $this->customerStorage;
    }

    protected function relateOrderToVisit(Icm_Commerce_Customer $customer) {
        $visit = $customer->getVisit();
        $this->order->app_id = $visit->app_id;
        $this->order->visit_id = $visit->visit_id;
        $this->order->visitor_id = $visit->visitor_id;
          $this->order->customer_id = $customer->customer_id;
        $this->order->is_test = $customer->getCreditCard()->isTestCCNumber();
        $this->getOrderStorage()->save($this->order);
    }

    public function  setGateway(Icm_Commerce_Gateway_Interface $gateway) {
        $this->gateway = $gateway;
    }

    public function getDecidedGateway() {
        return $this->decidedGateway;
    }

    public function getTransaction() {
        if (is_null($this->transaction)) {
            throw new Icm_Commerce_Exception("Transaction has not been set yet");
        }

        return $this->transaction;
    }

    protected function setTransaction(Icm_Commerce_Transaction $transaction) {
        $this->transaction = $transaction;
    }

    /**
     * @param $customer
     * @return Icm_Commerce_Gateway_Response
     * @throws Exception
     */
    protected function processTransaction($customer) {
        try{
            $response = $this->getGateway()->process($customer, $this->order, $this->generateReportGroup($customer));
            return $response;
        } catch(Exception $e){
            $this->getTransaction()->failure = 1;
            $this->getTransaction()->failure_reason = $e->getMessage();
            $this->getTransactionStorage()->save($this->getTransaction());
            throw $e;
        }
    }

    protected function addCustomerToCrm($customer) {
        try{
            $customerResponse = $this->crm->createCustomer($customer, $this->order); //create the customer record in Orange
            $customer->guid = $customerResponse->q_cust_guid; //store the customer's Orange guid in the customer object
            $this->getCustomerStorage()->save($customer); //save the customer record in the DB
            return $customerResponse;
        } catch(Icm_Commerce_Crm_Exception $e) {
            $this->getTransaction()->failure = 1;
            $this->getTransaction()->failure_reason = $e->getMessage();
            $this->getTransactionStorage()->save($this->getTransaction());
            $this->refundTransaction($customer);
            throw $e;
        }
    }

    protected function addTransactionToCrm($customer){
        try{
            $products = $this->getProducts()->getKeySet();
            $product = $products[0];
            $productGateway = $this->getProductGateway()->get($product, $this->getGateway());
            $transactionResponse = $this->crm->addTransaction($customer, $this->getTransaction(), $productGateway, Icm_Service_Orange_Api::TRANSACTION_STATUS_APPROVED);
            $this->getTransaction()->orange_guid = $transactionResponse->q_tran_guid;
            $this->getTransactionStorage()->save($this->getTransaction());
            return $transactionResponse;
        } catch(Icm_Commerce_Crm_Exception $e) {
            $transaction = $this->getTransaction();
            $transaction->failure = 1;
            $transaction->failure_reason = $e->getMessage();
            $this->getTransactionStorage()->save($transaction);
            $this->refundTransaction($customer);
            $this->crm->cancelCustomer($customer);
            $this->crm->archiveCustomer($customer);
            throw $e;
        }
    }


    protected function refundTransaction(Icm_Commerce_Customer $customer) {
        $refundResponse = $this->getGateway()->refund(
            $this->getTransaction(), $customer, false, $this->generateReportGroup($customer)
        );

        /**
         * @var Icm_Commerce_Transaction $refundTransaction
         */
        $refundTransaction = Icm_Commerce_Transaction::fromArray(array(
            'parent_id' => $this->getTransaction()->transaction_id,
            'gateway_id' => $this->getGateway()->getGatewayId(),
            'order_id' => $this->order->order_id,
            'type' => Icm_Commerce_Transaction::TYPE_REFUND,
            'gateway_txid' => $refundResponse->getTransactionId(),
            'amount' => $this->getSubtotal() * -1 //Make it a negative amount so we can SUM() on it
        ));

        $this->getTransactionStorage()->save($refundTransaction);
        $this->setRefundTransaction($refundTransaction);
    }

    protected function deactivateProductsForOrder() {
        $products = $this->getProducts()->getKeySet();
        $refundTxId = $this->getRefundTransaction()->transaction_id;

        foreach ($products as $product) {
            /**
             * @var Icm_Commerce_Product $product
             * @var Icm_Commerce_Order_Product $orderProduct
             */
            $orderProduct = $this->getOrderProductStorage()->getByOrderAndProduct($this->order, $product);
            $orderProduct->status = Icm_Commerce_Order_Product::STATUS_INACTIVE;
            $orderProduct->refund_id = $refundTxId;
            $this->getOrderProductStorage()->save($orderProduct);
            // @TODO Remove any credits added for these products.
        }
    }

    protected function persistOrderProducts(Icm_Commerce_Customer $customer, $status = Icm_Commerce_Order_Product::STATUS_ACTIVE) {
        $products = $this->getProducts()->getKeySet();

        foreach ($products as $product){
            /**
             * @var Icm_Commerce_Product $product
             * @var Icm_Commerce_Order_Product $orderProduct
             */
            $orderProduct = Icm_Commerce_Order_Product::fromArray(array(
                'order_id' => $this->order->getOrderId(),
                'product_id' => $product->product_id,
                'customer_id' => $customer->customer_id,
                'status' => $status
            ));
            $this->getOrderProductStorage()->save($orderProduct);

            $this->getCustomerGuidsStorage()->insert($customer, $orderProduct);

            $this->order->addOrderProduct($orderProduct);

        }

        $this->getCustomerStorage()->save($customer);
    }

    public function setMode($mode) {
        $this->mode = $mode;
        $this->updateTestState();
    }

    protected  function updateTestState(Icm_Commerce_Customer $customer = null) {
        $card = !is_null($customer) ? $customer->getCreditCard() : null;

        switch($this->mode) {
            case self::MODE_TEST:

                if (!is_null($card)) {
                    $this->setCancelArchive($card->isTestCCNumber());
                }

                $this->crm->setTestToken(true);
                $this->gateway->setSandbox(true);
            break;

            case self::MODE_PRODUCTION:
            default:
                if (!is_null($card) && $card->isTestCCNumber()) {
                    // If we're in prod, with a test number
                    $this->crm->setTestToken(false);
                    $this->setCancelArchive(true);
                    $this->gateway->setSandbox(true);
                }
                elseif (!is_null($card) && !$card->isTestCCNumber()) {
                    // If we're in prod, with a real number
                    $this->crm->setTestToken(false);
                    $this->setCancelArchive(false);

                    // set the sandbox to true if this is a test customer
                    $this->gateway->setSandbox(!is_null($customer) && $customer->isTest());
                }
        }
    }

    protected function setCancelArchive($bool) {
        $this->cancelAndArchive = $bool;
    }

    /**
     * @param \Icm_Commerce_Transaction $refundTransaction
     */
    public function setRefundTransaction($refundTransaction) {
        $this->refundTransaction = $refundTransaction;
    }

    /**
     * @return \Icm_Commerce_Transaction
     */
    public function getRefundTransaction() {
        return $this->refundTransaction;
    }


}
