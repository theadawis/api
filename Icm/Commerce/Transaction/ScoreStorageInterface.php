<?php
interface Icm_Commerce_Transaction_ScoreStorageInterface{
    /**
     * Store a transaction record
     * @param Icm_Commerce_Transaction $transaction
     */
    public function save(Icm_Commerce_Score $score);

}
