<?php
interface Icm_Commerce_Transaction_StorageInterface{
    /**
     * Store / update a transaction record
     * @param Icm_Commerce_Transaction $transaction
     */
    public function save(Icm_Commerce_Transaction $transaction);

    /**
     * Retrieve a transaction
     * @param int $id
     * @return Icm_Commerce_Transaction
     */
    public function getById($id);

    public function getFirstForOrder($orderId);
}