<?php
class Icm_Commerce_Transaction_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Transaction_StorageInterface{
    /**
     * Store / update a transaction record
     * @param Icm_Commerce_Transaction $transaction
     * @return mixed
     */
    public function save(Icm_Commerce_Transaction $transaction){
        if ($transaction->transaction_id){
            return $this->updateOne($transaction->toArray());
        }
        else{
            $this->insertOne($transaction->toArray());
            $transaction->transaction_id = $this->getLastInsert();
        }
    }

    /**
     * Retrieve a transaction
     * @param int $id
     * @return Icm_Commerce_Transaction
    */
    public function getById($id){
        return Icm_Commerce_Transaction::fromArray($this->findOneBy('transaction_id', $id));
    }

    public function getFirstForOrder($orderId) {
        $transactionData = $this->conn->fetch("
            SELECT
            *
            FROM {$this->tableName}
            WHERE order_id = :order_id
            ORDER BY created ASC
            LIMIT 1
        ", array("order_id" => $orderId));
        return Icm_Commerce_Transaction::fromArray($transactionData);
    }
}