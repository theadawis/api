<?php

class Icm_Commerce_Transaction_ScoreStorageAdapter extends Icm_Db_Table implements Icm_Commerce_Transaction_ScoreStorageInterface{
    /**
     * Store a RIS score record
     * @param Icm_Commerce_Transaction_Score $score
     * @return mixed
     */
    public function save(Icm_Commerce_Score $score){
        $this->insertOne($score->toArray());
        $score->ris_score_id = $this->getLastInsert();
    }
}
