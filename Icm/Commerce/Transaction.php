<?php
/**
 * Transaction.php
 * User: chris
 * Date: 12/21/12
 * Time: 12:56 PM
 */
class Icm_Commerce_Transaction extends Icm_Struct{
    const TYPE_PURCHASE = 0;
    const TYPE_REFUND = 1;
    const TYPE_CHARGEBACK = 2;

    public $transaction_id;
    public $parent_id;
    public $gateway_id;
    public $order_id;
    public $type;
    public $gateway_txid;
    public $amount;
    public $failure;
    public $failure_reason;
    public $orange_guid;
    public $card_type;
    public $created;
    public $updated;

    public function getTransactionId() {
        return $this->transaction_id;
    }

    public function getOrderId() {
        return $this->order_id;
    }

}
