<?php
interface Icm_Commerce_Customer_StorageInterface{
    /**
     * Retrieve a customer
     * @param string $id
     * @return Icm_Commerce_Customer
     */
    public function getById($id);

    /**
     * Retrieve a customer based on the visitor Id
     * @abstract
     * @param $visitorId
     * @return Icm_Commerce_Customer
     */
    public function getByVisitorId($visitorId);
    /**
     * Store / update a customer record
     * @param Icm_Commerce_Customer $customer
     * @return boolean true on success
     * @throws Icm_Commerce_Exception
     */
    public function save(Icm_Commerce_Customer $customer);

    /**
     * @abstract
     *
     * @param $email
     *
     * @return Icm_Commerce_Customer
     */
    public function getByEmail($email, $app_id = null);

    /**
     * Finds a customer record, by email address, limiting to a given product group
     * Can limit to a specific application, if app_id is supplied
     * @abstract
     *
     * @param $email
     * @param $productGroup
     * @param null $app_id
     *
     * @return mixed
     */
    public function findOneWithAccessToProduct($email, $productType, $app_id = null);
    public function updatePasswordTimeStamp(Icm_Commerce_Customer $customer);
    public function checkPasswordLastUpdate(Icm_Commerce_Customer $customer);
}
