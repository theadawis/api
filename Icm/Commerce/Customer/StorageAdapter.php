<?php
/**
 * Icm_Commerce_Customer_StorageAdapter.php
 * User: chris
 * Date: 12/31/12
 * Time: 5:08 PM
 */
class Icm_Commerce_Customer_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Customer_StorageInterface
{
    const DAY = 86400; //convert day into seconds 24*60*60;
    const EXPIRATION = 90; //Set password expiration to 90 days
    public function getById($id) {
        $customer = Icm_Commerce_Customer::fromArray($this->findOneBy($this->pk, $id));
        if (!isset($customer->customer_id)) {
            $customer = new Icm_Commerce_Customer();
        }
        return $customer;
    }

    /**
     * Get a customer by email address
     * @param string $email
     * @return Icm_Commerce_Customer
     */
    public function getByEmail($email, $app_id = null){
        // will return db object or false
/*        $customerRecord = $this->findOneBy('email', $email);
        if ($customerRecord) {
            // if object
            $customer = Icm_Commerce_Customer::fromArray($customerRecord);
        } else {
            // record wasn't found so create empty customer object
            $customer = new Icm_Commerce_Customer();
        }
        return $customer;*/
        /**
         * This is a giant hack for now
         * Please kill this when we have unified customer records
         */
        if (is_null($app_id)) {
            $app_id = Icm_Api::getInstance()->getConfig()->getSection("global")->getOption("app_id");
        }
        if (!$app_id) {
            throw new Icm_Exception("APP ID IS NOT DEFINED IN GLOBAL CONFIG");
        }
        $record = $this->conn->fetch("
            SELECT customer.*
            FROM customer
            LEFT JOIN orders USING(customer_id)
            WHERE
            customer.email = :email
            AND
            (orders.app_id = :app_id OR orders.app_id IS NULL)
        ", array(":email" => $email, ":app_id" => $app_id));
        if ($record) {
            return Icm_Commerce_Customer::fromArray($record, true);
        }
        return new Icm_Commerce_Customer();
    }
    /**
     * Checks if the password is older than 90 days
     * @param Icm_Commerce_Customer $customer
     * @return boolean
     */
    public function checkPasswordLastUpdate(Icm_Commerce_Customer $customer) {
        // $now = date("Y-m-d H:i:s", strtotime('+90 days'));
        // $now = strtotime($now);
        $now = time();
        $record = $this->conn->fetch("
            SELECT `password_last_changed`
            FROM customer
            WHERE
            `customer_id` = :customer_id
        ", array(":customer_id" => $customer->customer_id));
        $convertToTimeFormat = strtotime($record['password_last_changed']);
        $result = round($now - $convertToTimeFormat)/(self::DAY);
        if ($result >= self::EXPIRATION) {
            return true;
        }
        else
        {
            return false;
        }

    }
    /**
     * Updates the password_last_changed field in the customer table.
     * @param Icm_Commerce_Customer $customer
     */
    public function updatePasswordTimeStamp(Icm_Commerce_Customer $customer) {
        // $sql = "UPDATE {customer} SET `password_last_changed` = `:password_last_changed`"; //current_timestamp
        $data = $customer->toArray();
        $doNotStore = array('email', 'first_name', 'last_name', 'zip', 'guid', 'created',
            'updated', 'preferred_gateway', 'forgot_password_expires', 'forgot_password_hash',
            'password', 'phone', 'address1', 'address2', 'city', 'state');
        foreach ($doNotStore as $not){
            if (array_key_exists($not, $data)){
                unset($data[$not]);
            }
        }
        if ($customer->customer_id) {
            $this->updateOne($data);
        }
    }
    /**
     * Get a customer by forgot password hash
     * @param string $forgot_password_hash
     * @return Icm_Commerce_Customer
     */
    public function getByForgotPasswordHash($forgot_password_hash){
        $customer = Icm_Commerce_Customer::fromArray($this->findOneBy('forgot_password_hash', $forgot_password_hash));
        if (!isset($customer->customer_id)) {
            $customer = new Icm_Commerce_Customer();
        }
        return $customer;
    }

    /**
     * Set customer forgot password hash and forgot password expires fields
     * @param string $forgot_password_hash
     * @return Icm_Commerce_Customer
     */
    public function setForgotPasswordHash(Icm_Commerce_Customer $customer){
        // do not want to create a customer
        if (!$customer->customer_id) {
            return false;
        }
        if ($customer->forgot_password_hash === 0 || !$customer->forgot_password_hash) {
            // set expires to 0 and leave forgot_password_hash, if set, in db
            $customer->forgot_password_expires = 0;
        } else {
            $customer->forgot_password_expires = strtotime("+24 hours");
        }
        $this->save($customer);
        return true;
    }

    /**
     * Retrieve a customer based on the visitor Id
     * @param $visitorId
     * @return Icm_Commerce_Customer
     */
    public function getByVisitorId($visitorId) {

        return Icm_Commerce_Customer::fromArray(
            $this->conn->fetch("
                SELECT customer.* FROM customer
                LEFT JOIN visitors USING(customer_id)
                WHERE visitors.visitor_id = :visitorId
            ", array(':visitorId' => $visitorId))
        );

    }

    public function findOneWithAccessToProduct($email, $productType, $app_id = null) {
        $replacements = array(
            ":email" => $email,
            ":product_type" => $productType
        );
        if (!is_null($app_id)) {
            $app_query = "AND app_products.app_id = :app_id";
            $replacements[":app_id"] = $app_id;
        }
        $data = $this->conn->fetch("
            SELECT customer.*
            FROM
            customer
            JOIN order_products USING(customer_id)
            JOIN products ON (order_products.product_id = products.product_id)
            JOIN app_products ON (products.product_id = app_products.product_id)
            WHERE
            products.product_type = :product_type
            AND
            customer.email = :email
        ", $app_id);

        if ($data) {
            return Icm_Commerce_Customer::fromArray($data, true);
        }
        return new Icm_Commerce_Customer();
    }

    /**
     * Store / update a customer record
     * @param Icm_Commerce_Customer $customer
     */
    public function save(Icm_Commerce_Customer $customer) {
        $data = $customer->toArray();
        $doNotStore = array('password', 'phone', 'address1', 'address2', 'city', 'state', 'app_id');

        foreach ($doNotStore as $not){
            if (array_key_exists($not, $data)){
                unset($data[$not]);
            }

        }

        // remove these since the database has a default value for them
        $removeIfNull = array('crm_config_id', 'is_test');

        foreach ($removeIfNull as $rem) {
            if (array_key_exists($rem, $data) && is_null($data[$rem])) {
                unset($data[$rem]);
            }
        }

        if ($customer->customer_id) {
            $this->updateOne($data);
        } else {
            $this->insertOne($data);
            $customer->{$this->pk} = $this->conn->lastInsert();
        }

        try{
            // Try to update the visitor record to have the customer id
            $visitor_id = $customer->getVisit()->visitor_id;
            $this->conn->execute("
                    INSERT INTO visitors
                    (visitor_id, customer_id)
                    VALUES(:visitor_id, :customer_id)
                    ON DUPLICATE KEY UPDATE customer_id=:customer_id
                ", array("visitor_id" => $visitor_id, "customer_id" => $customer->customer_id));
        } catch(Exception $e) {
            // Do nothing.
        }
    }

    /**
     * Get all customer guids associated with this customer.
     * Note: Checkmate currently only uses the guid in the customer table.
     * TODO: Remove the Note above when it is no longer applicable.
     *
     * @param Icm_Commerce_Customer $customer
     * @return array()
     */
    public function getGuidsForCustomer(Icm_Commerce_Customer $customer){
        $records = $this->conn->fetchAll("
            SELECT DISTINCT customer_guids.guid
            FROM order_products
            JOIN customer_guids USING(order_product_id)
            JOIN customer USING(customer_id)
            WHERE
            customer_id = :customer_id
            ", array(":customer_id" => $customer->customer_id));

        return $records;
    }
}
