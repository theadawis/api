<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginTracker
 *
 * @author fadi
 */
class Icm_Commerce_Customer_LoginTracker
{
    public  $email, $app_id;
    protected $redis, $keyPrefix, $key;
    const FIRST_ATTEMPT = 1; //Attempts start at 1
    const EXPIRATION = 7200; //Redis expiration key (2 hours)
    const MAX_ATTEMPTS = 5; //Max attempts before user is directed to CAPTCHA

    public function __construct(Icm_Redis $redis) {
       $this->redis = $redis;
       $this->keyPrefix = 'password_counter_';
       $this->email = '';
       $this->app_id = '';
    }
    /**
     * checks if key exists:
     * @return type
     */
    protected function doesKeyExist() {
        return $this->redis->exists($this->key) ? true : false;
    }
    /**
     * Creates a key for redis
     */
    protected function createKey() {
        try {
           $this->key = $this->keyPrefix . ':' . $this->app_id . ':' . $this->email;
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }

    }
    /**
     * Returns the value of a redis key
     * @return type
     */
    protected function returnKeyValue() {
        return $this->redis->get($this->key);
    }
    public function deleteKey() {
         $this->createKey();
         $this->redis->del($this->key);
    }
    /**
     * This function adds login attempts to redis
     * @param string $email
     * @param int $app_id
     * @return boolean
     */
    public function addLogInToQueue($userEmail = NULL, $applicationId = NULL) {
        if ($userEmail != NULL) {
            $this->email = $userEmail;
        }
        if ($applicationId != NULL) {
            $this->app_id = $applicationId;
        }
        // Create the key
        $this->createKey();
        // Check if key exists
        $isKey = $this->doesKeyExist();
        if ($isKey) {
           // Check the number of attempts
           $numberOfAttempts = $this->returnKeyValue();
           if ($numberOfAttempts == self::MAX_ATTEMPTS) {
               return true;
           }
           else
           {
            // Increment key:
            $this->redis->incr($this->key);
            return false;
           }


        }
        else
        {
            // Set key with expiration of 2 hours
            $this->redis->setex($this->key, self::EXPIRATION, self::FIRST_ATTEMPT);
            $this->redis->incr($this->key);
        }
    }

}

