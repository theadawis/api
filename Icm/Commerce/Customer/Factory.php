<?php
/**
 * Factory.php
 * User: chris
 * Date: 12/31/12
 * Time: 3:23 PM
 */
class Icm_Commerce_Customer_Factory
{

    /**
     * @var int
     */
    protected $registrationSectionId;

    /**
     * @var Icm_SplitTest_Tracker
     */
    protected $analyticsTracker;

    /**
     * @var Icm_Visit_Tracker
     */
    protected $visitTracker;

    /**
     * @var Icm_Service_Internal_Marketing_Affiliate
     */
    protected $affiliateStorageAdapter;


    /**
     * @var Icm_Commerce_Customer_StorageInterface
     */
    protected $customerStorageAdapter;

    /**
     * @var Icm_Commerce_Customer_Credits_StorageInterface
     */
    protected $creditsStorageAdapter;

    public function __construct(
                                Icm_Commerce_Customer_StorageInterface $customerStorageAdapter,
                                Icm_Commerce_Gateway_StorageInterface $gatewayStorageAdapter,
                                Icm_Visit_Tracker $visitTracker,
                                Icm_Service_Internal_Marketing_Affiliate $affiliateStorageAdapter,
                                Icm_SplitTest_Tracker $tracker,
                                $registrationSectionId
    ) {
        $this->customerStorageAdapter = $customerStorageAdapter;
        $this->visitTracker = $visitTracker;
        $this->affiliateStorageAdapter = $affiliateStorageAdapter;
        $this->gatewayStorageAdapter = $gatewayStorageAdapter;
        $this->registrationSectionId = $registrationSectionId;
        $this->analyticsTracker = $tracker;
    }

    /**
     * @param $visitorId
     * @return Icm_Commerce_Customer
     */
    public function getCustomer($customerId) {
        $customer = $this->customerStorageAdapter->getById($customerId);
        $customer->setVaration($this->getVariation());

        $visit = $this->getVisit();

        if (!is_null($visit)){
            $customer->setVisit($visit);
        }

        if ($customer->preferred_gateway) {
            $customer->setGateway($this->getGateway($customer->preferred_gateway));
        }

        return $customer;
    }

    protected function getVisit() {
        /**
         * @var Icm_Struct_Tracking_Visit $visit
         */
        $visitData = $this->visitTracker->getVisitData();

        if ($visitData){
            $visit = Icm_Struct_Tracking_Visit::fromArray($visitData);
            $visit->setAffiliate($this->affiliateStorageAdapter->getById($visit->affiliate_id));
            return $visit;
        }

        return null;
    }

    protected function getVariation() {
        $ctx = $this->analyticsTracker->getTestContext();

        if (array_key_exists($this->registrationSectionId, $ctx)) {
            $data = array('testId' => 0,
                          'variationId' => $ctx[$this->registrationSectionId]['variationId']);
            return new Icm_SplitTest_Variation($data);
        }

        return new Icm_SplitTest_Variation(array('testId' => 0, 'variationId' => 0));
    }

    protected function getGateway($gatewayId) {
        return $this->gatewayStorageAdapter->getById($gatewayId);
    }
}
