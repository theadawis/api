<?php
/**
 * Icm_Commerce_Customer_Credits_Description_StorageAdapter.php
 * User: tom
 * Date: 02/01/13
 * Time: 11:21 AM
 */
class Icm_Commerce_Customer_Credits_Description extends Icm_Struct {
    public $credits_type_id,
        $description,
        $created,
        $updated;

}