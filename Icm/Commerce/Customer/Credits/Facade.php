<?php
/**
 * Icm_Commerce_Customer_Credits_Facade.php
 * User: tom
 * Date: 1/25/13
 * Time: 5:08 PM
 */
class Icm_Commerce_Customer_Credits_Facade {

    protected $creditsUsageStorage,
        $creditsDescriptionStorage;

    public function __construct(Icm_Commerce_Customer_Credits_Description_StorageInterface $creditsDescriptionStorage,
                                Icm_Commerce_Customer_Credits_Usage_StorageInterface $creditsUsageStorage) {

        $this->setCreditsDescriptionStorage($creditsDescriptionStorage);
        $this->setCreditsUsageStorage($creditsUsageStorage);

    }

    public function getByType($customerId, $creditsTypeId) {
        return Icm_Commerce_Customer_Credits::fromArray(array('customer_id' => $customerId, 'credits_type_id' => $creditsTypeId));
    }

    public function setCreditsUsageStorage(Icm_Commerce_Customer_Credits_Usage_StorageInterface $creditsUsageStorage) {
        $this->creditsUsageStorage = $creditsUsageStorage;
    }

    public function getCreditsUsageStorage() {
        if (!$this->creditsUsageStorage instanceof Icm_Commerce_Customer_Credits_Usage_StorageInterface) {
            throw new Icm_Commerce_Exception("Attempting to retrieve a credits usage object when none has been set.");
        }
        return $this->creditsUsageStorage;
    }

    public function setCreditsDescriptionStorage(Icm_Commerce_Customer_Credits_Description_StorageInterface $creditsDescriptionStorage) {
        $this->creditsDescriptionStorage = $creditsDescriptionStorage;
    }

    public function getCreditsDescriptionStorage() {
        if (!$this->creditsDescriptionStorage instanceof Icm_Commerce_Customer_Credits_Description_StorageInterface) {
            throw new Icm_Commerce_Exception("Attempting to retrieve a credits description object when none has been set.");
        }
        return $this->creditsDescriptionStorage;
    }

    public function getCreditsRemaining($customerId, $creditsTypeId) {
        $purchased = $this->sumCreditsPurchased($customerId, $creditsTypeId);
        $used = $this->sumCreditsUsed($customerId, $creditsTypeId);
        $creditsRemaining = $purchased - $used;
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($purchased .' - ' . $used.' = get credits remaining: ' . $creditsRemaining. 'for custid: ' . $customerId.' and type: ' . $creditsTypeId);
        return ($creditsRemaining < 1) ? 0 : $creditsRemaining;
    }

    public function sumCreditsUsed($customerId, $creditsTypeId) {
        return $this->getCreditsUsageStorage()->sumCreditsUsed($customerId, $creditsTypeId);
    }

    public function sumCreditsPurchased($customerId, $creditsTypeId) {
        return $this->getCreditsUsageStorage()->sumCreditsPurchased($customerId, $creditsTypeId);
    }

}