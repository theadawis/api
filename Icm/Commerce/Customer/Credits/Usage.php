<?php

class Icm_Commerce_Customer_Credits_Usage extends Icm_Struct {
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $customer_credits_usage_id,
        $customer_id,
        $order_product_id,
        $credits_used,
        $data_source,
        $data_source_id,
        $status,
        $created,
        $updated = "";

}