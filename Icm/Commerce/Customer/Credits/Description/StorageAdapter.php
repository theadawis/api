<?php
/**
 * Icm_Commerce_Customer_Credits_Description_StorageAdapter.php
 * User: tom
 * Date: 02/01/13
 * Time: 11:21 AM
 */
class Icm_Commerce_Customer_Credits_Description_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Customer_Credits_Description_StorageInterface {
    public function getById($id) {
        $description = Icm_Commerce_Customer_Credits_Description::fromArray($this->findOneBy($this->pk, $id));
        if (!isset($description->credits_type_id)) {
            $description = new Icm_Commerce_Customer_Credits_Description();
        }
        return $description;
    }
}