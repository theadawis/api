<?php

interface Icm_Commerce_Customer_Credits_Description_StorageInterface {

    public function getById($creditsTypeId);

}