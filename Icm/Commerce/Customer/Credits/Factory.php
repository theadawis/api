<?php

class Icm_Commerce_Customer_Credits_Factory {

    protected $customerId;

    protected $creditsFacade;

    public function __construct($customerId) {
        $this->customerId = $customerId;
    }

    public function setCreditsUsageStorage(Icm_Commerce_Customer_Credits_Usage_StorageInterface $creditsUsageStorage) {
        $this->creditsUsageStorage = $creditsUsageStorage;
    }

    public function getCreditsUsageStorage() {
        if (!$this->creditsUsageStorage instanceof Icm_Commerce_Customer_Credits_Usage_StorageInterface) {
            throw new Icm_Commerce_Exception("Attempting to retrieve a credits usage object when none has been set.");
        }
        return $this->creditsUsageStorage;
    }

    public function setCreditsDescriptionStorage(Icm_Commerce_Customer_Credits_Description_StorageInterface $creditsDescriptionStorage) {
        $this->creditsDescriptionStorage = $creditsDescriptionStorage;
    }

    public function getCreditsDescriptionStorage() {
        if (!$this->creditsDescriptionStorage instanceof Icm_Commerce_Customer_Credits_Description_StorageInterface) {
            throw new Icm_Commerce_Exception("Attempting to retrieve a credits description object when none has been set.");
        }
        return $this->creditsDescriptionStorage;
    }

    // instantiate a credits object of the appropriate type and return it
    public function getCredits($creditsTypeId) {
        if (!$this->creditsFacade instanceof Icm_Commerce_Customer_Credits_Facade) {
            $this->creditsFacade = new Icm_Commerce_Customer_Credits_Facade($this->getCreditsDescriptionStorage(), $this->getCreditsUsageStorage());
        }
        $creditsObj = $this->creditsFacade->getByType($this->customerId, $creditsTypeId);
        $creditsObj->setCreditsFacade($this->creditsFacade);

        return $creditsObj;
    }

}