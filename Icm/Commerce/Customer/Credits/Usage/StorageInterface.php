<?php

interface Icm_Commerce_Customer_Credits_Usage_StorageInterface {

    public function getBySource($customerId, $creditsTypeId, $dataSource, $dataSourceId);

    public function save(Icm_Commerce_Customer_Credits_Usage $creditsUsage);

}