<?php
/**
 * Icm_Commerce_Customer_Credits_Usage_StorageAdapter.php
 * User: tom
 * Date: 1/29/13
 * Time: 1:36 PM
 */

class Icm_Commerce_Customer_Credits_Usage_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Customer_Credits_Usage_StorageInterface {

    public function sumCreditsPurchased($customerId, $creditsTypeId) {
        $sql = "SELECT sum(p.credits) as credits_available
                FROM order_products op
                JOIN products p on op.product_id = p.product_id
                WHERE op.customer_id = :customerId AND p.credits_type_id = :creditsTypeId
                AND op.status = 1";

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);

        $result = $this->conn->fetch($sql, array(':customerId' => $customerId,
                                                ':creditsTypeId' => $creditsTypeId));

        return $result['credits_available'];
    }

    public function sumCreditsUsed($customerId, $creditsTypeId) {
        $sql = "SELECT sum(credits_used) as credits_used FROM customer_credits_usage cu
                JOIN order_products op on op.customer_id = cu.customer_id AND op.order_product_id = cu.order_product_id
                JOIN products p on op.product_id = p.product_id
                WHERE cu.customer_id = :customerId AND p.credits_type_id = :creditsTypeId
                AND cu.status = 1 and op.status = 1";

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);

        $result = $this->conn->fetch($sql, array(':customerId' => $customerId,
                                                ':creditsTypeId' => $creditsTypeId));


        return (is_array($result)) ? $result['credits_used'] : null;

    }

    public function getBySource($customerId, $creditsTypeId, $dataSource, $dataSourceId) {

        // get the credits usage row for this data source and data source id
        // in other words - did they use a credit of type $creditTypeId on this report?
        $sql = "SELECT cu.*, p.credits_type_id, p.credits as product_credits, p.product_id, op.order_id
                FROM customer_credits_usage cu
                JOIN order_products op ON op.order_product_id = cu.order_product_id
                JOIN products p ON op.product_id = p.product_id
                WHERE cu.customer_id = :customerId
                AND cu.data_source = :dataSource
                AND cu.data_source_id = :dataSourceId
                AND p.credits_type_id = :creditsTypeId,
                AND cu.status = 1
                AND op.status = 1";

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        $usageData = $this->conn->fetch($sql, array(':customerId'    => $customerId,
                                                   ':dataSource'    => $dataSource,
                                                   ':dataSourceId'  => $dataSourceId,
                                                   ':creditsTypeId' => $creditsTypeId));

        $creditsUsage = Icm_Commerce_Customer_Credits_Usage::fromArray($usageData);

        if (isset($creditsUsage->customer_id)) {
            return $creditsUsage;
        } else {
            return false;
        }
    }


    public function getNextAvailableOrderProductId($customerId, $creditsTypeId) {
        // get all the order product ids for this credit type
        // find the first order_product row that has un-used credits
        $sql = "SELECT op.order_product_id FROM order_products op
                JOIN products p on p.product_id = op.product_id
                LEFT JOIN customer_credits_usage cu on cu.order_product_id = op.order_product_id
                WHERE op.customer_id = :customerId
                AND p.credits_type_id = :creditsTypeId
                AND ((p.credits - coalesce(cu.credits_used, 0)) > 0)
                AND (cu.status = 1 OR cu.status is NULL)
                AND op.status = 1
                ORDER BY op.created asc LIMIT 1";
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        $orderProduct = $this->conn->fetch($sql, array(':customerId' => $customerId,
                                                      ':creditsTypeId' => $creditsTypeId));

        Icm_Util_Logger_Syslog::getInstance('checkmate')->logDebug('CREDITS: next available order product id:' . $orderProduct['order_product_id']);
        return $orderProduct['order_product_id'];
    }

    /**
     * get all credits usage rows for this customer and productId
     * @param integer $customerId
     * @param integer $productId
     * @return array query result
     */
    public function getByProductId($customerId, $productId) {
        $sql = "SELECT cu.data_source_id
                FROM customer_credits_usage cu
                    JOIN order_products op ON op.order_product_id = cu.order_product_id
                    JOIN products p ON op.product_id = p.product_id
                WHERE cu.customer_id = :customerId
                    AND p.product_id = :productId
                    AND cu.status = 1
                    AND op.status = 1";

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        return $this->conn->fetchAll($sql, array(':customerId'    => $customerId,
                                                ':productId'     => $productId));
    }

    /**
     * get all credits usage rows for this customer and productTier
     * @param integer $customerId
     * @param string $productTier
     * @return array query result
     */
    public function getSourceIdsByProductTier($customerId, $productTier='Premium') {
        $sql = "SELECT cu.data_source, cu.data_source_id
                FROM customer_credits_usage cu,
                     order_products op,
                     products p
                WHERE cu.customer_id = :customerId
                    AND op.order_product_id = cu.order_product_id
                    AND op.product_id = p.product_id
                    AND p.product_tier = :productTier
                    AND cu.status = 1
                    AND op.status = 1";

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        return $this->conn->fetchAll($sql, array(':customerId'   => $customerId,
                                                ':productTier' => $productTier));
    }

    /**
     * Store / update a customer credits usage record
     * @param Icm_Commerce_Customer_Credits_Usage $creditsUsage
     */
    public function save(Icm_Commerce_Customer_Credits_Usage $creditsUsage) {
        $data = $creditsUsage->toArray();

        if ($creditsUsage->customer_credits_usage_id) {
            $this->updateOne($data);
        } else {
            $data['status'] = $creditsUsage::STATUS_ACTIVE;
            $this->insertOne($data);
            $creditsUsage->{$this->pk} = $this->conn->lastInsert();
        }
    }




}
