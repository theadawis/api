<?php
/**
 * Writer.php
 * User: chris
 * Date: 2/21/13
 * Time: 11:10 AM
 */
class Icm_Commerce_Customer_Log_Writer extends Zend_Log_Writer_Abstract {

    public function __construct(MongoCollection $collection) {
        $this->collection = $collection;
    }

    protected function _write($event) {
        $this->collection->update(
            array("customer_id" => $event["customer"]->customer_id), //Match the customer id
            array('$push' =>
                  array(
                      "message" => $event["message"],
                      "data" => $event["data"]
                    )
            ) //Push the message
        );
    }


    /**
     * Construct a Zend_Log driver
     *
     * @param  array|Zend_Config $config
     *
     * @return Zend_Log_FactoryInterface
     */
    static public function factory($config) {
        throw new Exception("Not implemented");
    }
}