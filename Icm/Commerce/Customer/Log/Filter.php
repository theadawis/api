<?php
/**
 * Filter.php
 * User: chris
 * Date: 2/21/13
 * Time: 11:10 AM
 */
class Icm_Commerce_Customer_Log_Filter implements Zend_Log_Filter_Interface {
    /**
     * Returns TRUE to accept the message, FALSE to block it.
     *
     * @param  array    $event    event data
     *
     * @return boolean            accepted?
     */
    public function accept($event) {
        return array_key_exists("customer", $event) && is_a($event["customer"], "Icm_Commerce_Customer");
    }
}
