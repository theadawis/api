<?php

class Icm_Commerce_Customer_History_Report_StorageAdapter extends Icm_Db_Table
{
    /**
     * Fetch the report history for a given customer
     *
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    public function getByCustomer(Icm_Commerce_Customer $customer) {
        $history = array();

        // don't query if no customer id
        if (empty($customer->customer_id)) {
            return $history;
        }

        // pull the report history via customer id order by updated date
        $sql = "SELECT * FROM report_history"
            . " WHERE customer_id = ?"
            . " ORDER BY updated DESC";
        $params = array($customer->customer_id);
        $results = $this->conn->fetchAll($sql, $params);

        foreach ($results as $row) {
            $history[] = Icm_Commerce_Customer_History_Report_Struct::fromArray($row);
        }

        return $history;
    }

    /**
     * Save a report history struct to the db. Increment the number of report views
     * if the report has been viewed more than once.
     *
     * @param Icm_Commerce_Customer_History_Report_Struct $history
     * @return int
     */
    public function save(Icm_Commerce_Customer_History_Report_Struct $history) {
        $existing = $this->findOneByFields(array(
            'report_key' => $history->report_key,
            'customer_id' => $history->customer_id,
            'report_type_id' => $history->report_type_id
        ));

        // check if this report has been viewed before
        if (isset($existing["report_history_id"])) {
            // increment the views
            $existing['view_count']++;
            $this->updateOne($existing);

            return $existing["report_history_id"];
        }

        $data = $history->toArray();
        $data['view_count'] = 1;
        $this->insertOne($data);
        $id = $this->conn->lastInsert();

        return $id;
    }

    /**
     * Fetch the number of times this report type has been viewed by this customer
     * over the last 30 days
     *
     * @param int $reportTypeId
     * @param Icm_Commerce_Customer $customer
     * @return int
     */
    public function getDistinctMonthlyViews($reportTypeId, Icm_Commerce_Customer $customer) {
        $date = new Icm_Util_DateHelper();
        $date->setTimestampFromString("-30 days");

        $sql = "SELECT COUNT(distinct(report_key)) AS view_count"
            . " FROM report_history"
            . " WHERE customer_id = :customer_id"
            . " AND report_type_id = :report_type_id"
            . " AND created > :date";

        $params = array(
            ':customer_id' => $customer->customer_id,
            ':report_type_id' => $reportTypeId,
            ':date' => $date->format('Ymd'),
        );

        $results = $this->conn->fetchAll($sql, $params);

        // return the view count if possible
        if (isset($results[0]) && isset($results[0]['view_count'])) {
            return $results[0]['view_count'];
        }

        return 0;
    }
}
