<?php

class Icm_Commerce_Customer_History_Report_Struct extends Icm_Struct
{
    public $report_history_id;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $county;
    public $phone;
    public $dob;
    public $view_count;
    public $customer_id;
    public $report_key;
    public $report_type_id;
    public $created;
    public $updated;
}
