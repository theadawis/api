<?php
/**
 * Icm_Commerce_Customer_Guids_StorageAdapter.php
 * User: tom
 */
class Icm_Commerce_Customer_Guids_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Customer_Guids_StorageInterface
{

    public function getGuidsForCustomer(Icm_Commerce_Customer $customer){
        $records = $this->conn->fetchAll("
            SELECT DISTINCT customer_guids.guid
            FROM order_products
            JOIN customer_guids USING(order_product_id)
            JOIN customer USING(customer_id)
            WHERE
            customer_id = :customer_id
            ", array(":customer_id" => $customer->customer_id));
        return $records;
    }

    public function insert(Icm_Commerce_Customer $customer, Icm_Commerce_Order_Product $orderProduct) {
        if ($orderProduct->order_product_id && $customer->guid) {
            $customerGuid = array(
                'order_product_id' => $orderProduct->order_product_id,
                'guid' => $customer->guid,
            );

            // only add crm_config_id if it's set, since the db has a default value
            if (!empty($customer->crm_config_id)) {
                $customerGuid['crm_config_id'] = $customer->crm_config_id;
            }

            $this->insertOne($customerGuid);
        }
    }
}
