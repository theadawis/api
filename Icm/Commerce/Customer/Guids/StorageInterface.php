<?php
/**
 * Icm_Commerce_Order_Product_StorageInterface.php
 * User: tomw
 */
interface Icm_Commerce_Customer_Guids_StorageInterface
{
    public function insert(Icm_Commerce_Customer $customer, Icm_Commerce_Order_Product $orderProduct);
    public function getGuidsForCustomer(Icm_Commerce_Customer $customer);
}
