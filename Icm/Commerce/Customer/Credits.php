<?php

class Icm_Commerce_Customer_Credits extends Icm_Struct {
    public $credits_type_id,
        $customer_id;

    protected $creditsFacade;

    public function setCreditsFacade(Icm_Commerce_Customer_Credits_Facade $creditsFacade) {
        $this->creditsFacade = $creditsFacade;
    }

    public function getCreditsFacade() {
        if (!$this->creditsFacade instanceof Icm_Commerce_Customer_Credits_Facade) {
            throw new Icm_Commerce_Exception("Attempting to retrieve a credits facade object when none has been set.");
        }
        return $this->creditsFacade;
    }

    // count of active credits purchased - credits used for this credit_type
    public function getCreditsRemaining() {
        return $this->getCreditsFacade()->getCreditsRemaining($this->customer_id, $this->credits_type_id);
    }

    // sum of products.credits join order_products on product_id where customer_id = this->customer_id
    public function getCreditsPurchased() {
        return $this->getCreditsFacade()->sumCreditsPurchased($this->customer_id, $this->credits_type_id);
    }

    // sum of customer_credits_usage.credits_used where customer_id and credits_type_id and status = active
    public function getCreditsUsed() {
        return $this->getCreditsFacade()->getCreditsUsageStorage()->sumCreditsUsed($this->customer_id, $this->credits_type_id);
    }

    public function getCreditsDescriptionObject() {
        return $this->getCreditsFacade()->getCreditsDescriptionStorage()->getById($this->credits_type_id);
    }

    public function getDescription() {
        return $this->getCreditsDescriptionObject()->description;
    }

    public function useCredits($dataSource, $dataSourceId, $orderProductId = null) {
        // first check if there are credits available to use
        if ($this->getCreditsRemaining() < 1) {
            throw new Icm_Commerce_Exception('no credits available to use');
        }

        if (is_null($orderProductId)) {
            $orderProductId = $this->getNextAvailableOrderProductId();
        }

        // insert into credits usage
        // hard code 1 credit to use for now
        $creditsUsage = Icm_Commerce_Customer_Credits_Usage::fromArray(array(
                                                                             'customer_id'      => $this->customer_id,
                                                                             'credits_used'     => 1,
                                                                             'order_product_id' => $orderProductId,
                                                                             'data_source'      => $dataSource,
                                                                             'data_source_id'   => $dataSourceId,
                                                                             ));

        return $this->getCreditsFacade()->getCreditsUsageStorage()->save($creditsUsage);

    }

    // this function is something like:
    // SELECT order_product_id from order_products op join products p on p.product_id = op.product_id
    public function getNextAvailableOrderProductId() {
        Icm_Util_Logger_Syslog::getInstance('checkmate')->logDebug('CREDITS: getting next available order product id:' . $this->customer_id.' credits type id: ' . $this->credits_type_id);
        return $this->getCreditsFacade()->getCreditsUsageStorage()->getNextAvailableOrderProductId($this->customer_id, $this->credits_type_id);
    }
}