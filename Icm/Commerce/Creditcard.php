<?php
/**
 * @author Joe Linn
 */
class Icm_Commerce_Creditcard extends Icm_Struct{
    public $number,
        $cvv,
        $expiration_month,
        $expiration_year,
        $first_name,
        $last_name;

    const CARD_TYPE_VISA = 'visa';
    const CARD_TYPE_MASTERCARD = 'mastercard';
    const CARD_TYPE_AMEX = 'amex';
    const CARD_TYPE_DISCOVER = 'discover';
    const CARD_TYPE_DINERS = 'diners';

    // TODO: affiliate test card: 4895895895895895

    protected $testCCs = array(
        // visa
        "4116480559370132",
        "4119692004990226",
        "4112296825756779",
        "4110716719147654",
        "4112530021797363",
        "4112653620165475",
        "4113864427850982",
        "4111111111111111",
        "4012888888881881",
        // mastercard
        "5123695007103193",
        "5122415444933925",
        "5106447623213738",
        "5155659853141666",
        "5199339067794810",
        "5102881917494147",
        "5155239927190195",
        "5184778657904478",
        "5555555555554444",
        "5105105105105100",
        // amex
        "378282246310005",
    );
    /*
    /**
     * @param int $number
     * @return boolean
    */
    public function isTestCCNumber($number = null){
        if (is_null($number)) {
            $number = $this->number;
        }
        return in_array($number, $this->testCCs);
    }

    /**
     * Add a valid credit card number to the internal list of accepted test credit cards
     * @param int $number credit card number
     * @throws Exception
     */
    public function addTestCCNumber($number){
        if (!$this->isValidCCNumber($number)){
            throw new Exception("$number is not a valid credit card number.");
        }
        $this->testCCs[] = $number;
    }

    /**
     * @return string|boolean card type if $this->number is valid; false otherwise
     */
    public function getCardType(){
        $cardRegexes = array(
            "/^4\d{12}(\d\d\d){0,1}$/" => self::CARD_TYPE_VISA,
            "/^5[12345]\d{14}$/"       => self::CARD_TYPE_MASTERCARD,
            "/^3[47]\d{13}$/"          => self::CARD_TYPE_AMEX,
            "/^6011\d{12}$/"           => self::CARD_TYPE_DISCOVER,
            "/^30[012345]\d{11}$/"     => self::CARD_TYPE_DINERS,
            "/^3[68]\d{12}$/"          => self::CARD_TYPE_DINERS,
        );
        foreach ($cardRegexes as $regex => $type){
            if (preg_match($regex, $this->number)){
                return $type;
            }
        }
        return false;
    }

    /**
     * Determines whether or not the currently stored $number is a valid credit card number.
     * @param boolean $number
     * @return boolean
     */
    public function isValidCCNumber($number = false){
        if (!$number){
            if (!$this->number) {
                return false;
            }
            $number = $this->number;
        }
        $reverse = strrev($number);
        $checksum = 0;
        for ($i = 0; $i < strlen($reverse); $i++){
            $current = intval($reverse[$i]);
            if ($i & 1){
                // odd position
                $current *= 2;
            }
            $checksum += $current % 10;
            if ($current > 9){
                $checksum++;
            }
        }
        return $checksum % 10 == 0;
    }

    public function isValidExpirationDate(){
        if (empty($this->expiration_month) || empty($this->expiration_year)){
            return false;
        }
        return strtotime($this->expiration_month . '/' . date('t') . '/' . $this->expiration_year) > time();
    }

    /**
     * Format the credit card's expiration date using PHP's date() function
     * @param string $format
     * @return string (defaults to MMYYYY format)
     */
    public function formatExpiration($format = 'mY'){
        return date($format, strtotime($this->expiration_month . '/01/' . $this->expiration_year));
    }

    public function validate(){
        if (!$this->getCardType()) {
            throw new Icm_Service_Litle_Exception("Credit card type is null");
        }
    }

    public function getNumber(){
        return $this->number;
    }

    public function getFullName() {
        return $this->first_name . " " . $this->last_name;
    }
}
