<?php

/**
 * Score.php
 */
class Icm_Commerce_Score extends Icm_Struct{

    public $ris_score_id;
    public $transaction_id;
    public $order_id;
    public $score;
    public $ris_score;
    public $error_code;
    public $proxy;
    public $cards;
    public $emails;
    public $velo;
    public $auto;
    public $reason;
    public $response;
    public $created;
    public $updated;

}
