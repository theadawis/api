<?php
class Icm_Commerce_Order extends Icm_Struct{
    public $order_id;
    public $app_id;
    public $customer_id;
    public $visitor_id;
    public $visit_id;
    // public $pixel_fired;
    public $created;
    public $updated;
    public $is_test;

    /**
     * @var Icm_HashMap
     */
    protected $products;

    /**
     * @var Icm_Collection
     */
    protected $transactions;

    protected $orderProducts = array();

    public function  __construct(){
        $this->products = new Icm_HashMap('Icm_Commerce_Product');
        $this->transactions = new Icm_Collection(array());
    }

    /**
     * Add a Product to the cart
     * @param Icm_Commerce_Product $product
     * @param int $quantity
     * @return void
     */
    public function addProduct(Icm_Commerce_Product $product, $quantity = 1){
        if ($this->isValidQuantity($quantity)){
            if (isset($this->products[$product])){
                $this->products[$product] += $quantity;
            }
            else{
                $this->products[$product] = $quantity;
            }
        }
    }

    /**
     * Add an OrderProduct to the Order
     * @param Icm_Commerce_Order_Product $orderProduct
     * @return null
     */
    public function addOrderProduct(Icm_Commerce_Order_Product $orderProduct){
        $this->orderProducts[$orderProduct->order_product_id] = $orderProduct;
    }

    /**
     * Add an OrderProduct to the Order
     * @return array
     */
    public function getOrderProducts() {
        if (!count($this->orderProducts)) {
            throw new Icm_Commerce_Exception('there are no order products set for this order');
        }
        return $this->orderProducts;
    }

    /**
     * Add an OrderProduct to the Order
     * @param int
     * @return Icm_Commerce_Order_Product
     */
    public function getOrderProduct($orderProductId) {
        if (array_key_exists($orderProductId, $this->getOrderProducts())) {
            return $this->orderProducts[$orderProductId];
        }
        throw new Icm_Commerce_Exception('could not find order product for that id.  shunnnnnnnnn the non-believer!');
    }

    /**
     * @return boolean
     */
    public function hasProducts(){
        return $this->products->count() > 0;
    }

    /**
     * @param Icm_Commerce_Product $product
     * @return number
     */
    public function getQuantity(Icm_Commerce_Product $product){
        if ($this->products->contains($product)){
            return $this->products[$product];
        }
        return 0;
    }

    /**
     * @param Icm_Commerce_Product $product
     * @param int $quantity
     */
    public function setQuantity(Icm_Commerce_Product $product, $quantity){
        if ($this->isValidQuantity($quantity)){
            $this->products[$product] = $quantity;
        }
    }

    /**
     * @return Icm_HashMap
     */
    public function getProducts(){
        $x = $this->products; //because PHP passes objects by reference
        return $x;
    }

    /**
     * @return number
     */
    public function getSubtotal(){
        $total = 0;
        foreach ($this->products as $index => $product){
            $quantity = $product[1];
            $product = $product[0];
            $total += $product->initial_price * $quantity;
        }
        return $total;
    }

    /**
     * @param int $quantity
     * @throws Exception
     * @return boolean
     */
    protected function isValidQuantity($quantity){
        if (!is_numeric($quantity) || $quantity < 0){
            throw new Exception("Quantity must be a positive number.");
        }
        return true;
    }

    public function removeAllProducts() {
        $this->products = new Icm_HashMap('Icm_Commerce_Product');
    }

    public function getOrderId() {
        return $this->order_id;
    }
}
