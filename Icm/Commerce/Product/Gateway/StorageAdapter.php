<?php
class Icm_Commerce_Product_Gateway_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Product_Gateway_StorageInterface{
    /**
     * @param Icm_Commerce_Product_Gateway $productGateway
     * @return mixed
     */
    public function save(Icm_Commerce_Product_Gateway $productGateway){
        $exists = $this->findOneByFields(array('gateway_id'=> $productGateway->gateway_id, 'product_id' => $productGateway->product_id));
        if (is_array($exists) && sizeof($exists)){
            // we already have this product_gateway; this is an update operation
            return $this->conn->execute("UPDATE {$this->tableName} SET token=:token, bank=:bank WHERE gateway_id=:gateway_id AND product_id=:product_id;", array(
                ':token' => $productGateway->token,
                ':bank' => $productGateway->bank,
                ':gateway_id' => $productGateway->gateway_id,
                ':product_id' => $productGateway->product_id
            ));
        }
        else{
            return $this->insertOne($productGateway->toArray());
        }
    }

    /**
     * @param Icm_Commerce_Product $product
     * @param Icm_Commerce_Gateway_Abstract $gateway
     * @return Icm_Commerce_Product_Gateway
    */
    public function get(Icm_Commerce_Product $product, Icm_Commerce_Gateway_Abstract $gateway){
        return Icm_Commerce_Product_Gateway::fromArray($this->findOneByFields(
            array(
                'gateway_id' => $gateway->gateway_id,
                'bank' => $gateway->bank,
                'product_id' => $product->product_id
            )
        ));
    }
}