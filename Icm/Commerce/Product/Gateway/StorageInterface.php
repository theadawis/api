<?php
interface Icm_Commerce_Product_Gateway_StorageInterface{
    /**
     * @param Icm_Commerce_Product_Gateway $productGateway
     */
    public function save(Icm_Commerce_Product_Gateway $productGateway);

    /**
     * @param Icm_Commerce_Product $product
     * @param Icm_Commerce_Gateway_Abstract $gateway
     * @return Icm_Commerce_Product_Gateway
     */
    public function get(Icm_Commerce_Product $product, Icm_Commerce_Gateway_Abstract $gateway);
}