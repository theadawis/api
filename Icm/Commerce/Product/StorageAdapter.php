<?php
/**
 * Adapter.php
 * User: chris
 * Date: 12/19/12
 * Time: 10:22 AM
 */
class Icm_Commerce_Product_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Product_StorageInterface
{

    /**
     * Store a product
     * @param Icm_Commerce_Product $product
     * @return boolean true on success
     * @throws Icm_Commerce_Exception
     */
    public function save(Icm_Commerce_Product $product){
        if ($product->product_id){
            $this->updateOne($product->toArray());
        } else{
            $this->insertOne($product->toArray());
            $product->product_id = $this->conn->lastInsert();
        }
    }

    /**
     * Retrieve a product
     * @param int $id
     * @return Icm_Commerce_Product
     */
    public function getById($id) {
        return Icm_Commerce_Product::fromArray($this->findOneBy('product_id', $id));
    }


}
