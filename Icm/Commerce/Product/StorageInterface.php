<?php
interface Icm_Commerce_Product_StorageInterface{
    /**
     * Store a product
     * @param Icm_Commerce_Product $product
     * @return boolean true on success
     * @throws Icm_Commerce_Exception
     */
    public function save(Icm_Commerce_Product $product);

    /**
     * Retrieve a product
     * @param int $id
     * @return Icm_Commerce_Product
     */
    public function getById($id);
}