<?php
/**
 * @property string q_cust_guid
 * @property string q_tran_guid
 */
class Icm_Commerce_Crm_Response{
    protected $code;
    protected $result;
    protected $reason;
    protected $data;

    public function __construct($code, $result, $reason, $data){
        $this->code = $code;
        $this->result = $result;
        $this->reason = $reason;
        $this->data = $data;
    }

    /**
     * @param int $code
     * @return Icm_Commerce_Crm_Response
     */
    public function setCode($code){
        $this->code = $code;
        return $this;
    }

    public function isOk(){
        return $this->code == 1;
    }

    public function __get($key){
        if (isset($this->data[$key])){
            return $this->data[$key];
        }
        return NULL;
    }
}