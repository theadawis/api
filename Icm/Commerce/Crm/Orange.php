<?php
/**
 * CRM interface wrapper for our Orange API adapter
 * @author Joe Linn
 *
 */
class Icm_Commerce_Crm_Orange implements Icm_Commerce_Crm_Interface{
    /**
     * @var Icm_Service_Orange_Api
     */
    protected $api;

    /**
     * @var bool $useTestToken
     */
    protected $useTestToken;

    /**
     * @param Icm_Service_Orange_Api $api
     */
    public function __construct(Icm_Service_Orange_Api $api){
        $this->api = $api;
    }

    /**
     * @param Icm_Service_Orange_Api $api
     * @return Icm_Commerce_Crm_Orange
     */
    public function setApi(Icm_Service_Orange_Api $api){
        $this->api = $api;
        return $this;
    }

    /**
     * Add a customer to the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @return Icm_Commerce_Crm_Response
     */
    public function createCustomer(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order){
        try{
            $data = $this->api->addCustomer($customer, $order);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        return $this->makeResponse($data);
    }

    /**
     * Modify an existing customer's data
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function updateCustomer(Icm_Commerce_Customer $customer){
        try{
            $data = $this->api->updateCustomer($customer);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        return $this->makeResponse($data);
    }

    /**
     * Add a transaction to the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Transaction $transaction
     * @param Icm_Commerce_Product_Gateway $productGateway
     * @return Icm_Commerce_Crm_Response
     */
    public function addTransaction(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway, $status=Icm_Service_Orange_Api::TRANSACTION_STATUS_APPROVED){
        try{
            $data = $this->api->addTransaction($customer, $transaction, $productGateway, Icm_Service_Orange_Api::TRANSACTION_TYPE_PURCHASE, $status);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        return $this->makeResponse($data);
    }

    /**
     * Validate a customer's credentials
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function validateCustomer(Icm_Commerce_Customer $customer){
        try{
            // $data = $this->api->authorizeCustomer($customer);
            $data = $this->api->getCustomer($customer);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        $response = $this->makeResponse($data);
        if (strtoupper($response->q_cust_webpassword) !== strtoupper($customer->password) || (strtolower($response->q_cust_status) != 'active' && strtolower($response->q_cust_status) != 'trial')){
            // TODO: We need to set the user's password to all caps because Orange does this on their end...
            // passwords do not match or user is not active; throw an exception
            throw new Icm_Commerce_Crm_AuthenticationError('Invalid user credentials provided.');
        }
        return $response;
    }

    /**
     * Cancel a customer
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function cancelCustomer(Icm_Commerce_Customer $customer){
        try{
            $data = $this->api->cancelCustomer($customer);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        return $this->makeResponse($data);
    }

    /**
     * Archive a customer
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function archiveCustomer(Icm_Commerce_Customer $customer){
        try{
            $data = $this->api->archiveCustomer($customer);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }
        return $this->makeResponse($data);
    }

    /**
     * Retrive customer data from a CRM
     * @param Icm_Commerce_Customer $customer
     * @param boolean $getCcInfo true to include credit card info in customer object
     * @return Icm_Commerce_Customer
     */
    public function getCustomer(Icm_Commerce_Customer $customer, $getCcInfo = false){
        try{
            $data = $this->api->getCustomer($customer, $getCcInfo);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_Exception($e->getMessage());
        }

        $this->checkResponse($data);

        $orangeCustomer = Icm_Commerce_Customer::fromArray(array(
            'email' => $data['q_cust_email'],
            'first_name' => $data['q_cust_first_name'],
            'last_name' => $data['q_cust_last_name'],
            'zip' => $data['q_cust_bill_zip'],
            'guid' => $data['q_cust_guid']
        ));
        $orangeCustomer->setGateway($customer->getGateway());

        if ($getCcInfo && isset($data['q_cust_ccname'])){
            // this customer token has billing querying enabled
            $name = explode(' ', $data['q_cust_ccname']);
            $creditcard = Icm_Commerce_Creditcard::fromArray(array(
                'number' => $data['q_cust_ccacct'],
                'expiration_month' => substr($data['q_cust_ccexpire'], 0, 2),
                'expiration_year' => '20'.substr($data['q_cust_ccexpire'], 2), //change this to be y2.1k compliant
                'first_name' => $name[0],
                'last_name' => implode(' ', array_slice($name, 1, sizeof($name) - 1))
            ));
            $orangeCustomer->setCreditCard($creditcard);
        }

        return $orangeCustomer;
    }

    /**
     * Refund a transaction via the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Transaction $transaction
     * @param Icm_Commerce_Product_Gateway $productGateway
     * @return Icm_Commerce_Crm_Response
     */
    public function refund(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway){
        try{
            $data = $this->api->refundCustomer($customer, $transaction, $productGateway);
        }
        catch(Icm_Service_Orange_Exception $e){
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }

        return $this->makeResponse($data);
    }

    /**
     * @param Icm_Commerce_Customer $customer
     * @param $comment
     * @param string $subject
     */
    public function addComment(Icm_Commerce_Customer $customer, $comment, $subject = "") {
        try{
            $data = $this->api->addCustomerComment($customer, $subject, $comment);
        } catch(Icm_Service_Orange_Exception $e) {
            throw new Icm_Commerce_Crm_NotAvailableError($e->getMessage());
        }

        return $this->makeResponse($data);
    }

    /**
     * @param array $data
     * @return Icm_Commerce_Crm_Response
     */
    protected function makeResponse(array $data){
        $this->checkResponse($data);
        return new Icm_Commerce_Crm_Response($data['code'], $data['result'], $data['reason'], $data);
    }

    protected function checkResponse(array $data){
        switch($data['code']){
            case 2: //refused
                throw new Icm_Commerce_Crm_Exception($data['result'] . ': ' . $data['reason'], $data['code']);
                break;
            case 3: //error
                throw new Icm_Commerce_Crm_Exception($data['result'] . ': ' . $data['reason'], $data['code']); //TODO: do we want a different exception for this?
                break;
            default: //code 1, everything worked
        }
    }

    public function setTestToken($bool) {
        $this->useTestToken = $bool;
        $this->api->useTestToken($bool);
    }
}
