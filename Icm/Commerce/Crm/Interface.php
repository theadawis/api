<?php
interface Icm_Commerce_Crm_Interface{
    /**
     * Add a customer to the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @return Icm_Commerce_Crm_Response
     */
    public function createCustomer(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order);

    /**
     * Modify an existing customer's data
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function updateCustomer(Icm_Commerce_Customer $customer);

    /**
     * Retrive customer data from a CRM
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function getCustomer(Icm_Commerce_Customer $customer);

    /**
     * Add a transaction to the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Transaction $transaction
     * @param Icm_Commerce_Product_Gateway $productGateway
     * @return Icm_Commerce_Crm_Response
     */
    public function addTransaction(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway);

    /**
     * Validate a customer's credentials
     * @param Icm_Commerce_Customer $customer
     * @return Icm_Commerce_Crm_Response
     */
    public function validateCustomer(Icm_Commerce_Customer $customer);

    /**
     * Remove a customer from their current billing plan
     * @abstract
     * @param Icm_Commerce_Customer $customer
     * @return mixed
     */
    public function cancelCustomer(Icm_Commerce_Customer $customer);

    /**
     * @abstract
     * @param Icm_Commerce_Customer $customer
     * @return mixed
     */
    public function archiveCustomer(Icm_Commerce_Customer $customer);

    /**
     * Refund a transaction via the CRM
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Transaction $transaction
     * @param Icm_Commerce_Product_Gateway $productGateway
     * @return Icm_Commerce_Crm_Response
     */
    public function refund(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway);

    public function setTestToken($bool);

    public function addComment(Icm_Commerce_Customer $customer, $comment, $subject = "");
}
