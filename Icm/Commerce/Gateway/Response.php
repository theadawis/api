<?php
class Icm_Commerce_Gateway_Response
{
    protected $transaction_id;
    protected $authcode;
    protected $status;
    protected $message;
    protected $raw;

    public function __construct($transaction_id, $authcode, $status, $message, $raw){
        $this->transaction_id = $transaction_id;
        $this->authcode = $authcode;
        $this->status = $status;
        $this->message = $message;
        $this->raw = $raw;
    }

    /**
     * @return string
     */
    public function getTransactionId(){
        return $this->transaction_id;
    }

    public function getAuthCode(){
        return $this->authcode;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getMessage(){
        return $this->message;
    }

    public function getRawResponse(){
        return $this->raw;
    }
}
