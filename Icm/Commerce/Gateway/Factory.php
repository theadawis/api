<?php
/**
 * Factory.php
 * User: chris
 * Date: 1/11/13
 * Time: 9:20 AM
 */
class Icm_Commerce_Gateway_Factory
{

    protected $config;

    public function __construct(Icm_Config $config) {
        $this->config = $config;
    }

    public function nmi($data = array()) {
        $gateway = new Icm_Commerce_Gateway_Nmi(new Icm_Service_Nmi_Api($this->config->getSection("nmi")));
        foreach ($data as $k => $v){
            $gateway->$k = $v;
        }
        return $gateway;
    }

    public function litle($data = array()) {
        $gateway = new Icm_Commerce_Gateway_Litle(new Icm_Service_Litle_Api($this->config->getSection("litle")));
        foreach ($data as $k => $v){
            $gateway->$k = $v;
        }
        return $gateway;
    }

}
