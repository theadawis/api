<?php
/**
 * Abstract.php
 * User: chris
 * Date: 1/3/13
 * Time: 4:22 PM
 */
abstract class Icm_Commerce_Gateway_Abstract extends Icm_Struct
{

    public $gateway_id,
            $short_name,
            $name,
            $bank,
            $created,
            $updated = '';

    public function getGatewayId() {
        return $this->gateway_id;
    }
}
