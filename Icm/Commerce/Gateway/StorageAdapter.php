<?php
class Icm_Commerce_Gateway_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Gateway_StorageInterface{

    /**
     * @var Icm_Commerce_Gateway_Factory
     */
    protected $factory;

    /**
     * @param Icm_Commerce_Gateway_Factory $factory
     */
    public function __construct($tableName, $pk, Icm_Db_Interface $conn, Icm_Commerce_Gateway_Factory $factory) {
        parent::__construct($tableName, $pk, $conn);
        $this->factory = $factory;
    }

    /**
     * @param Icm_Commerce_Gateway_Abstract $gateway
     */
    public function save(Icm_Commerce_Gateway_Abstract $gateway){
        if ($gateway->gateway_id){
            // return $this->updateOne($gateway->toArray());
        }
        else{
            $this->insertOne($gateway->toArray());
            $gateway->gateway_id = $this->getLastInsert();
        }
    }

    /**
     * @param int $id
     * @return Icm_Commerce_Gateway_Abstract
    */
    public function getById($id){
        $data = $this->findOneBy('gateway_id', $id);
        $name = strtolower($data['short_name']);
        if (!method_exists($this->factory, $name)){
            throw new Icm_Commerce_Gateway_Exception("No gateway could be found by the name of {$name}");
        }
        return $this->factory->$name($data);
    }
}