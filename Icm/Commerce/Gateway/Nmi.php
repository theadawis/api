<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Commerce_Gateway_Nmi extends Icm_Commerce_Gateway_Abstract implements Icm_Commerce_Gateway_Interface{
    /**
     * @var Icm_Service_Nmi_Api
     */
    protected $api;

    /**
     * @var bool $sandbox
     */
    protected  $sandbox;

    /**
     * @param Icm_Service_Nmi_Api $api
     */
    public function __construct(Icm_Service_Nmi_Api $api){
        $this->api = $api;
    }

    /**
     * Charge a credit card
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @return Icm_Commerce_Gateway_Response
     */
    public function process(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order, $amount = false, $reportGroup = ""){
        try{
            $response = $this->api->sale($customer, $order);
        }
        catch(Icm_Service_Nmi_Exception $e){
            throw new Icm_Commerce_Gateway_ValidationError($e->getMessage());
        }
        $this->checkResponse($response['response'], $response['response_code'], $response['responsetext']);
        return new Icm_Commerce_Gateway_Response($response['transactionid'], $response['authcode'], $response['response_code'], $response['responsetext'], $response);
    }

    /**
     * Refund
     * @param Icm_Commerce_Creditcard $card
     * @param float $amount
     * @return Icm_Commerce_Gateway_Response
    */
    public function refund(Icm_Commerce_Transaction $transaction, Icm_Commerce_Customer $customer, $amount = false, $reportGroup = ""){
        $response = $this->api->refund($transaction, $amount);
        $this->checkResponse($response['response'], $response['response_code'], $response['responsetext']);
        return new Icm_Commerce_Gateway_Response($response['transactionid'], $response['authcode'], $response['response_code'], $response['responsetext'], $response);
    }

    protected function checkResponse($response, $code, $responseText){

        if ($response != 1){
            // something went wrong
            switch($code){
                case 200: throw new Icm_Commerce_Gateway_TransactionError("Transaction declined by processor.");
                    break;
                case 201: throw new Icm_Commerce_Gateway_TransactionError("Do not honor.");
                    break;
                case 202: throw new Icm_Commerce_Gateway_InsufficientFunds("Insufficient funds.");
                    break;
                case 203: throw new Icm_Commerce_Gateway_TransactionError("Over limit.");
                    break;
                case 204: throw new Icm_Commerce_Gateway_TransactionError("Transaction not allowed.");
                    break;
                case 221:
                case 222:
                case 223:
                case 224:
                case 225:
                case 250:
                case 251:
                case 252:
                case 253:
                case 260:
                case 261:
                case 262:
                case 263:
                case 264: throw new Icm_Commerce_Gateway_TransactionError($responseText);
                    break;
                case 300: throw new Icm_Commerce_Gateway_TransactionError("Transaction rejected by gateway.");
                    break;
                default: throw new Icm_Commerce_Gateway_TransactionError($responseText);
            }
        }
    }

    public function getReportGroupCode() {
        return "SLCT";
    }


    public function setSandbox($bool) {
        $this->sandbox = $bool;
        $this->api->setSandbox($bool);
    }
}