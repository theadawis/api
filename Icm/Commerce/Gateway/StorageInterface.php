<?php
/**
 * @author Joe Linn
 *
 */
interface Icm_Commerce_Gateway_StorageInterface{
    /**
     * @param Icm_Commerce_Gateway_Abstract $gateway
     */
    public function save(Icm_Commerce_Gateway_Abstract $gateway);

    /**
     * @param int $id
     * @return Icm_Commerce_Gateway_Abstract
     */
    public function getById($id);
}