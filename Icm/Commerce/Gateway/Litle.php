<?php
/**
 * Icm_Commerce_Gateway_Litle.php
 * User: chris
 * Date: 12/21/12
 * Time: 4:00 PM
 */
class Icm_Commerce_Gateway_Litle extends Icm_Commerce_Gateway_Abstract implements Icm_Commerce_Gateway_Interface
{
    const STATUS_SUCESS = "000";


    protected $api;
    protected $useSandbox = false;

    public function __construct(Icm_Service_Litle_Api $api) {
        $this->api = $api;
    }

    /**
     * Charge a credit card
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @param string $reportGroup
     * @return Icm_Commerce_Gateway_Response
     * @throws Icm_Commerce_Gateway_ValidationError
     * @throws Icm_Commerce_Gateway_TransactionError
     * @throws Icm_Commerce_Gateway_InvalidCardError
     * @throws Icm_Commerce_Gateway_NotAvailableError
     */
    public function process(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order, $reportGroup = "") {
        try{
            $rawResponse = $this->api->chargeCard($customer, $order, $reportGroup);
            $transactionId = isset($rawResponse->saleResponse->litleTxnId) ? (string) $rawResponse->saleResponse->litleTxnId : null;
            $message = isset($rawResponse["message"]) ? (string) $rawResponse["message"] : null;
            $message = isset($rawResponse->saleResponse->message) ? (string) $rawResponse->saleResponse->message : $message;
            $responseCode = isset($rawResponse->saleResponse->response) ? (string) $rawResponse->saleResponse->response : null;
            $token = isset($rawResponse->saleResponse->authCode) ? (string) $rawResponse->saleResponse->authCode : null;

            if ($responseCode == self::STATUS_SUCESS){
                return new Icm_Commerce_Gateway_Response($transactionId, $token , $message, $responseCode, $rawResponse->asXML());
            }

            switch($responseCode) {
                case "010":
                    throw new Icm_Commerce_Gateway_TransactionError("Partially Approved");
                    break;
                case "110":
                case "707":
                    throw new Icm_Commerce_Gateway_InsufficientFunds("Insufficient Funds");
                    break;
                case "102":
                    throw new Icm_Commerce_Gateway_TransactionError("Resubmit Transaction");
                    break;
                case "320":
                    throw new Icm_Commerce_Gateway_InvalidCardError("Invalid Expiration Date");
                    break;
                case "305":
                    throw new Icm_Commerce_Gateway_InvalidCardError("Card has expired");
                    break;
                case "352":
                    throw new Icm_Commerce_Gateway_InvalidCardError("Invalid CVV");
                    break;
                default:
                    throw new Icm_Commerce_Gateway_TransactionError($message);
            }

        }
        catch(Icm_Service_Litle_Exception $e) {
            throw new Icm_Commerce_Gateway_ValidationError($e->getMessage());
        } catch(Icm_Util_Http_Exception $e) {
            throw new Icm_Commerce_Gateway_NotAvailableError($e->getMessage());
        }
    }

    /**
     * Refund
     * @param Icm_Commerce_Transaction $transaction
     * @param Icm_Commerce_Customer $customer
     * @param bool|float $amount
     * @param string $reportGroup
     * @throws Icm_Commerce_Gateway_Exception
     * @throws Icm_Commerce_Gateway_TransactionError
     * @throws Icm_Commerce_Gateway_NotAvailableError
     * @internal param \Icm_Commerce_Creditcard $card
     * @return Icm_Commerce_Gateway_Response
     */
    public function refund(Icm_Commerce_Transaction $transaction, Icm_Commerce_Customer $customer, $amount = false, $reportGroup = "") {

        try{
            $rawResponse = $this->api->void($transaction, $reportGroup);
            $transactionId = isset($rawResponse->voidResponse->litleTxnId) ? (string) $rawResponse->voidResponse->litleTxnId : null;
            $message = isset($rawResponse->voidResponse->message) ? (string) $rawResponse->voidResponse->message : null;
            $responseCode = isset($rawResponse->voidResponse->response) ? (string) $rawResponse->voidResponse->response : null;

            if ($responseCode == self::STATUS_SUCESS) {
                return new Icm_Commerce_Gateway_Response($transactionId, null, $responseCode, $message, $rawResponse->asXML());
            }

            switch($responseCode) {
                case "360":
                    throw new Icm_Commerce_Gateway_TransactionError("Invalid transaction id");
                break;
                default:
                    throw new Icm_Commerce_Gateway_Exception("Cannot void transaction", $responseCode);
                break;
            }

        } catch(Icm_Util_Http_Exception $e){
            throw new Icm_Commerce_Gateway_NotAvailableError($e->getMessage());
        }
    }

    public function getReportGroupCode() {
        return "LTL";
    }

    public function setSandbox($bool) {
        $this->useSandbox = $bool;
        $this->api->setSandbox($bool);
    }
}