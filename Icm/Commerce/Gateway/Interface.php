<?php
interface Icm_Commerce_Gateway_Interface{
    /**
     * Charge a credit card
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @param string $reportGroup
     * @return Icm_Commerce_Gateway_Response
     */
    public function process(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order, $reportGroup = "");

    /**
     * Refund
     * @param Icm_Commerce_Creditcard $card
     * @param float $amount
     * @return Icm_Commerce_Gateway_Response
     */
    public function refund(Icm_Commerce_Transaction $transaction, Icm_Commerce_Customer $customer, $amount = false, $reportGroup = "");

    public function getReportGroupCode();
    public function getGatewayId();

    public function setSandbox($bool);
}