<?php
interface Icm_Commerce_Order_StorageInterface{
    /**
     * Store an order record
     * @param Icm_Commerce_Order $order
     * @return boolean true on success
     * @throws Icm_Commerce_Exception
     */
    public function save(Icm_Commerce_Order $order);

    /**
     * Retrieve an order record
     * @param int $id
     * @return Icm_Commerce_Order
     */
    public function getById($id);
}