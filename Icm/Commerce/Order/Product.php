<?php
/**
 * Product.php
 * User: chris
 * Date: 1/4/13
 * Time: 4:20 PM
 */
class Icm_Commerce_Order_Product extends Icm_Struct {
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $order_product_id,
        $order_id,
        $product_id,
        $customer_id,
        $refund_id,
        $status,
        $created,
        $updated = "";
}
