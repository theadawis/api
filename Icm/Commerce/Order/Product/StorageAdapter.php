<?php
/**
 * StorageAdapter.php
 * User: chris
 * Date: 1/4/13
 * Time: 4:24 PM
 */
class Icm_Commerce_Order_Product_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Order_Product_StorageInterface
{

    public function save(Icm_Commerce_Order_Product $order_product) {
        if ($order_product->order_product_id) {
            $this->updateOne($order_product->toArray());
        } else {
            $this->insertOne($order_product->toArray());
            $order_product->order_product_id = $this->conn->lastInsert();
        }
    }

    public function getById($id) {
        return Icm_Commerce_Order_Product::fromArray($this->findOneBy('order_product_id', $id));
    }

    public function getByOrderAndProduct(Icm_Commerce_Order $order, Icm_Commerce_Product $product) {
        return Icm_Commerce_Order_Product::fromArray($this->findOneByFields(array(
            "order_id" => $order->order_id,
            "product_id" => $product->product_id
        )));
    }

    public function getByCustomer(Icm_Commerce_Customer $customer) {
        $return = array();
        $products = $this->findByFields(array("customer_id" => $customer->customer_id));

        if (!empty($products)) {
            foreach ($products as $rawProd){
                $return[] = Icm_Commerce_Order_Product::fromArray($rawProd);
            }
        }

        return $return;
    }

    public function getByOrderAndCustomer(Icm_Commerce_Order $order, Icm_Commerce_Customer $customer) {
        return Icm_Commerce_Order_Product::fromArray($this->findOneByFields(array(
            "order_id" => $order->order_id,
            "customer_id" => $customer->customer_id
        )));
    }

    public function getByProductAndCustomer(Icm_Commerce_Product $product, Icm_Commerce_Customer $customer) {
        $results = $this->findByFields(array(
            "product_id" => $product->product_id,
            "customer_id" => $customer->customer_id
        ));

        $returnedResults = array();

        foreach ($results as $result) {
            $returnedResults[] = Icm_Commerce_Order_Product::fromArray($result);
        }

        return $returnedResults;
    }
}
