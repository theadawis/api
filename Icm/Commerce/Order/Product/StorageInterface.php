<?php
/**
 * Icm_Commerce_Order_Product_StorageInterface.php
 * User: chris
 * Date: 1/4/13
 * Time: 4:22 PM
 */
interface Icm_Commerce_Order_Product_StorageInterface
{
    public function save(Icm_Commerce_Order_Product $product);
    public function getById($id);
    public function getByOrderAndProduct(Icm_Commerce_Order $order, Icm_Commerce_Product $product);
    public function getByOrderAndCustomer(Icm_Commerce_Order $order, Icm_Commerce_Customer $customer);
}
