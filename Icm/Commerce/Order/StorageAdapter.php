<?php

class Icm_Commerce_Order_StorageAdapter extends Icm_Db_Table implements Icm_Commerce_Order_StorageInterface{

    /**
     * Store an order record
     * @param Icm_Commerce_Order $order
     * @return boolean true on success
     * @throws Icm_Commerce_Exception
     */
    public function save(Icm_Commerce_Order $order){
        if ($order->getOrderId()){
            return $this->updateOne($order->toArray());
        }
        else {
            if ($order->visitor_id) {
                $this->conn->execute("INSERT INTO visitors (visitor_id)
                                      VALUES (:visitor_id)
                                      ON DUPLICATE KEY UPDATE visitor_id=visitor_id", array(
                                      "visitor_id" => $order->visitor_id
                ));
            }

            $this->insertOne($order->toArray());
            $order->order_id = $this->getLastInsert();
        }
    }

    /**
     * Retrieve an order record
     * @param int $id
     * @return Icm_Commerce_Order
    */
    public function getById($id){
        return Icm_Commerce_Order::fromArray($this->findOneBy('order_id', $id));
    }
}