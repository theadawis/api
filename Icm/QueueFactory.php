<?php

/**
 * Class with set of Factory Methods for building Zend_Queue objects.
 * Each method builds and returns a Zend_Queue instance that's
 * ready to send and receive messages to the appropriate queue.
 */
class Icm_QueueFactory {
    protected $options;

    /**
     * Constructor. Expects config object with queue options
     *
     * @param Icm_Config $config
     */
    public function __construct(Icm_Config $config) {
        // use the config to set the options
        $defaults = array(
            'host' => 'haproxy',
            'port' => 5672,
            'login' => 'guest',
            'password' => 'guest',
            'vhost' => '/',
        );

        $options = array();

        foreach ($defaults as $key => $value) {
            $options[$key] = $config->getOption($key, $value);
        }

        $this->options = $options;
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the worker queue
     *
     * @return Zend_Queue
     */
    public function getWorkerQueue() {
        // get the config options for the zend queue
        $defaults = array(
            'name' => 'icm_work_queue',
            'exchange_name' => 'worker',
            'routing_key' => 'icm_work',
        );

        $options = array_merge($this->options, $defaults);

        // return a new zend queue with the rabbitmq adapter
        return $this->buildRabbitmqQueue($options);
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the feedback queue
     *
     * @return Zend_Queue
     */
    public function getFeedbackQueue() {
        // get the config options for the zend queue
        $defaults = array(
            'name' => 'icm_feedback_queue',
            'exchange_name' => 'offline',
            'routing_key' => 'icm_feedback',
        );

        $options = array_merge($this->options, $defaults);

        // return a new zend queue with the rabbitmq adapter
        return $this->buildRabbitmqQueue($options);
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the email validate queue
     *
     * @return Zend_Queue
     */
    public function getEmailValidateQueue() {
        $defaults = array(
            'name' => 'icm_email_validate_queue',
            'exchange_name' => 'email',
            'routing_key' => 'icm_email_validate'
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the email send queue
     *
     * @return Zend_Queue
     */
    public function getEmailSendQueue() {
        $defaults = array(
            'name' => 'icm_email_send_queue',
            'exchange_name' => 'email',
            'routing_key' => 'icm_email_send'
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the email partners queue
     *
     * @return Zend_Queue
     */
    public function getEmailPartnersQueue() {
        $defaults = array(
            'name' => 'icm_email_partners_queue',
            'exchange_name' => 'email',
            'routing_key' => 'icm_email_partners'
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the create click queue
     *
     * @return Zend_Queue
     */
    public function getCreateClickQueue() {
        $defaults = array(
            'name' => 'icm_create_click_queue',
            'exchange_name' => 'tracker',
            'routing_key' => 'icm_create_click',
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the update click queue
     *
     * @return Zend_Queue
     */
    public function getUpdateClickQueue() {
        $defaults = array(
            'name' => 'icm_update_click_queue',
            'exchange_name' => 'tracker',
            'routing_key' => 'icm_update_click',
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending and receiving messages to the postback queue
     *
     * @return Zend_Queue
     */
    public function getPostbackQueue() {
        $defaults = array(
            'name' => 'icm_postback_queue',
            'exchange_name' => 'tracker',
            'routing_key' => 'icm_postback',
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Factory method for building a Zend_Queue object for
     * sending messages to the splittest queue
     */
    public function getSplitTestQueue() {
        $defaults = array(
           'name' => 'icm_splittest_queue',
           'exchange_name' => 'splittest',
           'routing_key' => ''
        );
        return $this->buildRabbitmqQueue(array_merge($this->options, $defaults));
    }

    /**
     * Build a new Zend_Queue for rabbitmq
     *
     * @param array $options
     * @return Zend_Queue
     */
    protected function buildRabbitmqQueue($options) {
        // build a new zend queue with the rabbitmq adapter
        $adapter = new Icm_Queue_Adapter_Rabbitmq($options);
        $queue = new Zend_Queue($adapter, $options);

        // return the queue
        return $queue;
    }
}

