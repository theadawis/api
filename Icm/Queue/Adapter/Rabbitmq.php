<?php

/**
 * Class for communicating with RabbitMQ
 * Meant for use with Zend_Queue
 */
class Icm_Queue_Adapter_Rabbitmq extends Zend_Queue_Adapter_AdapterAbstract {
    protected $connection;
    protected $channel;
    protected $exchange;

    // setup defaults
    protected $defaults = array(
        'host' => 'localhost',
        'port' => 5672,
        'login' => 'guest',
        'password' => 'guest',
        'exchange_name' => 'default',
        'exchange_type' => AMQP_EX_TYPE_DIRECT,
    );

    /**
     * Constructor
     *
     * @param  array|Zend_Config $config An array having configuration data
     * @param  Zend_Queue The Zend_Queue object that created this class
     */
    public function __construct($options, Zend_Queue $queue = null) {
        parent::__construct($options);

        // set defaults if not passed in
        foreach ($this->defaults as $key => $value) {
            if (!isset($this->_options[$key]) || empty($this->_options[$key])) {
                $this->_options[$key] = $value;
            }
        }

        // connect to the rabbitmq server
        $this->connect();

        // open a new channel
        $this->openChannel();

        // declare the exchange
        $this->declareExchange();
    }

    /**
     * Destructor
     */
    public function __destruct() {
        // gracefully disconnect
        $this->connection->disconnect();
    }

    /**
     * Send a new message to the queue
     *
     * @param string or array $message
     * @param Zend_Queue $queue
     * @return bool
     */
    public function send($message, Zend_Queue $queue = null) {
        // use our queue if none sent
        if (empty($queue)) {
            $queue = $this->_queue;
        }

        // if no queue set at all, throw exception
        if (empty($queue)) {
            throw new Zend_Queue_Exception('Must have queue to send messages');
        }

        // if the message is empty, don't publish it
        $stripped_message = str_replace(' ', '', $message);
        if (empty($stripped_message)) {
            Icm_Util_Logger_Syslog::getInstance('api')->logErr('Received empty message for queue: ' . $queue->getName());
            return false;
        }

        // if the message is an array, convert to json
        if (is_array($message)) {
            $message = json_encode($message);
        }

        // declare the queue
        $amqp_queue = $this->declareQueue($queue);

        // use the queue name as the routing key
        $routing_key = $queue->getOption('routing_key');

        if (empty($routing_key)) {
            $routing_key = $queue->getName() . '.key';
        }

        // publish the message to our exchange
        return $this->exchange->publish($message, $routing_key);
    }

    /**
     * Pull messages from the queue
     *
     * @param int $maxMessages
     * @param int $timeout
     * @param Zend_Queue $queue
     * @return Zend_Queue_Message_Iterator
     */
    public function receive($maxMessages = 1, $timeout = null, Zend_Queue $queue = null) {
        // use our queue if none provided
        if (empty($queue)) {
            $queue = $this->_queue;
        }

        // declare the queue
        $amqp_queue = $this->declareQueue($queue);

        // pull the max messages from the queue
        $data = array();

        for ($i = 0; $i < $maxMessages; $i++) {
            $message = $amqp_queue->get(AMQP_AUTOACK);

            if (!empty($message)) {
                $data[] = array('body' => $message->getBody());
            }
        }

        $options = array(
            'queue' => $queue,
            'data' => $data,
            'messageClass' => $queue->getMessageClass()
        );

        return new Zend_Queue_Message_Iterator($options);
    }

    /**
     * Purge all the messages from the connected queue
     *
     * @param Zend_Queue $queue
     */
    public function purgeQueue(Zend_Queue $queue = null) {
        // use our queue if none provided
        if (empty($queue)) {
            $queue = $this->_queue;
        }

        // declare the queue
        $amqp_queue = $this->declareQueue($queue);

        // purge the queue
        $amqp_queue->purge();
    }

    /**
     * List the methods this adapter implements
     * @return array
     */
    public function getCapabilities() {
        $methods = array(
            'create' => false,
            'delete' => false,
            'getQueues' => false,
            'isExists' => false,
            'count' => false,
            'send' => true,
            'receive' => true,
            'deleteMessage' => false,
        );

        return $methods;
    }

    public function deleteMessage(Zend_Queue_Message $message) {
        throw new Zend_Queue_Exception('deleteMessage() method is not implemented on this adapter');
    }

    public function count(Zend_Queue $queue = null) {
        throw new Zend_Queue_Exception('count() method is not implemented on this adapter');
    }

    public function isExists($name) {
        throw new Zend_Queue_Exception('isExists() method is not implemented on this adapter');
    }

    public function create($name, $timeout = null) {
        throw new Zend_Queue_Exception('create() method is not implemented on this adapter');
    }

    public function delete($name) {
        throw new Zend_Queue_Exception('delete() method is not implemented on this adapter');
    }

    public function getQueues() {
        throw new Zend_Queue_Exception('getQueues() method is not implemented on this adapter');
    }

    /**
     * Connect to the given RabbitMQ host
     *
     * @return AMQPConnection
     * @throws Exception
     */
    protected function connect() {
        // create new connection
        $this->connection = new AMQPConnection($this->_options);

        // try to connect
        if ($this->connection->connect()) {
            return $this->connection;
        }

        // if failed, throw an exception
        throw new Exception("Couldn't connect to AMQP");
    }

    /**
     * Open a new RabbitMQ Channel
     */
    protected function openChannel() {
        $this->channel = new AMQPChannel($this->connection);
        return $this->channel;
    }

    /**
     * Declare a new RabbitMQ exchange
     *
     * @param string $name
     * @param string $type
     * @return AMQPExchange
     */
    protected function declareExchange() {
        // create the exchange
        $exchange = new AMQPExchange($this->channel);

        // set attributes
        $exchange->setName($this->_options['exchange_name']);
        $exchange->setType($this->_options['exchange_type']);
        $exchange->setFlags(AMQP_DURABLE);

        // declare and return
        $exchange->declareExchange();
        $this->exchange = $exchange;

        return $this->exchange;
    }

    /**
     * Declare a new AMQP Queue
     *
     * @param Zend_Queue $queue
     * @return AMQPQueue
     */
    protected function declareQueue(Zend_Queue $queue) {
        // create and define the queue
        $amqp_queue = new AMQPQueue($this->channel);
        $amqp_queue->setName($queue->getName());
        $amqp_queue->setFlags(AMQP_DURABLE);
        $amqp_queue->declareQueue();

        $routing_key = $queue->getOption('routing_key');

        if (empty($routing_key)) {
            $routing_key = $queue->getName() . '.key';
        }

        $amqp_queue->bind($this->exchange->getName(), $routing_key);

        $this->_amqp_queue = $amqp_queue;
        return $this->_amqp_queue;
    }
}