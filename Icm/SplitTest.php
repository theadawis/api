<?php

class Icm_SplitTest implements Icm_SplitTest_Interface {

    /*All public functions return:
     *Array
    *  status: 1000 = success/no errors
    *          1910 = Insufficient parameters supplied by caller
    *          1920 = Invalid parameters supplied by caller
    *          1930 = Database did not update
    *  return:
    *          [relevant information for specific call]
    *  optionalMessage:
    *          [relevant information for specific call]
    */

    // Split test status constants
    const STATUS_DRAFT = 1;
    const STATUS_PENDING = 2;
    const STATUS_LIVE = 3;
    const STATUS_PAUSED = 4;
    const STATUS_ARCHIVED = 5;

    protected $conn;
    protected $status;
    protected $msg;
    protected $results;
    protected $redis;

    // Split test status not constants:
    protected $DRAFT = 1;
    protected $PENDING = 2;
    protected $LIVE = 3;
    protected $PAUSED = 4;
    protected $ARCHIVED = 5;

    // Split Variation status
    protected $VARLIVE = 1;
    protected $VARARCHIVED = 2;

    // use this helper function to ensure that our expected return structure is satisfied
    protected function validateReturn($result, $caller) {

        if (empty($result)) {
            throw new Exception("Return array empty for: $caller .");
        }

        if (!array_key_exists('status', $result)) {
            throw new Exception("Status not found in return array for: $caller .");
        }

        if (!array_key_exists('return', $result)) {
            throw new Exception("Return key not found in return array for: $caller .");
        }

        if ($result['status'] != 1000) {
            $status = $result['status'];
            $return = $result['return'];
            $msg = "";

            if (array_key_exists('optionalMessage', $result)){
                $msg = $result['optionalMessage'];
            }
            throw new Exception("Status was " . $status ." when validating return array for: $caller.  Message: " . $msg . ". Returned: " . json_encode($return));
        }
        // Else we pass, throw nothing and exit
    }


    protected function validateOpts($optParams, $tests){

        foreach ($tests as $test => $value){

            if (array_key_exists($test, $optParams)){

                if ($value == "string"){

                    if (!is_string($optParams[$test])){
                        $this->msg = "$test must be a valid string";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value =="int"){

                    if (!is_int($optParams[$test])){
                        $this->msg = "$test must be a valid integer";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "bool"){

                    if ($optParams[$test] != 0 && $optParams[$test] != 1){
                        $this->msg = "$test must be a boolean value of 0 or 1";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "array"){

                    if (!is_array($optParams[$test])){
                        $this->msg = "$test must be an array";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "float"){

                    if (!is_float($optParams[$test])){
                        $this->msg = "$test must be a float";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }
            }
        }
    }

    protected function validateReqs($reqParams, $tests){

        foreach ($tests as $test => $value){

            if (array_key_exists($test, $reqParams) && $reqParams[$test] !== null){

                if ($value == "string"){

                    if (!is_string($reqParams[$test])){
                        $this->msg = "$test must be a valid string";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "int"){

                    if (!is_int($reqParams[$test])){
                        $this->msg = "$test must be an integer";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "bool"){

                    if ($reqParams[$test] != 0 && $reqParams[$test] != 1){
                        $this->msg = "$test must be boolean value of 1 or 0";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "array"){

                    if (!is_array($reqParams[$test])){
                        $this->msg = "$test must be an array";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }

                if ($value == "float"){

                    if (!is_float($reqParams[$test])){
                        $this->msg = "$test must be a float";
                        $this->status = 1920;
                        throw new Exception($this->msg);
                    }
                }
            }

            else{
                $this->msg = "You must include $test as a parameter";
                $this->status = 1910;
                throw new Exception($this->msg);
            }
        }
    }


    protected function getAppForContextId($section_id){
        // app should tell us what their app id is
        // for now, its always 1 so hard coding it until i come back to this
        return 1;

        $sql = "SELECT
            `app_id`
            FROM
            `split_section`
            WHERE
            `id` = :id";
        $params = array(':id' => $section_id);
        $results = $this->conn->fetch($sql, $params);
        $app_id = 0;

        foreach ($results as $context){
            $app_id = intval($context['app_id']);
        }

        return $app_id;

    }

    protected function getAppAndContextFromTestId($test_id){

        try{

            if ($test_id == null || $test_id < 1) {
                throw new Exception("Test id invalid or null. ");
            }

            $sql = "SELECT
                `app_id`, `section_id`
                FROM
                `split_test`
                WHERE
                `id` = :test_id
                ";
            $params = array(':test_id' => $test_id);
            $result = $this->conn->fetch($sql, $params);

            if (empty($result) || !array_key_exists('app_id', $result) || !array_key_exists('section_id', $result)) {
                throw new Exception("No App and Context for Test Id");
            }

            return $result;
        }
        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error while obtaining Application ID and context ID" . $e->getMessage();

            return $this->compileReturn();
        }

    }


    protected function getWeightFromTestId($test_id) {

        try {

            if ($test_id == null || $test_id < 1) {
                throw new Exception("Test id invalid or null");
            }

            $sql = "SELECT
                `weight`
                FROM
                `split_test`
                WHERE
                `id` = :test_id
                ";
            $params = array(':test_id' => $test_id);
            $weight = $this->conn->fetch($sql, $params);

            if (empty($weight)) {
                throw new Exception("No information could be found for the test id: $test_id");
            }

            return $weight['weight'];
        }
        catch(Exception $e) {
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error while getting weight from test ID" . $e->getMessage();

            return $this->compileReturn();
        }

    }

    protected function compileReturn() {
        $retArr = array('status' => $this->status, 'return' => $this->results, 'optionalMessage' => $this->msg);

        return $retArr;
    }

    public function __construct(Icm_Db_Interface $dbconn, Icm_Redis $redconn) {
        $this->conn = $dbconn;
        $this->status = 1000;
        $this->msg = "";
        $this->results = array();
        $this->redis = $redconn;
    }

    protected function getSegmentIdsFromKeyVals($segments) {

        try {
            $params = array();
            $sql = "SELECT
                `id`
                FROM
                `cg_segment`
                WHERE ";

            foreach ($segments as $key => $segment){

                if ($key != 0) {
                    $sql .= " OR ";
                }

                $paramKeyKey = ":key$key";
                $paramKeyVal = ":value$key";
                $sql .= " (`key` = $paramKeyKey AND
                `value` = $paramKeyVal ) ";
                $params[$paramKeyKey] = $segment['key'];
                $params[$paramKeyVal] = $segment['value'];

            }

            $segIds = $this->conn->fetchAll($sql, $params);

            $retSegIds = array();

            foreach ($segIds as $segId) {
                $retSegIds[] = $segId['id'];
            }

            return $retSegIds;
        }
        catch (Exception $e) {
            $this->status = 1200;
            $this->msg = "Error getting Segment Ids from Key Vals" . $e->getMessage();
            $this->results = array();

            return $this->compileReturn();
        }
    }

    // get the meta associated with the variation id
    public function getTestMeta($variationId){
        $results = $this->getTestMetaWrapped($variationId);

        return $results['return'];
    }


    public function getTestMetaWrapped($variationId){

        try{
            $variationId = (int) $variationId;
            $reqParams = array('variation_id' => $variationId);
            $tests = array('variation_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $results = $this->listVariations(array('variation_id' => $variationId));
            $this->validateReturn($results, "getTestMetaWrappedListVar:#$variationId");
            $metas = array();

            foreach ($results['return'] as $var){
                $metas = $var['meta'];
            }

            $retMeta = array();

            foreach ($metas as $meta ){
                $retMeta[$meta['key']] = $meta['value'];
            }

            $this->results = $retMeta;
            $this->status = 1000;
            $this->msg = "Succesfully grabbed test meta for the variation: $variationId";

            return $this->compileReturn();
        }

        catch(Exception $e){
            $this->status = 1200;
            $this->results = new Icm_SplitTest_Variation(array('sectionId' => 0, 'variationId' => 0, 'testId' => 0));
            $this->msg = "Error getting meta for test's variation id";

            return $this->compileReturn();
        }
    }

    // get test data for variation
    public function getTestByVariation($variationId){
        $results = $this->getTestByVariationWrapped(intval($variationId));

        return $results['return'];
    }

    public function getTestByVariationWrapped($variationId){

        try{
            $reqParams = array('variation_id' => $variationId);
            $tests = array('variation_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $results = $this->listVariations(array('variation_id' => $variationId));
            $var = array();

            foreach ($results['return'] as $variation){
                $var = $variation;
            }

            // return a test 0 for this call
            $test = array(
                'id' => 0,
                'app_id' => 0,
                'control_variation_id' => 0,
                'section_id' => 0,
                'type' => "",
                'name' => "",
                'desc' => "",
                'weight' => "",
                'status' => 0,
                'last_modified' => 0,
                'start_time' => 0,
                'end_time' => 0
            );

            $retTest = array();
            $retTest['test'] = $test;
            $retTest['variation'] = $var;


            $splittest = new Icm_SplitTest_Variation(array('sectionId' => $test['section_id'], 'testId' => $test['id'], 'variationId' => $var['id']));

            // put each meta item into the test
            foreach ($var['meta'] as $meta){
                $splittest->setMeta($meta['key'], $meta['value']);
            }


            $this->results = $splittest;
            $this->status = 1000;
            $this->msg = "";

            return $this->compileReturn();
        }

        catch(Excpetion $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error getting specific variation and test 0";

            return $this->compileReturn();
        }
    }


    // return true/false to see if the given segment is a super segment
    public function isSuperSegment($type, $value){
        $results = $this->isSuperSegmentWrapped($type, $value);

        return $results['return'];
    }

    public function isSuperSegmentWrapped($type, $value){

        try{
            $reqParams = array('type' => $type, 'value' => $value);
            $tests = array('type' => 'string', 'value' => 'string');
            $this->validateReqs($reqParams, $tests);

            $sql = "SELECT
                `super`
                FROM
                `cg_segment`
                WHERE
                `key` = :key
                AND
                `value` = :value
                LIMIT 1
                ";
            $params = array(':key' => $type, ':value' => $value);
            $results = $this->conn->fetchAll($sql, $params);
            $super = false;

            foreach ($results as $segment){

                if (intval($segment['super']) == 1) {
                    $super = true;
                }
                else {
                    $super = false;
                }
            }

            $this->status = 1000;
            $this->results = $super;
            $this->msg = "Success";

            return $this->compileReturn();
        }
        catch(Exception $e){
            $this->status = 1200;
            $this->results = false;
            $this->msg = "Error while determining if type:$type / value:$value is super segment" . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // get the segment id for a key/val pair
    public function getSegmentId($type, $value){
        $results = $this->getSegmentIdWrapped($type, $value);

        return $results['return'];
    }

    public function getSegmentIdWrapped($type, $value){

        try{
            $key = $type;
            $reqParams = array('key' => $key, 'value' => $value);
            $tests = array('key' => 'string', 'value' => 'string');
            $this->validateReqs($reqParams, $tests);
            $results = $this->getSegmentIdsFromKeyVals(array(
                array('key' => $type, 'value' => $value)
            )
            );

            $segId = 0;

            foreach ($results as $segment){
                $segId = intval($segment);
            }

            if ($segId == 0) {
                throw new Exception("No segment id found for the key: $key and val:$value");
            }

            $this->status = 1000;
            $this->results = $segId;
            $this->msg = "Successfully fetched segment id";

            return $this->compileReturn();
        }
        catch(Excpetion $e){
            $this->status = 1200;
            $this->results = 0;
            $this->msg = "Error getting segment id for the key:$type and val:$value. " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // Internal helper function, accepts a variation's `meta_ids` string
    // Explodes the string and fetches associated meta info for each meta id
    protected function decodeMeta($metaIds){

        try{

            if ($metaIds == null || $metaIds == "") {
                return array();
            }

            $metaIds = explode(',', $metaIds);

            // print_r($metaIds);
            $metaStr = "";

            foreach ($metaIds as $index => $meta){

                if ($index != 0) {
                    $metaStr .= " OR ";
                }

                $metaStr .= " `id` = " . $meta;
            }

            $sql = "SELECT * FROM `split_meta`
                WHERE " . $metaStr;
            $metaInfo = $this->conn->fetchAll($sql);

            return $metaInfo;
        }
        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error while decoding meta info: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createApplication($name, $optParams=array()){

        try{
            // Grab the optional parameters if they're available
            $desc = "";
            $url = "";
            $tests = array('desc' => 'string', 'url' => 'string');

            // No return value, if there's an error we catch the exception
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            if (array_key_exists('url', $optParams)) {
                $url = $optParams['url'];
            }

            // Check the required params are valid
            $reqParams = array('name' => $name);
            $tests = array('name' => 'string');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert statement
            $results = $this->conn->execute('
                INSERT INTO
                `cg_application`(`name`, `desc`, `url`)
                VALUES
                (:appname, :desc, :url)', array(':appname' => $name, ':desc' => $desc, ':url' => $url)
            );

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Application not created";
            }
            else{
                $this->results = array('app_id' => $id);
                $this->msg = "Successfully created application";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){
            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating application: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listApplications($optParams = array()){

        try{
            // Get optional parameters
            // In this case, app id will trump name if both are supplied
            $app_id = null;
            $name = "";
            $tests = array('app_id' => 'int', 'name' => 'string');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('app_id', $optParams)) {
                $app_id = $optParams['app_id'];
            }

            if (array_key_exists('name', $optParams)) {
                $name = $optParams['name'];
            }

            $params = array();
            $sql ="
                SELECT
                `id`, `name`, `desc`, `url`
                FROM
                `cg_application`
                ";

            if ($app_id != null){
                $sql = $sql . " WHERE `id` = :app_id";
                $params = array(':app_id' => $app_id);
            }

            else if ($name != ""){
                $sql = $sql . " WHERE `name` = :name";
                $params = array(':name' => $name);
            }
            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){
            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing applications: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteApplication($app_id){
        try{
            // Only 1 required param to validate
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `cg_application`
                WHERE
                `id` = :app_id
                ";
            $params = array(':app_id' => $app_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting application: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createContext($name, $type, $slug, $app_id, $optParams=array()){

        try{
            // validate the optional parameters
            $default_variation_id = null;
            $control_variation_id = null;
            $order = 99;
            $tests = array(    'default_variation_id' => 'int',
                'control_variation_id' => 'int',
                'order' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('control_variation_id', $optParams)) {
                $control_variation_id = $optParams['control_variation_id'];
            }

            if (array_key_exists('default_variation_id', $optParams)) {
                $default_variation_id = $optParams['default_variation_id'];
            }

            if (array_key_exists('order', $optParams)) {
                $order = $optParams['order'];
            }

            // validate the required params
            $reqParams = array('name' => $name, 'type' => $type, 'slug' => $slug, 'app_id' => $app_id);
            $tests = array('name' => 'string', 'type' => 'string', 'slug' => 'string', 'app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert
            $sql = "INSERT INTO
                `split_section`
                (`name`, `type`, `slug`, `app_id`, `default_variation_id`, `control_variation_id`, `order`)
                VALUES
                (:name, :type, :slug, :app_id, :default_variation_id, :control_variation_id, :order)
                ";

            $params = array(':name' => $name,
                ':type' => $type,
                ':slug' => $slug,
                ':app_id' => $app_id,
                ':default_variation_id' => $default_variation_id,
                ':control_variation_id' => $control_variation_id,
                ':order' => $order);

            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Context not created";
            }
            else{
                $this->results = array('section_id' => $id);
                $this->msg = "Successfully created context";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating context: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listContexts($app_id, $optParams = array()){

        try{
            // Get optional parameters
            // If context id and name supplied, use the context id
            $section_id = null;
            $name = "";
            $type= "";
            $slug = "";
            $limit = null;
            $tests = array('section_id' => 'int', 'name' => 'string', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('name', $optParams)) {
                $name = $optParams['name'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('slug', $optParams)) {
                $slug = $optParams['slug'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required parameters
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `id`, `name`, `type`, `slug`,
                `app_id`, `default_variation_id`,
                `control_variation_id`, `url`, `path`
                FROM
                `split_section`
                WHERE
                `app_id` = :app_id
                ";

            if ($section_id != ""){
                $sql = $sql . " AND `id` = :section_id";
                $params[':section_id'] = $section_id;
            }

            else if ($name != ""){
                $sql = $sql . " AND `name` = :name";
                $params[':name'] = $name;
            }

            else if ($type != ""){
                $sql = $sql . " AND `type` = :type";
                $params[':type'] = $type;
            }

            else if ($slug != ""){
                $sql = $sql . " AND `slug` = :slug";
                $params[':slug'] = $slug;
            }

            $sql = $sql . " ORDER BY `order` ";

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();

        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing contexts: " . $e->getMessage();

            return $this->compileReturn();
        }
    }


    public function updateContext($section_id, $optParams = array()){

        try{
            // validate the optional params
            $name = "";
            $slug = "";
            $app_id = null;
            $type = "";
            $default_variation_id = null;
            $control_variation_id = null;
            $order = null;
            $tests = array(    'name' => 'string',
                'slug' => 'string',
                'app_id' => 'int',
                'type' => 'string',
                'default_variation_id' => 'int',
                'control_variation_id' => 'int',
                'order' => 'int');

            // If optParams is completely empty, then we aren't updating anything, just return
            if (empty($optParams)){
                $this->status = 1000;
                $this->results = array();
                $this->msg  = "No params supplied, nothing to update";

                return $this->compileReturn();
            }

            $this->validateOpts($optParams, $tests);

            if (array_key_exists('name', $optParams)) {
                $name = $optParams['name'];
            }

            if (array_key_exists('app_id', $optParams)) {
                $app_id = $optParams['app_id'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('slug', $optParams)) {
                $slug = $optParams['slug'];
            }

            if (array_key_exists('default_variation_id', $optParams)) {
                $default_variation_id = $optParams['default_variation_id'];
            }

            if (array_key_exists('control_variation_id', $optParams)) {
                $control_variation_id = $optParams['control_variation_id'];
            }

            if (array_key_exists('order', $optParams)) {
                $order = $optParams['order'];
            }

            // validate required params
            $tests = array('section_id' => 'int');
            $reqParams = array('section_id' => $section_id);
            $this->validateReqs($reqParams, $tests);

            $params = array(':section_id' => $section_id);

            $sql = "
                UPDATE
                `split_section`
                SET ";
            $use_comma = false;

            if ($name != ""){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `name` = :name ";
                $params[':name'] = $name;
                $use_comma = true;
            }

            if ($slug != ""){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `slug` = :slug ";
                $params[':slug'] = $slug;
                $use_comma = true;
            }

            if ($app_id != null){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `app_id` = :app_id ";
                $params[':app_id'] = $app_id;
                $use_comma = true;
            }

            if ($type != ""){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `type` = :type ";
                $params[':type'] = $type;
                $use_comma = true;
            }

            if ($default_variation_id != null){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `default_variation_id` = :default_variation_id ";
                $params[':default_variation_id'] = $default_variation_id;
                $use_comma = true;
            }

            if ($control_variation_id != null){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `control_variation_id` = :control_variation_id ";
                $params[':control_variation_id'] = $control_variation_id;
                $use_comma = true;
            }

            if ($order != null){

                if ($use_comma) {
                    $sql = $sql . " , ";
                }

                $sql = $sql . " `order` = :order ";
                $params[':order'] = $order;
                $use_comma = true;
            }

            // Finish out the SQL
            $sql = $sql . " WHERE `id` = :section_id ";
            $numRows = $this->conn->execute($sql, $params);

            if (!($numRows > 0)) {
                throw new Exception("No rows were indicated to be updated:");
            }

            // purge default and control caches for this section too
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $this->redis);
            $BarrelCounter->purgeSectionControl($section_id);
            $BarrelCounter->purgeSectionDefault($section_id);

            $this->status = 1000;
            $this->results = array('updated_rows' => $numRows);
            $this->msg = "Successfully updated the context";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error updating contexts: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteContext($section_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('section_id' => $section_id);
            $tests = array('section_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_section`
                WHERE
                `id` = :section_id
                ";
            $params = array(':section_id' => $section_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting context: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    protected function checkSegmentExists($key, $value, $app_id){

        try{
            $sql = "SELECT `id`
                FROM `cg_segment`
                WHERE `key` = :key
                AND `value` = :value
                AND `app_id` = :app_id";
            $params = array(':key' => $key,
                ':value' => $value,
                'app_id' => $app_id);

            $result = $this->conn->fetch($sql, $params);

            if ($result != null && array_key_exists('id', $result) && $result['id'] != 0) {
                return $result['id'];
            }
            else {
                return 0;
            }
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error checking segment exists: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createSegment($key, $value, $app_id, $optParams=array()){

        try{
            // validate the optional parameters
            $super = 0;
            $tests = array('super' => 'bool');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('super', $optParams)) {
                $super = $optParams['super'];
            }

            // validate the required params
            $reqParams = array('key' => $key, 'value' => $value, 'app_id' => $app_id);
            $tests = array('key' => 'string', 'value' => 'string', 'app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // If the segment already exists, just return the segment id
            $seg_id = $this->checkSegmentExists($key, $value, $app_id);

            if ($seg_id != 0){
                $this->status = 1000;
                $this->results = array('segment_id' => $seg_id);
                $this->msg = "Found existing segment that matches app/key/value supplied";

                return $this->compileReturn();
            }

            // Build the SQL insert
            $sql = "INSERT INTO
                `cg_segment`
                (`key`, `value`, `app_id`, `super`)
                VALUES
                (:key, :value, :app_id, :super)
                ";
            $params = array(':key' => $key, 'value' => $value, ':app_id' => $app_id, ':super' => $super);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Segment not created";
            }

            else{
                $this->results = array('segment_id' => $id);
                $this->msg = "Successfully created segment";
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating segment: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listSegments($app_id, $optParams = array()){

        try{
            // Get optional parameters
            // If segment id, key, value, or super are supplied, use segment id first, then key, then value, then super
            $segment_id = null;
            $key = "";
            $value = "";
            $super = null;
            $limit = null;
            $tests = array('segment_id' => 'int', 'key' => 'string', 'value' => 'string', 'super' => 'bool', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('segment_id', $optParams)) {
                $segment_id = $optParams['segment_id'];
            }

            if (array_key_exists('key', $optParams)) {
                $key = $optParams['key'];
            }

            if (array_key_exists('value', $optParams)) {
                $key = $optParams['value'];
            }

            if (array_key_exists('super', $optParams)) {
                $super = $optParams['super'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required parameters
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `id`, `key`, `value`, `app_id`, `super`
                FROM
                `cg_segment`
                WHERE
                `app_id` = :app_id
                ";

            if ($segment_id != null){
                $sql = $sql . " AND `id` = :segment_id";
                $params[':segment_id'] = $segment_id;
            }

            else{

                if ($key != ""){
                    $sql = $sql . " AND `key` = :key";
                    $params[':name'] = $name;
                }

                if ($value != ""){
                    $sql = $sql . " AND `key` = :key";
                    $params[':key'] = $key;
                }

                if ($super != null){
                    $sql = $sql . " AND `super` = :super";
                    $params[':super'] = $super;
                }
            }

            if ($limit != null){
                $sql = $sql . "LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();

        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing contexts: " . $e->getMessage();

            return $this->compileReturn();
        }
    }


    public function deleteSegment($segment_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('segment_id' => $segment_id);
            $tests = array('segment_id' => 'int');
            $this->validateReqs($reqParams, $tests);


            // Build the SQL delete statement
            $sql = "DELETE FROM
                `cg_segment`
                WHERE
                `id` = :segment_id
                ";
            $params = array(':segment_id' => $segment_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }


            $this->status = 1000;
            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting segment: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createMeta($key, $value, $app_id){

        try{
            // Check the required params are valid
            $reqParams = array('key' => $key, 'value' => $value, 'app_id' => $app_id);
            $tests = array('key' => 'string', 'value' => 'string', 'app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert statement
            $sql = "
                INSERT INTO
                `split_meta`(`key`, `value`, `app_id`)
                VALUES
                (:key, :value, :app_id)
                ";
            $params = array(':key' => $key, ':value' => $value, ':app_id' => $app_id);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Meta not created";
            }

            else{
                $this->results = array('meta_id' => $id);
                $this->msg = "Successfully created meta";
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating meta: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listMetas($app_id, $optParams = array()){

        try{
            // Get optional parameters
            $key = "";
            $value = "";
            $meta_id = null;
            $tests = array('meta_id' => 'int', 'key' => 'string', 'value' => 'string');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('key', $optParams)) {
                $key = $optParams['key'];
            }

            if (array_key_exists('value', $optParams)) {
                $value = $optParams['value'];
            }

            if (array_key_exists('meta_id', $optParams)) {
                $meta_id = $optParams['meta_id'];
            }

            // Get required params
            // Only 1 required param to validate
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `id`, `key`, `value`, `app_id`
                FROM
                `split_meta`
                WHERE
                `app_id` = :app_id
                ";

            if ($meta_id != null){
                $sql = $sql . " AND `id` = :meta_id";
                $params[':meta_id'] = $meta_id;
            }

            if ($key != ""){
                $sql = $sql . " AND `key` = :key";
                $params[':key'] = $key;
            }

            if ($value != ""){
                $sql = $sql . " AND `value` = :value";
                $params['value'] = $value;
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();

        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing metas: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteMeta($meta_id){

        try{

            // Only 1 required param to validate
            $reqParams = array('meta_id' => $meta_id);
            $tests = array('meta_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_meta`
                WHERE
                `id` = :meta_id
                ";
            $params = array(':meta_id' => $meta_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();

        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting meta: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createEvent($desc, $slug, $type, $app_id, $optParams = array()){

        try{
            // validate the optional parameters
            $order = 99;
            $tests = array('order' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('order', $optParams)) {
                $order = $optParams['order'];
            }

            // Check the required params are valid
            $reqParams = array('app_id' => $app_id, 'slug' => $slug, 'desc' => $desc, 'type' => $type);
            $tests = array('app_id' => 'int', 'type' => 'string', 'desc' => 'string', 'slug' => 'string');
            $this->validateReqs($reqParams, $tests);

            $last_modified = time();

            // Build the SQL insert statement
            $sql = "
                INSERT INTO
                `cg_event_map`(`app_id`, `type`, `desc`, `slug`, `order`, `last_modified`)
                VALUES
                (:app_id, :type, :desc, :slug, :order, :last_modified)
                ";
            $params = array(':app_id' => $app_id, ':type' => $type, 'desc' => $desc, 'slug' => $slug, 'order' => $order, 'last_modified' => $last_modified);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Event not created";
            }
            else{
                $this->results = array('event_id' => $id);
                $this->msg = "Successfully created event";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating event: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listEvents($app_id, $optParams=array()){

        try{
            // Get optional parameters
            // If we get both event id and type, just search for event id
            $type = "";
            $event_id = null;
            $slug = null;
            $limit = null;
            $tests = array('event_id' => 'int', 'type' => 'string', 'limit' => 'int', 'slug' => 'string');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            if (array_key_exists('slug', $optParams)) {
                $slug = $optParams['slug'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required params
            // Only 1 required param to validate
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `id`, `slug`, `type`, `app_id`, `order`, `desc`, `last_modified`
                FROM
                `cg_event_map`
                WHERE
                `app_id` = :app_id
                ";

            if ($event_id != null){
                $sql = $sql . " AND `id` = :event_id";
                $params['event_id'] = $event_id;
            }

            else if ($slug != ''){
                $sql = $sql . " AND `slug` = :slug";
                $params[':slug'] = $slug;
            }

            else if ($type != ""){
                $sql = $sql . " AND `type` = :type";
                $params[':type'] = $type;
            }
            $sql = $sql . " ORDER BY `order` ASC ";

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }


            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();

        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing events: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function updateEvents($event_id, $optParams=array()){

        try{
            // get optional parameters
            $type = "";
            $slug = "";
            $desc = "";
            $app_id = null;
            $order = null;
            $last_modified = time();

            $tests = array(
                'type' => 'string',
                'slug' => 'string',
                'desc' => 'string',
                'app_id' => 'int',
                'order' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('slug', $optParams)) {
                $slug = $optParams['slug'];
            }

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            if (array_key_exists('app_id', $optParams)) {
                $app_id = $optParams['app_id'];
            }

            if (array_key_exists('order', $optParams)) {
                $order = $optParams['order'];
            }

            // validate the required parameter
            $reqParams = array('id' => $event_id);
            $tests = array('id' => 'int');
            $this->validateReqs($reqParams, $tests);
            $params = array(':id' => $event_id, ':last_modified' => $last_modified);
            $sql = "UPDATE
                `cg_event_map`
                SET
                `last_modified` = :last_modified ";

            if ($type != ""){
                $sql .= " , `type` = :type ";
                $params[':type'] = $type;
            }

            if ($slug != ""){
                $sql .= " , `slug` = :slug ";
                $params[':slug'] = $slug;
            }

            if ($desc != ""){
                $sql .= " , `desc` = :desc ";
                $params[':desc'] = $desc;
            }

            if ($app_id != null){
                $sql .= " , `app_id` = :app_id ";
                $params[':app_id'] = $app_id;
            }

            if ($order != null){
                $sql .= " , `order` = :order";
                $params[':order'] = $order;
            }

            // Finish the stmt
            $sql .= " WHERE `id` = :id";

            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->status = 1920;
                $this->msg = "Number of rows affected in event update differed from expected.";
                $this->results = array('rowsAffected' => $numRows);

                return $this->compileReturn();
            }

            $this->status = 1000;
            $this->msg = "Successfully updated event";
            $this->results = array('rowsAffected' => $numRows);

            return $this->compileReturn();

        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing events: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteEvent($event_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('event_id' => $event_id);
            $tests = array('event_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `cg_event_map`
                WHERE
                `id` = :event_id
                ";
            $params = array(':event_id' => $event_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting event: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createVariation($label, $meta_ids, $section_id, $type, $optParams=array()){

        try{
            // validate the optional parameters
            $desc = '';
            $status = $this->VARLIVE;

            $tests = array('desc' => 'string', 'status' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            // validate the required params
            $reqParams = array('label' => $label, 'meta_ids' => $meta_ids, 'section_id' => $section_id);
            $tests = array('label' => 'string', 'meta_ids' => 'string', 'section_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert
            $sql = "INSERT INTO
                `split_variation`
                (`label`, `meta_ids`, `section_id`, `status`, `type`, `desc`)
                VALUES
                (:label, :meta_ids, :section_id, :status, :type, :desc)
                ";
            $params = array(':label' => $label, 'meta_ids' => $meta_ids, ':section_id' => $section_id, ':status' => $status, ':type' => $type, ':desc' => $desc);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Variation not created";
            }
            else{
                $this->results = array('variation_id' => $id);
                $this->msg = "Successfully created variation";
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating variation: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listVariations($optParams = array()){

        try{
            // Get optional parameters
            // If id, label, status, or super are supplied, use variation id first, then label
            // then status
            $variation_id = null;
            $label = "";
            $status = null;
            $type = "";
            $section_id = null;
            $limit = null;
            $tests = array('variation_id' => 'int', 'label' => 'string', 'status' => 'int', 'limit' => 'int', 'type' => 'string', 'section_id' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('variation_id', $optParams)) {
                $variation_id = $optParams['variation_id'];
            }

            if (array_key_exists('label', $optParams)) {
                $label = $optParams['label'];
            }

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            $params = array();
            $sql ="
                SELECT
                `id`, `label`, `meta_ids`, `section_id`, `status`, `desc`, `type`, `last_updated`
                FROM
                `split_variation`
                WHERE
                1 = 1
                ";

            if ($variation_id != null){
                $sql = $sql . " AND `id` = :variation_id";
                $params[':variation_id'] = $variation_id;
            }

            if ($label != ""){
                $sql = $sql . " AND `label` = :label";
                $params[':label'] = $label;
            }

            if ($status != null){
                $sql = $sql . " AND `status` = :status";
                $params[':status'] = $status;
            }

            if ($type != null){
                $sql = $sql . " AND `type` = :type";
                $params[':type'] = $type;
            }

            if ($section_id != null){
                $sql = $sql . " AND `section_id` = :section_id";
                $params[':section_id'] = $section_id;
            }

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            $superset = array();

            foreach ($this->results as $key => $variation){
                $meta_ids = $variation['meta_ids'];
                $metaData = $this->decodeMeta($meta_ids);
                $variation['meta'] = $metaData;
                $superset[$key] = $variation;
            }

            $this->results = $superset;
            $this->msg = "Successfully grabbed variation and meta data";
            $this->status = 1000;

            return $this->compileReturn();

        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing variations: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteVariation($variation_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('variation_id' => $variation_id);
            $tests = array('variation_id' => 'int');
            $this->validateReqs($reqParams, $tests);
            $list = $this->getAppConForVar($variation_id);
            $app = $list['app_id'];
            $con = $list['section_id'];
            $list = $this->listTestVarAssocForVar($variation_id);
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $con, $this->conn, $this->redis);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_variation`
                WHERE
                `id` = :variation_id
                ";
            $params = array(':variation_id' => $variation_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            // update redis queues
            foreach ($list as $tva){

                if (!$BarrelCounter->removeVariationFromQueue($tva['test_id'], $variation_id)){
                    $tid = $tva['test_id'];
                    $msg = "Error deleting var:$variation_id on test:$tid from redis queue";
                    throw new Exception($msg);
                }
                $BarrelCounter->purgeVariation($variation_id);
            }

            // purge default and control caches for this section too
            $BarrelCounter->purgeSectionControl($con);
            $BarrelCounter->purgeSectionDefault($con);

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting variation: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function updateVariation($variation_id, $optParams=array()){

        try{
            // Get the optional parameters
            $label = "";
            $meta_ids = "";
            $section_id = null;
            $status = null;
            $desc = "";
            $type = "";
            $tests = array('label' => 'string',
                'meta_ids' => 'string',
                'section_id' => 'int',
                'status' => 'int',
                'desc' => 'string',
                'type' => 'string');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('label', $optParams)) {
                $label = $optParams['label'];
            }

            if (array_key_exists('meta_ids', $optParams)) {
                $meta_ids = $optParams['meta_ids'];
            }

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            // Now the required
            $reqParams = array('variation_id' => $variation_id);
            $tests = array('variation_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':variation_id' => $variation_id);
            $time = time();
            $sql = "UPDATE `split_variation` SET `last_updated` = '$time' ";

            if ($label != ""){
                $sql = $sql . ", `label` = :label ";
                $params[':label'] = $label;
            }

            if ($meta_ids != ""){
                $sql = $sql . ", `meta_ids` = :meta_ids ";
                $params[':meta_ids'] = $meta_ids;
            }

            if ($section_id != null){
                $sql = $sql . ", `section_id` = :section_id ";
                $params[':section_id'] = $section_id;
            }

            if ($status != null){
                $sql = $sql . ", `status` = :status ";
                $params[':status'] = $status;
            }

            if ($desc != ""){
                $sql = $sql . ", `desc`= :desc ";
                $params[':desc'] = $desc;
            }

            if ($type != ""){
                $sql = $sql . ", `type`= :type ";
                $params[':type'] = $type;
            }

            $sql = $sql . "WHERE `id` = :variation_id ";

            // Finnish ftw (any f1 fans in the house?)
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Update was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            // Since we can set the status of a variation from here
            // now is an appropriate time to update the redis queue
            if ($status != null){
                $list = $this->getAppConForVar($variation_id);
                $app = $list['app_id'];
                $con = $list['section_id'];
                $list = $this->listTestVarAssocForVar($variation_id);

                $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $con, $this->conn, $this->redis);

                if (!$BarrelCounter->rebuildQueues()) {
                    throw new Exception("Failed to update redis queues");
                }

                $BarrelCounter->purgeVariation($variation_id);
                $BarrelCounter->purgeSectionControl($con);
                $BarrelCounter->purgeSectionDefault($con);
            }

            $this->status = 1000;
            $this->results = array('affectedRows' => $numRows);
            $this->msg = "Successfully updated record";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error updating variation: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // helper function to get test var associaitons from a var id
    protected function listTestVarAssocForVar($variation_id){

        try{
            $sql = "SELECT *
                FROM
                `split_test_variation_assoc`
                WHERE
                `variation_id` = :variation_id";
            $params = array(':variation_id' => $variation_id);
            $list = $this->conn->fetchAll($sql, $params);

            return $list;
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error get test-variation assocs for var:$variation_id: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // helper function to get App and Context from variation
    protected function getAppConForVar($variation_id){

        try{
            $sql = "SELECT
                `sc`.`app_id`, `sv`.`section_id`
                FROM
                `split_variation` as `sv`
                JOIN
                `split_section` as `sc`
                WHERE
                `sv`.`id`=:variation_id
                AND
                `sc`.`id` = `sv`.`section_id`
                ";
            $params = array(':variation_id' => $variation_id);
            $items = $this->conn->fetch($sql, $params);

            if (empty($items) || !array_key_exists('app_id', $items) || !array_key_exists('section_id', $items)){
                $exMsg = "No information available for variation id: " . $test_id;
                throw new Exception($exMsg);
            }

            return $items;
        }

        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error getting app, context ids for test: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createTest($app_id, $section_id, $type, $name, $optParams=array()){

        try{
            // validate the optional parameters
            $control_variation_id = 0;
            $status = 0;
            $desc = "";
            $weight = 1;
            $start_time = 0;
            $end_time = 0;
            $tests = array(    'control_variation_id' => 'int',
                'status' => 'int',
                'desc' => 'string',
                'weight' => 'int',
                'start_time' => 'int',
                'end_time' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('control_variation_id', $optParams)) {
                $control_variation_id = $optParams['control_variation_id'];
            }

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            if (array_key_exists('weight', $optParams)) {
                $weight = $optParams['weight'];
            }

            if (array_key_exists('start_time', $optParams)) {
                $start_time = $optParams['start_time'];
            }

            if (array_key_exists('end_time', $optParams)) {
                $end_time = $optParams['end_time'];
            }

            // validate the required params
            $reqParams = array(    'app_id' => $app_id,
                'name' => $name,
                'section_id' => $section_id,
                'type' => $type);
            $tests = array(    'app_id' => 'int',
                'section_id' => 'int',
                'type' => 'string',
                'name' => 'string');
            $this->validateReqs($reqParams, $tests);

            $last_modified = time();

            // Build the SQL insert
            $sql = "INSERT INTO
                `split_test`
                (`app_id`, `control_variation_id`, `section_id`, `type`, `name`, `desc`, `weight`, `status`, `last_modified`, `start_time`, `end_time`)
                VALUES
                (:app_id, :control_variation_id, :section_id, :type, :name, :desc, :weight, :status, :last_modified, :start_time, :end_time)
                ";
            $params = array(':app_id' => $app_id,
                ':control_variation_id' => $control_variation_id,
                ':section_id' => $section_id,
                ':type' => $type,
                ':name' => $name,
                ':desc' => $desc,
                ':weight' => $weight,
                ':status' => $status,
                ':last_modified' => $last_modified,
                ':start_time' => $start_time,
                ':end_time' => $end_time);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Test not created";
            }
            else{
                $this->results = array('test_id' => $id);
                $this->msg = "Successfully created test";
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating test: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listAllTestsAndEvents($status = false){

        try{
            $sql = "SELECT
                `st`.`id` as `test_id`, `st`.`control_variation_id`,
                `sg`.`event_id`
                FROM
                `split_test` as `st`
                JOIN
                `split_goal` as `sg`
                ON
                `st`.`id` = `sg`.`test_id`
                ";

            if ($status){
                $sql .= " WHERE `st`.`status` = $status";
            }

            $results = $this->conn->fetchAll($sql);

            if (empty($results)) {
                throw new Exception("There were no goals -ie, no test event associations-");
            }

            $retArr = array();
            $testIds = array();

            foreach ($results as $key => $goalRow){

                if (array_key_exists($goalRow['test_id'], $testIds) ){
                    $testIds[$goalRow['test_id']]['goals'][] = $goalRow['event_id'];
                }
                else{
                    $testIds[$goalRow['test_id']] = array(
                        'control' => $goalRow['control_variation_id'],
                        'goals' => array($goalRow['event_id'])
                    );
                }
            }

            foreach ($testIds as $id => $test){
                $retArr[] = array(
                    'testId' => $id,
                    'control' => $test['control'],
                    'goals' => $test['goals']
                );
            }

            $this->results = $retArr;
            $this->msg = "Successfully fetched all test and event data";
            $this->status = 1000;

            return $this->compileReturn();

        }
        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error fetching tests and goals: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listTests($app_id, $optParams = array()){

        try{
            // Get optional parameters
            // If multiple opt params, use in the following order testid/segid/type/status
            $test_id = null;
            $section_id = null;
            $type = "";
            $status = null;
            $limit = null;
            $getSegments = false;
            $tests = array('test_id' => 'int',
                'type' => 'string',
                'status' => 'int',
                'section_id' => 'int',
                'limit' => 'int',
                'getSegments' => 'bool');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('test_id', $optParams)) {
                $test_id = $optParams['test_id'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            if (array_key_exists('getSegments', $optParams)) {
                $getSegments = $optParams['getSegments'];
            }

            // Get required parameters
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `id`,     `name`, `app_id`,
                `control_variation_id`, `section_id`, `type`,
                `status`, `desc`, `weight`, `status`,
                `last_modified`, `start_time`, `end_time`
                FROM
                `split_test`
                WHERE
                `app_id` = :app_id
                ";

            if ($test_id != null){
                $sql = $sql . " AND `id` = :test_id ";
                $params[':test_id'] = $test_id;
            }

            if ($type != null){
                $sql = $sql . " AND `type` = :type ";
                $params[':type'] = $type;
            }

            if ($status != null){
                $sql = $sql . " AND `status` = :status ";
                $params[':status'] = $status;
            }

            if ($section_id != null){
                $sql = $sql . " AND `section_id` = :section_id ";
                $params[':section_id'] = $section_id;
            }

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $testResults = $this->conn->fetchAll($sql, $params);

            if (empty($testResults)){
                $this->results = array();
                $this->msg = "Result set for test criteria was empty";
                $this->status = 1000;

                return $this->compileReturn();
            }

            $this->results = $testResults;

            $this->status = 1000;
            $this->msg = "Successfully returned tests and segment data";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing tests: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    protected function getSegmentsForTest($test_id){

        try{
            $sql = "SELECT
                `stsa`.`segment_id`, `stsa`.`exclusive`, `stsa`.`exclude`,
                `cgs`.`key`, `cgs`.`value`, `cgs`.`super`, `cgs`.`app_id`
                FROM
                `split_test_segment_assoc` as `stsa`
                JOIN
                `cg_segment` as `cgs`
                ON
                `stsa`.`test_id` = :test_id AND
                `cgs`.`id` = stsa.`segment_id`
                ";
            $params = array(':test_id' => $test_id);
            $items = $this->conn->fetchAll($sql, $params);

            return $items;
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing tests: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // helper function to get app, context, segment ids for a given test
    protected function getAppConForTest($test_id){

        try{
            $sql = "SELECT
                `app_id`, `section_id`
                FROM
                `split_test`
                WHERE
                `id`=:test_id
                ";
            $params = array(':test_id' => $test_id);
            $items = $this->conn->fetch($sql, $params);

            if (empty($items) || !array_key_exists('app_id', $items) || !array_key_exists('section_id', $items)){
                $exMsg = "No information available for test id: " . $test_id;
                throw new Exception($exMsg);
            }

            return $items;
        }

        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error getting app, context ids for test: " . $e->getMessage();

            return $this->compileReturn();
        }

    }

    public function deleteTest($test_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('test_id' => $test_id);
            $tests = array('test_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // We need the app, context, and segment of the test before we delete it so we can get our redis test round robin key
            $items = $this->getAppConForTest($test_id);
            $app_id = $items['app_id'];
            $section_id = $items['section_id'];
            $weight = $this->getWeightFromTestId($test_id);

            // we'll fix this when we update getTest
            $segment_id = 1;

            // Build the SQL delete statement to remove from DB
            $sql = "DELETE FROM
                `split_test`
                WHERE
                `id` = :test_id
                ";
            $params = array(':test_id' => $test_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }
            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $this->redis);

            if (!$BarrelCounter->removeTestFromQueue($test_id)) {
                $msg = "Error removing test:$test_id from redis queue.";
                throw new Exception($msg);
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }
            $this->results = array();
            $this->msg = "Error deleting test: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function updateTest($test_id, $optParams=array()){

        try{
            // Sift the optional parameters
            $app_id = null;
            $control_variation_id = null;
            $section_id = null;
            $type = "";
            $name = "";
            $desc = "";
            $weight = null;
            $status = null;
            $start_time = null;
            $end_time = null;
            $last_modified = time();

            $tests = array('app_id' => 'int',
                'control_variation_id' => 'int',
                'section_id' => 'int',
                'type' => 'string',
                'name' => 'string',
                'desc' => 'string',
                'weight' => 'int',
                'status' => 'int',
                'start_time' => 'int',
                'end_time' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('app_id', $optParams)) {
                $app_id = $optParams['app_id'];
            }

            if (array_key_exists('control_variation_id', $optParams)) {
                $control_variation_id = $optParams['control_variation_id'];
            }

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('type', $optParams)) {
                $type = $optParams['type'];
            }

            if (array_key_exists('name', $optParams)) {
                $name = $optParams['name'];
            }

            if (array_key_exists('desc', $optParams)) {
                $desc = $optParams['desc'];
            }

            if (array_key_exists('weight', $optParams)) {
                $weight = $optParams['weight'];
            }

            if (array_key_exists('status', $optParams)) {
                $status = $optParams['status'];
            }

            if (array_key_exists('start_time', $optParams)) {
                $start_time = $optParams['start_time'];
            }

            if (array_key_exists('end_time', $optParams)) {
                $end_time = $optParams['end_time'];
            }

            // Check required parameters
            $reqParams = array('test_id' => $test_id);
            $tests = array('test_id' => 'int');
            $this->validateReqs($reqParams, $tests);


            // If we're updating app, context, or weight, we'll need to also update redis
            // To update redis, get the app, context info for the test
            $items = $this->getAppConForTest($test_id);
            $orig_app_id = $items['app_id'];
            $orig_section_id = $items['section_id'];
            $params = array(':test_id' => $test_id, ':last_modified' => $last_modified);
            $sql = "
                UPDATE
                `split_test`
                SET
                `last_modified` = :last_modified
                ";

            if ($app_id != null){
                $sql = $sql . " , `app_id` = :app_id";
                $params[':app_id'] = $app_id;
            }

            if ($control_variation_id != null){
                $sql = $sql . " , `control_variation_id` = :control_variation_id";
                $params[':control_variation_id'] = $control_variation_id;
            }

            if ($section_id != null){
                $sql = $sql . " , `section_id` = :section_id";
                $params[':section_id'] = $section_id;
            }

            if ($type != ""){
                $sql = $sql . " , `type` = :type";
                $params[':type'] = $type;
            }

            if ($desc != ""){
                $sql = $sql . " , `desc` = :desc";
                $params[':desc'] = $desc;
            }

            if ($name != ""){
                $sql = $sql . " , `name` = :name";
                $params[':name'] = $name;
            }

            if ($weight != null){
                $sql = $sql . " , `weight` = :weight";
                $params['weight'] = $weight;
            }

            if ($status != null){
                $sql = $sql . " , `status` = :status";
                $params[':status'] = $status;
            }

            if ($start_time != null){
                $sql = $sql . " , `start_time` = :start_time";
                $params[':start_time'] = $start_time;
            }

            if ($end_time != null){
                $sql = $sql . " , `end_time` = :end_time";
                $params[':end_time'] = $end_time;
            }

            // Finish out the SQL
            $sql = $sql . " WHERE `id` = :test_id ";
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Update was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }
            $this->results = array('affectedRows' => $numRows);
            $this->msg = "Successfully updated record";

            // Now keep redis up to date
            // If the status was changed to no longer be active, then simply delete the test from the round robin
            // otherwise we can just update accordingly

            // create function to get segments from test seg assoc table
            // update seg
            $list = $this->getAppConForTest($test_id);
            $app = $list['app_id'];
            $con = $list['section_id'];
            $testSegData = $this->getSegmentsForTest($test_id);
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $con, $this->conn, $this->redis);

            if (!$BarrelCounter->rebuildQueues()) {
                throw new Exception("Failed to update redis queues");
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error updating test: " . $e->getMessage();

            return $this->compileReturn();
        }

    }


    public function createGoal($event_id, $test_id, $optParams = array()){

        try{
            // validate the optional parameters
            $primary_goal = 0;
            $limit = null;
            $tests = array('primary_goal' => 'bool', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('primary_goal', $optParams)) {
                $primary_goal = $optParams['primary_goal'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Check the required params are valid
            $reqParams = array('event_id' => $event_id, 'test_id' => $test_id);
            $tests = array('event_id' => 'int', 'test_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert statement
            $sql = "
                INSERT INTO
                `split_goal`(`event_id`, `test_id`, `primary_goal`)
                VALUES
                (:event_id, :test_id, :primary_goal)
                ";
            $params = array(':event_id' => $event_id, ':test_id' => $test_id, 'primary_goal' => $primary_goal);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Goal not created";
            }
            else{
                $this->results = array('goal_id' => $id);
                $this->msg = "Successfully created goal";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating goal: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listGoals($test_id, $optParams = array()){

        try{
            // Get optional parameters
            $goal_id = null;
            $event_id = null;
            $limit = null;
            $tests = array('goal_id' => $goal_id, 'event_id' => 'int', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            if (array_key_exists('goal_id', $optParams)) {
                $goal_id = $optParams['goal_id'];
            }

            // Get required params
            // Only 1 required param to validate
            $reqParams = array('test_id' => $test_id);
            $tests = array('test_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':test_id' => $test_id);
            $sql ="
                SELECT
                `sg`.`id`, `sg`.`event_id`, `sg`.`test_id`, `sg`.`primary_goal`
                FROM
                `split_goal` as `sg`
                JOIN `cg_event_map` as `cem` ON(`cem`.`id` = `sg`.`event_id`)
                WHERE
                `sg`.`test_id` = :test_id
                ";

            if ($goal_id != null){
                $sql = $sql . " AND `sg`.`id` = :goal_id";
                $params['goal_id'] = $goal_id;
            }
            else if ($event_id != null){
                $sql = $sql . " AND `sg`.`event_id` = :event_id";
                $params['event_id'] = $event_id;
            }
            $sql = $sql . " ORDER BY `cem`.`order` ASC ";

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();
        }
        catch (Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing goals: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteGoal($goal_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('goal_id' => $goal_id);
            $tests = array('goal_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_goal`
                WHERE
                `id` = :goal_id
                ";
            $params = array(':goal_id' => $goal_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting goal: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function updateGoal($goal_id, $optParams=array()){

        try{
            // Sift the optional parameters
            $primary_goal = 0;


            $tests = array('primary_goal' => 'bool');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('primary_goal', $optParams)) {
                $primary_goal = $optParams['primary_goal'];
            }

            // Check required parameters
            $reqParams = array('goal_id' => $goal_id);
            $tests = array('goal_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':goal_id' => $goal_id, ':primary_goal' => $primary_goal);
            $sql = "
                UPDATE
                `split_goal`
                SET
                `primary_goal` = :primary_goal
                WHERE
                `id` = :goal_id
                ";

            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Update was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('affectedRows' => $numRows);
            $this->msg = "Successfully updated record";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error updating goals: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createTestVariationAssoc($test_id, $variation_id, $optParams=array()){

        try{
            // validate the optional parameters
            $weight = 1;
            $active = 0;
            $tests = array('weight' => 'int', 'active' => 'bool');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('weight', $optParams)) {
                $weight = $optParams['weight'];
            }

            if (array_key_exists('active', $optParams)) {
                $active = $optParams['active'];
            }

            // validate the required params
            $reqParams = array('test_id' => $test_id, 'variation_id' => $variation_id);
            $tests = array('test_id' => 'int', 'variation_id' => 'int');
            $this->validateReqs($reqParams, $tests);


            // Build the SQL insert
            $sql = "INSERT INTO
                `split_test_variation_assoc`
                (`test_id`, `variation_id`, `weight`, `active`)
                VALUES
                (:test_id, :variation_id, :weight, :active)
                ";
            $params = array(':test_id' => $test_id, 'variation_id' => $variation_id, ':weight' => $weight, ':active' => $active);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "Test variation assoc not created";
            }

            else{
                $this->results = array('test_variation_assoc_id' => $id);
                $this->msg = "Successfully created Test Variation Assoc";
            }

            // If the associaiton is active
            // we need to add it to redis so it can be round robin'd
            if ($active){
                $list = $this->getAppConForTest($test_id);
                $app_id = $list['app_id'];
                $section_id = $list['section_id'];
                $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $this->redis);

                if (!$BarrelCounter->addVariationToQueue($test_id, $variation_id)){
                    $msg = "Error adding variation: $variation_id to test: $test_id.";
                    throw new Exception($msg);
                }
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating Test Variation Assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listTestVariationAssocs($test_id, $optParams = array()){

        try{
            // Get optional parameters
            // If id, label, status, or super are supplied, use TestVariationAssoc id first, then label
            // then status
            $test_variation_assoc_id = null;
            $variation_id = null;
            $limit = null;
            $tests = array('test_variation_assoc_id' => 'int', 'variation_id' => 'int', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('test_variation_assoc_id', $optParams)) {
                $test_variation_assoc_id = $optParams['test_variation_assoc_id'];
            }

            if (array_key_exists('variation_id', $optParams)) {
                $variation_id = $optParams['variation_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required parameters
            $reqParams = array('test_id' => $test_id);
            $tests = array('test_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':test_id' => $test_id);
            $sql ="
                SELECT
                `stva`.`id`, `stva`.`test_id`,
                `stva`.`variation_id`, `stva`.`weight`, `stva`.`active`,
                `sv`.`label`, `sv`.`meta_ids`, `sv`.`section_id`, `sv`.`last_updated`,
                `sv`.`status` as `variation_status`, `sv`.`desc`, `sv`.`type`
                FROM
                `split_test_variation_assoc` as `stva`
                JOIN
                `split_variation` as `sv`
                ON
                `stva`.`test_id` = :test_id
                AND
                `stva`.`variation_id` = `sv`.`id`
                ";

            if ($test_variation_assoc_id != null){
                $sql = $sql . " AND `stva`.`id` = :test_variation_assoc_id";
                $params[':test_variation_assoc_id'] = $test_variation_assoc_id;
            }
            else if ($variation_id != null){
                $sql = $sql . " AND `stva`.`variation_id` = :variation_id";
                $params[':variation_id'] = $variation_id;
            }

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            $superset = array();

            foreach ($this->results as $key => $variation){
                $meta_ids = $variation['meta_ids'];
                $metaData = $this->decodeMeta($meta_ids);
                $variation['meta'] = $metaData;
                $superset[$key] = $variation;
            }

            $this->status = 1000;
            $this->msg = "Successfully returned list of tvas";
            $this->results = $superset;

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing test variation assocs: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function updateTestVariationAssoc($testVar_id, $optParams=array()){

        try{

            // get optional parameters
            $active = null;
            $weight = null;
            $tests = array('active' => 'int', 'weight' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('active', $optParams)) {
                $active = $optParams['active'];
            }

            if (array_key_exists('weight', $optParams)) {
                $weight = $optParams['weight'];
            }

            if ($weight == null && $active == null){
                // nothing is going to be updated, just skip it
                $this->status = 1000;
                $this->msg = "No parameters supplied to update row";
                $this->results = array();

                return $this->compileReturn();
            }

            // get required
            $reqParams = array('id' => $testVar_id);
            $tests = array('id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':id' => $testVar_id);
            $sql = "UPDATE
                `split_test_variation_assoc`
                SET
                ";
            $use_comma = false;

            if ($weight != null){

                if ($use_comma) {
                    $sql .= " , ";
                }

                $sql .= " `weight` = :weight ";
                $params[':weight'] = $weight;
                $use_comma = true;
            }

            if ($active != null){

                if ($use_comma) {
                    $sql .= " , ";
                }

                $sql .= " `active` = :active";
                $params[':active'] = $active;
                $use_comma = true;
            }

            // Finish the sql
            $sql .= " WHERE `id` = :id ";

            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->status = 1920;
                $this->results = array('rowAffected' => $numRows);
                $this->msg = "Number of rows returned from update differs from expected";

                return $this->compileReturn();
            }

            // redis queue
            $list = $this->getTestVarFromAssocId($testVar_id);
            $test_id = $list['test_id'];
            $variation_id = $list['variation_id'];
            $list = $this->getAppConForTest($test_id);
            $app = $list['app_id'];
            $con = $list['section_id'];
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $con, $this->conn, $this->redis);

            if (!$BarrelCounter->rebuildQueues()){
                $msg = "Error rebuilding queues after testvar: $testVar_id, test:$test_id, var:$variation_id.";
                throw new Exception($msg);
            }

            // delete the cached variations
            $BarrelCounter->purgeVariation($variation_id);
            $BarrelCounter->purgeSectionControl($con);
            $BarrelCounter->purgeSectionDefault($con);


            $this->status = 1000;
            $this->results = array('rowsAffected' => $numRows);
            $this->msg = "Successfully updated the test variation association";

            return $this->compileReturn();

        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error updating test variation assocs: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    // helper function to get the test and var id's from a test var ass'n id
    protected function getTestVarFromAssocId($testVar_id){

        try{
            $sql = "SELECT
                `test_id`, `variation_id`
                FROM
                `split_test_variation_assoc`
                WHERE
                `id` = :testVar_id
                ";
            $params = array(':testVar_id' => $testVar_id);
            $items = $this->conn->fetch($sql, $params);

            if (empty($items) || !array_key_exists('test_id', $items) || !array_key_exists('variation_id', $items) ){
                $exMsg = "No info found for test variation assoc id: " . $testVar_id;
                throw new Exception($exMsg);
            }

            return $items;
        }

        catch(Exception $e){
            $this->status = 1200;
            $this->results = array();
            $this->msg = "Error getting test variation id from assocs: " . $e->getMessage();

            return $this->compileReturn();
        }


    }

    public function deleteTestVariationAssoc($test_variation_assoc_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('test_variation_assoc_id' => $test_variation_assoc_id);
            $tests = array('test_variation_assoc_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // If we're deleting the variation, we want to keep redis in the loop as well, fetch the test and var id before deleting
            $items = $this->getTestVarFromAssocId($test_variation_assoc_id);
            $test_id = $items['test_id'];
            $variation_id = $items['variation_id'];

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_test_variation_assoc`
                WHERE
                `id` = :test_variation_assoc_id
                ";
            $params = array(':test_variation_assoc_id' => $test_variation_assoc_id);

            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }

            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            // newredis queue
            $list = $this->getAppConForTest($test_id);
            $app = $list['app_id'];
            $con = $list['section_id'];
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app, $con, $this->conn, $this->redis);

            if (!$BarrelCounter->removeVariationFromQueue($test_id, $variation_id)){
                $msg = "Error deleting testvar: $testVar_id, test:$test_id, var:$variation_id.";
                throw new Exception($msg);
            }

            // delete the cached variations
            $BarrelCounter->purgeVariation($variation_id);
            $BarrelCounter->purgeSectionControl($con);
            $BarrelCounter->purgeSectionDefault($con);

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting test variation assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function createContextEventAssoc($section_id, $event_id){

        try{
            // validate the required params
            $reqParams = array('section_id' => $section_id, 'event_id' => $event_id);
            $tests = array('section_id' => 'int', 'event_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert
            $sql = "INSERT INTO
                `split_section_event_assoc`
                (`section_id`, `event_id`)
                VALUES
                (:section_id, :event_id)
                ";
            $params = array(':section_id' => $section_id, ':event_id' => $event_id);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->status = 1930;
                $this->msg = "section event assoc not created";
            }

            else{
                $this->results = array('section_event_assoc_id' => $id);
                $this->msg = "Successfully created section event assoc";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating section event Assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listContextEventAssocs($section_id, $optParams = array()){

        try{
            // Get optional parameters
            $section_event_assoc_id = null;
            $event_id = null;
            $limit = null;
            $tests = array('section_event_assoc_id' => 'int', 'event_id' => 'int', 'limit' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('section_event_assoc_id', $optParams)) {
                $section_event_assoc_id = $optParams['section_event_assoc_id'];
            }

            if (array_key_exists('event_id', $optParams)) {
                $event_id = $optParams['event_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required parameters
            $reqParams = array('section_id' => $section_id);
            $tests = array('section_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':section_id' => $section_id);
            $sql ="
                SELECT
                `id`, `section_id`, `event_id`
                FROM
                `split_section_event_assoc`
                WHERE
                `section_id` = :section_id
                ";

            if ($section_event_assoc_id != null){
                $sql = $sql . " AND `id` = :section_event_assoc_id";
                $params[':section_event_assoc_id'] = $section_event_assoc_id;
            }
            else if ($event_id != null){
                $sql = $sql . " AND `event_id` = :event_id";
                $params[':event_id'] = $event_id;
            }

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();

        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing context event assocs: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteContextEventAssoc($section_event_assoc_id){

        try{
            // Only 1 required param to validate
            $reqParams = array('section_event_assoc_id' => $section_event_assoc_id);
            $tests = array('section_event_assoc_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_section_event_assoc`
                WHERE
                `id` = :section_event_assoc_id
                ";
            $params = array(':section_event_assoc_id' => $section_event_assoc_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows != 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }
            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting context event assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    protected function isTestAssociatedWithAllSeg($test_id){
        $sql = "SELECT count(*) FROM
            `split_test_segment_assoc` as `stsa`
            JOIN
            `cg_segment` as `cs`
            ON
            `cs`.`id` = `stsa`.`segment_id` AND
            `cs`.`key` = 'all' AND
            `cs`.`value` = 'all' AND
            `stsa`.`test_id` = :test_id
            ";
        $params = array(':test_id' => $test_id);
        $result = $this->conn->fetch($sql, $params);

        if ($result['count(*)'] < 1) {
            return false;
        }
        else {
            return true;
        }
    }

    public function createTestSegmentAssoc($test_id, $segment_id, $optParams=array()){

        try{
            // validate the optional parameters
            $exclusive = 0;
            $exclude = 0;
            $tests = array('exclusive' => 'bool', 'exclude' => 'bool');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('exclusive', $optParams)) {
                $exclusive = $optParams['exclusive'];
            }

            if (array_key_exists('exclude', $optParams)) {
               $exclude = $optParams['exclude'];
            }

            $exclusive +=0;
            $exclude +=0;

            // validate the required params
            $reqParams = array('test_id' => $test_id, 'segment_id' => $segment_id);
            $tests = array('test_id' => 'int', 'segment_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // Build the SQL insert
            $sql = "INSERT INTO
                `split_test_segment_assoc`
                (`test_id`, `segment_id`, `exclusive`, `exclude`)
                VALUES
                (:test_id, :segment_id, :exclusive, :exclude)
                ";
            $params = array(':test_id' => $test_id, 'segment_id' => $segment_id, ':exclusive' => $exclusive, ':exclude' => $exclude);
            $results = $this->conn->execute($sql, $params);

            // Process the results from the SQL operation
            $id = $this->conn->lastInsert();

            if (!($id > 0)){
                $this->results = array();
                $this->status = 1930;
                $this->msg = "Test segment assoc not created";

                $this->compileReturn();
            }

            // Get app, weight, and context
            $item = $this->getAppAndContextFromTestId($test_id);
            $app_id = $item['app_id'];
            $section_id = $item['section_id'];
            $app_id +=0;
            $section_id +=0;
            $weight = $this->getWeightFromTestId($test_id);

            // newredis queues
            $barrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $this->redis);

            if (!$barrelCounter->addTestToQueue($test_id)){
                $msg = "Adding test: $test_id to redis queue failed";
                throw new Exception($msg);
            }

            $this->status = 1000;
            $this->results = array('test_segment_assoc_id' => $id);
            $this->msg = "Successfully created Test Segment Assoc";

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error creating Test Segment Assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function listTestSegmentAssocs($app_id, $optParams = array()){

        try{
            // Get optional parameters
            // If id, label, status, or super are supplied, use TestSegmentAssoc id first, then label
            // then status
            $test_segment_assoc_id = null;
            $test_id = null;
            $segment_id = null;
            $section_id = null;
            $limit = null;
            $tests = array('test_segment_assoc_id' => 'int', 'segment_id' => 'int', 'limit' => 'int', 'test_id' => 'int');
            $this->validateOpts($optParams, $tests);

            if (array_key_exists('test_segment_assoc_id', $optParams)) {
                $test_segment_assoc_id = $optParams['test_segment_assoc_id'];
            }

            if (array_key_exists('segment_id', $optParams)) {
                $segment_id = $optParams['segment_id'];
            }

            if (array_key_exists('test_id', $optParams)) {
                $test_id = $optParams['test_id'];
            }

            if (array_key_exists('section_id', $optParams)) {
                $section_id = $optParams['section_id'];
            }

            if (array_key_exists('limit', $optParams)) {
                $limit = $optParams['limit'];
            }

            // Get required parameters
            $reqParams = array('app_id' => $app_id);
            $tests = array('app_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            $params = array(':app_id' => $app_id);
            $sql ="
                SELECT
                `stsa`.`id`, `stsa`.`test_id`, `stsa`.`segment_id`, `stsa`.`exclusive`, `stsa`.`exclude`
                FROM
                `split_test_segment_assoc` as `stsa`
                JOIN
                `split_test` as `st` ON(`st`.`id` = `stsa`.`test_id`)
                WHERE
                `st`.`app_id` = :app_id
                AND `st`.`status` = 3
                ";

            if ($test_segment_assoc_id != null){
                $sql = $sql . " AND `stsa`.`id` = :test_segment_assoc_id";
                $params[':test_segment_assoc_id'] = $test_segment_assoc_id;
            }

            if ($segment_id != null){
                $sql = $sql . " AND `stsa`.`segment_id` = :segment_id";
                $params[':segment_id'] = $segment_id;
            }

            if ($test_id != null){
                $sql  = $sql . " AND `stsa`.`test_id` = :test_id";
                $params[':test_id'] = $test_id;
            }

            if ($section_id != null){
                $sql  = $sql . " AND `st`.`section_id` = :section_id";
                $params[':section_id'] = $section_id;
            }

            $sql = $sql . " GROUP BY `stsa`.`id` ";

            if ($limit != null){
                $sql = $sql . " LIMIT $limit";
            }

            $this->status = 1000;
            $this->results = $this->conn->fetchAll($sql, $params);

            if (empty($this->results)) {
                $this->msg = "The result set was empty";
            }
            else {
                $this->msg = "Successfully fetched results";
            }

            return $this->compileReturn();
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error listing test segment assocs: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    protected function getTestDataForSegAssoc($test_segment_id){

        try{
            $sql  = "SELECT
                `stsa`.`test_id`,
                `stsa`.`segment_id`,
                `st`.`app_id`,
                `st`.`section_id`
                FROM
                `split_test_segment_assoc` as `stsa`
                JOIN
                `split_test` as `st`
                ON
                `stsa`.`id` = :test_segment_id AND
                `stsa`.`test_id` = `st`.`id`
                ";
            $params = array(':test_segment_id' => $test_segment_id);
            $result = $this->conn->fetch($sql, $params);

            if (empty($result)) {
                throw new Exception('No app test segment relationship found');
            }

            return $result;
        }
        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error getting app id for test segment assocs: " . $e->getMessage();

            return $this->compileReturn();
        }
    }

    public function deleteTestSegmentAssoc($test_segment_assoc_id){

        try{
            // debug_array($test_segment_assoc_id, "id");
            // Only 1 required param to validate
            $reqParams = array('test_segment_assoc_id' => $test_segment_assoc_id);
            $tests = array('test_segment_assoc_id' => 'int');
            $this->validateReqs($reqParams, $tests);

            // first list the test and segment id for the association
            $list= $this->getTestDataForSegAssoc($test_segment_assoc_id);

            // debug_array($list, "list");
            $app_id = $list['app_id'];
            $app_id +=0;
            $test_id = $list['test_id'];
            $test_id +=0;
            $segment_id = $list['segment_id'];
            $segment_id +=0;
            $section_id = $list['section_id'];
            $section_id +=0;


            // Build the SQL delete statement
            $sql = "DELETE FROM
                `split_test_segment_assoc`
                WHERE
                `id` = :test_segment_assoc_id
                ";
            $params = array(':test_segment_assoc_id' => $test_segment_assoc_id);
            $numRows = $this->conn->execute($sql, $params);

            if ($numRows < 1){
                $this->msg = "Delete was unsuccessful, " . $numRows . " returned for rows affected";
                $this->status = 1930;
                throw new Exception($this->msg);
            }
            $this->results = array('deletedRows' => $numRows);
            $this->msg = "Successfully deleted record";

            // Check to see if there are any other non-exclusive test-seg-assocs for the test id
            $optParams = array('exclusive' => 0, 'test_id' => $test_id);
            $tsaList = $this->listTestSegmentAssocs($app_id, $optParams);

            // newredis queue delete
            $BarrelCounter = new Icm_SplitTest_BarrelCounter($app_id, $section_id, $this->conn, $this->redis);

            if (!$BarrelCounter->removeTestFromQueue($test_id)){
                $msg = "Could not remove test:$test_id from redis";
                throw new Exception($msg);
            }

            return $this->compileReturn();
        }

        catch(Exception $e){

            if ($this->status == 1000) {
                $this->status = 1200;
            }

            $this->results = array();
            $this->msg = "Error deleting test segment assoc: " . $e->getMessage();

            return $this->compileReturn();
        }
    }
}
