<?php

class Icm_Visit_Pixel
{
    protected $redis;
    protected $postbackQueue;
    protected $marketingConfig;
    protected $visitTracker;
    protected $tokenReplacements;
    protected $orderId;

    public function __construct(Icm_Visit_Tracker $visitTracker,
                                Icm_Redis $redis,
                                Icm_Config $marketingConfig,
                                Icm_QueueFactory $queueFactory)
    {
        $this->visitTracker = $visitTracker;
        $this->redis = $redis;
        $this->postbackQueue = $queueFactory->getPostbackQueue();
        $this->marketingConfig = $marketingConfig;
    }

    /**
     * Method for fetching and processing the pixels
     *
     * @param int $sectionId
     * @param bool $riskMitigationEnabled
     * @return array
     */
    public function getPixels($sectionId, $riskMitigationEnabled = true) {
        $pixels = array();

        if (intval($sectionId) < 1) {
            throw new Exception("Invalid sectionId");
        }

        // get the visit data
        $visitData = $this->visitTracker->getVisitData();

        // make sure we have at least a campaign before proceeding
        if (!isset($visitData['campaign_id']) || intval($visitData['campaign_id']) < 1) {
            return $pixels;
        }

        // fetch the pixels from redis
        $redisResults = $this->fetchRedisPixels($sectionId, $visitData);

        // process the data from redis
        $pixels = $this->processRedisResults($sectionId, $riskMitigationEnabled, $visitData, $redisResults);

        return $pixels;
    }

    public function setOrderId($orderId) {
        return $this->orderId = $orderId;
    }

    /**
     * Pull pixels for the current visit from redis
     *
     * @param int $sectionId
     * @param array $visitData
     * @return array
     */
    protected function fetchRedisPixels($sectionId, $visitData) {
        // setup defaults
        $redisPixels = array();
        $campaignId = $visitData['campaign_id'];
        $affiliateId = isset($visitData['affiliate_id']) ? $visitData['affiliate_id'] : null;
        $subId = isset($visitData['sub_id']) ? $visitData['sub_id'] : null;

        // select the pixel db
        $pixelDatabase = $this->marketingConfig->getOption('redisDatabase');
        $this->redis->select($pixelDatabase);

        // fetch the campaign pixel
        $campaignPixel = $this->redis->get($this->marketingConfig->getOption('campaignPrefix') . $campaignId);
        $redisPixels['campaign_pixel'] = $campaignPixel;

        // fetch the affiliate pixel
        if ($affiliateId > 0) {
            $affiliatePixel = $this->redis->get($this->marketingConfig->getOption('affiliatePrefix') . $affiliateId);
            $redisPixels['affiliate_pixel'] = $affiliatePixel;
        }

        // set up the lookup key
        $lookupKey = $this->marketingConfig->getOption('pixelPrefix') . "{$campaignId}_{$sectionId}";

        // fetch all the possible pixel keys
        $keys = $this->redis->keys($lookupKey . '*');

        // filter out the unassociated keys
        $filteredKeys = $this->filterPixelKeys($keys, $lookupKey, $affiliateId, $subId);

        // fetch all the pixels for the given keys
        $pipe = $this->redis->pipeline();
        $pipe->select($pixelDatabase);

        foreach ($filteredKeys as $key) {
            $pipe->get($key);
        }

        $results = $pipe->execute();

        // keep everything but the first value (from the db select)
        $pixels = array_slice($results, 1);

        // reset the redis db
        $this->redis->select(Icm_Redis::DEFAULT_DATABASE);

        // return all the pixels
        $redisPixels['normal_pixels'] = $pixels;
        return $redisPixels;
    }

    /**
     * Filter out all the possible pixel keys that don't belong to this affiliate
     * or aren't global pixels
     *
     * @param array $keys
     * @param string $lookupKey
     * @param int $affiliateId
     * @param int $subId
     * @return array
     */
    protected function filterPixelKeys($keys, $lookupKey, $affiliateId, $subId) {
        // filter the keys via regex
        // final regex will look like: /pixel-1_1(_45(_2)?)?_pxid-\.*/
        //                          or /pixel-1_1_pxid-\.*/
        // depending on presence of affiliateId
        $pixelIdPrefix = $this->marketingConfig->getOption('pixelIdPrefix');
        $regex = $affiliateId > 0
            ? "/{$lookupKey}(_{$affiliateId}(_{$subId})?)?_{$pixelIdPrefix}\.*/"
            : "/{$lookupKey}_{$pixelIdPrefix}\.*/";
        $filteredKeys = preg_grep($regex, $keys);

        return $filteredKeys;
    }

    /**
     * Process the results we got back from redis into pixels
     *
     * @param int $sectionId
     * @param bool $riskMitigationEnabled
     * @param array $visitData
     * @param array $redisResults
     * @return array
     */
    protected function processRedisResults($sectionId, $riskMitigationEnabled, $visitData, $redisResults) {
        $pixels = array();

        // if the current visit is already converted then no point going any further since pixels were already displayed
        if ($this->visitTracker->hasConvertedVisit()) {
            return $pixels;
        }

        // pull out the campaign pixel
        $campaign = json_decode($redisResults['campaign_pixel'], true);

        // check if we made it to the conversion page
        $justConverted = $campaign['conversion_split_context_id'] == $sectionId;

        // check if pixels are risk mitigated
        $affiliateId = $visitData['affiliate_id'];
        $riskMitigated = $this->isRiskMitigated($redisResults, $riskMitigationEnabled);

        // go through all the non-risk-mitigated pixels
        if (!$riskMitigated) {
            foreach ($redisResults['normal_pixels'] as $key => $pixel) {
                // decode the pixel
                $pixel = json_decode($pixel, true);

                // check for pixel firing
                if ($justConverted && $this->hasPixelFired($pixel, $affiliateId)) {
                    $visitData['is_pixel_fired'] = true;
                }

                // add any postback pixels to a worker queue
                $pixel = $this->queuePixelPostback($pixel, $visitData);

                // add the custom variables
                $pixel['pixel_code'] = $this->replaceTokens($pixel['pixel_code'], $visitData);

                // add non-postback pixels to the list
                if (!isset($pixel['postback_url']) || empty($pixel['postback_url'])) {
                    $pixels[] = $pixel;
                }
            }
        }
        else {
            // if risk mitigated, mark not fired and skip rest of processing
            $visitData['is_pixel_fired'] = false;
        }

        // update the visit data to reflect the conversion if there was one
        if ($justConverted) {
            $visitData['is_converted'] = true;
            $visitData['converted'] = date('Y-m-d H:i:s');
            $this->visitTracker->updateVisit($visitData);
        }

        return $pixels;
    }

    /**
     * Check to see if a pixel has fired. Ignore risk mitigation
     *
     * @param array $pixel
     * @param int $affiliateId
     * @return boolean
     */
    protected function hasPixelFired($pixel, $affiliateId) {
        if ($affiliateId > 0 && array_key_exists('affiliate_id', $pixel) && $affiliateId == $pixel['affiliate_id']) {
            return true;
        }

        return false;
    }

    /**
     * Check to see if this group of pixels is risk mitigated
     *
     * @param array $redisResults
     * @param boolean $riskMitigationEnabled
     * @return boolean
     */
    protected function isRiskMitigated($redisResults, $riskMitigationEnabled) {
        $riskMitigated = false;

        // generate a random number to check risk mitigation against
        $randomNumber = mt_rand(1, 100);

        // pull the risk mitigation number from the affiliate if possible
        if ($riskMitigationEnabled && isset($redisResults['affiliate_pixel'])) {
            $affiliate = json_decode($redisResults['affiliate_pixel'], true);

            if (isset($affiliate['risk_mitigation']) && $randomNumber <= $affiliate['risk_mitigation']) {
                $riskMitigated = true;
            }
        }

        return $riskMitigated;
    }

    /**
     * Queue the pixel's postback url, if available
     *
     * @param array $pixel
     * @param array $visitData
     * @return array
     */
    protected function queuePixelPostback($pixel, $visitData) {
        if (array_key_exists('postback_url', $pixel) && $pixel['postback_url']) {
            // get the postback url compiled with custom variables set in the admin
            $pixel['postback_url'] = $this->replaceTokens($pixel['postback_url'], $visitData);
            $this->postbackQueue->send($pixel['postback_url']);
        }

        return $pixel;
    }

    /**
     * Update token replacements with visit data
     *
     * @param array $visitData
     * @return array $replacements
     */
    public function getTokenReplacements($visitData = array()) {
        // get the variables needed
        if (empty($visitData)) {
            $visitData = $this->visitTracker->getVisitData();
        }

        // set up the replacements array
        $replacements = array(
            '{{visit_id}}' => isset($visitData['visit_id']) ? $visitData['visit_id'] : '',
            '{{order_id}}' => intval($this->orderId) > 0 ? $this->orderId : '',
            '{{useragent}}' => isset($visitData['useragent']) ? $visitData['useragent'] : '',
            '{{ip_address}}' => isset($visitData['ip_address']) ? $visitData['ip_address'] : '',
            '{{payout}}' => isset($visitData['payout']) ? $visitData['payout'] : '',
            '{{affiliate_id}}' => isset($visitData['affiliate_id']) ? $visitData['affiliate_id'] : '',
            '{{sub_id}}' => isset($visitData['sub_id']) ? $visitData['sub_id'] : '',
            '{{s1}}' => isset($visitData['s1']) ? $visitData['s1'] : '',
            '{{s2}}' => isset($visitData['s2']) ? $visitData['s2'] : '',
            '{{s3}}' => isset($visitData['s3']) ? $visitData['s3'] : '',
            '{{s4}}' => isset($visitData['s4']) ? $visitData['s4'] : '',
            '{{s5}}' => isset($visitData['s5']) ? $visitData['s5'] : '',
        );

        return $replacements;
    }

    /**
     * Use the replacements to fill in tokens in a given string
     *
     * @param string $variable_string
     * @param array $visitData
     * @return string
     */
    public function replaceTokens($variable_string, $visitData) {
        // if the character {{ isn't found then don't bother going through the replacements
        if (strpos($variable_string, '{{') === false) {
            return $variable_string;
        }

        // pull the replacements from the visit data
        $replacements = $this->getTokenReplacements($visitData);

        // replace the variables
        foreach ($replacements as $token => $replacement) {
            $variable_string = str_replace($token, $replacement, $variable_string);
        }

        return $variable_string;
    }
}
