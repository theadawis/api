<?php

class Icm_Visit_Tracker
{
    protected $visitId;
    protected $visitData;
    protected $visitorId = NULL;

    protected $visitTrackerConfig;
    protected $appId;

    protected $visitorCookie;
    protected $visitDb;

    protected $redis;
    protected $cgDb;
    protected $queueFactory;

    protected $visitPrefix;
    protected $visitRedisExpTime;
    protected $visitRecordExpTime;

    /* APP_ID => CAMPAIGN_ID */
    protected $appIdToOrganicCampaignMap = array(
        1    => 1,
        1000 => 4,
    );

    /* APP_ID => AFFILIATE_ID */
    protected $appIdToDefaultAffiliateMap = array(
        1    => 45,
        1000 => 45,
    );

    protected $expectedVisitDataKeys = array(
        'sub_id' => 'sid',
        's1' => 's1',
        's2' => 's2',
        's3' => 's3',
        's4' => 's4',
        's5' => 's5',
    );

    protected $inTesting;

    const DEFAULT_AFFILIATE_ID = 1;
    const DUPLICATE_VISIT_WINDOW = 3600;

    /**
     * @param Icm_Config $visitTrackerConfig
     * @param Icm_Config $redisConfig
     * @param Icm_Config $cgDbConfig
     * @param int $appId
     */
    public function __construct(Icm_Config $visitTrackerConfig,
                                 Icm_Config $redisConfig,
                                 Icm_Config $cgDbConfig,
                                 Icm_Config $rabbitmqConfig,
                                 $appId)
    {
        // set the visit tracker config
        $this->visitTrackerConfig = $visitTrackerConfig;
        $this->redis = new Icm_Redis( $redisConfig );
        $this->queueFactory = new Icm_QueueFactory($rabbitmqConfig);
        $this->appId = $appId;

        // set up the visitor cookie
        $this->visitorCookie = new Icm_Util_Cookie(
            $this->visitTrackerConfig->getOption('visitorCookieName'),
            null,
            $this->visitTrackerConfig->getOption('visitorCookieExpTime')
        );

        // pull the visitor id from the cookie if possible
        $this->visitorId = $this->visitorCookie->getValue();

        // set inTesting to false
        $this->inTesting = false;

        // setup the db
        // @TODO The Icm_Service_Internal_Tracking_Visit should be injected, in my opinion - Chris
        $this->cgDb = Icm_Db_Pdo::connect('cgDb', $cgDbConfig);
        $this->visitDb = new Icm_Service_Internal_Tracking_Visit($this->cgDb);

        // get the variables needed from config
        $this->visitPrefix = $this->visitTrackerConfig->getOption('visitPrefix');
        $this->visitRecordExpTime = $this->visitTrackerConfig->getOption('visitRecordExpTime');
        $this->visitRedisExpTime = $this->visitTrackerConfig->getOption('visitRedisExpTime');

        // set initial visit data
        $this->visitData = array();
    }

    /**
     * Create a new visit record. Prevent bot and duplicate visits from being created.
     *
     * @param array $link
     * @param array $getParams
     * @return bool
     */
    public function createVisit($link = null, $getParams = array()) {
        // create user agent
        $userAgent = new Icm_Util_Useragent();

        // check for robots and duplicates
        if ($userAgent->isRobot()) {
            return false;
        }

        // reset our visit data
        $this->visitData = array();

        // check for duplicates
        $updateRedis = true;

        if ($this->isDuplicate()) {
            $updateRedis = false;
            $this->visitData['is_duplicate'] = true;
        }

        // get or generate a visitor id
        $visitorId = $this->getVisitorId();

        // generate a visit id
        $this->visitId = $this->generateUniqueId();

        // get the params
        $params = array_change_key_case($getParams, CASE_LOWER);

        // setup the initial visit data
        $initialData = $this->getInitialVisitData($visitorId, $userAgent, $params);
        $this->visitData = array_merge($this->visitData, $initialData);

        // add in link data if available
        $linkData = $this->getLinkVisitData($link);
        $this->visitData = array_merge($this->visitData, $linkData);

        // if there's a src but not link, use affiliate data
        if (!isset($link['id']) && isset($params['src'])) {
            $affiliateData = $this->getAffiliateVisitData($params);
            $this->visitData = array_merge($this->visitData, $affiliateData);
        }

        // pure organic traffic
        if (!isset($link['id']) && !isset($params['src'])) {
            $organicData = $this->getOrganicVisitData();
            $this->visitData = array_merge($this->visitData, $organicData);
        }

        // add the data to the visit queue
        $queue = $this->queueFactory->getCreateClickQueue();
        $queue->send($this->visitData);

        // store the data into redis
        if ($updateRedis) {
            $this->redis->setex($this->visitPrefix . $this->visitorId, $this->visitRedisExpTime, json_encode($this->visitData));
        }

        return true;
    }

    public function updateVisit(array $updates) {
        // add visit_id to the updates
        $updates['visit_id'] = $this->getVisitId();
        $updates['visitor_id'] = $this->getVisitorId();

        // set the updated timestamp
        $updates['updated'] = date('Y-m-d H:i:s');

        // merge the data
        $this->visitData = array_merge($this->getVisitData(), $updates);

        // save it back in redis if unique
        if (!array_key_exists('is_duplicate', $this->visitData) || !$this->visitData['is_duplicate']) {
            $this->redis->setex($this->visitPrefix . $this->visitorId, $this->visitRedisExpTime, json_encode($this->visitData));
        }

        // update ES and mysql with the worker
        $queue = $this->queueFactory->getUpdateClickQueue();
        $queue->send($this->visitData);

        return true;
    }

    /**
     * Fetch the current visitor id
     *
     * @return string
     */
    public function getVisitorId() {
        // if the visitor id is empty, try to get it from the cookie
        if (empty($this->visitorId)) {
            $this->visitorId = $this->visitorCookie->getValue();
        }

        // if it's still empty, create a new one
        if (empty($this->visitorId)) {
            // generate a visitor id
            $this->visitorId = $this->generateUniqueId();

            // set the visitor id cookie
            if (!$this->inTesting) {
                $this->visitorCookie->setValue($this->visitorId);
            }
        }

        // return the visitor id
        return $this->visitorId;
    }

    /**
     * Check if the visit is a duplicate
     *
     * @return bool
     */
    public function isDuplicate() {
        // if our visit data is marked as a duplicate, it's a duplicate
        if (isset($this->visitData['is_duplicate']) && $this->visitData['is_duplicate']) {
            return true;
        }

        // if the last visit was less than an hour from now, it's a duplicate
        $lastVisit = $this->readVisitData();

        if ($lastVisit && strtotime($lastVisit['created']) + self::DUPLICATE_VISIT_WINDOW > time()) {
            return true;
        }

        return false;
    }

    public function getVisitData() {
        // read the visit data if we don't have it yet
        if (empty($this->visitData)) {
            $this->visitData = $this->readVisitData();
        }

        return $this->visitData;
    }

    public function getVisitId() {
        // if visit id empty, try to pull from visit data
        if (empty($this->visitId)) {
            $visitData = $this->readVisitData();

            if (isset($visitData['visit_id'])) {
                $this->visitId = $visitData['visit_id'];
            }
        }

        // if visit data empty, try to pull from cookie
        if (empty($this->visitId)) {
            $visitCookie = new Icm_Util_Cookie(
                $this->visitTrackerConfig->getOption('visitCookieName'),
                $_SERVER['HTTP_HOST'],
                $this->visitRecordExpTime
            );

            $this->visitId = $visitCookie->getValue();
        }

        return $this->visitId;
    }

    public function getConfig() {
        return $this->visitTrackerConfig;
    }

    /**
     * Set the inTesting flag. Used only by unit tests to prevent cookie creation
     *
     * @param bool $testing
     */
    public function setInTesting($testing = false) {
        $this->inTesting = $testing;
    }

    /**
     * Fetch tracking link info from redis or db
     *
     * @param string $linkHashId
     * @return array
     */
    public function getByLinkByHashId($linkHashId) {
        // get config vars
        $linkPrefix = $this->visitTrackerConfig->getOption('linkPrefix');

        // try to fetch the object from redis
        $link = $this->redis->get($linkPrefix . $linkHashId);

        if (!$link) {
            // get the link from mysql if it doesn't exist in redis
            $linkDb = new Icm_Service_Internal_Marketing_Affiliate_Link($this->cgDb);
            $link = $linkDb->getByHash($linkHashId);

            if ($link) {
                // if the link can be retrieved from mysql, then update redis
                $this->redis->set($linkPrefix . $linkHashId, json_encode($link));
            }
        }
        else {
            // decode the link (to an array) before returning it
            $link = json_decode($link, true);
        }

        return $link;
    }

    /**
     * Pull visit data from redis first, and fall back to mysql
     *
     * @return array
     */
    protected function readVisitData() {
        // make sure we have a visitor id first
        if (!$this->visitorId) {
            return array();
        }

        // setup default visit data
        $visitData = array();

        // try to pull from redis first
        $redisData = $this->redis->get($this->visitPrefix . $this->visitorId);

        if ($redisData && $redisData != "null") {
            $visitData = json_decode($redisData, true);
        }
        else {
            // if not there, try pulling from db
            $dbData = $this->visitDb->getMostRecentForVisitorId($this->visitorId);

            if ($dbData) {
                // save the data in redis for next time
                $visitData = $dbData->toArray();
                $this->redis->setex($this->visitPrefix . $this->visitorId, $this->visitRedisExpTime, json_encode($visitData));
            }
        }

        // ensure visit data has boolean values for is_duplicate and is_converted
        if (!empty($visitData)) {
            $visitData['is_duplicate'] = isset($visitData['is_duplicate']) ? (bool) $visitData['is_duplicate'] : false;
            $visitData['is_converted'] = isset($visitData['is_converted']) ? (bool) $visitData['is_converted'] : false;
        }

        // set the visit data only if it's less than 30 days old
        return !$this->isOldVisit($visitData) ? $visitData : array();
    }

    /**
     * Check if the current visit data is > 30 days old
     *
     * @param array $visitData
     * @return boolean
     */
    public function isOldVisit($visitData) {
        // don't bother checking empty visit data
        if (empty($visitData)) {
            return false;
        }

        // pull the visit created date
        $created = strtotime($visitData['created']);

        // add 30 days
        $createdPlusThirty = $created + $this->visitRecordExpTime;

        // grab current timestamp
        $today = time();

        // if the result is less than today's date, it's too old
        if ($createdPlusThirty <= $today) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the current visitor has converted or not
     * Note: Uses visit data, not order data to make determination.
     * @return bool
     */
    public function hasConvertedVisit() {
        $convertedVisits = array_filter($this->visitDb->getAllForVisitor($this->visitorId), function($item) {
            /**
             * @var Icm_Struct_Tracking_Visit $item
             */
            return (bool) $item->is_converted;
        });

        $hasExistingConversion = count($convertedVisits) > 0;

        // On occasion I've noticed that is_converted and is_duplicate are treated as 0/1 in string form.
        $is_converted = array_key_exists("is_converted", $this->visitData) &&
            $this->visitData["is_converted"] &&
            $this->visitData["is_converted"] !== "0";

        return $is_converted || $hasExistingConversion;
    }

    protected function generateUniqueId() {
        return hash('sha1', microtime() . mt_rand() . Icm_Util_IPAddress::get());
    }

    /**
     * Setup the basic visit data array
     *
     * @param string $visitorId
     * @param Icm_Util_Useragent $userAgent
     * @param array $params
     * @return array
     */
    protected function getInitialVisitData($visitorId, $userAgent, $params = array()) {
        // set the basic info for all visits
        $visitData = array();
        $visitData['visit_id'] = $this->getVisitId();
        $visitData['app_id'] = $this->appId;
        $visitData['visitor_id'] = $visitorId;
        $visitData['ip_address'] = Icm_Util_IPAddress::get();
        $visitData['useragent'] = $userAgent->getAgent();
        $visitData['referer'] = $userAgent->getReferrer();
        $visitData['created'] = date('Y-m-d H:i:s');
        $visitData['updated'] = date('Y-m-d H:i:s');
        $visitData['is_converted'] = false;
        $visitData['is_pixel_fired'] = false;
        $visitData['params'] = json_encode($params);

        // set the "expected" parameters into visit data
        foreach ($this->expectedVisitDataKeys as $key => $paramsKey) {
            $visitData[$key] = isset($params[$paramsKey]) ? $params[$paramsKey] : null;
        }

        return $visitData;
    }

    /**
     * Build visit data with info from tracking link
     *
     * @param array $link
     * @return array
     */
    protected function getLinkVisitData($link = array()) {
        $linkData = array();
        $linkData['affiliate_link_id'] = isset($link['id']) ? $link['id'] : null;
        $linkData['campaign_id'] = isset($link['campaign_id']) ? $link['campaign_id'] : $this->appIdToOrganicCampaignMap[$this->appId];
        $linkData['affiliate_id'] = isset($link['affiliate_id']) ? $link['affiliate_id'] : self::DEFAULT_AFFILIATE_ID;
        $linkData['payout'] = isset($link['payout']) ? $link['payout'] : null;

        return $linkData;
    }

    /**
     * Build array of visit data with affiliate info
     *
     * @param array $params
     * @return array
     */
    protected function getAffiliateVisitData($params = array()) {
        // setup defaults
        $cnt = isset($params['cnt']) ? $params['cnt'] : null;
        $cmp = isset($params['cmp']) ? $params['cmp'] : null;
        $srcData = array(
            'sub_id' => $params['src'],
            's1' => $cmp,
            's2' => $cnt,
        );

        // try to pull affiliate data from redis
        $result = $this->redis->get($this->visitTrackerConfig->getOption('affiliateMapPrefix') . $this->appId . '_' . strtolower($params['src']));

        // if no data, treat as "misc" affiliate
        if (is_null($result)) {
            // set default affiliate if possible
            if (array_key_exists($this->appId, $this->appIdToDefaultAffiliateMap)) {
                $srcData['affiliate_id'] = $this->appIdToDefaultAffiliateMap[$this->appId];
            }

            return $srcData;
        }

        // decode the redis result
        $result = json_decode($result, true);

        // set the affiliate id
        $srcData['affiliate_id'] = $result['affiliate_id'];

        // if the subid is set, then we want to use it and set everything accordingly
        if (isset($result['subid']) && strlen($result['subid']) > 0) {
            $srcData['sub_id'] = $result['subid'];
            return $srcData;
        }

        // not having sub_id has a little different logic
        $srcData['sub_id'] = $cmp;
        $srcData['s1'] = $cnt;
        unset($srcData['s2']);

        // return the src data
        return $srcData;
    }

    /**
     * Build array of organic traffic visit data
     *
     * @return array
     */
    protected function getOrganicVisitData() {
        // organic traffic (no link and no src parameter), then set different logic
        $organicData = array();

        // set up the member cookie
        $isMemberCookie = new Icm_Util_Cookie("is_member");

        // set the s4 to the current page they landed on
        $organicData['s4'] = $_SERVER['REQUEST_URI'];

        // set the s5 to member or non
        $organicData['s5'] = ($isMemberCookie->getValue() == 'true') ? 'member' : 'non-member';

        return $organicData;
    }
}
