<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/25/12
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Analytics_Db_Data
{
    public function insert($eventFk, Icm_Analytics_Event $event);
}
