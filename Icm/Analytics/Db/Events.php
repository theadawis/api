<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/25/12
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Analytics_Db_Events
{

    public function insert($visitorId, $timestamp, $eventId);

}
