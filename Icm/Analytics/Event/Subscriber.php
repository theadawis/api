<?php
class Icm_Analytics_Event_Subscriber implements Icm_Event_Subscriber_Interface
{

    private static $EVT_LIST =  array(
        Icm_Analytics_Event::EVT_PAGELOAD,
        Icm_Analytics_Event::EVT_CLICK,
        Icm_Analytics_Event::EVT_ACTION
    );

    protected $logger;

    public function __construct(array $mapping, Icm_Util_Logger_Interface $logger) {
        $this->mapping = $mapping;
        $this->log = $logger;
    }

    /**
     * @return array
     */
    public function getEventList() {
        return self::$EVT_LIST;
    }

    /**
     * @param $evtName
     * @param Icm_Event_Interface $event
     * @return mixed
     */
    public function dispatch($evtName, Icm_Event_Interface $event) {
        if (!is_subclass_of($event, 'Icm_Analytics_Event') && !is_a($event, 'Icm_Analytics_Event')) {
            return;
        }

        /**
         * @var Icm_Analytics_Event $event
         */

        if (!array_key_exists($evtName, $this->mapping)) {
            return;
        }

        $event->setEventId($this->mapping[$evtName]);

        if ($evtName == Icm_Analytics_Event::EVT_PAGELOAD) {
            return $this->log->logEvent($event->toArray());
        }
    }
}
