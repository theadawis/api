<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/25/12
 * Time: 10:47 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Analytics_Event_Subscriber_Syslog implements Icm_Event_Subscriber_Interface
{

    private static $EVT_LIST =  array(
        Icm_Analytics_Event::EVT_PAGELOAD,
        Icm_Analytics_Event::EVT_CLICK,
        Icm_Analytics_Event::EVT_ACTION,
        Icm_Analytics_Event::EVT_TRANSACTION
    );

    protected $logger;

    public function __construct(Icm_Util_Logger_Interface $logger) {
        $this->log = $logger;
    }

    /**
     * @return array
     */
    public function getEventList() {
        return self::$EVT_LIST;
    }

    /**
     * @param $evtName
     * @param Icm_Event_Interface $event
     * @return mixed
     */
    public function dispatch($evtName, Icm_Event_Interface $event) {
        if (!is_a($event, 'Icm_Analytics_Event')) {
            return;
        }

        /**
         * @var Icm_Analytics_Event $event
         */
        return $this->log->logEvent($event->toArray());
    }
}
