<?php
/**
 * Icm_Analytics_Event_Subscriber_RabbitMQ.php
 * User: chris
 * Date: 5/18/13
 * Time: 9:29 PM
 */
class Icm_Analytics_Event_Subscriber_Queue implements Icm_Event_Subscriber_Interface{

    private static $EVT_LIST =  array(
        Icm_Analytics_Event::EVT_PAGELOAD,
        Icm_Analytics_Event::EVT_CLICK,
        Icm_Analytics_Event::EVT_ACTION,
        Icm_Analytics_Event::EVT_TRANSACTION
    );

    /**
     * @var Zend_Queue
     */
    protected $queue;

    public function __construct(Zend_Queue $queue) {
        $this->queue = $queue;
    }

    public function dispatch($evtName, Icm_Event_Interface $event) {
        if(is_a($event, 'Icm_Analytics_Event')) {
            $this->queue->send($event->toArray());
        }
    }

    /**
     * @return array
     */
    public function getEventList() {
        return self::$EVT_LIST;
    }
}
