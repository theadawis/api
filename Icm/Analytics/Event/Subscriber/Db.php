<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/25/12
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Analytics_Event_Subscriber_Db implements Icm_Event_Subscriber_Interface
{

    private static $EVT_LIST = array(
        Icm_Analytics_Event::EVT_PAGELOAD,
        Icm_Analytics_Event::EVT_TRANSACTION,
        Icm_Analytics_Event::EVT_ACTION
    );

    /**
     * @var array $mapping
     */
    protected $mapping = array();
    /**
     * @var Icm_Db_Table $table
     */
    protected $evtTable;

    /**
     * @var Icm_Db_Table
     */
    protected $evtDataTable;

    public function __construct(array $mapping, Icm_Analytics_Db_Events $evtTable, Icm_Analytics_Db_Data $evtDataTable) {
        $this->evtTable = $evtTable;
        $this->evtDataTable = $evtDataTable;
        $this->mapping = $mapping;
    }

    public function dispatch($evtName, Icm_Event_Interface $event) {
        if (!is_subclass_of($event, 'Icm_Analytics_Event') && !is_a($event, 'Icm_Analytics_Event')) {
            return;
        }
        /**
         * @var Icm_Analytics_Event $event
         */
        $fk = $this->evtTable->insert($event->getVisitorId(), $event->getTimestamp(), $event->getEventId());
        $this->evtDataTable->insert($fk, $event);
    }

    /**
     * @return array
     */
    public function getEventList() {
        return self::$EVT_LIST;
    }
}
