<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/2/12
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Analytics_Event_Factory_Interface
{

    /**
     * @abstract
     * @param $evtId
     * @return Icm_Analytics_Event_Interface
     */
    public function createEvent($evtId);

}
