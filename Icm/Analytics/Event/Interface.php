<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/2/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Analytics_Event_Interface extends Icm_Event_Interface
{

    public function setAppId($appId);
    public function setEventId($eventId);
    public function setEventData($eventDataKey, $evtDataVal = null);
    public function setVisitorId($visitorId);
    public function setVisitId($visitId);

    public function setCurrentTest(Icm_SplitTest_Variation $test);
    public function setRequestContext(array $requestContext);
    public function setTestContext(array $testContext);
    public function setTrackingContext(array $trackingContext);
    public function setTimestamp($timestamp);


    public function getEventData($key = null, $default = null);
    public function getVisitorId();
    public function getVisitId();
    public function getCurrentTest();
    public function getRequestContext();
    public function getTestContext();
    public function getTrackingContext();
    public function getTimestamp();

    public function toArray();
}
