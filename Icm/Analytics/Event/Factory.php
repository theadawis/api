<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/2/12
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Analytics_Event_Factory implements Icm_Analytics_Event_Factory_Interface
{

    protected $evtClass;
    protected $tracker;
    protected $appId;

    public function __construct(Icm_SplitTest_Tracker $tracker, $evtClass = "Icm_Analytics_Event") {
        $this->evtClass = $evtClass;
        $this->tracker = $tracker;
    }

    public function setAppId($appId = 1) {
        $this->appId = $appId;
    }

    public function setEventClass($evtClass) {
        if (!class_exists($evtClass)) {
            throw new Exception("$evtClass does not exist");
        }
        if (!in_array('Icm_Analytics_Event_Interface', class_implements($evtClass))){
            throw new Exception("$evtClass does not implement Icm_Event_Interface");
        }
        $this->evtClass = $evtClass;
    }

    public function getEventClass() {
        return $this->evtClass;
    }

    /**
     * @param $evtId
     * @param $evtData
     * @return Icm_Analytics_Event_Interface
     */
    public function createEvent($evtId, array $evtData = array()){

        $class = $this->getEventClass();
        $evt = new $class();

        /**
         * @var Icm_Analytics_Event_Interface $evt
         */
        $evt->setAppId($this->appId);
        $evt->setEventId($evtId);
        $currentTest = $this->tracker->getCurrentTest();
        if (is_a($currentTest, 'Icm_SplitTest_Variation')){
            $evt->setCurrentTest($currentTest);
        }
        $evt->setTestContext($this->tracker->getTestContext());
        $evt->setRequestContext($this->tracker->getRequestContext());
        $evt->setTrackingContext($this->tracker->getTrackingContext());
        $evt->setVisitorId($this->tracker->getVisitorId());
        $evt->setVisitId($this->tracker->getVisitId());
        $evt->setTimestamp(time());

        foreach ($evtData as $k => $v) {
            $evt->setEventData($k, $v);
        }

        return $evt;

    }
}
