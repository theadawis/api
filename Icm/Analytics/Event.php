<?php
class Icm_Analytics_Event extends Icm_Event implements Icm_Analytics_Event_Interface
{

    const EVT_PAGELOAD = 'pageLoad';
    const EVT_CLICK = 'click';
    const EVT_ACTION = 'action';
    const EVT_TRANSACTION = 'transaction';

    protected $appId;
    protected $eventId;
    protected $visitorId;
    protected $visitId;
    protected $timestamp;

    protected $requestContext;
    protected $trackingContext;
    protected $testContext;

    /**
     * @var Icm_SplitTest_Variation
     */
    protected $currentTest;

    /**
     * @var array arbitrary data.
     */
    protected $evtData = array();

    /**
     * @param $requestContext
     */
    public function setRequestContext(array $requestContext) {
        $this->requestContext = $requestContext;
    }

    /**
     * @return array
     */
    public function getRequestContext() {
        return $this->requestContext;
    }

    /**
     * @param $testContext
     */
    public function setTestContext(array $testContext) {
        $this->testContext = $testContext;
    }

    /**
     * @return array
     */
    public function getTestContext() {
        return $this->testContext;
    }

    /**
     * @param $trackingContext
     */
    public function setTrackingContext(array $trackingContext) {
        $this->trackingContext = $trackingContext;
    }

    /**
     * @return array
     */
    public function getTrackingContext() {
        return $this->trackingContext;
    }

    public function setTimestamp($timestamp) {
        $this->timestamp = (int) $timestamp;
    }

    public function getTimestamp() {
        return $this->timestamp;
    }

    /**
     * @return array
     */
    public function toArray() {
        $data = array(
            'appId' => $this->getAppId(),
            'visitorId' => $this->getVisitorId(),
            'visitId' => $this->getVisitId(),
            'eventId' => $this->getEventId(),
            'timestamp' => $this->getTimestamp(),
            'testContext' => $this->getTestContext(),
            'requestContext' => $this->getRequestContext(),
            'trackingContext' => $this->getTrackingContext(),
        );

        try{
            $data['eventContext'] = array_merge($this->getEventData(), array(
                    'sectionId' => $this->getCurrentTest()->getSectionId(),
                    'variationId' => $this->getCurrentTest()->getVariationId(),
                    'testId' => $this->getCurrentTest()->getTestId(),
            ));
        } catch(Icm_Exception $e) {
            $data['eventContext'] = $this->getEventData();
        }

        if (count($this->evtData)) {
            $data['eventData'] = $this->getEventData();
        }
        return $data;
    }

    public function setEventData($evtDataKey, $evtDataVal = null) {
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug("Setting event Data..." . print_r($evtDataKey, true));
        if (is_array($evtDataKey)) {
            foreach ($evtDataVal as $k => $v) {
                $this->setEventData($k, $v);
            }
            return;
        }
        $this->evtData[$evtDataKey] = $evtDataVal;
    }

    public function getEventData($key = null, $default = null) {
        if (is_null($key)) {
            return $this->evtData;
        }
        if (array_key_exists($key, $this->evtData)) {
            return $this->evtData[$key];
        }
        return $default;
    }

    public function setEventId($eventId) {
        $this->eventId = (int) $eventId;
    }

    public function getEventId() {
        return $this->eventId;
    }

    public function setVisitorId($visitorId) {
        $this->visitorId = $visitorId;
    }

    public function getVisitorId() {
        return $this->visitorId;
    }

    public function setVisitId($visitId){
        $this->visitId = $visitId;
    }

    public function getVisitId() {
        return $this->visitId;
    }

    public function setCurrentTest(Icm_SplitTest_Variation $t) {
        $this->currentTest = $t;
    }

    public function getCurrentTest() {
        if (is_a($this->currentTest, 'Icm_SplitTest_Variation')) {
            return $this->currentTest;
        }
        throw new Icm_Exception("There is not a current test.");
    }

    public function setAppId($appId) {
        $this->appId = (int) $appId;
    }

    public function getAppId() {
        return $this->appId;
    }
}
