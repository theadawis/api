<?php

require_once '/sites/external-libs/Snappy/autoload.php';
use Knp\Snappy\Pdf;

class Icm_Service_Pdf_Snappy implements Icm_Service_Pdf_Interface
{
    protected $snappy;
    protected $location = 'DISPLAY=:5 /usr/local/bin/wkhtmltopdf';

    public function __construct() {
        $options = array(
            'use-xserver' => true
        );

        $this->snappy = new Pdf($this->location, $options);
    }

    public function getPdfFromHtml($htmlContent) {
        return $this->snappy->getOutputFromHtml($htmlContent);
    }

}
