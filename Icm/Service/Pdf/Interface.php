<?php

interface Icm_Service_Pdf_Interface
{
    public function getPdfFromHtml($htmlContent);
}
