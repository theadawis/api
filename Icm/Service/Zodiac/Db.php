<?php

/**
 * Data adapter for our Zodiac Sign table
 * @author Emacs Daddy
 *
 */
class Icm_Service_Zodiac_Db implements Icm_Configurable{
    protected $connection, $tableName;
    protected $config;
    const tableName = 'horoscope_matches';

    public function __construct(Icm_Db_Interface $conn = NULL){
        $this->connection = $conn;
        $this->tableName = self::tableName;
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->connection = Icm_Db::getInstance($this->config->getOption('connection'));
    }

    public function getCompatibility($sign1, $sign2){
        $data = $this->connection->fetch("SELECT * FROM horoscope_matches WHERE sign1=:sign1 AND sign2=:sign2;", array(':sign1' => $sign1, ':sign2' => $sign2));

        if (!empty($data)) {
            $data['paraphrased'] = utf8_encode(html_entity_decode($data['paraphrased']));
            $data['score_phrase'] = utf8_encode($data['score_phrase']);
            $data['sign1'] = utf8_encode($data['sign1']);
            $data['sign2'] = utf8_encode($data['sign2']);
            $data['score'] = utf8_encode($data['score']);

            unset($data['original']);
        }

        return $data;
    }
}
