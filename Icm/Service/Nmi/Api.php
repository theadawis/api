<?php
/**
 * @author Joe Linn
 */
class Icm_Service_Nmi_Api extends Icm_Service_Api{
    protected $config = array();

    /**
     * "Not crap." -Chris G.
     * @var array
     */
    protected $defaultConfig = array(
        'url' => 'https://secure.networkmerchants.com/api/transact.php',
        'username' => 'InstantCheckMate',
        'password' => 'ch3ckm4t3',
        'test_user' => 'demo',
        'test_password' => 'password',
        'processorId' => ''
    );
    public $sandbox;

    public function __construct(Icm_Config $config){
        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $config->getOption($key, $default);
        }

        $this->httpClient = $this->getHttpClient();
        $this->httpClient->setUrl($this->config['url']);
    }

    /**
     * Charge a credit card
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @param string $descriptor
     * @param string $type
     * @return array
     */
    public function sale(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order, $descriptor = false, $type = 'SALE'){

        try {
            $creditCard = $customer->getCreditCard();
        } catch (Icm_Commerce_Exception $ex) {
            throw new Icm_Service_Nmi_Exception("Customer must have a credit card.");
        }

        if (!$creditCard->isValidCCNumber()){
            throw new Icm_Service_Nmi_Exception("Invalid credit card number.");
        }

        if (!$creditCard->isValidExpirationDate()){
            throw new Icm_Service_Nmi_Exception("Invalid credit card expiration date.");
        }

        if (!$order->getOrderId()){
            throw new Icm_Service_Nmi_Exception("Invalid or no order id provided.");
        }

        $products = $order->getProducts();

        if (count($products) < 1) {
            throw new Icm_Service_Nmi_Exception("At least one product must be provided in this order.");
        }

        $products->rewind();
        $currentProduct = $products->current();
        $currentProduct = $currentProduct[0];
        $variation = $customer->getVaration();
        $data = array(
            'type' => $type,
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
            'ccnumber' => $creditCard->getNumber(),
            'ccexp' => $creditCard->formatExpiration(),
            'amount' => $order->getSubtotal(), //change this if we need to charge tax
            'cvv' => !empty($creditCard->cvv) ? $creditCard->cvv : '',
            'payment' => 'creditcard', //change this if we need to accept checks
            'processor_id' => $this->config['processorId'], //change this if we need to use multiple processors
            'descriptor' => !empty($descriptor) ? $descriptor : '',
            'orderdescription' => $currentProduct->name,
            'orderid' => $order->getOrderId(),
            'ipaddress' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '',
            'tax' => '0.00', //change this if we charge tax
            'shipping' => '0.00', //change this if we start shipping stuff
            'firstname' => $customer->first_name,
            'lastname' => $customer->last_name,
            'address1' => $customer->address1,
            'address2' => $customer->address2,
            'city' => $customer->city,
            'state' => $customer->state,
            'zip' => $customer->zip,
            'country' => 'US', //change this to support international billing
            'phone' => $customer->phone,
            'email' => $customer->email,
            'merchant_defined_field_1' => $order->getOrderId(),
            'merchant_defined_field_2' => $currentProduct->getContinuity(), //continuity: -1 = one-time charge; 1 = recurring
            'merchant_defined_field_3' => '', //TODO: product group ('criminal_records')
            'merchant_defined_field_4' => $currentProduct->sku,
            'merchant_defined_field_5' => 'instantcheckmate.com',
            'merchant_defined_field_6' => $variation->getTestId() . ':' . $variation->getVariationId(), //test:variation
            'merchant_defined_field_7' => $customer->getVisit()->getAffiliate()->short_name, //tracking src (affiliate short name)
            'merchant_defined_field_8' => $customer->getVisit()->sub_id, //tracking cnt (affiliate sub_id)
            'merchant_defined_field_17' => $creditCard->getCardType(),
        );
        $counter = 1;

        foreach ($order->getProducts() as $product){
            $data['product_sku_' . $counter] = $product[0]->sku;
            $counter++;
        }

        $result = $this->httpClient->post($data);
        parse_str($result, $result);

        return $result;
    }

    /**
     * Refund a transaction
     * @param Icm_Commerce_Transaction $transaction
     * @param float $amount
     * @throws Icm_Service_Nmi_Exception
     * @return array
     */
    public function refund(Icm_Commerce_Transaction $transaction, $amount = false){

        if (!is_numeric($transaction->amount) && !is_numeric($amount)){
            throw new Icm_Service_Nmi_Exception('Transaction amount or amount must be specified.');
        }

        if ($amount && !is_numeric($amount)){
            throw new Icm_Service_Nmi_Exception('$amount must be either a numeric value or boolean false.');
        }

        if ($amount > $transaction->amount){
            throw new Icm_Service_Nmi_Exception('Refund amount cannot be greater than the original transaction amount.');
        }

        if (strlen($transaction->getTransactionId()) < 1){
            throw new Icm_Service_Nmi_Exception('Transaction Id is required.');
        }

        if (strlen($transaction->getOrderId()) < 1){
            throw new Icm_Service_Nmi_Exception('Order Id is required.');
        }

        $data = array(
            'type' => 'refund',
            'username' => $this->config['username'],
            'password' => $this->config['password'],
            'transactionid' => $transaction->gateway_txid,
            'amount' => $amount ? $amount : $transaction->amount,
            'orderid' => $transaction->getOrderId()
        );

        $result = $this->httpClient->post($data);
        parse_str($result, $result);

        return $result;
    }

    public function setSandbox($bool) {
        $this->sandbox = $bool;
    }

    protected function getUsername() {
        return $this->sandbox ? $this->config["test_user"] : $this->config["username"];
    }

    protected function getPassword() {
        return $this->sandbox ? $this->config["test_password"] : $this->config["password"];
    }

}