<?php
/**
 * Storage adapter for social network tokens
 * @author Shiem Edelbrock
 */

class Icm_Service_Social{
    const QUEUE_KEY = 'icm_social_keys';
    const FACEBOOK_KEY = ':facebook_token';
    const ALIAS = 'social_keys';
    const TYPE = 'social_keys';

    /**
     * @var Icm_Redis
     */
    protected $redis = NULL;
    /**
     * @param Icm_Redis $redis
     * @param Icm_Service_Internal_Elasticsearch $elasticsearch
     */
    public function __construct(Icm_Redis $redis = NULL){
        if (!is_null($redis)){
            $this->redis = $redis;
        }
    }
    public function setRedis(Icm_Redis $redis){
        $this->redis = $redis;
        return $this;
    }

    /**
     * Add an authorized token to redis
     * @param {string} $type  [social network type e.g. 'facebook']
     * @param {string} $token [the access_token provided by the network]
     */
    public function addToken($type, $token){
        if ($type === 'facebook'){
            return $this->getRedis()->sadd(self::QUEUE_KEY . self::FACEBOOK_KEY, $token);
        }
        else{
            throw new Exception("Social Network not recognized");
        }
    }
/**
 * Get random access tokens for a social network
 * @param  {[string]} $type  [Social network type e.g. facebook]
 * @param  {[int]} $count  [The number of records you would like returned (defaults to 5)]
 */
    public function getToken($type, $count = 5){
        if ($type === 'facebook'){
            return $this->getRedis()->srandmember(self::QUEUE_KEY . self::FACEBOOK_KEY, $count);
        }
        else{
            throw new Exception("Social Network not recognized");
        }
    }

    /**
     *
     * @throws Exception
     * @return Icm_Redis
     */
    protected function getRedis(){
        if (is_null($this->redis)){
            throw new Exception("You must properly set the Redis service before attempting a Redis operation.");
        }
        return $this->redis;
    }
}