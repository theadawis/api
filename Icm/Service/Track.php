<?php
/**
 * Storage adapter for random tracking data.  We need to track random events, and since we don't want to add an extra layer to our stack.... we decided to user redis
 * @author Shiem Edelbrock
 */

class Icm_Service_Track{
    const QUEUE_KEY = 'icm_track:';

    /**
     * @var Icm_Redis
     */
    protected $redis = NULL;
    /**
     * @param Icm_Redis $redis
     */
    public function __construct(Icm_Redis $redis = NULL){
        if (!is_null($redis)){
            $this->redis = $redis;
        }
    }

    /**
     * @param string $key - the list key we want to roll up to (defualts to no_key for bad people)
     * @param string(json) $value - the json value we are storing
     * @return function
     * @throws error if no value is set
     */
    public function push($key = "no_key", $value){
        if (empty($value)){
            throw new Exception('You must pass a valid JSON value');
        }
        else{
            return $this->getRedis()->lpush(self::QUEUE_KEY . $key, $value);
        }
    }

    /**
     * @param string $key The key which we are going to increment
     * @return function
     */
    public function incr($key = "no_key"){
        return $this->getRedis()->incr(self::QUEUE_KEY . $key);
    }

    /**
     * @param string $key The key which we are going to de-increment
     * @return function
     */
    public function decr($key = "no_key"){
        return $this->getRedis()->decr(self::QUEUE_KEY . $key);
    }
    /**
     *
     * @throws Exception
     * @return Icm_Redis
     */
    protected function getRedis(){
        if (is_null($this->redis)){
            throw new Exception("You must properly set the Redis service before attempting a Redis operation.");
        }
        return $this->redis;
    }
}