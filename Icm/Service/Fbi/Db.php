<?php
/**
 * Data adapter for our fbi table
 * @author Nathan Dean Eat Your Beans
 *
 */
class Icm_Service_Fbi_Db implements Icm_Configurable{

    protected $connection, $cityCrime, $stateCrime, $countryCrime, $notes, $config;
    const cityCrime  = 'fbi_crime_stats_city';
    const stateCrime = 'fbi_crime_stats_state';
    const countryCrime = 'fbi_crime_stats_country';
    const notes       = 'fbi_notes';


    public function __construct(Icm_Db_Interface $conn = NULL){
        $this->connection  = $conn;
        $this->cityCrime  = self::cityCrime;
        $this->stateCrime = self::stateCrime;
        $this->countryCrime = self::countryCrime;
        $this->notes       = self::notes;
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->connection = Icm_Db::getInstance($this->config->getOption('connection'));
    }

    /**
     * This function returns all of the fbi crime stats for a city by city name
     * @param array $criteria This should be an associative array...
     * @throws Exception
     * @return array
     */
    function getCityStats($args = false){

        if (isset($args['stateName']) && isset($args['cityName'])){

            // configurate optional query stuffs
            $yearQuery = '';
            if (isset($args['year'])){
                $yearQuery = " AND year=:year";
                $replacements[':year'] = $args['year'];
            }

            // configurate query replacements
            $replacements[':state'] = $args['stateName'];
            $replacements[':city'] = $args['cityName'];

            // build the query
            $query = "
                SELECT
                    `population`,
                    `violent_crimes`,
                    `violent_crimes_per_capita`,
                    `murder_and_nonnegligent_manslaughter`,
                    `murder_and_nonnegligent_manslaughter_per_capita`,
                    `forcible_rape`,
                    `forcible_rape_per_capita`,
                    `robbery`,
                    `robbery_per_capita`,
                    `aggravated_assault`,
                    `aggravated_assault_per_capita`,
                    `property_crimes`,
                    `property_crimes_per_capita`,
                    `burglary`,
                    `burglary_per_capita`,
                    `larceny_theft`,
                    `larceny_theft_per_capita`,
                    `motor_vehicle_theft`,
                    `motor_vehicle_theft_per_capita`,
                    `arson`,
                    `arson_per_capita`,
                    `year`
                FROM `{$this->cityCrime}`
                WHERE state=:state AND city=:city $yearQuery
                ORDER BY year ASC
            ";
            // echo '<pre>'.print_r($query, true) . '</pre>';
            $return = $this->connection->fetchAll($query, $replacements);
            return $return;
        }
        return array();
    }

    /**
     * This function returns all of the fbi crime stats for a city by city name
     * @param array $criteria This should be an associative array...
     * @throws Exception
     * @return array
     */
    function getStateStats($args = false){
        if (isset($args['stateName'])){

            // configurate optional query stuffs
            $yearQuery = '';
            if (isset($args['year'])){
                $yearQuery = " AND year=:year";
                $replacements[':year'] = strtolower($args['year']);
            }
            if (isset($args['under_18'])){
                $yearQuery = " AND under_18=:under_18";
                $replacements[':under_18'] = strtolower($args['under_18']);
            }
            else{
                $yearQuery = " AND under_18=0";
            }

            // configurate query replacements
            $replacements[':state'] = $args['stateName'];

            // build the query
            $query = "
                SELECT
                    `under_18`,
                    `total_arrests`,
                    `total_arrests_per_capita`,
                    `violent_crimes`,
                    `violent_crimes_per_capita`,
                    `property_crimes`,
                    `property_crimes_per_capita`,
                    `murder_and_nonnegligent_manslaughter`,
                    `murder_and_nonnegligent_manslaughter_per_capita`,
                    `forcible_rape`,
                    `forcible_rape_per_capita`,
                    `robbery`,
                    `robbery_per_capita`,
                    `aggravated_assault`,
                    `aggravated_assault_per_capita`,
                    `burglary`,
                    `burglary_per_capita`,
                    `larceny_theft`,
                    `larceny_theft_per_capita`,
                    `motor_vehicle_theft`,
                    `motor_vehicle_theft_per_capita`,
                    `arson`,
                    `arson_per_capita`,
                    `other_assults`,
                    `other_assults_per_capita`,
                    `forgery_and_counterfeiting`,
                    `forgery_and_counterfeiting_per_capita`,
                    `fraud`,
                    `fraud_per_capita`,
                    `embezzlement`,
                    `embezzlement_per_capita`,
                    `stolen_property`,
                    `stolen_property_per_capita`,
                    `vandalism`,
                    `vandalism_per_capita`,
                    `weapons_posession`,
                    `weapons_posession_per_capita`,
                    `prostitution`,
                    `prostitution_per_capita`,
                    `sex_offenses`,
                    `sex_offenses_per_capita`,
                    `drug_abuse_violations`,
                    `drug_abuse_violations_per_capita`,
                    `gambling`,
                    `gambling_per_capita`,
                    `offenses_against_family_and_children`,
                    `offenses_against_family_and_children_per_capita`,
                    `dui`,
                    `dui_per_capita`,
                    `liquor_laws`,
                    `liquor_laws_per_capita`,
                    `drunkenness`,
                    `drunkenness_per_capita`,
                    `disorderly_conduct`,
                    `disorderly_conduct_per_capita`,
                    `vagrancy`,
                    `vagrancy_per_capita`,
                    `all_others`,
                    `all_others_per_capita`,
                    `suspicion`,
                    `suspicion_per_capita`,
                    `curfew_and_loitering`,
                    `curfew_and_loitering_per_capita`,
                    `runaways`,
                    `runaways_per_capita`,
                    `number_of_agencies`,
                    `estimated_population`,
                    `year`
                FROM `{$this->stateCrime}`
                WHERE LOWER(state)=:state $yearQuery
                ORDER BY year ASC
            ";
            $return = $this->connection->fetchAll($query, $replacements);
            return $return;
        }
        return array();
    }

    /**
     * This function returns all of the fbi crime stats for a city by city name
     * @param array $criteria This should be an associative array...
     * @throws Exception
     * @return array
     */
    function getCountryStats($args = false){

        // configurate optional query stuffs
        $replacements = array();
        $yearQuery = '';
        if (isset($args['year'])){
            $yearQuery = " AND year=:year";
            $replacements[':year'] = strtolower($args['year']);
        }

        // build the query
        $query = "
            SELECT
                `total_arrests`,
                `total_arrests_per_capita`,
                `violent_crimes`,
                `violent_crimes_per_capita`,
                `property_crimes`,
                `property_crimes_per_capita`,
                `murder_and_nonnegligent_manslaughter`,
                `murder_and_nonnegligent_manslaughter_per_capita`,
                `forcible_rape`,
                `forcible_rape_per_capita`,
                `robbery`,
                `robbery_per_capita`,
                `aggravated_assault`,
                `aggravated_assault_per_capita`,
                `burglary`,
                `burglary_per_capita`,
                `larceny_theft`,
                `larceny_theft_per_capita`,
                `motor_vehicle_theft`,
                `motor_vehicle_theft_per_capita`,
                `arson`,
                `arson_per_capita`,
                `other_assults`,
                `other_assults_per_capita`,
                `forgery_and_counterfeiting`,
                `forgery_and_counterfeiting_per_capita`,
                `fraud`,
                `fraud_per_capita`,
                `embezzlement`,
                `embezzlement_per_capita`,
                `stolen_property`,
                `stolen_property_per_capita`,
                `vandalism`,
                `vandalism_per_capita`,
                `weapons_posession`,
                `weapons_posession_per_capita`,
                `prostitution`,
                `prostitution_per_capita`,
                `sex_offenses`,
                `sex_offenses_per_capita`,
                `drug_abuse_violations`,
                `drug_abuse_violations_per_capita`,
                `gambling`,
                `gambling_per_capita`,
                `offenses_against_family_and_children`,
                `offenses_against_family_and_children_per_capita`,
                `dui`,
                `dui_per_capita`,
                `liquor_laws`,
                `liquor_laws_per_capita`,
                `drunkenness`,
                `drunkenness_per_capita`,
                `disorderly_conduct`,
                `disorderly_conduct_per_capita`,
                `vagrancy`,
                `vagrancy_per_capita`,
                `all_others`,
                `all_others_per_capita`,
                `suspicion`,
                `suspicion_per_capita`,
                `curfew_and_loitering`,
                `curfew_and_loitering_per_capita`,
                `runaways`,
                `runaways_per_capita`,
                `number_of_agencies`,
                `estimated_population`,
                `year`
            FROM `{$this->countryCrime}`
            WHERE 1=1 $yearQuery
            ORDER BY year ASC
        ";
        $return = $this->connection->fetchAll($query, $replacements);
        return $return;
    }

    /**
     * This function returns fbi data notes, I wish we didn't have to do this.
     * @param array $criteria This should be an associative array...
     * @throws Exception
     * @return array
     */
    function getNotes($args = false){

        // configurate optional query stuffs
        $stateQuery = '';
        if (isset($args['stateName'])){
            $stateQuery = " AND LOWER(state)=:state";
            $replacements[':state'] = strtolower($args['stateName']);
        }

        $cityQuery = '';
        if (isset($args['cityName'])){
            $cityQuery = " AND LOWER(state)=:city";
            $replacements[':city'] = strtolower($args['cityName']);
        }

        $columnQuery = '';
        if (isset($args['columnName'])){
            $columnQuery = " AND LOWER(`column`)=:column";
            $replacements[':column'] = strtolower($args['columnName']);
        }

        $yearQuery = '';
        if (isset($args['year'])){
            $yearQuery = " AND year=:year";
            $replacements[':year'] = strtolower($args['year']);
        }

        $datasetQuery = '';
        if (isset($args['dataset'])){
            $datasetQuery = " AND dataset=:dataset";
            $replacements[':dataset'] = strtolower($args['dataset']);
        }

        if (isset($args['dataset'])){
            // build the query
            $query = "
                SELECT *
                FROM `{$this->notes}`
                WHERE 1=1 $stateQuery $cityQuery $columnQuery $yearQuery $datasetQuery
            ";
            $return = $this->connection->fetchAll($query, $replacements);
            return $return;
        }
        else{
            throw new Icm_Service_Exception("You must provide a dataset to query (options are 'city' or 'state')");
        }

        return array();
    }
}