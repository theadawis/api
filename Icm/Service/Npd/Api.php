<?php

class Icm_Service_Npd_Api implements Icm_Configurable
{
    protected $username, $password, $testmode;

    /**
     * @var Icm_Config
     */
    protected $config;

    protected $http;
    const url = 'http://www.nationalpublicdata.net/feeds/FDSFeed.cfm';

    /**
     * @param string $username
     * @param string $password
     * @param string $testmode true or false
     */
    public function __construct($username = null, $password = null, $testmode = false) {
        $this->username = $username;
        $this->password = $password;
        $this->testmode = $testmode;
    }

    public function domainSearch(array $params) {
        // TODO: our account doesn't seem to support this
        throw new Icm_Exception('Method not implemented yet');
    }

    public function doctorSearch(array $params) {
        // TODO: our account doesn't seem to support this
        throw new Icm_Exception('Method not implemented yet');
    }

    public function birthSearch(array $params) {
        $data = $this->get('GNBTH', $params);
        $return = array();

        if ($data->ResultCount > 0) {

            for ($i = 0; $i < $data->searchResults->BirthSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->BirthSearch->Result[$i])) {
                    $d = $data->searchResults->BirthSearch->Result[$i];
                    $d = array(
                        'first_name' => (string)$d->firstName,
                        'middle_name' => (string)$d->middleName,
                        'last_name' => (string)$d->lastName,
                        'dob' => (string)$d->DOB,
                        'gender' => (string)$d->gender,
                        'birth_county' => (string)$d->birthCounty,
                        'state' => (string)$d->state
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Birthrecord::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    /**
     * @param array $params array('firstName' => required, 'lastName' => required, 'state' => optional, 'county' => optional, but state is required if county is used)
     * @return multitype:Icm_Struct |NULL
     */
    public function marriageSearch(array $params) {
        $defaultParams = array('firstName' => NULL, 'lastName' => NULL, 'state' => NULL, 'county' => NULL);
        $params = array_merge($defaultParams, $params);
        $data = $this->get('MARDV', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->MarriageSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->MarriageSearch->Result[$i])) {
                    $d = $data->searchResults->MarriageSearch->Result[$i];
                    $d = array(
                        'certificate_number' => (string)$d->certificateNumber,
                        'volume_number' => (string)$d->volumeNumber,
                        'first_person_first_name' => (string)$d->FirstPersonFirst,
                        'first_person_middle_name' => (string)$d->FirstPersonMid,
                        'first_person_last_name' => (string)$d->FirstPersonLast,
                        'second_person_first_name' => (string)$d->SecondPersonFirst,
                        'second_person_middle_name' => (string)$d->SecondPersonMid,
                        'second_person_last_name' => (string)$d->SecondPersonLast,
                        'county' => (string)$d->marriageCounty,
                        'state' => (string)$d->state,
                        'date' => (string)$d->MarriageDate
                    );
                    $d['date'] = str_replace('~', '', $d['date']); // c'mon, NPD...

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Marriagerecord::fromArray($d);
                }
            }

            for ($i = 0; $i < $data->searchResults->DivorceSearch->Result->count(); $i++) {

            }

            return $return;
        }

        return NULL;
    }

    /**
     * @param array $params array('firstName' => required, 'lastName' => required, 'state' => optional, 'county' => optional, but state is required if county is used)
     * @return multitype:Icm_Struct |NULL
     */
    public function divorceSearch(array $params) {
        $defaultParams = array('firstName' => NULL, 'lastName' => NULL, 'state' => NULL, 'county' => NULL);
        $params = array_merge($defaultParams, $params);
        $data = $this->get('MARDV', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->DivorceSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->DivorceSearch->Result[$i])) {
                    $d = $data->searchResults->DivorceSearch->Result[$i];
                    $d = array(
                        'certificate_number' => (string)$d->certificateNumber,
                        'volume_number' => (string)$d->volumeNumber,
                        'first_person_first_name' => (string)$d->FirstPersonFirst,
                        'first_person_middle_name' => (string)$d->FirstPersonMid,
                        'first_person_last_name' => (string)$d->FirstPersonLast,
                        'second_person_first_name' => (string)$d->SecondPersonFirst,
                        'second_person_middle_name' => (string)$d->SecondPersonMid,
                        'second_person_last_name' => (string)$d->SecondPersonLast,
                        'county' => (string)$d->divorceCounty,
                        'state' => (string)$d->state,
                        'date' => (string)$d->DivorceDate
                    );

                    $d['date'] = str_replace('~', '', $d['date']); // c'mon, NPD...

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Marriagerecord::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    /**
     * @param array $params array('phonenumber' => 10 digit number)
     */
    public function reversePhone(array $params) {
        $data = $this->get('PFPHN', $params);
        $return = array();

        if ($data->ResultCount > 0) {

            for ($i = 0; $i < $data->searchResults->PeopleFinderbyAddress->Result->count(); $i++ ) {

                if (!empty($data->searchResults->PeopleFinderbyAddress->Result[$i]->LastName)) {
                    $d = $data->searchResults->PeopleFinderbyAddress->Result[$i];
                    $d = array(
                        'firstname' => (string) $d->FirstName,
                        'lastname' => (string) $d->LastName,
                        'middlename' => (string) $d->MiddleName,
                        'dob' => (string) $d->DOB,
                        'address' => (string) $d->Address,
                        'county' => (string) $d->City,
                        'state' => (string) $d->State,
                        'zip' => (string) $d->Zip,
                        'phone' => (string) $d->Phone,
                        'personid' => (string) $d->personID
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_People::fromArray($d);
                }
            }
        }

        return $return;
    }

    /**
     * Performs a NPD criminal record search
     * @param array $params array('firstName' => 'bob', 'lastName' => 'smith', 'DOB' => '12/31/1950', 'state' => 'CA')
     * @return multitype:NULL
     */
    public function criminalSearch(array $params) {
        $data = $this->get('CRSOR', $params);
        $return = array();

        if ($data->ResultCount > 0) {

            for ($i = 0; $i < $data->searchResults->CriminalSearch->Result->count(); $i++) {

                // only add return data if it's an actual record
                if (!empty($data->searchResults->CriminalSearch->Result[$i]->lastname)) {
                    $d = $data->searchResults->CriminalSearch->Result[$i];
                    $d = array(
                        'first_name' => (string)$d->firstname,
                        'middle_name' => (string)$d->middlename,
                        'last_name' => (string)$d->lastname,
                        'generation' => (string)$d->generation,
                        'dob' => (string)$d->dob,
                        'birth_state' => (string)$d->birthState,
                        'age' => (string)$d->age,
                        'case_number' => (string)$d->casenumber,
                        'aka1' => (string)$d->AKA1,
                        'aka2' => (string)$d->AKA2,
                        'dob_aka' => (string)$d->DOBAKA,
                        'address' => (string)$d->address,
                        'address2' => (string)$d->address2,
                        'city' => (string)$d->city,
                        'state' => (string)$d->state,
                        'zip' => (string)$d->zip,
                        'hair_color' => (string)$d->haircolor,
                        'eye_color' => (string)$d->eyecolor,
                        'height' => (string)$d->height,
                        'weight' => (string)$d->weight,
                        'race' => (string)$d->race,
                        'sex' => (string)$d->sex,
                        'skin_tone' => (string)$d->skintone,
                        'scars_marks' => (string)$d->scarsmarks,
                        'military_service' => (string)$d->militaryservice,
                        'charge_category' => (string)$d->ChargeCategory,
                        'charges_filed_date' => (string)$d->ChargesFiledDate,
                        'offense_date' => (string)$d->OffenseDate,
                        'offense_code' => (string)$d->OffenseCode,
                        'offense_description1' => (string)$d->offensedescription1,
                        'offense_description2' => (string)$d->offensedescription2,
                        'ncic_code' => (string)$d->NCICCode,
                        'counts' => (string)$d->Counts,
                        'plea' => (string)$d->Plea,
                        'conviction_date' => (string)$d->ConvictionDate,
                        'conviction_place' => (string)$d->ConvictionPlace,
                        'court' => (string)$d->court,
                        'source' => (string)$d->source,
                        'sentence_date' => (string)$d->SentenceYYYMMDDD,
                        'probation_date' => (string)$d->ProbationYYYMMDDD,
                        'disposition' => (string)$d->Disposition,
                        'disposition_date' => (string)$d->Dispositiondate,
                        'court_costs' => (string)$d->CourtCosts,
                        'arresting_agency' => (string)$d->ArrestingAgency,
                        'case_type' => (string)$d->caseType,
                        'fines' => (string)$d->fines,
                        'source_name' => (string)$d->SourceName,
                        'source_state' => (string)$d->SourceState
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Criminalrecord::fromArray($d);
                }
            }
        }

        return $return;
    }

    public function sexOffenderSearch(array $params) {
        $data = $this->get('CTSOR', $params);
        $return = array();

        if ($data->ResultCount > 0) {

            for ($i = 0; $i < $data->searchResults->SORSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->SORSearch->Result[$i])) {
                    $d = $data->searchResults->SORSearch->Result[$i];

                    $d = array(
                        'first_name' => (string)$d->firstname,
                        'last_name' => (string)$d->lastname,
                        'middle_name' => (string)$d->middlename,
                        'suffix' => (string)$d->suffix,
                        'image' => (string)$d->image,
                        'dob' => (string)$d->dob,
                        'case_number' => (string)$d->casenumber,
                        'risk_level' => (string)$d->risklevel,
                        'aliases' => (string)$d->Aliases,
                        'address' => (string)$d->address,
                        'address2' => (string)$d->address2,
                        'city' => (string)$d->city,
                        'state' => (string)$d->state,
                        'zip' => (string)$d->zip,
                        'county' => (string)$d->county,
                        'offender_status' => (string)$d->offenderstatus,
                        'offender_category' => (string)$d->offendercategory,
                        'hair_color' => (string)$d->haircolor,
                        'eye_color' => (string)$d->eyecolor,
                        'height' => (string)$d->height,
                        'weight' => (string)$d->weight,
                        'race' => (string)$d->race,
                        'sex' => (string)$d->sex,
                        'skin_tone' => (string)$d->skintone,
                        'scars_marks' => (string)$d->scarsmarks,
                        'offense_date' => (string)$d->offensedate,
                        'offense_code' => (string)$d->offensecode,
                        'offense_description1' => (string)$d->offensedescription1,
                        'offense_description2' => (string)$d->offensedescription2,
                        'counts' => (string)$d->counts,
                        'conviction_date' => (string)$d->convictiondate,
                        'conviction_place' => (string)$d->convictionplace,
                        'victims_age' => (string)$d->victimsage,
                        'victim_minor_flag' => (string)$d->victimminorflag,
                        'victim_sex' => (string)$d->victimsex,
                        'court' => (string)$d->court,
                        'source' => (string)$d->source
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Sexoffender::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    public function terroristSearch(array $params) {
        // TODO: no access
        throw new Icm_Exception('Method not implemented');
    }

    public function merchantVesselSearch(array $params) {
        // TODO: error returned
        throw new Icm_Exception('Method not implemented');
    }

    public function aircraftSearch(array $params) {
        throw new Icm_Exception('Method not implemented');
    }

    /**
     * Params Firstname and Lastname are required, FAANumber is optional
     * @param array $params array('Firstname' => string, 'Lastname' => string, 'FAANumber' => string)
     */
    public function faaLicenseSearch(array $params) {
        $defaultParams = array('Firstname' => NULL, 'Lastname' => NULL, 'FAANumber' => NULL);
        $params = array_merge($defaultParams, $params);
        $data = $this->get('LCFAA', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->FAALicenseSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->FAALicenseSearch->Result[$i])) {
                    $d = $data->searchResults->FAALicenseSearch->Result[$i];
                    $d = array(
                        'first_name' => (string)$d->firstname,
                        'middle_name' => (string)$d->middlename,
                        'last_name' => (string)$d->lastname,
                        'address' => (string)$d->address,
                        'address2' => (string)$d->address2,
                        'city' => (string)$d->city,
                        'state' => (string)$d->state,
                        'zip' => (string)$d->zip,
                        'faa_number' => (string)$d->FAANumber,
                        'medical_exp_date' => (string)$d->medicalexpdate,
                        'certifications' => $d->Certifications
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Faalicense::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    /**
     * NPD Death Search <br/>
     * Note: all "optional" params MUST be present, even if their values are NULL
     * @param array $params array('firstName' => required, 'lastName' => required, 'ssn' => optional, 'dob' => optional)
     * @return multitype:Icm_Struct |NULL
     */
    public function deathSearch(array $params) {
        $data = $this->get('GNDTH', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->DeathSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->DeathSearch->Result[$i])) {
                    $d = $data->searchResults->DeathSearch->Result[$i];
                    $d = array(
                        'first_name' => (string)$d->firstname,
                        'middle_name' => (string)$d->middlename,
                        'last_name' => (string)$d->lastname,
                        'dob' => (string)$d->DateofBirth,
                        'dod' => (string)$d->DateofDeath,
                        'suffix' => (string)$d->suffix,
                        'last_county' => (string)$d->lastcounty,
                        'state' => (string)$d->State,
                        'ssn' => (string)$d->SSN
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Deathrecord::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    /**
     * NPD DEA License search
     * @param array $params array('Deanumber' => optional, 'firstName' => required, 'lastName' => required, 'middlename' => optional, 'city' => optional, 'state' => optional)
     * @return multitype:Icm_Struct |NULL
     */
    public function deaSearch(array $params) {
        $defaultParams = array('Deanumber' => NULL, 'firstName' => NULL, 'lastName' => NULL, 'middlename' => NULL, 'city' => NULL, 'state' => NULL);
        $params = array_merge($defaultParams, $params);
        $data = $this->get('LCDEA', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->DEAlicenseSearch->Result->count(); $i++) {

                if (!empty($data->searchResults->DEAlicenseSearch->Result[$i])) {
                    $d = $data->searchResults->DEAlicenseSearch->Result[$i];
                    $d = array(
                        'dea_number' => (string)$d->deanumber,
                        'business_type' => (string)$d->businesstype,
                        'schedules' => (string)$d->schedules,
                        'expiration_date' => (string)$d->expirationdate,
                        'last_name' => (string)$d->lastname,
                        'first_name' => (string)$d->firstname,
                        'address' => (string)$d->address,
                        'city' => (string)$d->city,
                        'state' => (string)$d->state,
                        'zip' => (string)$d->zip,
                        'county' => (string)$d->county
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Dealicense::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    public function businessSearch(array $params) {
        // TODO: no access
        throw new Icm_Exception('Method is not implemented');
    }

    public function whitePagesSearch(array $params) {
        // TODO: no access
        throw new Icm_Exception('Method is not implemented');
    }

    /**
     * Performs a cell phone record search. Only first + last name OR phone are required, but all params MUST be defined
     * @param array $params array('firstname' => string, 'lastname' => string, 'state' => string, 'phone' => string)
     * @return multitype:Icm_Struct |NULL
     */
    public function cellSearch(array $params) {
        $defaultParams = array('firstname' => NULL, 'lastname' => NULL, 'state' => NULL, 'phone' => NULL);
        $params = array_merge($defaultParams, $params);
        $data = $this->get('CELTS', $params);

        if ($data->ResultCount > 0) {
            $return = array();

            for ($i = 0; $i < $data->searchResults->Cellphonesearch->Result->count(); $i++) {

                if (!empty($data->searchResults->Cellphonesearch->Result[$i])) {
                    $d = $data->searchResults->Cellphonesearch->Result[$i];
                    $d = array(
                        'first_name' => (string)$d->firstname,
                        'last_name' => (string)$d->lastname,
                        'address' => (string)$d->address,
                        'city' => (string)$d->city,
                        'state' => (string)$d->state,
                        'zip' => (string)$d->zip,
                        'phone' => (string)$d->phone,
                        'provider' => (string)$d->provider,
                        'type' => (string)$d->type
                    );

                    foreach ($d as &$element) {
                        $element = trim($element);
                    }

                    $return[] = Icm_Struct_Cellphone::fromArray($d);
                }
            }

            return $return;
        }

        return NULL;
    }

    /**
     * Build XML request for NPD API operation
     * @param string $searchType see NPD docs for available search types
     * @param array $params associative array of search parameters array('name' => value)
     * @param int $detail 0 or 1; not used for all search types; see NPD docs
     * @return string
     */
    protected function buildXMLRequest($searchType, array $params, $detail = 1) {
        $searchParams = '';

        foreach ($params as $name  =>  $value) {
            $searchParams .= "<$name>".strtoupper($value)."</$name>";
        }

        $query = '
            <FDSRequest>
            <username>' . $this->username.'</username>
            <password>' . $this->password.'</password>
            <sType>' . $searchType.'</sType>
            <detail>' . $detail.'</detail>
            <testMode>' . $this->testmode.'</testMode>
            <searchParams>
            ' . $searchParams.'
            </searchParams>
            </FDSRequest>';

        return $query;
    }

    /**
     * Perform a NPD API operation
     * @param string $searchType see NPD docs for available search types
     * @param array $params associative array of search parameters array('name' => value)
     * @param int $detail 0 or 1; not used for all search types; see NPD docs
     * @return SimpleXMLElement
     */
    public function get($searchType, $params, $detail = 1) {
        $this->getClient()->setUrl(self::url);
        $result = $this->getClient()->post(array('xml' => $this->buildXMLRequest($searchType, $params, $detail)));
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($result);
        $result = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $result); // NPD sometimes returns non-ASCII filth; cleanse it

        $resultParsed = simplexml_load_string(trim($result));

        if (!$resultParsed) {
            $this->_throwXmlException();
        }

        return $resultParsed;
    }

    /**
     * Get an instance of the HTTP Client
     * @return Icm_Util_Http_Client
     */
    public function getClient() {
        if (is_null($this->http)) {
            $this->setClient(new Icm_Util_Http_Client());
        }

        return $this->http;
    }

    /**
     * Set an instance of the HTTP Client
     * @param Icm_Util_Http_Client $client
     */
    public function setClient(Icm_Util_Http_Client $client) {
        $this->http = $client;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        $this->username = $this->config->getOption('username');
        $this->password = $this->config->getOption('password');
    }
}
