<?php

/**
 *
 */
class Icm_Service_Npd_Db implements Icm_Configurable{

    protected $_cache;
    protected $config;

    protected static $fieldNameMap = array(
       'first_name'    => 'FirstName',
       'last_name'     => 'LastName',
       'validity_date' => 'ValidityDate',
       'middle_name'   => 'MiddleName',
       'dob'           => 'DateOfBirth',
       'address'       => 'address',
       'city'          => 'city',
       'state'         => 'state',
       'zip'           => 'zip',
       'county_name'   => 'countyName',
       'phone_number'  => 'phoneNumber',
       'person_id'     => 'ID',
   );

    protected static $rafieldNameMap = array(
            'first_name'    => 'firstName',
            'last_name'     => 'lastName',
            'validity_date' => 'validityDate',
            'middle_name'   => 'middleName',
            'dob'           => 'dateOfBirth',
            'address'       => 'address',
            'city'          => 'city',
            'state'         => 'state',
            'zip'           => 'zip',
            'county_name'   => 'countyName',
            'phone'  => 'phonenumber',
            'person_id'     => 'id',
    );


    public function getConfig(){
        return $this->config;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
    }

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct($conn = NULL) {

        $this->conn = $conn;

    }


    /**
     * @static
     * @return string
     */
    private function personSelect() {
        $str = 'SELECT ';
        if ($this->conn->maxResults()) {
            $str .= ' TOP ' . $this->conn->maxResults();
        }
        $str .= ' ValidityDate,
              LastName,
              FirstName,
              MiddleName,
              DateOfBirth,
              address,
              city,
              state,
              zip,
              countyName,
              phoneNumber,
              ID';

        return $str;
    }

    // args contain where clause criteria
    /**
     * @param array $args
     * @return array
     *
     * $args should contain:
     * - first_name
     * - last_name
     * - state
     */
    public function getPerson($args=array()) {
        // this function requires a last_name
        if (!isset($args['last_name']) || strlen($args['last_name']) < 1) {
            throw new Icm_Service_Exception('last_name is required.');
        }

        if (count($args) < 2) {
            throw new Icm_Service_Exception('last_name and at least 1 other criterion is required.');
        }

        $sql = $this->personSelect();
        // determine table name (npd person tables are sharded by 1st letter of last name)
        $table = 'PF_' . strtoupper(substr($args['last_name'], 0, 1));
        $sql .= " FROM $table WHERE ";

        $cnt = 0;
        $bindings = array();
        foreach ($args as $key => $value) {
            $cnt++;
            $k      = self::$fieldNameMap[$key];
            $parsed = Icm_Db::parseQueryString($value);

            $sql .= $k . $parsed[0] . ':' . $k;

            $bindings[$k] = $parsed[1];
            if ($cnt < count($args)) {
                $sql .= ' AND ';
            }
        }

        // order the results - TODO: this should be passed in as args
        $sql .= ' ORDER BY DateOfBirth DESC, city ASC, ValidityDate ASC';

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug(json_encode($bindings));

        $fieldNameMap = array_flip(self::$fieldNameMap);

        $callback = function($row) use ($fieldNameMap) {
            $remappedRow = array();
            foreach ($row as $key => $value) {
                $remappedRow[$fieldNameMap[$key]] = $value;
            }
            return $remappedRow;
        };

        $result = $this->conn->fetchAll($sql, $bindings, $callback);
        return $result;
    }

    public function reverseAddress(array $args = array()){
        $address = $args['address'];
        $city = $args['city'];
        $state = $args['state'];
        $zip = $args['zip'];

        foreach ($args as $arg){
            if (is_null($arg) || ctype_space($arg) || empty($arg)){
                return array();
            }
        }

        $fieldNameMap = array_flip(self::$rafieldNameMap);

        $callback = function($row) use ($fieldNameMap) {
            $remappedRow = array();
            foreach ($row as $key => $value) {
                $remappedRow[$fieldNameMap[$key]] = $value;
            }
            return $remappedRow;
        };

        $query = sprintf('EXEC sp_addressSearch2 "%s", "%s", "%s", "%s"', $address, $city, $state, substr($zip, 0, 5));
        // echo $query.'<br/>';
        if ($results = $this->conn->fetchall($query, array(), $callback)){
            return $results;
        }
        else{
            return array();
        }
    }

    public function sexOffenderSearch(array $args){
        $query = "SELECT * FROM aprilsormain WHERE ";
        $and = '';
        $replacements = array();
        foreach ($args as $key => $value){
            $query .= "$and$key = :$key";
            $and = ' AND ';
            $replacements[$key] = $value;
        }
        $query .= ';';

        $callback = function($row){
            $d = array(
                    'first_name' => $row['firstname'],
                    'last_name' => $row['lastname'],
                    'middle_name' => $row['middlename'],
                    'suffix' => $row['suffix'],
                    'image' => $row['imagehyperlink'],
                    'dob' => $row['dob'],
                    'case_number' => $row['casenumber'],
                    'risk_level' => $row['risklevel'],
                    // 'aliases' => $row['Aliases'],
                    'address' => $row['address'],
                    'address2' => $row['address2'],
                    'city' => $row['city'],
                    'state' => $row['state'],
                    'zip' => $row['zip'],
                    'county' => $row['county'],
                    'offender_status' => $row['offenderstatus'],
                    'offender_category' => $row['offendercatagory'], //catagory...  ::facepalm::
                    'hair_color' => $row['haircolor'],
                    'eye_color' => $row['eyecolor'],
                    'height' => $row['height'],
                    'weight' => $row['weight'],
                    'race' => $row['race'],
                    'sex' => $row['sex'],
                    'skin_tone' => $row['skintone'],
                    'scars_marks' => $row['scarsmarks'],
                    'offense_date' => $row['offensedate'],
                    'offense_code' => $row['offensecode'],
                    'offense_description1' => $row['offensedescription1'],
                    'offense_description2' => $row['offensedescription2'],
                    'counts' => $row['counts'],
                    'conviction_date' => $row['convictiondate'],
                    'conviction_place' => $row['convictionplace'],
                    'victims_age' => $row['victimsage'],
                    'victim_minor_flag' => $row['victimminorflag'],
                    'victim_sex' => $row['victimsex'],
                    'court' => $row['court'],
                    'source' => $row['source']
            );
            // $d['image'] = str_replace('sorpix/', 'sxo/', $d['image']);
            if (isset($d['image']) && trim($d['image']) != ''){
                $d['image'] = 'http://www.recordscheck.net/' . $d['image'];
            }
            return $d;
        };

        if ($data = $this->conn->fetchAll($query, $replacements, $callback)){
            return $data;
        }
        return array();
    }
}
