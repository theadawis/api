<?php

class Icm_Service_Census_Db {
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    public function __construct(Icm_Db_Pdo $db){
        $this->db = $db;
    }

    public function getByZip($zip){
        $sql = sprintf("SELECT
                Population,
                PersonsPerHousehold,
                AverageHouseValue,
                MedianAge,
                Latitude,
                Longitude
            FROM
              data_census
            WHERE
              ZipCode = :zip
            LIMIT 1", array(':zip' => $zip));

        return $this->db->fetchAll($sql, array(':zip' => $zip));
    }
}
