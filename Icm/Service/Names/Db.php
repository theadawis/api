<?php
/**
 * @author Joe Linn
 */
class Icm_Service_Names_Db{
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    public function __construct(Icm_Db_Pdo $db){
        $this->db = $db;
    }

    /**
     * @param string $name
     * @return string m, f, or u
     */
    public function getGender($name){
        $gender = $this->db->fetch("SELECT gender FROM pd_gender WHERE name=:name;", array(':name' => $name));
        if (!$gender){
            return 'u'; //name not in DB
        }
        return strtolower($gender['gender']);
    }

    public function getGenders(array $names){
        if (sizeof($names)){
            $qmarks = array_fill(0, sizeof($names), '?');
            $genders = $this->db->fetchAll("SELECT name, gender FROM pd_gender WHERE name IN (".implode(', ', $qmarks).");", $names);
            $return = array();
            foreach ($genders as $gender){
                $return[strtolower($gender['name'])] = strtolower($gender['gender']);
            }
            foreach ($names as $name){
                if (!isset($return[strtolower($name)])){
                    $return[strtolower($name)] = 'u';
                }
            }
            return $return;
        }
        return array();
    }

    public function getFullName($nick){
        $name = $this->db->fetchAll("SELECT name FROM pd_nickname WHERE variation=:nick;", array(':nick' => $nick));
        if (!$name){
            return $name;
        }
        foreach ($name as &$value){
            $value = strtolower($value['name']);
        }
        return $name;
    }
}