<?php

class Icm_Service_Reversephone_Db {
    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    public function __construct(Icm_Db_Pdo $db){
        $this->db = $db;
    }

    public function getByPhone($phone) {
        $parts = $this->getPhoneNumberParts($phone);
        $row = $this->db->fetch("
            SELECT
                NPA.ZIP,
                NPA.LTYPE,
                NPA.STATE,
                OCN.CommonName as CARRIER
            FROM
                data_phone_npanxx NPA
            LEFT JOIN
                data_phone_ocn OCN
            ON
                NPA.OCN = OCN.OCN
            WHERE
                NPA.NPA = :NPA
            AND
                NPA.NXX =:NXX
            AND
                (NPA.BLOCK_ID = :BLOCK_ID OR NPA.BLOCK_ID = 'A')
        ", $parts);

        if ($row) {
            switch(strtoupper($row['LTYPE'])) {
                case 'S':
                    $ltype = 'Land Line';
                    break;
                case 'C':
                    $ltype = 'Cell Phone';
                    break;
                case 'P':
                    $ltype = 'Pager';
                    break;
                case 'M':
                default:
                    $ltype = 'Wireless or Land Line';
                    break;
            }

            $row['ltype'] = $ltype;
            $row['phone_number'] = $phone;

            return $this->buildEntity($row);
        }
        else{
            return NULL;
        }
    }

    private function buildEntity($row){
        $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('phone')->create();

        foreach ($row as $k => $v){
            $entity->set(strtolower($k), $v);
        }

        return $entity;
    }

    public function getPhoneNumberParts($phone) {
        $phone_number = $this->strip_phone_formatting($phone);
        $parts = array();
        preg_match(':([2-9][0-9][0-9])([0-9]{3})([0-9]):', $phone, $parts);

        if (!$parts){
            return false;
        }

        return array(
            'NPA' => $parts[1],
            'NXX' => $parts[2],
            'BLOCK_ID' => $parts[3]
        );
    }

    protected function strip_phone_formatting($phoneNumber) {
        foreach (array('(', ')', '-', ' ') as $char) {
            $phoneNumber = str_replace($char, '', $phoneNumber);
        }

        return $phoneNumber;
    }

    /**
     */
    public function getByZip($zip){
        $sql = sprintf("SELECT
                Population,
                PersonsPerHousehold,
                AverageHouseValue,
                MedianAge,
                Latitude,
                Longitude
            FROM
              data_census
            WHERE
              ZipCode = :zip
            LIMIT 1", array(':zip' => $zip));

        return $this->db->fetchAll($sql, array(':zip' => $zip));
    }
}
