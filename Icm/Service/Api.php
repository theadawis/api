<?php
abstract class Icm_Service_Api{
    /**
     * @var Icm_Util_Http_Client
     */
    protected $httpClient;

    /**
     * Set http client object
     * @param Icm_Util_Http_Client $httpClient
     */
    public function setHttpClient(Icm_Util_Http_Client $httpClient) {
        $this->httpClient = $httpClient;
    }

    /**
     * @return Icm_Util_Http_Client
     */
    public function getHttpClient() {
        if (is_null($this->httpClient)) {
            $this->httpClient = new Icm_Util_Http_Client();
        }
        return $this->httpClient;
    }
}
