<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/8/12
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Service_Lexis_Teaser extends Icm_Service_Lexis_Rest
{

    const BASEURL = "http://thinteaser.edata.com/WsAccurint/";
    const PORT = 8001;

    /**
     * Performs a background check teaser search
     *
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function thinRollupPersonSearch($searchBy = array(), $options = array()) {
        $params = array_merge(
            $this->arrayToParams('SearchBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            )),
            array(
                'ver_' => '1.65'
            )
        );
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug('PARAMS: '. print_r($params, true));
        return $this->get('ThinRollupPersonSearch', $params);

    }

}
