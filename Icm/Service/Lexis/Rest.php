<?php
class Icm_Service_Lexis_Rest implements Icm_Configurable
{
    /**
     * @var Icm_Util_Http_Client $http
     */
    protected $http;

    protected $username;
    protected $password;
    protected $config;
    protected $storage = array();

    const BASEURL = 'https://wsonline.seisint.com/WsAccurint/';
    const PORT = 8000;

    public function __construct($username = NULL, $password = NULL){
        libxml_use_internal_errors(true);
        $this->setUsername($username);
        $this->setPassword($password);
    }

    /**
     * Perform a reverse phone search.  Does not include wireless numbers
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function directoryAssistanceReverseSearch($searchBy = array(), $options = array()) {
        $params = array_merge(
            $this->arrayToParams('SearchBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            ))
        );

        return $this->get('DirectoryAssistanceReverseSearch', $params);

    }

    /**
     * Performs a reverse search, includes wireless results.  See documentation for possible search clauses and options
     *
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function directoryAssistanceWirelessSearch($searchBy = array(), $options = array()) {
        $params = array_merge(
            $this->arrayToParams('SearchBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            ))
        );

        return $this->get('DirectoryAssistanceWirelessSearch', $params);

    }

    /**
     * Performs a Lexis BpsSearch.  See Lexis documentation for possible search clauses and options
     * @param array $searchBy
     * @param array $options
     * @param array $name
     * @return SimpleXMLElement
     */
    function personSearch($searchBy = array(), $options = array()){
        $params = array_merge(
                $this->arrayToParams('SearchBy', $searchBy),
                $this->arrayToParams('Options', $options),
                // $this->arrayToParams('Name', $name),
                $this->arrayToParams('User', array(
                        'GLBPurpose' => 0,
                        'DLPurpose' => 0
                )),
                array(
                    'ver_' => '1.65'
                )
        );

        return $this->get('BpsSearch', $params);
    }

    /**
     * Run a full-on Lexis comprehensive report
     * @param array $searchBy
     * @param array $options
     * @return SimpleXMLElement
     */
    public function comprehensiveSearch(array $searchBy, array $options = array()){
        $params = array_merge(
            $this->arrayToParams('ReportBy', $searchBy),
            $this->arrayToParams('Options', $options),
            $this->arrayToParams('User', array(
                'GLBPurpose' => 0,
                'DLPurpose' => 0
            )),
            array(
                'ver_' => '1.65'
            )
        );
        return $this->get('CompReport', $params);
    }

    /**
     * Execute the request
     * @param $func
     * @param $params
     * @return SimpleXMLElement
     */
    protected function get($func, $params) {
        $url = $this->getUrl();
        $result = $index = $key = null;

        // check for an existing cache
        if ($this->getConfig()->getOption("enableStorage")) {

          // generate the cache key
          if ($func == 'CompReport'  && isset($params['ReportBy.UniqueId'])){
              $key = $params['ReportBy.UniqueId'];
              $index = "comprehensive";
          }
          else if ($func == 'BpsSearch'  && isset($params['SearchBy.UniqueId'])){
              $key = $params['SearchBy.UniqueId'];
              $index = "basic";
          }
          
          if (!is_null($key) && $this->getStorage($index)->test($key)) {
              // get the result from storage
              $result = $this->getStorage($index)->load($key);
          }

        }

        if (is_null($result)) {
            // $url = static::BASEURL . $func . '?' . http_build_query($params, '', '&');
            // echo $url;
            $this->getClient()->setUrl($url . $func, $params);
            $result = $this->getClient()->post();

            // cache if necessary
            if ($this->getConfig()->getOption("enableStorage")) {

                try {
                    $this->getStorage($index)->save($result, $key);
                } catch (Exception $e) {
                    Icm_Util_Logger_Syslog::getInstance('api')->logErr('failed to store lexis response: ' . $e->getMessage());
                }
            }
        }
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($result);

        $resultParsed = simplexml_load_string($result);
        if (!$resultParsed){
            // var_dump($result);
            $this->_throwXmlException();
        }
        return $resultParsed;
    }

    /**
     * Convert an array to Lexis Nexis's url notation
     * @param $glue
     * @param $array
     * @return array
     */
    protected function arrayToParams($glue, $array) {
        $params = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $params += $this->arrayToParams("{$glue}.{$key}", $value);
            } else {
                $params["{$glue}.$key"] = $value;
            }
        }
        return $params;
    }

    /**
     * Get an instance of the HTTP Client
     * @return Icm_Util_Http_Client
     */
    public function getClient() {
        if (is_null($this->http)) {
            $this->setClient(new Icm_Util_Http_Client());
        }
        return $this->http;
    }

    /**
     * Set an instance of the HTTP Client
     * @param Icm_Util_Http_Client $client
     */
    public function setClient(Icm_Util_Http_Client $client) {
        $this->http = $client;
        $this->http->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $this->http->setOption(CURLOPT_USERPWD, $this->getUsername() . ':' . $this->getPassword());
    }

    /**
     * @param $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }

    /**
     * @param $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    protected function _throwXmlException(){
        $errs = array();
        foreach (libxml_get_errors() as $err) {
            $errs[] = $err->message;
        }
        throw new Icm_Service_Lexis_Exception(implode('\n', $errs));
    }

    public function getUrl() {
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug("TRYING URL: " . static::BASEURL);
        $proxy = $this->config->getOption('proxy', true);
        if ($proxy === 'stage'){
            return 'mgmt.prod.instantcheckmate.com:'.static::PORT.'/';
        }
        else if ($proxy){
            return 'web0'.mt_rand(1, 8) . '.pub.instantcheckmate.com:'.static::PORT.'/';
        }
        return static::BASEURL;
    }

    public function getStorage($name) {
        if (isset($this->storage[$name])) {
            return $this->storage[$name];
        }
        throw new Icm_Service_Lexis_Storage_Exception("tried to getStorage($name) but storage $name is not set");
    }

    public function setStorage(Icm_Service_Lexis_Storage_Interface $storage) {
        $this->storage[$storage->getIndex()] = $storage;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        $this->setUsername($this->config->getOption('username'));
        $this->setPassword($this->config->getOption('password'));
    }

}
