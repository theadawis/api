<?php
/**
 * User: tom
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Service_Lexis_Storage_Interface
{
    public function save($raw, $key);
    public function test($key);
    public function load($key);
}
