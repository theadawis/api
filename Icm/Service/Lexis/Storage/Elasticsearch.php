<?php
/**
 * Store comprehensive reports in an es table.  We store by key (personID), attach the raw data as XML (base64)
 *
 * @category: Icm_Service_Lexis_Storage_Elasticsearch
 * @author: Shiem Edelbrock
 *
 */

class Icm_Service_Lexis_Storage_Elasticsearch implements Icm_Service_Lexis_Storage_Interface
{
    private $documentType;

    /**
     * __Construct
     * 
     *  @param Icm_Config $config - Config containg ES hosts
     *  @param string     $index  - ES Index for this instance
     *
     *  @throws Icm_Service_Exception - if index is not passed set
     *
     */
    public function __construct(Icm_Config $config, $documentType){
        if (is_null($documentType)) {
            throw new Icm_Service_Exception("Document type must be set");
        }

        $this->documentType = $documentType;
        $configHosts = array();

        foreach ($config as $host) {
            $host = explode(':', $host);
            $configHosts[] = array('host' => $host[0], 'port' => $host[1]);
        }

        $this->elasticsearch = new Icm_Service_Internal_Elasticsearch_Client(
            array('servers' => $configHosts, 
                  'timeout' => $config->getOption('timeout', 10)
              )
        );
    }

    /**
     * save
     * 
     *  @param string $raw - raw XML as a string
     *  @param string $key - the person ID for the attached data
     *
     *  @return bool
     *
     *  @throws Icm_Service_Exception - if key could not be saved
     *
     */
    public function save($raw, $key) {
        if ($this->test($key, $this->documentType)) {
            throw new Icm_Service_Exception("Key: $key already exists in " . $this->documentType . " Storage.");
        }
    
        $xml = simplexml_load_string($raw);

        $type = $this->elasticsearch->getIndex('report')
            ->getType($this->documentType);

        $person = array();

        // We don't want Elasticsearch to index a comprehensive report due to type conflicts
        // so we let give it have an empty array
        if($this->documentType != 'comprehensive'){
            $person = json_decode(json_encode($xml), true);
        }

        $doc = new Elastica_Document((string) $key, $person);
        $doc->addFileContent("raw_response", $raw);
        $type->addDocument($doc);

        // refresh the index
        $this->elasticsearch->getIndex('report')
            ->refresh();

        return true;
    }

    /**
     * Test (test to see if a given key exsists)
     * 
     *  @param string $key - the person ID to search for
     *
     *  @return bool       - true if found, false if absent
     */
    public function test($key){
        $query = new Elastica_Query_Builder('{
                "fields": "_id",
                    "query": {
                        "bool": {
                            "must": [{
                                "query_string": {
                                    "default_field": "_id",
                                    "query": "' . $key . '"
                                }
                            }]
                        }
                    },
                "size": 1
            }'
        );

        $query = new Elastica_Query($query->toArray());
        $search = new Elastica_Search($this->elasticsearch);

        $results = $search->addIndex('report')
            ->addType($this->documentType)
            ->search($query);

        return ($results->_totalHits > 0);
    }

    /**
     * load - Load raw XML from Elastic Search by given person id
     * 
     *  @param string $key - PersonID
     *
     *  @return string     - Raw XML data
     *  
     */
    public function load($key){
        $query = new Elastica_Query_Builder('{
                "fields": "raw_response",
                    "query": {
                        "bool": {
                           "must": [{
                             "query_string": {
                                "default_field": "_id",
                                "query": "' . $key . '"
                             }
                            }]
                        }
                    },
                "size": 1
            }'
        );
        $query = new Elastica_Query($query->toArray());
        $search = new Elastica_Search($this->elasticsearch);

        $results = $search->addIndex('report')
            ->addType($this->documentType)
            ->search($query);

        if ($results->count() < 1) {
            // return null so that we can fetch the report from lexis
            return null;
        }

        $person = $results->current()->getData();

        return base64_decode($person['raw_response']);
    }

    /**
     * getIndex - Get the index name for this instance (corraletes to the relative ES index)
     * 
     *  @return string - index name
     *
     *  @throws Icm_Service_Exception - if index name has not been set
     */
    public function getIndex(){
        if (isset($this->documentType)) {
            return $this->documentType;
        }

        throw new Icm_Service_Exception("Index isn't set");
    }
}

