<?php
require("OAuth.php");

//EXAMPLE USE CASE:
//
//$yahooAPI = new Icm_Service_Yahoo_Api();
//$yahooAPI->search("higgs boson", array("web"));

class Icm_Service_Yahoo_Api implements Icm_Configurable{
    const SERVICE_WEB = 'web';
    const SERVICE_LIMITEDWEB = 'limitedweb';
    const SERVICE_IMAGES = 'images';
    const SERVICE_NEWS = 'news';
    const SERVICE_BLOGS = 'blogs';
    const SERVICE_ADS = 'ads';
    const SERVICE_GEOLOCATION_URL = 'http://open.mapquestapi.com/geocoding/v1/address?key=Fmjtd%7Cluub2quynd,an=o5-9u7ng0&location=';

    protected $url;
    protected $args;
    protected $consumer;
    protected $request;
    protected $ch;

    protected $httpClient;

    /**
     * @var Icm_Config
     */
    protected $config;

    public function getHttpClient() {
        if (is_null($this->httpClient)) {
            $this->httpClient = new Icm_Util_Http_Client();
        }

        return $this->httpClient;
    }

    public function setHttpClient(Icm_Util_Http_Client $c) {
        $this->httpClient = $c;
    }

    public function __construct(Icm_Config $c = NULL){
        $this->config = $c;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->url = $c->getOption('url');
        $this->consumer = new Icm_OAuth_Consumer($c->getOption('appId'), $c->getOption('appSecret'));
        $this->getHttpClient();
        $this->args = array("format" => "json");
    }

    public function getConfig(){
        return $this->config;
    }

    /*
    * string $query = the search term as it would appear in a search bar
    * array $types = array of the type of search to perform, if empty, it defaults to a web search
    *    valid types =  array("web", "news", "images", "blogs");
    */
    public function search($query, $types = array()){
        $this->args["q"] = ($query);

        // Default will just be a web search
        $typeStr = "web";

        if (sizeof($types) == 0){
            $types = array('web');
        }

        if (!empty($types)){
            $typeStr = implode(', ', $types);
        }

        $this->url .= $typeStr;

        $this->request = Icm_OAuth_Request::from_consumer_and_token($this->consumer, NULL, "GET", $this->url, $this->args);
        $this->request->sign_request(new Icm_OAuth_SignatureMethod_HmacSha1(), $this->consumer, NULL);
        $this->url = sprintf("%s?%s", $this->url, Icm_OAuth_Util::build_http_query($this->args));

        $headers = array($this->request->to_header());

        $this->httpClient->setOption(CURLOPT_HTTPHEADER, $headers);
        $this->httpClient->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->httpClient->setUrl($this->url);
        $rsp = $this->httpClient->get();

        $retArr = json_decode($rsp, true);

        // less code is better
        if (!array_key_exists('bossresponse', $retArr)){
            return null;
        }

        // saves a little bit of mem by eliminating $response
        if (!array_key_exists('responsecode', $retArr['bossresponse'])){
            return null;
        }

        $results = array(); //hold final list of
        $makeStruct = function($result) {
            return Icm_Struct_WebResults::fromArray($result);
        };

        foreach ($types as $type) {
            if (array_key_exists($type, $retArr['bossresponse']) && array_key_exists('results', $retArr['bossresponse'][$type])){
                $typeResults = $retArr['bossresponse'][$type]['results'];
                $typeResultsStructs = array_map($makeStruct, $retArr['bossresponse'][$type]['results']);
                $results = array_merge($results, $typeResultsStructs);
            }
        }

        // null if no results else return a collection of them
        return empty($results) ? null : new Icm_Collection( $results );
    }

    public function geocode($address) {
        $config = new Icm_Config();
        $opt = $config->getOption('connection');

        if ($config->getOption('enableCache', false)){
            $cacheName = $config->getOption('cacheTemplateName', 'searchcache_');
            $cacheKey = md5(serialize($q->getCriteria()) . $q->getSearchType() . 'Icm_Service_Yahoo_Api' . 'search');

            if ($this->cachemanager->getCache($cacheName)->test($cacheKey)){
                // we have cached data; filter, then return it
                $resultSet = $this->cachemanager->getCache($cacheName)->load($cacheKey);

                // $resultSet->applyFilters($q->getFilters());
                return $resultSet;
            }
        }

        $this->getHttpClient();
        $this->httpClient->setUrl(self::SERVICE_GEOLOCATION_URL . urlencode($address) . ',USA&callback=renderGeocode');
        $response = $this->httpClient->get();
        $geodata = $this->jsonp_decode($response, TRUE);

        // Fallback to zip code
        if (count($geodata['results'][0]['locations']) == 0) {
            $this->httpClient->setUrl(self::SERVICE_GEOLOCATION_URL . substr($address, -5) . ',USA&callback=renderGeocode');
            $response = $this->httpClient->get();
            $geodata = $this->jsonp_decode($response, TRUE);
        }

        if ($config->getOption('enableCache', false)){
            $this->cachemanager->getCache($cacheName)->save($resultSet, $cacheKey);
        }

        return $geodata;
    }

    public function isTeaser($address) {
        $geodata = $this->geocode($address);

        if (isset($geodata['results'][0]['locations'])) {
            return true;
        }
        else {
            return false;
        }
    }

    private function jsonp_decode($jsonp, $assoc = false) {
        if ($jsonp[0] !== '[' && $jsonp[0] !== '{') {
            $jsonp = substr($jsonp, strpos($jsonp, '('));
        }

        return json_decode(trim($jsonp, '();'), $assoc);
    }
}
