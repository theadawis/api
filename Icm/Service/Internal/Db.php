<?php

class Icm_Service_Internal_Db implements Icm_Configurable{

    protected $_cache;

    /**
     * @var Icm_Config $config
     */
    protected $config;

    protected static $fieldNameMap = array(
                                           'id'              => 'id',
                                           'status'          => 'status',
                                           'first_name'      => 'first_name',
                                           'last_name'       => 'last_name',
                                           'address'         => 'address',
                                           'city'            => 'city',
                                           'state'           => 'state',
                                           'zip'             => 'zip',
                                           'yob'             => 'yob',
                                           'dob'             => 'dob',
                                           'email'           => 'email',
                                           'order_id'        => 'order_id',
                                           'ip_address'      => 'ip_address',
                                           'date_added'      => 'date_added',
                                           'added_csr_id'    => 'added_csr_id',
                                           'date_approved'   => 'date_approved',
                                           'approved_csr_id' => 'approved_csr_id',
                                           );

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct($conn = null) {
        $this->conn = $conn;
    }

    // args contain where clause criteria
    /**
     * @param array $args
     * @return array
     *
     * $args should contain:
     * - first_name
     * - last_name
     */
    public function getOptouts($args=array()) {
        $sql = 'select * from `site_optouts` where ';

        $cnt = 0;
        $bindings = array();
        $and = '';
        foreach ($args as $key => $value) {
            if (!isset(self::$fieldNameMap[$key])){
                continue;
            }
            $cnt++;
            $k      = self::$fieldNameMap[$key];
            $parsed = Icm_Db::parseQueryString($value);

            $sql .= $and.$k . $parsed[0] . ':' . $k;

            $bindings[$k] = $parsed[1];
            $and = ' AND ';
        }

        // limit
        if ($this->conn->maxResults()) {
            $sql .= ' LIMIT ' . $this->conn->maxResults();
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug(json_encode($bindings));

        $fieldNameMap = array_flip(self::$fieldNameMap);

        $callback = function($row) use ($fieldNameMap) {
            $remappedRow = array();
            foreach ($row as $key => $value) {
                $remappedRow[$fieldNameMap[$key]] = $value;
            }
            return $remappedRow;
        };

        $result = $this->conn->fetchAll($sql, $bindings, $callback);
        return $result;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
        }
    }


}