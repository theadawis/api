<?php
/**
 * This is essentially a storage adapter for the feedback tool
 * @author Joe Linn
 */
class Icm_Service_Internal_Feedback{
    const QUEUE_KEY = 'icm_feedback_queue';
    const ALIAS = 'feedback';
    const TYPE = 'feedback';

    /**
     * @var Icm_Redis
     */
    protected $redis = NULL;

    /**
     * @var Icm_Service_Internal_Elasticsearch
     */
    protected $elasticsearch = NULL;

    /**
     * @param Icm_Redis $redis
     * @param Icm_Service_Internal_Elasticsearch $elasticsearch
     */
    public function __construct(Icm_Redis $redis = NULL, Icm_Service_Internal_Elasticsearch $elasticsearch = NULL){
        if (!is_null($redis)){
            $this->redis = $redis;
        }
        if (!is_null($elasticsearch)){
            $this->elasticsearch = $elasticsearch;
        }
    }

    /**
     * Set the Elasticsearch service
     * @param Icm_Service_Internal_Elasticsearch $elasticsearch
     * @return Icm_Service_Internal_Feedback
     */
    public function setElasticsearch(Icm_Service_Internal_Elasticsearch $elasticsearch){
        $this->elasticsearch = $elasticsearch;
        return $this;
    }

    /**
     * Set the Redis service
     * @param Icm_Redis $redis
     * @return Icm_Service_Internal_Feedback
     */
    public function setRedis(Icm_Redis $redis){
        $this->redis = $redis;
        return $this;
    }

    /**
     * Add feedback data to the work queue
     * @param string $data json data
     * @param string $job the job name; defaults to 'feedback'
     */
    public function addQueueData($data, $job = 'feedback'){
        return $this->getRedis()->rpush(self::QUEUE_KEY, json_encode(array('job' => $job, 'data' => $data)));
    }

    /**
     * Get comment text
     * @param int $section
     * @param int $subsection
     */
    public function getCommentText($section, $subsection = false){
        $query = new Icm_Service_Internal_Elasticsearch_Query(array('sort' => array(
            array('timestamp' => array('order' => 'desc'))
        )));
        $query->setLimit(750);

        if (!is_array($section)) {
            $sectionTerms = array($section);
        } else {
            $sectionTerms = $section;
        }
        $query->addFilter(new Elastica_Filter_Terms('section', $sectionTerms));

        if ($subsection){
            $query->addFilter(new Elastica_Filter_Terms('subsection', array($subsection)));
        }
        $query->setParam('partial_fields', array(
                'record' => array(
                        'include' => array('comments')
                )
        ));
        $results = $this->elasticsearch->search($query, NULL, self::ALIAS);
        $string = '';
        foreach ($results as $result){
            $result = $result->getData();
            $string .= $result['record']['comments'];
            $string .= ' ';
        }
        return $string;
    }

    /**
     * Get all comments for the given section and optional subsection which contain the given keyword
     * @param string $keyword
     * @param int $section
     * @param int $subsection
     * @return array
     */
    public function getCommentsByKeyword($keyword, $section, $subsection = false){
        $query = new Icm_Service_Internal_Elasticsearch_Query(array('sort' => array(
            array('timestamp' => array('order' => 'desc'))
        )));
        $query->setLimit(750);

        if (!is_array($section)) {
            $sectionTerms = array($section);
        } else {
            $sectionTerms = $section;
        }
        $query->addFilter(new Elastica_Filter_Terms('section', $sectionTerms));

        if ($subsection){
            $query->addFilter(new Elastica_Filter_Terms('subsection', array($subsection)));
        }
        $query->addFilter(new Elastica_Filter_Terms('comments', array($keyword)));
        $results = $this->elasticsearch->search($query, NULL, self::ALIAS);
        return $results->getResults();
    }

    public function getFeedback($section, $subsection = false, $keyword = false){
        $query = new Icm_Service_Internal_Elasticsearch_Query(array('sort' => array(
            array('timestamp' => array('order' => 'desc'))
        )));

        if (!is_array($section)) {
            $sectionTerms = array($section);
        } else {
            $sectionTerms = $section;
        }
        $query->addFilter(new Elastica_Filter_Terms('section', $sectionTerms));

        $query->setLimit(5000);
        if ($subsection){
            $query->addFilter(new Elastica_Filter_Terms('subsection', array($subsection)));
        }
        if ($keyword){
            $query->addFilter(new Elastica_Filter_Terms('comments', array($keyword)));
        }
        $results = $this->elasticsearch->search($query, NULL, self::ALIAS);
        return $results;
    }

    /**
     * @param mixed $section
     * @param mixed $subsection
     * @param bool $keyword
     *
     * @return mixed
     */
    public function getSectionStats($section, $subsection = false, $keyword = false){
        $query = new Icm_Service_Internal_Elasticsearch_Query();
        $facet = new Elastica_Facet_Statistical('feedback');

        if (!is_array($section)) {
            $sectionTerms = array($section);
        } else {
            $sectionTerms = $section;
        }
        $query->addFilter(new Elastica_Filter_Terms('section', $sectionTerms));

        $query->setLimit(0);
        if ($subsection){
            $query->addFacetFilter('feedback', new Elastica_Filter_Terms('subsection', array($subsection)));
        }
        if ($keyword){
            $query->addFacetFilter('feedback', new Elastica_Filter_Terms('comments', array($keyword)));
        }
        $facet->setField('rating');
        $query->addFacet($facet);
        $results = $this->elasticsearch->search($query, NULL, self::ALIAS)->getFacets();
        return $results['feedback'];
    }

    /**
     *
     * @throws Exception
     * @return Icm_Redis
     */
    protected function getRedis(){
        if (is_null($this->redis)){
            throw new Exception("You must properly set the Redis service before attempting a Redis operation.");
        }
        return $this->redis;
    }

    /**
     *
     * @throws Exception
     * @return Icm_Service_Internal_Elasticsearch
     */
    protected function getElasticsearch(){
        if (is_null($this->elasticsearch)){
            throw new Exception("You must properly set the Elasticsearch service before attempting an Elasticsearch operation.");
        }
        return $this->elasticsearch;
    }
}