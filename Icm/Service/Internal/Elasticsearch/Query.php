<?php
class Icm_Service_Internal_Elasticsearch_Query{
    protected $queryString;
    protected $filters = array();
    protected $filterCombination = 'and';
    protected $facets = array();
    protected $facetFilters = array();
    protected $sort = array();
    protected $limit = 1000;
    protected $params = array();
    protected $routing = false;

    public function __construct(array $params = array()){
        if (isset($params['queryString'])){
            $this->queryString = $params['queryString'];
        }
        if (isset($params['filters']) && is_array($params['filters'])){
            $this->filters = $params['filters'];
        }
        if (isset($params['facets'])){
            $this->facets = $params['facets'];
        }
        if (isset($params['facetFilters'])){
            $this->facetFilters = $params['facetFilters'];
        }
        if (isset($params['limit'])){
            $this->limit = $params['limit'];
        }
        if (isset($params['sort'])){
            $this->sort = $params['sort'];
        }
        if (isset($params['routing'])){
            $this->routing = $params['routing'];
        }
    }

    public function setQueryString(Elastica_Query_QueryString $string){
        $this->queryString = $string;
    }

    public function addFilter(Elastica_Filter_Abstract $filter){
        $this->filters[] = $filter;
    }

    public function addFacet(Elastica_Facet_Abstract $facet){
        $this->facets[$facet->getName()] = $facet;
    }

    public function addFacetFilter($facetName, Elastica_Filter_Abstract $filter){
        $this->facetFilters[$facetName][] = $filter;
    }

    public function setParam($key, $value){
        $this->params[$key] = $value;
    }

    /**
     * @param integer $limit
     */
    public function setLimit($limit){
        $this->limit = $limit;
    }

    /**
     * @return Elastica_Query
     */
    public function getElasticaQuery(){
        $query = new Elastica_Query();
        if ($this->queryString){
            $query->setQuery($this->queryString);
        }
        if (sizeof($this->filters)){
            $query->setFilter($this->combineFilters($this->filters));
        }
        if ($this->routing){
            $query->setParam('routing', $this->routing);
        }
        $query->setLimit($this->limit);
        if (sizeof($this->sort)){
            foreach ($this->sort as $sort){
                $query->addSort($sort);
            }
        }
        if (sizeof($this->facets)){
            foreach ($this->facets as $name => $facet){
                if (isset($this->facetFilters[$name])){
                    $facet->setFilter($this->combineFilters($this->facetFilters[$name]));
                }
                $query->addFacet($facet);
            }
        }
        if (sizeof($this->params)){
            foreach ($this->params as $key => $value){
                $query->setParam($key, $value);
            }
        }
        // $query->setParam('query', $query->toArray());
        // echo '<pre>'.print_r(json_encode($query->toArray()), true) . '</pre>';
        return $query;
    }

    public function setRouting($routing){
        $this->routing = $routing;
    }

    public function getRouting($unset = false){
        $routing = $this->routing;
        if ($unset){
            $this->routing = false;
        }
        return $routing;
    }

    public function debug($json = false){
        $debug = $this->getElasticaQuery()->toArray();
        if ($json){
            echo json_encode($debug);
        }
        else{
            echo '<pre>'.print_r($debug, true) . '</pre>';
        }
    }

    /**
     * @param array $filters
     * @return Elastica_Filter_Abstract_Multi
     */
    protected function combineFilters(array $filters){
        switch($this->filterCombination){
            case 'and': $combinedFilter = new Elastica_Filter_And();
                break;
            case 'or': $combinedFilter = new Elastica_Filter_Or();
        }
        foreach ($filters as $filter){
            $combinedFilter->addFilter($filter);
        }
        return $combinedFilter;
    }
}