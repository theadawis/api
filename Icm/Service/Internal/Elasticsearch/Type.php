<?php
class Icm_Service_Internal_Elasticsearch_Type extends Elastica_Type{
    public function getDocument($id, $routing = NULL){
        $path = $id;
        $query = array();
        if (!is_null($routing)){
            $query['routing'] = $routing;
        }

        try {
            $result = $this->request($path, Elastica_Request::GET, array(), $query)->getData();
        } catch (Elastica_Exception_Response $e) {
            throw new Elastica_Exception_NotFound('doc id ' . $id . ' not found');
        }

        if (empty($result['exists'])) {
            throw new Elastica_Exception_NotFound('doc id ' . $id . ' not found');
        }

        $data = isset($result['_source']) ? $result['_source'] : array();
        $document = new Elastica_Document($id, $data, $this->getName(), $this->getIndex());
        $document->setVersion($result['_version']);

        return $document;
    }

    /**
     * Deletes an entry by its unique identifier
     *
     * @param  int|string        $id Document id
     * @return Elastica_Response Response object
     * @link http://www.elasticsearch.org/guide/reference/api/delete.html
     */
    public function deleteById($id, $routing = NULL){
        if (empty($id) || !trim($id)) {
            throw new InvalidArgumentException();
        }
        $query = array();
        if (!is_null($routing)){
            $query['routing'] = $routing;
        }
        return $this->request($id, Elastica_Request::DELETE, array(), $query);
    }
}