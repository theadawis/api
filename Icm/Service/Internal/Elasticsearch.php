<?php

class Icm_Service_Internal_Elasticsearch implements Icm_Configurable{
    const PERSON_ALIAS = 'person';
    const PERSON_INDEX = 'person_';

    protected $aliasIndexMap = array(
        self::PERSON_ALIAS => array('person3', 'person4'),
    );

    /**
     * @var Elastica_Client
     */
    protected $elastica;

    protected $config;
    protected $defaultConfig = array(
        'hosts' => array(
            array('host' => 'jlinn.local.icm', 'port' => 9200),
            array('host' => 'admin.qa02.local.icm', 'port' => 9200)
        )
    );

    protected $defaultHosts = array();

    /**
     * @var Icm_Db_Interface
     */
    protected $connection;

    public function __construct(Icm_Config $config = NULL, Icm_Db_Interface $conn = NULL){
        $configHosts = array();
        $hosts = $this->defaultHosts;

        if (!is_null($config)){
            $hosts = $config;
        }

        foreach ($hosts as $host){
            $host = explode(':', $host);
            $configHosts[] = array('host' => $host[0], 'port' => $host[1]);
        }

        $this->connection = $conn;
        $this->elastica = new Icm_Service_Internal_Elasticsearch_Client(array('servers' => $configHosts, 'timeout' => $config->getOption('timeout', 10)));
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(Icm_Config $c){
        $this->config = $c;
        $this->connection = Icm_Db::getInstance($this->config->getOption('connection'));
    }

    public function getStats($indexName){
        return $this->elastica->getIndex($indexName)->getStats();
    }

    /**
     * Get a single document by ID
     * @param string $id
     * @param string $index
     * @param string $type
     * @param string $routing
     * @return Elastica_Document
     */
    public function getDocument($id, $index, $type, $routing = false){
        $index = $this->elastica->getIndex($index);

        if ($routing){
            $type = new Icm_Service_Internal_Elasticsearch_Type($index, $type);

            return $type->getDocument($id, $routing);
        }
        else{
            return $index->getType($type)->getDocument($id);
        }
    }

    /**
     * @param Icm_Service_Internal_Elasticsearch_Query $query
     * @return Elastica_ResultSet
     */
    public function search(Icm_Service_Internal_Elasticsearch_Query $query,
                           $options = NULL,
                           $index = self::PERSON_ALIAS,
                           $type = NULL){
        $index = $this->elastica->getIndex($index);

        $search = new Elastica_Search($this->getClient());
        $search->addIndex($index);

        if (is_string($type)) {
            $search->addType($type);
        }
        elseif (is_array($type)) {
            $search->addTypes($type);
        }

        if ($routing = $query->getRouting(true)){
            $options['routing'] = $routing;
        }

        $resultSet = $search->search($query->getElasticaQuery(), $options);

        return $resultSet;
    }

    /**
     * Converts an elasticsearch person result to an Icm_Entity_Person
     * @param Elastica_Result $result
     * @return Icm_Entity_Person
     */
    public function teaserToEntity(Elastica_Result $result){
        $address = $result->address;
        $data = array(
            'person_id' => $result->person_id,
            'age' => $result->age,
            'dob' => $result->dob,
            'yob' => $result->yob,
            'priorAddresses' => $result->priorAddresses,
            'address' => $address['street'],
            'city' => $address['city'],
            'state' => $address['state'],
            'zip' => $address['zip']
        );

        // turn name.first into first_name, etc.
        $data = array_merge($data, $this->addNameSuffix($result->name));

        foreach ($result->aliases as $alias){
            $data['aliases'][] = $this->addNameSuffix($alias);
        }

        foreach ($result->relatives as $relative){
            $data['relatives'][] = array_merge(array('person_id' => $relative['person_id']), $this->addNameSuffix($relative['name']));
        }

        return Icm_Entity_Person::create($data);
    }

    /**
     * Iterates over $name and appends '_name' to each key
     * @param array $name
     * @return array
     */
    protected function addNameSuffix(array $name){
        $data = array();

        foreach ($name as $key => $value){
            $data[$key.'_name'] = $value;
        }

        return $data;
    }

    /**
     * Translates query criteria from ICM format to elasticsearch format
     * @param Icm_Data_Query $query
     * @return Icm_Data_Query
     */
    protected function translateQuery(Icm_Data_Query $query){
        $addressFields = array(Icm_Data_Query::CRITERIA_CITY, Icm_Data_Query::CRITERIA_STATE, Icm_Data_Query::CRITERIA_ZIP);
        $nameFields = array(Icm_Data_Query::CRITERIA_FIRSTNAME, Icm_Data_Query::CRITERIA_MIDDLENAME, Icm_Data_Query::CRITERIA_LASTNAME);
        $criteria = array();

        foreach ($query->getCriteria() as $key => $value){
            if (in_array($key, $addressFields)){
                $key = 'address.' . $key;
            }
            else if (in_array($key, $nameFields)){
                $key = str_replace('_name', '', $key);
                $key = 'name.' . $key;
            }

            $criteria[$key] = $value;
        }

        $query->setCriteria($criteria);

        return $query;
    }

    public function updateDocumentField($field, $value, $id, $type, $index = self::PERSON_INDEX){
        $index = $this->elastica->getIndex($index);
        $type = $index->getType($type);
        $script = "ctx._source.{$field} = \"{$value}\"";
        $script = new Elastica_Script($script);

        return $type->updateDocument($id, $script);
    }

    /**
     * @param array $people is comprised of Icm_Entity_Person objects
     */
    public function addTeaserDocuments($people){
        $bulkPeople = array();
        $peopleIDs = array();

        foreach ($people as $key => &$person){
            $doc = $this->formatTeaserDocument($person);
            $bulkPeople[] = array('index' => array('_index' => $doc->getIndex(), '_type' => $doc->getType(), '_id' => $doc->getId()));
            $bulkPeople[] = $doc->getData();
        }

        try{
            if (sizeof($bulkPeople)){
                $this->elastica->bulk($bulkPeople);
            }
        }
        catch (Elastica_Exception_BulkResponse $e){
            Icm_Daemon_Worker_ElasticsearchTeaser::log('Elasticsearch exception: '.print_r($e->getFailures(), true));
            exit;
        }

        return sizeof($bulkPeople) / 2;
    }

    /**
     * Check for a duplicate document in our person index
     * @param string $id
     * @param string $state  abbreviation
     * @return boolean false if no duplicate is found; true otherwise
     */
    public function checkDuplicate($id, $state){
        $query = new Elastica_Query_Ids('teaser', array($id));
        $index = $this->elastica->getIndex(self::PERSON_ALIAS);

        try{
            $result = $index->search($query, array('routing' => strtoupper($state)));
        }
        catch (Elastica_Exception_Abstract $e){
            return true;
        }

        return (int)$result->count() > 0;
    }

    /**
     * @param Icm_Entity_Person $person
     * @return Elastica_Document
     */
    protected function formatTeaserDocument(Icm_Entity_Person $person){
        if (!is_null($this->connection)){
            $zipCodes = new Icm_Data_Zipcodes_Db($this->connection);
        }

        $id = $person->person_id;
        $aliases = array();

        if (isset($person->aliases) && is_array($person->aliases)){
            foreach ($person->aliases as $key => $alias){
                $aliases[] = array(
                    'first' => $alias['first_name'],
                    'middle' => $alias['middle_name'],
                    'last' => $alias['last_name']
                );
            }
        }

        $priorAddresses = array();

        if (isset($person->priorAddresses) && is_array($person->priorAddresses)){
            foreach ($person->priorAddresses as $address){
                $priorAddresses[] = array(
                    'street' => $address['street'],
                    'city' => $address['city'],
                    'state' => $address['state'],
                    'zip' => $address['zip']
                );
            }
        }

        $relatives = array();

        if (isset($person->relatives) && is_array($person->relatives)){
            foreach ($person->relatives as $relative){
                $relatives[] = array(
                    'person_id' => $relative->person_id,
                    'name' => array(
                        'first' => $relative->first_name,
                        'middle' => $relative->middle_name,
                        'last' => $relative->last_name
                    )
                );
            }
        }

        if (isset($zipCodes)){
            $coordinates = $zipCodes->getCoordinates($person->zip);
        }

        $data = array(
            'person_id' => $person->person_id,
            'source' => $person->source,
            'name' => array(
                'first' => $person->first_name,
                'middle' => $person->middle_name,
                'last' => $person->last_name,
                'first_last' => $person->first_name.' ' . $person->last_name
            ),
            'aliases' => $aliases,
            'address' => array(
                'street' => $person->street,
                'city' => $person->city,
                'state' => $person->state,
                'city_state' => $person->city.'-' . $person->state,
                'zip' => $person->zip,
                'coordinates' => array(
                        'lat' => isset($coordinates)?$coordinates['Latitude']:NULL,
                        'lon' => isset($coordinates)?$coordinates['Longitude']:NULL
                )
            ),
            'priorAddresses' => $priorAddresses,
            'age' => isset($person->age) ? $person->age : '',
            'dob' => isset($person->dob) ? $person->dob : '',
            'yob' => isset($person->yob) ? $person->yob : '',
            'relatives' => $relatives,
            '_timestamp' => date('c'),
        );

        return new Elastica_Document($id, $data, 'teaser', self::PERSON_INDEX.substr(strtolower($person->last_name), 0, 1));
    }

    public function addTeaserDocument(Icm_Entity_Person $person){
        $index = $this->elastica->getIndex(self::PERSON_INDEX);
        $type = $index->getType('teaser');
        $personDocument = $this->formatTeaserDocument($person);
        $added = $type->addDocument($personDocument);
    }

    /**
     * Creates the /person/teaser index in elasticsearch. RUNNING THIS WILL ERASE ALL CURRENTLY INDEXED DATA!
     */
    public function createTeaserIndex(){
        $index = $this->elastica->getIndex('person');
        $index->create(array(
            'number_of_replicas' => 1,
            'number_of_shards' => 5,
            'analysis' => array(
                'analyzer' => array(
                    'comma' => array(
                        'type' => 'pattern',
                        'pattern' => ', '
                    )
                ),
                'filter' => array(

                )
            )
        ), true);

        $type = $index->getType('teaser');
        $mapping = new Elastica_Type_Mapping();
        $mapping->setType($type);

        $mapping->setParam('_boost', array('name' => '_boost', 'null_value' => 1.0));

        $mapping->setParam('_timestamp', array('enabled' => true));
        $mapping->setParam('ignore_conflicts', true);
        $mapping->setParam('_routing', array('required' => true, 'path' => 'address.state'));
        $mapping->setProperties(array(
            'person_id' => array('type' => 'string', 'include_in_all' => false),
            'counter' => array('type' => 'integer', 'include_in_all' => false, 'null_value' => 0),
            'source' => array('type' => 'string', 'include_in_all' => false),
            'name' => array(
                'type' => 'object',
                'properties' => array(
                    'first' => array('type' => 'string', 'include_in_all' => true),
                    'middle' => array('type' => 'string', 'include_in_all' => true),
                    'last' => array('type' => 'string', 'include_in_all' => true, 'analyzer' => 'comma'),
                    'first_last' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma')
                )
            ),
            'aliases' => array(
                'type' => 'object',
                'lists' => array(
                    'properties' => array(
                        'first' => array('type' => 'string', 'include_in_all' => false),
                        'middle' => array('type' => 'string', 'include_in_all' => false),
                        'last' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma'),
                    )
                )
            ),
            'address' => array(
                'type' => 'object',
                'properties' => array(
                    'street' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma'),
                    'city' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma'),
                    'city_state' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma'),
                    'state' => array('type' => 'string', 'include_in_all' => false, 'index' => 'not_analyzed'),
                    'zip' => array('type' => 'string', 'include_in_all' => false),
                    'coordinates' => array('type' => 'geo_point', 'include_in_all' => false)
                )
            ),
            'priorAddresses' => array(
                'type' => 'object',
                'lists' => array(
                    'properties' => array(
                        'street' => array('type' => 'string', 'include_in_all' => false),
                        'city' => array('type' => 'string', 'include_in_all' => false, 'analyzer' => 'comma'),
                        'state' => array('type' => 'string', 'include_in_all' => false, 'index' => 'not_analyzed'),
                        'zip' => array('type' => 'string', 'include_in_all' => false),
                    )
                )
            ),
            'age' => array('type' => 'integer', 'include_in_all' => false),
            'yob' => array('type' => 'integer', 'include_in_all' => false),
            'dob' => array('type' => 'string', 'include_in_all' => false),
            'relatives' => array(
                'type' => 'object',
                'lists' => array(
                    'properties' => array(
                        'person_id' => array('type' => 'string', 'include_in_all' => false),
                        'name' => array(
                            'type' => 'object',
                            'properties' => array(
                                'first' => array('type' => 'string', 'include_in_all' => false),
                                'middle' => array('type' => 'string', 'include_in_all' => false),
                                'last' => array('type' => 'string', 'include_in_all' => false),
                            )
                        )
                    )
                )
            )
        ));

        $mapping->send();
    }

    /**
     * @param unknown_type $term
     * @param unknown_type $type
     * @param unknown_type $routing
     * @param unknown_type $index
     * @return Elastica_Response
     */
    public function deleteByTerm($term, $type, $routing = false, $index = self::PERSON_INDEX){
        $path = "/{$index}/{$type}/_query";
        $params = array();

        if ($routing){
            $params['routing'] = $routing;
        }

        return $this->elastica->request($path, Elastica_Request::DELETE, array('term' => $term), $params);
    }

    public function deleteTeaserById($id, $index = self::PERSON_INDEX, $routing = NULL){
        if (array_key_exists($index, $this->aliasIndexMap)) {
            $result = null;
            $successfulResult = null;

            // Can't delete from an alias, so try to delete from each index in that alias
            foreach ($this->aliasIndexMap[$index] as $i) {
                $result = $this->deleteTeaserById($id, $i, $routing);
                $data = $result->getData();

                if ($data['found']) {
                    $successfulResult = $result;
                }
            }

            return $successfulResult ? $successfulResult : $result;
        }

        $index = $this->elastica->getIndex($index);
        $type = new Icm_Service_Internal_Elasticsearch_Type($index, 'teaser');

        return $type->deleteById($id, $routing);
    }

    /**
     * @return Elastica_Client|Icm_Service_Internal_Elasticsearch_Client
     */
    public function getClient() {
        return $this->elastica;
    }
}
