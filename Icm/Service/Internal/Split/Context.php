<?php

class Icm_Service_Internal_Split_Context {

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'split_section';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getById( $id ) {

         // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getAllByAppId( $appId ) {

         // get all
        $data = $this->conn->fetchAll("SELECT * FROM {$this->mainTableName} WHERE app_id = ?", array($appId));

        $results = array();
        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

}