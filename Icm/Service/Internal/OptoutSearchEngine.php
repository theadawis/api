<?php
/**
 * This class will allow optouts to be performed by person id OR search criteria.
 * Use this class for teaser searches.
 * @author Joe Linn
 *
 */
class Icm_Service_Internal_OptoutSearchEngine extends Icm_Search_Engine{
    public function search(Icm_Search_Query_Interface $q) {
        if ($this->getConfig()->getOption('enableCache', false)){
            $cacheName = $this->getConfig()->getOption('cacheTemplateName', 'searchcache_');
            $cacheKey = md5(serialize($q->getCriteria()) . $q->getSearchType().get_class($this->_additiveAdapters[0]) . 'search');
            if ($this->cachemanager->getCache($cacheName)->test($cacheKey)){
                // we have cached data; filter, then return it
                $resultSet = $this->cachemanager->getCache($cacheName)->load($cacheKey);
                // $resultSet->applyFilters($q->getFilters());
                return $resultSet;
            }
        }

        $criteria = $q->getCriteria();
        $type = $q->getSearchType();
        $resultSet = new Icm_Search_Result($criteria, $type, array());

        $adapters = $this->getAdditiveAdapters();
        if (!count($adapters)) {
            throw new Icm_Service_Exception("No additive Data Adapters present. Shame.");
        }

        $personIDs = array();
        foreach ($adapters as $adapter) {
            // Check if paid queries are allowed
            /**
             * @var Icm_Search_Adapter_Interface $adapter
             */

            /*echo '<pre>'.print_r($adapter, true) . '</pre>';
             echo '<pre>'.print_r($type, true) . '</pre>';*/
            if (!method_exists($adapter, $type)) {
                $class = get_class($adapter);
                throw new Icm_Service_Exception("Invalid search type: $type in $class. ...Shame. Shame on you.");
            }

            // $adapter->setEntityFactory($this->entityFactory);

            if ($adapter->isPaid($type)) {
                if (!$q->allowPaid()) {
                    continue;
                }
            }

            $results = $adapter->$type($q->getCriteria());
            if (count($results)) {
                $resultSet->addAll($results);
                foreach ($results as $result){
                    $personIDs[] = $result->person_id;
                }
            }
        }

        foreach ($this->getSubtractiveAdapters() as $adapter){
            if (!method_exists($adapter, $type)) {
                $class = get_class($adapter);
                throw new Icm_Service_Exception("Invalid search type: $type in $class. ...Shame. Shame on you.");
            }

            if ($adapter->isPaid($type) && !$q->allowPaid()){
                continue;
            }

            $results = $adapter->$type($q->getCriteria(), $personIDs);

            if (count($results)){
                foreach ($results as $result){
                    if (!is_a($result, 'Icm_Entity')){
                        throw new Icm_Service_Exception("Data sources MUST return Entities. SHAME!");
                    }
                    $q->hasHash($result->getHash());
                }
            }
        }


        // filter AFTER caching
        $resultSet->applyFilters($q->getFilters());
        if ($this->getConfig()->getOption('enableCache', false)){
            $this->cachemanager->getCache($cacheName)->save($resultSet, $cacheKey);
        }

        return $resultSet;
    }
}