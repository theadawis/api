<?php

class Icm_Service_Internal_Marketing_Pixel implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_pixels';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getById( $id ) {

         // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAll( $order = null ) {
         // get all
        $data = $this->table->getAll( $order );

        $results = array();
        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAllByAffiliateId( $affiliateId ) {

        $sql = "SELECT mp.*, mc.name as campaign_name, mpt.label as pixel_type_label, ma.company_name as affiliate_name
                FROM {$this->mainTableName} mp
                LEFT JOIN `marketing_campaigns` mc ON mp.campaign_id = mc.id
                LEFT JOIN `marketing_affiliates` ma ON mp.affiliate_id = ma.id
                LEFT JOIN `marketing_pixel_types` mpt ON mp.pixel_type_id = mpt.id
                WHERE mp.affiliate_id = ?";

         // get all
        return $this->conn->fetchAll($sql, array($affiliateId));
    }

    /**
     * @param id
     * @return array
     *
     */
    public function remove( $id ) {

         // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

         // get all the results
        return $this->conn->execute($sql, array( $id ));
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_Marketing_Pixel $obj, $skipOverNull = true ) {

        $data = $obj->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data, null, $skipOverNull);
            return $data['id'];

        } else {

             // insert
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }

}