<?php

class Icm_Service_Internal_Marketing_Affiliate implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_affiliates';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param int $id
     * @return Icm_Struct_Marketing_Affiliate
     *
     */
    public function getById($id) {
        // get by id
        return Icm_Struct_Marketing_Affiliate::fromArray($this->table->findOneBy('id', $id));
        // return $this->table->findOneBy('id', $id);
    }

    /**
     * Fetch a marketing affiliate by name
     *
     * @param string $name
     * @return Icm_Struct_Marketing_Affiliate
     */
    public function getByName($name) {
        $affiliate_row = $this->table->findOneBy('company_name', $name);

        if ($affiliate_row) {
            return Icm_Struct_Marketing_Affiliate::fromArray($affiliate_row);
        }

        return null;
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAll($order = null) {
        // get all
        $data = $this->table->getAll($order);

        $results = array();
        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @param id
     * @return array
     *
     */
    public function remove($id) {
        // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

        // get all the results
        return $this->conn->execute($sql, array($id));
    }

    /**
     * Save the object: insert or update depending if the id is set
     * @param Icm_Struct_Marketing_Affiliate $obj
     * @return mixed
     */
    public function save(Icm_Struct_Marketing_Affiliate $obj) {
        $data = $obj->toArray();

        if (isset($data['id']) && intval($data['id']) > 0) {
            // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);
            return $data['id'];
        } else {
            // insert
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }

}