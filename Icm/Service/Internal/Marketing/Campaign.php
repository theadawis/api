<?php

class Icm_Service_Internal_Marketing_Campaign implements Icm_Configurable{

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_campaigns';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     */
    public function getById($id) {
        // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * Fetch a campaign by its name
     *
     * @param string $name
     * @return Icm_Struct_Marketing_Campaign
     */
    public function getByName($name) {
        $campaign_row = $this->table->findOneBy('name', $name);

        if ($campaign_row) {
            return Icm_Struct_Marketing_Campaign::fromArray($campaign_row);
        }

        return null;
    }

    /**
     * @param $order - order array to fetch
     * @return array
     */
    public function getAll($order = null) {
        // get all
        $data = $this->table->getAll($order);

        $results = array();

        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @return array
     */
    public function getAllActive() {
        // get all
        $sql = "SELECT *
                FROM {$this->mainTableName}
                WHERE status = ?";
        $data = $this->conn->fetchAll($sql, array(self::STATUS_ACTIVE));
        $results = array();

        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @param $id - campaign id
     * @return array
     */
    public function getAllLandingPagesByCampaignId($id) {
        $sql = "SELECT mlp.*
                FROM `marketing_campaign_landing_pages` mclp
                JOIN `marketing_landing_pages` mlp ON mclp.landing_page_id = mlp.id
                WHERE mclp.campaign_id = ?";

        // get all the landing pages using the campaign id
        return $this->conn->fetchAll($sql, array($id));
    }

    /**
     * @param id
     * @return array
     *
     */
    public function remove($id) {
        // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

        // get all the results
        return $this->conn->execute($sql, array( $id ));
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     */
    public function save(Icm_Struct_Marketing_Campaign $obj) {
        $data = $obj->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {
            // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);

            return $data['id'];
        } else {
            // insert
            $data['status'] = 'active';
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);

            return $this->conn->lastInsert();
        }

    }

    /**
     * Save the landing page relation
     *
     * @param $campaignId - campaign id
     * @param $landingPageId - landing page id
     * @return id
     */
    public function saveLandingPageRelation($campaignId, $landingPageId) {
        // set up the right table connection
        $table = new Icm_Db_Table('marketing_campaign_landing_pages', 'id', $this->conn);

        // check if the (campaign - landing page) relation already exist for these ids
        $sql = "SELECT * FROM `marketing_campaign_landing_pages` WHERE campaign_id = ? AND landing_page_id = ?";
        $record = $this->conn->fetch($sql, array($campaignId, $landingPageId));

        if ($record === FALSE) {
            // insert relation
            $data['campaign_id'] = $campaignId;
            $data['landing_page_id'] = $landingPageId;
            $table->insertOne($data);

            return $this->conn->lastInsert();
        }
    }

    /**
     * Remove the entry in the campaign landing page table
     *
     * @param $campaignLandingPageId - campaign id
     * @return execute result
     */
    public function removeLandingPageRelation($campaignId, $landingPageId) {
        // generate the initial sql statement
        $sql = "DELETE
        FROM `marketing_campaign_landing_pages`
        WHERE campaign_id = ? AND landing_page_id = ?";

        // get all the results
        return $this->conn->execute($sql, array($campaignId, $landingPageId));
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;

        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }
}