<?php

class Icm_Service_Internal_Marketing_Affiliate_Link implements Icm_Configurable
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_affiliate_links';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getById( $id ) {
         // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param hash
     * @return array
     *
     */
    public function getByHash( $hash ) {
        $sql = "SELECT mal.*, mlp.url AS landing_page_url, ma.risk_mitigation as affiliate_risk_mitigation,
                    ma.short_name as affiliate_short_name, ma.category as affiliate_category
                FROM {$this->mainTableName} mal
                JOIN `marketing_affiliates` ma ON mal.affiliate_id = ma.id
                JOIN `marketing_landing_pages` mlp ON mal.landing_page_id = mlp.id
                WHERE mal.hash = ?";

         // get the query
        return $this->conn->fetch($sql, array($hash));
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAll( $order = null ) {

         // get all
        return $this->table->getAll( $order );
    }

    /**
     * @return array
     *
     */
    public function getAllActive() {
         // get all
        $data = $this->conn->fetchAll("SELECT * FROM {$this->mainTableName} WHERE status = ? ", array(self::STATUS_ACTIVE));

        $results = array();
        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAllByAffiliateId( $affiliateId ) {

        $sql = "SELECT mal.*, mc.name as campaign_name, mlp.name as landing_page_name
                FROM {$this->mainTableName} mal
                LEFT JOIN `marketing_campaigns` mc ON mal.campaign_id = mc.id
                LEFT JOIN `marketing_landing_pages` mlp ON mal.landing_page_id = mlp.id
                WHERE mal.affiliate_id = ?";

         // get all
        return $this->conn->fetchAll($sql, array($affiliateId));
    }

    /**
     * @param id
     * @return array
     *
     */
    public function remove( $id ) {
         // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

         // get all the results
        return $this->conn->execute($sql, array( $id ));
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_Marketing_Affiliate_Link $obj ) {
        $data = $obj->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);
            return $data['id'];

        } else {

             // generate a unique hash
            $currentAttempt = 0;
            $maxAttempts = 10;
            do {

                if ($currentAttempt == $maxAttempts) {
                    throw new Exception('Max attempts reached while trying to generate hash');
                }

                $currentAttempt++;
                $hash = base_convert(rand(1500, time()), 10, 36);
                $record = $this->getByHash($hash);

            } while( is_array($record) );

             // insert
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $data['hash'] = $hash;
            $this->table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }

}