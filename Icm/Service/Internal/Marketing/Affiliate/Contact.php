<?php

class Icm_Service_Internal_Marketing_Affiliate_Contact implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_affiliate_contacts';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getById( $id ) {

         // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param affiliateId
     * @return array
     *
     */
    public function getByAffiliateId( $affiliateId ) {

         // get by id
        return $this->table->findOneBy('affiliate_id', $affiliateId);
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAll( $order = null ) {

         // get all
        return $this->table->getAll( $order );
    }

    /**
     * @param id
     * @return array
     *
     */
    public function remove( $id ) {

         // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

         // get all the results
        return $this->conn->execute($sql, array( $id ));
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_Marketing_Affiliate_Contact $obj ) {

        $data = $obj->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);
            return $data['id'];

        } else {

             // insert
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }

}