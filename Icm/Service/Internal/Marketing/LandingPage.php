<?php

class Icm_Service_Internal_Marketing_LandingPage implements Icm_Configurable{

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_landing_pages';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     */
    public function getById($id) {

        // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param $order - order array to fetch
     * @return array
     */
    public function getAll($order = null) {

        // get all
        return $this->table->getAll($order);
    }

    /**
     * @return array
     */
    public function getAllActive() {
        // get all
        $data = $this->conn->fetchAll("SELECT * FROM {$this->mainTableName} WHERE status = ? ", array(self::STATUS_ACTIVE));

        $results = array();

        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    /**
     * @param id
     * @return array
     */
    public function getAllActiveByCampaignId($campaignId) {

        // generate the initial sql statement
        $sql = "SELECT tlp.*
                FROM `marketing_campaign_landing_pages` AS tclp
                JOIN `marketing_landing_pages` AS tlp ON tclp.landing_page_id = tlp.id
                WHERE campaign_id = ? AND status = ?";

        // get all the results
        return $this->conn->fetchAll($sql, array($campaignId, self::STATUS_ACTIVE));
    }

    /**
     * Fetch a landing page by its url
     *
     * @param string $url
     * @return Icm_Struct_Marketing_LandingPage
     */
    public function getByUrl($url) {
        $landing_page_row = $this->table->findOneBy('url', $url);

        if ($landing_page_row) {
            return Icm_Struct_Marketing_LandingPage::fromArray($landing_page_row);
        }

        return null;
    }

    /**
     * @param id
     * @return array
     */
    public function remove($id) {

        // generate the initial sql statement
        $sql = "DELETE
                FROM `{$this->mainTableName}`
                WHERE id = ?";

        // get all the results
        return $this->conn->execute($sql, array($id));
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     */
    public function save(Icm_Struct_Marketing_LandingPage $obj) {

        $data = $obj->toArray();

        if (isset($data['id']) && intval($data['id']) > 0) {
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);

            return $data['id'];
        } else {
            $data['status'] = 'active';
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);

            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }
}