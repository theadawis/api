<?php

class Icm_Service_Internal_Marketing_PixelSplitContext {

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'marketing_pixel_split_contexts';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param $pixelId - pixel id
     * @return array
     *
     */
    public function getAllSplitContextIdsByPixelId($pixelId) {
         // get all
        $data = $this->conn->fetchAll("SELECT * FROM {$this->mainTableName} WHERE pixel_id = ? ", array($pixelId));

        $results = array();
        foreach ($data as $key => $result) {
            $results[] = $result['split_context_id'];
        }

        return $results;
    }

    /**
     * @param $pixelId - pixel id
     * @return array
     *
     */
    public function removeAllByPixelId($pixelId) {
        $sql = "DELETE FROM `{$this->mainTableName}` WHERE pixel_id = ?";
        $result = $this->conn->execute($sql, array($pixelId));

        return $result;
    }

    /**
     * Save the object: insert or update depending if the id is set
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_Marketing_PixelSplitContext $obj ) {

        $data = $obj->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->updateOne($data);
            return $data['id'];

        } else {

             // insert
            $data['created'] = date('Y-m-d H:i:s');
            $data['updated'] = date('Y-m-d H:i:s');
            $this->table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

}