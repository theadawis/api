<?php

class Icm_Service_Internal_Source implements Icm_Configurable {

    /**
     * @var Icm_Config $config
     */
    protected $config;

    protected $conn;

    const SOURCE_NPD = 1;
    const SOURCE_LEXIS = 2;

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
    }

    /**
     * @param optoutId
     * @return array
     *
     */
    public function getById( $id ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `sources`
                WHERE id = ?";

         // get all the results
        return $this->conn->fetch( $sql, array( $id ) );
    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
        }
    }


}