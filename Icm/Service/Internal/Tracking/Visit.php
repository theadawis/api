<?php

class Icm_Service_Internal_Tracking_Visit implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'visitor_visits';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'visit_id', $this->conn);
    }

    /**
     * @param int $id
     * @return Icm_Struct_Tracking_Visit
     */
    public function getById( $id ) {
        // get by id
        $result = $this->table->findOneBy('visit_id', $id);

        return $result ? Icm_Struct_Tracking_Visit::fromArray($result) : null;
    }

    /**
     * Find the most recent non-duplicate visit for the given visitor id
     *
     * @param string $visitorId
     * @return Icm_Struct_Tracking_Visit
     */
    public function getMostRecentForVisitorId($visitorId) {
        $sql = "SELECT * FROM {$this->mainTableName} WHERE visitor_id = ? AND is_duplicate = 0 ORDER BY created DESC";
        $replacements = array($visitorId);
        $result = $this->conn->fetch($sql, $replacements);

        return $result ? Icm_Struct_Tracking_Visit::fromArray($result) : null;
    }

    /**
     * Finds all visits for a given visitor id
     * @param $visitorId
     * @return array
     */
    public function getAllForVisitor($visitorId) {
        $sql = "SELECT * FROM {$this->mainTableName} WHERE visitor_id = ?";
        $replacements = array($visitorId);
        $result = $this->conn->fetchAll($sql, $replacements);

        if ($result){
            return array_map(function($item) {
                return Icm_Struct_Tracking_Visit::fromArray($item);
            }, $result);
        }

        return array();
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;

        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }
}