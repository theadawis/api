<?php

class Icm_Service_Internal_Optout implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;

    protected $conn;

    protected static $fieldNameMap = array(
            'id'                    => 'id',
            'optout_request_id'     => 'optout_request_id',
            'external_person_id'    => 'external_person_id',
            'source_id'                => 'source_id',
            'first_name'            => 'first_name',
            'last_name'                 => 'last_name',
            'address'               => 'address',
            'city'                    => 'city',
            'state'                     => 'state',
            'zip'                   => 'zip',
            'dob'                   => 'dob',
            'date_created'          => 'date_created',
    );

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
    }

    /**
     * @param optoutId
     * @return array
     *
     */
    public function getOptoutById( $optoutId ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `optouts`
                WHERE id = ?";

         // get all the results
        return $this->conn->fetch($sql, array($optoutId));
    }

    public function getAllExternalPersonIds() {
         // generate the initial sql statement
          $sql = "SELECT external_person_id, state, source_id
                FROM `optouts`
                WHERE external_person_id IS NOT NULL
                AND date_created > UNIX_TIMESTAMP()-2*24*60*60";
         // get all the results
        return $this->conn->fetchAll($sql);
    }

    /**
     * @param optoutId
     * @return array
     *
     */
    public function remove( $optoutId ) {

         // generate the initial sql statement
        $sql = "DELETE
                FROM `optouts`
                WHERE id = ?";

         // get all the results
        return $this->conn->execute($sql, array( $optoutId ));
    }

    /**
     * @param optoutRequestId
     * @return array
     *
     */
    public function getAllByOptoutRequestId( $optoutRequestId ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `optouts`
                WHERE `external_person_id` IS NOT NULL AND optout_request_id = ?";

         // get all the results
        return $this->conn->fetchAll( $sql, array( $optoutRequestId ) );
    }

    public function getOptoutsByPersonIds( $personIds = array()) {

         // return the
        if ( count($personIds) == 0 ) {
            return array();
        }

        $personIdString = '';
        foreach ($personIds as $personId) {
            $personIdString .= $this->conn->escape($personId) . ', ';
        }
        $personIdString = rtrim($personIdString, ', ');

         // generate the initial sql statement
        $sql = "SELECT `o`.external_person_id, `o`.source_id
                FROM `optouts` AS o
                JOIN `optout_requests` AS `or` ON `o`.optout_request_id = `or`.id
                WHERE `o`.external_person_id IN ({$personIdString}) AND `or`.status = 'active'";

         // get all the results
        return $this->conn->fetchAll( $sql );
    }

    /**
     * This method will get optout requests using first name and last name and other data from the original request.
     *
     * @param array $args
     * @return array
     *
     * $args should contain:
     * - first_name
     * - last_name
     */
    public function getOptouts( $args = array() ) {

         // this query is using a join to optout requests to make sure the optout is active just in case
         // also there is a joining back onto itself
        $sql = '

            (
                SELECT `o2`.external_person_id, `o2`.source_id
                FROM `optouts` AS `o`
                JOIN `optout_requests` AS `or` ON `o`.optout_request_id = `or`.id
                JOIN `optouts` AS `o2` ON `o2`.optout_request_id = `or`.id
                WHERE `o`.`external_person_id` IS NOT NULL AND `o2`.`external_person_id` IS NOT NULL AND
                      `or`.status = "ACTIVE" ';

        $cnt = 0;
        $bindings = array();
        $and = ' AND ';
        foreach ($args as $key => $value) {
            if (!isset(self::$fieldNameMap[$key])){
                continue;
            }
            $cnt++;
            $k      = self::$fieldNameMap[$key];
            $parsed = Icm_Db::parseQueryString($value);

            $sql .= $and.'`o`.' . $k . $parsed[0] . ':' . $k;

            $bindings[$k] = $parsed[1];
        }

         // add group by
        $sql .= ' GROUP BY `o2`.id';

        $sql .= '
            )
                UNION
            (
                SELECT `o2`.external_person_id, `o2`.source_id
                FROM `pd_nickname` AS `n`
                JOIN `optouts` AS `o` ON `n`.variation = `o`.first_name
                JOIN `optout_requests` AS `or` ON `o`.optout_request_id = `or`.id
                JOIN `optouts` AS `o2` ON `o2`.optout_request_id = `or`.id
                WHERE `o`.`external_person_id` IS NOT NULL AND `o2`.`external_person_id` IS NOT NULL AND
                    `or`.status = "ACTIVE" AND `n`.name = :first_name AND `n`.relflag = 1 ';

        $cnt = 0;
        $and = ' AND ';
        foreach ($args as $key => $value) {

            if ($key == 'first_name') {
                continue;
            }

            if (!isset(self::$fieldNameMap[$key])){
                continue;
            }
            $cnt++;
            $k      = self::$fieldNameMap[$key];
            $parsed = Icm_Db::parseQueryString($value);

            $sql .= $and.'`o`.' . $k . $parsed[0] . ':' . $k;

            $bindings[$k] = $parsed[1];
        }

         // add group by
        $sql .= ' GROUP BY `o2`.id )';

        // var_dump($sql);
        // die();

         // limit query
        if ($this->conn->maxResults()) {
            $sql .= ' LIMIT ' . $this->conn->maxResults();
        }

        Icm_Util_Logger_Syslog::getInstance('api')->logDebug($sql);
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug(json_encode($bindings));

        $fieldNameMap = array_flip(self::$fieldNameMap);

        $callback = function($row) use ($fieldNameMap) {
            $remappedRow = array();
            foreach ($row as $key => $value) {

                if ( $key == 'dob' && strlen($value) > 0  ) {

                     // dob is special in that it needs to be converted
                    $remappedRow[$fieldNameMap[$key]] = date('m/d/Y', $value);

                } else {

                    $remappedRow[$fieldNameMap[$key]] = $value;
                }

            }

            return $remappedRow;
        };

        return $this->conn->fetchAll($sql, $bindings, $callback);
    }

    /**
     * Save the Optout, insert or update depending if the id is set
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_Optout $optout ) {

        $table = new Icm_Db_Table('optouts', 'id', $this->conn);
        $data = $optout->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $table->updateOne($data);
            return $data->id;

        } else {

             // insert
            $data['date_created'] = time();
            $table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
        }
    }

}