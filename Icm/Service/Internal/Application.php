<?php

class Icm_Service_Internal_Application implements Icm_Configurable{

    /**
     * @var Icm_Config $config
     */
    protected $config;
    protected $conn;
    protected $table;
    protected $mainTableName = 'cg_application';

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
        $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
    }

    /**
     * @param id
     * @return array
     *
     */
    public function getById( $id ) {

         // get by id
        return $this->table->findOneBy('id', $id);
    }

    /**
     * @param $order - order array to fetch
     * @return array
     *
     */
    public function getAll( $order = null ) {
         // get all
        $data = $this->table->getAll( $order );

        $results = array();
        foreach ($data as $key => $result) {
            $results[ $result['id'] ] = $result;
        }

        return $results;
    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
            $this->table = new Icm_Db_Table($this->mainTableName, 'id', $this->conn);
        }
    }

}