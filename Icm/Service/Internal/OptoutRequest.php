<?php

class Icm_Service_Internal_OptoutRequest implements Icm_Configurable{

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const STATUS_PENDING = 'PENDING';

    /**
     * @var Icm_Config $config
     */
    protected $config;

    /**
     * @var connection
     */
    protected $conn;

    /**
     * @param Icm_Db_Interface $conn
     */
    public function __construct(Icm_Db_Interface $conn = null) {
        $this->conn = $conn;
    }

    /**
     * @param status
     * @return array
     *
     */
    public function getOptoutRequests( $status = null, $limit = null ) {

        $replacements = array();

         // generate the initial sql statement
        $sql = "SELECT `or`.*, count(o.id) as `total_optouts`
                FROM `optout_requests` AS `or`
                LEFT JOIN `optouts` AS o ON o.optout_request_id = `or`.id AND o.external_person_id IS NOT NULL ";

         // add the where clause
        if ( !is_null($status) ) {
            $sql .= "WHERE status = ? ";
            $replacements[] = $status;
        }

         // append the grouping
        $sql .= "GROUP BY or.id ";

         // append limit if specified
        if ( !is_null($limit) ) {
            $limit = intval($limit);
            $sql .= "LIMIT {$limit} ";
        }

         // get all the results
        return $this->conn->fetchAll($sql, $replacements);
    }

    /**
     * @param optout request id
     * @return array
     *
     */
    public function getOptoutRequestOtherLocationsByOptoutRequestId( $optoutRequestId ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `optout_request_other_locations`
                WHERE optout_request_id = ?";

         // get all the results
        return $this->conn->fetchAll($sql, array( $optoutRequestId ));
    }

    /**
     * @param optoutRequestId
     * @return array
     *
     */
    public function getOptoutRequestById( $optoutRequestId ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `optout_requests`
                WHERE id = ?";

         // get all the results
        return $this->conn->fetch($sql, array( $optoutRequestId ));
    }

    /**
     * @param authenticationHash
     * @return array
     *
     */
    public function getOptoutRequestByAuthenticationHash( $authenticationHash ) {

         // generate the initial sql statement
        $sql = "SELECT *
                FROM `optout_requests`
                WHERE `authentication_hash` = ?";

         // get all the results
        return $this->conn->fetch($sql, array( $authenticationHash ));
    }

    /**
     * @param optoutRequestId
     * @return array
     *
     */
    public function getOptoutRequestCountByEmail( $email ) {

         // generate the initial sql statement
        $sql = "SELECT count(id) as total
                FROM `optout_requests`
                WHERE `email` = ?";

         // get all the results
        $result = $this->conn->fetch($sql, array( $email ));

         // return the total counter
        return $result['total'];
    }

    /**
     * @param optoutRequestId
     * @return array
     *
     */
    public function remove( $optoutRequestId ) {

         // generate the initial sql statement
        $sql = "DELETE
                FROM `optout_requests`
                WHERE id = ?
                LIMIT 1";

         // get all the results
        return $this->conn->execute($sql, array( $optoutRequestId ));
    }

    /**
     * @param id
     * @return array
     *
     */
    public function removeOtherLocation( $id ) {

         // generate the initial sql statement
        $sql = "DELETE
                FROM `optout_request_other_locations`
                WHERE id = ?
                LIMIT 1";

         // get all the results
        return $this->conn->execute($sql, array( $id ));
    }

    /**
     * Save the Optout Request, insert or update
     *
     * @param array
     * @return id
     *
     */
    public function save( Icm_Struct_OptoutRequest $optoutRequest ) {

        $table = new Icm_Db_Table('optout_requests', 'id', $this->conn);
        $data = $optoutRequest->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $table->updateOne($data);
            return $data['id'];

        } else {

             // add date added if it wasn't set
            if ( is_null($data['date_added']) ) {
                $data['date_added'] = strtotime('now');
            }

             // insert
            $table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    /**
     * Save the Optout Request's Other Location, insert or update
     *
     * @param array
     * @return id
     *
     */
    public function saveOtherLocation( Icm_Struct_OptoutRequestOtherLocation $orol ) {

        $table = new Icm_Db_Table('optout_request_other_locations', 'id', $this->conn);
        $data = $orol->toArray();

        if ( isset($data['id']) && intval($data['id']) > 0) {

             // update
            $table->updateOne($data);
            return $data['id'];

        } else {

             // add date added if it wasn't set
            if ( is_null($data['date_created']) ) {
                $data['date_created'] = strtotime('now');
            }

             // insert
            $table->insertOne($data);
            return $this->conn->lastInsert();
        }

    }

    public function getConfig() {
        return $this->config;
    }


    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->getOption('connection')) {
            $this->conn = Icm_Db::getInstance($this->config->getOption('connection'));
        }
    }


}