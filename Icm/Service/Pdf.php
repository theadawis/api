<?php

class Icm_Service_Pdf implements Icm_Service_Pdf_Interface
{

    protected $pdfService;

    public function __construct($service = 'snappy') {
         // set up the pdf service
        switch ($service) {
            case 'snappy':
                $this->pdfService = new Icm_Service_Pdf_Snappy();
                break;
            default:
                throw new Icm_Service_Exception('Invalid pdf service');
        }

    }

    protected function isPdf($pdfString) {
        return (substr($pdfString, 0, 5) == "%PDF-") ? true : false;
    }

    public function getPdfFromHtml($htmlContent) {
        $pdfString = $this->pdfService->getPdfFromHtml($htmlContent);

        // TODO: we may want to handle this differently
        if (!$this->isPdf($pdfString)) {
            throw new Icm_Service_Exception('PDF could not be generated');
        }

        return $pdfString;
    }

    public function setHeaders($fileName = 'file') {
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $fileName.'.pdf"');
    }
}
