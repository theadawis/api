<?php
class Icm_Service_Pipl_Api{

    protected $url;
    protected $args;
    protected $appId;
    /**
     * @var Icm_Util_Http_Client
     */
    protected $httpClient;

    protected $config;
    protected $defaultConfig = array(
        'url' => 'http://api.pipl.com/search/v3/json/',
        'appId' => 'rjxs3vybpzsj4t83kchrdhrh'
    );

    public function __construct(Icm_Config $c){
        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $c->getOption($key, $default);
        }
        $this->getHttpClient();
        $this->args = array("format" => "json");
    }

    public function search($searchFields){
        // $searchFields should be an array
        $searchFields["key"] = $this->config['appId'];

        $this->httpClient->setUrl(
            $this->httpClient->buildUrlString($this->config['url'], $searchFields)
        );

        $response = $this->httpClient->get();
        $response = json_decode($response, true);

        if (!array_key_exists('@records_count', $response) || intval($response['@records_count']) < 1){
            // no matching results returned
            return array();
        }

        if (!array_key_exists('records', $response) || empty($response['records'])){
            return array();
        }

        $records = $response['records'];

        $socialCollect = new Icm_Collection(array());
        foreach ($records as $record){
            $recVar = array();
            if (array_key_exists('@query_person_match', $record)){
                $record['query_person_match'] = $record['@query_person_match'];
                unset($record['@query_person_match']);
            }
            if (array_key_exists('@query_params_match', $record)){
                $record['query_params_match'] = $record['@query_params_match'];
                unset($record['@query_params_match']);
            }
            unset($record['source']['@is_sponsored']);

            // ensure that the result is in the country we're looking for
            if (isset($record['addresses']) && sizeof($record['addresses'])){
                foreach ($record['addresses'] as $address){
                    if (isset($address['country'])){
                        if (strtolower($address['country']) == strtolower('us')){
                            // ensure that the result is in the state we're looking for
                            if (isset($address['state']) && !is_null($address['state'])){
                                if (strtolower($address['state']) == strtolower($searchFields['state'])){
                                    // ensure that the result is in the city we're looking for
                                    if (isset($address['city']) && !is_null($address['city'])){
                                        if (strtolower($address['city']) == strtolower($searchFields['city'])){
                                            $socialStruct = Icm_Struct_Social::fromArray($record);
                                            $socialCollect->add($socialStruct);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // ensure that the result is in the state we're looking for
                        if (isset($address['state']) && !is_null($address['state'])){
                            if (strtolower($address['state']) == strtolower($searchFields['state'])){
                                // ensure that the result is in the city we're looking for
                                if (isset($address['city']) && !is_null($address['city'])){
                                    if (strtolower($address['city']) == strtolower($searchFields['city'])){
                                        $socialStruct = Icm_Struct_Social::fromArray($record);
                                        $socialCollect->add($socialStruct);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $socialCollect;

    }

    public function getHttpClient() {
        if (is_null($this->httpClient)) {
            $this->httpClient = new Icm_Util_Http_Client();
        }
        return $this->httpClient;
    }

    public function setHttpClient(Icm_Util_Http_Client $c) {
        $this->httpClient = $c;
    }


}