<?php

/**
 *
 */
class Icm_Service_Whitepages_Api implements Icm_Configurable
{
    const URL_FREE = 'http://api.whitepages.com';
    const URL_PAID = 'http://proapi.whitepages.com';

    const MODE_FREE = 'free';
    const MODE_PAID = 'paid';

    const METHOD_REVERSEPHONE = 'reverse_phone';
    const METHOD_FIND_PERSON = 'find_person';
    const METHOD_REVERSE_ADDRESS = 'reverse_address';

    const OUTPUT_JSON = 'JSON';
    const OUTPUT_XML = 'XML';

    const PARAM_PHONE = 'phone';

    /**
     * @var Icm_Config;
     */
    protected $config;

    private $mode;

    private $api_key;

    private $version;

    private $outputFormat;

    public function __construct(Icm_Config $config = null) {
        if (!empty($config)) {
            $this->setApiKey($config->getOption('apiKey'));
        }
    }

    /**
     * @static
     * @param $apiKey
     * @param $mode
     * @param $version
     * @param $output
     * @return Icm_Service_Whitepages_Api
     */
    public static function create($apiKey, $mode = self::MODE_FREE, $version = '1.0', $output = self::OUTPUT_JSON) {
        $wp = new Icm_Service_Whitepages_Api();
        $wp->setApiKey($apiKey);
        $wp->setMode($mode);
        $wp->setVersion($version);
        $wp->setOutput($output);

        return $wp;
    }


    /**
     * @param $phone_number
     * @return string|void
     */
    public function reversePhone($phone_number) {
        return $this->execute(self::METHOD_REVERSEPHONE, array(
            self::PARAM_PHONE => $phone_number
        ));
    }

    /**
     * @param $method
     * @param $params
     * @return array|SimpleXMLElement
     */
    public function execute($method, $params) {
        $curl = new Icm_Util_Http_Client();
        $curl->setUrl($this->getUrl($method, $params));
        try{
            $contents = $curl->execute();
        } catch(Icm_Util_Http_Exception $e) {
            throw new Icm_Service_Whitepages_Exception($e->getMessage(), $e->getCode());
        }

        switch($this->getOutput()) {
            case self::OUTPUT_JSON:
                $contents = json_decode($contents, true);
            break;
            case self::OUTPUT_XML:
                $contents = simplexml_load_string($contents);
            break;
        }
        return $contents;
    }

    /**
     * @param $output
     */
    public function setOutput($output) {
        $this->outputFormat = $output;
    }

    /**
     * @param $modeFlag
     */
    public function setMode($modeFlag) {
        $this->mode = $modeFlag;
    }


    /**
     * @param $api_key
     */
    public function setApiKey($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * @param string $version
     */
    public function setVersion($version = '1.0') {
        $this->version = $version;
    }

    /**
     * @param $method
     * @param $params
     * @return string
     */
    public function getUrl($method, $params) {
        $url = implode('/', array($this->getDomain(), $method, $this->getVersion()));
        if ($this->outputFormat && !array_key_exists('outputtype', $params)) {
            $params = array_merge_recursive($params, array('outputtype' => $this->getOutput()));
        }
        $params = array_merge_recursive($params, array('api_key' => $this->getApiKey()));
        $url .= '?' . http_build_query($params, null, ';');
        return $url;
    }

    /**
     * @return mixed
     */
    public function getOutput() {
        return $this->outputFormat;
    }

    /**
     * @return string
     */
    public function getDomain() {
        return self::MODE_FREE == $this->getMode() ? self::URL_FREE : self::URL_PAID;
    }

    /**
     * @return mixed
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * @return mixed
     */
    public function getMode() {
        return $this->mode;
    }

    /**
     * @return mixed
     */
    public function getApiKey() {
        return $this->api_key;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Icm_Config $c) {
        $this->config = $c;
        if ($this->config->apiKey) {
            $this->setApiKey($this->config->apiKey);
        }
    }
}
