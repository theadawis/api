<?php
/**
 * Icm_Service_Litle_Api.php
 * User: chris
 * Date: 12/18/12
 * Time: 11:15 AM
*/
class Icm_Service_Litle_Api extends Icm_Service_Api
{

    const CARD_AMEX = "AX";
    const CARD_MASTERCARD = "MC";
    const CARD_VISA = "VI";
    const CARD_DISCOVER = "DI";
    const CARD_DINERS = "DC";
    const LITLE_PORT = 8002;

    protected static  $CARD_TYPEMAP = array (
        Icm_Commerce_Creditcard::CARD_TYPE_AMEX => self::CARD_AMEX,
        Icm_Commerce_Creditcard::CARD_TYPE_DISCOVER => self::CARD_DISCOVER,
        Icm_Commerce_Creditcard::CARD_TYPE_DINERS => self::CARD_DINERS,
        Icm_Commerce_Creditcard::CARD_TYPE_MASTERCARD => self::CARD_MASTERCARD,
        Icm_Commerce_Creditcard::CARD_TYPE_VISA => self::CARD_VISA,
    );

    /**
     * @var Icm_Util_Http_Client
     */
    protected $httpClient;

    /**
     * @var DOMDocument
     */
    protected $document;

    /**
     * @var Icm_Config
     */
    protected $config;

    /**
     * @var DOMDocument
     */
    protected $root;

    /**
     * @var bool
     */
    public $sandbox = false;

    /**
     * @param Icm_Config $config
     */
    public function __construct(Icm_Config $config) {
        $this->config = $config;
    }

    /**
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @param string $reportGroup
     * @internal param Icm_Commerce_Creditcard $card
     * @return SimpleXMLElement
     */
    public function chargeCard(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order, $reportGroup = "") {
        $this->createDocument();
        $this->validateCustomer($customer);
        $this->validateOrder($order);

        $creditCard = $customer->getCreditCard();
        $creditCardType = $creditCard->getCardType();

        $saleNode = $this->document->createElement("sale");
        $saleNode->setAttribute("id", "");
        $saleNode->setAttribute("reportGroup",  $reportGroup);
        $saleNode->setAttribute("customerId", $customer->customer_id);
        $saleNode->appendChild($this->document->createElement("orderId", $order->getOrderId()));
        $saleNode->appendChild($this->document->createElement("amount", $order->getSubtotal() * 100));
        $saleNode->appendChild($this->document->createElement("orderSource", "ecommerce"));

        $addressNode = $this->document->createElement("billToAddress");
        $addressNode->appendChild($this->document->createElement("name", $creditCard->getFullName()));
        $addressNode->appendChild($this->document->createElement("addressLine1", $customer->address1));
        $addressNode->appendChild($this->document->createElement("addressLine2", $customer->address2));
        $addressNode->appendChild($this->document->createElement("city", $customer->city));
        $addressNode->appendChild($this->document->createElement("email", $customer->email));
        $addressNode->appendChild($this->document->createElement("phone", $customer->phone));

        $cardNode = $this->document->createElement("card");
        $cardNode->appendChild($this->document->createElement("type", self::$CARD_TYPEMAP[$creditCardType]));
        $cardNode->appendChild($this->document->createElement("number", $creditCard->getNumber()));
        $cardNode->appendChild($this->document->createElement("expDate", $creditCard->formatExpiration("my")));
        $cardNode->appendChild($this->document->createElement("cardValidationNum", $creditCard->cvv));

        $saleNode->appendChild($addressNode);
        $saleNode->appendChild($cardNode);
        $this->root->appendChild($saleNode);
        $result = $this->post();

        return simplexml_load_string($result);
    }

    protected function post() {
        $name = $this->sandbox ? "test_url" : "url";
        $url = $this->config->getOption($name);

        // Load proxy flag for processing
        $proxy = $this->config->getOption('proxy', true);

        // If Litle is to use the proxy, select from web01-web08 only.  Send request to /test if we are in sandbox mode
        if ($proxy) {
            $url = ($this->sandbox)
                ? 'web0' . mt_rand(1, 8) . '.pub.instantcheckmate.com:' . self::LITLE_PORT . '/test'
                : 'web0' . mt_rand(1, 8) . '.pub.instantcheckmate.com:' . self::LITLE_PORT;
        }

        if (is_null($url)) {
            throw new Icm_Service_Litle_Exception("You must have '$name' defined in your configuration");
        }

        $this->getHttpClient()->setOption(CURLOPT_URL, $url);
        $this->getHttpClient()->setOption(CURLOPT_TIMEOUT, 10);
        $this->getHttpClient()->setOption(CURLOPT_POST, true);
        $this->getHttpClient()->setOption(CURLOPT_SSL_VERIFYHOST, 2);
        $this->getHttpClient()->setOption(CURLOPT_TIMEOUT, 10);

        // If $url does not match a port regular expression, set the default port to 443
        if (!preg_match('/:\d{2,4}/', $url)) {
            $this->getHttpClient()->setOption(CURLOPT_PORT, 443);
        }

        $xml = $this->document->saveXML($this->root);

        return $this->getHttpClient()->post($xml);
    }

    protected function createDocument() {
        // $this->document = new DOMDocument("1.0", "UTF-8");
        $this->document = new DOMDocument();
        $root = $this->document->createElement("litleOnlineRequest");
        $root->setAttribute("version", "8.11");
        $root->setAttribute("merchantId", $this->config->getOption("merchant_id"));
        $root->setAttribute("xmlns", "http://www.litle.com/schema");

        $this->root = $root;

        $authentication = $this->document->createElement("authentication");

        $usernameKey = $this->sandbox ? "test_user" : "user";
        $passwordKey = $this->sandbox ? "test_password" : "password";
        $authArray = array(
            'user' => $this->config->getOption($usernameKey),
            'password' => $this->config->getOption($passwordKey)
        );

        foreach ($authArray as $nodeName => $nodeValue) {

            if (is_null($nodeValue)) {
                throw new Icm_Service_Litle_Exception("Litle must have a config value for $nodeName");
            }

            $node = $this->document->createElement($nodeName, $nodeValue);
            $authentication->appendChild($node);
        }

        $this->root->appendChild($authentication);

    }

    protected function validateCustomer(Icm_Commerce_Customer $customer) {
        $creditCard = $customer->getCreditCard();

        if (is_null($creditCard)) {
            throw new Icm_Service_Litle_Exception("The customers credit card object may not be null");
        }

        $this->validateCreditCard($creditCard);
    }

    protected function validateCreditCard(Icm_Commerce_Creditcard $creditCard) {
        $creditCardType = $creditCard->getCardType();

        if (is_null($creditCardType)) {
            throw new Icm_Service_Litle_Exception("Credit card type is null");
        }

        if (!array_key_exists($creditCardType, self::$CARD_TYPEMAP)) {
            throw new Icm_Service_Litle_Exception("Invalid credit card type: $creditCardType");
        }

        if (!$creditCard->isValidCCNumber()) {
            throw new Icm_Service_Litle_Exception("Invalid credit card number: {$creditCard->getNumber()}");
        }
    }

    protected function validateOrder(Icm_Commerce_Order $order) {
        if (!$order->getOrderId()) {
            throw new Icm_Service_Litle_Exception("Order id must be a truthy value");
        }
    }

    /**
     * @param Icm_Commerce_Transaction $transaction
     * @param string $reportGroup
     * @return SimpleXMLElement
     */
    public function void(Icm_Commerce_Transaction $transaction, $reportGroup = '') {
        $this->createDocument();
        $voidNode = $this->document->createElement("void");
        $voidNode->setAttribute("id", $transaction->getTransactionId());
        $voidNode->setAttribute("reportGroup", $reportGroup);
        $txnId = $this->document->createElement("litleTxnId", $transaction->gateway_txid);
        $voidNode->appendChild($txnId);
        $this->root->appendChild($voidNode);
        return simplexml_load_string($this->post());
    }

    public function setSandbox($bool) {
        $this->sandbox = $bool;
    }
}
