<?php

/**
 * Api.php
 * User: chris
 * Date: 12/18/12
 * Time: 10:59 AM
 */
class Icm_Service_Orange_Api extends Icm_Service_Api{
    const TRANSACTION_TYPE_PURCHASE = 'PURCHASE';
    const TRANSACTION_TYPE_CAPTURE = 'CAPTURE';
    const TRANSACTION_TYPE_PREAUTH = 'PREAUTH';
    const TRANSACTION_TYPE_REFUND = 'REFUND';
    const TRANSACTION_STATUS_ON_HOLD = 'ON HOLD';
    const TRANSACTION_STATUS_INQUEUE = 'INQUEUE';
    const TRANSACTION_STATUS_APPROVED = 'APPROVED';
    const TRANSACTION_STATUS_DECLINED = 'DECLINED';
    const TRANSACTION_STATUS_ERROR = 'ERROR';

    const ORANGE_ICM_INSTANCE = 1;
    const ORANGE_TCG_INSTANCE = 2;

    protected $orangeUrls = array(
        self::ORANGE_ICM_INSTANCE => array(
            'https://icmapi.orangecrm.com/icm/crmapi.nsf/gateway?openagent',
            'https://icm2api.orangecrm.com/icm/crmapi.nsf/gateway?openagent',
        ),
        self::ORANGE_TCG_INSTANCE => array(
            'https://tcgapi.orangecrm.com/tcg/crmapi.nsf/gateway?openagent',
            'https://tcg2api.orangecrm.com/tcg/crmapi.nsf/gateway?openagent',
        ),
    );

    protected $defaultConfig = array(
        'transactionKey' => '8RQT42',
        'customerKey' => '8HV2VX',
        'testKey' => '94Y5F5',
        'testTransactionKey' => '94Y64A',
        'ccCustomerKey' => '93SVFR'
    );

    protected $config = array();

    protected $useTestToken = false;

    public function __construct(Icm_Config $config) {
        // setup rest of config
        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $config->getOption($key, $default);
        }

        $this->getHttpClient();
    }

    /**
     * Add a customer to Orange
     * @param Icm_Commerce_Customer $customer
     * @param Icm_Commerce_Order $order
     * @return array
     */
    public function addCustomer(Icm_Commerce_Customer $customer, Icm_Commerce_Order $order) {
        $card = $customer->getCreditCard();
        $products = $order->getProducts();
        $products->rewind();
        $product = $products->current();
        $product = $product[0]; //products are returned as array(object, quantity)
        $variation = $customer->getVaration();

        // build the request data
        $data = array(
            'q_action'=> 'CUST_ADD',
            'q_cust_status' => 'NEW',
            'q_system_key' => $this->useTestToken() ? $this->config['testKey'] : $product->orange_cust_token,
            'q_cust_first_name' => $customer->first_name,
            'q_cust_last_name' => $customer->last_name,
            'q_cust_username' => $customer->email,
            'q_cust_affiliatecode' => $customer->getVisit()->getAffiliate()->short_name, // report group or tracking src (affiliate short name)
            'q_cust_affiliatesub' => $customer->getVisit()->sub_id, //tracking cnt   (affiliate sub_id)
            'q_cust_extid' => $order->getOrderId(),
            'q_cust_salespage' => $variation->getTestId() . ':' . $variation->getVariationId(), // reg page version (test:varation)
            'q_cust_webusername' => $customer->email,
            'q_cust_webpassword' => strtoupper($customer->password), //TODO: This is horrible. Orange converts user passwords to upper case on CUST_UPDATE calls, so we have to do it here. See Chris G. for a lighter if you feel the need to burn your eyeballs out after reading this.
            'q_cust_record' => $customer->getVisit()->ip_address, // customer's ip address
            'q_cust_bill_address1' => $customer->address1,
            'q_cust_bill_city' => $customer->city,
            'q_cust_bill_state' => $customer->state,
            'q_cust_bill_zip' => !empty($customer->zip) ? $customer->zip : 0,
            'q_cust_bill_country' => 'US', // change this to accept international customers
            'q_cust_ship_address1' => $customer->address1,
            'q_cust_ship_city' => $customer->city,
            'q_cust_ship_state' => $customer->state,
            'q_cust_ship_zip' => !empty($customer->zip) ? $customer->zip : 0,
            'q_cust_ship_country' => 'US', // change this to accept international customers
            'q_cust_email' => $customer->email,
            'q_cust_phone' => $customer->phone,
            'q_cust_ccname' => $card->first_name.' ' . $card->last_name,
            'q_cust_ccacct' => $card->number,
            'q_cust_ccexpire' => $card->expiration_month.$card->expiration_year,
            'q_cust_order' => date('Ymd', strtotime($order->created)),
            'q_cust_sold' => $product->name,
            'q_cust_category' => '' // category assigned to customer set (we don't currently use this)
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        $result = $this->request($data, $urls);

        return $result;
    }

    public function addTransaction(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway, $type = self::TRANSACTION_TYPE_PURCHASE, $status = self::TRANSACTION_STATUS_APPROVED){
        $data = array(
            'q_action' => 'TRAN_ADD',
            'q_system_key' => $this->useTestToken ? $this->config['testTransactionKey'] : $productGateway->token, //TODO: this needs to come from the customer object
            'q_cust_guid' => $customer->guid,
            'q_tran_type' => $type, // PURCHASE, CAPTURE, PREAUTH, REFUND
            'q_tran_status' => $status,  // ON HOLD, INQUEUE, APPROVED, DECLINED, ERROR
            'q_tran_amount' => $transaction->amount,
            'q_tran_respcode' => "empty",
            'q_tran_issue' => date('Ymd', strtotime($transaction->created)),
            'q_tran_txn' => $transaction->gateway_txid
        );

        try{
            $creditcard = $customer->getCreditCard();
        }
        catch (Exception $e){
            $creditcard = false;
        }

        if ($creditcard){
            $data['q_tran_ccname'] = $creditcard->first_name.' ' . $creditcard->last_name;
            $data['q_tran_ccacct'] = $creditcard->number;
            $data['q_tran_ccexpire'] = $creditcard->expiration_month.substr($creditcard->expiration_year, 2);
        }

        if (!empty($transaction->transaction_id)){
            $data['q_tran_txn'] = $transaction->transaction_id;
            $data['q_tran_export'] = date('Ymd', strtotime($transaction->created));
        }

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    public function refundCustomer(Icm_Commerce_Customer $customer, Icm_Commerce_Transaction $transaction, Icm_Commerce_Product_Gateway $productGateway){
        $data = array(
            'q_action' => 'TRAN_REFUND',
            'q_system_key' => $this->useTestToken ? $this->config['testTransactionKey'] : $productGateway->token,
            'q_tran_refund_amt' => abs($transaction->amount),
            'q_tran_guid' => $transaction->orange_guid,
            'q_tran_status' => 'APPROVED'
        );

        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Authenticate a user
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    public function authorizeCustomer(Icm_Commerce_Customer $customer){
        $data = array(
            'q_action' => 'CUST_AUTH',
            'q_system_key' => $this->useTestToken() ? $this->config['testKey'] : $this->config['customerKey'],
            'q_cust_username' => $customer->email,
            'q_cust_password' => $customer->password
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Cancel a customer
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    public function cancelCustomer(Icm_Commerce_Customer $customer){
        $data = array(
            'q_action' => 'CUST_UPDATE',
            'q_system_key'=> $this->useTestToken() ? $this->config['testKey'] : $this->config['customerKey'],
            'q_cust_guid' => $customer->guid,
            'q_cust_status' => 'CANCELED'
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Archive a customer
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    public function archiveCustomer(Icm_Commerce_Customer $customer){
        $data = array(
            'q_action' => 'CUST_UPDATE',
            'q_system_key'=> $this->useTestToken() ? $this->config['testKey'] : $this->config['customerKey'],
            'q_cust_guid' => $customer->guid,
            'q_cust_status' => 'ARCHIVED'
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Add a comment to a customer
     *
     * @param Icm_Commerce_Customer $customer
     * @param $subject
     * @param $comment
     * @param string $creator
     *
     * @return array
     */
    public function addCustomerComment(Icm_Commerce_Customer $customer, $subject, $comment, $creator = "From_Website") {
        $data = array(
            'q_action' => 'CMT_ADD',
            'q_system_key' => $this->config['customerKey'],
            'q_cust_guid' => $customer->guid,
            'q_cmt_subject' => $subject,
            'q_cmt_message' => $comment,
            'q_cmt_creator' => $creator
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Modify a customer record
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    public function updateCustomer(Icm_Commerce_Customer $customer){
        $data = array(
            'q_action' => 'CUST_UPDATE',
            'q_system_key'=> $this->useTestToken() ? $this->config['testKey'] : $this->config['customerKey'],
            'q_cust_guid' => $customer->guid,
            'q_cust_first_name' => $customer->first_name,
            'q_cust_last_name' => $customer->last_name,
            'q_cust_email' => $customer->email,
            'q_cust_webusername' => $customer->email
        );

        if ($customer->password){
            $data['q_cust_webpassword'] = $customer->password;
        }

        try{
            $creditCard = $customer->getCreditCard();
        }
        catch (Exception $e){
            $creditCard = false;
        }

        if ($creditCard){
            $data['q_cust_ccname'] = $creditCard->first_name.' ' . $creditCard->last_name;
            $data['q_cust_ccacct'] = $creditCard->number;
            $data['q_cust_ccexpire'] = $creditCard->expiration_month.substr($creditCard->expiration_year, 2);
        }

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    /**
     * Retrieve customer data from Orange
     * @param Icm_Commerce_Customer $customer
     * @throws Icm_Service_Orange_Exception
     * @return array
     */
    public function getCustomer(Icm_Commerce_Customer $customer, $getCcInfo = false){
        if (!$customer->guid){
            throw new Icm_Service_Orange_Exception('The customer must have an Orange guid.');
        }

        $data = array(
            'q_action' => 'CUST_QUERY',
            'q_system_key' => $getCcInfo ? $this->config['ccCustomerKey'] : $this->config['customerKey'],
            'q_cust_guid' => $customer->guid
        );

        // get urls from customer
        $urls = $this->getUrls($customer);

        return $this->request($data, $urls);
    }

    protected function request($data, $urls = array()){
        $request = '';
        $comma = '';

        foreach ($data as $key => $value){
            $request .= "{$comma}{$key}={$value}";
            $comma = '&';
        }

        do {
            $failure = false;

            try {
                $url = array_shift($urls);
                $result = $this->httpClient->setUrl($url)->post($request);
            }
            catch (Icm_Util_Http_Exception $e){
                $failure = true;
            }
        } while ($failure && count($urls));

        if ($failure){
            // we tried all Orange urls and all failed
            throw new Icm_Service_Orange_Exception("Attempted to use all Orange URLs. All failed.", 1);
        }
        return $this->stripHTML($result);
    }

    /**
     * Get the right set of orange urls to use for the customer
     *
     * @param Icm_Commerce_Customer $customer
     * @return array
     */
    protected function getUrls($customer) {
        // default to the new instance
        $urls = $this->orangeUrls[self::ORANGE_TCG_INSTANCE];

        // use the customer's config if set
        if (isset($customer->crm_config_id)) {
            $urls = $this->orangeUrls[$customer->crm_config_id];
        }

        return $urls;
    }

    /**
     * AKA omgorangesuckssomuchwehavetodothishackyshit
     * @param string $string
     * @return array
     */
    protected function stripHTML($string){
        $string = str_replace("<html>\n<head>\n</head>\n<body text=\"#000000\">\n", '', $string);
        $string = str_replace("</body>\n</html>", '', $string);
        $string = str_replace("\n", '', $string);
        parse_str($string, $string);
        return $string;
    }

    public function useTestToken($bool = null) {
        if (!is_null($bool)) {
            $this->useTestToken = $bool;
        }

        return $this->useTestToken;
    }
}