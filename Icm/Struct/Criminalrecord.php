<?php
class Icm_Struct_Criminalrecord extends Icm_Struct{
    public $first_name, $middle_name, $last_name, $generation, $dob, $birth_state, $age;
    public $case_number, $aka1, $aka2, $dob_aka;
    public $address, $address2, $city, $state, $zip;
    public $hair_color, $eye_color, $height, $weight, $race, $sex, $skin_tone, $scars_marks;
    public $military_service, $charge_category, $charges_filed_date, $offense_date, $offense_code, $offense_description1, $offense_description2;
    public $ncic_code, $counts, $plea, $conviction_date, $conviction_place, $court, $source, $sentence_date, $probation_date;
    public $disposition, $disposition_date, $court_costs, $arresting_agency, $case_type, $fines, $source_name, $source_state;

    public function __toString() {
        return $this->case_number;
    }
}