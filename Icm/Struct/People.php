<?php
class Icm_Struct_People extends Icm_Struct{
    public $validitydate;
    public $lastname;
    public $firstname;
    public $middlename;
    public $dob;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $county;
    public $phone;
    public $personid;
}
