<?php
class Icm_Struct_Lexis_ProfessionalLicense extends Icm_Struct{
    /**
     * @var Icm_Entity_Location
     */
    public $address;

    public $first_name,
        $middle_name,
        $last_name,
        $name_prefix,
        $number,
        $status,
        $type,
        $issuedDate,
        $licenseState,
        $expirationDate,
        $board;
}