<?php
class Icm_Struct_Lexis_Property extends Icm_Struct{
    /**
     * @var Icm_Entity_Location
     */
    public $address;

    /**
     * @var Icm_Entity_Location
     */
    public $ownerAddress;

    public $lender,
        $ownerName,
        $sellerName,
        $salePrice,
        $saleDate,
        $recordingDate,
        $type,
        $titleCompany,
        $landUse;
}