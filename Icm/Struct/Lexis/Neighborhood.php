<?php
class Icm_Struct_Lexis_Neighborhood extends Icm_Struct{
    /**
     * @var Icm_Entity_Location
     */
    public $subjectAddress;

    /**
     * @var Icm_Entity_Location[]
     */
    public $neighborAddresses = array();
}