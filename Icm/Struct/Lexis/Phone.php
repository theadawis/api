<?php
/**
 * A Lexis phone record
 * @author Joe Linn
 *
 */
class Icm_Struct_Lexis_Phone extends Icm_Struct{
    public $number,
        $dateLastSeen,
        $dateFirstSeen,
        $listingName,
        $first_name,
        $last_name,
        $public;
}