<?php
class Icm_Struct_Lexis_Bankruptcy extends Icm_Struct{
    public $originalCaseNumber,
        $meetingDate,
        $meetingAddress,
        $judgeName,
        $disposition,
        $dischargeDate,
        $filingDate,
        $caseNumber,
        $filingStatus,
        $originalFilingDate,
        $filerType;
    public $debtors = array();
    public $attorneys = array();
    public $chapter,
        $courtName,
        $originalChapter;
    public $trustees = array();
    public $courtCode;
}