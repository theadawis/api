<?php
class Icm_Struct_Lexis_CorporateAffiliation extends Icm_Struct{
    /**
     * @var Icm_Entity_Location
     */
    public $address;

    public $first_name,
        $last_name,
        $namePrefix,
        $title,
        $corporationNumber,
        $recordDate,
        $recordType,
        $status,
        $state,
        $companyName,
        $statusDescription,
        $filingDate;
}