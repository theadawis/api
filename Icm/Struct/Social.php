<?php

class Icm_Struct_Social extends Icm_Struct{
        public  $query_params_match, $query_person_match, $source, $names,
                    $addresses, $phones, $emails,
                    $jobs, $educations, $images,
                    $usernames, $user_ids, $dobs,
                    $related_urls, $relationships, $tags;
}