<?php
class Icm_Struct_OptoutRequest extends Icm_Struct {

    public $id;
    public $status;
    public $first_name;
    public $last_name;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $dob;
    public $email;
    public $authentication_hash;
    public $order_id;
    public $ip_address;
    public $notes;
    public $possible_relatives;
    public $date_added;
    public $added_csr_id;
    public $date_approved;
    public $approved_csr_id;

}
