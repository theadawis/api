<?php
class Icm_Struct_Cellphone extends Icm_Struct{
    public $first_name, $last_name;
    public $address, $city, $state, $zip, $phone;
    public $provider, $type;
}