<?php
class Icm_Struct_Dealicense extends Icm_Struct{
    public $dea_number, $business_type, $schedules, $expiration_date;
    public $last_name, $first_name, $address, $city, $state, $zip, $county;
}