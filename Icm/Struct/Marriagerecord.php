<?php
class Icm_Struct_Marriagerecord extends Icm_Struct{
    public $certificate_number, $volume_number;
    public $first_person_last_name, $first_person_first_name, $first_person_middle_name;
    public $second_person_last_name, $second_person_first_name, $second_person_middle_name;
    public $county, $state, $date;
}