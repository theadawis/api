<?php

class Icm_Struct_WebResults extends Icm_Struct{
    public $date, $clickurl, $url, $dispurl, $title, $abstract;
    public $score, $provider, $keyterms, $author, $icon;
}
