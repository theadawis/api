<?php
class Icm_Struct_OptoutRequestOtherLocation extends Icm_Struct {

    public $id;
    public $optout_request_id;
    public $city;
    public $state;
    public $date_created;

}
