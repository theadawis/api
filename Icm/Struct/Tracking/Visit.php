<?php
class Icm_Struct_Tracking_Visit extends Icm_Struct {

    public $visit_id;
    public $app_id;
    public $visitor_id;
    public $affiliate_link_id;
    public $affiliate_id;
    public $campaign_id;
    public $sub_id;
    public $s1;
    public $s2;
    public $s3;
    public $s4;
    public $s5;
    public $params;
    public $payout;
    public $ip_address;
    public $is_duplicate;
    public $is_converted;
    public $is_pixel_fired;
    public $useragent;
    public $converted;
    public $referer;
    public $updated;
    public $created;

    /**
     * @var Icm_Struct_Marketing_Affiliate
     */
    protected $affiliate;

    /**
     * @param Icm_Struct_Marketing_Affiliate $affiliate
     * @return Icm_Struct_Tracking_Visit
     */
    public function setAffiliate(Icm_Struct_Marketing_Affiliate $affiliate){
        $this->affiliate = $affiliate;
        return $this;
    }

    /**
     * @return Icm_Struct_Marketing_Affiliate
     */
    public function getAffiliate(){
        if (!$this->affiliate instanceof Icm_Struct_Marketing_Affiliate){
            throw new Icm_Exception("Attempting to get an affiliate when no affiliate has been set.");
        }
        return $this->affiliate;
    }
}
