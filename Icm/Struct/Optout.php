<?php
class Icm_Struct_Optout extends Icm_Struct {

    public $id;
    public $first_name;
    public $last_name;
    public $optout_request_id;
    public $source_id;
    public $external_person_id;
    public $dob;
    public $city;
    public $state;
    public $zip;
    public $date_created;

}