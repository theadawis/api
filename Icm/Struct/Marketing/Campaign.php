<?php
class Icm_Struct_Marketing_Campaign extends Icm_Struct {

    public $id;
    public $name;
    public $short_name;
    public $app_id;
    public $default_payout;
    public $conversion_split_context_id;
    public $status;
    public $created;
    public $updated;

}
