<?php
class Icm_Struct_Marketing_Affiliate_Link extends Icm_Struct {

    public $id;
    public $hash;
    public $label;
    public $affiliate_id;
    public $campaign_id;
    public $landing_page_id;
    public $payout;
    public $status;
    public $created;
    public $updated;

}
