<?php
class Icm_Struct_Marketing_Affiliate_Contact extends Icm_Struct {

    public $id;
    public $affiliate_id;
    public $type;
    public $first_name;
    public $last_name;
    public $email;
    public $primary_phone;
    public $secondary_phone;
    public $street;
    public $city;
    public $state;
    public $zip;
    public $created;
    public $updated;

}
