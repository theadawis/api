<?php
class Icm_Struct_Marketing_Pixel extends Icm_Struct {

    public $id;
    public $affiliate_id;
    public $campaign_id;
    public $subid;
    public $label;
    public $status;
    public $postback_url;
    public $pixel_code;
    public $created;
    public $updated;

}
