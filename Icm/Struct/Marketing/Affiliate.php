<?php
class Icm_Struct_Marketing_Affiliate extends Icm_Struct {

    public $id;
    public $company_name;
    public $category;
    public $risk_mitigation;
    public $short_name;
    public $notes;
    public $created;
    public $updated;
    public $type;
    public $time_zone;
    public $status;
}
