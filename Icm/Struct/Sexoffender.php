<?php
class Icm_Struct_Sexoffender extends Icm_Struct{
    public $first_name, $last_name, $middle_name, $suffix;
    public $image, $dob, $case_number, $risk_level, $aliases;
    public $address, $address2, $city, $state, $zip, $county;
    public $offender_status, $offender_category;
    public $hair_color, $eye_color, $height, $weight, $race, $sex, $skin_tone, $scars_marks;
    public $offense_date, $offense_code, $offense_description1, $offense_description2, $counts, $conviction_date, $conviction_place;
    public $victims_age, $victim_minor_flag, $victim_sex, $court, $source;
}