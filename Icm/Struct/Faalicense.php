<?php
class Icm_Struct_Faalicense extends Icm_Struct{
    public $first_name, $last_name, $middle_name;
    public $address, $address2, $city, $state, $zip;
    public $faa_number, $medical_exp_date, $certifications;
    public $location;
}
