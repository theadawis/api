<?php
interface Icm_Hashable{
    /**
     * Returns a UNIQUE string based on the current instance of an object.
     * Note that this string should not change. Ever.
     * @return string
     */
    public function getHash();
}