<?php
/**
 * @author Joe Linn
 *
 */
class Icm_OAuth2{
    const CLIENT_ID_REGEXP = "/^[a-z0-9-_]{3, 32}$/i";

    const RESPONSE_TYPE_ACCESS_TOKEN = 'token';
    const RESPONSE_TYPE_AUTH_CODE = 'code';
    const RESPONSE_TYPE_CODE_AND_TOKEN = 'code-and-token';
    const RESPONSE_TYPE_REGEXP = "/^(token|code|code-and-token)$/";

    const GRANT_TYPE_AUTH_CODE = 'authorization_code';
    const GRANT_TYPE_IMPLICIT = 'token';
    const GRANT_TYPE_USER_CREDENTIALS = 'password';
    const GRANT_TYPE_CLIENT_CREDENTIALS = 'client_credentials';
    const GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';
    const GRANT_TYPE_EXTENSIONS = 'extensions';
    const GRANT_TYPE_REGEXP = '#^(authorization_code|token|password|client_credentials|refresh_token|http://.*)$#';

    const PARAM_NAME_TOKEN = 'access_token';

    const BEARER_HEADER_NAME = 'Bearer';

    const TOKEN_TYPE_BEARER = 'bearer';

    const HTTP_FOUND = '302 Found';
    const HTTP_BAD_REQUEST = '400 Bad Request';
    const HTTP_UNAUTHORIZED = '401 Unauthorized';
    const HTTP_FORBIDDEN = '403 Forbidden';
    const HTTP_UNAVAILABLE = '503 Service Unavailable';

    const ERROR_INVALID_REQUEST = 'invalid_request';
    const ERROR_INVALID_CLIENT = 'invalid_client';
    const ERROR_UNAUTHORIZED_CLIENT = 'unauthorized_client';
    const ERROR_REDIRECT_URI_MISMATCH = 'redirect_uri_mismatch';
    const ERROR_USER_DENIED = 'access_denied';
    const ERROR_UNSUPPORTED_RESPONSE_TYPE = 'unsupported_response_type';
    const ERROR_INVALID_SCOPE = 'invalid_scope';
    const ERROR_INVALID_GRANT = 'invalid_grant';
    const ERROR_UNSUPPORTED_GRANT_TYPE = 'unsupported_grant_type';
    const ERROR_INVALID_TOKEN = 'invalid_token';
    const ERROR_EXPIRED_TOKEN = 'expired_token';
    const ERROR_INSUFFICIENT_SCOPE = 'insufficient_scope';
    const ERROR_UNSUPPORTED_OUTPUT_FORMAT = 'unsupported_output_format';

    const OUTPUT_FORMAT_JSON = 'json';
    const OUTPUT_FORMAT_XML = 'xml';

    /**
     * @var Icm_OAuth2_Storage_Interface
     */
    protected $storage;

    protected $oldToken;

    protected $outputFormat;

    protected $config = array();

    protected $defaultConfig = array(
        'accessTokenLifetime' => 3600, //1 hour
        'authcodeLifetime' => 30,
        'refreshTokenLifetime' => 1209600, //2 weeks
        'realm' => 'ICM',
        'enforceRedirect' => false, //set to true to force client to provide a redirect URI for authorization and tokens
        'enforceState' => false, //set to true to force client to pass state for authorization
        'supportedScopes' => 'basic',
        'defaultOutputFormat' => 'json'
    );

    public function __construct(Icm_OAuth2_Storage_Interface $storage, Icm_Config $config = NULL){
        $this->storage = $storage;

        if (is_null($config)){
            $config = Icm_Config::fromArray(array());
        }

        foreach ($this->defaultConfig as $key => $default){
            $this->config[$key] = $config->getOption($key, $default);
        }
    }

    /**
     * Verifies that the given access token has valid and has access to the requested scope when applicable.
     * @param string $token
     * @param string $scope
     * @throws Icm_OAuth2_Authexception
     * @return Icm_OAuth2_Accesstoken
     */
    public function verifyAccessToken($token, $scope = NULL){
        $tokenType = self::TOKEN_TYPE_BEARER; //make this dynamic to support other token types
        $realm = $this->config['realm'];

        if (!is_string($token)){
            // access token not provided
            throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'A token string must be provided.', $scope);
        }

        $token = $this->storage->getAccessToken($token);

        if (is_null($token)){
            throw new Icm_OAuth2_Authexception(self::HTTP_UNAUTHORIZED, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'Invalid access token provided.', $scope);
        }

        if (!isset($token->expires) || !isset($token->clientID)){
            throw new Icm_OAuth2_Authexception(self::HTTP_UNAUTHORIZED, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'Access token is missing a client ID or it has expired.', $scope);
        }

        if (isset($token->expires) && time() > $token->expires){
            throw new Icm_OAuth2_Authexception(self::HTTP_UNAUTHORIZED, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The provided access token has expired.', $scope);
        }

        if ($scope && (!isset($token->scope) || !$token->scope || !$this->checkScope($scope, $token->scope))){
            throw new Icm_OAuth2_Authexception(self::HTTP_FORBIDDEN, $tokenType, $realm, self::ERROR_INSUFFICIENT_SCOPE, 'You do not have access to the scope required to complete this request.', $scope);
        }

        return $token;
    }

    /**
     * Retrieve the access token string from various auth methods
     * @throws Icm_OAuth2_Authexception
     * @return string access token
     */
    public function getBearerToken(){
        if (isset($_SERVER['HTTP_AUTHORIZATION'])){
            $headers = trim($_SERVER['HTTP_AUTHORIZATION']);
        }
        else if (function_exists('apache_request_headers')){
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));

            if (isset($requestHeaders['Authorization'])){
                $headers = trim($requestHeaders['Authorization']);
            }
        }

        $tokenType = self::TOKEN_TYPE_BEARER; //TODO: make this dynamic to support other token types
        $realm = $this->config['realm'];
        $methodsUsed = !empty($headers) + isset($_GET[self::PARAM_NAME_TOKEN]) + isset($_POST[self::PARAM_NAME_TOKEN]);

        if ($methodsUsed > 1){
            throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'Only one authentication method at a time, please.');
        }
        else if ($methodsUsed == 0){
            throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'Access token not found.');
        }

        if (!empty($headers)){
            // header auth
            if (!preg_match('/'.self::BEARER_HEADER_NAME.'\s(\S+)/', $headers, $matches)){
                throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'Invalid auth header.');
            }

            return $matches[1]; //access token
        }

        if (isset($_POST[self::PARAM_NAME_TOKEN])){
            // POST auth
            if ($_SERVER['REQUEST_METHOD'] != 'POST'){
                throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'Request method must be post when token is in request body.');
            }

            if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] != 'application/x-www-form-urlencoded'){
                throw new Icm_OAuth2_Authexception(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'Content type for POST auth must be "application/x-www-form-urlencoded".');
            }
        }

        // must be GET auth
        return $_GET[self::PARAM_NAME_TOKEN];
    }

    /**
     * Check if the required scope is in the available scope
     * @param string or array $required
     * @param string or array $available
     * @return boolean true if required scope is a subset of available scope; false otherwise
     */
    protected function checkScope($required, $available){
        if (!is_array($required)){
            $required = explode(' ', trim($required));
        }

        if (!is_array($available)){
            $available = explode(' ', trim($available));
        }

        return count(array_diff($required, $available)) == 0;
    }

    /**
     * Grant an access token if all parameters / credentials are in order. Throw an exception otherwise.
     * @param array $input
     * @param array $headers
     * @throws Icm_OAuth2_Exception
     */
    public function grantAccessToken(array $input = NULL, array $headers = NULL){
        $filters = array(
            'grant_type' => array('filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => self::GRANT_TYPE_REGEXP),
                'flags' => FILTER_REQUIRE_SCALAR),
            'scope' => array('flags' => FILTER_REQUIRE_SCALAR),
            'code' => array('flags' => FILTER_REQUIRE_SCALAR),
            'redirect_uri' => array('filter' => FILTER_SANITIZE_URL),
            'client_id' => array('flags' => FILTER_REQUIRE_SCALAR),
            'client_secret' => array('flags' => FILTER_REQUIRE_SCALAR),
            'refresh_token' => array('flags' => FILTER_REQUIRE_SCALAR),
        );

        if (!isset($input)){
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                $input = $_POST;
            }
            else{
                $input = $_GET;
            }
        }

        if (is_null($headers)){
            $headers = $this->getAuthHeader();
        }

        $input = filter_var_array($input, $filters);

        if (!$input['grant_type']){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Invalid or absent grant_type parameter.');
        }

        $client = $this->getClientCredentials($input, $headers);

        if ($this->storage->checkClientCredentials($client[0], $client[1]) === false){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'Invalid client credentials.');
        }

        if (!$this->storage->checkGrantType($client[0], $input['grant_type'])){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNAUTHORIZED_CLIENT, 'Requested grant type is not authorized for this client.');
        }

        switch($input['grant_type']){
            case self::GRANT_TYPE_AUTH_CODE:
                if (!($this->storage instanceof Icm_OAuth2_Storage_Grantcode)){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
                }

                if (!$input['code']){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Parameter "code" is required.');
                }

                if ($this->config['enforceRedirect'] && !$input['redirect_uri']){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'The "redirect_uri" parameter is required.');
                }

                $stored = $this->storage->getAuthCode($input['code']); //attempt to retrieve auth code from storage

                if (is_null($stored) || $client[0] != $stored->clientID){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT, 'Access token does not exist.');
                }

                if ($input['redirect_uri'] && !$this->validateRedirectUri($input['redirect_uri'], $stored->redirectUri)){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_REDIRECT_URI_MISMATCH, 'Provided redirect URI does not match stored redirect URI.');
                }

                if ($stored->expires < time()){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT, 'The provided auth code has expired.');
                }
                break;
            case self::GRANT_TYPE_USER_CREDENTIALS:
                if (!($this->storage instanceof Icm_OAuth2_Storage_Grantuser)){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
                }

                if (!$input['username'] || !$input['password']){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Parameters "username" and "password" are required.');
                }

                $stored = $this->storage->checkUserCredentials($client[0], $input['username'], $input['password']);

                if ($stored === false){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT);
                }
                break;
            case self::GRANT_TYPE_CLIENT_CREDENTIALS:
                if (!($this->storage instanceof Icm_OAuth2_Storage_Grantclient)){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
                }

                if (empty($client[1])){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'Parameter "client_secret" is required.');
                }

                $stored = $this->storage->checkClientCredentials($client[0], $client[1]);
                break;
            case self::GRANT_TYPE_REFRESH_TOKEN:
                if (!($this->storage instanceof Icm_OAuth2_Storage_Refreshtoken)){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
                }

                if (!$input['refresh_token']){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Parameter "refresh_token" is required.');
                }

                $stored = $this->storage->getRefreshToken($input['refresh_token']);

                if (is_null($stored) || $client[0] != $stored->clientID){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT, 'Invalid refresh token.');
                }

                if ($stored->expires < time()){
                    throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT, 'Refresh token has expired.');
                }

                $this->oldToken = $stored->token;
                break;
            default:
                throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Unsupported "grant_type" or grant type not specified.');
        }

        if (!isset($stored->scope)){
            $stored->scope = NULL;
        }

        if ($input['scope'] && (!is_a($stored, 'Icm_Struct') || !isset($stored->scope)) || !$this->checkScope($input['scope'], $stored->scope)){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_SCOPE, 'Unsupported scope requested.');
        }

        $userID = NULL;

        if (isset($stored->userID)){
            $userID = $stored->userID;
        }

        $permanent = false;

        if (isset($stored->permanent)){
            $permanent = $stored->permanent;
        }

        $token = $this->createAccessToken($client[0], $userID, $stored->scope, $permanent);
        $this->sendOutput($token);
    }

    /**
     * Get client credentials from basic HTTP auth or POST data.  HTTP basic auth recommended.
     * @param array $input
     * @param array $headers
     * @throws Icm_OAuth2_Exception
     * @return array(client id, client secret)
     */
    protected function getClientCredentials(array $input, array $headers){
        if (!empty($headers['PHP_AUTH_USER'])){
            // basic HTTP auth
            return array($headers['PHP_AUTH_USER'], $headers['PHP_AUTH_PW']);
        }
        else if (empty($input['client_id'])){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'Client ID not present in headers or body.');
        }
        else{
            return array($input['client_id'], $input['client_secret']);
        }
    }

    /**
     * Get authorization data from the HTTP request
     * @param array $input GET params
     * @throws Icm_OAuth2_Exception
     * @throws Icm_OAuth2_Redirectexception
     * @return array Retrieved authorization parameters
     */
    public function getAuthParams(array $input = NULL){
        $filters = array(
            'client_id' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => self::CLIENT_ID_REGEXP),
                'flags' => FILTER_REQUIRE_SCALAR),
            'response_type' => array('flags' => FILTER_REQUIRE_SCALAR),
            'redirect_uri' => array('filter' => FILTER_SANITIZE_URL),
            'state' => array('flags' => FILTER_REQUIRE_SCALAR),
            'scope' => array('flags' => FILTER_REQUIRE_SCALAR)
        );

        if (is_null($input)){
            $input = $_GET;
        }

        $input = filter_var_array($input, $filters);

        if (!$input['client_id']){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'No client ID provided.');
        }

        $stored = $this->storage->getClientDetails($input['client_id']);

        if ($stored === false){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'Invalid client ID.');
        }

        if (!$input['redirect_uri'] && !$stored['redirectUri']){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_REDIRECT_URI_MISMATCH, 'No supplied or stored redirect URI.');
        }

        if ($this->config['enforceRedirect'] && !$input['redirect_uri']){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_REDIRECT_URI_MISMATCH, 'Parameter "redirect_uri" is required.');
        }

        if (!isset($input['redirect_uri'])){
            $input['redirect_uri'] = $stored['redirect_uri'];
        }

        if (!$input['response_type']){
            throw new Icm_OAuth2_Redirectexception($input['redirect_uri'], self::ERROR_INVALID_REQUEST, 'Parameter "response_type" invalid or missing.', $input['state']);
        }

        if ($input['respons_type'] != self::RESPONSE_TYPE_AUTH_CODE && $input['response_type'] != self::RESPONSE_TYPE_ACCESS_TOKEN){
            throw new Icm_OAuth2_Redirectexception($input['redirect_uri'], self::ERROR_UNSUPPORTED_RESPONSE_TYPE, NULL, $input['state']);
        }

        if ($input['scope'] && !$this->checkScope($input['scope'], $this->config['supportedScopes'])){
            throw new Icm_OAuth2_Redirectexception($input['redirect_uri'], self::ERROR_INVALID_SCOPE, 'Unsupported scope requested.', $input['state']);
        }

        if ($this->config['enforceState'] && !$input['state']){
            throw new Icm_OAuth2_Redirectexception($input['redirect_uri'], self::ERROR_INVALID_REQUEST, 'Parameter "state" is required.');
        }

        return $input + $stored;
    }

    /**
     * Redirect the user after authorization has been approved.
     * @param boolean $isAuthorized
     * @param string $userID
     * @param array $params array('response_type' => (access token, auth code, or both), 'client_id' => client ID, 'redirect_uri' => URI, 'scope' => scope string, 'state' => state string)
     */
    public function finishAuthorization($isAuthorized, $userID = NULL, $params = array()){
        list($redirectUri, $result) = $this->getAuthResult($isAuthorized, $userID, $params);
        $this->doRedirect($redirectUri, $result);
    }

    /**
     * @param boolean $isAuthorized
     * @param string $userID
     * @param string $params
     * @throws Icm_OAuth2_Redirectexception
     * @return array
     */
    public function getAuthResult($isAuthorized, $userID = NULL, $params = array()){
        $params = $this->getAuthParams($params);
        $params += array('scope' => NULL, 'state' => NULL);
        extract($params);

        if ($state !== NULL){
            $result['query']['state'] = $state;
        }

        if (!$isAuthorized){
            throw new Icm_OAuth2_Redirectexception($redirect_uri, self::ERROR_USER_DENIED, 'The user denied access to your application.', $state);
        }
        else{
            if ($response_type == self::RESPONSE_TYPE_AUTH_CODE){
                $result['query']['code'] = $this->createAuthCode($client_id, $user_id, $redirect_uri, $scope);
            }
            else if ($response_type == self::RESPONSE_TYPE_ACCESS_TOKEN){
                $result['fragment'] = $this->createAccessToken($client_id, $user_id, $scope);
            }
        }

        return array($redirect_uri, $result);
    }

    /**
     * Send output to the client
     * @param array $data
     */
    public function sendOutput(array $data){
        $format = $this->getOutputFormat();

        switch($this->getOutputFormat()){
            case self::OUTPUT_FORMAT_JSON:
                $this->sendJSONHeaders();
                echo json_encode($data);
                break;
            case self::OUTPUT_FORMAT_XML:
                $this->sendXMLHeaders();
                echo $this->xmlEncode($data);
                break;
        }
    }

    /**
     * Get the requested output format if present.
     * @throws Icm_OAuth2_Exception
     * @return string
     */
    protected function getOutputFormat(){
        $format = $this->config['defaultOutputFormat'];

        if (isset($_GET['output_format'])){
            $format = $_GET['output_format'];
        }
        else if (isset($_POST['output_format'])){
            $format = $_POST['output_format'];
        }

        $supportedFormats = array(
            self::OUTPUT_FORMAT_JSON,
            self::OUTPUT_FORMAT_XML
        );

        if (!in_array($format, $supportedFormats)){
            throw new Icm_OAuth2_Exception(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_OUTPUT_FORMAT, 'The requested output format is not supported.');
        }

        return $format;
    }

    /**
     * Convert the given array to XML
     * @param array $data
     * @return string
     */
    protected function xmlEncode(array $data){
        $xml = new SimpleXMLElement('<?xml version="1.0"?><response></response>');
        $convertFunction = function($data, SimpleXMLElement &$xml) use (&$convertFunction){

            foreach ($data as $key => $value){
                if (is_array($value)){
                    if (!is_numeric($key)){
                        $subNode = $xml->addChild($key);
                        $convertFunction($value, $subNode);
                    }
                    else{
                        $subNode = $xml->addChild($xml->getName());
                        $convertFunction($value, $subNode);
                    }
                }
                else{
                    // $xml->addChild($key, $value);
                    $xml->$key = $value;
                }
            }
        };

        $convertFunction($data, $xml);

        return $xml->asXML();
    }

    /**
     * Redirect the user to the given URI
     * @param string $redirectUri
     * @param array $params passed to buildUr()
     */
    protected function doRedirect($redirectUri, $params){
        header('HTTP/1.1 '.self::HTTP_FOUND);
        header('Location: ' . $this->buildUri($redirectUri, $params));
        exit;
    }

    /**
     * @param string $uri
     * @param array $params
     * @return string
     */
    protected function buildUri($uri, array $params){
        $parsed = parse_url($uri);

        foreach ($params as $key => $value){
            if (isset($parsed[$key])){
                $parsed[$key] .= '&'.http_build_query($value);
            }
            else{
                $parsed[$key] = http_build_query($value);
            }
        }

        extract($parsed);

        return (isset($scheme) ? $scheme.'://' : '') . (isset($user) ? $user . (isset($pass) ? ':' . $pass : '') . '@' : '') .
            (isset($host) ? $host : '') . (isset($port) ? ':' . $port : '') . (isset($path) ? $path : '') . (isset($query) ? '?' . $query : '') .
            (isset($fragment) ? '#' . $fragment : '');
    }

    /**
     * Create an access token, and refresh token if supported.
     * @param string $clientID
     * @param string $userID
     * @param string $scope
     * @param boolean $permanent
     * @return array Token properties
     */
    protected function createAccessToken($clientID, $userID, $scope = NULL, $permanent = false){
        $token = array(
            'access_token' => $this->genAccessToken(),
            'expires_in' => $this->config['accessTokenLifetime'],
            'token_type' => self::TOKEN_TYPE_BEARER, //TODO: make this dynamic to support other token types
            'scope' => $scope
        );

        if ($permanent){
            $token['expires_in'] = 0;
        }

        $this->storage->setAccessToken($token['access_token'], $clientID, $userID, $token['expires_in']);

        if ($this->storage instanceof Icm_OAuth2_Storage_Refreshtoken){
            // we support refrsh tokens; issue one
            $token['refresh_token'] = $this->genAccessToken();
            $this->storage->setRefreshToken($token['refresh_token'], $clientID, $userID, time() + $this->config['refreshTokenLifetime'], $scope);

            if ($this->oldToken){
                // expire the old token
                $this->storage->expireRefreshToken($this->oldToken);
                unset($this->oldToken);
            }
        }

        return $token;
    }

    /**
     * Create an auth code. Assumes that our storage engine implements Icm_OAuth2_Storage_Grantcode
     * @param string $clientID
     * @param string $userID
     * @param string $redirectUri
     * @param string $scope
     * @return Icm_OAuth2_Authcode
     */
    protected function createAuthCode($clientID, $userID, $redirectUri, $scope = NULL){
        $code = $this->genAuthCode();
        $this->storage->setAuthCode($code, $clientID, $userID, $redirectUri, time() + $this->config['authcodeLifetime'], $scope);
        return $code;
    }

    /**
     * Generate a random string
     * @return string
     */
    protected function genAccessToken(){
        $length = 40;
        $random = mt_rand().mt_rand().mt_rand().mt_rand().microtime(true).uniqid(mt_rand(), true);
        return substr(hash('sha512', $random), 0, $length);
    }

    /**
     * Calls genAccessToken(). Override this to implement a different generation scheme.
     * @return string
     */
    protected function genAuthCode(){
        return $this->genAccessToken();
    }

    /**
     * Retrieve and return HTTP authorization header in array form
     * @return array
     */
    protected function getAuthHeader(){
        return array(
            'PHP_AUTH_USER' => isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '',
            'PHP_AUTH_PW' => isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : ''
        );
    }

    /**
     * Send headers for JSON content
     */
    protected function sendJSONHeaders(){
        if (php_sapi_name() === 'cli' || headers_sent()){
            return;
        }

        header('Content-Type: application/json');
        header('Cache-Control: no-store');
    }

    /**
     * Send headers for XML content
     */
    protected function sendXMLHeaders(){
        if (php_sapi_name() === 'cli' || headers_sent()){
            return;
        }

        header('Content-Type: text/xml');
        header('Cache-Control: no-store');
    }

    /**
     * Validate redirect URI supplied with request against stored URI
     * @param string $input URI supplied by client
     * @param string $stored URI stored on behalf of client
     * @return boolean true if URIs match; false otherwise
     */
    protected function validateRedirectUri($input, $stored){
        if (!$input || !$stored){
            // one of the URIs is missing
            return false;
        }
        return strcasecmp(substr($input, 0, strlen($stored)), $stored) === 0;
    }
}