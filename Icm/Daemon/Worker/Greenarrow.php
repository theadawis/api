<?php
class Icm_Daemon_Worker_Greenarrow extends Icm_Worker{
    /**
     * @var Icm_Container
     */
    protected $dataContainer;

    /**
     * @var Icm_Email_Greenarrow
     */
    protected $greenArrow;

    protected $params;

    public function __construct(array $params){
        set_error_handler(array('Icm_Daemon_Worker_Greenarrow', 'error'));
        $this->params = $params;
        $this->dataContainer = new Icm_Container_Factory();
        $this->dataContainer->loadXml(simplexml_load_file('/sites/api/scripts/' . 'services/dataContainer.xml'));
        $this->start();
    }

    protected function run(){
        $start = microtime(true);
        $this->greenArrow = $this->dataContainer->get('greenarrow');
        $method = $this->params['method'];
        $this->greenArrow->$method($this->params['params']);
        $end = microtime(true);
        $elapsed = $end - $start;
        $this->log("1 email sent in $elapsed seconds.");
    }
}