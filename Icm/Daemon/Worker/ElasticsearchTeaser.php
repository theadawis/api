<?php
class Icm_Daemon_Worker_ElasticsearchTeaser extends Icm_Worker{
    protected $query;

    /**
     * @var Icm_Container
     */
    protected $dataContainer;

    public function __construct(Icm_Search_Query $query){
        set_error_handler(array('Icm_Daemon_Worker_ElasticsearchTeaser', 'error'));
        $this->query = $query;
        $this->dataContainer = new Icm_Container_Factory();
        $this->dataContainer->loadXml(simplexml_load_file('/sites/api/scripts/' . 'services/dataContainer.xml'));
        $this->start();
    }

    protected function run(){
        $startTime = microtime(true);
        $engine = $this->dataContainer->get('lexisOnlyBackgroundSearch');
        try{
            $results = $engine->search($this->query);
        }
        catch(Exception $e){
            $this->log($e->getMessage(), true);
            exit;
        }
        $elastic = $this->dataContainer->get('elasticsearchAdapter');
        $added = $elastic->addTeaserDocuments($results);
        $endTime = microtime(true);
        $totalTime = $endTime - $startTime;
        $this->log($added.' results added in ' . $totalTime.' seconds.');
    }
}