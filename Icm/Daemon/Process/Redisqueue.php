<?php
class Icm_Daemon_Process_Redisqueue extends Icm_Process{
    const QUEUE = 'icm_work_queue';

    protected $redis;
    protected $currentJob;

    public function __construct(Icm_Config $config = NULL){
        $this->redis = new Icm_Redis($config->getSection('redis'));
        parent::__construct($config->getSection('phpdaemon'));
    }

    protected function run(){
        $jobs = array();
        while(sizeof($jobs) < ($this->config['maxChildren'] - sizeof($this->children)) && ($job = $this->redis->rpop(self::QUEUE))){
            $jobs[] = $job;
        }

        // blocking prevents PHP from handling signals, so sleep
        sleep(1);

        if (sizeof($jobs)){
            // we have work to do
            foreach ($jobs as $job){
                $this->currentJob = json_decode($job, true);

                try{
                    $this->createWorker();
                }
                catch(Exception $e){
                    $this->log($e->getMessage());
                    exit;
                }

                unset($this->currentJob);
            }
        }
    }

    protected function worker(){
        try{
            switch($this->currentJob['worker']){
                case 'ElasticsearchTeaser':
                    $query = unserialize($this->currentJob['data']);

                    if (is_a($query, 'Icm_Search_Query')){
                        new Icm_Daemon_Worker_ElasticsearchTeaser($query);
                    }
                    else{
                        throw new Exception("Object placed in work queue must be an Icm_Search_Query instance.");
                    }
                    break;
                case 'Greenarrow':
                    new Icm_Daemon_Worker_Greenarrow($this->currentJob['data']);
                    break;
            }
        }
        catch(Exception $e){
            Icm_Daemon_Worker_ElasticsearchTeaser::log(print_r($e->getMessage(), true));
            exit;
        }
    }
}