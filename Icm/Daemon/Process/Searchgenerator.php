<?php
class Icm_Daemon_Process_Searchgenerator extends Icm_Process{
    protected $file;

    /**
     * @var Icm_Db_Pdo
     */
    protected $db;

    /**
     * @var resource
     */
    protected $mssql;

    protected $tables = array();

    protected $currentTable = -1;

    public function __construct(Icm_Config $config = NULL){
        $this->db = Icm_Db_Pdo::connect('checkmateDb', $config->getSection('checkmateDb'));
        $this->file = fopen('/tmp/activity_search_20121203.txt', 'r');
        $this->mssql = $this->getMssqlConnection($config->getSection('mssql'));
        $this->setUpMssqlCursor();
        parent::__construct($config->getSection('phpdaemon'));
    }

    protected function getMssqlConnection(Icm_Config $config){
        return mssql_pconnect($config->getOption('hostname'), $config->getOption('username'), $config->getOption('password'));
    }

    protected function setUpMssqlCursor(){
        mssql_select_db('PF', $this->mssql);
        $tables = mssql_query("EXEC sp_tables @table_type = \"'table'\"", $this->mssql);
        if (!mssql_num_rows($tables)){
            // no tables? get out of here...
            exit;
        }
        for ($i = 0; $i < mssql_num_rows($tables); $i++){
            $name = mssql_result($tables, $i, 'TABLE_NAME');
            if (strpos($name, 'PF_') !== false){
                $this->tables[] = $name;
            }
        }
    }

    protected function getDbData(){
        if ($this->currentTable < 0){
            // first time through; grab the first table and start a cursor
            $this->currentTable = 0;
            $this->openCursor();
        }
        $result = mssql_query("FETCH NEXT FROM db_cursor", $this->mssql);
        if (!mssql_num_rows($result)){
            // no data returned; end of table
            $this->currentTable++;
            if (!isset($this->tables[$this->currentTable])){
                // we're out of tables; exit
                $this->closeCursor();
                exit;
            }
            $this->closeCursor(); //close the previous cursor
            $this->openCursor();  //open a new cursor
            return $this->getDbData();
        }
        $return = array();
        $return['first_name'] = mssql_result($result, 0, 'FirstName');
        $return['last_name'] = mssql_result($result, 0, 'LastName');
        $return['state'] = mssql_result($result, 0, 'state');
        return $return;
    }

    protected function openCursor(){
        mssql_query("DECLARE db_cursor CURSOR FOR SELECT FirstName, LastName, state FROM PF.dbo.{$this->tables[$this->currentTable]}; OPEN db_cursor", $this->mssql);
    }

    protected function closeCursor(){
        mssql_query("CLOSE db_cursor");
    }

    protected function run(){
        $jobs = array();
        while(sizeof($jobs) < ($this->config['maxChildren'] - sizeof($this->children)) && $data = $this->getDbData()){
            $jobs[] = $data;
        }
        sleep(1);
        if ($jobs && sizeof($jobs) > 0){
            // work to do, son
            foreach ($jobs as $job){
                $this->currentJob = $job;
                try{
                    $this->createWorker();
                }
                catch(Exception $e){
                    $this->log($e->getMessage());
                    exit;
                }
                unset($this->currentJob);
            }
        }
    }

    protected function worker(){
        try{
            $this->log(print_r($this->currentJob, true));
            $query = new Icm_Search_Query($this->currentJob, 'backgroundCheck');
            new Icm_Daemon_Worker_ElasticsearchTeaser($query);
        }
        catch(Exception $e){
            Icm_Daemon_Worker_ElasticsearchTeaser::log(print_r($e->getMessage(), true));
            exit;
        }
    }

    protected function getChunk(){
        $data = fgets($this->file);
        if ($data){
            $data = explode("\t", $data);
            $data = array('first_name' => $data[2], 'last_name' => $data[4], 'state' => $data[6]);
            return $data;
        }
        return false;
    }
}