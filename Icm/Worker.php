<?php
abstract class Icm_Worker{
    public function __construct(){

    }

    final protected function start(){
        pcntl_signal(SIGTERM, array($this, "signalHandler"));
        pcntl_signal(SIGHUP, array($this, "signalHandler"));
        $this->run();
        exit;
    }

    /**
     * Child classes do work here
     */
    abstract protected function run();

    protected function stop(){
        // TODO: ensure clean exit
        $this->log('stopping a worker');
        exit;
    }

    public function signalHandler($signo, $pid = NULL, $status = NULL){
        switch($signo){
            case SIGTERM: $this->stop();
            break;
            case SIGHUP: //TODO: handle restart
                break;
            case SIGUSR1:
                break;
        }
    }

    public static function log($string, $isError = false){
        if ($isError || defined('ENVIRONMENT') && (ENVIRONMENT == 'development' || ENVIRONMENT == 'stage')){
            $handle = fopen("/tmp/worker.log", "a");
            if ($handle){
                flock($handle, LOCK_EX, $wouldblock);
                fwrite($handle, date("Y-m-d h:i:s", time()) . ' ' . $string."\r\n");
                flock($handle, LOCK_UN);
                fclose($handle);
            }
        }
    }

    public static function error($number, $message, $file, $line){
        static::log("Error: $number. $message in $file on $line.", true);
        exit;
    }
}

