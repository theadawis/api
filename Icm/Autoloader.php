<?php
class Icm_Autoloader {
    protected $directory;
    protected $prefix;
    protected $prefixLength;

    public function __construct($baseDirectory, $namespace){
        $this->directory = $baseDirectory;
        $this->prefix = $namespace . '\\';
        $this->prefixLength = strlen($this->prefix);
    }

    /**
     * Registers the autoloader class with the PHP SPL autoloader.
     *
     * @param boolean $prepend Prepend the autoloader on the stack instead of appending it.
     */
    public function register($prepend = false){
        spl_autoload_register(array($this, 'autoload'), true, $prepend);
    }

    public function autoload($className){
        if (0 === strpos($className, $this->prefix)) {
            $parts = explode('\\', substr($className, $this->prefixLength));
            $filepath = $this->directory.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $parts) . '.php';

            if (is_file($filepath)) {
                require($filepath);
            }
        }
    }
}
