<?php

/**
 * Base Entity class. Contains filters, and get/set methods
 * User: chrisg
 * Date: 4/11/12
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class Icm_Entity
{
    protected static $propConfig = array();

    /**
     * @var Icm_Entity_Plugin_Broker
     */
    protected $broker;

    public function setBroker(Icm_Entity_Plugin_Broker $b) {
        $this->broker = $b;
    }

    public function getEntityPropertiesMap() {
        return Icm_Entity_Map::getInstance();
    }

    /**
     * @param
     * @return Icm_Entity
     */
    public static function create($properties) {
        /*
          - make an object of the called class
          - loop through dataProperties
          - apply each validator function for each property
          - set the valid property in the object
         */

        // <3 late static binding :)
        $className = get_called_class();

        /**
         * @var Icm_Entity $obj
         */
        $obj = new $className();

        if (is_array($properties)){

            foreach ($properties as $key => $value) {

                if (!in_array($key, $className::$propConfig)) {
                    throw new Icm_Entity_Exception('Attempt to set an invalid property: ' . $key . ' for ' . $className);
                }

                // still here? woot, set the value of the property
                $obj->set($key, $value);
            }
        }

        return $obj;
    }

    /**
     * @param $filterType
     * @return bool
     */
    public function hasA($filterType) {
        return isset($this->$filterType) && !empty($this->$filterType);
    }

    /**
     * @param $filterType
     * @return bool
     */
    public function isA($filterType) {
        $isA = is_subclass_of($this, $filterType) || is_a($this, $filterType);
        return $isA;
    }

    /**
     * filter function - allows an entity object to be filtered by a hash derived from a supplied assoc array
     * returns false if this entity should not be included in a set
     * * filtering is not case-sensitive
     *
     * e.g. pass in:
     * array('first_name' => 'John',
     *       'last_name' => 'Smith')
     *
     * this will return false for every intity object that has a matching first_name and last_name
     *
     * @param array
     * @return bool
     */
    public function hasHash($args = array()) {
        // stringify args and stringify corresponding entity properties. compare the strings if ===, return false as they should be filtered
        $filterStr = '';
        $entityStr = '';

        foreach ($args as $key => $val) {
            $filterStr .= strtoupper($val);

            if (isset($this->$key)) {
                $entityStr .= isset($this->$key) ? strtoupper($this->$key) : '';
            }
        }

        return ($filterStr === $entityStr) ? false : true;
    }

    abstract public function getHash();

    /**
     * IMPORTANT: PREVIOUS NAME setADDRESS was overriding setAddress required by whitepages adapter
     *
     * @param null $street
     * @param null $city
     * @param null $state
     * @param null $zip
     */
    public function putAddress($street = null, $city = null, $state = null, $zip = null){
        if (!is_null($street)) {
            $this->street = $street;
        }

        if (!is_null($city)) {
            $this->city = $city;
        }

        if (!is_null($state)){
            $this->state = $state;
        }

        if (!is_null($zip)){
            $this->zip = $zip;
        }

        return $this;
    }

    /**
     *
     * @param array1, array2
     * @return array
     */
    public static function orderBy($data, $options) {
        $position = 0;

        usort($data, function($a, $b) use ($options, $position) {
            return Icm_Entity::compare($a, $b, $options, $position);
        });

        return $data;
    }

    public static function compare($a, $b, $sortOptions, $position) {
        $options = $sortOptions[$position];
        $prop  = $options[0];
        $type  = $options[1];
        $order = $options[2];

        if ($a->$prop === $b->$prop) {
            $position++;

            if (empty($sortOptions[$position])) {
                return 0;
            }

            return Icm_Entity::compare($a, $b, $sortOptions, $position);
        }

        if ($type == 'STRING') {
            $rv = strcmp($a->$prop, $b->$prop);
            return ($order === 'DESC') ? -$rv : $rv;
        }

        $rv = ($a->$prop > $b->$prop) ? 1 : -1;

        return ($order === 'DESC') ? -$rv : $rv;
    }


    /*
     * Magic be here.
     */
    /**
     * @param $property
     * @param $value
     */
    public function set($property, $value) {
        $this->{'set' . ucfirst($property)}($value);
    }

    /**
     * @param $property
     * @return mixed
     */
    public function get($property) {
        return $this->{'get' . ucfirst($property)}();
    }

    /**
     * @param $method
     * @param $parameters
     * @return bool|void
     * @throws Icm_Exception
     */
    public function __call($method, $parameters){
        if (substr($method, 0, 3) == 'get') {
            return $this->_get($this->_getPropertyName($method));
        }

        if (substr($method, 0, 3) == 'set') {
            return $this->_set($this->_getPropertyName($method), $parameters);
        }

        throw new Icm_Exception(get_called_class() . ' has no method ' . $method);
    }

    /**
     * @param $property
     * @return bool
     */
    protected function _get($property) {
        if (isset($this->$property)) {
            return $this->$property;
        }
        else {
            $b = &$this->broker;

            if (!is_null($this->broker) && $b($property, $this)) {

                if (isset($this->$property)){
                    return $this->$property;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param $property
     * @param $parameters
     * @throws Icm_Exception
     */
    protected function _set($property, $parameters) {
        if (!count($parameters)) {
            throw new Icm_Exception('To call set' . ucfirst($property) . ' you must pass a value');
        }

        $this->$property = $parameters[0];
        $this->_mapEntityProperty($this, $property, $parameters[0]);
    }

    /**
     * @param $method
     * @return string
     */
    protected function _getPropertyName($method) {
        return lcfirst(str_replace(array('get', 'set'), '', $method));
    }

    public function fromArray(array $array){
        foreach ($array as $key => $value){
            $this->set($key, $value);
        }

        return $this;
    }

    /**
     * Map a called property onto the current entity
     *
     * @param Icm_Entity $parent
     * @param string $k
     * @param string|array $v
     * @param int $level
     */
    protected function _mapEntityProperty(&$parent, &$k, &$v, $level = 0) {
        if (!is_scalar($v)){

            if ($level > 99) {
                throw new Exception('recursion too deep in mapEntityProperties');
            }

            $this->_mapEntityProperties($v, $level);

            // DO NOT return, it is possible the section we are looking for is not scalar
        }

        $section = $this->getEntityPropertiesMap()->getSection($k);

        if (!$section) {
            return;
        }

        $service = $section['service'];
        $method =  $section['method'];

        if (method_exists($service, $method)) {
            $this->getEntityPropertiesMap()->$method($parent, $k, $v);
        }
    }


    /**
     * loop through properties and associate field names to functionality
     * provided by $entityPropertiesConfig
     *
     * @param array $data
     * @param int $level
     */
    protected function _mapEntityProperties(&$data = array(), $level = 0) {
        if (!$data) {
            return;
        }

        $level++;

        foreach ($data as $k => $v) {
            $this->_mapEntityProperty($data, $k, $v, $level);
        }
    }


    public function __get($key){
        return $this->get($key);
    }

    public function __set($key, $value){
        $this->set($key, $value);
    }
}
