<?php

class Icm_Util_PopularNames {

    /**
     * build popular name redis key by prepending environment and namespace
     * @param string $position firstname or lastname
     * @param string $name name partial to search for
     * @return string $key constructed key
     */
    public static function makePopularnamesRedisSearchKey($position='firstname', $name) {
        // get namespace
        $key = Icm_Api::getInstance()->getDefaults()->getSection('popularnames')->getOption('popularnames_key');
        // validate position
        if ($position != 'firstname' && $position != 'lastname') {
            $position = 'firstname';
        }
        // append pieces
        $key .= '-' . $position . ':' . $name . '*';
        return $key;
    }
}