<?php

/**
 *
 */
class Icm_Util_Http_Client
{

    /**
     * @var
     */
    protected $_handle;

    /**
     * @return mixed
     */
    public function getHandle() {
        if (!is_resource($this->_handle)) {
            $this->_handle = curl_init();
        }
        return $this->_handle;
    }

    /**
     * Synonym for setCurlOpt
     * @param $option
     * @param $value
     */
    public function setOption($option, $value) {
        $this->setCurlOpt($option, $value);
    }

    /**
     * @param $curlOpt
     * @param $value
     */
    public function setCurlOpt($curlOpt, $value) {
        curl_setopt($this->getHandle(), $curlOpt, $value);
    }

    /**
     * @param int $curlOpt
     * @return mixed
     */
    public function getCurlOpt($curlOpt){
        return curl_getinfo($this->getHandle(), $curlOpt);
    }

    /**
     * @param $url
     * @param null $params
     */
    public function setUrl($url, $params = null) {
        if (!is_null($params)){
            $url = self::buildUrlString($url, $params);
        }
        $this->setCurlOpt(CURLOPT_URL, $url);
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(){
        return $this->getCurlOpt(CURLINFO_EFFECTIVE_URL);
    }

    /**
     * @param $postData
     */
    public function setPostData($postData) {
        $this->setCurlOpt(CURLOPT_POST, true);
        $this->setCurlOpt(CURLOPT_POSTFIELDS, $postData);
    }

    /**
     * Perform an HTTP GET and return the response
     * @return string
     */
    public function get(){
        $this->setOption(CURLOPT_HTTPGET, 1);
        return $this->execute();
    }

    /**
     * Perform an HTTP POST and return the response
     * @param array $postData
     * @return string
     */
    public function post($postData = array()) {
        $this->setPostData($postData);
        return $this->execute();
    }

    public function put() {
        throw new Icm_Util_Http_Exception("PUT requests are not currently implemented");
    }

    public function delete() {
        throw new Icm_Util_Http_Exception("Delete requests are not currently implemented");
    }

    /**
     * @throws Exception
     * @return string
     */
    public function execute() {
        $this->setCurlOpt(CURLOPT_SSL_VERIFYHOST, 2);
        $this->setCurlOpt(CURLOPT_SSL_VERIFYPEER, false);
        $this->setCurlOpt(CURLOPT_RETURNTRANSFER, true);
        $this->setCurlOpt(CURLOPT_FAILONERROR, true);
        $response = curl_exec($this->getHandle());
        if (curl_errno($this->getHandle())) {
            throw new Icm_Util_Http_Exception(
                "Curl call failed with message: " . curl_error($this->getHandle()),
                curl_errno($this->getHandle())
            );
        }
        $logger = Icm_Util_Logger_Syslog::getInstance('api');
        $logger->logInfo('CURL call to ' . $this->getUrl());
        return $response;
    }

    /**
     * @static
     * @param $baseUrl
     * @param $params
     * @param string $separator
     * @param string $qs_separator
     * @return string
     */
    public static function buildUrlString($baseUrl, $params, $separator = '&', $qs_separator = '?'){
        return $baseUrl . $qs_separator . http_build_query($params, null, $separator);
    }

}
