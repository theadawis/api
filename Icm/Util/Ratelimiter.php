<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Util_Ratelimiter{
    const TIME_LIMIT_KEY = 'icm_ratelimiter_time'; //time limit in seconds
    const VIEWS_LIMIT_KEY = 'icm_ratelimiter_views'; //number of views not to be exceeded in the specified time
    const WHITELIST_KEY = 'icm_ratelimiter_whitelist'; //list of IPs which are exempt from rate limiting
    const LIMITER_PREFIX = 'icm_ratelimiter_'; //prepended to IP addresses to create their Redis keys

    /**
     * @var Icm_Redis
     */
    protected $redis;

    /**
     * @var Icm_Db_Pdo
     */
    protected $cgdb;

    /**
     * @param Icm_Redis $redis
     * @param Icm_Db_Pdo $cgdb should have access to the rate_limiter table
     */
    public function __construct(Icm_Redis $redis, Icm_Db_Pdo $cgdb){
        $this->redis = $redis;
        $this->cgdb = $cgdb;
    }

    /**
     * Check the rate limit for the given IP address
     * @param string $ip
     * @return boolean true if the IP is over the rate limit; false otherwise
     */
    public function checkLimit($ip){
        $key = $this->getKey($ip);
        $pipe = $this->redis->pipeline();
        $pipe->get(self::TIME_LIMIT_KEY);
        $pipe->get(self::VIEWS_LIMIT_KEY);
        $pipe->get($key);
        $pipe->sismember(self::WHITELIST_KEY, $ip);
        $pipe->ttl($key);
        list($timeLimit, $viewsLimit, $views, $whitelist, $ttl) = $pipe->execute();

        if ($whitelist || !$viewsLimit || !$timeLimit) {
            // dont limit if they are whitelisted or the limits aren't set
            return false;
        }

        if ($views && $ttl != -1) {
            if ($views >= $viewsLimit){
                // user is over the limit
                $this->logLimit($ip, $timeLimit - $ttl);
                return true;
            }
            $this->redis->incr($key);

            // update the ttl to confirm that it didn't expire right before we incremented
            $ttl = $this->redis->ttl($key);
        }

        if (!$views || $ttl == -1) {
            // new user or a key without a timeout; start from scratch
            $this->redis->setex($key, $timeLimit, 1);
        }

        return false;
    }

    /**
     * Remove the rate limit for the given IP address
     * @param string $ip
     * @return boolean true if the rate limit was removed; false if the given IP is not currently limited
     */
    public function removeLimit($ip){
        return $this->redis->del(self::LIMITER_PREFIX.$ip);
    }

    /**
     * Inserts a record of the given IP address havng been rate limited only if a record does not already exist for the current rate limit period.
     * @param string $ip
     */
    public function logLimit($ip, $timeLimit){
        $timestamp = time();
        $timeLimit = time() - $timeLimit;
        $this->cgdb->execute("INSERT INTO cg_internal.rate_limiter (ip, timestamp, action) SELECT '{$ip}', {$timestamp}, 1 FROM DUAL
            WHERE NOT EXISTS (SELECT * FROM cg_internal.rate_limiter WHERE ip='{$ip}' AND `timestamp` >= {$timeLimit} AND action=1);");
    }

    /**
     * Log the manual removal of a rate limit for the given IP address
     * @param string $ip
     */
    public function logRemoval($ip){
        $time = time();
        $this->cgDb->execute("INSERT INTO cg_internal.rate_limiter (ip, timestamp, action) VALUES ('{$ip}', {$time}, 0);");
    }

    /**
     * Alter the rate limit settings in Redis. Pass boolean false for either parameter to leave that setting unchanged.
     * @param int $timeLimit Length of time (in seconds) for the rate limiting period
     * @param int $viewsLimit Number of pageviews not to be exceeded during the set time limit
     */
    public function setLimits($timeLimit = false, $viewsLimit = false){
        $pipe = $this->redis->pipeline();
        if ($timeLimit){
            $pipe->set(self::TIME_LIMIT_KEY, $timeLimit);
        }
        if ($viewsLimit){
            $pipe->set(self::VIEWS_LIMIT_KEY, $viewsLimit);
        }
        $pipe->execute();
    }

    /**
     * @return array of whitelisted (rate limit exempt) IP addresses
     */
    public function getWhitelist(){
        return $this->redis->smembers(self::WHITELIST_KEY);
    }

    /**
     * Add an IP address to the rate limiter whitelist
     * @param string $ip
     */
    public function addToWhitelist($ip){
        $this->redis->sadd(self::WHITELIST_KEY, $ip);
    }

    /**
     * Remove an IP address from the rate limiter whitelist
     * @param string $ip
     * @return boolean true on success; false otherwise
     */
    public function removeFromWhitelist($ip){
        return $this->redis->srem(self::WHITELIST_KEY, $ip);
    }

    /**
     * Get the current rate limit settings from Redis
     * @return array array(0 => time limit, 1 => pageviews limit)
     */
    public function getLimits(){
        $pipe = $this->redis->pipeline();
        $pipe->get(self::TIME_LIMIT_KEY);
        $pipe->get(self::VIEWS_LIMIT_KEY);
        return $pipe->execute();
    }

    protected function getKey($ip){
        return self::LIMITER_PREFIX.$ip;
    }

    /**
     * Encodes an IP address for customer / call center use
     * @param string $ip
     * @return string
     */
    public static function getCode($ip){
        return implode('-', array_map(function($value){
            return dechex($value);
        }, explode('.', $ip)));
    }

    /**
     * Converts a rate limiter "code" into an IP address
     * @param string $code
     * @return string|number
     */
    public static function decode($code){
        return implode('.', array_map(function($value){
            return hexdec($value);
        }, explode('-', $code)));
    }
}