<?php

class Icm_Util_Cache {
    /**
     * @var Zend_Cache_Manager
     */
    private static $cacheManager;

    public static function generateKey($ary) {
        return md5(serialize($ary)) . '_cache';
    }

    protected function __construct(){

    }

    /**
     * pass in an Icm_Config obj that has the following possible config entries:
     *   enableCache = bool - use it or not
     *   cacheTemplateName = string - if you use this template name more than once, it will only be instantiated once
     *   cacheLifetime = int - in seconds: e.g. 3600
     *   cacheCleaningFactor = int - number of cache hits before clearing expired caches
     *   cacheBackend = string - one of these: http://framework.zend.com/manual/en/zend.cache.backends.html (File, Sqlite, Memcached, etc...)
     *   cacheDir = where to cache (if cacheBackend requires it)
     *
     * different objects may want to utilizie different caching mechanisms.  Zend_Cache_Manager allows us to do that
     * this util function is here to keep the cacheManager as a singleton and give us the convenience of
     * instantiating cache templates from Icm_Config and leveraging the cacheManager to store those cache templates
     *
     * @param Icm_Config|null $config
     * @return Zend_Cache_Manager
     */
    public static function initCacheFromConfig($config = null) {
        if (is_null($config)) {
            $config = new Icm_Config();
        }
        // some defaults (just in case there are no config settings)
        $templateName   = $config->getOption('cacheTemplateName')   ? $config->getOption('cacheTemplateName')   : 'default';
        $lifetime       = $config->getOption('cacheLifetime')       ? $config->getOption('cacheLifetime')       : 3600;
        $cleaningFactor = $config->getOption('cacheCleaningFactor') ? $config->getOption('cacheCleaningFactor') : 10000;
        $cacheDir       = $config->getOption('cacheDir')            ? $config->getOption('cacheDir')            : '/dev/shm';
        $cacheBackend   = $config->getOption('cacheBackend')        ? $config->getOption('cacheBackend')        : 'File';
        $cacheIdPrefix  = $config->getOption('cacheIdPrefix')        ? $config->getOption('cacheIdPrefix')        : '';

        // make sure we have a cacheManager
        if (empty(self::$cacheManager)) {
            self::$cacheManager = new Zend_Cache_Manager();
        }

        // check if this template exists
        if (self::$cacheManager->hasCache($templateName)) {
            return self::$cacheManager;
        }


        // set up the cache backend
        switch($cacheBackend) {
            case 'file':
                $backend = array(
                    'name' => 'File',
                    'options' => array('cache_dir' => $cacheDir)
                );
            break;
            case 'redis':
                $backend = array(
                    'name' => 'Icm_Cache_Backend_Redis',
                    'customBackendNaming' => true,
                    'options' => array(
                        'host' => $config->getOption('host', 'redis01.prod.instantcheckmate.com'),
                        'port' => $config->getOption('port', '6379')
                    )
                );
            break;
            default :
                $backend = array(
                    'name' => 'File',
                    'options' => array('cache_dir' => $cacheDir)
                );
        }

        $dbCache = array(
                         'frontend' => array(
                                             'name' => 'Core',
                                             'options' => array(
                                                                'lifetime' => $lifetime,
                                                                'automatic_serialization' => true,
                                                                'automatic_cleaning_factor' => $cleaningFactor,
                                                                 'cache_id_prefix' => $cacheIdPrefix
                                                                )
                                             ),
                         'backend' => $backend

                         );
        self::$cacheManager->setCacheTemplate($templateName, $dbCache);
        return self::$cacheManager;
    }

    /**
     * @static
     * @return mixed
     */
    public static function getCacheManager() {
        if (empty(self::$cacheManager)) {
            self::initCacheFromConfig();
        }
        return self::$cacheManager;
    }

}