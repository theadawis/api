<?php

  /*
    this is a very thin wrapper around rsyslog

    * important stuff *
    - this wrapper makes some assumptions - the main risky one is that LOG_NOTICE is reserved for event logs
    - that means if you use syslog on local0 and fire a notice message that is not an event - it will go into the event log
    - and probably ruin the event log and cause the world to come to a grinding halt
    - in short - if you use syslog local0 - use this wrapper for all logging
    - log levels: http://php.net/manual/en/function.syslog.php
  */

class Icm_Util_Logger_Syslog extends Icm_Util_Logger implements Icm_Util_Logger_Interface {

    public $app;

    protected $startTime;
    protected $lastTime;


    private static $instance = array();
    private static $allowedApps = array('api', 'checkmate', 'rp', 'ud');
    private static $logLevel = LOG_CRIT;

    public function __construct($app) {
        $this->app = $app;
        openlog($app, LOG_NDELAY, LOG_LOCAL0);
    }

    public static function setLoggingLevel($level) {
        self::$logLevel = $level;
    }

    /**
     * @static
     * @param $app
     * @return Icm_Util_Logger_Syslog
     * @throws Icm_Util_Exception
     */
    public static function getInstance($app) {
        // make sure this app is allowed
        if (!in_array($app, self::$allowedApps)) {
            throw new Icm_Util_Exception("$app is not set up properly for logging.");
        }

        if (!isset(self::$instance[$app])) {
            $obj = new Icm_Util_Logger_Syslog($app);
            self::$instance[$app] = $obj;
        }
        return self::$instance[$app];
    }

    // all apps logEvents to the same logfile
    public function logEvent($args = array()) {
        if (!is_array($args)) {
            throw new Icm_Util_Exception('Invalid arguments for logEvent(). $args must be an array()');
        }

        $str = json_encode($args);
        return syslog(LOG_NOTICE, $str);
    }

    /**
     * Log critical errors in compact format
     * total log line length <= 500 characters
     * Info at: http://wiki.gentoo.org/wiki/Rsyslog
     *
     * @param string $msg plain text error message
     * @param string $visitorid visitor id
     * @param string $container class name (optional)
     * @param string $method or function name making call (optional)
     * @return boolean true on success, false on error, invalid logLevel or message not a string
     */
    public function logProd($msg, $visitorid, $container='-', $method='-') {
        if (self::$logLevel >= LOG_CRIT && is_string($msg)) {
             // format: dateTime  host  app  container  method  visitorid  message
             // e.g. May 14 11:14:52 localhost api: - -   12345678    Testing logProd
            $msg = "$container\t$method\t$visitorid\t" . preg_replace('/\t/', ' ', $msg);

             // max message length
            $msg = substr($msg, 0, 500);
            return syslog(LOG_CRIT, $msg);
        }
        return false;
    }

    public function logErr($msg) {
        if (self::$logLevel >= LOG_ERR) {
            return syslog(LOG_ERR, $msg);
        }
    }

    public function logInfo($msg) {
        if (self::$logLevel >= LOG_INFO) {
            return syslog(LOG_INFO, $msg);
        }
    }

    public function logDebug($msg) {
        if (self::$logLevel >= LOG_DEBUG) {
            return syslog(LOG_DEBUG, $msg);
        }
    }

    // start a timer and then each subsequent call will log the time since the beginning and since the last call
    // pass in: marker => 'string' to prepend the time stamps
    // pass in: reset => true to start a new timer
    public function logElapsedTime($args=array()) {

        $now = microtime(true);

        // start a new timer
        if (!isset($this->startTime) || (isset($args['reset']) && $args['reset'])) {
            $this->startTime = $now;
        }

        // init lastTime if it is not set
        if (!isset($this->lastTime)) {
            $this->lastTime = $now;
        }

        // determine time elapsed since start
        $totalTime = $now - $this->startTime;

        // determined time elapsed since last time
        $elapsed = $now - $this->lastTime;
        $this->lastTime = $now;

        if (isset($args['marker'])) {
            $msg = $args['marker'] . ': ';
        }
        $msg .= 'now: ' . $now.' since last call: ' . $elapsed.' since start: ' . $totalTime.' ';


        return $this->logDebug($msg);
    }

}
