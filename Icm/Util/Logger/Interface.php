<?php

interface Icm_Util_Logger_Interface {

    public function logEvent($args=array());
    public function logErr($msg);
    public function logInfo($msg);
    public function logDebug($msg);

}