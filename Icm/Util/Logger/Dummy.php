<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/15/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Util_Logger_Dummy implements Icm_Util_Logger_Interface
{

    protected $events = array();
    protected $errors = array();
    protected $info = array();
    protected $debug = array();

    public function logEvent($args = array()) {
        $this->events[] = $args;
    }

    public function logErr($msg) {
        $this->errors[] = $msg;
    }

    public function logInfo($msg) {
        $this->info[] = $msg;
    }

    public function logDebug($msg) {
        $this->debug[] = $msg;
    }

    public function getEventCount() {
        return count($this->events);
    }
}
