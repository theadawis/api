<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Util_Useragent{

    protected $useragent;
    protected $referrer;
    protected $isMobile;
    protected $isAutobot;
    protected $isDecepticon;
    protected $isRobot;
    protected $isBrowser;
    protected $browser;


    /**
     * @param string $useragent If not provided, $_SERVER['HTTP_USER_AGENT'] will be used.
     */
    public function __construct($useragent = NULL){
        $this->useragent = is_null($useragent) && isset($_SERVER['HTTP_USER_AGENT']) ? trim($_SERVER['HTTP_USER_AGENT']) : $useragent;
    }

    public function getAgent(){
        return $this->useragent;
    }

    public function getReferrer() {

        if (is_null($this->referrer)) {
            $this->referrer = (!isset($_SERVER['HTTP_REFERER']) OR $_SERVER['HTTP_REFERER'] == '') ? '' : trim($_SERVER['HTTP_REFERER']);
        }

        return $this->referrer;
    }

    public function isMobile(){

        if (is_null($this->isMobile)){
            $this->isMobile = !$this->isRobot() && $this->matchUserAgent($this->mobiles);
        }

        return $this->isMobile;
    }

    public function isAutobot(){

        if (is_null($this->isAutobot)){
            $this->isAutobot = $this->matchUserAgent($this->autobots);
        }

        return $this->isAutobot;
    }

    public function isDecepticon(){

        if (is_null($this->isDecepticon)){
            $this->isDecepticon = $this->matchUserAgent($this->decepticons);
        }

        return $this->isDecepticon;
    }

    public function isRobot(){

        if (is_null($this->isRobot)){
            $this->isRobot = $this->isAutobot() || $this->isDecepticon();
        }

        return $this->isRobot;
    }

    public function isBrowser(){

        if (is_null($this->isBrowser)) {
            $this->isBrowser = (!$this->isRobot() && $this->matchUserAgent($this->browsers)) || $this->isMobile();
        }

        return $this->isBrowser;
    }

    /**
     * @return string - Format:  browser_name/version_number
     */
    public function getBrowser() {

        // if browser is already set, just return it
        if (!is_null($this->browser)) {
            return $this->browser;
        }

        // set the default value to empty string
        $this->browser = '';

        // no need to look for a browser if it is not applicable
        if (!$this->isBrowser()) {
            return $this->browser;
        }

        foreach ($this->browsers as $agent => $name) {

            // Note:  The pattern only looks for the first string of digits for the version for simplicity
            $pattern = '/(' . $agent . ')[\/ ]+([0-9]+)/';
            preg_match($pattern, $this->useragent, $matches);

            if ($matches) {
                $this->browser = $matches[1] . '/' . $matches[2];
                break;
            }
        }

        return $this->browser;
    }

    /**
     * @param array $agents
     * @return boolean
     */
    protected function matchUserAgent(array $agents){

        foreach ($agents as $agent => $name){

            if (stripos(strtolower($this->useragent), strtolower($agent)) !== false){
                return true;
            }
        }

        return false;
    }


    // good robots
    protected $autobots = array(
        'googlebot'             => 'Googlebot',
        'adsbot-google'         => 'Google AdsBot',
        'msnbot'                => 'MSNBot',
        'bingbot'               => 'Bing',
        'slurp'                 => 'Inktomi Slurp',
        'yahoo'                 => 'Yahoo',
        'askjeeves'             => 'AskJeeves',
        'fastcrawler'           => 'FastCrawler',
        'infoseek'              => 'InfoSeek Robot 1.0',
        'lycos'                 => 'Lycos',
        'facebookexternalhit'   => 'facebookexternalhit',
    );

    // evil robots
    protected $decepticons = array(
        'curl'                  => 'cURL',
        'check_http'            => 'Apache Monitor',
        'server'                => 'Server Density',
        'crawl'                 => 'Generic Crawler',
        'monitor'               => 'Generic Monitor',
        'bot'                   => 'Generic Bot',
        'httpclient'            => 'Jakarta HttpClient',
        'Wget'                  => 'Wget',
        'libwww-perl'           => 'libwww-perl',
        'Java'                  => 'Java',
        'Python'                => 'Python',
        'Test Certificate Info' => 'Test Certificate Info',
        'Microsoft Office'      => 'Microsoft Office',
        'panopta.com'           => 'panopta.com',
        'WinHTTP'               => 'WinHTTP',
        'Genieo'                => 'Genieo',
        'spider'                => 'spider',
        'Ruby'                  => 'Ruby',
        'VB Project'            => 'VB Project',

    );

    protected $mobiles = array(
        'mobileexplorer'    => 'Mobile Explorer',
        'palmsource'        => 'Palm',
        'palmscape'            => 'Palmscape',

        // Phones and Manufacturers
        'motorola'                => 'Motorola',
        'nokia'                    => 'Nokia',
        'palm'                    => 'Palm',
        'iphone'                => 'Apple iPhone',
        'ipad'                    => 'iPad',
        'ipod'                    => 'Apple iPod Touch',
        'sony'                    => 'Sony Ericsson',
        'ericsson'                => 'Sony Ericsson',
        'blackberry'            => 'BlackBerry',
        'cocoon'                => 'O2 Cocoon',
        'blazer'                => 'Treo',
        'lg'                    => 'LG',
        'amoi'                    => 'Amoi',
        'xda'                    => 'XDA',
        'mda'                    => 'MDA',
        'vario'                    => 'Vario',
        'htc'                    => 'HTC',
        'samsung'                => 'Samsung',
        'sharp'                    => 'Sharp',
        'sie-'                    => 'Siemens',
        'alcatel'                => 'Alcatel',
        'benq'                    => 'BenQ',
        'ipaq'                    => 'HP iPaq',
        'mot-'                    => 'Motorola',
        'playstation portable'    => 'PlayStation Portable',
        'playstation 3'            => 'PlayStation 3',
        'hiptop'                => 'Danger Hiptop',
        'nec-'                    => 'NEC',
        'panasonic'                => 'Panasonic',
        'philips'                => 'Philips',
        'sagem'                    => 'Sagem',
        'sanyo'                    => 'Sanyo',
        'spv'                    => 'SPV',
        'zte'                    => 'ZTE',
        'sendo'                    => 'Sendo',
        'dsi'                    => 'Nintendo DSi',
        'ds'                    => 'Nintendo DS',
        'wii'                    => 'Nintendo Wii',
        '3ds'                    => 'Nintendo 3DS',
        'open web'                => 'Open Web',
        'openweb'                => 'OpenWeb',

        // Operating Systems
        'android'        => 'Android',
        'symbian'        => 'Symbian',
        'SymbianOS'        => 'SymbianOS',
        'elaine'        => 'Palm',
        'series60'        => 'Symbian S60',
        'windows ce'    => 'Windows CE',

        // Browsers
        'obigo'            => 'Obigo',
        'netfront'        => 'Netfront Browser',
        'openwave'        => 'Openwave Browser',
        'mobilexplorer'    => 'Mobile Explorer',
        'operamini'        => 'Opera Mini',
        'opera mini'    => 'Opera Mini',
        'opera mobi'    => 'Opera Mobile',
        'fennec'        => 'Firefox Mobile',

        // Other
        'digital paths'    => 'Digital Paths',
        'avantgo'        => 'AvantGo',
        'xiino'            => 'Xiino',
        'novarra'        => 'Novarra Transcoder',
        'vodafone'        => 'Vodafone',
        'docomo'        => 'NTT DoCoMo',
        'o2'            => 'O2',

        // Fallback
        'mobile'        => 'Generic Mobile',
        'wireless'        => 'Generic Mobile',
        'j2me'            => 'Generic Mobile',
        'midp'            => 'Generic Mobile',
        'cldc'            => 'Generic Mobile',
        'up.link'        => 'Generic Mobile',
        'up.browser'    => 'Generic Mobile',
        'smartphone'    => 'Generic Mobile',
        'cellphone'        => 'Generic Mobile'
    );

    // Order matters!  We mostly care about detecting IE so check for that first.
    protected $browsers = array(
        'MSIE'                => 'Internet Explorer',
        'Internet Explorer'    => 'Internet Explorer',
        'Firefox'            => 'Firefox',
        'Chrome'            => 'Chrome',
        'Flock'                => 'Flock',
        'Opera'                => 'Opera',
        'Shiira'            => 'Shiira',
        'Chimera'            => 'Chimera',
        'Phoenix'            => 'Phoenix',
        'Firebird'            => 'Firebird',
        'Camino'            => 'Camino',
        'Netscape'            => 'Netscape',
        'OmniWeb'            => 'OmniWeb',
        'Safari'            => 'Safari',
        'Mozilla'            => 'Mozilla',
        'Konqueror'            => 'Konqueror',
        'icab'                => 'iCab',
        'Lynx'                => 'Lynx',
        'Links'                => 'Links',
        'hotjava'            => 'HotJava',
        'amaya'                => 'Amaya',
        'IBrowse'            => 'IBrowse'
    );

    protected $platforms = array(
        'windows nt 6.2'    => 'Windows 8',
        'windows nt 6.1'    => 'Windows 7',
        'windows nt 6.0'    => 'Windows Vista',
        'windows nt 5.2'    => 'Windows 2003',
        'windows nt 5.1'    => 'Windows XP',
        'windows nt 5.0'    => 'Windows 2000',
        'windows nt 4.0'    => 'Windows NT 4.0',
        'winnt4.0'            => 'Windows NT 4.0',
        'winnt 4.0'            => 'Windows NT',
        'winnt'                => 'Windows NT',
        'windows 98'        => 'Windows 98',
        'win98'                => 'Windows 98',
        'windows 95'        => 'Windows 95',
        'win95'                => 'Windows 95',
        'windows'            => 'Unknown Windows OS',
        'android'            => 'Android',
        'blackberry'        => 'BlackBerry',
        'iphone'            => 'iOS',
        'ipad'                => 'iOS',
        'ipod'                => 'iOS',
        'os x'                => 'Mac OS X',
        'ppc mac'            => 'Power PC Mac',
        'freebsd'            => 'FreeBSD',
        'ppc'                => 'Macintosh',
        'linux'                => 'Linux',
        'debian'            => 'Debian',
        'sunos'                => 'Sun Solaris',
        'beos'                => 'BeOS',
        'apachebench'        => 'ApacheBench',
        'aix'                => 'AIX',
        'irix'                => 'Irix',
        'osf'                => 'DEC OSF',
        'hp-ux'                => 'HP-UX',
        'netbsd'            => 'NetBSD',
        'bsdi'                => 'BSDi',
        'openbsd'            => 'OpenBSD',
        'gnu'                => 'GNU/Linux',
        'unix'                => 'Unknown Unix OS'
    );
}