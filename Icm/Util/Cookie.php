<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/31/12
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Util_Cookie
{
    // define the defaults
    const DEFAULT_DOMAIN = '';
    const DEFAULT_EXPIREOFFSET = 63072000; // 2 years
    const DEFAULT_SECURE = false;
    const DEFAULT_PATH = '/';
    const DEFAULT_HTTPONLY = false;
    const SESSION_NAME = '_COOKIE';

    // the keys of the cookie parameter to store in the session
    private $sessionKeys = array('domain', 'expire', 'secure', 'path', 'httpOnly', 'value');

    protected $name;
    protected $expire;
    protected $domain;
    protected $secure;
    protected $path;
    protected $httpOnly;
    protected $value;
    protected $session;

    public function __construct(
        $name,
        $domain = null,
        $expireOffset = null,
        $secure = null,
        $path = null,
        $httpOnly = null
    ) {
        $this->name = $name;

        // set the default values if not provided
        $this->domain = !is_null($domain) ? $domain : self::DEFAULT_DOMAIN;
        $this->expire = time() + (!is_null($expireOffset) ? $expireOffset : self::DEFAULT_EXPIREOFFSET);
        $this->secure = !is_null($secure) ? $secure : self::DEFAULT_SECURE;
        $this->path = !is_null($path) ? $path : self::DEFAULT_PATH;
        $this->httpOnly = !is_null($httpOnly) ? $httpOnly : self::DEFAULT_HTTPONLY;

        $this->session = new Icm_Session_Namespace(self::SESSION_NAME);
    }

    /**
     * Get the value of the cookie with $this->name.
     * If the cookie does not exist but is stored in the session, set the cookie and return
     * @return mixed
     */
    public function getValue() {
        if (is_null($this->value)) {
            // use the cookie if it has been set
            if (isset($_COOKIE[$this->name])) {
                $this->value = $_COOKIE[$this->name];
            }
            else if (!is_null($this->session->{$this->name})) {
                // copy all of the cookie parameters from the session and re-set the cookie
                $cookieParams = $this->session->{$this->name};

                foreach ($cookieParams as $key => $val) {
                    $this->$key = $val;
                }
                $this->setValue($cookieParams['value']);
            }
        }

        return $this->value;
    }

    /**
     * set the cookie AND populate the session with the cookie information
     * @param mixed $value
     */
    public function setValue($value) {
        setcookie($this->name, $value, $this->expire, $this->path, $this->domain, $this->secure, $this->httpOnly);

        // if the cookie is to be expired, unset the session variable and the value and return
        if ($this->expire < time()) {
            unset($this->session->{$this->name});
            unset($this->value);
            return;
        }
        $this->value = $value;
        $cookieParams = array();

        // add all of the cookie information into the session
        foreach ($this->sessionKeys as $key) {
            $cookieParams[$key] = $this->$key;
        }
        $this->session->{$this->name} = $cookieParams;
    }

    /**
     * This function is to more conveniently replace php's setcookie()
     * @param string $name
     * @param mixed $value
     * @param int $expireOffset
     * @param string $path
     * @param string $domain
     * @param string $secure
     * @param string $httpOnly
     * @return Icm_Util_Cookie
     */
    public static function setCookie($name, $value, $expireOffset = null, $path = null, $domain = null, $secure = null, $httpOnly = null) {
        $cookie = new self($name, $domain, $expireOffset, $secure, $path, $httpOnly);
        $cookie->setValue($value);
        return $cookie;
    }

    /**
     * Conveniently get the cookie value
     * @param string $name
     */
    public static function getCookieValue($name) {
        $cookie = new self($name);
        return $cookie->getValue();
    }

    /**
     * Conveniently delete a cookie
     * @param mixed $name
     * @param string $domain
     * @param string $path
     */
    public static function deleteCookie($name, $domain = null, $path = null) {
        $expireOffset = -3600;
        $cookie = new self($name, $domain, $expireOffset, null, $path);
        $cookie->setValue(null);
    }
}
