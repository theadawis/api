<?php

class Icm_Util_SplitTest {

    public static function makeSectionDefaultVariationRedisKey($sectionId, $appId = null) {
        $key = Icm_Api::getInstance()->getDefaults()->getSection('splittest')->getOption('section_default_variation_key');
        $key .= ':' . $sectionId;
        return $key;
    }

    public static function makeSectionControlVariationRedisKey($sectionId, $appId = null) {
        $key = Icm_Api::getInstance()->getDefaults()->getSection('splittest')->getOption('section_control_variation_key');
        $key .= ':' . $sectionId;
        return $key;
    }

    public static function makeVariationRedisKey($variationId, $appId = null) {
        $key = Icm_Api::getInstance()->getDefaults()->getSection('splittest')->getOption('variation_key');
        $key .= ':' . $variationId;
        return $key;
    }

    public static function makeTestCounterRedisKey($sectionId, $appId = null) {
        $key = Icm_Api::getInstance()->getDefaults()->getSection('splittest')->getOption('all_key');
        $key .= ':' . $sectionId;
        return $key;
    }

    public static function makeVariationCounterRedisKey($sectionId, $testId) {
        $key = Icm_Api::getInstance()->getDefaults()->getSection('splittest')->getOption('tva_key');
        $key .= ':' . $testId;
        return $key;
    }

}