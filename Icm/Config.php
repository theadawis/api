<?php
/**
 * A general configuration class,  can load an INI file.
 * User: chrisg
 * Date: 4/6/12
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Config implements IteratorAggregate
{

    protected static $instances = array();

    protected $_data = array();

    /**
     * @param $option
     * @param $value
     */
    public function setOption($option, $value){
        $this->_data[$option] = $value;
    }

    /**
     * @param $option
     * @param null $default
     * @return null
     */
    public function getOption($option, $default = null) {
        if (!array_key_exists($option, $this->_data)){
            return $default;
        }
        return $this->_data[$option];
    }


    public function loadIni($path, $environment = null) {
        if (!file_exists($path)) {
            throw new Icm_Exception("{$path} does not exist");
        }
        if (!is_readable($path)) {
            throw new Icm_Exception("{$path} is not readable");
        }
        $data = parse_ini_file($path, true);
        if (!is_null($environment)) {
            if (!array_key_exists($environment, $data)){
                throw new Icm_Exception("Section {$environment} does not exist in {$path}");
            }
            $data = $data[$environment];
        }
        foreach ($data as $k => $v){
            $this->setOption($k, $v);
        }
        return $this;

    }

    /**
     * @static
     * @param $path
     * @param null $environment
     * @return Icm_Config
     * @throws Icm_Exception
     */
    public static function fromIni($path, $environment = null){
        if (!self::instanceExists($path)){
            $config = new static();
            $config->loadIni($path);
            self::putInstance($path, $config);
        } else{
            $config = self::getInstance($path);
        }
        if ($environment) {
            return $config->getSection($environment);
        }
        return $config;
    }

    public static function instanceExists($path) {
        return array_key_exists(md5($path), self::$instances);
    }

    public static function getInstance($path) {
        return self::$instances[md5($path)];
    }

    public static function putInstance($path, Icm_Config $config) {
        self::$instances[md5($path)] = $config;
    }

    public static function fromArray($array) {
        $config = new self();
        foreach ($array as $k => $v) {
            $config->setOption($k, $v);
        }
        return $config;
    }

    public function loadArray($array){
        foreach ($array as $k => $v) {
            $this->setOption($k, $v);
        }
        return $this;
    }

    public function getSection($sectionName){
        $section = $this->getOption($sectionName, null);
        if (!is_array($section)) {
            throw new Icm_Exception("No section by the name of $sectionName");
        }
        return self::fromArray($section);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator() {
        return new ArrayIterator($this->_data);
    }
}
