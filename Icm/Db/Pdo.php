<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/13/12
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Db_Pdo extends Icm_Db implements Icm_Db_Interface {

    /**
     * @var PDO
     */
    protected $conn;

    protected $details;

    protected $counts = array(
        "execute" => 0,
        "fetch" => 0,
        "fetchAll" => 0
    );

    /**
     * variable to hold the max results for a query - to protect our db from getting hosed
     *
     * @var maxResults
     */
    public static $maxResults;

    /**
     * @static
     * @param $name
     * @param Icm_Config $config
     * @return Icm_Db_Pdo
     * @throws Icm_Db_Exception
     */
    public static function connect($name, Icm_Config $config) {

        $logger = Icm_Util_Logger_Syslog::getInstance('checkmate');

        $dsn = $config->getOption('driver') .
            ':host=' . $config->getOption('host') .
            ';dbname=' . $config->getOption('database');

        $logger->logDebug("[Icm_Db_Pdo::connect] Entered ::connect() for $name");

        try{
            $instance = parent::getInstance($name);
            $logger->logDebug("[Icm_Db_Pdo::connect] Returning existing connection for $name");
            return $instance;
        } catch (Icm_Db_Exception $e) {}

        $logger->logDebug("[Icm_Db_Pdo::connect] Making new instance of connection for $name");
        $pdo = new self(
            self::makeProvider($dsn, $config, $name),
            array($dsn, $config->getOption('username'), $config->getOption('password'), $name)
        );

        parent::addInstance($name, $pdo);

        if ($config->getOption('maxResults')) {
            self::$maxResults = $config->getOption('maxResults');
        }

        return $pdo;
    }

    protected function __construct(callable $provider, $details) {
        $this->conn = null;

        $this->providers = array("master" => $provider);
        $this->details = $details;
    }

    protected function lazyConnect() {
        if (is_null($this->conn)) {
            $this->conn = $this->providers["master"]();
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    protected static function makeProvider($dsn, $config, $name) {
        return function() use ($dsn, $config, $name) {
            $logger = Icm_Util_Logger_Syslog::getInstance('checkmate');
            $logger->logDebug("[Icm_Db_Pdo::<provider[{$name}]> creating new connection");

            try{
                return new PDO(
                    $dsn,
                    $config->getOption('username'),
                    $config->getOption('password'),
                    array(PDO::ATTR_PERSISTENT)
                );
            } catch (PDOException $e) {
                throw new Icm_Db_Exception($e->getMessage(), $e->getCode());
            }
        };
    }

    public function __sleep() {
        // unset($this->conn);
        return array('details');
    }

    public function __wakeup() {
        $this->conn = new PDO($this->details[0], $this->details[1], $this->details[2]);
    }

    public function escape($value) {
        return $this->conn->quote($value);
    }
    public function beginTransaction() {
        $this->lazyConnect();
        $this->conn->beginTransaction();
    }
    public function commit() {
        $this->conn->commit();
    }
    public function rollBack() {
        $this->conn->rollBack();
    }
    public function execute($sql, $replacements = array()) {
        $this->lazyConnect();
        $this->counts["fetch"] += 1;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute($replacements);
        return $stmt->rowCount();
    }

    public function fetch($sql, $replacements = array(), $callback = null) {
        $this->lazyConnect();
        $this->counts["fetch"] += 1;
        $stmt = $this->conn->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute($replacements);
        if (is_null($callback)) {
            return $stmt->fetch();
        }
        return $callback($stmt->fetch());
    }

    public function maxResults() {
        return self::$maxResults;
    }

    public function lastInsert() {
        return $this->conn->lastInsertId();
    }

    public function fetchAll($sql, $replacements = array(), $callback = null) {
        $this->lazyConnect();
        $this->counts["fetchAll"] += 1;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute($replacements);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        if (!is_null($callback)){
            $results = array();
            while ($row = $stmt->fetch()) {
                $results[] = $callback($row);
            }
            return $results;
        }
        return $stmt->fetchAll();
    }

   public function __destruct() {
       $logger = Icm_Util_Logger_Syslog::getInstance('checkmate');
       $status = is_null($this->conn) ? "Not connected" : "Connected";
       $logger->logDebug("[Icm_Db_Pdo::__destruct {$this->details[3]}] Connection status: {$status}");
       $logger->logDebug("[Icm_Db_Pdo::__destruct {$this->details[3]}] executes: {$this->counts['execute']}");
       $logger->logDebug("[Icm_Db_Pdo::__destruct {$this->details[3]}] fetches: {$this->counts['fetch']}");
       $logger->logDebug("[Icm_Db_Pdo::__destruct {$this->details[3]}] fetchAlls: {$this->counts['fetchAll']}");
   }

}
