<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/20/12
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Db_Interface
{
    public function beginTransaction();
    public function commit();
    public function rollBack();
    public function execute($sql, $replacements = array());
    public function escape($value);
    public function fetch($sql, $replacements = array(), $callback = null);
    public function fetchAll($sql, $replacements = array(), $callback = null);
    public function lastInsert();
    public static function connect($name, Icm_Config $config);
    public static function getInstance($name);

}
