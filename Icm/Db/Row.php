<?php
/**
 * @author Joe Linn
 *
 */
abstract class Icm_Db_Row{
    protected $connection; //Icm_Db_Pdo object
    protected $table; //should be given an Icm_Db_Table object in the constructor
    protected $tableData = array();

    /**
     * Loads given array into tableData. Does NOT write to the database.
     * @param array $data is an associative array: array('field1' => value)
     * @return boolean true on success; false on failure
     */
    public function loadTableData(array $data){
        if (is_array($data) && sizeof($data) > 0){
            $this->tableData = $data;
            return true;
        }
        return false;
    }

    /**
     * Retrieve table data for the given id and populate $this->tableData
     * @param int $id
     * @return associative array on success, false if the given id does not exist
     */
    public function getById($id){
        if ($info = $this->table->findOneBy('id', $id)){
            $this->tableData = $info;
            return $this->tableData;
        }
        return false;
    }

    /**
     * Returns an array of ALL table data
     * @param array $order is an associative array: array('field1' => 'ASC', 'field2' => 'DESC')
     */
    public function getAll($order = NULL){
        return $this->table->getAll($order);
    }

    public function getAllByFields(array $values){
        $class = get_called_class();
        $rows = array();
        foreach ($this->table->findByFields($values) as $row){
            $rowObject = new $class($this->connection);
            $rowObject->loadTableData($row);
            $rows[] = $rowObject;
        }
        return $rows;
    }

    /**
     * Create a new table row
     * @param array $data is an associative array: array('field' => value)
     * @return boolean true on successful row creation; false otherwise
     */
    public function create(array $data){
        if (!isset($data['id'])){
            if ($this->table->insertOne($data)){
                $this->tableData = $data;
                return true;
            }
        }
        return false;
    }

    /**
     * Modify a table row; assumes 'id' is the primary key
     * @param array $data is an associative array: array('field' => value)
     * @return boolean true on success; false otherwise
     */
    public function modify(array $data){
        if (!isset($data['id'])){
            $data['id'] = $this->tableData['id'];
        }
        if ($this->table->updateOne($data, 'id')){
            // successful modification
            $this->tableData = array_merge($this->tableData, $data); //update data in memory to match new table data
            return true;
        }
        return false;
    }

    /**
     * Set a field for the current table row.  Assumes that $this->tableData has been populated.
     * @param string $field
     * @param string $value
     * @return boolean
     */
    public function set($field, $value, $primaryKey = 'id'){
        if ($this->table->updateOne(array($field => $value, $primaryKey => $this->tableData[$primaryKey]), $primaryKey)){
            $this->tableData[$field] = $value;
            return true;
        }
        return false;
    }

    /**
     * Returns the value of the given field for the current table row.  Assumes that $this->tableData has been populated.
     * @param string $field
     * @return multitype:
     */
    public function get($field){
        return $this->tableData[$field];
    }

    public function __get($value){
        if (isset($this->tableData[$value])){
            return $this->tableData[$value];
        }
        return false;
    }
}