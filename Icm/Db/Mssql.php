<?php

/**
 *
 */
class Icm_Db_Mssql extends Icm_Db implements Icm_Db_Interface{

    /*
    protected static $hostname = "mssql.instantcheckmate.com:8888";
    protected static $username = "checkmate";
    protected static $password = "2923a7250c0167108eb";
    protected static $database = "";
    protected static $dbdriver = "mssql";
    protected static $dbprefix = "";
    protected static $pconnect = FALSE;
    protected static $db_debug = FALSE;
    protected static $cache_on = FALSE;
    protected static $cachedir = "/dev/shm";
    protected static $char_set = "windows-1252";
    protected static $dbcollat = "SQL_Latin1_General_CP1_CI_AS";
    */

    public static $maxResults;

    public $details;

    protected $conn;

    /**
     * @static
     * @param string $name
     * @param Icm_Config $config
     * @return Icm_Db_Mssql
     */
    public static function connect($name, Icm_Config $config) {
        $instance = new self(/*mssql_connect(
            $config->getOption('hostname'),
            $config->getOption('username'),
            $config->getOption('password'),
            $config->getOption('newLink')
        )*/);
        $instance->details = array(
            $config->getOption('hostname'),
            $config->getOption('username'),
            $config->getOption('password'),
            $config->getOption('newLink'),
            $name
        );
        parent::addInstance($name, $instance);

        if ($config->getOption('maxResults')) {
            self::$maxResults = $config->getOption('maxResults');
        }

        return $instance;
    }


    protected function __construct($conn = NULL) {
        $this->conn = $conn;
    }

    public function __sleep() {
        return array('details');
    }

    public function __wakeup() {
        // echo '<pre>'.print_r($this->details, true) . '</pre>';
        $instance = new self(/*mssql_connect(
            $this->details[0],
            $this->details[1],
            $this->details[2],
            $this->details[3]
        )*/);
        // $this->_instances[$this->details[4]] = $instance;
        self::$_instances[$this->details[4]] = $instance;
        /*$this->conn = mssql_connect(
            $this->details[0],
            $this->details[1],
            $this->details[2],
            $this->details[3]
        );*/
    }
    public function beginTransaction() {

    }
    public function commit() {

    }
    public function rollBack() {

    }
    public function escape($value) {
        // http://stackoverflow.com/questions/3252651/how-do-you-escape-quotes-in-a-sql-query-using-php
        return str_replace("'", "''", $value);
    }

    public function fetch($sql, $replacements = array(), $callback = null) {
        $this->getConnection();
        $sql = $this->_parseBindings($sql, $replacements);
        $results = mssql_query($sql, $this->conn);

        $row = mssql_fetch_assoc($results);
        if (!is_null($callback)) {
            $row = $callback($row);
        }
        mssql_free_result($results);
        return $row;
    }

    public function maxResults() {
        return self::$maxResults;
    }

    public function fetchAll($sql, $replacements = array(), $callback = null) {
        $this->getConnection();
        $sql = $this->_parseBindings($sql, $replacements);
        Icm_Util_Logger_Syslog::getInstance('api')->logDebug('sql: ' . $sql);
        $rows = array();

        $results = mssql_query($sql, $this->conn);

        while ($row = mssql_fetch_assoc($results)) {
            $rows[] = (!is_null($callback)) ? $callback($row) : $row;
        }
        mssql_free_result($results);
        return $rows;
    }

    /**
     * Safely escape any bound params
     * @return string $sql
     * @param string $sql
     * @param array $replacements
     */
    protected function _parseBindings($sql, array $replacements) {
        if (count($replacements)) {
            foreach ($replacements as $key => $val) {
                $sql = str_replace(":$key", "'" . $this->escape($val) . "'", $sql);
            }
        }
        return $sql;
    }

    public function execute($sql, $replacements = array(), $callback = null) {

    }

    public function lastInsert() {

    }

    protected function getConnection(){
        if (is_null($this->conn)){
            $this->conn = mssql_connect(
                    $this->details[0],
                    $this->details[1],
                    $this->details[2],
                    $this->details[3]
            );
        }
    }
}
