<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/20/12
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Db_Table
{

    /**
     * @var Icm_Db_Interface
     */
    protected $conn;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var $pk
     */
    protected $pk;

    /**
     * @param string $tableName Name of the table
     * @param string $pk Table's primary key
     * @param Icm_Db_Interface $conn a connection to the database
     */
    public function __construct($tableName, $pk, Icm_Db_Interface $conn) {

        $this->tableName = $tableName;
        $this->pk = $pk;
        $this->conn = $conn;
    }

    /**
     * Find all rows with $fieldName = $value
     * @param string $fieldName
     * @param mixed $value
     * @return array
     */
    public function findBy($fieldName, $value) {

        return $this->conn->fetchAll("
            SELECT
                *
            FROM
              {$this->tableName}
            WHERE
              {$fieldName} = {$value}
        ", array());

    }

    /**
     * Selects all rows from the current table which match the given array of key => value pairs
     * @param array $values is an associative array of key => value pairs
     * @return array of all data from the current table matching the query parameters
     */
    public function findByFields(array $values){
        $sql = "SELECT * FROM {$this->tableName} WHERE";
        $replacements = array();
        $counter = 0;
        $comma = '';
        foreach ($values as $field => $value){
            $sql .= " {$comma} {$field} = :val{$counter}";
            $comma = "AND";
            $replacements[':val' . $counter] = $value;
            $counter++;
        }

        return $this->conn->fetchAll($sql, $replacements);
    }

    /**
     * Find one row by field name, where it is equal to $value
     * @param string $fieldName
     * @param mixed $value
     * @return array
     */
    public function findOneBy($fieldName, $value) {

        return $this->conn->fetch("
            SELECT
                *
            FROM
              {$this->tableName}
            WHERE
              {$fieldName} = :val
        ", array(':val' => $value));
    }

    /**
     * Find one row by one or more field names => value pair(s)
     * @param array $values is an associative array: array('field1Name' => 'field1Value', 'field2Name' => 'field2Value)
     * @return array if successful; false otherwise
     */
    public function findOneByFields(array $values){
        $sql = "SELECT * FROM {$this->tableName} WHERE";
        $replacements = array();
        $counter = 0;
        $comma = '';
        foreach ($values as $field => $value){
            $sql .= " {$comma} {$field} = :val{$counter}";
            $comma = "AND";
            $replacements[':val' . $counter] = $value;
            $counter++;
        }
        return $this->conn->fetch($sql, $replacements);
    }

    /**
     * @param string $fieldName
     * @param mixed $value
     * @return bool
     */
    public function rowExists($fieldName, $value = NULL) {
        if (is_array($fieldName)){
            $query = "SELECT * FROM `{$this->tableName}` WHERE ";
            $query .= $this->parseWhere($fieldName);
            $query .= " LIMIT 1;";
            $replacements = $this->makeReplacements($fieldName);
            $result = $this->conn->fetch($query, $replacements);
            return $result !== false; //if it returns anything other than (bool)false, that means we got data
        }
        else{
            $value = $this->conn->escape($value);
            $result = $this->conn->fetch("
                SELECT
                    COUNT({$fieldName}) as num_rows
                FROM
                  {$this->tableName}
                WHERE
                  {$fieldName} = {$value}
            ");

            return $result['num_rows'] > 0;
        }
    }

    protected function parseWhere(array $where){
        $and = '';
        $sql = '';
        foreach ($where as $key => $value){
            $sql .= "$and`$key` = :$key";
            $and = " AND ";
        }
        return $sql;
    }

    protected function makeReplacements(array $where){
        $return = array();
        foreach ($where as $key => $value){
            $return[':' . $key] = $value;
        }
        return $return;
    }

    /**
     * Insert one row with the given field / value pairs
     * @param array $values is an associative array: array('field1Name' => field1Value)
     * @return boolean true if the insert operation was successful; false otherwise
     */
    public function insertOne(array $values){
        $sql = "INSERT INTO {$this->tableName} (";
        $comma = '';
        $counter = 0;
        $replacements = array();
        $valuesString = '';
        foreach ($values as $field => $value){
            $sql .= "{$comma}`{$field}`";
            $valuesString .= "{$comma}:val{$counter}";
            $comma = ', ';
            $replacements[':val' . $counter] = $value;
            $counter++;
        }
        $sql .= ") VALUES ($valuesString)";

        return $this->conn->execute($sql, $replacements) > 0;
    }

    /**
     * @param array $values is an associative array: array('field1Name' => field1Value)
     * @param string $primaryKey is the key by which the row to be changed will be identified. This row's key => value pair MUST be present in $values
     * @param bool $skipOverNull
     * @return boolean true if a row was updated; false otherwise. Will return false if the target row was already in the desired state
     * Fadi added modification. I surronded the $fieldname variable with `` otherwise it would rais an error.
     */
    public function updateOne(array $values, $primaryKey = null, $skipOverNull = true){

        $primaryKey = ($primaryKey == null) ? $this->pk : $primaryKey;

        $sql = "UPDATE {$this->tableName} SET ";
        $comma = '';
        $counter = 0;
        $replacements = array();
        $primaryKeyReplacement = NULL;
        foreach ($values as $field => $value){

             // skip over null fields
            if ($skipOverNull && is_null($value)) {
                continue;
            }

            $sql .= "{$comma}`{$field}`=:val{$counter}";
            $comma = ', ';
            $replacements[':val' . $counter] = $value;
            if ($field == $primaryKey){
                $primaryKeyReplacement = $counter;
            }
            $counter++;
        }
        $sql .= " WHERE `{$primaryKey}`=:val{$primaryKeyReplacement}";
        return $this->conn->execute($sql, $replacements) > 0;
    }

    /**
     * @param array $order is an associative array: array('field1' => 'ASC', 'field2' => 'DESC')
     */
    public function getAll($order = NULL){
        $orderBy = '';
        if ($order != NULL){
            $orderBy = " ORDER BY ";
            $comma = '';
            foreach ($order as $key => $ord){
                $orderBy .= "$comma `$key` $ord";
                $comma = ', ';
            }
        }
        return $this->conn->fetchAll("SELECT * FROM {$this->tableName} $orderBy");
    }

    public function getLastInsert(){
        return $this->conn->lastInsert();
    }

    public function getPrimaryKey(){
        return $this->pk;
    }

    /**
     * @param $sql
     * @param $replacements
     * @return mixed
     */
    public function execute($sql, $replacements) {
        return $this->conn->execute($sql, $replacements);
    }
}
