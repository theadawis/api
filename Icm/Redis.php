<?php
/**
 * ICM Redis connection class
 * Requires predis or phpredis
 *
 * pear channel-discover pear.nrk.io
 * pear install nrk/predis
 *
 * https://github.com/nicolasff/phpredis
 *
 * @author Joe Linn
 */
class Icm_Redis{
    const DEFAULT_DATABASE = 0;

    protected $defaultConfig = array(
        'scheme' => 'tcp',
        'host' => '127.0.0.1',
        'port' => '6379',
        'driver' => 'predis' //predis or phpredis
    );

    protected $config = array();

    protected $redis;

    /**
     * @param Icm_Config $config
     */
    public function __construct(Icm_Config $config){
        foreach ($this->defaultConfig as $key => $value){
            $this->config[$key] = $config->getOption($key, $value);
        }
        switch($this->config['driver']){
            case 'predis':
                require_once 'Predis/Autoloader.php';
                Predis\Autoloader::register();
                $this->redis = new Predis\Client($this->config);
                break;
            case 'phpredis':
                $this->redis = new Redis();
                $this->redis->connect($this->config['host'], $this->config['port']);
                break;
        }
    }

    /**
     * Pass calls to the instantiated Predis object
     * @param string $name
     * @param unknown_type $args
     * @return mixed
     */
    public function __call($name, $args){
        if ($this->config['driver'] == 'phpredis' && $name == 'pipeline'){
            // quasi-hack to make predis pipelining syntax work with phpredis
            $name = 'multi';
        }
        return call_user_func_array(array($this->redis, $name), $args);
    }

    public function getRedis() {
        return $this->redis;
    }
}