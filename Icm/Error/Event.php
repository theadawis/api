<?php
/**
 * Event.php
 * User: chris
 * Date: 2/11/13
 * Time: 11:50 AM
 */
class Icm_Error_Event extends Icm_Event{

    /**
     * @var Exception
     */
    protected $e;
    protected $visitorId;

    /**
     * @param Exception $e
     */
    public function __construct($visitorId = null, Exception $e = null) {
        $this->setVisitorId($visitorId);
        $this->setException($e);
    }

    /**
     * @param Exception $e
     */
    public function setException(Exception $e) {
        $this->e = $e;
    }

    /**
     * @return Exception
     */
    public function getException() {
        return $this->e;
    }

    public function setVisitorId($visitorId) {
        $this->visitorId = $visitorId;
    }

    public function getVisitorId() {
        return $this->visitorId;
    }
}
