<?php
/**
 * Listener.php
 * User: chris
 * Date: 2/11/13
 * Time: 11:55 AM
 */

class Icm_Error_Listener implements Icm_Event_Listener_Interface
{

    /**
     * @var Zend_Log
     */
    protected $logger;

    public function __construct(Zend_Log $logger) {
        $this->logger = $logger;
    }

    public function dispatch($evtName, Icm_Event_Interface $event) {
        if (is_a($event, "Icm_Error_Event")) {
            /**
             * @var Icm_Error_Event $event
             * @var Exception $e
             */

            $e = $event->getException();
            $this->logger->log($e->getMessage(), Zend_Log::ERR, array(
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "stack_trace" => $e->getTrace(),
                "stack_trace_string" => $e->getTraceAsString(),
                "exception_class" => get_class($e)
            ));
        }
    }
}
