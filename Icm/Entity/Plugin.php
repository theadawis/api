<?php
interface Icm_Entity_Plugin extends Icm_Configurable
{

    public function setFactoryMap(Icm_Entity_Factory_Map $map);
    public function __construct(Icm_Config $c = null);


    /**
     * Return the appropriate data if preconditions are met (proper entity type passed in) and useful data is found. Return NULL otherwise.
     * @param Icm_Entity $e
     * @return mixed|NULL
     */
    public function __invoke(Icm_Entity $e);
    // public function populate(Icm_Entity $e);

}
