<?php

/**
 * Class Icm_Entity_Map
 * @author : Shiem Edelbrock
 * @description:  This class gets called from Icm_Entity.
 *                Icm_Entity will iterate over all properties and call an Icm_Entity_Map method
 *                if it find an item is set and/or a property listed from a config file.
 *                (see entity_config.ini for mapping);
 */
class Icm_Entity_Map
{
    protected static $_instance = null;
    protected $_map = null;

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getSection($section){
        return (isset($this->_map->{$section})) ? $this->_map->{$section} : null;
    }

    /**
     * Create and set a new date helper object, plus day/month/year, on the incoming parent
     *
     * @param Icm_Entity $parent Parent object in which the given key was found
     * @param string $key The key that triggered the method
     * @param stdClass $value The value of $key
     */
    public function dateHelper(&$parent, &$key, &$value){
        // this is a lexis formated date | array('day' => '23', month => '07', 'year' => '1984') | Lexis always has a Month && Year
        if (isset($value->month) && isset($value->year)
            && !($value instanceof Icm_Util_DateHelper || $parent instanceof Icm_Util_DateHelper)) {

            $lexisDate = $value;
            $day = isset($lexisDate->day) ? $lexisDate->day : 1;

            $parent->$key = new Icm_Util_DateHelper();
            $parent->$key->setDay($day);
            $parent->$key->setMonth($lexisDate->month);
            $parent->$key->setYear($lexisDate->year);
        }
    }

    /**
     * Create and set a new datehelper object on the incoming parent object
     *
     * @param Icm_Entity $parent
     * @param string $key
     * @param string $value
     */
    public function dobHelper(&$parent, &$key, &$value){
        // ensure neither value nor parent are datehelpers
        if (!($value instanceof Icm_Util_DateHelper || $parent instanceof Icm_Util_DateHelper)) {
            $date = new Icm_Util_DateHelper();
            $date->setTimestamp(strtotime($value));
            $parent->$key = $date;
        }
    }

    /**
     * Create and set a new location entity on the parent object
     *
     * @param Icm_Entity $parent
     * @param string $key
     * @param mixed $value
     */
    public function addressHelper(&$parent, &$key, &$value){
        // check that we've got a zip5 and no existing location entities
        if ((isset($value->zip5) || (is_array($value) && isset($value['zip5'])))
            && !($parent instanceof Icm_Entity_Location || $value instanceof Icm_Entity_Location)) {

            $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('location')->create();

            foreach ($value as $k => $v){

                if (strtolower($k) == 'address') {
                    $entity->set('street', $v);
                }

                $entity->set($k, $v);
            }

            $parent->$key = $entity;
        }

        // if no zip5, check for zip and no current location entities
        if (isset($value->zip) && !($parent instanceof Icm_Entity_Location || $value instanceof Icm_Entity_Location)) {
            $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('location')->create();

            foreach ($value as $k => $v){

                if (strtolower($k) == 'address') {
                    $entity->set('street', $v);
                }

                $entity->set($k, $v);
            }

            $parent->$key = $entity;
        }
    }

    /**
     * Create and set a phone entity on the incoming parent object
     *
     * @param Icm_Entity $parent
     * @param string $key
     * @param mixed $value
     */
    public function phoneGenerator(&$parent, &$key, &$value){
        if (is_array($value)){

            foreach ($value as $phoneArr => $phone){

                if (isset($phone->phone10) && !($phone instanceof Icm_Entity_Phone)) {
                    $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('phone')->create();
                    $entity->set('phone_number', $phone->phone10);
                    $entity->pretty_phone = preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '($1) $2-$3', $phone->phone10);

                    unset($phone->phone10);

                    foreach ($phone as $k => $v){
                        $entity->set($k, $v);
                    }

                    $parent->$key = $entity;
                }
            }

            return;
        }

        if (isset($value->phone10) && !($parent instanceof Icm_Entity_Phone || $value instanceof Icm_Entity_Phone)) {
            $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('phone')->create();
            $entity->set('phone_number', $value->phone10);
            unset($value->phone10);

            foreach ($value as $k => $v){
                $entity->set($k, $v);
            }

            $parent->$key = $entity;
        }
    }

    /**
     *  Create and set a location entity on the incoming parent object
     *
     * @param Icm_Entity $parent
     * @param string $key
     * @param mixed $value
     */
    public function locationGenerator(&$parent, $key, $value){
        // ensure neither parent nor value are locations
        if ($parent instanceof Icm_Entity_Location || $value instanceof Icm_Entity_Location) {
            return;
        }

        if ($key == 'location'){
            $location = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('location')->create();

            foreach ($value as $key => $value){
                if (strtolower($key) == 'address') {
                    $location->set('street', $value);
                }

                $location->set($key, $value);
            }

            $parent->location = $location;
        }
        else if (is_object($parent) && property_exists($parent, 'zip')){
            $parent->location = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('location')->create();
            $parent->location->set('zip', $parent->zip);
        }
    }

    /**
     * Create and set a person entity on the incoming parent object
     *
     * @param Icm_Entity $parent
     * @param string $key
     * @param mixed $value
     */
    public function personGenerator(&$parent, &$key, &$value){
        // ensure neither parent nor value are persons
        if ($parent instanceof Icm_Entity_Person || $value instanceof Icm_Entity_Person) {
            return;
        }

        if (is_array($value)){

            foreach ($value as $personArr => $person){
                $entity = $this->mapValuesToPersonEntity($person);

                if (!empty($entity)) {
                    $parent->$key = $entity;
                }
            }

            return;
        }

        $entity = $this->mapValuesToPersonEntity($value);

        if (!empty($entity)) {
            $parent->$key = $entity;
        }
    }

    /**
     * Map the attributes of a stdClass to a person entity
     *
     * @param stdClass $valueObject
     * @return Icm_Entity_Person
     */
    protected function mapValuesToPersonEntity($valueObject) {
        if (isset($valueObject->name)){
            $entity = Icm_Api::getInstance()->dataContainer->get('entityFactoryMap')->getEntityFactory('person')->create();

            if (isset($valueObject->name->first)) {
                $entity->set('first_name', $valueObject->name->first);
            }

            if (isset($valueObject->name->middle)) {
                $entity->set('middle_name', $valueObject->name->middle);
            }

            if (isset($valueObject->name->last)) {
                $entity->set('last_name', $valueObject->name->last);
            }

            if (isset($valueObject->uniqueId)) {
                $entity->set('personId', $valueObject->uniqueId);
            }

            foreach ($valueObject as $k => $v){
                $entity->set($k, $v);
            }

            return $entity;
        }

        return null;
    }

    private function __construct(){
        $_map = Icm_Api::getInstance()->dataContainer->get('entityPropertiesMap')->getIterator();
        $this->callCount = 0;
        $this->personCallCount = 0;
        $this->_map = new stdClass();

        foreach ($_map as $k => $v) {
            $this->_map->{$k} = $v;
        }
    }
}
