<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/11/12
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Business extends Icm_Entity
{

    protected $businessName;

    /**
     * @param $businessName
     */
    public function setBusinessName($businessName){
        $this->businessName = $businessName;
    }

    public function getHash() {
        return array();
    }

}
