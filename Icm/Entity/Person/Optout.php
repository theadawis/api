<?php

class Icm_Entity_Person_Optout extends Icm_Entity_Person {

    public static function create($properties=array()) {

         // sweet hack to set the person id the same as external person id from db
        if (isset($properties['external_person_id'])) {
            $properties['person_id'] = $properties['external_person_id'];
        }

        // add some fields to propConfig
        self::$propConfig[] = 'id';
        self::$propConfig[] = 'status';
        self::$propConfig[] = 'optout_request_id';
        self::$propConfig[] = 'external_person_id';
        self::$propConfig[] = 'source_id';
        self::$propConfig[] = 'email';
        self::$propConfig[] = 'order_id';
        self::$propConfig[] = 'ip_address';
        self::$propConfig[] = 'date_created';
        self::$propConfig[] = 'date_added';
        self::$propConfig[] = 'added_csr_id';
        self::$propConfig[] = 'date_approved';
        self::$propConfig[] = 'approved_csr_id';
        return parent::create($properties);
    }
}