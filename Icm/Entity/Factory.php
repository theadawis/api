<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/26/12
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Factory
{

    protected $type;
    protected $broker;

    /**
     * @var ReflectionClass
     */
    // protected $reflection;

    public function __construct($type, Icm_Entity_Plugin_Broker $broker = NULL) {
        $this->setType($type);
        if (!is_null($broker)){
            $this->setBroker($broker);
        }
    }

    public function setType($type) {
        if (!class_exists($type)) {
            throw new Icm_Exception("Class $type does not exist");
        }
        $r = new ReflectionClass($type);
        if (!$r->isSubclassOf('Icm_Entity')) {
            throw new Icm_Exception("Class $type is not a subclass of Icm_Entity");
        }
        $this->type = $type;
        // $this->reflection = $r;
    }

    public function setBroker(Icm_Entity_Plugin_Broker $b) {
        $this->broker = $b;
    }


    /**
     * @return Icm_Entity
     */
    public function create(){
        /**
         * @var Icm_Entity $e
         */
        $r = new ReflectionClass($this->type);
        $e = $r->newInstance();
        if (!is_null($this->broker)){
            $e->setBroker($this->broker);
        }
        return $e;
    }

    public function fromStruct(Icm_Struct $struct){
        $e = $this->create();
        foreach ($struct as $key => $value){
            $e->set($key, $value);
        }
        return $e;
    }

}
