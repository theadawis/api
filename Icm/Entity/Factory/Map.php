<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 8/1/12
 * Time: 9:12 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Factory_Map
{
    protected $factories = array();
    protected $brokers = array();
    protected $entityPropertiesMap = array();

    public function attach($key, Icm_Entity_Factory $f, Icm_Entity_Plugin_Broker $b) {
        $this->factories[$key] = $f;
        $this->brokers[$key] = $b;
    }

    public function getEntityFactory($key) {
        if (!array_key_exists($key, $this->factories)) {
            throw new Icm_Entity_Exception("No factory registered to key: $key");
        }

        /**
         * @var Icm_Entity_Factory $f;
         * @var Icm_Entity_Plugin_Broker $b
         */
        $f = $this->factories[$key];
        $b = $this->brokers[$key];
        $b->setFactoryMap($this);
        $f->setBroker($b);

        return $f;
    }
}
