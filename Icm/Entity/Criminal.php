<?php
/**
 *
 */
class Icm_Entity_Criminal extends Icm_Entity_Person { //I suppose this doesn't account for BofA -- definetely a criminal business :D (how very droll, Chris)
    protected static $propConfig = array(
        'first_name',
        'last_name',
        'generation',
        'middle_name',
        'birthState',
        'dob',
        'age',
        'case_number',
        'aka1',
        'aka2',
        'dob_aka',
        'address',
        'address2',
        'city',
        'state',
        'zip',
        'hair_color',
        'eye_color',
        'height',
        'weight',
        'race',
        'sex',
        'skin_tone',
        'scars_marks',
        'military_service',
        'charge_category',
        'charges_filed_date',
        'offense_date',
        'offense_code',
        'offense_description1',
        'offense_description2',
        'ncic_code',
        'counts',
        'plea',
        'conviction_date',
        'conviction_place',
        'court',
        'source',
        'sentence_date',
        'probation_date',
        'disposition',
        'disposition_date',
        'court_costs',
        'arresting_agency',
        'case_type',
        'fines',
        'source_name',
        'source_state',
        'birth_state',
    );

    public static function create($row) {
        self::$propConfig = array_merge(self::$propConfig, parent::$propConfig);
        self::$propConfig = array_unique(self::$propConfig);
        self::$propConfig[] = 'yob';

        // person objects need yob..if its not in $row, but dob is, then derive yob from dob
        if (!isset($row['yob']) && isset($row['dob']) && !empty($row['dob'])) {
            $row['yob'] = substr($row['dob'], -4);
        } else {
            $row['yob'] = '';
        }

        // make a dob field for sorting by dob
        self::$propConfig[] = 'dob_sort';
        if (strlen(trim($row['dob'])) > 0) {
            $pieces = explode('/', $row['dob']);
            // $row['dob_sort'] = $pieces[2] . $pieces[0] . $pieces[1];
            $row['dob_sort'] = $pieces[2] . $pieces[0];
        } else {
            $row['dob_sort'] = '';
        }

        return parent::create($row);
    }
}