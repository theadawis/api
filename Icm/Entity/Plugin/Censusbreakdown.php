<?php
/**
 * Uses an Icm_Service_Zipcodes_Db adapter to find and return ome data for a Icm_Entity_Location object
 * @author Shiem Edelbrock
 *
 */
class Icm_Entity_Plugin_Censusbreakdown extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Zipcodes_Db
     */
    protected $adapter;

    protected $dbFields = array(
        'AverageHouseValue',
        'Latitude',
        'Longitude',
        'Elevation',
        'NumberOfBusinesses',
        'NumberOfEmployees',
        'BusinessAnnualPayroll',
        'GrowthRank',
        'Population',
        'HouseholdsPerZipcode',
        'IncomePerHousehold',
        'County'
    );

    public function setAdapter(Icm_Service_Zipcodes_Db $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e instanceof Icm_Entity_Location){
           return $this->adapter->get($this->dbFields, $e->getZip());
        }

        return null;
    }
}