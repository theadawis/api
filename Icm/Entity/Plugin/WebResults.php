<?php

/**
*    @author Ryan Baker
*
*/

class Icm_Entity_Plugin_WebResults extends Icm_Entity_Plugin_Abstract{

    protected $yahooAdapter;

    protected $searchType;

    public function setSearchType(){
        $this->searchType = array("web");

    }

    public function __invoke(Icm_Entity $e){

        $this->setSearchType();

        if ($e->isA('Icm_Entity_Person')){
            $first = $e->first_name;
            $last = $e->last_name;
            if ($first == null || $last == null) {
                return null;
            }

            $query = $first . "+" . $last;
            return $this->yahooAdapter->search($query, $this->searchType);
        }
        elseif ($e->isA('Icm_Entity_Crime')){
            $pc = $e->penal_code;
            if ($pc == null) {
                return null;
            }
            $query = "site:*.gov penal code $pc";
            return $this->yahooAdapter->search($query, $this->searchType);
        }
        elseif ($e->isA('Icm_Entity_Business')){
            $busiName = $e->getBusinessName();
            if ($busiName == null) {
                return null;
            }
            $query = $busiName;
            return $this->yahooAdapter->search($query, $this->searchType);

        }
        elseif ($e->isA('Icm_Entity_Criminal')){
            $first_name = $e->first_name;
            $last_name = $e->last_name;
            if ($first_name == null || $last_name == null) {
                return null;
            }
            $query = "$first_name+$last_name";
            return $this->yahooAdapter->search($query, $this->searchType);
        }
        elseif ($e->isA('Icm_Entity_Location')){
            $zip = $e->get('zip_code');
            if ($zip == null) {
                return null;
            }
            $query = "zip code: $zip";
            return $this->yahooAdapter->search($query, $this->searchType);
        }
        else{
            return null;
        }

    }

    public function setAdapter(Icm_Service_Yahoo_Api $a){
        $this->yahooAdapter = $a;
    }


}
