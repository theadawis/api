<?php
/**
 * Uses an Icm_Service_Zipcodes_Db adapter to find and return home value data for a Icm_Entity_Location object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Homevalue extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Zipcodes_Db
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Zipcodes_Db $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Location')){
            return $this->adapter->getAverageHouseValue($e->getZip());
        }
        return NULL;
    }
}