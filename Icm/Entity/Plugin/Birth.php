<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return a birth record for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Birth extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $params = array(
                'firstName' => $e->first_name,
                'middlename' => isset($e->middle_name)?$e->middle_name:NULL,
                'lastName' => $e->last_name,
                'state' => $e->state
            );
            $data = $this->adapter->birthSearch($params);
            if (is_null($data)){
                return array();
            }
            return $data;
        }
        return NULL;
    }
}