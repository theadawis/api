<?php

/**
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Location extends Icm_Entity_Plugin_Abstract{
    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person') && !empty($e->zip)){
            $street = isset($e->street) ? trim($e->street) : trim($e->address);

            $address = array(
                'zip' => $e->zip,
                'address' => $street,
                'city' => $e->city,
                'state' => $e->state
            );

            if (preg_match("/^P[.| ]?O\\.? /uim", $street)) {
                $address['is_pobox'] = true;
            }

            $location = $this->map->getEntityFactory('location')->create()->fromArray($address);

            return $location;
        }

        return NULL;
    }
}
