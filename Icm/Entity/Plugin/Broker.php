<?php
class Icm_Entity_Plugin_Broker
{

    protected $plugins = array();
    protected $factoryMap;

    public function registerPlugin($key, Icm_Entity_Plugin $p) {
       $this->plugins[$key] = $p;
    }

    public function setFactoryMap(Icm_Entity_Factory_Map $map) {
        $this->factoryMap = $map;
    }
    /**
     * @param $key
     * @throws Icm_Exception if object retrieved from container is not an Icm_Entity_Plugin.
     * @return Icm_Entity_Plugin|bool
     */
    public function getPlugin($key) {
        if (array_key_exists($key, $this->plugins)) {

            $plugin = $this->plugins[$key];
            if (!is_a($plugin, 'Icm_Entity_Plugin')) {
                throw new Icm_Exception(get_class($plugin) . "is not an instance of Icm_Entity_Plugin.  Shame.");
            }
            return $plugin;
        }
        return false;
    }

    public function hasPlugin($key){
        return array_key_exists($key, $this->plugins);
    }

    public function __invoke($key, Icm_Entity $e) {
        /**
         * @var Icm_Entity_Plugin $plugin
         */
        $plugin = $this->getPlugin($key);
        if ($plugin) {
            $plugin->setFactoryMap($this->factoryMap);
            $e->set($key, $plugin($e));
        }
        return true;
    }
}
