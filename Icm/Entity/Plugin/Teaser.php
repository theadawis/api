<?php
/**
 * Uses an Icm_Search_Adapter_Lexis source to get a single Lexis teaser record
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Teaser extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Lexis_Teaser
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Lexis_Teaser $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person') && !empty($e->person_id)){
            // echo '<pre>'.print_r($e, true) . '</pre>';
            $e->person_id = str_pad($e->person_id, 12, '0', STR_PAD_LEFT);
            echo 'PERSON ID ' . $e->person_id;
            exit;
            $teaserData = $this->adapter->thinRollupPersonSearch(array('UniqueId' => $e->person_id));
            // echo '<pre>'.print_r($teaserData, true) . '</pre>';
        }
        return NULL;
    }
}