<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return marriage / divorce records for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Marriage extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $params = array(
                'firstName' => $e->first_name,
                'lastName' => $e->last_name,
                'state' => $e->state
            );
            $data = $this->adapter->marriageSearch($params);

            if (is_null($data)){
                return array();
            }

            $people = array();

            // filter out names with no middle initial
            if (isset($e->middle_name)) {
                $middleInitial = substr($e->middle_name,0,1);

                foreach($data as $person) {
                    if ($person['first_person_middle_name'] == $middleInitial) {
                        $people[] = $person;
                    }
                }
            } else {
                $people = $data;
            }

            return $people;
        }

        return NULL;
    }
}
