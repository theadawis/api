<?php

/**
 * Uses an Icm_Service_Fbi_Db adapter to render FBI data for the city and state (all years).  Uses
 * Icm_Service_Zipcodes_Db adapter to geoencode the Icm_Entity_Location object if city/state are not available
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Fbidata extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Zipcodes_Db
     */
    protected $adapter;

    /**
     * @param Icm_Service_Zipcodes_Db $geo
     * @param Icm_Service_Fbi_Db $a
     */
    public function setAdapter(Icm_Service_Zipcodes_Db $geo, Icm_Service_Fbi_Db $a){
        $this->adapter = $a;
        $this->geoService = $geo;
    }

    public function __invoke(Icm_Entity $e){
        // only worry about locations
        if (!($e instanceof Icm_Entity_Location)) {
            return null;
        }

        // if we don't have this info, we can try to reverse geocode
        if ((!$e->getCity() || !$e->getStateLongName()) && $e->getZip()){
            $geoData = $this->geoService->getAliasesByZips(array($e->getZip()));

            $e->set('stateLongName', $geoData[0]['stateLongName']);
            $e->set('city', $geoData[0]['city']);
        }

        $args = array(
            'cityName' => ucwords($e->getCity()),
            'stateName' => strtolower($e->getStateLongName()),
        );

        return array(
            'city' => $this->adapter->getCityStats($args),
            'state' => $this->adapter->getStateStats($args)
        );
    }
}