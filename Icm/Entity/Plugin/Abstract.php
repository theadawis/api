<?php
/**
 * @author Joe Linn
 *
 */
abstract class Icm_Entity_Plugin_Abstract implements Icm_Entity_Plugin{
    /**
     * @var Icm_Config
     */
    protected $config;

    /**
     * @var Icm_Entity_Factory_Map
     */
    protected $map;
    /**
     * @return Icm_Config
     */
    public function getConfig(){
        if (is_null($this->config)){
            $this->config = new Icm_Config();
        }
        return $this->config;
    }

    /**
     * @param Icm_Config $config
     */
    public function setConfig(Icm_Config $config){
        $this->config = $config;
    }

    public function setFactoryMap(Icm_Entity_Factory_Map $map) {
        $this->map = $map;
    }

    public function __construct(Icm_Config $c = NULL){
        $this->config = $c;
    }

    abstract public function __invoke(Icm_Entity $e);
}