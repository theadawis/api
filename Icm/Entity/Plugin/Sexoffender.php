<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return sex offender records
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Sexoffender extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    protected $skipKeys = array(
        'scars_marks',
        'state',
        'offense_code',
        'offense_description1',
        'offense_description2'
    );

    public function setAdapter(Icm_Service_Npd_Db $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $params = array(
                    'firstname' => $e->first_name,
                    'lastname' => $e->last_name,
                    'city' => $e->getCity(),
                    'state' => $e->getState(),
                    'zip' => substr($e->zip, 0, 5)
            );
            $data = $this->adapter->sexOffenderSearch($params);

            if (is_null($data)){
                return array();
            }

            foreach ($data as &$d){
                $d = $this->structToEntity($d);
            }

            return $data;
        }
        else if ($e->isA('Icm_Entity_Location')){
            $params = array(
                'zip' => substr($e->zip, 0, 5)
            );

            $data = $this->adapter->sexOffenderSearch($params);

            if (is_null($data)){
                return array();
            }

            $data = $this->removeDuplicates($data);
            $sort = function($a, $b){

                // offenders with mugshots should show up first
                if (!empty($a['image']) && empty($b['image'])){
                    return -1;
                }
                else if (empty($a['image']) && !empty($b['image'])){
                    return 1;
                }

                if (strcasecmp($a['last_name'], $b['last_name']) > 0){
                    // $a comes before $b
                    return 1;
                }
                else if (strcasecmp($a['last_name'], $b['last_name']) < 0){
                    // $a comes after $b
                    return -1;
                }

                // last names are the same
                if (strcasecmp($a['first_name'], $b['first_name']) > 0){
                    // $a comes before $b
                    return 1;
                }
                else if (strcasecmp($a['first_name'], $b['first_name']) < 0){
                    // $a comes after $b
                    return -1;
                }

                return 0;
            };

            usort($data, $sort);

            foreach ($data as &$d){
                $d = $this->structToEntity($d);
            }

            return $data;
        }

        return null;
    }

    protected function removeDuplicates(array $data){
        $return = array();
        $hashes = array();

        foreach ($data as $d){
            $hash = md5($d['first_name'] . $d['last_name'] . $d['address'] . $d['offense_description1']);

            if (!in_array($hash, $hashes)){

                if (!isset($return[$d['first_name'] . $d['last_name']])){
                    $return[$d['first_name'] . $d['last_name']] = $d;
                }
                else{
                    $return[$d['first_name'] . $d['last_name']]['offense_description1'] .= '; ' . $d['offense_description1'];
                }

                $hashes[] = $hash;
            }
        }

        return $return;
    }

    /**
     * Convert the given struct into a sexoffender entity
     *
     * @param Icm_Struct $struct
     * @return Icm_Entity_Sexoffender
     */
    public function structToEntity($struct){
        // fix the case of the struct values
        foreach ($struct as $key => $value) {
            // skip processing some of the values
            if (!in_array($key, $this->skipKeys)) {
                $struct[$key] = ucwords(strtolower($struct[$key]));
            }
        }

        return $this->map->getEntityFactory('sexoffender')->create()->fromArray($struct);
    }
}
