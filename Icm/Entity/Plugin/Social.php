<?php

/*
*    @author: Ryan Baker
*
*/

class Icm_Entity_Plugin_Social extends Icm_Entity_Plugin_Abstract{

    protected $socialAdapter;

    public function __invoke(Icm_Entity $e){

        if ($e->isA('Icm_Entity_Person')){

            $searchFields = array('country' => 'US', 'no_sponsored' => true, 'exact_name' => false);

            $first = $e->first_name;
            $middle = $e->middle_name;
            $last = $e->last_name;
            $yob = $e->yob;
            $dob = $e->dob;
            $city = $e->city;
            $state = $e->state;
            $phone = $e->phone_number;

            // Min req
            if (    $first == null ||
                $last == null  ||
                ($yob == null  && $city == null && $state == null)){
                return null;
            }
            else{
                $searchFields['first_name'] = $first;
                $searchFields['last_name'] = $last;
            }
            if ($middle != null){
                $searchFields['middle_name'] = $middle;
            }
            // Get from, to age
            if ($yob != null){
                date_default_timezone_set('America/Los_Angeles');
                $curr_year = date("Y");
                $age = $curr_year - $yob;
                $from_age = $age - 1;
                $to_age = $age + 1;
                /* $serchFields['from_age'] = $from_age;
                $searchFields['to_age'] = $to_age; */
            }
            if ($city != null){
                $searchFields['city'] = $city;
            }
            if ($state != null){
                $searchFields['state'] = $state;
            }
            if ($phone != null){
                $searchFields['phone_number'] = $phone;
            }

            $searchResults = $this->socialAdapter->search($searchFields);
            $results = new Icm_Collection(array());
            $goodDomains = array('myspace.com', 'linkedin.com', 'facebook.com');
            foreach ($searchResults as $result){
                // if (in_array($result->source['domain'], $goodDomains)){
                /* if (isset($result->images) && sizeof($result->images)){
                    $results->add($result);
                } */
            }

            // echo '<pre>'.print_r($searchFields, true) . '</pre>'; exit;
            return $results;

        }

        return null;

    }

    public function setAdapter(Icm_Service_Pipl_Api $a){
        $this->socialAdapter = $a;
    }
}
