<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return criminal records for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Criminal extends Icm_Entity_Plugin_Abstract
{
    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    /**
     * @var Icm_Entity_Factory
     */
    protected $criminalFactory;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $dob = NULL;

            if (isset($e->dob)){
                $dob = $this->formatDob($e->get('dob'));
            }

            $params = array(
                'firstName' => $e->get('first_name'),
                'state' => $e->get('state'),
                'lastName' => $e->get('last_name'),
                'DOB' => $dob
            );

            $data = $this->adapter->criminalSearch($params);

            if (!empty($e->priorAddresses) && is_array($e->priorAddresses)){
                foreach ($e->priorAddresses as $prior){
                    $params['state'] = $prior['state'];
                    $newData = $this->adapter->criminalSearch($params);
                    $data = array_merge($data, $newData);
                }
            }


            $data = array_unique($data);

            foreach ($data as &$d){
                $d = $this->structToEntity($d);
            }

            $data = new Icm_Search_Result($params, 'criminal', $data);

            return $data;
        }

        return NULL;
    }

    protected function formatDob($dob){
        if (substr_count($dob, '/') < 1){
            return '00/00/' . $dob;
        } else if (substr_count($dob, '/') < 2){
            $dob = explode('/', $dob);
            $dob = $dob[0] . '/00/' . $dob[1];
        }

        return $dob;
    }

    public function setFactory($cf){
        $this->criminalFactory = $cf;
    }

    protected function structToEntity(Icm_Struct_Criminalrecord $struct){
        return $this->map->getEntityFactory('criminal')->create()->fromArray($struct->toArray());
    }
}