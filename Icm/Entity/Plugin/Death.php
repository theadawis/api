<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return a death record for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Death extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $params = array(
                    'firstName' => $e->first_name,
                    'ssn' => NULL,
                    'lastName' => $e->last_name,
                    'dob' => $e->dob
            );
            $data = $this->adapter->deathSearch($params);
            if (is_null($data)){
                return array();
            }
            return $data;
        }
        return NULL;
    }
}