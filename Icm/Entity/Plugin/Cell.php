<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return a cell phone record for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Cell extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            if (!isset($e->phone)){
                return array();
            }
            $params = array(
                    'firstname' => $e->first_name,
                    'state' => $e->state,
                    'lastname' => $e->last_name,
                    'phone' => $e->phone
            );
            $data = $this->adapter->cellSearch($params);
            return $data;
        }
        return NULL;
    }
}