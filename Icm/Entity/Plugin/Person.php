<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Person extends Icm_Entity_Plugin_Abstract{

    protected $adapter;

    public function setAdapter($a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Location')){
            $args = array(
                'address' => $e->street,
                'city' => $e->city,
                'state' => $e->state,
                'zip' => $e->zip
            );
            $people = $this->adapter->reverseAddress($args);
            $people = $this->removeDuplicates($people);
            $sort = function($a, $b){
                if (strcasecmp($a['last_name'], $b['last_name']) > 0){
                    // $a comes before $b
                    return 1;
                }
                else if (strcasecmp($a['last_name'], $b['last_name']) < 0){
                    // $a comes after $b
                    return -1;
                }
                // last names are the same
                if (strcasecmp($a['first_name'], $b['first_name']) > 0){
                    // $a comes before $b
                    return 1;
                }
                else if (strcasecmp($a['first_name'], $b['first_name']) < 0){
                    // $a comes after $b
                    return -1;
                }
                return 0;
            };
            usort($people, $sort);
            foreach ($people as &$person){
                $person = $this->map->getEntityFactory('person')->create()->fromArray($person);
            }
            return $people;
        }
        return NULL;
    }

    protected function removeDuplicates($people){
        $hashes = array();
        $return = array();
        foreach ($people as $p){
            $hash = $p['first_name'] . $p['last_name'];
            if (!in_array($hash, $hashes)){
                $return[] = $p;
                $hashes[] = $hash;
            }
        }
        return $return;
    }
}