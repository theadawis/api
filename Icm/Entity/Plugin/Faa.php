<?php
/**
 * Uses an Icm_Service_Npd_Api adapter to find and return a FAA license record for an Icm_Entity_Person object
 * @author Joe Linn
 *
 */
class Icm_Entity_Plugin_Faa extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Npd_Api
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Npd_Api $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e->isA('Icm_Entity_Person')){
            $params = array(
                    'Firstname' => $e->first_name,
                    'Lastname' => $e->last_name,
            );
            $data = $this->adapter->faaLicenseSearch($params);
            if (is_null($data)){
                return array();
            }
            return $data;
        }
        return NULL;
    }
}