<?php

/**
 * Uses an Icm_Service_Zodiac_Db adapter to find and return zodiac data for a Icm_Entity_Person object
 * @author Emacs Daddy
 *
 */
class Icm_Entity_Plugin_Zodiac extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Zodiac_Db
     */
    protected $adapter;

    public function setAdapter(Icm_Service_Zodiac_Db $a){
        $this->adapter = $a;
    }

    public function __invoke(Icm_Entity $e){
        if ($e instanceof Icm_Entity_Person){
            return function($a, $b) {
                return $this->adapter->getCompatibility($a, $b);
            };
        }

        return null;
    }
}
