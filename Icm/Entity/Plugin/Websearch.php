
<?php

/**
 *    @author Ryan Baker
 *
 */

class Icm_Entity_Plugin_Websearch extends Icm_Entity_Plugin_Abstract{

    /**
     * @var Icm_Service_Yahoo_Api
     */
    protected $yahooAdapter;

    protected $type;

    public function setResultType($type) {
        $this->type = $type;
    }

    public function __invoke(Icm_Entity $e){
        /**
         * @var Icm_Entity_Person $e
         */
        if ($e->isA('Icm_Entity_Person')){
            $first = $e->first_name;
            $last = $e->last_name;
            if ($first == null || $last == null) {
                return null;
            }

            $query = $first . "+" . $last;
            return $this->yahooAdapter->search($query, array($this->type));
        }
        elseif ($e->isA('Icm_Entity_Crime')){
            /**
             * @var Icm_Entity_Crime $e
             */
            $pc = $e->penal_code;
            if ($pc == null) {
                return null;
            }
            $query = "site:*.gov penal code $pc";
            return $this->yahooAdapter->search($query($this->type));
        }
        elseif ($e->isA('Icm_Entity_Business')){
            /**
             * @var Icm_Entity_Business $e
             */
            $busiName = $e->getBusinessName();
            if ($busiName == null) {
                return null;
            }
            $query = $busiName;
            return $this->yahooAdapter->search($query, array($this->type));

        }
        elseif ($e->isA('Icm_Entity_Criminal')){
            /**
             * @var Icm_Entity_Criminal $e
             */
            $first_name = $e->first_name;
            $last_name = $e->last_name;
            if ($first_name == null || $last_name == null) {
                return null;
            }
            $query = "$first_name+$last_name";
            return $this->yahooAdapter->search($query, array($this->type));
        }
        elseif ($e->isA('Icm_Entity_Location')){
            /**
             * @var Icm_Entity_Criminal $e
             */
            $zip = $e->zip_code;
            if ($zip == null) {
                return null;
            }
            $query = "zip code: $zip";
            return $this->yahooAdapter->search($query, array($this->type));
        }
        else{
            return null;
        }

    }

    public function setAdapter(Icm_Service_Yahoo_Api $a){
        $this->yahooAdapter = $a;
    }


}
