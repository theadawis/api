<?php

/**
 * Popular person entity file
 *
 * @version 1.0
 * @package Api
 * @subpackage Api_Entity
 */

/**
 * Popular person entity class
 *
 * @package Api
 * @subpackage Api_Entity
 */
class Icm_Entity_PopularPerson extends Icm_Entity {

    /**
     * field map
     * @var array $propConfig field map
     */
    protected static $propConfig = array(
        'name',
    );

    /**
     *
     * @param array $row result set to map to $propConfig
     * @return Icm_Entity
     */
    public static function create($row) {
        return parent::create($row);
    }

    /**
     * (non-PHPdoc)
     * @see Icm_Entity::getHash()
     */
    public function getHash(){
        return array();
    }

}