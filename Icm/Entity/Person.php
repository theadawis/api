<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 4/11/12
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Entity_Person extends Icm_Entity {

    protected static $propConfig = array(
        'first_name',
        'last_name',
        'validity_date',
        'middle_name',
        'yob',
        'dob',
        'address',
        'city',
        'county_name',
        'state',
        'zip',
        'phone_number',
        'person_id',
        'hash_key',
        'source',
        'age',
        'priorAddresses',
        'aliases',
        'relatives',
        'first_last_name',
        'priorPhones',
        'professionalLicenses',
    );

    public static function create($row) {
        self::$propConfig[] = 'yob';

        // person objects need yob..if its not in $row, but dob is, then derive yob from dob
        if (!isset($row['yob']) && isset($row['dob']) && !empty($row['dob'])) {
            $row['yob'] = substr($row['dob'], -4);
        } else if (!isset($row['yob'])){
            $row['yob'] = '';
        }

        // make a dob field for sorting by dob
        self::$propConfig[] = 'dob_sort';
        if ( isset($row['dob']) && strlen($row['dob']) > 0) {
            $pieces = explode('/', $row['dob']);

            if (isset($pieces[2]) && isset($pieces[0])){
                $row['dob_sort'] = $pieces[2] . $pieces[0];
            }
            else{
                $row['dob_sort'] = '';
            }
        } else {
            $row['dob_sort'] = '';
        }

        // phone_number is sometimes just a space character
        if (!isset($row['phone_number']) || $row['phone_number'] == ' ') {
            $row['phone_number'] = '';
        }

        return parent::create($row);
    }

    public function formatData(){
        self::$propConfig[] = 'yob';

        // person objects need yob..if its not in $row, but dob is, then derive yob from dob
        if (!isset($this->yob) && isset($this->dob) && !empty($this->dob)) {
            $this->yob = substr($this->dob, -4);
        }
        else {
            $this->yob = '';
        }

        // make a dob field for sorting by dob
        self::$propConfig[] = 'dob_sort';

        if (!empty($this->dob) && strlen($this->dob) > 0) {
            $pieces = explode('/', $this->dob);

            if (isset($pieces[2]) && isset($pieces[0])) {
                $this->dob_sort = $pieces[2] . $pieces[0];
            }
            else {
                $this->dob_sort = '';
            }
        }
        else {
            $this->dob_sort = '';
        }

        // phone_number is sometimes just a space character
        if (!isset($this->phone_number) || $this->phone_number == ' ') {
            $this->phone_number = '';
        }
    }

    public function hasHas( $args = array() ) {

         // check if this is a new style optout
        if (isset($args['person_id'])) {
            return ($this->person_id == $args['person_id']) ? true : false;
        }

         // old style
        return parent::hasHash($args);
    }

    public function getHash(){
         // compare by first_name, last_name and other possible parameters
        $hashArray = array(
            'first_name',
            'last_name',
            'person_id',
            'city',
            'state',
            'yob',
        );

        $return = array();
        foreach ($hashArray as $hash){
            $method = 'get'.ucfirst($hash);
            $var = $this->$method();
            if (!empty($var)){
                $return[$hash] = $var;
            }
        }

        return $return;
    }
}

