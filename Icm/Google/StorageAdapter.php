<?php
/**
 * Retrieves user info from admin db.
 * @author fadi
 */
class Icm_Google_StorageAdapter extends Icm_Db_Table
{
   /**
    * Searches admin db for a user with a username that matches
    * gmail account.
    * @param array $user
    * @return array
    */
   public function verifyUser($user) {
       $userEmail = $user['email'];
       $sql = "SELECT `id`, `username`, `first_name`, `last_name`, `role_id`
           FROM {$this->tableName} WHERE `email` = :email";
       $parameter =  array(':email' => $userEmail);
       return $this->conn->fetch($sql, $parameter);
   }
}
