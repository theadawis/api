use cg_analytics;
INSERT INTO `cg_application` (`id`,`name`,`url`,`desc`)VALUES('1','InstantCheckmate','www.instantcheckmate.com','Public background checks');


INSERT INTO `cg_segment` (`id`,`key`,`value`,`app_id`,`super`)VALUES('1','src','6527','1','0');
INSERT INTO `cg_event_map`(`id`,`type`,`slug`,`desc`,`app_id`,`order`,`last_modified`)VALUES('1','URL','home_page_view','Home Page View','1','1','1339779797');
INSERT INTO `split_section` (`id`,`name`,`type`,`slug`,`app_id`)VALUES('1','Home Page','URL','home_page', '1');
INSERT INTO `split_meta`(`id`,`key`,`value`,`app_id`) VALUES ('1','buttonColor','green','1');
INSERT INTO `split_variation`(`id`,`label`,`meta_ids`,`section_id`,`status`,`desc`,`type`)VALUES('1','StandardVariation1','1','1','1','Test variations','custom');

INSERT INTO `split_test`(`id`,`app_id`,`control_variation_id`,`section_id`,`type`,`name`,`desc`,`weight`,`status`,`last_modified`,`start_time`,`end_time`)VALUES('1','1','1','1','buttonTest','big button test','This tests green buttons','1','1','1339779797','1339779797','1339779797');

INSERT INTO `split_goal`(`id`,`event_id`,`test_id`,`primary_goal`)VALUES('1','1','1','0');

INSERT INTO `split_test_variation_assoc`(`id`,`test_id`,`variation_id`,`weight`,`active`)
VALUES('1','1','1','1','1');

