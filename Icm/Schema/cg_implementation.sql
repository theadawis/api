DROP DATABASE IF EXISTS cg_analytics;
CREATE DATABASE cg_analytics;


USE cg_analytics;
CREATE USER 'cg_dbuser'@'%' IDENTIFIED BY 'cgdbadminp@55';

CREATE USER 'cg_dbuser'@'localhost' IDENTIFIED BY 'cgdbadminp@55';
GRANT ALL ON cg_analytics.* TO 'cg_dbuser'@'%';



CREATE TABLE `cg_application` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255),
    `desc` VARCHAR(255),
    UNIQUE(`name`)
)AUTO_INCREMENT=1000;
/*Populate application with at least one row 
INSERT INTO `cg_application` (`name`,`url`,`desc`)VALUES('InstantCheckmate','www.instantcheckmate.com','Public background checks');
*/

CREATE TABLE `cg_segment`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `key` VARCHAR(255) NOT NULL,
    `value` VARCHAR(255) NOT NULL,
    `super` BOOL NOT NULL DEFAULT 0,
    `app_id` INT NOT NULL,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;
/*Populate segment with at least one row 
INSERT INTO `cg_segment` (`key`,`value`,`app_id`,`super`)VALUES('src','6527','1','0');
*/

CREATE TABLE `cg_event_map`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `type` VARCHAR(255) NOT NULL,
    `slug` VARCHAR(255) NOT NULL,
    `desc` VARCHAR(255),
    `app_id` INT NOT NULL,
    `order` INT NOT NULL,
    `last_modified` INT(11) NOT NULL,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`)
)AUTO_INCREMENT = 1000;
/*Populate event map
INSERT INTO `cg_event_map`(`type`,`slug`,`desc`,`app_id`,`order`,`last_modified`)VALUES('URL','home_page_view','Home Page View','1','1','1339779797');
*/

CREATE TABLE `split_section`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `app_id` INT NOT NULL,
	`slug` VARCHAR(255) NOT NULL,
    `type` VARCHAR(255),
    `default_variation_id` INT,
	`control_variation_id` INT,
    `order` SMALLINT,
	`url` VARCHAR(255) NOT NULL,
	`path` VARCHAR(255) NOT NULL,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;
/*Populate context with at least one row 
INSERT INTO `split_section` (`name`,`type`,`slug`,`app_id`)VALUES('Home Page','URL','home_page', '1');
*/

CREATE TABLE `split_meta`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `key` VARCHAR(255) NOT NULL,
    `value` VARCHAR(255) NOT NULL,
    `app_id` INT NOT NULL,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;
/*Populate meta with at least one row
 INSERT INTO `split_meta`(`key`,`value`,`app_id`) VALUES ('buttonColor','green','1');
*/

CREATE TABLE `split_variation`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    `meta_ids` VARCHAR(255) NOT NULL,
    `section_id` INT NOT NULL,
    `status` INT NOT NULL,
    `desc` VARCHAR(255),
    `type` VARCHAR(255)
)AUTO_INCREMENT = 1000;
/*Populate variation with at least one row*/
/*INSERT INTO `split_variation`(`label`,`meta_ids`,`section_id`,`status`,`desc`,`type`)VALUES('StandardVariation1','1','1','1','Test variations','custom');
*/

CREATE TABLE `split_test`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `app_id` INT NOT NULL,
    `control_variation_id` INT,
    `section_id` INT NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `desc` VARCHAR(255),
    `weight` INT NOT NULL,
    `status` INT NOT NULL,
    `last_modified` INT(11) NOT NULL,
    `start_time` INT(11),
    `end_time` INT(11),
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`section_id`) REFERENCES `split_section`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;
/*INSERT INTO `split_test`(`app_id`,`control_variation_id`,`section_id`,`type`,`name`,`desc`,`weight`,`status`,`last_modified`,`start_time`,`end_time`)VALUES('1','1','1','buttonTest','big button test','This tests green buttons','1','1','1339779797','1339779797','1339779797');
*/


CREATE TABLE `split_goal`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `event_id` INT NOT NULL,
    `test_id` INT NOT NULL,
    `primary_goal` BOOL NOT NULL DEFAULT 0,
    FOREIGN KEY (`event_id`) REFERENCES `cg_event_map`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE   
)AUTO_INCREMENT = 1000;
/*
INSERT INTO `split_goal`(`event_id`,`test_id`,`primary_goal`)VALUES('1','1','0');
*/

CREATE TABLE `split_test_variation_assoc`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `test_id` INT NOT NULL,
    `variation_id` INT NOT NULL,
    `weight` INT NOT NULL,
    `active` BOOL NOT NULL DEFAULT 0,
    FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`variation_id`) REFERENCES `split_variation`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;
/*INSERT INTO
    `split_test_variation_assoc`(`test_id`,`variation_id`,`weight`,`active`)
    VALUES('1','1','1','1');
*/

CREATE TABLE `split_test_segment_assoc`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `test_id` INT NOT NULL,
    `segment_id` INT NOT NULL,
    `exclusive` BOOL NOT NULL DEFAULT 0,
    FOREIGN KEY (`segment_id`) REFERENCES `cg_segment`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;

CREATE TABLE `split_context_event_assoc`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `section_id` INT NOT NULL,
    `event_id` INT NOT NULL,
    FOREIGN KEY (`section_id`) REFERENCES `split_section`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`event_id`) REFERENCES `cg_event_map`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;

CREATE TABLE `split_test_notes_assoc`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `user_name` VARCHAR(255) NOT NULL, 
    `test_id` INT NOT NULL,
    `type` VARCHAR(255),
    `note` TEXT NOT NULL,
    `app_id` INT NOT NULL,
    `timestamp` INT NOT NULL,
    FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;

CREATE TABLE `cg_variation_summary`(
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,
    `app_id` INT NOT NULL,
    `section_context_id` INT NOT NULL,
    `test_id` INT NOT NULL,
    `variation_id` INT NOT NULL,
	`event_id` INT NOT NULL,
    `views` INT NOT NULL,
    `conversions` INT NOT NULL,
    `timestamp` INT(11) NOT NULL,
    FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`section_context_id`) REFERENCES `split_section`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`event_id`) REFERENCES `cg_event_map`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`variation_id`) REFERENCES `split_variation`(`id`) ON DELETE CASCADE
)AUTO_INCREMENT = 1000;



CREATE TABLE `cg_variation_agg_stat`(
	`app_id` INT NOT NULL,
	`variation_id` INT NOT NULL,
	`test_id` INT NOT NULL,
	`event_id` INT NOT NULL,
	`standard_deviation` DECIMAL(14,4),
	`standard_error` DECIMAL(14,4),
	`improvement` DECIMAL(14,4),
	`pooled_sample_proportion` DECIMAL(14,4),
	`estimated_standard_error` DECIMAL(14,4),
	`two_proportion_obt_z` DECIMAL(14,4),
	`expected_chi_squared` DECIMAL(14,4),
	`chi_squared` DECIMAL(14,4),
	`effect_size_chi_squared` DECIMAL(14,4),
	`chi_significance` BOOL,
	`obt_z_significance` BOOL,
	`obt_z_difference` DECIMAL(14,4),
	`normal_distribution` DECIMAL(14,4),
	`views` INT,
	`conversions` INT,
	`conversion_rate` DECIMAL(5,4),
	PRIMARY KEY (`variation_id`,`test_id`,`event_id`),
	FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE,	
	FOREIGN KEY (`variation_id`) REFERENCES `split_variation`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`event_id`) REFERENCES `cg_event_map`(`id`) ON DELETE CASCADE
);

CREATE TABLE `cg_test_agg_stat`(
	`app_id` INT NOT NULL,
	`test_id` INT NOT NULL,
	`event_id` INT NOT NULL,
	`views` INT,
	`conversions` INT,
	`conversion_rate` DECIMAL(5,4),
	PRIMARY KEY (`test_id`,`event_id`),
	FOREIGN KEY (`app_id`) REFERENCES `cg_application`(`id`) ON DELETE CASCADE,	
	FOREIGN KEY (`test_id`) REFERENCES `split_test`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`event_id`) REFERENCES `cg_event_map`(`id`) ON DELETE CASCADE
);
