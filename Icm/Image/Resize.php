<?php
/**
 * @author Joe Linn
 *
 */
class Icm_Image_Resize{

    /**
     * @var resource
     */
    protected $image;

    protected $imageType;

    /**
     * @param string $name can be either a URL or a file path
     * @return Icm_Image_Resize
     */
    public static function load($name){
        $image = new self();
        $image->image = imagecreatefromstring(file_get_contents($name));
        return $image;
    }

    /**
     * Save the loaded image to a file
     * @param string $filename path and name of the new image file
     * @param int $imageType
     * @param int $compression
     * @param int $permissions
     */
    public function save($filename, $imageType = IMAGETYPE_JPEG, $compression = 75, $permissions = NULL){
        switch($imageType){
            case IMAGETYPE_JPEG: imagejpeg($this->image, $filename, $compression);
                break;
            case IMAGETYPE_GIF: imagegif ($this->image, $filename, $compression);
                break;
            case IMAGETYPE_PNG: imagepng($this->image, $filename, $compression);
                break;
        }

        if ($permissions != NULL){
            chmod($filename, $permissions);
        }
    }

    /**
     * Returns the raw image data in the specified format
     * @param int $imageType
     * @return string
     */
    public function output($imageType = IMAGETYPE_JPEG){
        ob_start();

        switch($imageType){
            case IMAGETYPE_JPEG: imagejpeg($this->image);
                break;
            case IMAGETYPE_GIF: imagegif ($this->image);
                break;
            case IMAGETYPE_PNG: imagepng($this->image);
                break;
        }

        $output = ob_get_clean();
        return $output;
    }

    /**
     * Returns base64 encoded image data ready to be inserted into the src attribute of a html img tag
     * @param int $imageType
     * @return string
     */
    public function outputSrc($imageType = IMAGETYPE_JPEG){
        $type = 'jpeg';

        switch($imageType){
            case IMAGETYPE_JPEG: $type = 'jpeg';
                break;
            case IMAGETYPE_GIF: $type = 'gif';
                break;
            case IMAGETYPE_PNG: $type = 'png';
                break;
        }

        return 'data:image/' . $type.';base64, '.base64_encode($this->output($imageType));
    }

    /**
     * Resize the current image to specified dimensions
     * @param int $width
     * @param int $height
     * @return Icm_Image_Resize
     */
    public function resize($width, $height){
        $newImage = imagecreatetruecolor($width, $height);
        imagecopyresampled($newImage, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;

        return $this;
    }

    /**
     * @return number
     */
    public function getWidth(){
        return imagesx($this->image);
    }

    /**
     * @return number
     */
    public function getHeight(){
        return imagesy($this->image);
    }

    /**
     * Resize the current image to the specified width
     * @param int $width
     * @return Icm_Image_Resize
     */
    public function resizeToWidth($width){
        $ratio = $width / $this->getWidth();
        $height = $this->getHeight() * $ratio;

        return $this->resize($width, $height);
    }

    /**
     * Resize the current image to the specified height
     * @param int $height
     * @return Icm_Image_Resize
     */
    public function resizeToHeight($height){
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;

        return $this->resize($width, $height);
    }

    /**
     * Constrain the current image to the specified dimensions. If the image is already smaller than or equal to the given dimensions, no resizing will be performed.
     * @param int $maxWidth
     * @param int $maxHeight
     * @return Icm_Image_Resize
     */
    public function resizeToConstraints($maxWidth, $maxHeight){
        // $start = microtime(true);
        if ($this->getWidth() > $maxWidth){
            $this->resizeToWidth($maxWidth);
        }

        if ($this->getHeight() > $maxHeight){
            $this->resizeToHeight($maxHeight);
        }

        return $this;
    }

    /**
     * Resize the current image to the specified dimensions. Any extra space will be filled white, and the image will be centered.
     * @param int $maxWidth
     * @param int $maxHeight
     * @return Icm_Image_Resize
     */
    public function resizeWithWhitespace($maxWidth, $maxHeight){
        $xPosition = 0;
        $yPosition = 0;
        $height = $maxHeight;
        $width = $maxWidth;

        if ($this->getWidth() >= $this->getHeight() && $this->getWidth() > $maxWidth){
            $width = $maxWidth;
            $ratio = $maxWidth / $this->getWidth();
            $height = $this->getHeight() * $ratio;
        }

        if ($this->getHeight() >= $this->getWidth() && $this->getHeight() > $maxHeight){
            $height = $maxHeight;
            $ratio = $maxHeight / $this->getHeight();
            $width = $this->getWidth() * $ratio;
        }

        $yPosition = ($maxHeight - $height) / 2;
        $xPosition = ($maxWidth - $width) / 2;
        $newImage = imagecreatetruecolor($maxWidth, $maxHeight);
        $white = imagecolorallocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $white);
        imagecopyresampled($newImage, $this->image, $xPosition, $yPosition, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $newImage;

        return $this;
    }
}