<?php
class Icm_Session
{

    /**
     * @var Icm_Session_Storage_Interface
     */
    protected static $_storage;

    protected static $_started = false;

    protected static $_defaultOptionsSet = false;

    protected static $_isClosed = false;

    protected static $_defaultOptions = array(
        'save_path'                 => null,
        'name'                      => null, /* this should be set to a unique value for each application */
        'save_handler'              => null,
        // 'auto_start'                => null, /* intentionally excluded (see manual) */
        'gc_probability'            => null,
        'gc_divisor'                => null,
        'gc_maxlifetime'            => null,
        'serialize_handler'         => null,
        'cookie_lifetime'           => null,
        'cookie_path'               => null,
        'cookie_domain'             => null,
        'cookie_secure'             => null,
        'cookie_httponly'           => null,
        'use_cookies'               => null,
        'use_only_cookies'          => 'on',
        'referer_check'             => null,
        'entropy_file'              => null,
        'entropy_length'            => null,
        'cache_limiter'             => null,
        'cache_expire'              => null,
        'use_trans_sid'             => null,
        'bug_compat_42'             => null,
        'bug_compat_warn'           => null,
        'hash_function'             => null,
        'hash_bits_per_character'   => null
    );


    public static function start() {

        if (session_id() || self::$_started) {
            throw new Icm_Exception("Session already started");
        }
        if (!self::$_defaultOptionsSet) {
            self::setOptions(array());
        }
        session_start();
        register_shutdown_function(array(get_called_class(), 'writeClose'));
        self::$_started = true;
    }

    public static function setOptions(array $userOptions) {
        if (!self::$_defaultOptionsSet) {
            foreach (self::$_defaultOptions as $defaultOption => $value) {
                if (isset(self::$_defaultOptions[$defaultOption])) {
                    ini_set("session.$defaultOption", $value);
                }
            }
            self::$_defaultOptionsSet = true;
        }

        foreach ($userOptions as $userOption => $value) {
            ini_set('session.' . strtolower($userOption), $value);
        }

    }

    public static function isStarted() {
        return self::$_started;
    }

    public static function setStorageAdapter(Icm_Session_Storage_Interface $storage) {
        self::$_storage = $storage;
        session_set_save_handler(
            array(&self::$_storage, 'open'),
            array(&self::$_storage, 'close'),
            array(&self::$_storage, 'read'),
            array(&self::$_storage, 'write'),
            array(&self::$_storage, 'destroy'),
            array(&self::$_storage, 'gc')
        );
    }

    public static function writeClose() {
        session_write_close();
        self::$_isClosed = true;
        self::$_started = false;
    }
}
