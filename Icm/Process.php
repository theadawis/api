<?php
declare(ticks=1);

abstract class Icm_Process{
    protected $isRunning = false;
    protected $children = array();

    protected $handleLockFile;
    protected $pidName = 'phpdaemon';

    protected $config = array();
    protected $defaultConfig = array(
        'pidName' => 'phpdaemon',
        'user' => 2, //daemon
        'group' => 2,
        'maxChildren' => 10,
        'logging' => false
    );

    public function __construct(Icm_Config $config = NULL){
        if (!is_null($config)){
            foreach ($this->defaultConfig as $key => $default){
                $this->config[$key] = $config->getOption($key, $default);
            }
        }
        else{
            $this->config = $this->defaultConfig;
        }
        $this->start();
    }

    final protected function start(){
        $this->log("We're starting.");
        $pidFileName = "/var/run/{$this->config['pidName']}.pid";
        if (!($this->handleLockFile = fopen($pidFileName, "a+"))){
            // script is already running
            return 0;
        }
        if (!flock($this->handleLockFile, LOCK_EX | LOCK_NB, $wouldblock) || $wouldblock){
            // script already running
            flcose($this->handleLockFile);
            return 0;
        }
        $this->writePidFile();

        $childPid = pcntl_fork();
        if ($childPid === -1){
            // something has gone terribly wrong
            die('Could not fork!');
        }
        else if ($childPid){
            // this is the parent process
            echo "Child PID: $childPid\n";
            sleep(2);  //wait for child to wake up
            flock($this->handleLockFile, LOCK_UN); //release the lock on our PID file
            fclose($this->handleLockFile); //close PID file
            exit; //kill the parent
        }
        while(!($this->handleLockFile = fopen($pidFileName, "a+"))){
            // keep trying to open the PID file until it works
            usleep(100);
        }
        $this->writePidFile();

        /* posix_setuid($this->config['user']);
        posix_setgid($this->config['group']); */
        chdir('/');
        fclose(STDIN);
        fclose(STDOUT);
        fclose(STDERR);
        $STDIN = fopen('/dev/null', 'r');
        $STDOUT = fopen('/dev/null', 'wb');
        $STDERR = fopen('/dev/null', 'wb');
        // $STDERR = fopen('/var/log/' . $this->config['pidName'] . '.error.log', 'ab');
        // TODO: logging
        pcntl_signal(SIGTERM, array($this, "signalHandler"));
        pcntl_signal(SIGHUP, array($this, "signalHandler"));
        pcntl_signal(SIGUSR1, array($this, "signalHandler"));
        pcntl_signal(SIGCHLD, array($this, "signalHandler"));
        posix_setsid(); //set session leader
        $this->isRunning = true; //we're running!
        while($this->isRunning){
            if (sizeof($this->children) <= $this->config['maxChildren']){
                $this->run();
            }
        }
    }

    final private function writePidFile(){
        ftruncate($this->handleLockFile, 0);
        fseek($this->handleLockFile, 0, 0);
        fwrite($this->handleLockFile, getmypid());
        fflush($this->handleLockFile);
    }

    final protected function createWorker(){
        $pid = pcntl_fork();
        if ($pid === -1){
            // we have a problem...
            die("Could not fork worker!");
        }
        else if ($pid == 0){
            // this is the child; do stuff
            chdir('/');
            // TODO: change user
            /* pcntl_signal(SIGTERM, array($this, "signalHandler"));
            pcntl_signal(SIGHUP, array($this, "signalHandler")); */
            if (@get_resource_type(STDIN) && @get_resource_type(STDIN) != 'Unknown'){
                fclose(STDIN);
                fclose(STDOUT);
                fclose(STDERR);
            }
            $STDIN = fopen('/dev/null', 'r');
            $STDOUT = fopen('/tmp/phpdaemon.log', 'wb');
            $STDERR = fopen('/dev/null', 'wb');
            $this->worker();
        }
        else{
            // still in the parent; keep track of the child's PID and carry on
            $this->children[$pid] = true;
            $this->log(sizeof($this->children) . ' children.');
        }
    }

    public function signalHandler($signo, $pid = NULL, $status = NULL){
        $this->log("Received signal $signo");
        switch($signo){
            case SIGTERM: $this->stop();
                break;
            case SIGHUP: //TODO: handle restart
                break;
            case SIGUSR1: $this->status();
                break;
            case SIGCHLD:
                if (!$pid){
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                while($pid > 0){
                    // completed child processes are present
                    if ($pid && isset($this->children[$pid])){
                        unset($this->children[$pid]);
                        $this->log('Killed a child. '.sizeof($this->children) . ' children remaining.');
                    }
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                break;
        }
    }

    /**
     * Child classes do work here
     */
    abstract protected function run();

    abstract protected function worker();

    protected function stop(){
        $this->log("we're stopping");
        $this->isRunning = false;
        flock($this->handleLockFile, LOCK_UN); //release the lock on our PID file
        fclose($this->handleLockFile); //close PID file
        unlink("/var/run/{$this->config['pidName']}.pid"); //delete PID file
    }

    protected function log($string){
        if ($this->config['logging']){
            $handle = fopen("/var/log/{$this->config['pidName']}.log", "a");
            flock($handle, LOCK_EX, $wouldblock);
            fwrite($handle, date("Y-m-d h:i:s", time()) . ' ' . $string."\r\n");
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

    protected function status(){
        // TODO: figure out what to do here...
        $this->log(sizeof($this->children) . ' children running.');
    }
}