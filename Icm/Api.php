<?php
class Icm_Api {
    /*
      API has a need for bootstrapping.  in particular we need:
        - declare an environment in which to operate (production, stage, qa, development, etc..)
        - autoload API libs
        - interface with api services held in Containers

      Icm_Api provides this bootstrap functionality for applications. For example
    */
    protected static $_instance;

    protected static $_defaultConfigDir = "/etc/tcg/api/";

    /**
     * @var Icm_Config
     */
    protected $_config;

    /**
     * @var Icm_Config
     */
    protected $_defaults;

    protected $_environment;

    /**
     * @var Zend_Loader_Autoloader
     */
    protected $_autoloader;

    /**
     * @var Icm_Container
     */
    public $dataContainer;

    /**
     * @var Icm_SplitTest_Tracker
     */
    public $tracker;

    /**
     * @var Icm_SplitTest_Strategy
     */
    public $splitTestStrategy;

    /**
     * @var Icm_Event_Dispatcher
     */
    public $eventDispatcher;

    protected $logger;

    protected $exceptionCallbacks = array();

    /**
     *
     * @static
     * @return Icm_Api
     * @throws Icm_Exception
     */
    public static function getInstance() {
        if (isset(self::$_instance)) {
            return self::$_instance;
        }

        throw new Icm_Exception("No instance of Icm_Api exists - try: Icm_Api::bootstrap");
    }

    public static function bootstrap(Icm_Config $config, $get = array(), $post = array()) {
        self::$_instance = new Icm_Api($config, $get, $post);
        return self::getInstance();
    }

    protected function __construct(Icm_Config $config, $get = array(), $post = array()) {

        $this->post = $post;
        $this->get = $get;

        // the app can manipulate the api by passing in its own config
        $this->_initDefaults();

        // add to the include paths
        set_include_path(get_include_path() . PATH_SEPARATOR . $this->_defaults->getSection('global')->getOption('api_path'));
        set_include_path(get_include_path() . PATH_SEPARATOR . $this->_defaults->getSection('global')->getOption('api_lib_path'));
        set_include_path(get_include_path() . PATH_SEPARATOR . $this->_defaults->getSection('global')->getOption('elastica_path'));

        // default api config
        $this->_initConfig($config);

        // set up autoloader
        $this->_initAutoloader();
        if ($config->getSection('init')->getOption('predis', true)) {
            // load the Predis library
            $this->_initPredis();
        }

        // set the environment
        $this->_initEnvironment();

        if ($config->getSection('init')->getOption('session', true)) {
            $this->_initSession();
        }

        // Set logging levels
        if ($this->getConfig()->getSection("init")->getOption("error_logging")) {
            $this->_initLogging();
        }

        // bootstrap services
        $this->_initDataContainer();

    }

    protected function _initAutoloader() {
        require_once('Zend/Loader/Autoloader.php');
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace("Icm");
        $autoloader->registerNamespace("Elastica");
        $namespaceAutoloader = new Icm_Autoloader('/sites/external-libs/Bitter/src/FreeAgent/', 'FreeAgent');
        $namespaceAutoloader->register();

        $include_paths = $this->_config->getSection('global')->getOption('include_paths');

        if (is_array($include_paths)) {
            $includes = implode(PATH_SEPARATOR, $include_paths);
            set_include_path($includes . PATH_SEPARATOR . get_include_path());
        }

        $autoloadNamespaces = $this->_config->getSection('global')->getOption('autoload_namespaces');

        if (is_array($autoloadNamespaces)) {
            foreach ($autoloadNamespaces as $namespace) {
                $autoloader->registerNamespace($namespace);
            }
        }
        $this->_autoloader = $autoloader;
    }

    protected function _initPredis() {
        require_once 'Predis/Autoloader.php';
        Predis\Autoloader::register();
    }

    /**
     * @return Zend_Loader_Autoloader
     */
    public function getAutoloader() {
        return $this->_autoloader;
    }

    protected function _initConfig(Icm_Config $config) {
        $this->_config = $config;
    }

    /**
     * @return Icm_Config
     */
    public function getConfig() {
        return $this->_config;
    }

    protected function _initEnvironment() {
        // check config - if it is passed in via config that is an explicit instruction by the app and overrides all other methods of determining what environment we're in
        if ($this->_config->getSection('global')->getOption('environment')) {
            $this->_environment = $this->_config->getSection('global')->getOption('environment');
            return;
        }

        // check $_ENV
        // set an environment variable as a quick and dirty way to give API environment context
        if (isset($_ENV) && isset($_ENV['api_env'])) {
            $this->_environment = $_ENV['api_env'];
            return;
        }

        // check $_SERVER
        if (isset($_SERVER) && isset($_SERVER['api_env'])) {
            $this->_environment = $_SERVER['api_env'];
            return;
        }

        if (isset($_SERVER) && isset($_SERVER['environment'])) {
            $this->_environment = $_SERVER['environment'];
            return;
        }

        // check ENVIRONMENT constant
        if (defined('ENVIRONMENT')) {
            $this->_environment = ENVIRONMENT;
            return;
        }

        // lastly default to production
        $this->_environment = 'production';
        return;
    }

    public function getEnvironment() {
        return $this->_environment;
    }

    // defaults are generally non-app specific configuration
    // for example...apps don't care what the split test redis keys are
    // but we do need to have different keys based on what environment we're operating in
    // app specific config should come from the app and are specified in the file that is passed in to the constructor
    protected function _initDefaults() {
        $this->_defaults = Icm_Config::fromIni(self::$_defaultConfigDir . 'defaults.ini');
    }

    public function getConfigFileFromIni($filename) {
        return Icm_Config::fromIni(self::$_defaultConfigDir . $filename);
    }

    protected function _initDataContainer() {
        if (!$this->dataContainer) {
            if ($this->_config->getSection('global')->getOption('servicesConfig')) {
                $this->dataContainer = new Icm_Container();
                $this->dataContainer->loadXml(simplexml_load_file($this->_config->getSection('global')->getOption('servicesConfig')));
            }
        }
    }

    protected function _initLogging() {
        $logLevel = Zend_Log::ERR;

        if ($this->_environment != "production") {
            Icm_Util_Logger_Syslog::setLoggingLevel(LOG_DEBUG);
        } else {
            Icm_Util_Logger_Syslog::setLoggingLevel(LOG_CRIT);
        }

        /*
        if (!$this->logger) {
            $logger = new Zend_Log();
            $env = $this->_environment;
            $config = $this->getConfig();
            $visitTracker = $this->getVisitTracker();
            $analyticsTracker = $this->getTracker();
            $get = $this->get;
            $post = $this->post;

            $esErrWriter = new Icm_Log_Writer_ElasticSearch(
                $this->dataContainer->get("elasticsearchTeaserData"),
                $config, $visitTracker, $get, $post, $analyticsTracker
            );

            $esErrWriter->addFilter(new Zend_Log_Filter_Priority($logLevel));

            $logger->addWriter($esErrWriter);

            $logger->registerErrorHandler();

            $this->logger = $logger;

            if (function_exists('xdebug_print_function_stack') && $this->_environment != "production") {
                $this->addExceptionCallback(function($e) {
                    xdebug_print_function_stack($e->getMessage());
                });
            }
            $this->setExceptionHandler();
        }
        */
    }

    protected function _initSession() {

        $agent = new Icm_Util_Useragent();

        if (!$agent->isRobot()) {
            // real user; configure the Redis session handler
            $redis = new Icm_Redis($this->_config->getSection('redis'));
            $storage = new Icm_Session_Storage_Redis($this->_config->getSection('session'), $redis);
        }
        else{
            // bot; configure the dummy session handler
            $storage = new Icm_Session_Storage_Dummy();
        }

        // set the session handler
        Icm_Session::setStorageAdapter($storage);

        // start new session
        Icm_Session::start();

        // set up the session variable
        $vts = new Icm_Session_Namespace('visitTracker');
        $vts->firstPageVisit = (is_null($vts->firstPageVisit)) ? true : false;

        // Icm_Util_Logger_Syslog::getInstance('api')->logErr("SESSION:");
        // Icm_Util_Logger_Syslog::getInstance('api')->logErr(print_r($_SESSION, true));

    }

    /**
     * Initialize the visit tracker
     */
    protected function _initVisitTracker() {

        if (isset($this->visitTracker) && is_a($this->visitTracker, 'Icm_Visit_Tracker')) {
            return;
        }

        // set up the session variable
        $vts = new Icm_Session_Namespace('visitTracker');

        // try to pull the api id from the session
        $apiSession = new Icm_Session_Namespace('API');
        $appId = isset($apiSession->customer->app_id) ? $apiSession->customer->app_id : $this->_config->getSection('global')->getOption('app_id');

        // set up the visitTracker
        $this->visitTracker = new Icm_Visit_Tracker(
            $this->_config->getSection('visitTracker'),
            $this->_config->getSection('redis'),
            $this->_config->getSection('cgDb'),
            $this->_config->getSection('rabbitmq'),
            $appId
        );

        // if there's no visit data or they just came in with a src param then create a new visit
        $visitData = $this->visitTracker->getVisitData();
        $firstPageVisitWithSrc = ($vts->firstPageVisit && (isset($this->get['src']) && $this->get['src'] != ''));

        if (empty($visitData) || $firstPageVisitWithSrc) {
            // setup default get params
            $getParams = $this->get;

            // check the uri prefix to make sure it's an SEO directory page
            $peoplePrefix = '/people';
            $criminalRecordsPrefix = '/criminal-records';
            $namesPrefix = '/names';

            if (strncmp($_SERVER['REQUEST_URI'], $peoplePrefix, strlen($peoplePrefix)) == 0
                || strncmp($_SERVER['REQUEST_URI'], $namesPrefix, strlen($namesPrefix)) == 0
                || strncmp($_SERVER['REQUEST_URI'], $criminalRecordsPrefix, strlen($criminalRecordsPrefix)) == 0) {
                $getParams['sid'] = 'seo_directory';
            }

            $this->visitTracker->createVisit(null, $getParams);
        }
    }

    protected function _initTracker() {
        /**
         * Initialize the tracker
         * It only depends on the visit tracker,
         * and the decider these days.
         * Tell it if the user's logged in, too.
         *
         * It'll take care of the rest
         */
        $this->_initDataContainer();
        $testDecider = $this->dataContainer->get('testDecider');


        $this->tracker = new Icm_SplitTest_Tracker(
            $this->getVisitTracker(),
            $testDecider
        );

        // are they logged in?
        $session = $this->dataContainer->get('session');
        $this->tracker->loggedIn($session->is_logged_in);

    }


    public function getDefaults() {
        return $this->_defaults;
    }

    /**
     * @param $key
     * @return string|bool
     */
    public function getRequestParam($key) {
        if (is_array($this->get) && array_key_exists($key, $this->get)) {
            return $this->get[$key];
        } else if (is_array($this->post) && array_key_exists($key, $this->post)) {
            return $this->post[$key];
        }
        return false;
    }

    /**
     * @param string $evtClass
     * @return Icm_Analytics_Event_Factory
     */
    public function getEventFactory($evtClass = "Icm_Analytics_Event") {
      $fact = new Icm_Analytics_Event_Factory($this->getTracker(), $evtClass);
        $fact->setAppId($this->_config->getSection('global')->getOption('app_id'));
        return $fact;
    }

    public function getSplitTestStrategy() {
        if (!$this->splitTestStrategy) {
            $this->splitTestStrategy = new Icm_SplitTest_Strategy($this->getTracker(), $this->dataContainer->get("testDecider"));
        }
        return $this->splitTestStrategy;
    }

    public function restoreTests() {
        // set up our previous tests, now that we have our split testing initialized
        $cookieDomain = substr($_SERVER['HTTP_HOST'], strpos($_SERVER['HTTP_HOST'], '.'));
        $testsCookie = new Icm_Util_Cookie('tests',  $cookieDomain, 48*60*60); //expire in 2 days
        $trackedTests = unserialize($testsCookie->getValue());

        if (is_array($trackedTests)) {

            foreach ($trackedTests as $sectionId => $test) {

                try{
                    $this->tracker->trackTest($this->getSplitTestStrategy()->fromArray($sectionId, $test));
                } catch(Exception $e) {
                    Icm_Util_Logger_Syslog::getInstance('api')->logDebug("<--->Could not track: " . print_r($test, true));
                }
            }
        }
    }

    public function trackTests() {

        /**
         * We need to actually set the cookie.  I'd do this in the api, but we it really needs to be done before any
         * content is sent to the browser
         */
        $context = $this->tracker->getTrackedTestsBySection();
        $cookieDomain = substr($_SERVER['HTTP_HOST'], strpos($_SERVER['HTTP_HOST'], '.'));
        $testsCookie = new Icm_Util_Cookie('tests', $cookieDomain, 48*60*60);
        $testsCookie->setValue(serialize($context));
    }

    public function getEventDispatcher() {
        if (!$this->eventDispatcher) {
            $appName = $this->_config->getSection('global')->getOption('app_name');

            if (!$appName) {
                throw new Icm_Exception("An app_name is required under the 'global' section of the configuration");
            }

            $queueFactory = new Icm_QueueFactory($this->_config->getSection('rabbitmq'));

            $this->eventDispatcher = new Icm_Event_Dispatcher();

            $this->eventDispatcher->addSubscriber(
                new Icm_Analytics_Event_Subscriber_Syslog(Icm_Util_Logger_Syslog::getInstance($appName))
            );
            $this->eventDispatcher->addSubscriber(
                new Icm_Analytics_Event_Subscriber_Queue($queueFactory->getSplitTestQueue())
            );
            // $this->eventDispatcher->addListener("error", new Icm_Error_Listener($this->getLogger()));
        }
        return $this->eventDispatcher;
    }

    public function getTracker() {
        if (!isset($this->tracker)) {
            $this->_initTracker();
        }

        return $this->tracker;
    }

    public function getVisitTracker() {
        if (!isset($this->visitTracker)) {
            $this->_initVisitTracker();
        }

        return $this->visitTracker;
    }

    /**
     * @return Zend_Log
     */
    public function getLogger() {
        if (!$this->logger) {
            $this->_initLogging();
        }
        return $this->logger;
    }

    public function setExceptionHandler() {
        $logger = $this->logger;
        $callbacks =& $this->exceptionCallbacks;
        set_exception_handler(function(Exception $e) use ($logger, &$callbacks) {
            /**
             * @var Zend_Log $logger
             * @var Icm_Config $config
             * @var Icm_Visit_Tracker $visitTracker
             * @var Icm_SplitTest_Tracker $tracker
             */
            $logger->log($e->getMessage(), Zend_Log::ERR, array(
                "file" => $e->getFile(),
                "line" => $e->getLine(),
                "stack_trace" => $e->getTrace(),
                "stack_trace_string" => $e->getTraceAsString(),
                "exception_class" => get_class($e)
            ));

            foreach ($callbacks as $callback) {
                if (is_callable($callback) || function_exists($callback)) {
                    $callback($e);
                }
            }
        });
    }

    public function addExceptionCallback($callback) {
        if (!in_array($callback, $this->exceptionCallbacks)) {
            $this->exceptionCallbacks[] = $callback;
        }
    }
}
