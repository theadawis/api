<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/25/12
 * Time: 10:58 AM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Configurable
{
    public function getConfig();
    public function setConfig(Icm_Config $c);

}
