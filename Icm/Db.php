<?php

/**
 *
 */
class Icm_Db {

    protected static $_instances = array();

    /**
     * @static
     * @param $name
     * @return mixed
     * @throws Icm_Db_Exception
     */
    public static function getInstance($name) {
        if (array_key_exists($name, self::$_instances)) {
            return self::$_instances[$name];
        } else {
            throw new Icm_Db_Exception("Could not find connection by the name of {$name}");
        }
    }


    protected static function addInstance($name, &$instance) {
        self::$_instances[$name] =& $instance;
    }

    /**
     * translates lucene query syntax into sql syntax
     * lucene query syntax can be found here:
     * http://lucene.apache.org/core/old_versioned_docs/versions/3_5_0/queryparsersyntax.html
     *
     * this function is where we translate lucene query syntax to work for database queries
     * at the moment we support:
     * - multiple character wildcard searches at the end of a string (so we translate: foo => 'bar*' to foo like 'bar%')
     *
     * @param string $value
     * @return array
     *
     */
    public static function parseQueryString($value) {

        // check to see if the last char is a *, if so - make it a like query
        $operator = ' = ';

        $rulesFunctions = array('_multipleCharacterWildcard');
        foreach ($rulesFunctions as $f) {
            $result = self::$f($value);
            if ($result) {
                return $result;
            }
        }

        return array($operator, $value);
    }

    /*
      these are the functions that translate query syntax into sql
      they should return false if the rule doesn't match
    */

    /**
     * @param $value
     * @return bool|string
     */
    public static function _multipleCharacterWildcard($value) {
        $star = substr($value, -1);
        if ($star == '*') {
            // swap out the star for a % and make the operator a like
            $begin = substr($value, 0, -1);

            // $begin can't be empty string so we don't hose the db
            if ($begin == '') {
                throw new Icm_Service_Exception('Wildcard must be used at the end of a string with 1 or more characters');
            }

            $value = $begin.'%';
            $operator = ' like ';
            return array($operator, $value);
        }
        return false;
    }



}