<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 7/23/12
 * Time: 7:56 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Collection extends Icm_Struct implements IteratorAggregate, Countable, Serializable, ArrayAccess
{

    protected $data = array();

    public function __construct($data) {
        $this->data = $data;
    }

    public function setData(array $data) {
        $this->data = $data;
    }

    public function add($item) {
        $this->data[] = $item;
    }

    public function addAll(array $items) {
        foreach ($items as $item) {
            $this->data[] = $item;
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator() {
        return new ArrayIterator($this->data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize() {

        $toSerialized = array();
        foreach (get_object_vars($this) as $k => $v) {
            $toSerialized[$k] = $v;
        }

        return serialize($toSerialized);

    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return mixed the original value unserialized.
     */
    public function unserialize($serialized) {
        $unserialized = unserialize($serialized);
        foreach ($unserialized as $k => $v) {
            $this->$k = $v;
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count() {
        return count($this->data);
    }

    public function offsetExists($offset){
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset){
        if (!$this->offsetExists($offset)) {
            throw new Icm_Exception("Offset $offset does not exist");
        }
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value){
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset){
        if (!$this->offsetExists($offset)) {
            throw new Icm_Exception("Offset $offset does not exist");
        }
        unset($this->data[$offset]);
    }
}
