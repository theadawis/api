<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/26/12
 * Time: 10:19 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Container_Definition
{

    protected $class;
    protected $arguments = array();
    protected $calls = array();
    protected $isSingleton = false;

    public function __construct($class) {
        $this->setClass($class);
    }

    public function setClass($class) {
        $this->class = $class;
    }

    public function setSingleton($flag) {
        $this->isSingleton = $flag;
    }

    public function isSingleton() {
        return $this->isSingleton;
    }
    public function addArgument($arg) {
        $this->arguments[] = $arg;
    }

    public function addCall($name, $args) {
        $this->calls[] = array('name' => $name, 'args' => $args);
        // $this->calls[$name] = $args;
    }

    public function getCalls() {
        return $this->calls;
    }

    public function getArguments() {
        return $this->arguments;
    }

    public function getClass() {
        return $this->class;
    }
}
