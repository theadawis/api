<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/26/12
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Container_Reference
{
    public function __construct($value) {
        $this->value = $value;
    }

    public function getValue() {
        return $this->value;
    }
}
