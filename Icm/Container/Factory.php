<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chris
 * Date: 7/26/12
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Container_Factory
{

    protected static $containers = array();
    protected $parameters = array();
    protected $classes = array();
    protected $objectCache;

    public function __construct() {
        $this->objectCache = new SplObjectStorage();
    }

    public function loadXml(SimpleXMLElement $e) {
        foreach ($e->service as $service) {
            /**
             * @var SimpleXMLElement $service
             */
            $name = (string) $service['name'];

            if (empty($service['class'])) {
                throw new Icm_Exception("Service '$name' does not have a class defined");
            }

            $definition = new Icm_Container_Definition((string)$service['class']);

            if ((bool) $service['singleton']) {
                $definition->setSingleton(true);
            }

            if (!empty($service->arg)) {
                foreach ($service->arg as $arg) {
                    $definition->addArgument((string) $arg);
                }
            }

            if (!empty($service->call)) {
                foreach ($service->children() as $key => $call) {

                    if ($key == 'call'){
                        $args = array();

                        if (!empty($call->arg)) {
                            foreach ($call->arg as $arg) {

                                if ($arg->children()->count()){
                                    $argArray = array();

                                    foreach ($arg->children() as $childKey => $childValue){
                                        $argArray[$childKey] = (string)$childValue;
                                    }

                                    $args[] = $argArray;
                                }
                                else{
                                    $args[] = (string) $arg;
                                }
                            }
                        }
                        $definition->addCall((string) $call['method'], $args);
                    }
                }
            }

            $this->addDefinition((string) $service['name'], $definition);
        }

        self::$containers[(string)$e['name']] = $this;
    }

    public function addParameter($key, $value) {
        $this->parameters[$key] = $value;
    }

    public function addDefinition($key, Icm_Container_Definition $d) {
        $this->classes[$key] = $d;
    }

    public function register($key, $class) {
        $d = new Icm_Container_Definition($class);
        $this->classes[$key] = $d;

        return $d;
    }

    public function get($serviceName) {
        if (!array_key_exists($serviceName, $this->classes)) {
            throw new Icm_Exception("$serviceName has not been registered");
        }
        $definition = $this->classes[$serviceName];

        if (!$this->objectCache->contains($definition)) {
            $object = $this->create($definition);
            $this->objectCache->attach($definition, $object);
        }

        return $this->objectCache[$definition];

    }

    protected function resolveSingleton(Icm_Container_Definition $definition, ReflectionClass $r) {
        foreach ($definition->getCalls() as $call) {
            $arguments = $call['args'];
            $methodName = $call['name'];
            $arguments = $this->resolveArguments($arguments);

            if (!$r->hasMethod($methodName)) {
                throw new Icm_Exception("{$definition->getClass()} does not have a method $call");
            }

            $arguments = $this->resolveArguments($arguments);
            $method = $r->getMethod($methodName);

            if (!$method->isStatic()) {
                throw new Icm_Exception("Cannot call instance method from static context: method: $call class: {$definition->getClass}");
            }

            return $method->invokeArgs(null, $arguments);
        }
    }

    protected function resolveInstance(Icm_Container_Definition $definition, ReflectionClass $r) {

        $service = (null === $r->getConstructor()) ? $r->newInstance() : $r->newInstanceArgs($this->resolveArguments($definition->getArguments()));

        foreach ($definition->getCalls() as $call) {
            $arguments = $call['args'];
            $methodName = $call['name'];

            if (!$r->hasMethod($methodName)) {
                throw new Icm_Exception("{$definition->getClass()} does not have a method $call");
            }

            $arguments = $this->resolveArguments($arguments);
            $method = $r->getMethod($methodName);

            if ($method->isStatic()) {
                $method->invokeArgs(null, $arguments);
            }
            else {
                $method->invokeArgs($service, $arguments);
            }
        }

        return $service;
    }

    protected function create(Icm_Container_Definition $definition) {
        $r = new ReflectionClass($definition->getClass());

        try{
            $singleton = !$r->getMethod('__construct')->isPublic();
        } catch(ReflectionException $e) {
            $singleton = false;
        }

        if ($singleton) {
            return $this->resolveSingleton($definition, $r);
        }

        return $this->resolveInstance($definition, $r);
    }

    protected function resolveArguments($arguments) {
        $returnArguments = array();

        foreach ($arguments as $argument) {
            if (is_string($argument)) {
                $returnArguments[] = $this->resolveString($argument);
            }
            elseif ($argument instanceof Icm_Container_Reference) {
                /**
                 * @var Icm_Container_Reference $argument
                 */
                $returnArguments[] = $this->resolveReference($argument);
            }
            else {
                $returnArguments[] = $argument;
            }
        }

        return $returnArguments;
    }

    protected function resolveString($argument) {
        $matches = array();

        if (preg_match('/^@(.*)/', $argument, $matches)) {
            return $this->resolveReference(new Icm_Container_Reference($matches[1]));
        }
        elseif (preg_match('/^:(.*)/', $argument, $matches)){
            $value = self::getContainer($matches[1]);

            if (!$value) {
                throw new Icm_Exception("Reference to $matches[1] not defined");
            }

            return $value;
        }
        elseif (preg_match('/(.*)\{(.*)\}(.*)/', $argument, $matches)){
            if (null !== constant($matches[2])) {
                return $matches[1] . constant($matches[2]) . $matches[3];
            }
            else {
                return $argument;
            }
        }
        else {
            return $argument;
        }
    }

    protected function resolveReference(Icm_Container_Reference $r) {
        return $this->get($r->getValue());
    }

    protected static function getContainer($name) {
        return array_key_exists($name, self::$containers) ? self::$containers[$name] : null;
    }

    /*
     * Some magic syntactic suga
     */
    public function __get($property) {
        return $this->get($property);
    }

    public function __call($method, $args) {
        return $this->get($method);
    }

    public function __invoke($property) {
        return $this->get($property);
    }
}
