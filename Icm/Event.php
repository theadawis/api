<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/11/12
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Event implements Icm_Event_Interface
{

    protected $propogationStopped = false;


    /**
     * @return bool
     */
    public function propogationStopped() {
        return $this->propogationStopped;
    }

    public function stopPropogation() {
        $this->propogationStopped = true;
    }
}
