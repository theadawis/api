<?php
/**
 * TCG HashMap object.
 * Objects used as hashmap keys MUST implement Icm_Hashmap
 * @author Joe Linn
 *
 */
class Icm_HashMap implements Iterator, ArrayAccess, Countable{
    protected $objectType;

    protected $values = array();
    protected $keys = array();
    protected $position = 0;

    public function __construct($objectType){
        if (!in_array('Icm_Hashable', class_implements($objectType))){
            throw new Exception("HashMap keys must implement Icm_Hashable. Shame be upon ye.");
        }
        if (is_string($objectType)){
            $this->objectType = $objectType;
        }
        else{
            $this->objectType = get_class($objectType);
        }
    }

    public function offsetExists($offset){
        if ($offset instanceof $this->objectType){
            return isset($this->values[$offset->getHash()]);
        }
        return isset($this->keys[$offset]) && isset($this->values[$offset]);
    }

    public function offsetGet($offset){
        if ($this->offsetExists($offset)){
            if ($offset instanceof $this->objectType){
                return $this->values[$offset->getHash()];
            }
            return $this->values[$offset];
        }
        return NULL;
    }

    public function offsetSet($offset, $value){
        if (!$offset instanceof $this->objectType){
            throw new Exception("Keys must be instances of {$this->objectType}.");
        }
        $hash = $offset->getHash();
        if (!is_scalar($hash)){
            throw new Exception("Hash values must be scalar values.");
        }
        $this->keys[$hash] = $offset;
        $this->values[$hash] = $value;
    }

    public function offsetUnset($offset){
        if ($this->offsetExists($offset)){
            unset($this->keys[$offset]);
            unset($this->values[$offset]);
        }
    }

    public function rewind(){
        reset($this->keys);
        reset($this->values);
        $this->position = 0;
    }

    /**
     *
     * @return array array(object (key), value)
     */
    public function current(){
        return array(current($this->keys), current($this->values));
    }

    /**
     * As specified by the Iterator interface, this function must return a scalar.
     * Therefore, we have elected to return the hash of the current key object.
     * @return scalar
     */
    public function key(){
        $keys = array_keys($this->values);
        return $keys[$this->position];
    }

    public function next(){
        next($this->keys);
        next($this->values);
        ++$this->position;
    }

    public function valid(){
        if (!current($this->keys) instanceof $this->objectType){
            return false;
        }
        return isset($this->values[current($this->keys)->getHash()]);
    }

    public function count(){
        return sizeof($this->keys);
    }

    public function getKeySet() {
        return array_values($this->keys);
    }

    public function contains($product){
        return $this->offsetExists($product);
    }
}