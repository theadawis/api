<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StorageAdapter
 * This class is used to perform operations pertaining to
 * user permissions
 * @author fadi
 */
class Icm_Auth_Permission_StorageAdapter extends Icm_Db_Table
{
    /**
     * This method returns an array of all permissions relavant of the role id
     * @param int $role
     * @return array
     */
    public function getPermissions($role) {
        if (is_array($role)) {
            $rolesArray = array();
            foreach ($role as $key => $value) {
                if ($value['parentId']) {
                    $sql = "SELECT * FROM {$this->tableName} WHERE
                    `role` = :role ";
                   $param = array(':role' => $value['parentId']);
                   $rolesArray[] = $this->conn->fetchAll($sql, $param);
                }
            }

            if (count($rolesArray)) {
                return $rolesArray;
            }

        }
        else
        {
           $sql = "SELECT * FROM {$this->tableName} WHERE
            `role` = :role ";
           $param = array(':role' => $role);
           return $this->conn->fetchAll($sql, $param);
        }

    }
}


