<?php
class Icm_Auth_Permission extends Icm_Auth_Abstract{
    public function __construct($connection){
        $this->connection = $connection;
        $this->table = new Icm_Db_Table('user_permissions', 'id', $this->connection);
    }
}
?>