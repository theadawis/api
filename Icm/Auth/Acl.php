<?php

/**
 * This class is used to authenticate users based on their permission
 * @author Fadi Adawi
 */
class Icm_Auth_Acl{
    protected $acl,
              $connection,
              $userPermissionAdapter,
              $userResourcesAdapter,
              $resources,
              $className,
              $methodName;

    public function __construct() {
        $this->acl = new Zend_Acl();
        $this->resources = array();

        // assign default access
        $this->globalAccess = array('user/login', 'error/unauthorized', 'company/index',
                                    'gateway/index', 'trafficsources/index',
                                    'fraud/index', 'user/logout', 'home/index');
    }

    /**********************************
     * adapter section:
     **********************************/

    /**
     * This method creates an adpater to handle all db
     * operations that pertain to the user_permission table
     * @param class $adapterName
     */
    public function setUserPermissionAdapter($adapterName) {
        if (!isset($this->userPermissionAdapter)) {
            $this->userPermissionAdapter = $adapterName;
        }
    }

    /**
     * This method creates an adpater to handle all db
     * operations that pertain to the user_resources table
     * @param class $adapterName
     */
    public function setUserResourcesAdapter($adapterName) {
        if (!isset($this->userResourcesAdapter)) {
            $this->userResourcesAdapter = $adapterName;
        }
    }

    /**
     * This method creates an adpater to handle all db
     * operations that pertain to the user_roles table
     * @param class $adapterName
     */
    public function setUserRolesAdapter($adapterName) {
        if (!isset($this->userRolesAdapter)) {
            $this->userRolesAdapter = $adapterName;
        }
    }

    protected function _getParentRoles($role) {
        return $this->userRolesAdapter->returnParentRoles($role);
    }

    /**
     * Convert an array of resource ids to names
     *
     * @param array $resourceIds
     * @return array
     */
    protected function _returnResourcesByName($resourceIds) {
        return $this->userResourcesAdapter->returnResourcesByName($resourceIds);
    }

    /**********************************
     * menu assignment section:
     **********************************/

    /**
     * This method determine if user has an access to a menu item
     * @param int $role
     * @param int $menuPermissionId
     * @return boolean
     */
    public function canViewMenu($role, $menuPermissionId) {
        return $role == $menuPermissionId ? true : false;
    }

    /**********************************
     * User permission assignment section:
     **********************************/
    /**
     * This method is used when a session exists for roles
     * @param string $role
     * @param array $sessionResources
     * @return boolean
     */
    public function createPermissionsFromSession($role, $sessionResources = array()) {
        $allowedUrls = array();
        $isAllowed = false;

        // flatten the session resources array
        if (count($sessionResources)) {
            $sessionResources = array_reduce($sessionResources, 'array_merge', array());
        }

        // save the resulting array
        $this->resources = $sessionResources;

        if (count($this->resources)) {
            // build the list of urls based on the passed-in resources
            foreach ($this->resources as $resource) {
                $allowedUrls[] = $this->buildResourceUrl($resource);
            }
        }

        // add resources from globalAccess array
        $allowedUrls = array_merge($this->globalAccess, $allowedUrls);

        // ensure the allowed urls are unique
        $allowedUrls = array_unique($allowedUrls);

        // add the role to our acl
        $this->acl->addRole(new Zend_Acl_Role($role));

        // add the urls to the role
        foreach ($allowedUrls as $url) {
            $this->acl->add(new Zend_Acl_Resource($url));
            $this->acl->allow($role, null, $url);
        }

        // check if the role is allowed to see the current url
        $currentUrl = $this->className . '/' . $this->methodName;
        $isAllowed = $this->canViewUrl($role, $currentUrl);

        return $isAllowed;
    }

    /**
     * Create array of permitted resources for a given role
     *
     * @param string $role
     * @return array
     */
    public function createPermissions($role) {
        $resources = array();

        // get a list of roles
        $resourceIds = $this->_getResources($role);

        // now that we have resource ids, get the resources by name:
        $resources = $this->_returnResourcesByName($resourceIds);
        $this->resources = array_merge($resources, $this->resources);

        return $this->resources;
    }

    /**
     * Check if the given role is allowed to view the given url
     *
     * @param string $role
     * @param string $url
     * @return bool
     */
    public function canViewUrl($role, $url = null) {
        // if we haven't had any resources setup, deny everything
        if (count($this->resources) == 0) {
            return false;
        }

        // use our set url if none passed in
        if (empty($url)) {
            $url = $this->className . '/' . $this->methodName;
        }

        // use zend acl to check permissions
        return $this->acl->isAllowed($role, null, $url);
    }

    /**
     * Build a string url for a given resource
     *
     * @param string $resource
     * @return string
     */
    protected function buildResourceUrl($resource) {
        $allowedUrl = '';
        $classResource = '';
        $methodResource = $this->methodName;

        // if there is an action, add it to the resources
        if (isset($resource['name'])) {
            // always add the name to the allowed urls list
            $allowedUrls[] = $resource['name'];

            // check if we should add the method name as well
            $lastOccurrance = strrchr($resource['name'], '/');

            if ($lastOccurrance == '/') {
                $allowedUrl = $resource['name'] . $this->methodName;
            }
        }
        else {
            if (isset($resource['class'])) {
                $classResource = $resource['class'];
            }

            if (isset($resource['method'])) {
                $methodResource = $resource['method'];
            }

            $allowedUrl = $classResource . '/' . $methodResource;
        }

        return $allowedUrl;
    }

    /**
     * This method gets a list of resource ids based on roles
     * @param int $role
     */
    protected function _getResources($role) {
        $resourceIds = array();

        // add permissions for the current role
        $resourceIds[] = $this->userPermissionAdapter->getPermissions($role);

        // add permissions for the parent roles
        $parentRoles = $this->_getParentRoles($role);

        foreach ($parentRoles as $key => $value) {
            $resourceIds[] = $this->userPermissionAdapter->getPermissions($value['parentId']);
        }

        return $resourceIds;
    }

    /**
     * Set the class name to use for permissions check
     * @param string $class
     */
    public function setClassName($class) {
        $this->className = $class;
    }

    /**
     * Set the method name to use for permissions check
     * @param string $method
     */
    public function setMethodName($method) {
        $this->methodName = $method;
    }

    /**
     * This method gets all roles
     * @return array
     */
    public function getDataRoles() {
        return $this->userRolesAdapter->getData();
    }

    /**
     *
     * The following are temporary methods that
     * will return true. They are implemented pending controller
     * code clean up that will eliminate the need for such methods.
     */
    public function can_view($para1 = NULL, $para2=NULL) {
        return true;
    }

    public function can_admin($para1 = NULL, $para2=NULL) {
        return true;
    }

    public function can_modify($para1 = NULL, $para2=NULL) {
        return true;
    }
}
