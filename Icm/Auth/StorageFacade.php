<?php
/**
 * This class assigns proper adapters and communicate with them
 * in order to perform operation pertaining to role management
 * @author Fadi Adawi
 */
class Icm_Auth_StorageFacade{
    protected $connection, $userPermissionAdapter,
                $userResourcesAdapter, $userRolesAdapter, $userAdapter,
                $resourceIds;

        /**********************************
         * adapter section:
         **********************************/

    /**
         * This method creates an adpater to handle all db
         * operations that pertain to the user_permission table
         * @param class $adapterName
         */
        public function setUserPermissionAdapter($adapterName) {
            if (!isset($this->userPermissionAdapter)) {
                 $this->userPermissionAdapter = $adapterName;
            }
        }
        /**
         * This method creates an adpater to handle all db
         * operations that pertain to the user_resources table
         * @param class $adapterName
         */
        public function setUserResourcesAdapter($adapterName) {
            if (!isset($this->userResourcesAdapter)) {
                 $this->userResourcesAdapter = $adapterName;
            }
        }
        /**
         * This method creates an adpater to handle all db
         * operations that pertain to the user_roles table
         * @param class $adapterName
         */
        public function setUserRolesAdapter($adapterName) {
            if (!isset($this->userRolesAdapter)) {
                 $this->userRolesAdapter = $adapterName;
            }
        }
        /**
         * This method creates an adpater to handle all db
         * operations that pertain to the users table
         * @param class $adapterName
         */
        public function setUserAdapter($adapterName) {
            if (!isset($this->userAdapter)) {
                 $this->userAdapter = $adapterName;
            }
        }

        /**********************************
         * User resources section:
         **********************************/
        /**
         * This method gets all parent classes based on the method name and resource id
         * @param string $methodName
         * @param int $currentResourceId
         * @return array
         */
        public function getParentClassForMethod($methodName, $currentResourceId) {
            return $this->userResourcesAdapter->getParentClassForMethod($methodName, $currentResourceId);
        }
        /**
        * This method gets all classes and methods based on the resource id
        * @return array
        */
        public function getclassAndMethodsByResourceId($resourceId) {
            return $this->userResourcesAdapter->getclassAndMethodsByResourceId($resourceId);
        }
        /**
         * This method gets all classes based on the resource id
         * @param int $currentResourceId
         * @return array
         */
        public function getclassByResourceId($resourceId) {
            return $this->userResourcesAdapter->getclassByResourceId($resourceId);
        }
        /**
         * This method returns an array of resources
         * @return array
         */
        public function getDataResources() {
            return $this->userResourcesAdapter->getData();
        }
        /**
         * This method returns an array of resources based on resource id
         * @param int $data
         * @return string
         */
        public function getResourceById($resourceId) {
            return $this->userResourcesAdapter->getDataById($resourceId);
        }
        /**
         * This method updates data pertaining to resources
         * @param array $data
         * @return string
         */
        public function saveDataResourcesEdit($data) {
           return $this->userResourcesAdapter->editData($data);
        }
        /**
         * This method saves resource data
         * @param array $data
         * @return array
         */
        public function saveDataResources($data) {
            return $this->userResourcesAdapter->saveData($data);
        }

        /**********************************
         * User permission assignment section:
         **********************************/

        /**
         * This method returns resource ids
         * @param int $role
         */
        protected function _getPermissions($role) {
            $this->resourceIds = $this->userPermissionAdapter->getPermissions($role);
        }

        /**
         * Calls the _getPermissions method and returns
         * all permissions that match the specified roleId
         * @param int $roleId
         * @return array
         */
        public function getDataPermissions($roleId) {
            $this->_getPermissions($roleId);
            return $this->resourceIds;

        }
         /**********************************
         * User roles section:
         **********************************/
        /**
         * This method saves roles in the user_roles table
         * @param array $data
         * @return array
         */
        public function saveDataRoles($data) {
            return $this->userRolesAdapter->saveData($data);
        }
        /**
         * This method updates roles in the user_roles table
         * @param array $data
         * @return array
         */
        public function updateDataRoles($data) {
            return $this->userRolesAdapter->updateData($data);
        }
        /**
         * This method gets all roles
         * @return array
         */
        public function getDataRoles() {
            return $this->userRolesAdapter->getData();
        }

}
