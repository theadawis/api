<?php
class Icm_Auth_Resource extends Icm_Auth_Abstract{
    const DEFAULT_TABLE = 'user_resources';
    public function __construct($connection) {
            $this->connection = $connection;
            $this->table = new Icm_Db_Table('user_resources', 'id', $this->connection);

    }
    public function getParents($currentParent=NULL) {
        $table = self::DEFAULT_TABLE;
        if ($currentParent != NULL) {
            $sql = "SELECT `id`, `name` FROM {$table}
            WHERE `id` != :currentParent";
            $param = array(':currentParent' => $currentParent);
            return $this->connection->fetchAll($sql, $param);
        }
        else
        {
            $sql = "SELECT `id`, `name` FROM {$table}
            ";
            return $this->connection->fetchAll($sql);
        }

    }
}
