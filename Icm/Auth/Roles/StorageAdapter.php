<?php
/**
 * This class handles all operations pertaining to user_roles
 * @author fadi
 */
class Icm_Auth_Roles_StorageAdapter extends Icm_Db_Table
{
    /**
     * This method returns an array or roles based on role id
     * @param int $roleId
     * @return array
     */
    public function returnParentRoles($roleId) {
        $sql = "SELECT `parentId` FROM {$this->tableName}
        WHERE `id` = :id";
        $params = array(':id' => $roleId);
        return $this->conn->fetchAll($sql, $params);
    }
    /**
     * This method returns an array of user_roles contents
     * @return array
     */
    public function getData() {
        $sql = "SELECT * FROM {$this->tableName}";
        return $this->conn->fetchAll($sql);
    }
    /**
     * This method inserts data into the user_permission table
     * @param array $data
     * @param int $roleId
     */
    protected function _createPermissions($data, $roleId) {
        $table = 'user_permissions';
        foreach ($data as $value) {
            $sql = "INSERT INTO {$table} (`role`, `resource`, `view`) VALUES (:role, :resource, :view)";
            $params = array(':role' => $roleId, ':resource' => $value, ':view' => '1');
            $this->conn->execute($sql, $params);
        }
    }
    /**
     * This method updates the user_permission table by deleting
     * permissions when a role is deleted
     * @param array $data
     * @param int $roleId
     */
    protected function _updatePermissions($data, $roleId) {
        $table = 'user_permissions';
        $sql = "DELETE FROM {$table} WHERE `role` = :role";
        $params = array(':role' => $roleId);
        $this->conn->execute($sql, $params);
        $this->_createPermissions($data, $roleId);
    }
    /**
     * This method saves data into the user_roles table
     * and then saves data to all involved tables. It uses transaction
     * to roll back if any of the operations don't go through
     * @param array $data
     * @return string
     */
    public function saveData($data) {
        $resourcesData = array('name' => $data['name'], 'description' => $data['description'], 'parentId' => $data['parentId']);
        try
            {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->insertOne($resourcesData);
                $roleId = $this->conn->lastInsert();
                $this->_createPermissions($data['resources'], $roleId);
                // All went wel so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
    }
    /**
     * This method updates data pertaining to the user_roles table
     * and all other involved tables. It uses transaction
     * to roll back if any of the operations don't go through
     * @param array $data
     * @return string
     */
    public function updateData($data) {
        $resourcesData = array('id' => $data['id'], 'name' => $data['name'], 'description' => $data['description'], 'parentId' => $data['parentId']);
        try
            {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->updateOne($resourcesData);
                // If not a super role
                if ($data['id'] != '4') {
                   $this->_updatePermissions($data['resources'], $data['id']);
                }

                // All went wel so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
    }
}
