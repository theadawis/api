<?php

/**
 * This StorageAdapter handles all operations that pertain to
 * resources
 * @author fadi
 */
class Icm_Auth_Resources_StorageAdapter extends Icm_Db_Table
{
    /**
     * This method returns all parent classes based on method name and
     * the resource id
     * @param int $methodName
     * @param int $currentResourceId
     * @return array
     */
    public function getParentClassForMethod($methodName, $currentResourceId) {
        $sql = "SELECT `cr`.`id`, `cr`.`class`, `cr`.`class_path`"
            . " FROM `class_resources` as `cr`"
            . " JOIN `method_resources` as `mr`"
            . " JOIN `user_resources` as `ur`"
            . " ON `mr`.`parent_id` = `cr`.`id`"
            . " AND `ur`.`id` = `cr`.`resources_id`"
            . " AND `mr`.`method` = :method"
            . " AND `ur`.`id` = :currentResourceId";

        $params = array(':method' => $methodName, ':currentResourceId' => $currentResourceId);

        return $this->conn->fetchAll($sql, $params);
    }

    /**
     * This method calls a protected method in order to get methods based on a
     * resource id
     * @param int $resourceId
     * @return array
     */
    public function getclassAndMethodsByResourceId($resourceId) {
        return $this->_getMethod($resourceId);
    }

    /**
     * This method calls a protected method in order to get classes based on a
     * resource id
     * @param int $resourceId
     * @return array
     */
    public function getclassByResourceId($resourceId) {
        return $this->_getClass($resourceId);
    }

    /**
     * This method returns single resource info basedon resource id
     * @param int $resourceId
     * @return array
     */
    public function getDataById($resourceId) {
        $sql = "SELECT * FROM {$this->tableName}
        WHERE `id` = :id";
        $params = array(':id' => $resourceId);
        return $this->conn->fetch($sql, $params);
    }

    /**
     * This method retrieves data from the user_resources table
     * @return array
     */
    public function getData() {
        $sql = "SELECT * FROM {$this->tableName}";
        return $this->conn->fetchAll($sql);
    }

    /**
     * This method returns an array of methods based on the resource id
     * @param int $id
     * @return array
     */
    protected function _getMethod($id) {
        $sql = "SELECT `ur`.id, `cr`.`resources_id`, `cr`.`class`, `cr`.`class_path`, `mr`.`method`
                FROM
        `user_resources` as `ur`
        JOIN
        `class_resources` as `cr`
                JOIN
        `method_resources` as `mr`
        ON
        `ur`.`id` = `cr`.`resources_id` AND
                `cr`.`id` = `mr`.`parent_id` AND `ur`.`id` =:id";
        $params = array(':id' => $id);

        return $this->conn->fetchAll($sql, $params);
    }

    /**
     * This method returns an array of actions based on the resource id
     * @param int $id
     * @return array
     */
    protected function _getAction($id) {
        $sql = "SELECT `name` FROM {$this->tableName} WHERE
        `id` = :id ";
        $params = array(':id' => $id);

        return $this->conn->fetchAll($sql, $params);
    }

    /**
     * This method returns an array of classes based on the resource id
     * @param int $id
     * @return array
     */
    protected function _getClass($id) {
        $table = 'class_resources';
        $sql = "SELECT `id`, `class`, `class_path` FROM {$table} WHERE `resources_id` = :resources_id";
        $param = array(':resources_id' => $id);
        return $this->conn->fetchAll($sql, $param);
    }

    /**
     * This class returns an array of resource names
     * @param array $resources
     * @param string $class
     * @return array
     */
    public function returnResourcesByName($resources) {
        $data = array();

        // Get resource name, description and parentId based on the array parameter:
        if (count($resources)) {
            foreach ($resources as $key => $resourceId) {
                foreach ($resourceId as $key2 => $value2) {
                    $id = $value2['resource'];
                    $sql = "SELECT `name`, `scope` FROM {$this->tableName} WHERE
                    `id` = :id ";
                    $param = array(':id' => $id);
                    $result = $this->conn->fetch($sql, $param);

                    if ($result['scope'] == 'class') {
                        $data[] = $this->_getClass($id);
                    }

                    if ($result['scope'] == 'method') {
                        $data[] = $this->_getMethod($id);
                    }

                    if ($result['scope'] == 'action') {
                        $data[] = $this->_getAction($id);
                    }
                }
            }
        }

        return $data;
    }

    /**
     * This function inserts class names into the "class_resources"
     * table
     * @param array/string $data
     * @param string $resourceId
     */
    protected function _saveClasses($data, $resourceId) {
        $table = 'class_resources';

        foreach ($data as $value) {
            $valueParsed = explode('-', $value);
            $className = $valueParsed[0];
            $classPath = $valueParsed[1];
            $sql = "INSERT INTO {$table} (`resources_id`, `class`, `class_path`) VALUES (:resources_id, :class, :classPath)";
            $params = array(':class' => $className, ':resources_id' => $resourceId, ':classPath' => $classPath);
            $this->conn->execute($sql, $params);
        }
    }

    /**
     * This method get the resource id based on class id
     * @param int $classId
     * @return array
     */
    protected function _getResourceIdFromClassId($classId) {
        $table = 'class_resources';
        $sql = "SELECT `resources_id` FROM {$table} WHERE `id` = :id";
        $params = array(':id' => $classId);

        return $this->conn->fetch($sql, $params);
    }

    /**
     * This method delets a resource based on its id
     * @param int $id
     * @return array
     */
    protected function _deleteResource($id) {
        $sql = "DELETE` FROM {$this->tableName} WHERE `id` = :id";
        $params = array(':id' => id);
        return $this->conn->execute($sql, $params);
    }

    /**
     * This method checks if a class has children methods
     * @param int $classId
     * @return array
     */
    protected function _doesClassHaveChildren($classId) {
        $table = 'method_resources';
        $sql = "SELECT `id` FROM {$table} WHERE `parent_id` = :parentId";
        $params = array(':parentId' => $classId);
        return $this->conn->fetchAll($sql, $params);
    }

    /**
     * This method delets the class if it doesn't have children methods
     * @param int $classId
     */
    protected function _deleteClassIfNoMethod($classId) {
        $table = 'class_resources';
        $isClass = $this->_doesClassHaveChildren($classId);

        if (!count($isClass)) {
            $resources = $this->_getResourceIdFromClassId($classId);
            $sql = "DELETE FROM {$table} WHERE `id` = :id";
            $params = array(':id' => $classId);
            $this->conn->execute($sql, $params);
        }
    }

    /**
     * This method delets a method based on the class name, method name
     * and the resource id
     * @param string $className
     * @param string $methodName
     * @param int $currentResourceId
     */
    protected function _deleteMethod($className, $methodName, $currentResourceId) {
        $table = 'method_resources';
        $classInfo = $this->getParentClassForMethod($methodName, $currentResourceId);

        foreach ($classInfo as $key => $value) {
            if ($value['class'] == $className) {
                $sql = "DELETE FROM {$table} WHERE `method` = :method AND
                `parent_id` = :parentId";
                $params = array(':method' => $methodName, ':parentId' => $value['id']);
                $this->conn->execute($sql, $params);
                $this->_deleteClassIfNoMethod($value['id']);
            }
        }
    }

    /**
     * This method delets a class based on the class name, class path and
     * resurce id
     * @param string $className
     * @param string $classPath
     * @param int $currentResourceId
     */
    protected function _deleteClass($className, $classPath, $currentResourceId) {
        $table = 'class_resources';
        $sql = "DELETE FROM {$table} WHERE `class` = :class AND
        `resources_id` = :resources AND `class_path` = :classPath";
        $params = array(':class' => $className, ':resources' => $currentResourceId,
                        ':classPath' => $classPath);
        $this->conn->execute($sql, $params);
    }

    public function _updateClasses($data, $unchecked, $resourceId) {
        $table = 'class_resources';
        $filter = array();
        $originalClasses = $this->_getClass($resourceId);

        // Get all original methods of the resource:
        foreach ($originalClasses as $key => $value) {
            $originalClassesArray[] = $value['class'] . '-' . $value['class_path'];
        }

        foreach ($unchecked as $key => $value) {
            $element = explode('-', $value);
            $className = strtolower($element[0]);
            $classPath = $element[1];
            $unckeckedArray[] = $className.'-' . $classPath;
        }

        // Find common elements:
        $result = array_intersect($originalClassesArray, $unckeckedArray);

        foreach ($result as $value) {
            // Delete methods
            $element = explode('-', $value);
            $className = $element[0];
            $classPath = $element[1];
            $this->_deleteClass($className, $classPath, $resourceId);
        }

        foreach ($data as $value) {
            $valueParsed = explode('-', $value);
            $className = $valueParsed[0];
            $classPath = $valueParsed[1];
            $status = $valueParsed[2];

            if ($status == 'new') {
                $sql = "INSERT INTO {$table} (`resources_id`, `class`, `class_path`) VALUES (:resources_id, :class, :classPath)";
                $params = array(':class' => $className, ':resources_id' => $resourceId, ':classPath' => $classPath);
                $this->conn->execute($sql, $params);
            }
        }
    }

    /**
     * This method is used to update resource methods.
     * It determines which methods have been checked and which methods have
     * been added. The "$unckecked" array contains a list of all unchecked
     * checkboxes. This step is necessary to determine new elements that have been added
     * @param array $data
     * @param array $unchecked
     * @param int $resourceId
     */
    public function _updateMethods($data, $unchecked, $resourceId) {
        $table = 'method_resources';
        $filter = array();
        $originalMethods = $this->_getMethod($resourceId);

        // Get all original methods of the resource:
        foreach ($originalMethods as $key => $value) {
            $originalMethodArray[] = $value['class'] . '-' . $value['method'];
        }

        // Now let clean up the unchecked data:
        foreach ($unchecked as $key => $value) {
            $element = explode('-', $value);
            $className = strtolower($element[0]);
            $methodName = $element[1];
            $unckeckedArray[] = $className.'-' . $methodName;
        }

        // Find common elements:
        $result = array_intersect($originalMethodArray, $unckeckedArray);

        foreach ($result as $value) {
            // Delete methods
            $element = explode('-', $value);
            $className = $element[0];
            $methodName = $element[1];
            $this->_deleteMethod($className, $methodName, $resourceId);
        }

        // Now add to the method resources:
        /*
         * Data is an array that consists of class methods in the following format:
         * Class-method-path-status. We need to strip the array element in order to extract the
         * class and method names.
         */
        foreach ($data as $value) {
            $elements = explode('-', $value);
            $className = strtolower($elements[0]);
            $methodName = $elements[1];
            $classPath = $elements[2];
            $status = $elements[3];

            // New element
            if ($status == 'new') {

                if (!in_array($className, $filter)) {
                    $filter[] = $className;
                    $classData = array($className.'-' . $classPath);
                    $this->_saveClasses($classData, $resourceId);
                    $classresourceId = $this->conn->lastInsert();
                }

                $sql = "INSERT INTO {$table} (`method`, `parent_id`) VALUES (:method, :parent_id)";
                $params = array(':method' => $methodName, ':parent_id' => $classresourceId);
                $this->conn->execute($sql, $params);
            }
        }
    }

    /**
     * This function saves the methods array into the "method_resources" table
     * @param array $data
     * @param string $resourceId
     */
    protected function _saveMethods($data, $resourceId) {
        $table = 'method_resources';
        $filter = array();

        /*
         * Data is an array that consists of class methods in the following format:
         * Class-method. We need to strip the array element in order to extract the
         * class and method names.
         */
        foreach ($data as $value) {
            $elements = explode('-', $value);
            $className = strtolower($elements[0]);
            $methodName = $elements[1];
            $classPath = $elements[2];

            if (!in_array($className, $filter)) {
                $filter[] = $className;
                $classData = array($className.'-' . $classPath);
                $this->_saveClasses($classData, $resourceId);
                $classresourceId = $this->conn->lastInsert();
            }

            $sql = "INSERT INTO {$table} (`method`, `parent_id`) VALUES (:method, :parent_id)";
            $params = array(':method' => $methodName, ':parent_id' => $classresourceId);
            $this->conn->execute($sql, $params);
        }
    }

    /**
     * This method updates resources data based on the scope parameter
     * @param array $data
     * @return string
     */
    public function editData($data) {
        $resourcesData = array('id' => $data['id'], 'name' => $data['name'], 'scope' => $data['scope']);

        switch($data['scope']) {
        case 'method':
            try {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->updateOne($resourcesData);
                $this->_updateMethods($data['methods'], $data['unchecked'], $data['id']);
                // All went wel so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
            break;

        case 'class':
            try {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->updateOne($resourcesData);
                $this->_updateClasses($data['class'], $data['unchecked'], $data['id']);
                // All went well so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
            break;

        case 'action':
            try {
                $this->updateOne($resourcesData);
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                return "Failed: " . $e->getMessage();
            }
            break;
        }
    }

    /**
     * This function saves the resources that consist of the class name and the
     * resource name in the "user_resources" table.
     * @param array $data
     */
    public function saveData($data) {
        $resourcesData = array('name' => $data['name'], 'scope' => $data['scope']);

        switch($data['scope']) {
        case 'method':
            try {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->insertOne($resourcesData);
                $resourceId = $this->conn->lastInsert();
                $this->_saveMethods($data['methods'], $resourceId);
                // All went wel so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
            break;

        case 'class':
            try {
                // Begin transaction
                $this->conn->beginTransaction();
                $this->insertOne($resourcesData);
                $resourceId = $this->conn->lastInsert();
                $this->_saveClasses($data['class'], $resourceId);

                // All went wel so commit
                $this->conn->commit();
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                $this->conn->rollBack();
                return "Failed: " . $e->getMessage();
            }
            break;
        case 'action':
            try {

                $this->insertOne($resourcesData);
                return 'Data was submitted successfully';
            }
            catch (Exception $e) {
                // Something went wrong, so roll back:
                return "Failed: " . $e->getMessage();
            }
            break;
        }
    }
}


