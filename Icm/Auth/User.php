<?php
class Icm_Auth_User extends Icm_Auth_Abstract{


        const RESOURCE_ADMIN_USERS = 1;
    const RESOURCE_ADMIN_ROLES = 2;
    const RESOURCE_ADMIN_RESOURCES = 3;
    const RESOURCE_OPTOUT = 15;
    const RESOURCE_MARKETING = 18;
    const RESOURCE_WIKI = 6;
    const RESOURCE_HOROSCOPE = 17;
    const RESOURCE_RATELIMITER = 16;
    const RESOURCE_SEARCH_PREVIEW = 19;
        /*
         * This is list of new constant to deal with menus
         * It will eventually replace the cons list above
         */
         const RESOURCE_SUPER_ADMIN = 4;
         const RESOURCE_DEVELOPER = 3;
         const RESOURCE_STAFF = 2;
    /**
     * @param Icm_Db_Pdo $connection
     */
    public function __construct(Icm_Db_Pdo $connection){
        $this->connection = $connection;
        $this->table = new Icm_Db_Table('users', 'id', $this->connection);
    }

    /**
     * Retrieves data for the given user id
     * @param int $id
     * @return mixed:|boolean
     */
    public function getUserById($id){
        return $this->getById($id);
    }

    /**
     * Retrieves data for the given username
     * @param string $username
     * @return mixed:|boolean
     */
    public function getUserByUsername($username){
        if ($info = $this->table->findOneBy('username', $username)){
            $this->tableData = $info;
            return $this->tableData;
        }
        return false;
    }

    /**
     * @param string $email
     * @return mixed:|boolean
     */
    public function getUserByEmail($email){
        if ($info = $this->table->findOneBy('email', $email)){
            $this->tableData = $info;
            return $this->tableData;
        }
        return false;
    }

    /**
     * @param string $username
     * @param string $password
     * @return array containing user data if credentials are valid and user is active; bool false otherwise
     */
    public function getUserByCredentials($username, $password){
        if ($info = $this->table->findOneByFields(array('username' => $username))){
            // Valid user credentials provided
            $this->tableData = $info;
            if ($this->tableData['active'] && $this->tableData['password'] === $this->hashPassword($password, $this->tableData['salt'])){
                // User's account is active
                return $this->tableData;
            }
        }
        return false;
    }

    /**
     * @param array $data is an associative array: array('field' => 'value'). Password data will be hashed in this function.
     * @return boolean true if user creation succeeded; false otherwise
     */
    public function create(array $data){
        if (!$this->getUserByUsername($data['username'])){
            // no user with the given username currently exists; proceed
            $data['salt'] = $this->randomString(5);
            $data['password'] = $this->hashPassword($data['password'], $data['salt']);
            return parent::create($data);
        }
        return false;
    }

    /**
     * @param array $data is an associative array: array('field' => 'value). Password will be hashed.
     * @return boolean
     */
    public function modify(array $data){
        if (isset($data['password'])){
            $data['salt'] = $this->randomString(5);
            $data['password'] = $this->hashPassword($data['password'], $data['salt']);
        }
        return parent::modify($data);
    }

    /**
     * @param string $value
     * @return boolean
     */
    public function setPassword($value){
        $salt = $this->randomString(5);
        return $this->set('password', $this->hashPassword($value, $salt));
    }

    /**
     * @param string $password
     * @param string $salt
     * @return string hashed password
     */
    protected function hashPassword($password, $salt){
        return hash('sha512', hash('sha512', $password) . $salt);
    }

    /**
     * @param int $length
     * @return string
     */
    protected function randomString( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $str = '';
        $size = strlen( $chars );
        for ( $i = 0; $i < $length; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }

        return $str;
    }
}
