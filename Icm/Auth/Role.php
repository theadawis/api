<?php
class Icm_Auth_Role extends Icm_Auth_Abstract{
    public function __construct(Icm_Db_Pdo $connection){
        $this->connection = $connection;
        $this->table = new Icm_Db_Table('user_roles', 'id', $this->connection);
    }

    /**
     * Assumes $this->tableData has been populated with data for a Role.
     * @return array of permissions data associated with the current Role
     */
    public function getPermissions(){
        return $this->connection->fetchAll("SELECT * FROM user_permissions WHERE role=:roleid", array(':roleid' => $this->tableData['id']));
    }
}
?>