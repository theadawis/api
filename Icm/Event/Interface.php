<?php
interface Icm_Event_Interface {

    /**
     * @abstract
     * @return bool
     */
    public function propogationStopped();

    public function stopPropogation();
}