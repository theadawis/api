<?php
interface Icm_Event_Subscriber_Interface extends Icm_Event_Listener_Interface
{
    /**
     * @abstract
     * @return array
     */
    public function getEventList();
}
