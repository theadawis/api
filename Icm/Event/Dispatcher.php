<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/11/12
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
class Icm_Event_Dispatcher
{

    protected $listeners = array();

    public function addListener($evtName, Icm_Event_Listener_Interface $listener) {
        $this->listeners[$evtName][] = $listener;
    }

    public function trigger($evtName, Icm_Event_Interface &$event) {
        foreach ($this->getListeners($evtName) as $listener) {
            $listener->dispatch($evtName, $event);
            if ($event->propogationStopped()) {
                break;
            }
        }
    }

    public function hasListeners($evtName) {
        return array_key_exists($evtName, $this->listeners);
    }

    public function getListeners($evtName) {
        if ($this->hasListeners($evtName)) {
            return $this->listeners[$evtName];
        }
        return array();
    }

    public function addSubscriber(Icm_Event_Subscriber_Interface $s) {
        foreach ($s->getEventList() as $evtName) {
            $this->addListener($evtName, $s);
        }
    }
}
