<?php
/**
 * Created by JetBrains PhpStorm.
 * User: chrisg
 * Date: 5/11/12
 * Time: 1:36 PM
 * To change this template use File | Settings | File Templates.
 */
interface Icm_Event_Listener_Interface
{

    public function dispatch($evtName, Icm_Event_Interface $event);

}
